/*
 * BUILD.h
 *
 *  Created on: May 7, 2021
 *
 */
#ifndef BUILD_H_
#define BUILD_H_

/* PLATFORM ID */
#define ETC				0
#define	ECOMP			1
#define FC				2


/* CUSTOMER ID */
#define STANDARD        0
#define JOHN_DEERE      1
#define AUDI            2
#define FORD            3


#define PLATFORM_ID		ECOMP
#define CUSTOMER_ID     AUDI


#define BUILD_SW_VER_MAJOR		1
#define BUILD_SW_VER_MINOR		1
#define BUILD_SW_VER_REV		4
#define BUILD_SW_VER_CUSTOMER	0


#endif /* BUILD_H_ */
