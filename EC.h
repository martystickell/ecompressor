/*
 * MCS.h
 *
 *  Created on: Oct 19, 2018
 *      Author: H297270
 */

#ifndef MCS_H_
#define MCS_H_

#define ENABLE 		1
#define DISABLE 	0

#define BETA_CB_TEST  	DISABLE
#define TRIBOARD_DEV 	DISABLE

#endif /* MCS_H_ */
