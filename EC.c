/*****************************************************
 *
 * eBoost_sw.c
 *
 * Description : Hello World in C, ANSI-style
 *
 */

#include <CAN_Driver.h>
#include "GTM.h"
#include "vadc.h"
#include <stdio.h>
#include "IfxGtm_reg.h"
#include "IfxPort_reg.h"
#include "IfxSrc_reg.h"
#include <VX1000If.h>
#include "Gpio.h"
#include "MCS.h"
#include "ControlBoardTest.h"

int counter;

/*
unsigned int get_time(void)
{
	return STM0_TIM0.U;;
}
*/

int main(void)
{

	/* initialize all GPIOs to be inputs */
	InitializeAllGPIO();

	/* Make sure your free running timer is running by now */
	VX1000If_Init();
	VX1000If_InitAsyncStart();
	/* Add any code you want to time in here */
	VX1000If_InitAsyncEnd();

	Gtm_PinConfig();
	Gtm_Global_Init();
	Gtm_TOM_Init();


#if (BETA_CB_TEST == DISABLE)
	Vadc_Init();
	GIO_Init();
#else
	CBTest_Vadc_Init();
	CBTest_DigIO_Init();
#endif

	GTM_ConfigureT0Interrupt();
	GTM_ConfigureT1Interrupt();
	GTM_ConfigureT2Interrupt();

	CAN_Driver_Init();
	//MulticanFifoDemo_init();

	Gtm_Enable();

	/* Enable the global interrupts of this CPU */
 	__enable();

	while(1)
	{
		/* do nothing */
	}
}
