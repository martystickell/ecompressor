
/*
 * ControlBoardTest.h
 *
 *  Created on: Oct 19, 2018
 *      Author: H297270
 */

#ifndef CONTROLBOARDTEST_H_
#define CONTROLBOARDTEST_H_

/******************************************************************************/
/*Includes*/
/******************************************************************************/
#include "vadc.h"
#include "Gpio.h"
#include "Gtm.h"

/*
 * Calibrations
 * */
IFX_EXTERN float Gf_CBTTestADCscale;


/******************************************************************************/
/*Data Structures*/
/******************************************************************************/
typedef struct ADC_Readings_f
{
	float ADC_5V_AN1_8;
	float ADC_12V_AN0_8;
	float ADC_IA_1_AN0_0;
	float ADC_IA_2_AN0_2;
	float ADC_IB_1_AN1_0;
	float ADC_IB_2_AN1_2;
	float ADC_IC_AN0_1;
	float ADC_VDC_AN1_1;
	float ADC_VA_AN0_3;
	float ADC_VB_AN1_3;
	float ADC_VC_AN0_4;
	float ADC_T_IMS_BOARD_AN0_5;
	float ADC_T_BOARD_1_AN1_4;
	float ADC_T_BOARD_2_AN1_5;
	float ADC_T_SENSOR_1_AN0_6;
	float ADC_T_SENSOR_2_AN0_7;
	float ADC_T_SENSOR_3_AN1_6;
	float ADC_EXT_AN1_7;
	float ADC_E34_AN0_9;
	float ADC_E35_AN1_9;
	float ADC_P40_10_AN0_10;
	float ADC_P40_11_AN0_11;
	float ADC_P41_10_AN1_10;
	float ADC_P41_11_AN1_11;
}ADC_Float_t;

typedef struct ADC_Readings_hex
{
	uint16 ADC_5V_AN1_8;
	uint16 ADC_12V_AN0_8;
	uint16 ADC_IA_1_AN0_0;
	uint16 ADC_IA_2_AN0_2;
	uint16 ADC_IB_1_AN1_0;
	uint16 ADC_IB_2_AN1_2;
	uint16 ADC_IC_AN0_1;
	uint16 ADC_VDC_AN1_1;
	uint16 ADC_VA_AN0_3;
	uint16 ADC_VB_AN1_3;
	uint16 ADC_VC_AN0_4;
	uint16 ADC_T_IMS_BOARD_AN0_5;
	uint16 ADC_T_BOARD_1_AN1_4;
	uint16 ADC_T_BOARD_2_AN1_5;
	uint16 ADC_T_SENSOR_1_AN0_6;
	uint16 ADC_T_SENSOR_2_AN0_7;
	uint16 ADC_T_SENSOR_3_AN1_6;
	uint16 ADC_EXT_AN1_7;
	uint16 ADC_E34_AN0_9;
	uint16 ADC_E35_AN1_9;
	uint16 ADC_P40_10_AN0_10;
	uint16 ADC_P40_11_AN0_11;
	uint16 ADC_P41_10_AN1_10;
	uint16 ADC_P41_11_AN1_11;
}ADC_Hex_t;

typedef struct GPIO_Inputs
{
	IfxPort_State IA_IB_IC_OC_FLT;
	IfxPort_State VDC_OV_FLT;
	IfxPort_State FAULTS_L_P21_2;
	IfxPort_State FAULTS_L_P33_8;
	IfxPort_State VAB_CROSSING_P00_0;
	IfxPort_State VAB_CROSSING_P00_1;
	IfxPort_State CAN_RXD_P13_1;
	IfxPort_State CAN_RXD_P14_1;
	IfxPort_State CAN_TXD_P14_0;
	IfxPort_State CAN_DISC_IN;
}CBTest_GPIO_Input_st_t;

typedef struct GPIO_Outputs
{
	IfxPort_State UC_CMD_AH;
	IfxPort_State UC_CMD_AL;
	IfxPort_State UC_CMD_BH;
	IfxPort_State UC_CMD_BL;
	IfxPort_State UC_CMD_CH;
	IfxPort_State UC_CMD_CL;
	IfxPort_State FAULTS_RES;
	IfxPort_State CAN_TXD_P13_0;
	IfxPort_State CAN_STB;
	IfxPort_State FLEXRAY_RXD;
	IfxPort_State FLEXRAY_TXD;
	IfxPort_State FLEXRAY_TXEN;
	IfxPort_State WDI;
	IfxPort_State RS422_TXD;
	IfxPort_State RS422_RXD;
	IfxPort_State SPI_1_DOUT;
	IfxPort_State SPI_1_DIN;
	IfxPort_State SPI_1_CLK;
	IfxPort_State SPI_1_CS_L;
	IfxPort_State SPI_2_DOUT;
	IfxPort_State SPI_2_DIN;
	IfxPort_State SPI_2_CLK;
	IfxPort_State SPI_2_CS_L;
	IfxPort_State GPIO_1;
	IfxPort_State GPIO_2;
	IfxPort_State GPIO_3;
	IfxPort_State GPIO_4;
}CBTest_GPIO_Output_st_t;

/******************************************************************************/
/*Global variables*/
/******************************************************************************/
IFX_EXTERN CBTest_GPIO_Input_st_t GsCBTest_GPIOInput;
IFX_EXTERN CBTest_GPIO_Output_st_t GsCBTest_GPIOOutput_val;
IFX_EXTERN CBTest_GPIO_Output_st_t GsCBTest_GPIOOutput_state;
IFX_EXTERN ADC_Hex_t GsCBT_ADC_Hex;
IFX_EXTERN ADC_Float_t GsCBT_ADC_Float;

/******************************************************************************/
/*Function Prototypes*/
/******************************************************************************/
IFX_EXTERN void CBTest_Vadc_Init(void);
IFX_EXTERN void CBTest_DigIO_Init(void);
IFX_EXTERN void CBTest_init_queue0_group0(void);
IFX_EXTERN void CBTest_init_queue0_group1(void);
IFX_EXTERN void SetOutputs(void);
IFX_EXTERN void GetGPIOStatus(void);
IFX_EXTERN void GetCBT_ADCValues(void);
IFX_EXTERN void GetCBT_ADCValues(void);
#endif /* APP_CBT_CONTROLBOARDTEST_H_ */
