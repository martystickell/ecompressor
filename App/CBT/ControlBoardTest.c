/*
 * GPIO_ControlBoardTest.c
 *
 *  Created on: Oct 19, 2018
 *      Author: H297270
 */
#include "ControlBoardTest.h"
#include "MCS.h"

#if (BETA_CB_TEST == ENABLE)

/* Variables  */
float Gf_CBTTestADCscale = 0.001221f;
CBTest_GPIO_Input_st_t GsCBTest_GPIOInput;
CBTest_GPIO_Output_st_t GsCBTest_GPIOOutput_val;
CBTest_GPIO_Output_st_t GsCBTest_GPIOOutput_state;
ADC_Hex_t GsCBT_ADC_Hex;
ADC_Float_t GsCBT_ADC_Float;

void CBTest_Vadc_Init(void)
{
	volatile unsigned int uwTemp;
	uint16  endinit_pw;
	endinit_pw = IfxScuWdt_getCpuWatchdogPassword(); //Get the password for the current CPU

	//Enable the Clock
	//
	//Clear the Endinit Function to access the protected Registers
	IfxScuWdt_clearCpuEndinit(endinit_pw);
		//A. Enable the GTM Module:
		VADC_CLC.U   = 0x00000000;   // load clock control register
		uwTemp         = VADC_CLC.U; // dummy read to avoid pipeline effects
		while ((VADC_CLC.U & 0x00000002 )== 0x00000002);  //wait until module is enabled
		//SET the Endinit Function after access to the protected Registers
	IfxScuWdt_setCpuEndinit(endinit_pw);
	//


	/*Configure Queue Source*/
		//1st Enable converter
		//2nd Set divider factor for analog internal clock and start calibration
		//3rd Enable arbitration slot 0 (queue source) and assign priority
		//4th Wait for calibration to complete (can be done later)
		//5th Select triggering and gating inputs
		//6th Enable triggering and gating
		//7th Select channels to be considered for scan
		//8th Map channel result to result register


	//Arbitration Configuration Register, Group x (GxARBCFG)
	//
	//ANONC (Analog Converter Control) Converter ON/OFF
	//Notes: Converter must be started before writing to SUCAL
	VADC_G0ARBCFG.U = 	(0x3 /*ANONC*/);
	VADC_G1ARBCFG.U = 	(0x3 /*ANONC*/);


	//Global Configuration Register (GLOBCFG)
	//
	//DIVA (Divider Factor for the Analog Internal Clock)
	//DIVWC (Write Control for Divider Parameters)
	//SUCAL (StartUp Calibration) Start calibration, status indicated by GxARBCFG.CAL
	VADC_GLOBCFG.U = (0x5     /*DIVA*/ )|
					 (0x1<<15 /*DIVWC*/);


	//Arbitration Priority Register, Group x (GxARBPR)
	//
	//PRIOx (Priority of Request Source x)
	//ASENy (Arbitration Slot y Enable) Group Queued source has slot 0
	VADC_G0ARBPR.U = (0x1 << 12 	/*PRIO1*/)| // auto scan priority 1
	 				 (0x1 << 25 	/*ASEN1*/); // auto scan arbitration slot enable

	//PRIOx (Priority of Request Source x)
	//ASENy (Arbitration Slot y Enable) Group Queued source has slot 0
	VADC_G1ARBPR.U = (0x1 << 12 	/*PRIO1*/)| // auto scan priority 1
	 	 	 	 	 (0x1 << 25 	/*ASEN1*/); // auto scan arbitration slot enable



	VADC_GLOBCFG.U |= (0x1<<31 /*SUCAL*/);	//Begin startup calibration

	while((VADC_G0ARBCFG.U & 0x30000000) != 0x20000000);  //Wait for startup calibration to complete
	while((VADC_G1ARBCFG.U & 0x30000000) != 0x20000000);  //Wait for startup calibration to complete

	CBTest_init_queue0_group0();
	CBTest_init_queue0_group1();

}

void CBTest_init_queue0_group0(void)
{

	//Autoscan Source Mode Register, Group x (GxASMR)
	//
	//ENGT (Enable Gate) Issue conversion request if pending bits are set
	//ENTR (Enable External Trigger) ON/OFF, Trigger selected by XTSEL
	VADC_G0ASMR.U = 	(0x1 << 0 /*ENGT*/)|
						(0x1 << 2 /*ENTR*/);
	//


	//Autoscan Source Channel Select Register, Group x (GxASSEL)
	//
	//CHSEL (Channel Selection) Set corresponding bits to include channels in scan sequence
	//Notes: Select channels 011 of Group 0 to be included in scan sequence
	VADC_G0ASSEL.U = (0x00000FFF /*CHSEL*/);
	//

	/* group 0 queue 0 */
	VADC_G0CHCTR0.U = 	(0x0 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR1.U = 	(0x1 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR2.U = 	(0x2 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR3.U = 	(0x3 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR4.U = 	(0x4 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR5.U = 	(0x5 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR6.U = 	(0x6 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR7.U = 	(0x7 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR8.U = 	(0x8 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR9.U = 	(0x9 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR10.U = 	(0xA << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR11.U = 	(0xB << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);
}


void CBTest_init_queue0_group1(void)
{

	//Autoscan Source Mode Register, Group x (GxASMR)
	//
	//ENGT (Enable Gate) Issue conversion request if pending bits are set
	//ENTR (Enable External Trigger) ON/OFF, Trigger selected by XTSEL
	VADC_G1ASMR.U = 	(0x1 << 0 /*ENGT*/)|
						(0x1 << 2 /*ENTR*/);
	//


	//Autoscan Source Channel Select Register, Group x (GxASSEL)
	//
	//CHSEL (Channel Selection) Set corresponding bits to include channels in scan sequence
	//Notes: Select channels 011 of Group 0 to be included in scan sequence
	VADC_G1ASSEL.U = (0x00000FFF /*CHSEL*/);
	//

	/* group 0 queue 0 */
	VADC_G1CHCTR0.U = 	(0x0 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR1.U = 	(0x1 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR2.U = 	(0x2 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR3.U = 	(0x3 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR4.U = 	(0x4 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR5.U = 	(0x5 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR6.U = 	(0x6 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR7.U = 	(0x7 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR8.U = 	(0x8 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR9.U = 	(0x9 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR10.U = 	(0xA << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR11.U = 	(0xB << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);
}

void CBTest_DigIO_Init(void)
{
    /* inputs */
	/* IA_IB_IC_OC_FLT */
	P33_OUT.B.P6 = 0;
	IfxPort_setPinMode(&MODULE_P33, 6, IfxPort_Mode_inputNoPullDevice);

	/* VDC_OC_FLT */
	P33_OUT.B.P7 = 0;
	IfxPort_setPinMode(&MODULE_P33, 7, IfxPort_Mode_inputNoPullDevice);

	/* FAULTS_L_P21_2 */
	P21_OUT.B.P2 = 0;
	IfxPort_setPinMode(&MODULE_P21, 2, IfxPort_Mode_inputNoPullDevice);
	GsCBTest_GPIOInput.FAULTS_L_P21_2 = IfxPort_getPinState(&MODULE_P21, 2);

	/* FAULTS_L_P33_8 */
	P33_OUT.B.P8 = 0;
	IfxPort_setPinMode(&MODULE_P33, 8, IfxPort_Mode_inputNoPullDevice);
	GsCBTest_GPIOInput.FAULTS_L_P33_8 = IfxPort_getPinState(&MODULE_P33, 8);

	/* VAB_CROSSING_P00_0 */
	P00_OUT.B.P0 = 0;
	IfxPort_setPinMode(&MODULE_P00, 0, IfxPort_Mode_inputNoPullDevice);
	GsCBTest_GPIOInput.VAB_CROSSING_P00_0 = IfxPort_getPinState(&MODULE_P00, 0);

    /* VAB_CROSSING_P00_1 */
	P00_OUT.B.P1 = 0;
	IfxPort_setPinMode(&MODULE_P00, 1, IfxPort_Mode_inputNoPullDevice);
	GsCBTest_GPIOInput.VAB_CROSSING_P00_1 = IfxPort_getPinState(&MODULE_P00, 1);

    /* CAN_RXD_P13_1 */
	P13_OUT.B.P1 = 0;
	IfxPort_setPinMode(&MODULE_P13, 1, IfxPort_Mode_inputNoPullDevice);
	GsCBTest_GPIOInput.CAN_RXD_P13_1 = IfxPort_getPinState(&MODULE_P13, 1);

    /* CAN_RXD_P14_1 */
	P14_OUT.B.P1 = 0;
	IfxPort_setPinMode(&MODULE_P14, 1, IfxPort_Mode_inputNoPullDevice);
	GsCBTest_GPIOInput.CAN_RXD_P14_1 = IfxPort_getPinState(&MODULE_P14, 1);

	/* CAN_TXD_P14_0 */
	P14_OUT.B.P1 = 0;
	IfxPort_setPinMode(&MODULE_P14, 0, IfxPort_Mode_inputNoPullDevice);
	GsCBTest_GPIOInput.CAN_TXD_P14_0 = IfxPort_getPinState(&MODULE_P14, 0);

    /* CAN_DISC_IN P15_6 */
	P15_OUT.B.P6 = 0;
	IfxPort_setPinMode(&MODULE_P15, 6, IfxPort_Mode_inputNoPullDevice);
	GsCBTest_GPIOInput.CAN_DISC_IN = IfxPort_getPinState(&MODULE_P15, 6);



	/* Outputs */
	P02_OUT.B.P0  = GsCBTest_GPIOOutput_val.UC_CMD_AH;
	IfxPort_setPinMode(&MODULE_P02, 0, IfxPort_Mode_outputPushPullGeneral);

	P02_OUT.B.P1  = GsCBTest_GPIOOutput_val.UC_CMD_AL;
	IfxPort_setPinMode(&MODULE_P02, 1, IfxPort_Mode_outputPushPullGeneral);

	P02_OUT.B.P2  = GsCBTest_GPIOOutput_val.UC_CMD_BH;
	IfxPort_setPinMode(&MODULE_P02, 2, IfxPort_Mode_outputPushPullGeneral);

	P02_OUT.B.P3  = GsCBTest_GPIOOutput_val.UC_CMD_BL;
	IfxPort_setPinMode(&MODULE_P02, 3, IfxPort_Mode_outputPushPullGeneral);

	P02_OUT.B.P4  = GsCBTest_GPIOOutput_val.UC_CMD_CH;
	IfxPort_setPinMode(&MODULE_P02, 4, IfxPort_Mode_outputPushPullGeneral);

	P02_OUT.B.P5  = GsCBTest_GPIOOutput_val.UC_CMD_CL;
	IfxPort_setPinMode(&MODULE_P02, 5, IfxPort_Mode_outputPushPullGeneral);

	P33_OUT.B.P9 = GsCBTest_GPIOOutput_val.FAULTS_RES;;
	IfxPort_setPinMode(&MODULE_P33, 9, IfxPort_Mode_outputPushPullGeneral);

	P13_OUT.B.P0  = GsCBTest_GPIOOutput_val.CAN_TXD_P13_0;
	IfxPort_setPinMode(&MODULE_P13, 0, IfxPort_Mode_outputPushPullGeneral);

	P15_OUT.B.P7  = GsCBTest_GPIOOutput_val.CAN_STB;
	IfxPort_setPinMode(&MODULE_P15, 7, IfxPort_Mode_outputPushPullGeneral);

	P11_OUT.B.P3  = GsCBTest_GPIOOutput_val.FLEXRAY_RXD;
	IfxPort_setPinMode(&MODULE_P11, 3, IfxPort_Mode_outputPushPullGeneral);

	P11_OUT.B.P6  = GsCBTest_GPIOOutput_val.FLEXRAY_TXD;
	IfxPort_setPinMode(&MODULE_P11, 6, IfxPort_Mode_outputPushPullGeneral);

	P11_OUT.B.P9  = GsCBTest_GPIOOutput_val.FLEXRAY_TXEN;
	IfxPort_setPinMode(&MODULE_P11, 9, IfxPort_Mode_outputPushPullGeneral);

	P15_OUT.B.P0  = GsCBTest_GPIOOutput_val.WDI;
	IfxPort_setPinMode(&MODULE_P15, 0, IfxPort_Mode_outputPushPullGeneral);

	P11_OUT.B.P2  = GsCBTest_GPIOOutput_val.RS422_TXD;
	IfxPort_setPinMode(&MODULE_P15, 2, IfxPort_Mode_outputPushPullGeneral);

	P11_OUT.B.P3  = GsCBTest_GPIOOutput_val.RS422_RXD;
	IfxPort_setPinMode(&MODULE_P15, 3, IfxPort_Mode_outputPushPullGeneral);

	P20_OUT.B.P14  = GsCBTest_GPIOOutput_val.SPI_1_DOUT;
	IfxPort_setPinMode(&MODULE_P20, 14, IfxPort_Mode_outputPushPullGeneral);

	P20_OUT.B.P12  = GsCBTest_GPIOOutput_val.SPI_1_DIN;
	IfxPort_setPinMode(&MODULE_P20, 12, IfxPort_Mode_outputPushPullGeneral);

	P20_OUT.B.P11  = GsCBTest_GPIOOutput_val.SPI_1_CLK;
	IfxPort_setPinMode(&MODULE_P20, 11, IfxPort_Mode_outputPushPullGeneral);

	P20_OUT.B.P13  = GsCBTest_GPIOOutput_val.SPI_1_CS_L;
	IfxPort_setPinMode(&MODULE_P20, 13, IfxPort_Mode_outputPushPullGeneral);

	P22_OUT.B.P0  = GsCBTest_GPIOOutput_val.SPI_2_DOUT;
	IfxPort_setPinMode(&MODULE_P22, 0, IfxPort_Mode_outputPushPullGeneral);

	P22_OUT.B.P1  = GsCBTest_GPIOOutput_val.SPI_2_DIN;
	IfxPort_setPinMode(&MODULE_P22, 1, IfxPort_Mode_outputPushPullGeneral);

	P22_OUT.B.P3  = GsCBTest_GPIOOutput_val.SPI_2_CLK;
	IfxPort_setPinMode(&MODULE_P22, 3, IfxPort_Mode_outputPushPullGeneral);

	P23_OUT.B.P1  = GsCBTest_GPIOOutput_val.SPI_2_CS_L;
	IfxPort_setPinMode(&MODULE_P23, 1, IfxPort_Mode_outputPushPullGeneral);

	P33_OUT.B.P0  = GsCBTest_GPIOOutput_val.GPIO_1;
	IfxPort_setPinMode(&MODULE_P33, 0, IfxPort_Mode_outputPushPullGeneral);

	P33_OUT.B.P1  = GsCBTest_GPIOOutput_val.GPIO_2;
	IfxPort_setPinMode(&MODULE_P33, 1, IfxPort_Mode_outputPushPullGeneral);

	P33_OUT.B.P2  = GsCBTest_GPIOOutput_val.GPIO_3;
	IfxPort_setPinMode(&MODULE_P33, 2, IfxPort_Mode_outputPushPullGeneral);

	P33_OUT.B.P3  = GsCBTest_GPIOOutput_val.GPIO_4;
	IfxPort_setPinMode(&MODULE_P33, 3, IfxPort_Mode_outputPushPullGeneral);
}

void GetGPIOStatus(void)
{
	GsCBTest_GPIOInput.IA_IB_IC_OC_FLT 			= IfxPort_getPinState(&MODULE_P33, 6);
	GsCBTest_GPIOInput.VDC_OV_FLT 				= IfxPort_getPinState(&MODULE_P33, 7);
	GsCBTest_GPIOInput.FAULTS_L_P21_2 			= IfxPort_getPinState(&MODULE_P21, 2);
	GsCBTest_GPIOInput.FAULTS_L_P33_8 			= IfxPort_getPinState(&MODULE_P33, 8);
	GsCBTest_GPIOInput.VAB_CROSSING_P00_0 		= IfxPort_getPinState(&MODULE_P00, 0);
	GsCBTest_GPIOInput.VAB_CROSSING_P00_1 		= IfxPort_getPinState(&MODULE_P00, 1);
	GsCBTest_GPIOInput.CAN_RXD_P13_1 			= IfxPort_getPinState(&MODULE_P13, 1);
	GsCBTest_GPIOInput.CAN_RXD_P14_1 			= IfxPort_getPinState(&MODULE_P14, 1);
	GsCBTest_GPIOInput.CAN_TXD_P14_0 			= IfxPort_getPinState(&MODULE_P14, 0);
	GsCBTest_GPIOInput.CAN_DISC_IN 				= IfxPort_getPinState(&MODULE_P15, 6);

	GsCBTest_GPIOOutput_state.UC_CMD_AH 		= IfxPort_getPinState(&MODULE_P02, 0);
	GsCBTest_GPIOOutput_state.UC_CMD_AL 		= IfxPort_getPinState(&MODULE_P02, 1);
	GsCBTest_GPIOOutput_state.UC_CMD_BH 		= IfxPort_getPinState(&MODULE_P02, 2);
	GsCBTest_GPIOOutput_state.UC_CMD_BL 		= IfxPort_getPinState(&MODULE_P02, 3);
	GsCBTest_GPIOOutput_state.UC_CMD_CH 		= IfxPort_getPinState(&MODULE_P02, 4);
	GsCBTest_GPIOOutput_state.UC_CMD_CL 		= IfxPort_getPinState(&MODULE_P02, 5);
	GsCBTest_GPIOOutput_state.FAULTS_RES 		= IfxPort_getPinState(&MODULE_P33, 9);
	GsCBTest_GPIOOutput_state.CAN_TXD_P13_0		= IfxPort_getPinState(&MODULE_P13, 0);
	GsCBTest_GPIOOutput_state.CAN_STB 			= IfxPort_getPinState(&MODULE_P15, 7);
	GsCBTest_GPIOOutput_state.FLEXRAY_RXD 		= IfxPort_getPinState(&MODULE_P11, 3);
	GsCBTest_GPIOOutput_state.FLEXRAY_TXD 		= IfxPort_getPinState(&MODULE_P11, 6);
	GsCBTest_GPIOOutput_state.FLEXRAY_TXEN 		= IfxPort_getPinState(&MODULE_P11, 9);
	GsCBTest_GPIOOutput_state.WDI 				= IfxPort_getPinState(&MODULE_P15, 0);
	GsCBTest_GPIOOutput_state.RS422_TXD 		= IfxPort_getPinState(&MODULE_P15, 2);
	GsCBTest_GPIOOutput_state.RS422_RXD 		= IfxPort_getPinState(&MODULE_P15, 3);
	GsCBTest_GPIOOutput_state.SPI_1_DOUT 		= IfxPort_getPinState(&MODULE_P20, 14);
	GsCBTest_GPIOOutput_state.SPI_1_DIN 		= IfxPort_getPinState(&MODULE_P20, 12);
	GsCBTest_GPIOOutput_state.SPI_1_CLK 		= IfxPort_getPinState(&MODULE_P20, 11);
	GsCBTest_GPIOOutput_state.SPI_1_CS_L 		= IfxPort_getPinState(&MODULE_P20, 13);
	GsCBTest_GPIOOutput_state.SPI_2_DOUT 		= IfxPort_getPinState(&MODULE_P22, 0);
	GsCBTest_GPIOOutput_state.SPI_2_DIN 		= IfxPort_getPinState(&MODULE_P22, 1);
	GsCBTest_GPIOOutput_state.SPI_2_CLK 		= IfxPort_getPinState(&MODULE_P22, 3);
	GsCBTest_GPIOOutput_state.SPI_2_CS_L 		= IfxPort_getPinState(&MODULE_P23, 1);
	GsCBTest_GPIOOutput_state.GPIO_1 			= IfxPort_getPinState(&MODULE_P33, 0);
	GsCBTest_GPIOOutput_state.GPIO_2 			= IfxPort_getPinState(&MODULE_P33, 1);
	GsCBTest_GPIOOutput_state.GPIO_3 			= IfxPort_getPinState(&MODULE_P33, 2);
	GsCBTest_GPIOOutput_state.GPIO_4	 		= IfxPort_getPinState(&MODULE_P33, 3);
}

void SetOutputs(void)
{
	/* Outputs */
	IfxPort_setPinState(&MODULE_P02, 0, GsCBTest_GPIOOutput_val.UC_CMD_AH);
	IfxPort_setPinState(&MODULE_P02, 1, GsCBTest_GPIOOutput_val.UC_CMD_AL);
	IfxPort_setPinState(&MODULE_P02, 2, GsCBTest_GPIOOutput_val.UC_CMD_BH);
	IfxPort_setPinState(&MODULE_P02, 3, GsCBTest_GPIOOutput_val.UC_CMD_BL);
	IfxPort_setPinState(&MODULE_P02, 4, GsCBTest_GPIOOutput_val.UC_CMD_CH);
	IfxPort_setPinState(&MODULE_P02, 5, GsCBTest_GPIOOutput_val.UC_CMD_CL);
	IfxPort_setPinState(&MODULE_P33, 9, GsCBTest_GPIOOutput_val.FAULTS_RES);
	IfxPort_setPinState(&MODULE_P13, 0, GsCBTest_GPIOOutput_val.CAN_TXD_P13_0);
	IfxPort_setPinState(&MODULE_P15, 7, GsCBTest_GPIOOutput_val.CAN_STB);
	IfxPort_setPinState(&MODULE_P11, 3, GsCBTest_GPIOOutput_val.FLEXRAY_RXD);
	IfxPort_setPinState(&MODULE_P11, 6, GsCBTest_GPIOOutput_val.FLEXRAY_TXD);
	IfxPort_setPinState(&MODULE_P11, 9, GsCBTest_GPIOOutput_val.FLEXRAY_TXEN);
	IfxPort_setPinState(&MODULE_P15, 0, GsCBTest_GPIOOutput_val.WDI);
	IfxPort_setPinState(&MODULE_P15, 2,  GsCBTest_GPIOOutput_val.RS422_TXD);
	IfxPort_setPinState(&MODULE_P15, 3,  GsCBTest_GPIOOutput_val.RS422_RXD);
	IfxPort_setPinState(&MODULE_P20, 14, GsCBTest_GPIOOutput_val.SPI_1_DOUT);
	IfxPort_setPinState(&MODULE_P20, 12, GsCBTest_GPIOOutput_val.SPI_1_DIN);
	IfxPort_setPinState(&MODULE_P20, 11, GsCBTest_GPIOOutput_val.SPI_1_CLK);
	IfxPort_setPinState(&MODULE_P20, 13, GsCBTest_GPIOOutput_val.SPI_1_CS_L);
	IfxPort_setPinState(&MODULE_P22, 0, GsCBTest_GPIOOutput_val.SPI_2_DOUT);
	IfxPort_setPinState(&MODULE_P22, 1, GsCBTest_GPIOOutput_val.SPI_2_DIN);
	IfxPort_setPinState(&MODULE_P22, 3, GsCBTest_GPIOOutput_val.SPI_2_CLK);
	IfxPort_setPinState(&MODULE_P23, 1, GsCBTest_GPIOOutput_val.SPI_2_CS_L);
	IfxPort_setPinState(&MODULE_P33, 0, GsCBTest_GPIOOutput_val.GPIO_1);
	IfxPort_setPinState(&MODULE_P33, 1, GsCBTest_GPIOOutput_val.GPIO_2);
	IfxPort_setPinState(&MODULE_P33, 2, GsCBTest_GPIOOutput_val.GPIO_3);
	IfxPort_setPinState(&MODULE_P33, 3, GsCBTest_GPIOOutput_val.GPIO_4);
}

void GetCBT_ADCValues(void)
{
	GsCBT_ADC_Hex.ADC_5V_AN1_8 			= VADC_G1RES8.B.RESULT;
	GsCBT_ADC_Hex.ADC_12V_AN0_8 		= VADC_G0RES8.B.RESULT;
	GsCBT_ADC_Hex.ADC_IA_1_AN0_0 		= VADC_G0RES0.B.RESULT;
	GsCBT_ADC_Hex.ADC_IA_2_AN0_2 		= VADC_G0RES2.B.RESULT;
	GsCBT_ADC_Hex.ADC_IB_1_AN1_0 		= VADC_G1RES0.B.RESULT;
	GsCBT_ADC_Hex.ADC_IB_2_AN1_2		= VADC_G1RES2.B.RESULT;
	GsCBT_ADC_Hex.ADC_IC_AN0_1			= VADC_G0RES1.B.RESULT;
	GsCBT_ADC_Hex.ADC_VDC_AN1_1			= VADC_G1RES1.B.RESULT;
	GsCBT_ADC_Hex.ADC_VA_AN0_3			= VADC_G0RES3.B.RESULT;
	GsCBT_ADC_Hex.ADC_VB_AN1_3			= VADC_G1RES3.B.RESULT;
	GsCBT_ADC_Hex.ADC_VC_AN0_4			= VADC_G0RES4.B.RESULT;
	GsCBT_ADC_Hex.ADC_T_IMS_BOARD_AN0_5 = VADC_G0RES5.B.RESULT;
	GsCBT_ADC_Hex.ADC_T_BOARD_1_AN1_4	= VADC_G1RES4.B.RESULT;
	GsCBT_ADC_Hex.ADC_T_BOARD_2_AN1_5	= VADC_G1RES5.B.RESULT;
	GsCBT_ADC_Hex.ADC_T_SENSOR_1_AN0_6	= VADC_G0RES6.B.RESULT;
	GsCBT_ADC_Hex.ADC_T_SENSOR_2_AN0_7 	= VADC_G0RES7.B.RESULT;
	GsCBT_ADC_Hex.ADC_T_SENSOR_3_AN1_6	= VADC_G1RES6.B.RESULT;
	GsCBT_ADC_Hex.ADC_EXT_AN1_7			= VADC_G1RES7.B.RESULT;
	GsCBT_ADC_Hex.ADC_E34_AN0_9			= VADC_G0RES9.B.RESULT;
	GsCBT_ADC_Hex.ADC_E35_AN1_9			= VADC_G1RES9.B.RESULT;
	GsCBT_ADC_Hex.ADC_P40_10_AN0_10		= VADC_G0RES10.B.RESULT;
	GsCBT_ADC_Hex.ADC_P40_11_AN0_11		= VADC_G0RES11.B.RESULT;
	GsCBT_ADC_Hex.ADC_P41_10_AN1_10		= VADC_G1RES10.B.RESULT;
	GsCBT_ADC_Hex.ADC_P41_11_AN1_11     = VADC_G1RES11.B.RESULT;

	GsCBT_ADC_Float.ADC_5V_AN1_8 				= VADC_G1RES8.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_12V_AN0_8 				= VADC_G0RES8.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_IA_1_AN0_0 				= VADC_G0RES0.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_IA_2_AN0_2 				= VADC_G0RES2.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_IB_1_AN1_0 				= VADC_G1RES0.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_IB_2_AN1_2				= VADC_G1RES2.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_IC_AN0_1				= VADC_G0RES1.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_VDC_AN1_1				= VADC_G1RES1.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_VA_AN0_3				= VADC_G0RES3.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_VB_AN1_3				= VADC_G1RES3.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_VC_AN0_4				= VADC_G0RES4.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_T_IMS_BOARD_AN0_5 		= VADC_G0RES5.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_T_BOARD_1_AN1_4			= VADC_G1RES4.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_T_BOARD_2_AN1_5			= VADC_G1RES5.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_T_SENSOR_1_AN0_6		= VADC_G0RES6.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_T_SENSOR_2_AN0_7 		= VADC_G0RES7.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_T_SENSOR_3_AN1_6		= VADC_G1RES6.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_EXT_AN1_7				= VADC_G1RES7.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_E34_AN0_9				= VADC_G0RES9.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_E35_AN1_9				= VADC_G1RES9.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_P40_10_AN0_10			= VADC_G0RES10.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_P40_11_AN0_11			= VADC_G0RES11.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_P41_10_AN1_10			= VADC_G1RES10.B.RESULT * Gf_CBTTestADCscale;
	GsCBT_ADC_Float.ADC_P41_11_AN1_11       	= VADC_G1RES11.B.RESULT * Gf_CBTTestADCscale;
}
#endif
