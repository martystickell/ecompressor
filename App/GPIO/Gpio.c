#include "Gpio.h"

volatile boolean FAULT_Reset_stat;

__near volatile boolean GbGIO_IabcOCHW	 		= FALSE;
__near volatile boolean GbGIO_VdcOVHW 			= FALSE;

void GIO_Init(void)
{
    P33_OUT.B.P0  = 0;
    IfxPort_setPinMode(&MODULE_P33, 0, IfxPort_Mode_outputPushPullGeneral);

    P33_OUT.B.P5  = 1;
    IfxPort_setPinMode(&MODULE_P33, 1, IfxPort_Mode_outputPushPullGeneral);

    P33_OUT.B.P8  = 1;
    IfxPort_setPinMode(&MODULE_P33, 8, IfxPort_Mode_outputPushPullGeneral);

    P33_OUT.B.P6 = 0;
    IfxPort_setPinMode(&MODULE_P33, 6, IfxPort_Mode_inputNoPullDevice);
    GbGIO_IabcOCHW = IfxPort_getPinState(&MODULE_P33, 6);

    P33_OUT.B.P3 = 0;
    IfxPort_setPinMode(&MODULE_P33, 3, IfxPort_Mode_inputPullUp);

    P33_OUT.B.P7 = 0;
    IfxPort_setPinMode(&MODULE_P33, 7, IfxPort_Mode_inputNoPullDevice);
    GbGIO_VdcOVHW = IfxPort_getPinState(&MODULE_P33, 7);

    P33_OUT.B.P9 = 0;
    IfxPort_setPinMode(&MODULE_P33, 9, IfxPort_Mode_outputPushPullGeneral);

    /* set pin 14.0 and 14.1 as inputs to prevent CAN driver conflict */
	IfxPort_setPinMode(&MODULE_P14, 0, IfxPort_Mode_inputPullDown);
	IfxPort_setPinMode(&MODULE_P14, 1, IfxPort_Mode_inputPullDown);

}

void InitializeAllGPIO(void)
{
	IfxPort_setGroupModeInput(&MODULE_P00, 0, MODULE_P00_MASK, IfxPort_Mode_inputPullDown);
	IfxPort_setGroupModeInput(&MODULE_P02, 0, MODULE_P02_MASK, IfxPort_Mode_inputPullDown);
	IfxPort_setGroupModeInput(&MODULE_P10, 0, MODULE_P10_MASK, IfxPort_Mode_inputPullDown);
	IfxPort_setGroupModeInput(&MODULE_P11, 0, MODULE_P11_MASK, IfxPort_Mode_inputPullDown);
	IfxPort_setGroupModeInput(&MODULE_P13, 0, MODULE_P13_MASK, IfxPort_Mode_inputPullDown);
	IfxPort_setGroupModeInput(&MODULE_P14, 0, MODULE_P14_MASK, IfxPort_Mode_inputPullDown);
	IfxPort_setGroupModeInput(&MODULE_P15, 0, MODULE_P15_MASK, IfxPort_Mode_inputPullDown);
	IfxPort_setGroupModeInput(&MODULE_P20, 0, MODULE_P20_MASK, IfxPort_Mode_inputPullDown);
	IfxPort_setGroupModeInput(&MODULE_P21, 0, MODULE_P21_MASK, IfxPort_Mode_inputPullDown);
	IfxPort_setGroupModeInput(&MODULE_P22, 0, MODULE_P22_MASK, IfxPort_Mode_inputPullDown);
	IfxPort_setGroupModeInput(&MODULE_P23, 0, MODULE_P23_MASK, IfxPort_Mode_inputPullDown);
	IfxPort_setGroupModeInput(&MODULE_P33, 0, MODULE_P33_MASK, IfxPort_Mode_inputPullDown);
	IfxPort_setGroupModeInput(&MODULE_P34, 0, MODULE_P34_MASK, IfxPort_Mode_inputPullDown);
	IfxPort_setGroupModeInput(&MODULE_P40, 0, MODULE_P40_MASK, IfxPort_Mode_inputPullDown);
	IfxPort_setGroupModeInput(&MODULE_P41, 0, MODULE_P41_MASK, IfxPort_Mode_inputPullDown);
}

void GIO_EnblResetHWFaults(void)
{
	IfxPort_setPinState(&MODULE_P33, 9, IfxPort_State_high);
}

void GIO_DisbResetHWFaults(void)
{
	IfxPort_setPinState(&MODULE_P33, 9, IfxPort_State_low);
}


// Hardware Over-Current Failure Input
boolean GIO_GetOCHWFlt(void)
{
	return IfxPort_getPinState(&MODULE_P33, 6);
}

// Hardware Over-Voltage Failure Input
boolean GIO_GetOVHWFlt(void)
{
	return IfxPort_getPinState(&MODULE_P33, 7);
}

// Rotation Direction Logic 1-Normal (A->B->C) Logic 0-Reverse(A->C->B)
boolean GIO_GetMotRotDir(void)
{
	return IfxPort_getPinState(&MODULE_P33, 3);
}
