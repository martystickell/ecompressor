#ifndef GPIO_H
#define GPIO_H 1

#include "IfxPort_reg.h"
#include "IfxPort.h"


#define MODULE_P00_MASK		0xFFFF 	/* 4 IOCRs are used 	*/
#define MODULE_P02_MASK		0xFFF	/* 3 IOCRs are used 	*/
#define MODULE_P10_MASK		0xFF	/* 2 IOCRs are used 	*/
#define MODULE_P11_MASK		0xFFFF	/* 4 IOCRs are used 	*/
#define MODULE_P13_MASK		0xF		/* 1 IOCR is used 		*/
#define MODULE_P14_MASK		0xFFF	/* 3 IOCRs are used 	*/
#define MODULE_P15_MASK		0xFFF	/* 3 IOCRs are used 	*/
#define MODULE_P20_MASK		0xFFFF	/* 4 IOCRs are used 	*/
#define MODULE_P21_MASK		0xFF	/* 2 IOCRs are used 	*/
#define MODULE_P22_MASK		0xFF	/* 2 IOCRs are used 	*/
#define MODULE_P23_MASK		0xF		/* 1 IOCR is used 		*/
#define MODULE_P33_MASK		0xFFFF	/* 4 IOCRs are used		*/
#define MODULE_P34_MASK		0xF		/* 1 IOCR is used 		*/
#define MODULE_P40_MASK		0xFFF	/* 3 IOCRs are used 	*/
#define MODULE_P41_MASK		0xFFF	/* 3 IOCRs are used 	*/

extern volatile boolean GbGIO_IabcOCHW;
extern volatile boolean GbGIO_VdcOVHW;
extern volatile boolean FAULT_Reset_stat;


extern void GIO_Init(void);
extern void GIO_EnblResetHWFaults(void);
extern void GIO_DisbResetHWFaults(void);

extern boolean GIO_GetOCHWFlt(void);
extern boolean GIO_GetOVHWFlt(void);
extern boolean GIO_GetMotRotDir(void);

extern void InitializeAllGPIO(void);

#endif
