
#include 	"Gtm.h"
#include 	"STM.h"

//#pragma section code "ISR_RAMCODE"

IFX_INTERRUPT(GTM_Update_T1_ISR, 0, GTM_T1_ISR_PRIO);
void GTM_Update_T1_ISR(void)
{
	__enable();

	GeSTM_LastStateT1 = GeSTM_CurrentStateT1;
	GeSTM_CurrentStateT1 = GeSTM_NextStateT1;

	switch(GeSTM_CurrentStateT1)
	{
		case Off:
			STM_OffT1();
			break;

		case TurnOn:
			STM_TurnOnT1();
			break;

		case StandBy:
			STM_StandByT1();
			break;

		case Run:
			STM_RunT1();
			break;

		case TurnOff:
			STM_TurnOffT1();
			break;

		case Fault:
			STM_FaultT1();
			break;
	}

	VX1000If_Event(1);
}
