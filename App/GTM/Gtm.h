#ifndef GTM_H
#define GTM_H 1

#include "Ifx_Types.h"
#include "CompilerTasking.h"
#include "IfxGtm_reg.h"
#include "IfxSrc_reg.h"
#include "IfxGtm_Trig.h"
#include "IfxVadc_reg.h"
#include "math.h"
#include "IfxStm_reg.h"
#include "IfxStm.h"
#include "IfxCbs_regdef.h"
#include "IfxCbs_reg.h"
#include <VX1000If.h>

typedef struct
{
	float dutyCycleA;
	float dutyCycleB;
	float dutyCycleC;
}dutyCycle_t;

#define GTM_T0_ISR_PRIO  	254
#define GTM_T1_ISR_PRIO   	253
#define GTM_T2_ISR_PRIO   	252

#define GTM_ISR0_INDEX 			0
#define GTM_ISR1_INDEX 			1
#define GTM_ISR2_INDEX 			2

#define	SEC_TO_T0CNT			1.0e8
#define SEC_TO_T1CNT			6.25e6
#define SEC_TO_T2CNT			6.25e6

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/
extern float KfGTM_dTadc;
extern float KfGTM_dTpwm;

// T0 period (s)
extern float KfGTM_T0P0;
extern float KfGTM_T0P1;

// T0 period speed break point (r/min)
extern float KfGTM_T0WrpmP0;
extern float KfGTM_T0WrpmP1;

// T0 period change slew rate limit (s/1 period)
extern float KfGTM_T0SlewLim;

extern float KfGTM_T1;
extern float KfGTM_T2;

extern float KfGTM_Tdead;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
extern uint16 Gu16GTM_T0CNT;
extern uint16 Gu16GTM_T0CNTCmd;

extern uint16 Gu16GTM_T1CNT;
extern uint16 Gu16GTM_T2CNT;

extern uint16 Gu16GTM_dTadcCNT;
extern uint16 Gu16GTM_dTpwmCNT;

extern dutyCycle_t GsGTM_OutputPWM;

extern volatile boolean GbGTM_PWMEnbl;

extern uint16 Gu16GTM_TdeadCNT;

extern float GfGTM_T0;
extern float GfGTM_T0New;
extern float GfGTM_T0Cmd;

extern float GfGTM_T1;
extern float GfGTM_T2;


/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
extern void Gtm_Global_Init(void);
extern void Gtm_TOM_Init(void);
extern void Gtm_Enable(void);
extern void Gtm_PinConfig(void);
extern void dsp_algo(void);

extern void GTM_ConfigureT0Interrupt(void);
extern void GTM_ConfigureT1Interrupt(void);
extern void GTM_ConfigureT2Interrupt(void);

extern void GTM_UpdatePWMOutputs(dutyCycle_t dutyCycle, uint16 period);

extern void GTM_T0Calc(void);
extern void GTM_InitT0T1T2(void);
extern void GTM_UpdatePWMPeriod(uint16 Lu16GTM_T0CNT, uint16 Lu16GTM_dTpwmCNT, uint16 Lu16GTM_dTadcCNT);

extern uint32_t STM_GetSysClk(void);

extern void GTM_PWMEnable(void);
extern void GTM_PWMDisable(void);

#endif
