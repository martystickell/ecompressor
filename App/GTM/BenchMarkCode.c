/*
 * BenchMarkCode.c
 *
 *  Created on: Oct 10, 2018
 *      Author: H297270
 */
double atan2(double,double), sqrt(double), cos(double), sin(double), sqrt(double);

volatile float

// input variables with initializations.  Note: All input and output variables double precision type float32
AA = 2,
AB = 3,
AC = 5,
AD = 1,
AE = 7,
AF = 8,
AG = 0.7,
AH = 17,
AI = 5.2,
AJ = 12,
AK = 15,
AL = 16,
AM = 2,
AN = 8,
AO = 11,
AP = 10,
AQ = 9,
BA = 4,
BB = 2,
BC = 6,
BD = 9,
BE = 18,
BF = 1,
BG = 9,
BH = 13,
BI = 14,
BJ = 10,
CA = 0.1,
DA = 0.5,
FA = 200,
FB = 100,
GA = -2.094395,	// -120 deg in rad/sec
IA = 3,
IB = 4,
JA = 2,
JB = 3,
KA = 0.523599,	// +30 deg in rad/sec

// output variables (with the correct computed values in the comments)
A,	// 464
B,	// -51.5
C,	// 4
D,	// 2
E,	// 1.10715
F,	// 173.205
G,	// -0.5
H,	// -0.866
I,	// 5
J,	// 0.6667
K,	// 0.5
L	// 0.8660
;





void BenchMarkCode(void)
{
# if 0
	now_stm_start = STM0_TIM0.U;
	//for(loop_counter = 1; loop_counter <= 100; loop_counter++)
	{
		A = AA + AB * (-AC * AD * AE + AF * AG + AH * AI + AJ * AK + AL * (AM - AN) + AO * (AP - AQ));
		B = BA + AB * (-BC * AA - AC * BD * AG - BE * BF + BG * BH + BI * (AM - AN) + BJ * (AP - AQ));
		C = AI + BB * (CA * (AM - AN));
		D = BF + BB * (DA * (AP - AQ));
		E = atan2(C,D);
		F = sqrt(FA * FA - FB * FB);
		G = cos(GA);
		H = sin(GA);
		I = sqrt(IA * IA + IB * IB);
		J = JA / JB;
		K = sin(KA);
		L = cos(KA);

		AA=AA+loop_counter*0.002;
		AB=AB-loop_counter*0.0005;
		AC=AC+loop_counter*0.005;
		AD=AD-loop_counter*0.001;
		AE=AE+loop_counter*0.007;
		AF=AF-loop_counter*0.008;
		AG=AG+loop_counter*0.0007;
		AH=AH+loop_counter*0.015;
		AI=AI+loop_counter*0.0052;
		AJ=AJ-loop_counter*0.012;
		AK=AK+loop_counter*0.015;
		AL=AL+loop_counter*0.016;
		AM=AM-loop_counter*0.002;
		AN=AN-loop_counter*0.008;
		AO=AO-loop_counter*0.011;
		AP=AP+loop_counter*0.01;
		AQ=AQ-loop_counter*0.009;
		BA=BA+loop_counter;
		BB=BB+loop_counter*0.002;
		BC=BC-loop_counter*0.006;
		BD=BD-loop_counter*0.009;
		BE=BE+loop_counter*0.018;
		BF=BF-loop_counter*0.001;
		BG=BG+loop_counter*0.009;
		BH=BH+loop_counter*0.013;
		BI=BI-loop_counter*0.014;
		BJ=BJ-loop_counter*0.04;
		CA=CA-loop_counter*0.0001;
		DA=DA-loop_counter*0.0001;
		FA=FA+loop_counter*1.1;
		FB=FB+loop_counter*1.12;
		GA=GA-loop_counter*0.0005;
		IA=IA+loop_counter*0.003;
		IB=IB-loop_counter*0.04;
		JA=JA+loop_counter*0.44;
		JB=JB+loop_counter*0.3;
		KA=KA+loop_counter*0.001;
	}

	// Record the end time here to measure the execution time for 100 iterations
	now_stm_end = STM0_TIM0.U;
	stm_diff = now_stm_end - now_stm_start;



	(void)an2_val_1;

#endif

# if 0

	now_stm_start = STM0_TIM0.U;
	for(loop_counter = 1; loop_counter <= 100; loop_counter++)
	{
#if 0
		A = AA + AB * (-AC * AD * AE + AF * AG + AH * AI + AJ * AK + AL * (AM - AN) + AO * (AP - AQ));
		B = BA + AB * (-BC * AA - AC * BD * AG - BE * BF + BG * BH + BI * (AM - AN) + BJ * (AP - AQ));
		C = AI + BB * (CA * (AM - AN));
		D = BF + BB * (DA * (AP - AQ));
		E = Ifx_LutAtan2F32_float32(C,D);
		F = sqrtf(FA * FA - FB * FB);
		//F = FA * FA - FB * FB;
		G = Ifx_LutLSincosF32_cos(GA);
		H = Ifx_LutLSincosF32_sin(GA);
		I = sqrtf(IA * IA + IB * IB);
		//I = IA * IA + IB * IB;
		J = JA / JB;
		K = Ifx_LutLSincosF32_sin(KA);
		L = Ifx_LutLSincosF32_cos(KA);
#endif

		A = AA + AB * (-AC * AD * AE + AF * AG + AH * AI + AJ * AK + AL * (AM - AN) + AO * (AP - AQ));
		B = BA + AB * (-BC * AA - AC * BD * AG - BE * BF + BG * BH + BI * (AM - AN) + BJ * (AP - AQ));
		C = AI + BB * (CA * (AM - AN));
		D = BF + BB * (DA * (AP - AQ));
		E = atan2(C,D);
		//F = sqrtf(FA * FA - FB * FB);
		F = FA * FA - FB * FB;
		G = cos(GA);
		H = sin(GA);
		//I = sqrtf(IA * IA + IB * IB);
		I = IA * IA + IB * IB;
		J = JA / JB;
		K = sin(KA);
		L = cos(KA);

		AA=AA+loop_counter*0.002;
		AB=AB-loop_counter*0.0005;
		AC=AC+loop_counter*0.005;
		AD=AD-loop_counter*0.001;
		AE=AE+loop_counter*0.007;
		AF=AF-loop_counter*0.008;
		AG=AG+loop_counter*0.0007;
		AH=AH+loop_counter*0.015;
		AI=AI+loop_counter*0.0052;
		AJ=AJ-loop_counter*0.012;
		AK=AK+loop_counter*0.015;
		AL=AL+loop_counter*0.016;
		AM=AM-loop_counter*0.002;
		AN=AN-loop_counter*0.008;
		AO=AO-loop_counter*0.011;
		AP=AP+loop_counter*0.01;
		AQ=AQ-loop_counter*0.009;
		BA=BA+loop_counter;
		BB=BB+loop_counter*0.002;
		BC=BC-loop_counter*0.006;
		BD=BD-loop_counter*0.009;
		BE=BE+loop_counter*0.018;
		BF=BF-loop_counter*0.001;
		BG=BG+loop_counter*0.009;
		BH=BH+loop_counter*0.013;
		BI=BI-loop_counter*0.014;
		BJ=BJ-loop_counter*0.04;
		CA=CA-loop_counter*0.0001;
		DA=DA-loop_counter*0.0001;
		FA=FA+loop_counter*1.1;
		FB=FB+loop_counter*1.12;
		GA=GA-loop_counter*0.0005;
		IA=IA+loop_counter*0.003;
		IB=IB-loop_counter*0.04;
		JA=JA+loop_counter*0.44;
		JB=JB+loop_counter*0.3;
		KA=KA+loop_counter*0.001;
	}

	// Record the end time here to measure the execution time for 100 iterations
	now_stm_end = STM0_TIM0.U;

	if(ctr < 10)
	{
		stm_diff[ctr] = now_stm_end - now_stm_start;
	}

#endif

}
