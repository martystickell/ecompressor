 ;******************************************************************************
 ; file: Ifx_sqrtF32_fast.asm
 ;
 ; Copyright (c) 2013 Infineon Technologies AG. All rights reserved.
 ;
 ; Compiler: TASKING
 ;
 ; $Revision: 1.1 $
 ;
 ; $Date: 2015/08/01 09:22:56 $
 ;
 ; @Description:
 ;
 ;*****************************************************************************
 ;                                 IMPORTANT NOTICE
 ;
 ; Infineon Technologies AG (Infineon) is supplying this file for use
 ; exclusively with Infineon's microcontroller products. This file can be freely
 ; distributed within development tools that are supporting such microcontroller
 ; products.
 ;
 ; THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER sqrtRESS, IMPLIED
 ; OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 ; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 ; INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 ; OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 ;
 ;*****************************************************************************

;------------------- Section for External Reference ---------------------------

    .global     Ifx_sqrtF32_fast

;------------------- Section for Code Segment Declaration ---------------------

;    .sdecl      ".text.dsplib",CODE
;    .sect       ".text.dsplib"
    .sdecl      ".text.cpu0_psram",CODE
    .sect       ".text.cpu0_psram"
;=====================Executable code==========================================

	.define	a		"d4"
	.define x		"d5"
	.define x2		"d6"
	.define a_2		"d11"
	.define lc		"a2"
	.define c1_5	"d10"
	.define	t		"d7"
	.define result	"d2"

    .align 8
Ifx_sqrtF32_fast:
	MOVH	a_2,#48896 ; -0.5
	QSEED.F	x,a
	MUL.F	a_2,a_2,a
	MUL.F	x2,x,x	; x*x
	MOVH	c1_5,#16320	; 1.5
	MADD.F	t,c1_5,a_2,x2	; 1.5 - 0.5 a * x*x
	;#
	;#
	MUL.F	x,x,t
	;#
	MUL.F	x2,x,x	; x*x
	;#
	MADD.F	t,c1_5,a_2,x2	; 1.5 - 0.5 a * x*x
	;#
	;#
	MUL.F	x,x,t
	;#
	MUL.F	result,a,x
	RET
