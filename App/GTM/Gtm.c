
#include "Gtm.h"
#include "vadc.h"
#include "CCC.h"

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/
// T0 period (s)
float KfGTM_T0P0 = 33.0e-6;
float KfGTM_T0P1 = 33.0e-6;

// T0 period speed break point (r/min)
float KfGTM_T0WrpmP0 = 30000.0;
float KfGTM_T0WrpmP1 = 100000.0;

// T0 period change slew rate limit (s/T1 period)
float KfGTM_T0SlewLim = 5e-6;

// T1 period (s)
float KfGTM_T1 = 1e-3;

// T2 period (s)
float KfGTM_T2 = 10e-3;

float KfGTM_dTadc = 4.5e-6;
float KfGTM_dTpwm = 0e-6;

float KfGTM_Tdead = 1.0e-6;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
uint16 Gu16GTM_T0CNT = 10000;
uint16 Gu16GTM_T0CNTCmd = 10000;

uint16 Gu16GTM_T1CNT = 6250;
uint16 Gu16GTM_T2CNT = 62500;

uint16 Gu16GTM_dTadcCNT = 1000;
uint16 Gu16GTM_dTpwmCNT = 500;

uint16 Gu16GTM_TdeadCNT = 100;

// T0 variable
float GfGTM_T0 = 100e-6;
float GfGTM_T0New = 100e-6;
float GfGTM_T0Cmd = 100e-6;

float GfGTM_T1 = 1e-3;
float GfGTM_T2 = 10e-3;

dutyCycle_t GsGTM_OutputPWM;

__near volatile boolean GbGTM_PWMEnbl = FALSE;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
void Gtm_TOM_Init()
{
	// Time to cnt conversion: T0, T1 & T2
	Gu16GTM_T0CNT = (uint16)((float)(GfGTM_T0 * (float)SEC_TO_T0CNT));
	Gu16GTM_T1CNT = (uint16)((float)(GfGTM_T1 * (float)SEC_TO_T1CNT));
	Gu16GTM_T2CNT = (uint16)((float)(GfGTM_T2 * (float)SEC_TO_T2CNT));

	// Time to cnt conversion: dTadc, dTsamp, dTpwm
	Gu16GTM_dTadcCNT = (uint16)((float)(KfGTM_dTadc * (float)SEC_TO_T0CNT));
	Gu16GTM_dTpwmCNT = (uint16)((float)(KfGTM_dTpwm * (float)SEC_TO_T2CNT));

	GTM_DTM1_CH_CTRL2.B.POL0_0 = 0x0;
	GTM_DTM1_CH_CTRL2.B.SL0_0 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC0_0 = 0x1;
	GTM_DTM1_CH_CTRL2.B.DT0_0 = 0x1; //Dead Time Path Enabled

	GTM_DTM1_CH_CTRL2.B.POL1_0 = 0x0;
	GTM_DTM1_CH_CTRL2.B.SL1_0 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC1_0 = 0x1;
	GTM_DTM1_CH_CTRL2.B.DT1_0 = 0x1; //Dead Time Path Enabled

	GTM_DTM1_CH_CTRL2.B.POL0_1 = 0x0;
	GTM_DTM1_CH_CTRL2.B.SL0_1 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC0_1 = 0x1;
	GTM_DTM1_CH_CTRL2.B.DT0_1 = 0x1; //Dead Time Path Enabled

	GTM_DTM1_CH_CTRL2.B.POL1_1 = 0x0;
	GTM_DTM1_CH_CTRL2.B.SL1_1 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC1_1 = 0x1;
	GTM_DTM1_CH_CTRL2.B.DT1_1 = 0x1; //Dead Time Path Enabled

	GTM_DTM1_CH_CTRL2.B.POL0_2 = 0x0;
	GTM_DTM1_CH_CTRL2.B.SL0_2 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC0_2 = 0x1;
	GTM_DTM1_CH_CTRL2.B.DT0_2 = 0x1; //Dead Time Path Enabled

	GTM_DTM1_CH_CTRL2.B.POL1_2 = 0x0;
	GTM_DTM1_CH_CTRL2.B.SL1_2 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC1_2 = 0x1;
	GTM_DTM1_CH_CTRL2.B.DT1_2 = 0x1; //Dead Time Path Enabled

	/* Dead Time duration setting */
	Gu16GTM_TdeadCNT = (uint16)((float)(KfGTM_Tdead * (float)SEC_TO_T0CNT));

	GTM_DTM1_CH0_DTV.B.RELRISE = Gu16GTM_TdeadCNT;   //Rising Edge Dead Time - 100ns
	GTM_DTM1_CH0_DTV.B.RELFALL = Gu16GTM_TdeadCNT;   //Falling Edge Dead Time - 100ns

	GTM_DTM1_CH1_DTV.B.RELRISE = Gu16GTM_TdeadCNT;   //Rising Edge Dead Time - 100ns
	GTM_DTM1_CH1_DTV.B.RELFALL = Gu16GTM_TdeadCNT;   //Falling Edge Dead Time - 100ns

	GTM_DTM1_CH2_DTV.B.RELRISE = Gu16GTM_TdeadCNT;   //Rising Edge Dead Time - 100ns
	GTM_DTM1_CH2_DTV.B.RELFALL = Gu16GTM_TdeadCNT;   //Falling Edge Dead Time - 100ns

	GTM_TOM0_CH1_CTRL.U = 0x01000000; // PWM timer base channel
	GTM_TOM0_CH2_CTRL.U = 0x00100000; // Interrupt generator
	GTM_TOM0_CH3_CTRL.U = 0x00100000; // ADC trigger
	GTM_TOM0_CH4_CTRL.U = 0x00100000; //PWM Period Timer
	GTM_TOM0_CH5_CTRL.U = 0x00100000; //PWM Period Timer DTM SL = 0, TRIG_CC0
	GTM_TOM0_CH6_CTRL.U = 0x00100000; //PWM Period Timer DTM SL = 0, TRIG_CC0

	/* 1 ms interrupt settings */
	GTM_TOM1_CH0_CTRL.U = 0x01001000; 	//PWM Period Timer DTM SL = 0, TRIG_CC0 CLK_SRC_SR = 001
	GTM_TOM1_CH0_SR0.U = Gu16GTM_T1CNT;			// 1 ms interrupt based on a 100Mhz/2^4 scaled clock
	GTM_TOM1_CH0_SR1.U = Gu16GTM_T1CNT >> 1;

	/* 10 ms interrupt settings */
	GTM_TOM1_CH2_CTRL.U = 0x01001000; 	//PWM Period Timer DTM SL = 0, TRIG_CC0 CLK_SRC_SR = 011
	GTM_TOM1_CH2_SR0.U = Gu16GTM_T2CNT;
	GTM_TOM1_CH2_SR1.U = Gu16GTM_T2CNT >> 1;

	/* Initialization of T0 base counter, ADC & interrupt counter */
	GTM_UpdatePWMPeriod(Gu16GTM_T0CNT, Gu16GTM_dTpwmCNT, Gu16GTM_dTadcCNT);

	/* Initialization of PWM duty cycle to 0.5 (0V) */
	GsGTM_OutputPWM.dutyCycleA = 0.5;
	GsGTM_OutputPWM.dutyCycleB = 0.5;
	GsGTM_OutputPWM.dutyCycleC = 0.5;

	GTM_UpdatePWMOutputs(GsGTM_OutputPWM, Gu16GTM_T0CNT);

	/*Enable channels*/
	GTM_TOM0_TGC0_OUTEN_CTRL.B.OUTEN_CTRL1 	= 	0x2;  //Enable Channel Output on Update Trigger
	GTM_TOM0_TGC0_OUTEN_CTRL.B.OUTEN_CTRL2 	= 	0x2;  //Enable Channel Output on Update Trigger
	GTM_TOM0_TGC0_OUTEN_CTRL.B.OUTEN_CTRL3 	= 	0x2;  //Enable Channel Output on Update Trigger
	GTM_TOM0_TGC0_OUTEN_CTRL.B.OUTEN_CTRL4 	= 	0x2;  //Enable Channel Output on Update Trigger
	GTM_TOM0_TGC0_OUTEN_CTRL.B.OUTEN_CTRL5 	= 	0x2;  //Enable Channel Output on Update Trigger
	GTM_TOM0_TGC0_OUTEN_CTRL.B.OUTEN_CTRL6 	= 	0x2;  //Enable Channel Output on Update Trigger
	GTM_TOM1_TGC0_OUTEN_CTRL.B.OUTEN_CTRL0 	= 	0x2;  //Enable Channel Output on Update Trigger
	GTM_TOM1_TGC0_OUTEN_CTRL.B.OUTEN_CTRL2 	= 	0x2;  //Enable Channel Output on Update Trigger

	/*Enable Update*/
	GTM_TOM0_TGC0_GLB_CTRL.B.UPEN_CTRL1	=	0x2;
	GTM_TOM0_TGC0_GLB_CTRL.B.UPEN_CTRL2	=	0x2;
	GTM_TOM0_TGC0_GLB_CTRL.B.UPEN_CTRL3	=	0x2;
	GTM_TOM0_TGC0_GLB_CTRL.B.UPEN_CTRL4	=	0x2;
	GTM_TOM0_TGC0_GLB_CTRL.B.UPEN_CTRL5	=	0x2;
	GTM_TOM0_TGC0_GLB_CTRL.B.UPEN_CTRL6	=	0x2;
	GTM_TOM1_TGC0_GLB_CTRL.B.UPEN_CTRL0	=	0x2;
	GTM_TOM1_TGC0_GLB_CTRL.B.UPEN_CTRL2	=	0x2;


	/*Enable outputs*/
	GTM_TOM0_TGC0_ENDIS_CTRL.B.ENDIS_CTRL1 = 0x2;
	GTM_TOM0_TGC0_ENDIS_CTRL.B.ENDIS_CTRL2 = 0x2;
	GTM_TOM0_TGC0_ENDIS_CTRL.B.ENDIS_CTRL3 = 0x2;
	GTM_TOM0_TGC0_ENDIS_CTRL.B.ENDIS_CTRL4 = 0x2;
	GTM_TOM0_TGC0_ENDIS_CTRL.B.ENDIS_CTRL5 = 0x2;
	GTM_TOM0_TGC0_ENDIS_CTRL.B.ENDIS_CTRL6 = 0x2;
	GTM_TOM1_TGC0_ENDIS_CTRL.B.ENDIS_CTRL0 = 0x2;
	GTM_TOM1_TGC0_ENDIS_CTRL.B.ENDIS_CTRL2 = 0x2;


	/* Select TOM CH3 as the ADC Trigger */
	GTM_ADCTRIG0OUT0.B.SEL0		=   (0x3); 		//Trigger is TOM0 Channel 3 Output for ADC Group 0
	GTM_ADCTRIG0OUT0.B.SEL1		=   (0x3); 		//Trigger is TOM0 Channel 3 Output for ADC Group 1

	/*Configure the DTM*/
	GTM_DTM1_CTRL.B.CLK_SEL  = 0x2; //Use FXCLK, same as TOM
	GTM_DTM1_CTRL.B.UPD_MODE = 0x0; //Asynchronous Mode

}


void GTM_ConfigureT0Interrupt(void)
{
	/* interrupt setting for interrupt on CN0 matching CM0 */
	GTM_TOM0_CH2_IRQ_EN.B.CCU0TC_IRQ_EN 	= 0x1;   //Enable the CCU0 Event, Period Event
	GTM_TOM0_CH2_IRQ_MODE.B.IRQ_MODE 		= 0x2;	 //Pulse Notify
	GTM_TOM0_TGC0_INT_TRIG.B.INT_TRIG2 		= 0x2;	 //Enable the IRQ out of the TOM Channel

	/* using SRC_GTMTOM01 because interrupts from TOM channels are bundled TOM channel x, x+1 is SRC_GTMTOM0x */
	SRC_GTMTOM01.B.SRPN = GTM_T0_ISR_PRIO;  				//Priority, or DMA CH4 to trigger next shadow register val
	SRC_GTMTOM01.B.TOS 	= 0;							//CPU
	SRC_GTMTOM01.B.SRE 	= 1;							//Enable
}


void GTM_ConfigureT1Interrupt(void)
{
	/* interrupt setting for interrupt on CN0 matching CM0 */
	GTM_TOM1_CH0_IRQ_EN.B.CCU0TC_IRQ_EN 	= 0x1;   //Enable the CCU0 Event, Period Event
	GTM_TOM1_CH0_IRQ_MODE.B.IRQ_MODE 		= 0x2;	 //Pulse Notify
	GTM_TOM1_TGC0_INT_TRIG.B.INT_TRIG0 		= 0x2;	 //Enable the IRQ out of the TOM Channel

	/* using SRC_GTMTOM10 because interrupts from TOM channels are bundled TOM channel x, x+1 is SRC_GTMTOM0x */
	SRC_GTMTOM10.B.SRPN = GTM_T1_ISR_PRIO; 			//Priority, or DMA CH4 to trigger next shadow register val
	SRC_GTMTOM10.B.TOS 	= 0;							//CPU
	SRC_GTMTOM10.B.SRE 	= 1;							//Enable
}


void GTM_ConfigureT2Interrupt(void)
{
	/* interrupt setting for interrupt on CN0 matching CM0 */
	GTM_TOM1_CH2_IRQ_EN.B.CCU0TC_IRQ_EN 	= 0x1;   //Enable the CCU0 Event, Period Event
	GTM_TOM1_CH2_IRQ_MODE.B.IRQ_MODE 		= 0x2;	 //Pulse Notify
	GTM_TOM1_TGC0_INT_TRIG.B.INT_TRIG2 		= 0x2;	 //Enable the IRQ out of the TOM Channel

	/* using SRC_GTMTOM11 because interrupts from TOM channels are bundled TOM channel x, x+1 is SRC_GTMTOM0x */
	SRC_GTMTOM11.B.SRPN = GTM_T2_ISR_PRIO;  		//Priority, or DMA CH4 to trigger next shadow register val
	SRC_GTMTOM11.B.TOS 	= 0;						//CPU
	SRC_GTMTOM11.B.SRE 	= 1;						//Enable
}


void Gtm_Global_Init(void)
{
	volatile uint32 uwTemp;
	uint16  endinit_pw;
	endinit_pw     = IfxScuWdt_getCpuWatchdogPassword(); //Get the password for the current CPU

	//-------------------------------------------------------------------------
	//Clear the Endinit Function to access the protected Registers
	IfxScuWdt_clearCpuEndinit(endinit_pw);
	//A. Enable the GTM Module:
	GTM_CLC.U   = 0x00000000;   						// load clock control register
	uwTemp         = GTM_CLC.U; 						// dummy read to avoid pipeline effects
	while ((GTM_CLC.U & 0x00000002 )== 0x00000002);  	//wait until module is enabled

	//CBS_OEC.B.IF_LCK_P = 1;

	//SET the Endinit Function after access to the protected Registers
	IfxScuWdt_setCpuEndinit(endinit_pw);
	/*Enable the fix clock*/
	GTM_CMU_CLK_EN.B.EN_FXCLK = 0x2;
}


void Gtm_Enable(void)
{
	GTM_TOM0_TGC0_GLB_CTRL.B.HOST_TRIG = 0x1;  //Start the Timers
	GTM_TOM1_TGC0_GLB_CTRL.B.HOST_TRIG = 0x1;  //Start the Timers
}


void Gtm_PinConfig(void)
{
	/* TOM Channel 4 */
	GTM_TOUTSEL0.B.SEL0 = 0x2;     //Select Timer C from TOUTSiEL.TOUT0 Table TOM0_4
	GTM_TOUTSEL0.B.SEL1 = 0x2;     //Select Timer C from TOUTSEL.TOUT1 Table TOM0_4N - added by HatimM

	/* TOM Channel 5 */
	GTM_TOUTSEL0.B.SEL2 = 0x2;     //Select Timer C from TOUTSEL.TOUT2 Table TOM0_5
	GTM_TOUTSEL0.B.SEL3 = 0x2;     //Select Timer C from TOUTSEL.TOUT3 Table TOM0_5N - added by HatimM

	/* TOM Channel 6 */
	GTM_TOUTSEL0.B.SEL4 = 0x2;     //Select Timer C from TOUTSEL.TOUT0 Table TOM0_6
	GTM_TOUTSEL0.B.SEL5 = 0x2;     //Select Timer C from TOUTSEL.TOUT16 Table TOM0_6N


	/*Configure ports, P02.0 is TOM0_4 and P02.1 is TOM0_4N*/
	P02_IOCR0.B.PC0 = 0x11;			//Setup the port to output TOM
	P02_IOCR0.B.PC1 = 0x11; 		// added by  HatimM to enable P02.1 as TOM_4N

	/* configure port P02.2 as TOM0_5 and P02.3 as TOM0_5N */
	P02_IOCR0.B.PC2 = 0x11; 		// added by  HatimM to enable P02.1 as TOM_5
	P02_IOCR0.B.PC3 = 0x11; 		// added by  HatimM to enable P02.1 as TOM_5N

	/*P02.4 is TOM0_CH6*/
	P02_IOCR4.B.PC4 = 0x11;			//Setup the port to output TOM
	P02_IOCR4.B.PC5 = 0x11;			//Setup the port to output TOM
}

// Read STM system clock
uint32_t STM_GetSysClk(void)
{
	return STM0_TIM0.U;
}

// PWM Enable function
void GTM_PWMEnable(void)
{
	GTM_DTM1_CH_CTRL2.B.SL0_0 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC0_0 = 0x0;

	GTM_DTM1_CH_CTRL2.B.SL1_0 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC1_0 = 0x0;

	GTM_DTM1_CH_CTRL2.B.SL0_1 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC0_1 = 0x0;

	GTM_DTM1_CH_CTRL2.B.SL1_1 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC1_1 = 0x0;

	GTM_DTM1_CH_CTRL2.B.SL0_2 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC0_2 = 0x0;

	GTM_DTM1_CH_CTRL2.B.SL1_2 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC1_2 = 0x0;

	GbGTM_PWMEnbl = TRUE;
}

// PWM Disable Function
void GTM_PWMDisable(void)
{
	GTM_DTM1_CH_CTRL2.B.SL0_0 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC0_0 = 0x1;

	GTM_DTM1_CH_CTRL2.B.SL1_0 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC1_0 = 0x1;

	GTM_DTM1_CH_CTRL2.B.SL0_1 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC0_1 = 0x1;

	GTM_DTM1_CH_CTRL2.B.SL1_1 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC1_1 = 0x1;

	GTM_DTM1_CH_CTRL2.B.SL0_2 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC0_2 = 0x1;

	GTM_DTM1_CH_CTRL2.B.SL1_2 = 0x0;
	GTM_DTM1_CH_CTRL2.B.OC1_2 = 0x1;

	GbGTM_PWMEnbl = FALSE;
}

// PWM frequency update
void GTM_UpdatePWMPeriod(uint16 Lu16GTM_T0CNT, uint16 Lu16GTM_dTpwmCNT, uint16 Lu16GTM_dTadcCNT)
{
	/* T0 timer base channel */
	GTM_TOM0_CH1_SR0.U = Lu16GTM_T0CNT;
	GTM_TOM0_CH1_SR1.U = Lu16GTM_T0CNT >> 1;

	/* interrupt generation channel */
	GTM_TOM0_CH2_SR0.U = Lu16GTM_T0CNT - Lu16GTM_dTpwmCNT;
	GTM_TOM0_CH2_SR1.U = 100;

	/* ADC trigger channel */
	GTM_TOM0_CH3_SR0.U = Lu16GTM_T0CNT - Lu16GTM_dTadcCNT;
	GTM_TOM0_CH3_SR1.U = 100;
}

void GTM_T0Calc(void)
{
	float LfGTM_T0Tmp = 0.0;
	float LfGTM_T0Diff = 0.0;
	float LfGTM_Slope = 0.0;

	if (GfCCC_Wrpm < KfGTM_T0WrpmP0)
	{
		LfGTM_T0Tmp = KfGTM_T0P0;
	}
	else if (GfCCC_Wrpm > KfGTM_T0WrpmP1)
	{
		LfGTM_T0Tmp = KfGTM_T0P1;
	}
	else
	{
		LfGTM_Slope = (KfGTM_T0P1 - KfGTM_T0P0) / (KfGTM_T0WrpmP1 - KfGTM_T0WrpmP0);
		LfGTM_T0Tmp = LfGTM_Slope * (GfCCC_Wrpm - KfGTM_T0WrpmP0) + KfGTM_T0P0;
	}

	LfGTM_T0Diff = LfGTM_T0Tmp - GfGTM_T0Cmd;

	if (LfGTM_T0Diff > KfGTM_T0SlewLim)
	{
		GfGTM_T0Cmd = GfGTM_T0Cmd + KfGTM_T0SlewLim;
	}
	else if (LfGTM_T0Diff < -KfGTM_T0SlewLim)
	{
		GfGTM_T0Cmd = GfGTM_T0Cmd - KfGTM_T0SlewLim;
	}
	else
	{
		GfGTM_T0Cmd = LfGTM_T0Tmp;
	}

	Gu16GTM_T0CNTCmd = (uint16)((float)(GfGTM_T0Cmd * (float)SEC_TO_T0CNT));
}

void GTM_InitT0T1T2(void)
{
	GfGTM_T0 = KfGTM_T0P0;
	GfGTM_T0New = KfGTM_T0P0;
	GfGTM_T0Cmd = KfGTM_T0P0;
	GfGTM_T1 = KfGTM_T1;
	GfGTM_T2 = KfGTM_T2;
}

// PWM Output update
void GTM_UpdatePWMOutputs(dutyCycle_t LsGTM_dutyCycle, uint16 Lu16GTM_T0CNT)
{
	float LfGTM_DutyTmp = 0.0;

	LfGTM_DutyTmp = (float)Lu16GTM_T0CNT * (LsGTM_dutyCycle.dutyCycleA);
	GTM_TOM0_CH4_SR1.U = (uint16)((float)(Lu16GTM_T0CNT - LfGTM_DutyTmp) * 0.5); 	// CM1 /* FIXME issue if CM1 <= 1, should be limited for up to AB step at least */
	GTM_TOM0_CH4_SR0.U = (uint16)((float)(Lu16GTM_T0CNT + LfGTM_DutyTmp) * 0.5); 	// CM0

	LfGTM_DutyTmp = (float)Lu16GTM_T0CNT * (LsGTM_dutyCycle.dutyCycleB);
	GTM_TOM0_CH5_SR1.U = (uint16)((float)(Lu16GTM_T0CNT - LfGTM_DutyTmp) * 0.5); 	// CM1 /* FIXME issue if CM1 <= 1, should be limited for up to AB step at least */
	GTM_TOM0_CH5_SR0.U = (uint16)((float)(Lu16GTM_T0CNT + LfGTM_DutyTmp) * 0.5); 	// CM0

	LfGTM_DutyTmp = (float)Lu16GTM_T0CNT * (LsGTM_dutyCycle.dutyCycleC);
	GTM_TOM0_CH6_SR1.U = (uint16)((float)(Lu16GTM_T0CNT - LfGTM_DutyTmp) * 0.5); 	// CM1 /* FIXME issue if CM1 <= 1, should be limited for up to AB step at least */
	GTM_TOM0_CH6_SR0.U = (uint16)((float)(Lu16GTM_T0CNT + LfGTM_DutyTmp) * 0.5); 	// CM0
}
