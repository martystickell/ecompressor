/*
 * Gtm_10ms_Isr.c
 *
 *  Created on: Oct 10, 2018
 *      Author: H297270
 */
#include "CAN_Interface_Tx.h"
#include "CAN_Interface_Rx.h"
#include "Gtm.h"
#include "STM.h"

IFX_INTERRUPT(GTM_Update_T2_ISR, 0, GTM_T2_ISR_PRIO);
void GTM_Update_T2_ISR(void)
{
	__enable();

	// System counter reading
	Gu32STM_T2Cnt0 = STM_GetSysClk();

	GeCanRxLink_MsgStat = GOOD;

	/* Receive Data */
	Receive_CMD_0();
	Receive_CMD_1();
	Receive_CMD_2();

	GeCanTxLink_MsgStat = GOOD;

	CAN_TxMsgUpdate();

	/* Transmit Data */
	Transmit_FDBK_0();
	Transmit_FDBK_1();
	Transmit_FDBK_2();
	Transmit_FDBK_3();
	Transmit_FDBK_4();
	Transmit_FDBK_5();
	Transmit_FDBK_6();
	Transmit_FDBK_7();
	Transmit_FDBK_8();

	// System counter reading
	Gu32STM_T2Cnt1 = STM_GetSysClk();
	Gu32STM_T2ThruPt = Gu32STM_T2Cnt1 - Gu32STM_T2Cnt0;
}


