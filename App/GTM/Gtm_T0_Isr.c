/*
 * Gtm_Isr.c
 *
 *  Created on: Oct 2, 2018
 *      Author: H297270
 */
#include "Gtm.h"
#include "vadc.h"
#include "Gpio.h"
#include "STM.h"
#include "MES.h"
#include "MCS.h"
#include <App/CBT/ControlBoardTest.h>

#define MAX_ARRAY_CT 16

volatile int max_ct = MAX_ARRAY_CT;

volatile float an0_array[MAX_ARRAY_CT];
volatile float an1_array[MAX_ARRAY_CT];
volatile float an2_array[MAX_ARRAY_CT];
volatile float an3_array[MAX_ARRAY_CT];
volatile float an4_array[MAX_ARRAY_CT];
volatile float an5_array[MAX_ARRAY_CT];
volatile float an6_array[MAX_ARRAY_CT];
volatile float an7_array[MAX_ARRAY_CT];
volatile float an8_array[MAX_ARRAY_CT];
volatile float an9_array[MAX_ARRAY_CT];

volatile int gate_input[MAX_ARRAY_CT];

volatile float an0_1_array[MAX_ARRAY_CT];
volatile float an1_1_array[MAX_ARRAY_CT];
volatile float an2_1_array[MAX_ARRAY_CT];
volatile float an3_1_array[MAX_ARRAY_CT];
volatile float an4_1_array[MAX_ARRAY_CT];
volatile float an5_1_array[MAX_ARRAY_CT];
volatile float an6_1_array[MAX_ARRAY_CT];
volatile float an7_1_array[MAX_ARRAY_CT];
volatile float an8_1_array[MAX_ARRAY_CT];
volatile float an9_1_array[MAX_ARRAY_CT];

#pragma section code "ISR_RAMCODE"

IFX_INTERRUPT(GTM_Update_T0_ISR, 0, GTM_T0_ISR_PRIO);
void GTM_Update_T0_ISR(void)
{
	// System counter reading
	Gu32STM_T0Cnt0 = STM_GetSysClk();

	// State update
	GeSTM_LastStateT0 = GeSTM_CurrentStateT0;
	GeSTM_CurrentStateT0 = GeSTM_NextStateT0;
	GeSTM_NextStateT0 = GeSTM_CurrentStateT1;

	// Handshaking of T0 period to have proper value
	GfGTM_T0 = GfGTM_T0New;
	GfGTM_T0New = GfGTM_T0Cmd;

	Gu16GTM_T0CNT = Gu16GTM_T0CNTCmd;

#if (BETA_CB_TEST == ENABLE)
	VADC_G0ASMR.B.LDEV = 1;		// trigger group 0 scan
	VADC_G1ASMR.B.LDEV = 1;		// trigger group 0 scan
	SetOutputs();
	GetCBT_ADCValues();
	GetGPIOStatus();
#endif

	switch(GeSTM_CurrentStateT0)
	{
		case Off:
			STM_OffT0();
			break;

		case TurnOn:
			STM_TurnOnT0();
			break;

		case StandBy:
			STM_StandByT0();
			break;

		case Run:
			STM_RunT0();
			break;

		case TurnOff:
			STM_TurnOffT0();
			break;

		case Fault:
			STM_FaultT0();
			break;
	}

	// T0 period update
	GTM_UpdatePWMPeriod(Gu16GTM_T0CNT, Gu16GTM_dTpwmCNT, Gu16GTM_dTadcCNT);

	// System counter reading
	Gu32STM_T0Cnt1 = STM_GetSysClk();
	Gu32STM_T0ThruPt = Gu32STM_T0Cnt1 - Gu32STM_T0Cnt0;

	VX1000If_Event(0);
}



