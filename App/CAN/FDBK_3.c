/*
 * FDBK_3.c
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */
#include "FDBK_3.h"

static uint64 FDBK_3_data;

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

/*****************************MSG_18E14280h***************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/

signalCfg_t GstCanTx_TToFullCfg;
signalCfg_t GstCanTx_IndcrPwrResiCfg;
signalCfg_t GstCanTx_TpMosfetCfg;
signalCfg_t GstCanTx_PwrIntglThdCfg;
signalCfg_t GstCanTx_TToEmptyCfg;
/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

esignal_stat GeCanTx_TToFull_stat;
float GfCanTx_TToFull;
uint8 Gu8CanTx_TToFull;
const signalCfg_t kst_TToFull		= {1.25f, -1.0f, 204.0f, 1.0f, TTOFULL_STAT_PTR};

esignal_stat GeCanTx_IndcrPwrResi_stat;
float GfCanTx_IndcrPwrResi;
uint8 Gu8CanTx_IndcrPwrResi;
const signalCfg_t kst_IndcrPwrResi	= {1.0f, 0.0f, 255.0f, 0.0f, INDCTRPWRRESI_STAT_PTR};

esignal_stat GeCanTx_TpMosfet_stat;
float GfCanTx_TpMosfet;
uint8 Gu8CanTx_TpMosfet;
const signalCfg_t kst_TpMosfet		= {2.0f, 0.0f, 255.5f, 0.0f, TPMOSFET_STAT_PTR};

esignal_stat GeCanTx_PwrIntglThd_stat;
float GfCanTx_PwrIntglThd;
uint16 Gu16CanTx_PwrIntglThd;
const signalCfg_t kst_PwrIntglThd	= {0.5f, 0.0f, 20460.0f, 0.0f, PWRINTGLTHD_STAT_PTR};

esignal_stat GeCanTx_TToEmpty_stat;
float GfCanTx_TToEmpty;
uint8 Gu8CanTx_TToEmpty;
const signalCfg_t kst_TToEmpty		= {1.25f, -1.0f, 204.0f, 1.0f, TTOEMPTY_STAT_PTR};

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

void Init_FDBK_3(void)
{
    CAN_Driver_MessageObjectInit(&g_MulticanBasic.drivers.can0SrcMsgObj3, MSG_ID_FDBK_3, IfxMultican_Frame_transmit, TRUE);

    GeCanTx_TToFull_stat = OK;
	GfCanTx_TToFull = 0.0f;
	Gu8CanTx_TToFull = 0;
	GstCanTx_TToFullCfg = kst_TToFull;

	GeCanTx_IndcrPwrResi_stat = OK;
	GfCanTx_IndcrPwrResi = 0.0f;
	Gu8CanTx_IndcrPwrResi = 0;
	GstCanTx_IndcrPwrResiCfg = kst_IndcrPwrResi;

	GeCanTx_TpMosfet_stat = OK;
	GfCanTx_TpMosfet = 0.0f;
	Gu8CanTx_TpMosfet = 0;
	GstCanTx_TpMosfetCfg = kst_TpMosfet;

	GeCanTx_PwrIntglThd_stat = OK;
	GfCanTx_PwrIntglThd = 0.0f;
	Gu16CanTx_PwrIntglThd = 0;
	GstCanTx_PwrIntglThdCfg = kst_PwrIntglThd;

	GeCanTx_TToEmpty_stat = OK;
	GfCanTx_TToEmpty = 0.0f;
	Gu8CanTx_TToEmpty = 0;
	GstCanTx_TToEmptyCfg = kst_TToEmpty;
}

static void Process_FDBK_3(void)
{
	memset(&FDBK_3_data, 0, sizeof(uint64));

	/*
	GfCanTx_TempBdr2 = GfMES_TempBdr2;
	Process_TxSignal_u8(GfCanTx_TempBdr2,	GstCanTx_TempBdr2Cfg, 	&Gu8CanTx_TempBdr2);
	FDBK_3_data |= FILL(Gu8CanTx_TempBdr2, BIT_MASK_TEMP_BDR2, BIT_START_POS_TEMP_BDR2);
	GeCanTxLink_MsgStat |= *(GstCanTx_TempBdr2Cfg.stat_ptr);
	*/

	//GfCanTx_TToFull = 0;
	Process_TxSignal_u8(GfCanTx_TToFull,	GstCanTx_TToFullCfg, 	&Gu8CanTx_TToFull);
	FDBK_3_data |= FILL(Gu8CanTx_TToFull, BIT_MASK_TToFull, BIT_START_POS_TToFull);
	GeCanTxLink_MsgStat |= *(GstCanTx_TToFullCfg.stat_ptr);

	//GfCanTx_IndcrPwrResi = 0;
	Process_TxSignal_u8(GfCanTx_IndcrPwrResi,	GstCanTx_IndcrPwrResiCfg, 	&Gu8CanTx_IndcrPwrResi);
	FDBK_3_data |= FILL(Gu8CanTx_IndcrPwrResi, BIT_MASK_IndcrPwrResi, BIT_START_POS_IndcrPwrResi);
	GeCanTxLink_MsgStat |= *(GstCanTx_IndcrPwrResiCfg.stat_ptr);

	//GfCanTx_TpMosfet = 0;
	Process_TxSignal_u8(GfCanTx_TpMosfet,	GstCanTx_TpMosfetCfg, 	&Gu8CanTx_TpMosfet);
	FDBK_3_data |= FILL(Gu8CanTx_TpMosfet, BIT_MASK_TpMosfet, BIT_START_POS_TpMosfet);
	GeCanTxLink_MsgStat |= *(GstCanTx_TpMosfetCfg.stat_ptr);

	//GfCanTx_PwrIntglThd
	if(GfCanTx_PwrIntglThd > GstCanTx_PwrIntglThdCfg.max )
	{
		Gu16CanTx_PwrIntglThd = GstCanTx_PwrIntglThdCfg.max;
		GeCanTx_PwrIntglThd_stat = OOR_HIGH;
	}
	else if(GfCanTx_PwrIntglThd < GstCanTx_PwrIntglThdCfg.min )
	{
		Gu16CanTx_PwrIntglThd = GstCanTx_PwrIntglThdCfg.min;
		GeCanTx_PwrIntglThd_stat = OOR_LOW;
	}
	else
	{
		Gu16CanTx_PwrIntglThd = (uint16)((GfCanTx_PwrIntglThd * GstCanTx_PwrIntglThdCfg.res) + GstCanTx_PwrIntglThdCfg.offset);
		GeCanTx_PwrIntglThd_stat = OK;
	}
	FDBK_3_data |= FILL(Gu16CanTx_PwrIntglThd, BIT_MASK_PwrIntglThd, BIT_START_POS_PwrIntglThd);
	GeCanTxLink_MsgStat |= GeCanTx_PwrIntglThd_stat;

	//GfCanTx_TToEmpty = 0;
	Process_TxSignal_u8(GfCanTx_TToEmpty,	GstCanTx_TToEmptyCfg, 	&Gu8CanTx_TToEmpty);
	FDBK_3_data |= FILL(Gu8CanTx_TToEmpty, BIT_MASK_TToFull, BIT_START_POS_TToEmpty);
	GeCanTxLink_MsgStat |= *(GstCanTx_TToEmptyCfg.stat_ptr);
}


void Transmit_FDBK_3(void)
{
	uint32 dataLowTx  = 0x00000000;
	uint32 dataHighTx = 0x00000000;
	uint64 datawordTx = 0;
	IfxMultican_Message msg_buf;

	Process_FDBK_3();

	memcpy(&datawordTx, &FDBK_3_data, sizeof(uint64));

	dataLowTx 	= (uint32)datawordTx;
	dataHighTx 	= (uint64)datawordTx >> 32;

	IfxMultican_Message_init(&msg_buf, MSG_ID_FDBK_3, dataLowTx , dataHighTx, IfxMultican_DataLengthCode_8);

	IfxMultican_Can_MsgObj_sendMessage(&g_MulticanBasic.drivers.can0SrcMsgObj3, &msg_buf);
}

#elif (CUSTOMER_ID == AUDI)

/*****************************MSG_18E14288h***************************************/
/*********************************************************************************/
/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/
signalCfg_t GstCanTx_TempBdr1Cfg;
signalCfg_t GstCanTx_TempBdr2Cfg;
signalCfg_t GstCanTx_TToFullCfg;
signalCfg_t GstCanTx_IndcrPwrResiCfg;
signalCfg_t GstCanTx_TToEmptyCfg;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
static uint64 FDBK_3_data;

esignal_stat GeCanTx_TempBdr1_stat;
float GfCanTx_TempBdr1;
const signalCfg_t kst_TempBdr1		= {0.50f, -254.0f, 254.0f, 254.0f, TEMP_BDR1_STAT_PTR};

esignal_stat GeCanTx_TempBdr2_stat;
float GfCanTx_TempBdr2;
const signalCfg_t kst_TempBdr2		= {0.50f, -254.0f, 254.0f, 254.0f, TEMP_BDR2_STAT_PTR};

esignal_stat GeCanTx_TToFull_stat;
float GfCanTx_TToFull;
const signalCfg_t kst_TToFull		= {1.0f, -1.0f, 204.0f, -1.0f, TTOFULL_STAT_PTR};

esignal_stat GeCanTx_IndcrPwrResi_stat;
float GfCanTx_IndcrPwrResi;
const signalCfg_t kst_IndcrPwrResi	= {1.0f, 0.0f, 255.0f, 0.0f, INDCTRPWRRESI_STAT_PTR};

esignal_stat GeCanTx_TToEmpty_stat;
float GfCanTx_TToEmpty;
const signalCfg_t kst_TToEmpty		= {1.0f, -1.0f, 204.0f, -1.0f, TTOEMPTY_STAT_PTR};

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
void Init_FDBK_3(void)
{
	GeCanTx_TempBdr1_stat = OK;
	GfCanTx_TempBdr1 = 0.0f;
	GstCanTx_TempBdr1Cfg = kst_TempBdr1;

	GeCanTx_TempBdr2_stat 	= OK;
	GfCanTx_TempBdr2 		= 0.0f;
	GstCanTx_TempBdr2Cfg 	= kst_TempBdr2;

	GeCanTx_TToFull_stat = OK;
	GfCanTx_TToFull = 0.0f;
	GstCanTx_TToFullCfg = kst_TToFull;

	GeCanTx_IndcrPwrResi_stat = OK;
	GfCanTx_IndcrPwrResi = 0.0f;
	GstCanTx_IndcrPwrResiCfg = kst_IndcrPwrResi;

	GeCanTx_TToEmpty_stat = OK;
	GfCanTx_TToEmpty = 0.0f;
	GstCanTx_TToEmptyCfg = kst_TToEmpty;
}

void Process_FDBK_3(void)
{
	static uint8 Gu8CanTx_data3;

	memset(&FDBK_3_data, 0, sizeof(uint64));

	GfCanTx_TempBdr2 = GfMES_TempBdr2;
	Process_TxSignal_u8(GfCanTx_TempBdr2,	GstCanTx_TempBdr2Cfg, 	&Gu8CanTx_data3);
	FDBK_3_data |= FILL(Gu8CanTx_data3, BIT_MASK_TEMP_BDR2, BIT_START_POS_TEMP_BDR2);
	GeCanTxLink_MsgStat |= *(GstCanTx_TempBdr2Cfg.stat_ptr);

	GfCanTx_TempBdr1 = GfMES_TempBdr1;
	Process_TxSignal_u8(GfCanTx_TempBdr1,	GstCanTx_TempBdr1Cfg, 	&Gu8CanTx_data3);
	FDBK_3_data |= FILL(Gu8CanTx_data3, BIT_MASK_TEMP_BDR1, BIT_START_POS_TEMP_BDR1);
	GeCanTxLink_MsgStat |= *(GstCanTx_TempBdr1Cfg.stat_ptr);

	//GfCanTx_TToFull = 0;
	Process_TxSignal_u8(GfCanTx_TToFull,	GstCanTx_TToFullCfg, 	&Gu8CanTx_data3);
	FDBK_3_data |= FILL(Gu8CanTx_data3, BIT_MASK_TToFull, BIT_START_POS_TToFull);
	GeCanTxLink_MsgStat |= *(GstCanTx_TToFullCfg.stat_ptr);

	//GfCanTx_IndcrPwrResi = 0;
	Process_TxSignal_u8(GfCanTx_IndcrPwrResi,	GstCanTx_IndcrPwrResiCfg, 	&Gu8CanTx_data3);
	FDBK_3_data |= FILL(Gu8CanTx_data3, BIT_MASK_IndcrPwrResi, BIT_START_POS_IndcrPwrResi);
	GeCanTxLink_MsgStat |= *(GstCanTx_IndcrPwrResiCfg.stat_ptr);

	//GfCanTx_TToEmpty = 0;
	Process_TxSignal_u8(GfCanTx_TToEmpty,	GstCanTx_TToEmptyCfg, 	&Gu8CanTx_data3);
	FDBK_3_data |= FILL(Gu8CanTx_data3, BIT_MASK_TToFull, BIT_START_POS_TToEmpty);
	GeCanTxLink_MsgStat |= *(GstCanTx_TToEmptyCfg.stat_ptr);
}

void Transmit_FDBK_3(void)
{
	uint32 dataLowTx  = 0x00000000;
	uint32 dataHighTx = 0x00000000;
	uint64 datawordTx = 0;
	IfxMultican_Message msg_buf;

	Process_FDBK_3();

	memcpy(&datawordTx, &FDBK_3_data, sizeof(uint64));

	dataLowTx 	= (uint32)datawordTx;
	dataHighTx 	= (uint64)datawordTx >> 32;

	IfxMultican_Message_init(&msg_buf, MSG_ID_FDBK_3, dataLowTx , dataHighTx, IfxMultican_DataLengthCode_8);
	IfxMultican_Can_MsgObj_sendMessage(&g_MulticanBasic.drivers.can0SrcMsgObj3, &msg_buf);
}

#else

void Init_FDBK_3(void)
{
	/* Do nothing */
}

void Transmit_FDBK_3(void)
{
	/* Do nothing */
}

#endif /* CUSTOMER_ID */
