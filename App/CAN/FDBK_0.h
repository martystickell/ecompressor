/*
 * FDBK_0.h
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */
#ifndef APP_CAN_FDBK_0_H_
#define APP_CAN_FDBK_0_H_

#include <Ifx_Types.h>
#include <Ifx_TypesReg.h>
#include <string.h>
#include "CAN_Driver.h"
#include "CAN_Interface_Tx.h"
#include "STM.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

/*****************************MSG_06Bh********************************************/
/*********************************************************************************/
extern bool GbCANTx_FaultREVPOL;
extern bool GbCANTx_FaultMECHFAILURE;
extern bool GbCANTx_FaultELECFAILURE;
extern bool GbCANTx_FaultCANTIMEOUT;
extern bool GbCANTx_FaultUT;

/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/
#define MSG_ID_FDBK_0				0x6B

#define BIT_START_POS_FAULT00		0
#define BIT_START_POS_FAULT01		1
#define BIT_START_POS_FAULT02		2
#define BIT_START_POS_FAULT03		3
#define BIT_START_POS_FAULT04		4
#define BIT_START_POS_FAULT05		5
#define BIT_START_POS_FAULT06		6
#define BIT_START_POS_FAULT07		7
#define BIT_START_POS_FAULT08		8
#define BIT_START_POS_FAULT09		9
#define BIT_START_POS_FAULT10		10
#define BIT_START_POS_FAULT11		11
#define BIT_START_POS_FAULT12		12
#define BIT_START_POS_FAULT13		14
#define BIT_START_POS_FAULT14		15
#define BIT_START_POS_FAULT15		16
#define BIT_START_POS_FAULT16		17
#define BIT_START_POS_FAULT17		18
#define BIT_START_POS_FAULT18		19
#define BIT_START_POS_FAULT19		20
#define BIT_START_POS_FAULT20		21
#define BIT_START_POS_FAULT21		22
#define BIT_START_POS_SPEED_FDBK	24
#define BIT_START_POS_FAULT22		36
#define BIT_START_POS_FAULT23		37
#define BIT_START_POS_FAULT24		38
#define BIT_START_POS_FAULT25		39
#define BIT_START_POS_BUS_CURR		40
#define BIT_START_POS_FAULT_REVPOL			57
#define BIT_START_POS_FAULT_CANTIMEOUT		58
#define BIT_START_POS_FAULT_ELECFAILURE		59
#define BIT_START_POS_FAULT_MECHFAILURE		60
#define BIT_START_POS_FAULT_UT		13


#define BIT_SIZE_FAULT00			1
#define BIT_SIZE_FAULT01			1
#define BIT_SIZE_FAULT02			1
#define BIT_SIZE_FAULT03			1
#define BIT_SIZE_FAULT04			1
#define BIT_SIZE_FAULT05			1
#define BIT_SIZE_FAULT06			1
#define BIT_SIZE_FAULT07			1
#define BIT_SIZE_FAULT08			1
#define BIT_SIZE_FAULT09			1
#define BIT_SIZE_FAULT10			1
#define BIT_SIZE_FAULT11			1
#define BIT_SIZE_FAULT12			1
#define BIT_SIZE_FAULT13			1
#define BIT_SIZE_FAULT14			1
#define BIT_SIZE_FAULT15			1
#define BIT_SIZE_FAULT16			1
#define BIT_SIZE_FAULT17			1
#define BIT_SIZE_FAULT18			1
#define BIT_SIZE_FAULT19			1
#define BIT_SIZE_FAULT20			1
#define BIT_SIZE_FAULT21			1
#define BIT_SIZE_SPEED_FDBK			12
#define BIT_SIZE_FAULT22			1
#define BIT_SIZE_FAULT23			1
#define BIT_SIZE_FAULT24			1
#define BIT_SIZE_FAULT25			1
#define BIT_SIZE_BUS_CURR			8
#define BIT_SIZE_FAULT_REVPOL		1
#define BIT_SIZE_FAULT_CANTIMEOUT	1
#define BIT_SIZE_FAULT_ELECFAILURE	1
#define BIT_SIZE_FAULT_MECHFAILURE	1
#define BIT_SIZE_FAULT_UT			1

#define BIT_MASK_FAULT00			(1<<BIT_SIZE_FAULT00)-1
#define BIT_MASK_FAULT01			(1<<BIT_SIZE_FAULT01)-1
#define BIT_MASK_FAULT02			(1<<BIT_SIZE_FAULT02)-1
#define BIT_MASK_FAULT03			(1<<BIT_SIZE_FAULT03)-1
#define BIT_MASK_FAULT04			(1<<BIT_SIZE_FAULT04)-1
#define BIT_MASK_FAULT05			(1<<BIT_SIZE_FAULT05)-1
#define BIT_MASK_FAULT06			(1<<BIT_SIZE_FAULT06)-1
#define BIT_MASK_FAULT07			(1<<BIT_SIZE_FAULT07)-1
#define BIT_MASK_FAULT08			(1<<BIT_SIZE_FAULT08)-1
#define BIT_MASK_FAULT09			(1<<BIT_SIZE_FAULT09)-1
#define BIT_MASK_FAULT10			(1<<BIT_SIZE_FAULT10)-1
#define BIT_MASK_FAULT11			(1<<BIT_SIZE_FAULT11)-1
#define BIT_MASK_FAULT12			(1<<BIT_SIZE_FAULT12)-1
#define BIT_MASK_FAULT13			(1<<BIT_SIZE_FAULT13)-1
#define BIT_MASK_FAULT14			(1<<BIT_SIZE_FAULT14)-1
#define BIT_MASK_FAULT15			(1<<BIT_SIZE_FAULT15)-1
#define BIT_MASK_FAULT16			(1<<BIT_SIZE_FAULT16)-1
#define BIT_MASK_FAULT17			(1<<BIT_SIZE_FAULT17)-1
#define BIT_MASK_FAULT18			(1<<BIT_SIZE_FAULT18)-1
#define BIT_MASK_FAULT19			(1<<BIT_SIZE_FAULT19)-1
#define BIT_MASK_FAULT20			(1<<BIT_SIZE_FAULT20)-1
#define BIT_MASK_FAULT21			(1<<BIT_SIZE_FAULT21)-1
#define BIT_MASK_SPEED_FDBK			(1<<BIT_SIZE_SPEED_FDBK)-1
#define BIT_MASK_FAULT22			(1<<BIT_SIZE_FAULT22)-1
#define BIT_MASK_FAULT23			(1<<BIT_SIZE_FAULT23)-1
#define BIT_MASK_FAULT24			(1<<BIT_SIZE_FAULT24)-1
#define BIT_MASK_FAULT25			(1<<BIT_SIZE_FAULT25)-1
#define BIT_MASK_BUS_CURR			(1<<BIT_SIZE_BUS_CURR)-1
#define BIT_MASK_FAULT_REVPOL		(1<<BIT_SIZE_FAULT_REVPOL)-1
#define BIT_MASK_FAULT_CANTIMEOUT	(1<<BIT_SIZE_FAULT_CANTIMEOUT)-1
#define BIT_MASK_FAULT_ELECFAILURE	(1<<BIT_SIZE_FAULT_ELECFAILURE)-1
#define BIT_MASK_FAULT_MECHFAILURE	(1<<BIT_SIZE_FAULT_MECHFAILURE)-1
#define BIT_MASK_FAULT_UT			(1<<BIT_SIZE_FAULT_UT)-1

IFX_EXTERN bool GbCanTx_Fault00;
IFX_EXTERN bool GbCanTx_Fault01;
IFX_EXTERN bool GbCanTx_Fault02;
IFX_EXTERN bool GbCanTx_Fault03;
IFX_EXTERN bool GbCanTx_Fault04;
IFX_EXTERN bool GbCanTx_Fault05;
IFX_EXTERN bool GbCanTx_Fault06;
IFX_EXTERN bool GbCanTx_Fault07;
IFX_EXTERN bool GbCanTx_Fault08;
IFX_EXTERN bool GbCanTx_Fault09;
IFX_EXTERN bool GbCanTx_Fault10;
IFX_EXTERN bool GbCanTx_Fault11;
IFX_EXTERN bool GbCanTx_Fault12;
IFX_EXTERN bool GbCanTx_Fault13;
IFX_EXTERN bool GbCanTx_Fault14;
IFX_EXTERN bool GbCanTx_Fault15;
IFX_EXTERN bool GbCanTx_Fault16;
IFX_EXTERN bool GbCanTx_Fault17;
IFX_EXTERN bool GbCanTx_Fault18;
IFX_EXTERN bool GbCanTx_Fault19;
IFX_EXTERN bool GbCanTx_Fault20;
IFX_EXTERN bool GbCanTx_Fault21;
IFX_EXTERN bool GbCanTx_Fault22;
IFX_EXTERN bool GbCanTx_Fault23;
IFX_EXTERN bool GbCanTx_Fault24;
IFX_EXTERN bool GbCanTx_Fault25;

#define  SPD_FDBK_STAT_PTR 		&GeCanTx_SpeedFeedback_stat
#define  BUS_CURR_STAT_PTR  	&GeCanTx_BusCurrent_stat

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

IFX_EXTERN float GfCanTx_SpeedFeedback;
IFX_EXTERN uint16 Gu16CanTx_SpeedFeedback;
IFX_EXTERN esignal_stat GfCanTx_SpeedFeedback_stat;

IFX_EXTERN float GfCanTx_BusCurrent;
IFX_EXTERN uint8 Gu8CanTx_BusCurrent;
IFX_EXTERN esignal_stat	GfCanTx_BusCurrent_stat;

IFX_EXTERN esignal_stat GeCanTx_SpeedFeedback_stat;
IFX_EXTERN esignal_stat GeCanTx_BusCurrent_stat;

#elif (CUSTOMER_ID == AUDI)

/*****************************MSG_0E3h********************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/
#define MSG_ID_FDBK_0						0x0E3

#define BIT_START_POS_DertgFlgSW_Heat	8
#define BIT_START_POS_DertgFlgSW_Temp	9
#define BIT_START_POS_DertgFlgVdc48v	10
#define BIT_START_POS_DertgFlgHW_Temp	11
#define BIT_START_POS_SpeedActual		12
#define BIT_START_POS_DcCurrent			28
#define BIT_START_POS_DcVoltage			36
#define BIT_START_POS_BoostUnderTempFlg	44

#define BIT_SIZE_DertgFlgSW_Heat		1
#define BIT_SIZE_DertgFlgSW_Temp		1
#define BIT_SIZE_DertgFlgVdc48v			1
#define BIT_SIZE_DertgFlgHW_Temp		1
#define BIT_SIZE_SpeedActual			16
#define BIT_SIZE_DcCurrent				8
#define BIT_SIZE_DcVoltage				8
#define BIT_SIZE_BoostUnderTempFlg		1

#define BIT_MASK_DertgFlgSW_Heat		((1<<BIT_SIZE_DertgFlgSW_Heat)-1)
#define BIT_MASK_DertgFlgSW_Temp		((1<<BIT_SIZE_DertgFlgSW_Temp)-1)
#define BIT_MASK_DertgFlgVdc48v			((1<<BIT_SIZE_DertgFlgVdc48v)-1)
#define BIT_MASK_DertgFlgHW_Temp		((1<<BIT_SIZE_DertgFlgHW_Temp)-1)
#define BIT_MASK_SpeedActual			((1<<BIT_SIZE_SpeedActual)-1)
#define BIT_MASK_DcCurrent				((1<<BIT_SIZE_DcCurrent)-1)
#define BIT_MASK_DcVoltage				((1<<BIT_SIZE_DcVoltage)-1)
#define BIT_MASK_BoostUnderTempFlg		((1<<BIT_SIZE_BoostUnderTempFlg)-1)

#define SpeedActual_STAT_PTR 	&GeCanTx_SpeedActual_stat
#define DcCurrent_STAT_PTR  	&GeCanTx_DcCurrent_stat
#define DcVoltage_STAT_PTR		&GeCanTx_DcVoltage_stat

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
IFX_EXTERN bool GbCanTx_DertgFlgSW_Heat;
IFX_EXTERN bool GbCanTx_DertgFlgSW_Temp;
IFX_EXTERN bool GbCanTx_DertgFlgVdc48v;
IFX_EXTERN bool GbCanTx_DertgFlgHW_Temp;
IFX_EXTERN float GfCanTx_SpeedActual;
IFX_EXTERN float GfCanTx_DcCurrent;
IFX_EXTERN float GfCanTx_DcVoltage;
IFX_EXTERN bool GbCanTx_BoostUnderTempFlg;

#endif /* CUSTOMER_ID */

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
IFX_EXTERN void Init_FDBK_0(void);
IFX_EXTERN void Transmit_FDBK_0(void);

#endif /* APP_CAN_FDBK_0_H_ */
