/*
 * FDBK_2.h
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */

#ifndef APP_CAN_FDBK_2_H_
#define APP_CAN_FDBK_2_H_

#include <Ifx_Types.h>
#include <Ifx_TypesReg.h>
#include <string.h>
#include "CAN_Driver.h"
#include "CAN_Interface_Tx.h"
#include "STM.h"
#include "MES.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)
/*****************************MSG_18E14240h***************************************/
/*********************************************************************************/
/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/


#define MSG_ID_FDBK_2					0x18E14240
#define FDBK_2_SUPPORT_SIGNED_VAL		0

#define BIT_START_POS_TEMP_SENS2		0
#define BIT_START_POS_TEMP_SENS3		8
#define BIT_START_POS_WARNING00			16
#define BIT_START_POS_WARNING01			17
#define BIT_START_POS_WARNING02			18
#define BIT_START_POS_WARNING03			19
#define BIT_START_POS_WARNING04			20
#define BIT_START_POS_WARNING05			21
#define BIT_START_POS_WARNING06			22
#define BIT_START_POS_WARNING07			23
#define BIT_START_POS_LIMIT00			24
#define BIT_START_POS_LIMIT01			25
#define BIT_START_POS_LIMIT02			26
#define BIT_START_POS_LIMIT03			27
//#define BIT_START_POS_LIMIT04			28
//#define BIT_START_POS_LIMIT05			29
//#define BIT_START_POS_LIMIT06			30
//#define BIT_START_POS_LIMIT07			31
#define BIT_START_POS_INT_SPD_CMD		32
#define BIT_START_POS_TQ_EST			40
#define BIT_START_POS_FAULT32			48
#define BIT_START_POS_FAULT33			49
#define BIT_START_POS_FAULT34			50
#define BIT_START_POS_FAULT35			51
#define BIT_START_POS_FAULT36			52
#define BIT_START_POS_FAULT37			53
#define BIT_START_POS_FAULT38			54
#define BIT_START_POS_FAULT39			55
#define BIT_START_POS_PBA_TEMP			56
#define BIT_START_POS_DERATE_HEAT		28
#define BIT_START_POS_DERATE_SWTEMP		29
#define BIT_START_POS_DERATE_VDC		30
#define BIT_START_POS_DERATE_HWTEMP		31

#define BIT_SIZE_TEMP_SENS2				8
#define BIT_SIZE_TEMP_SENS3				8
#define BIT_SIZE_WARNING00				1
#define BIT_SIZE_WARNING01				1
#define BIT_SIZE_WARNING02				1
#define BIT_SIZE_WARNING03				1
#define BIT_SIZE_WARNING04				1
#define BIT_SIZE_WARNING05				1
#define BIT_SIZE_WARNING06				1
#define BIT_SIZE_WARNING07				1
#define BIT_SIZE_LIMIT00				1
#define BIT_SIZE_LIMIT01				1
#define BIT_SIZE_LIMIT02				1
#define BIT_SIZE_LIMIT03				1
#define BIT_SIZE_LIMIT04				1
#define BIT_SIZE_LIMIT05				1
#define BIT_SIZE_LIMIT06				1
#define BIT_SIZE_LIMIT07				1
#define BIT_SIZE_INT_SPD_CMD			8
#define BIT_SIZE_TQ_EST					8
#define BIT_SIZE_FAULT32				1
#define BIT_SIZE_FAULT33				1
#define BIT_SIZE_FAULT34				1
#define BIT_SIZE_FAULT35				1
#define BIT_SIZE_FAULT36				1
#define BIT_SIZE_FAULT37				1
#define BIT_SIZE_FAULT38				1
#define BIT_SIZE_FAULT39				1
#define BIT_SIZE_PBA_TEMP				8
#define BIT_SIZE_DERATE_HEAT			1
#define BIT_SIZE_DERATE_SWTEMP			1
#define BIT_SIZE_DERATE_VDC				1
#define BIT_SIZE_DERATE_HWTEMP			1

#define BIT_MASK_TEMP_SENS2				(1<<BIT_SIZE_TEMP_SENS2)-1
#define BIT_MASK_TEMP_SENS3				(1<<BIT_SIZE_TEMP_SENS3)-1
#define BIT_MASK_WARNING00				(1<<BIT_SIZE_WARNING00)-1
#define BIT_MASK_WARNING01				(1<<BIT_SIZE_WARNING01)-1
#define BIT_MASK_WARNING02				(1<<BIT_SIZE_WARNING02)-1
#define BIT_MASK_WARNING03				(1<<BIT_SIZE_WARNING03)-1
#define BIT_MASK_WARNING04				(1<<BIT_SIZE_WARNING04)-1
#define BIT_MASK_WARNING05				(1<<BIT_SIZE_WARNING05)-1
#define BIT_MASK_WARNING06				(1<<BIT_SIZE_WARNING06)-1
#define BIT_MASK_WARNING07				(1<<BIT_SIZE_WARNING07)-1
#define BIT_MASK_LIMIT00				(1<<BIT_SIZE_LIMIT00)-1
#define BIT_MASK_LIMIT01				(1<<BIT_SIZE_LIMIT01)-1
#define BIT_MASK_LIMIT02				(1<<BIT_SIZE_LIMIT02)-1
#define BIT_MASK_LIMIT03				(1<<BIT_SIZE_LIMIT03)-1
#define BIT_MASK_LIMIT04				(1<<BIT_SIZE_LIMIT04)-1
#define BIT_MASK_LIMIT05				(1<<BIT_SIZE_LIMIT05)-1
#define BIT_MASK_LIMIT06				(1<<BIT_SIZE_LIMIT06)-1
#define BIT_MASK_LIMIT07				(1<<BIT_SIZE_LIMIT07)-1
#define BIT_MASK_INT_SPD_CMD			(1<<BIT_SIZE_INT_SPD_CMD)-1
#define BIT_MASK_TQ_EST					(1<<BIT_SIZE_TQ_EST)-1
#define BIT_MASK_FAULT32				(1<<BIT_SIZE_FAULT32)-1
#define BIT_MASK_FAULT33				(1<<BIT_SIZE_FAULT33)-1
#define BIT_MASK_FAULT34				(1<<BIT_SIZE_FAULT34)-1
#define BIT_MASK_FAULT35				(1<<BIT_SIZE_FAULT35)-1
#define BIT_MASK_FAULT36				(1<<BIT_SIZE_FAULT36)-1
#define BIT_MASK_FAULT37				(1<<BIT_SIZE_FAULT37)-1
#define BIT_MASK_FAULT38				(1<<BIT_SIZE_FAULT38)-1
#define BIT_MASK_FAULT39				(1<<BIT_SIZE_FAULT39)-1
#define BIT_MASK_PBA_TEMP				(1<<BIT_SIZE_PBA_TEMP)-1
#define BIT_MASK_DERATE_HEAT			(1<<BIT_SIZE_DERATE_HEAT)-1
#define BIT_MASK_DERATE_SWTEMP			(1<<BIT_SIZE_DERATE_SWTEMP)-1
#define BIT_MASK_DERATE_VDC				(1<<BIT_SIZE_DERATE_VDC)-1
#define BIT_MASK_DERATE_HWTEMP			(1<<BIT_SIZE_DERATE_HWTEMP)-1

#define TEMP_SENS2_STAT_PTR 			&GeCanTx_TempSens2_stat
#define TEMP_SENS3_STAT_PTR 			&GeCanTx_TempSens3_stat
#define INT_SPD_CMD_STAT_PTR			&GeCanTx_InternalSpeedCommand_stat
#define TQ_EST_STAT_PTR					&GeCanTx_TorqueEstCfg_stat
#define PBA_TEMP_STAT_PTR  				&GeCanTx_PBATemp_stat

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

IFX_EXTERN float GfCanTx_TempSens2;
IFX_EXTERN uint8 Gu8CanTx_TempSens2;
IFX_EXTERN esignal_stat GeCanTx_TempSens2_stat;

IFX_EXTERN float GfCanTx_TempSens3;
IFX_EXTERN uint8 Gu8CanTx_TempSens3;
IFX_EXTERN esignal_stat GeCanTx_TempSens3_stat;

IFX_EXTERN bool GbCanTx_Warning00;
IFX_EXTERN bool GbCanTx_Warning01;
IFX_EXTERN bool GbCanTx_Warning02;
IFX_EXTERN bool GbCanTx_Warning03;
IFX_EXTERN bool GbCanTx_Warning04;
IFX_EXTERN bool GbCanTx_Warning05;
IFX_EXTERN bool GbCanTx_Warning06;
IFX_EXTERN bool GbCanTx_Warning07;

IFX_EXTERN bool GbCanTx_Limit00;
IFX_EXTERN bool GbCanTx_Limit01;
IFX_EXTERN bool GbCanTx_Limit02;
IFX_EXTERN bool GbCanTx_Limit03;
IFX_EXTERN bool GbCanTx_Limit04;
IFX_EXTERN bool GbCanTx_Limit05;
IFX_EXTERN bool GbCanTx_Limit06;
IFX_EXTERN bool GbCanTx_Limit07;

IFX_EXTERN float GfCanTx_InternalSpeedCommand;
IFX_EXTERN uint8 Gu8CanTx_InternalSpeedCommand;
IFX_EXTERN esignal_stat GeCanTx_InternalSpeedCommand_stat;

IFX_EXTERN float GfCanTx_TorqueEstimate;
IFX_EXTERN uint8 Gu8CanTx_TorqueEstimate;
IFX_EXTERN esignal_stat GeCanTx_TorqueEstCfg_stat;

IFX_EXTERN uint8 Gu8CanTx_eTurboSerialNum;

IFX_EXTERN bool GbCanTx_Fault32;
IFX_EXTERN bool GbCanTx_Fault33;
IFX_EXTERN bool GbCanTx_Fault34;
IFX_EXTERN bool GbCanTx_Fault35;
IFX_EXTERN bool GbCanTx_Fault36;
IFX_EXTERN bool GbCanTx_Fault37;
IFX_EXTERN bool GbCanTx_Fault38;
IFX_EXTERN bool GbCanTx_Fault39;

IFX_EXTERN float GfCanTx_PBATemp;
IFX_EXTERN uint8 Gu8CanTx_PBATemp;
IFX_EXTERN esignal_stat GeCanTx_PBATemp_stat;

extern bool GbCANTx_DertgFlgSW_Heat;
extern bool GbCANTx_DertgFlgSW_Temp;
extern bool GbCANTx_DertgFlgSW_Vdc;
extern bool GbCANTx_DertgFlgHW_Temp;

#elif (CUSTOMER_ID == AUDI)

/*****************************MSG_18E14280h***************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/

#define MSG_ID_FDBK_2				0x18E14280

#define BIT_START_POS_IqCmd     	  0
#define BIT_START_POS_IdCmd      	  8
#define BIT_START_POS_IqFeedback      16
#define BIT_START_POS_IdFeedback      24
#define BIT_START_POS_SwVersion       32
#define BIT_START_POS_V05_UV	      48
#define BIT_START_POS_V12_UV    	  49
#define BIT_START_POS_V12_OV_SW       50
#define BIT_START_POS_V05_OV_SW       51
#define BIT_START_POS_Iabc_OC_HW      52
#define BIT_START_POS_Ia_OC_SW        53
#define BIT_START_POS_Ib_OC_SW        54
#define BIT_START_POS_Ic_OC_SW        55
#define BIT_START_POS_Speed_OL_CL     56

#define BIT_SIZE_IqCmd				8
#define BIT_SIZE_IdCmd				8
#define BIT_SIZE_IqFeedback			8
#define BIT_SIZE_IdFeedback			8
#define BIT_SIZE_SwVersion			8
#define BIT_SIZE_V05_UV				1
#define BIT_SIZE_V12_UV				1
#define BIT_SIZE_V12_OV_SW			1
#define BIT_SIZE_V05_OV_SW			1
#define BIT_SIZE_Iabc_OC_HW			1
#define BIT_SIZE_Ia_OC_SW			1
#define BIT_SIZE_Ib_OC_SW			1
#define BIT_SIZE_Ic_OC_SW			1
#define BIT_SIZE_Speed_OL_CL		1


#define BIT_MASK_VarNameTest		BIT_MASK(BIT_SIZE_IdCmd)

#define BIT_MASK_IqCmd				((1<<BIT_SIZE_IqCmd)-1)
#define BIT_MASK_IdCmd				((1<<BIT_SIZE_IdCmd)-1)
#define BIT_MASK_IqFeedback			((1<<BIT_SIZE_IdFeedback)-1)
#define BIT_MASK_IdFeedback			((1<<BIT_SIZE_IqFeedback)-1)
#define BIT_MASK_SwVersion			((1<<BIT_SIZE_SwVersion)-1)
#define BIT_MASK_V05_UV				((1<<BIT_SIZE_V05_UV)-1)
#define BIT_MASK_V12_UV				((1<<BIT_SIZE_V12_UV)-1)
#define BIT_MASK_V12_OV_SW			((1<<BIT_SIZE_V12_OV_SW)-1)
#define BIT_MASK_V05_OV_SW			((1<<BIT_SIZE_V05_OV_SW)-1)
#define BIT_MASK_Iabc_OC_HW			((1<<BIT_SIZE_Iabc_OC_HW)-1)
#define BIT_MASK_Ia_OC_SW			((1<<BIT_SIZE_Ia_OC_SW)-1)
#define BIT_MASK_Ib_OC_SW			((1<<BIT_SIZE_Ib_OC_SW)-1)
#define BIT_MASK_Ic_OC_SW			((1<<BIT_SIZE_Ic_OC_SW)-1)
#define BIT_MASK_Speed_OL_CL		((1<<BIT_SIZE_Speed_OL_CL)-1)


#define IqCmd_STAT_PTR				&GeCanTx_IqCmd_stat
#define IdCmd_STAT_PTR				&GeCanTx_IdCmd_stat
#define IqFeedback_STAT_PTR			&GeCanTx_IqFeedback_stat
#define IdFeedback_STAT_PTR			&GeCanTx_IdFeedback_stat
#define SwVersion_STAT_PTR			&GeCanTx_SwVersion_stat

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

IFX_EXTERN float GfCanTx_IqCmd;
IFX_EXTERN float GfCanTx_IdCmd;
IFX_EXTERN float GfCanTx_IqFeedback;
IFX_EXTERN float GfCanTx_IdFeedback;
IFX_EXTERN float GfCanTx_SwVersion;
IFX_EXTERN bool GbCanTx_V05_UV;
IFX_EXTERN bool GbCanTx_V12_UV;
IFX_EXTERN bool GbCanTx_V12_OV_SW;
IFX_EXTERN bool GbCanTx_V05_OV_SW;
IFX_EXTERN bool GbCanTx_Iabc_OC_HW;
IFX_EXTERN bool GbCanTx_Ia_OC_SW;
IFX_EXTERN bool GbCanTx_Ib_OC_SW;
IFX_EXTERN bool GbCanTx_Ic_OC_SW;
IFX_EXTERN bool GbCanTx_Speed_OL_CL;

#endif /* CUSTOMER_ID */


/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
IFX_EXTERN void Init_FDBK_2(void);
IFX_EXTERN void Transmit_FDBK_2(void);


#endif /* APP_CAN_FDBK_2_H_ */
