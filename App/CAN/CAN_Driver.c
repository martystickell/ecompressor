/**
 * \file MulticanBasicDemo.c
 * \brief Demo MulticanBasicDemo
 *
 * \version iLLD_Demos_1_0_1_3_0
 * \copyright Copyright (c) 2014 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 */

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/


#include <CAN_Driver.h>
#include <stdio.h>
#include "CAN_Interface_Tx.h"
#include "CAN_Interface_Rx.h"
#include "MCS.h"
#include "STM.h"
#include "CMD_0.h"
#include "CMD_1.h"
#include "CCC.h"
#include "FDBK_0.h"
#include "DerateProtFact.h"
#include "ThermProtn_Ecoe_2019a.h"

/******************************************************************************/
/*-----------------------------------Macros-----------------------------------*/
/******************************************************************************/

/******************************************************************************/
/*--------------------------------Enumerations--------------------------------*/
/******************************************************************************/

/******************************************************************************/
/*-----------------------------Data Structures--------------------------------*/
/******************************************************************************/

/******************************************************************************/
/*------------------------------Calibrations----------------------------------*/
/******************************************************************************/
boolean KbCAN_CANCmdUpdt = TRUE;
float KfCAN_MaxSpeedCmdCAN = 81000;

/******************************************************************************/
/*------------------------------Global variables------------------------------*/
/******************************************************************************/
App_MulticanBasic g_MulticanBasic; /**< \brief Demo information */

float GfCAN_CoolantTemp = 0.0;
float GfCAN_InletTemp = 0.0;

/******************************************************************************/
/*-------------------------Function Prototypes--------------------------------*/
/******************************************************************************/

/******************************************************************************/
/*------------------------Private Variables/Constants-------------------------*/
/******************************************************************************/


/******************************************************************************/
/*-------------------------Function Implementations---------------------------*/
/******************************************************************************/

void CAN_Driver_MessageObjectInit(IfxMultican_Can_MsgObj *msgObj, uint32 messageId, IfxMultican_Frame frame, boolean extendedFrame)
{
	IfxMultican_MsgObjId msgObjId = (msgObj - &g_MulticanBasic.drivers.can0DstMsgObj0)/sizeof(IfxMultican_Can_MsgObj);

    /* create message object config */
	IfxMultican_Can_MsgObjConfig canMsgObjConfig;
	IfxMultican_Can_MsgObj_initConfig(&canMsgObjConfig, &g_MulticanBasic.drivers.can0Node);

	canMsgObjConfig.msgObjId              = msgObjId;
	canMsgObjConfig.messageId             = messageId;
	canMsgObjConfig.acceptanceMask        = CAN_ACCEPTANCE_MASK;
	canMsgObjConfig.frame                 = frame;
	canMsgObjConfig.control.messageLen    = IfxMultican_DataLengthCode_8;
	canMsgObjConfig.control.extendedFrame = TRUE;
	canMsgObjConfig.control.matchingId    = TRUE;

	/* initialize message object */
	IfxMultican_Can_MsgObj_init(msgObj, &canMsgObjConfig);
}

/** \addtogroup IfxLld_Demo_MulticanBasic_SrcDoc_Main_Interrupt
 * \{ */

/** \} */

/** \brief Demo init API
 *
 * This function is called from main during initialization phase
 */

void CAN_Driver_Init(void)
{
    /* create module config */
    IfxMultican_Can_Config canConfig;

    IfxMultican_Can_initModuleConfig(&canConfig, &CAN_MODULE);

    /* initialize module */
    IfxMultican_Can_initModule(&g_MulticanBasic.drivers.can, &canConfig);

    /* create CAN node config */
    IfxMultican_Can_NodeConfig canNodeConfig;
    IfxMultican_Can_Node_initConfig(&canNodeConfig, &g_MulticanBasic.drivers.can);

    canNodeConfig.baudrate = CAN_BAUD_RATE;     /* 1 MBaud CAN0 Node configuration */
    {
        canNodeConfig.nodeId    = (IfxMultican_NodeId)((int)IfxMultican_NodeId_0);
        canNodeConfig.rxPin     = &RX_PIN;
        canNodeConfig.rxPinMode = IfxPort_InputMode_pullUp;
        canNodeConfig.txPin     = &TX_PIN;
        canNodeConfig.txPinMode = IfxPort_OutputMode_pushPull;
        IfxMultican_Can_Node_init(&g_MulticanBasic.drivers.can0Node, &canNodeConfig);
    }

	/* initialize Rx messages */
    Init_CMD_0();
    Init_CMD_1();
    Init_CMD_2();


    /* initialize Tx messages */
	Init_FDBK_0();
	Init_FDBK_1();
	Init_FDBK_2();
	Init_FDBK_3();
	Init_FDBK_4();
	Init_FDBK_5();
	Init_FDBK_6();
	Init_FDBK_7();
	Init_FDBK_8();
}

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

// CAN command update if flag is set
void CAN_CmdUpdate(void)
{
	if (KbCAN_CANCmdUpdt == TRUE)
	{
		// State Command Update
		if (GbCanRx_ResetFaults == TRUE)
		{
			GeSTM_StateCmd = StandBy;
		}
		else if ((GbCanRx_MotoringEnable == TRUE) || (GbCanRx_GeneratingEnable == TRUE) || (GbCanRx_TrackingEnable == TRUE))
		{
			GeSTM_StateCmd = Run;
		}
		else
		{
			GeSTM_StateCmd = StandBy;
		}

		if (GbCanRx_TorqueCmdMode == TRUE)
		{
			GeSTM_ControlModeCmd = TorqueControl;
		}
		else
		{
			GeSTM_ControlModeCmd = SpeedControl;
		}

		/* limit Speed Command to a max value */
		if(GfCanRx_SpeedCmd <= KfCAN_MaxSpeedCmdCAN)
		{
			GfCCC_WrpmCmdCAN = GfCanRx_SpeedCmd;
		}
		else
		{
			GfCCC_WrpmCmdCAN 	= KfCAN_MaxSpeedCmdCAN;
			Gu16CanRx_SpeedCmd	= MAX_SPEED_HEX_VAL_81K;
		}

		GfCCC_TeCmdCAN = (float)Gu16CanRx_SpeedCmd * KfCCC_TeCmdScale + KfCCC_TeCmdOffset;
		GfCCC_IsMaxGenCAN = GfCanRx_GeneratingCurLimit;
		GfCCC_IsMaxMotCAN = GfCanRx_MotoringCurLimit;
		GfCCC_WrpmCmdSlewDnCAN = GfCANRx_SlewRateDown;
		GfCCC_WrpmCmdSlewUpCAN = GfCANRx_SlewRateUp;

		// Torque command slew
		GfCCC_TrqCmdSlewDnCAN = (float)Gu8CanRx_SlewRateDown * KfCCC_TeCmdSlewScale + KfCCC_TeCmdSlewOffset;
		GfCCC_TrqCmdSlewUpCAN = (float)Gu8CanRx_SlewRateUp * KfCCC_TeCmdSlewScale + KfCCC_TeCmdSlewOffset;

		GfCCC_IdcMinCAN = -GfCanRx_DCOutputCurrLimit;	/* generating */
		GfCCC_PdcMinCAN = -GfCanRx_DCOutputPowerLimit;
		GfCCC_IdcMaxCAN = GfCanRx_DCInputCurrLimit;		/* motoring */
		GfCCC_PdcMaxCAN = GfCanRx_DCInputPowerLimit;

		// Coolant temperature update
		GfCAN_CoolantTemp = GfCanRx_WaterTemp;
		GfCAN_InletTemp = GfCanRx_CompInletTemp;
	}
	else
	{
		// State Command Update
		GeSTM_StateCmd = KeSTM_ManStateCmd;

		GeSTM_ControlModeCmd = KeSTM_ControlModeCmd;

		GfCCC_WrpmCmdCAN = KfCCC_WrpmCmd;
		GfCCC_TeCmdCAN = KfCCC_TeCmd;
		GfCCC_IsMaxGenCAN = KfCCC_IsMax;
		GfCCC_IsMaxMotCAN = KfCCC_IsMax;
		GfCCC_WrpmCmdSlewDnCAN = KfCCC_WrpmCmdSlewDn;
		GfCCC_WrpmCmdSlewUpCAN = KfCCC_WrpmCmdSlewUp;

		// Torque command slew
		GfCCC_TrqCmdSlewDnCAN = KfCCC_TrqCmdSlewDn;
		GfCCC_TrqCmdSlewUpCAN = KfCCC_TrqCmdSlewUp;

		GfCCC_IdcMinCAN = -KfCCC_IdcMaxGen;
		GfCCC_PdcMinCAN = -KfCCC_PdcMaxGen;
		GfCCC_IdcMaxCAN = KfCCC_IdcMaxMot;
		GfCCC_PdcMaxCAN = KfCCC_PdcMaxMot;
	}
}

void CAN_TxMsgUpdate(void)
{
	GfCanTx_SpeedFeedback = GfCCC_Wrpm;
	GfCanTx_BusCurrent = GfCCC_IdcEst;
	GeCanTx_OpertingMode = GeSTM_CurrentStateT1;
	Gu8CanTx_SWVersion = (uint8)Ku16STM_SWVersion;
	GfCanTx_BusVoltage = GfMES_Vdc;
	GfCanTx_InvTemp1 = LARGEST(GfCanTx_TempSens1, GfCanTx_TempSens2, GfCanTx_TempSens3);
	GfCanTx_IqCommand = GfCCC_IqCmd;
	GfCanTx_IdCommand = GfCCC_IdCmd;
	GfCanTx_IqFeedback = GfMES_Iqr;
	GfCanTx_IdFeedback = GfMES_Idr;
	GfCanTx_ACMotorVoltageVAC = GfCCC_MagVdqCmdNoLim;
	GfCanTx_TorqueEstimate = GfCCC_TeCmd;
	GfCanTx_MOSFETTempEstimate = 0.0;
	GfCanTx_Derating = GfCCC_DerateMin;

	GbCANTx_FaultELECFAILURE = GbSTM_IaOCSW | GbSTM_IbOCSW | GbSTM_IcOCSW | GbSTM_IabcOCHW | GbSTM_VdcOVHW;
	GbCANTx_FaultCANTIMEOUT = GbSTM_CANCMD0TimeOutSW;
	GbCANTx_FaultUT = GbDPF_LowTempIMS_DerateAct | GbDPF_LowTempBrdDerateAct | GbDPF_ExtCoolLowTempDerateAct;

	GbCANTx_DertgFlgSW_Vdc = GbDPF_High48vDerateAct | GbDPF_Low48vDerateAct;
	GbCANTx_DertgFlgHW_Temp  = GbDPF_LowTempIMS_DerateAct | GbDPF_HighTempIMS_DerateAct | GbDPF_LowTempBrdDerateAct |
			                  GbDPF_ExtCoolHighTempDerateAct | GbDPF_ExtCoolLowTempDerateAct | GbDPF_InletTempDerateAct;

	GbCanTx_Warning00 = GbSTM_V12_WarnLo; //GbSTM_V12UVSW;
	GbCanTx_Warning01 = GbSTM_V12_WarnHi; //GbSTM_V12OVSW;
	GbCanTx_Warning02 = GbSTM_VdcWarnLo; //GbSTM_VdcUVSW;
	GbCanTx_Warning03 = GbSTM_VdcWarnHi;//GbSTM_VdcOVSW | GbSTM_VdcOVHW;
	GbCanTx_Warning04 = GbSTM_BrdTempWarn | GbSTM_TempIMS_Warn | GbSTM_ExtCoolTempWarn | GbSTM_InletTempWarn;

	GbCANTx_DertgFlgSW_Heat = GbCCC_DerateThermEstActv;
	GfCanTx_TpMosfet     = rtY.TpMosfet;
	GfCanTx_IndcrPwrResi = rtY.IndcrPwrResi;
	GfCanTx_TToEmpty     = rtY.TToEmpty;
	GfCanTx_TToFull      = rtY.TToFull;
	GfCanTx_PwrIntglThd  = rtP.PwrIntglLowThd;
}

#elif (CUSTOMER_ID == AUDI)

// CAN command update if flag is set
void CAN_CmdUpdate(void)
{
	if (KbCAN_CANCmdUpdt == TRUE)
	{
		// State Command Update
		switch (GeSTM_CurrentStateT1)
		{
			case StandBy:
				switch(GeCanRx_eCState)
				{
					case eCStateStandby:
						GeSTM_StateCmd = StandBy;
						break;
					case eCStateActive:
						GeSTM_StateCmd = (GbCanRx_TerminalKL15Status == TRUE) ? Run : StandBy;
						break;
					case eCStateInit:
						GeSTM_StateCmd = StandBy;
						break;
					default:
						GeSTM_StateCmd = StandBy;
						break;
				}
				break;
			case Run:
				switch(GeCanRx_eCState)
				{
					case eCStateStandby:
						GeSTM_StateCmd = StandBy;
						break;
					case eCStateActive:
						GeSTM_StateCmd = (GbCanRx_TerminalKL15Status == TRUE) ? Run : StandBy;
						break;
					case eCStateInit:
						GeSTM_StateCmd = StandBy;
						break;
					default:
						GeSTM_StateCmd = StandBy;
						break;
				}
				break;

			case Fault:
				switch(GeCanRx_eCState)
				{
					case eCStateStandby:
						GeSTM_StateCmd = (GbCanRx_TerminalKL15Status == TRUE) ? Fault : StandBy;
						break;
					case eCStateActive:
						if (GfCanRx_SpeedCmd == 0)
						{
							// Reset SW faults
							STM_ResetT0Flt();
							STM_ResetT1Flt();

							GeSTM_StateCmd = StandBy;
						}
						break;
					case eCStateInit:
						GeSTM_StateCmd = (GbCanRx_TerminalKL15Status == TRUE) ? Fault : StandBy;
						break;
					default:
						GeSTM_StateCmd = Fault;
						break;
				}
				break;
			default:
				GeSTM_StateCmd = StandBy;
				break;
		}


		GfCCC_WrpmCmdCAN = GfCanRx_SpeedCmd;
		GeSTM_ControlModeCmd = SpeedControl;

		GfCCC_TeCmdCAN = KfCCC_TeCmd;
		GfCCC_IsMaxGenCAN = KfCCC_IsMax;
		GfCCC_IsMaxMotCAN = KfCCC_IsMax;
		GfCCC_WrpmCmdSlewDnCAN = KfCCC_WrpmCmdSlewDn;
		GfCCC_WrpmCmdSlewUpCAN = KfCCC_WrpmCmdSlewUp;

		// Torque command slew
		GfCCC_TrqCmdSlewDnCAN = KfCCC_TrqCmdSlewDn;
		GfCCC_TrqCmdSlewUpCAN = KfCCC_TrqCmdSlewUp;

		GfCCC_IdcMinCAN = -KfCCC_IdcMaxGen;
		GfCCC_PdcMinCAN = -KfCCC_PdcMaxGen;
		GfCCC_IdcMaxCAN = KfCCC_IdcMaxMot;
		GfCCC_PdcMaxCAN = KfCCC_PdcMaxMot;
	}
	else
	{
		// State Command Update
		GeSTM_StateCmd = KeSTM_ManStateCmd;

		GeSTM_ControlModeCmd = KeSTM_ControlModeCmd;

		GfCCC_WrpmCmdCAN = KfCCC_WrpmCmd;
		GfCCC_TeCmdCAN = KfCCC_TeCmd;
		GfCCC_IsMaxGenCAN = KfCCC_IsMax;
		GfCCC_IsMaxMotCAN = KfCCC_IsMax;
		GfCCC_WrpmCmdSlewDnCAN = KfCCC_WrpmCmdSlewDn;
		GfCCC_WrpmCmdSlewUpCAN = KfCCC_WrpmCmdSlewUp;

		// Torque command slew
		GfCCC_TrqCmdSlewDnCAN = KfCCC_TrqCmdSlewDn;
		GfCCC_TrqCmdSlewUpCAN = KfCCC_TrqCmdSlewUp;

		GfCCC_IdcMinCAN = -KfCCC_IdcMaxGen;
		GfCCC_PdcMinCAN = -KfCCC_PdcMaxGen;
		GfCCC_IdcMaxCAN = KfCCC_IdcMaxMot;
		GfCCC_PdcMaxCAN = KfCCC_PdcMaxMot;
	}
}

void CAN_TxMsgUpdate(void)
{
	static uint16 u16CAN_TxCount=0;
	boolean		  bStatus;

	// Limit count to prevent rollover
	if (u16CAN_TxCount < CAN_STATUS_TEST_NOT_COMPLETED_MAX_COUNT)
	{
		u16CAN_TxCount++;
	}


	//******************************************
	//* ElCmpr_02_XIX_Motor_SUBCAN - 0x0E3 *
	//******************************************
	GfCanTx_SpeedActual = GfCCC_Wrpm;
	GfCanTx_DcCurrent = GfCCC_IdcEst;
	GfCanTx_DcVoltage = GfMES_Vdc;


	//******************************************
	//* ElCmpr_01_XIX_Motor_SUBCAN - 0xAF95444 *
	//******************************************

	// enum State descriptions:
	//		Off		-
	//		TurnOn	- Starting/Waking up procedure
	//		StandBy	- Ready for Speed CMD
	//		Run		- Speed Control Runs in Open Loop control
	//		TurnOff	- Post run/ shut down procedure
	//		Fault	- Fault state
	switch (GeSTM_CurrentStateT1)
	{
		case Off:
			GeCanTx_eCState = eCStateInit;
			break;
		case TurnOn:
			GeCanTx_eCState = eCStateStandby;
			break;
		case StandBy:
			GeCanTx_eCState = eCStateStandby;
			break;
		case Run:
			GeCanTx_eCState = eCStateActive;
			break;
		case TurnOff:
			GeCanTx_eCState = eCStateInit;
			break;
		case Fault:
			GeCanTx_eCState = eCStateFailure;
			break;
		default:
			GeCanTx_eCState = eCStateInit;
			break;
	}


	// Electric Failure
	bStatus = (GbSTM_IaOCSW | GbSTM_IbOCSW | GbSTM_IcOCSW | GbSTM_IabcOCHW | GbSTM_VdcOVHW | GbSTM_V12OVSW | GbSTM_V12UVSW | GbSTM_VdcOVSW | GbSTM_VdcUVSW);
	GeCanTx_EleFailure = GetTestStatus(bStatus, u16CAN_TxCount);

	// Mechanical Error - Always sending 0 (No fault)
	GeCanTx_MechFailure = GetTestStatus(0, u16CAN_TxCount);

	// Can Timeout
	bStatus = (GbSTM_CANCMD1TimeOutSW | GbSTM_CANCMD0TimeOutSW);
	GeCanTx_CanToutFailure = GetTestStatus(bStatus, u16CAN_TxCount);

	// Over Temperature
	bStatus = (GbSTM_BrdTempWarn | GbSTM_TempIMS_Warn | GbSTM_ExtCoolTempWarn | GbSTM_InletTempWarn);
	GeCanTx_OverTempFailure = GetTestStatus(bStatus, u16CAN_TxCount);

	// Over Voltage
	bStatus = (GbSTM_VdcOVHW | GbSTM_VdcOVSW | GbSTM_VdcWarnHi | GbSTM_V12OVSW);
	GeCanTx_OverVoltFailure = GetTestStatus(bStatus, u16CAN_TxCount);

	// Under Voltage
	bStatus = (GbSTM_VdcUVSW | GbSTM_VdcWarnLo | GbSTM_V12UVSW);
	GeCanTx_UnderVoltFailure = GetTestStatus(bStatus, u16CAN_TxCount);

	// IMS Temperature
	GfCanTx_Temperature = GfMES_TempIMS;

	GbCanTx_DertgFlgSW_Heat = GbCCC_DerateThermEstActv;
	GfCanTx_TpMosfet     = rtY.TpMosfet;
	GfCanTx_IndcrPwrResi = rtY.IndcrPwrResi;
	GfCanTx_TToEmpty     = rtY.TToEmpty;
	GfCanTx_TToFull      = rtY.TToFull;
}

#endif /* CUSTOMER_ID */
