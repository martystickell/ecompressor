/*
 * CMD_0.h
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */

#ifndef APP_CAN_CMD_0_H_
#define APP_CAN_CMD_0_H_

#include "CAN_Interface_Rx.h"
#include "CCC.h"
#include <string.h>
#include "CAN_Driver.h"
#include "CAN_Defs.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

/*****************************MSG_06Dh********************************************/
/*********************************************************************************/
/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/

#define MSG_ID_CMD_0				0x6D
#define BIT_START_POS_SPD_CMD		16
#define BIT_SIZE_SPD_CMD			12
#define BIT_MASK_SPD_CMD			((1<<(BIT_SIZE_SPD_CMD))-1)

#define BIT_START_POS_CMD_BIT0		32
#define BIT_SIZE_CMD_BIT0			1
#define BIT_MASK_CMD_BIT0			((1<<(BIT_SIZE_CMD_BIT0))-1)

#define BIT_START_POS_CMD_BIT1		33
#define BIT_SIZE_CMD_BIT1			1
#define BIT_MASK_CMD_BIT1			((1<<(BIT_SIZE_CMD_BIT1))-1)

#define BIT_START_POS_CMD_BIT2		34
#define BIT_SIZE_CMD_BIT2			1
#define BIT_MASK_CMD_BIT2			((1<<(BIT_SIZE_CMD_BIT2))-1)

#define BIT_START_POS_CMD_BIT3		35
#define BIT_SIZE_CMD_BIT3			1
#define BIT_MASK_CMD_BIT3			((1<<(BIT_SIZE_CMD_BIT3))-1)

#define BIT_START_POS_CMD_BIT4		36
#define BIT_SIZE_CMD_BIT4			1
#define BIT_MASK_CMD_BIT4			((1<<(BIT_SIZE_CMD_BIT4))-1)

#define BIT_START_POS_CMD_BIT5		37
#define BIT_SIZE_CMD_BIT5			1
#define BIT_MASK_CMD_BIT5			((1<<(BIT_SIZE_CMD_BIT5))-1)

#define BIT_START_POS_CMD_BIT6		38
#define BIT_SIZE_CMD_BIT6			1
#define BIT_MASK_CMD_BIT6			((1<<(BIT_SIZE_CMD_BIT6))-1)

#define BIT_START_POS_CMD_BIT7		39
#define BIT_SIZE_CMD_BIT7			1
#define BIT_MASK_CMD_BIT7			((1<<(BIT_SIZE_CMD_BIT7))-1)


#define SPD_CMD_STAT_PTR			&GeCanRx_SpeedCmd_stat

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

IFX_EXTERN float GfCanRx_SpeedCmd;
IFX_EXTERN uint16 Gu16CanRx_SpeedCmd;
IFX_EXTERN esignal_stat GeCanRx_SpeedCmd_stat;

IFX_EXTERN bool GbCanRx_MotoringEnable;
IFX_EXTERN bool GbCanRx_GeneratingEnable;
IFX_EXTERN bool GbCanRx_ResetFaults;
IFX_EXTERN bool GbCanRx_TrackingEnable;
IFX_EXTERN bool GbCanRx_TorqueCmdMode;
IFX_EXTERN bool GbCanRx_MotoringGeneratingEnable;

#elif (CUSTOMER_ID == AUDI)

/*****************************MSG_AF95445h********************************************/
/*********************************************************************************/
/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/
#define MSG_ID_CMD_0				0xAF95445

#define BIT_START_POS_SPD_CMD		12
#define BIT_START_POS_eC_STATE		28
#define BIT_MASK_SPD_CMD			((1<<BIT_SIZE_SPD_CMD)-1)

#define BIT_SIZE_SPD_CMD			16
#define BIT_SIZE_eC_STATE			2
#define BIT_MASK_eC_STATE			((1<<BIT_SIZE_eC_STATE)-1)


#define SPD_CMD_STAT_PTR			&GeCanRx_SpeedCmd_stat

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
IFX_EXTERN float GfCanRx_SpeedCmd;
IFX_EXTERN uint16 Gu16CanRx_SpeedCmd;
IFX_EXTERN esignal_stat GeCanRx_SpeedCmd_stat;

IFX_EXTERN eCState GeCanRx_eCState;

#endif /* CUSTOMER_ID */


IFX_EXTERN uint16 Gu16CANRx_CMD0TimeOutCtr;
IFX_EXTERN bool GbCANRx_CMD0TimeOutSWT2;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

IFX_EXTERN void Init_CMD_0(void);
IFX_EXTERN void Receive_CMD_0(void);

#endif /* APP_CAN_CMD_0_H_ */
