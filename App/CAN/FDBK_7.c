/*
 * FDBK_7.c
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */

/*****************************MSG_18E14290h***************************************/
/*********************************************************************************/
#include "FDBK_7.h"
#include "GTM.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

static uint64 FDBK_7_data;

/*--------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
#if (FDBK_7_SUPPORT_SIGNED_VAL == 1)
	const signalCfg_t kst_IqCmd 		= {0.250f, 	-512.0f, 508.0f, 0.0f, IQ_CMD_STAT_PTR};
	const signalCfg_t kst_IdCmd			= {0.250f, 	-512.0f, 508.0f, 0.0f, ID_CMD_STAT_PTR};
	const signalCfg_t kst_IqFdbk 		= {0.250f, 	-512.0f, 508.0f, 0.0f, IQ_FDBK_STAT_PTR};
	const signalCfg_t kst_IdFdbk		= {0.250f, 	-512.0f, 508.0f, 0.0f, ID_FDBK_STAT_PTR};
	const signalCfg_t kst_ACMtrVtg 		= {1.000f, 	-128.0f, 126.0f, 0.0f, AC_MTR_VLTG_STAT_PTR};
	const signalCfg_t kst_PWMFreq		= {2.50f, 	-51.20f, 50.80f, 0.0f, PWM_FREQ_STAT_PTR};
	const signalCfg_t kst_MOSFETTempEst	= {0.333f, 	-384.0f, 381.0f, 0.0f, MOSFET_TEMP_EST_STAT_PTR};

	sint8 Gs8CanTx_IqCommand;
	sint8 Gs8CanTx_IdCommand;
	sint8 Gs8CanTx_IqFeedback;
	sint8 Gs8CanTx_IdFeedback;
	sint8 Gs8CanTx_ACMotorVoltageVAC;
	sint8 Gs8CanTx_PWMFrequency;
	sint8 Gs8CanTx_MOSFETTempEstimate;

#else
	const signalCfg_t kst_IqCmd 		= {0.250f, 	-508.0f, 	508.0f, 508.0f,	IQ_CMD_STAT_PTR};
	const signalCfg_t kst_IdCmd			= {0.250f, 	-508.0f, 	508.0f, 508.0f,	ID_CMD_STAT_PTR};
	const signalCfg_t kst_IqFdbk 		= {0.250f, 	-508.0f, 	508.0f, 508.0f,	IQ_FDBK_STAT_PTR};
	const signalCfg_t kst_IdFdbk		= {0.250f, 	-508.0f, 	508.0f, 508.0f,	ID_FDBK_STAT_PTR};
	const signalCfg_t kst_ACMtrVtg 		= {1.000f, 	-127.0f, 	127.0f, 127.0f,	AC_MTR_VLTG_STAT_PTR};
	const signalCfg_t kst_PWMFreq		= {2.50f, 	 0.0f,   	50.80f,	0.000f,	PWM_FREQ_STAT_PTR};
	const signalCfg_t kst_MOSFETTempEst	= {0.25f, 	-254.0f, 	254.0f, 254.0f,	MOSFET_TEMP_EST_STAT_PTR};

	uint8 Gu8CanTx_IqCommand;
	uint8 Gu8CanTx_IdCommand;
	uint8 Gu8CanTx_IqFeedback;
	uint8 Gu8CanTx_IdFeedback;
	uint8 Gu8CanTx_ACMotorVoltageVAC;
	uint8 Gu8CanTx_PWMFrequency;
	uint8 Gu8CanTx_MOSFETTempEstimate;

#endif

esignal_stat GeCanTx_IqCommand_stat;
esignal_stat GeCanTx_IdCommand_stat;
esignal_stat GeCanTx_IqFdbkCfg_stat;
esignal_stat GeCanTx_IdFdbkCfg_stat;
esignal_stat GeCanTx_ACMtrVtgCfg_stat;
esignal_stat GeCanTx_PWMFreqCfg_stat;
esignal_stat GeCanTx_MOSFETTempEstCfg_stat;

float GfCanTx_IqCommand;
float GfCanTx_IdCommand;
float GfCanTx_IqFeedback;
float GfCanTx_IdFeedback;
float GfCanTx_ACMotorVoltageVAC;
float GfCanTx_PWMFrequency;
float GfCanTx_MOSFETTempEstimate;

uint8_t Gu8CanTx_eTurboSerialNum;

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/
signalCfg_t GstCanTx_IqCmdCfg;
signalCfg_t GstCanTx_IdCmdCfg;
signalCfg_t GstCanTx_IqFdbkCfg;
signalCfg_t GstCanTx_IdFdbkCfg;
signalCfg_t GstCanTx_ACMtrVtgCfg;
signalCfg_t GstCanTx_PWMFreqCfg;
signalCfg_t GstCanTx_MOSFETTempEstCfg;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
void Init_FDBK_7(void)
{
    CAN_Driver_MessageObjectInit(&g_MulticanBasic.drivers.can0SrcMsgObj7, MSG_ID_FDBK_7, IfxMultican_Frame_transmit, TRUE);

    GstCanTx_IqCmdCfg 			= kst_IqCmd;
	GstCanTx_IdCmdCfg 			= kst_IdCmd;
	GstCanTx_IqFdbkCfg 			= kst_IqFdbk;
	GstCanTx_IdFdbkCfg			= kst_IdFdbk;
	GstCanTx_ACMtrVtgCfg	 	= kst_ACMtrVtg;
	GstCanTx_PWMFreqCfg			= kst_PWMFreq;
	GstCanTx_MOSFETTempEstCfg	= kst_MOSFETTempEst;
	Gu8CanTx_eTurboSerialNum	= DEFAULT_ETURBO_SN;

	GeCanTx_IqCommand_stat = OK;
	GeCanTx_IdCommand_stat = OK;
	GeCanTx_IqFdbkCfg_stat = OK;
	GeCanTx_IdFdbkCfg_stat = OK;
	GeCanTx_ACMtrVtgCfg_stat = OK;
	GeCanTx_PWMFreqCfg_stat = OK;
	GeCanTx_MOSFETTempEstCfg_stat = OK;
}

static void Process_FDBK_7(void)
{
	memset(&FDBK_7_data, 0, sizeof(uint64));

#if (FDBK_7_SUPPORT_SIGNED_VAL == 1)

	Process_TxSignal_s8(GfCanTx_IqCommand, 			GstCanTx_IqCmdCfg, 			&Gs8CanTx_IqCommand);
	FDBK_7_data |= FILL(Gs8CanTx_IqCommand, 		BIT_MASK_IQ_CMD, 			BIT_START_POS_IQ_CMD);

	Process_TxSignal_s8(GfCanTx_IdCommand, 			GstCanTx_IdCmdCfg, 			&Gs8CanTx_IdCommand);
	FDBK_7_data |= FILL(Gs8CanTx_IdCommand, 		BIT_MASK_ID_CMD, 			BIT_START_POS_ID_CMD);

	Process_TxSignal_s8(GfCanTx_IqFeedback, 			GstCanTx_IqFdbkCfg, 		&Gs8CanTx_IqFeedback);
	FDBK_7_data |= FILL(Gs8CanTx_IqFeedback, 		BIT_MASK_IQ_FDBK, 			BIT_START_POS_IQ_FDBK);

	Process_TxSignal_s8(GfCanTx_IdFeedback, 			GstCanTx_IdFdbkCfg, 		&Gs8CanTx_IdFeedback);
	FDBK_7_data |= FILL(Gs8CanTx_IdFeedback, 		BIT_MASK_ID_FDBK, 			BIT_START_POS_ID_FDBK);

	Process_TxSignal_s8(GfCanTx_ACMotorVoltageVAC, 	GstCanTx_ACMtrVtgCfg, 		&Gs8CanTx_ACMotorVoltageVAC);
	FDBK_7_data |= FILL(Gs8CanTx_ACMotorVoltageVAC, BIT_MASK_AC_MTR_VLTG, 		BIT_START_POS_AC_MTR_VLTG);

	Process_TxSignal_s8(GfCanTx_TorqueEstimate, 	GstCanTx_TorqueEstCfg, 		&Gs8CanTx_TorqueEstimate);
	FDBK_7_data |= FILL(Gs8CanTx_TorqueEstimate, 	BIT_MASK_TQ_EST, 			BIT_START_POS_TQ_EST);

	Process_TxSignal_s8(GfCanTx_PWMFrequency, 		GstCanTx_PWMFreqCfg, 		&Gs8CanTx_PWMFrequency);
	FDBK_7_data |= FILL(Gs8CanTx_PWMFrequency, 		BIT_MASK_PWM_FREQ, 			BIT_START_POS_PWM_FREQ);

	Process_TxSignal_s8(GfCanTx_MOSFETTempEstimate,	GstCanTx_MOSFETTempEstCfg, 	&Gs8CanTx_MOSFETTempEstimate);
	FDBK_7_data |= FILL(Gs8CanTx_MOSFETTempEstimate,BIT_MASK_MOSFET_TEMP_EST, 	BIT_START_POS_MOSFET_TEMP_EST);

#else

	Process_TxSignal_u8(GfCanTx_IqCommand, 			GstCanTx_IqCmdCfg, 			&Gu8CanTx_IqCommand);
	FDBK_7_data |= FILL(Gu8CanTx_IqCommand, 		BIT_MASK_IQ_CMD, 			BIT_START_POS_IQ_CMD);

	Process_TxSignal_u8(GfCanTx_IdCommand, 			GstCanTx_IdCmdCfg, 			&Gu8CanTx_IdCommand);
	FDBK_7_data |= FILL(Gu8CanTx_IdCommand, 		BIT_MASK_ID_CMD, 			BIT_START_POS_ID_CMD);

	Process_TxSignal_u8(GfCanTx_IqFeedback, 		GstCanTx_IqFdbkCfg, 		&Gu8CanTx_IqFeedback);
	FDBK_7_data |= FILL(Gu8CanTx_IqFeedback, 		BIT_MASK_IQ_FDBK, 			BIT_START_POS_IQ_FDBK);

	Process_TxSignal_u8(GfCanTx_IdFeedback, 		GstCanTx_IdFdbkCfg, 		&Gu8CanTx_IdFeedback);
	FDBK_7_data |= FILL(Gu8CanTx_IdFeedback, 		BIT_MASK_ID_FDBK, 			BIT_START_POS_ID_FDBK);

	Process_TxSignal_u8(GfCanTx_ACMotorVoltageVAC, 	GstCanTx_ACMtrVtgCfg, 		&Gu8CanTx_ACMotorVoltageVAC);
	FDBK_7_data |= FILL(Gu8CanTx_ACMotorVoltageVAC, BIT_MASK_AC_MTR_VLTG, 		BIT_START_POS_AC_MTR_VLTG);

	FDBK_7_data |= FILL(Gu8CanTx_eTurboSerialNum,	BIT_MASK_ETURBO_SN_NUM, 	BIT_START_POS_ETURBO_SN_NUM);

	if(GfGTM_T0 > 0.0f)
	{
		GfCanTx_PWMFrequency = ((1/GfGTM_T0) * HZ_TO_KHZ);
	}
	else
	{
		GfCanTx_PWMFrequency = 0;
	}

	Process_TxSignal_u8(GfCanTx_PWMFrequency, 		GstCanTx_PWMFreqCfg, 		&Gu8CanTx_PWMFrequency);
	FDBK_7_data |= FILL(Gu8CanTx_PWMFrequency, 		BIT_MASK_PWM_FREQ, 			BIT_START_POS_PWM_FREQ);

	Process_TxSignal_u8(GfCanTx_MOSFETTempEstimate,	GstCanTx_MOSFETTempEstCfg, 	&Gu8CanTx_MOSFETTempEstimate);
	FDBK_7_data |= FILL(Gu8CanTx_MOSFETTempEstimate,BIT_MASK_MOSFET_TEMP_EST, 	BIT_START_POS_MOSFET_TEMP_EST);
#endif

	GeCanTxLink_MsgStat |= *(GstCanTx_IqCmdCfg.stat_ptr);
	GeCanTxLink_MsgStat |= *(GstCanTx_IdCmdCfg.stat_ptr);
	GeCanTxLink_MsgStat |= *(GstCanTx_IqFdbkCfg.stat_ptr);
	GeCanTxLink_MsgStat |= *(GstCanTx_IdFdbkCfg.stat_ptr);
	GeCanTxLink_MsgStat |= *(GstCanTx_ACMtrVtgCfg.stat_ptr);
	GeCanTxLink_MsgStat |= *(GstCanTx_PWMFreqCfg.stat_ptr);
	GeCanTxLink_MsgStat |= *(GstCanTx_MOSFETTempEstCfg.stat_ptr);

}

void Transmit_FDBK_7(void)
{
	uint32 dataLowTx  = 0x00000000;
	uint32 dataHighTx = 0x00000000;
	uint64 datawordTx = 0;
	IfxMultican_Message msg_buf;

	Process_FDBK_7();

	memcpy(&datawordTx, &FDBK_7_data, sizeof(uint64));

	dataLowTx 	= (uint32)datawordTx;
	dataHighTx 	= (uint64)datawordTx >> 32;

	IfxMultican_Message_init(&msg_buf, MSG_ID_FDBK_7, dataLowTx , dataHighTx, IfxMultican_DataLengthCode_8);

	IfxMultican_Can_MsgObj_sendMessage(&g_MulticanBasic.drivers.can0SrcMsgObj7, &msg_buf);
}

#else

void Init_FDBK_7(void)
{
	/* Do nothing */
}

void Transmit_FDBK_7(void)
{
	/* Do nothing */
}

#endif /* CUSTOMER_ID */
