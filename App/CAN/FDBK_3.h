/*
 * FDBK_3.h
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */

#ifndef APP_CAN_FDBK_3_H_
#define APP_CAN_FDBK_3_H_

#include <Ifx_Types.h>
#include <Ifx_TypesReg.h>
#include <string.h>
#include "CAN_Driver.h"
#include "CAN_Interface_Tx.h"
#include "STM.h"
#include "MES.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)
/*****************************MSG_18E14280h***************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/

#define MSG_ID_FDBK_3				0x18E14280

#define BIT_MASK_TToFull			(1<<BIT_SIZE_TToFull)-1
#define BIT_MASK_IndcrPwrResi		(1<<BIT_SIZE_IndcrPwrResi)-1
#define BIT_MASK_TpMosfet			(1<<BIT_SIZE_TpMosfet)-1
#define BIT_MASK_PwrIntglThd		(1<<BIT_SIZE_PwrIntglThd)-1
#define BIT_MASK_TToEmpty			(1<<BIT_SIZE_TToEmpty)-1

#define BIT_SIZE_TToFull			8
#define BIT_SIZE_IndcrPwrResi		8
#define BIT_SIZE_TpMosfet			9
#define BIT_SIZE_PwrIntglThd   		10
#define BIT_SIZE_TToEmpty			8

#define BIT_START_POS_TToFull		0
#define BIT_START_POS_IndcrPwrResi	8
#define BIT_START_POS_TpMosfet      16
#define BIT_START_POS_PwrIntglThd   25
#define BIT_START_POS_TToEmpty      35


#define TTOFULL_STAT_PTR 			&GeCanTx_TToFull_stat
#define INDCTRPWRRESI_STAT_PTR 		&GeCanTx_IndcrPwrResi_stat
#define TPMOSFET_STAT_PTR			&GeCanTx_TpMosfet_stat
#define PWRINTGLTHD_STAT_PTR		&GeCanTx_PwrIntglThd_stat
#define TTOEMPTY_STAT_PTR			&GeCanTx_TToEmpty_stat
/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

IFX_EXTERN float GfCanTx_TToFull;
IFX_EXTERN float GfCanTx_IndcrPwrResi;
IFX_EXTERN float GfCanTx_TpMosfet;
IFX_EXTERN float GfCanTx_PwrIntglThd;
IFX_EXTERN float GfCanTx_TToEmpty;

#elif (CUSTOMER_ID == AUDI)

/*****************************MSG_18E14288h***************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/
#define MSG_ID_FDBK_3				0x18E14288

#define BIT_START_POS_TEMP_BDR2		0
#define BIT_START_POS_TEMP_BDR1		8
#define BIT_START_POS_TToFull		24
#define BIT_START_POS_IndcrPwrResi	32
#define BIT_START_POS_TToEmpty      56


#define BIT_SIZE_TEMP_BDR2			7
#define BIT_SIZE_TEMP_BDR1			7
#define BIT_SIZE_TToFull			8
#define BIT_SIZE_IndcrPwrResi		8
#define BIT_SIZE_TToEmpty			8


#define BIT_MASK_TEMP_BDR1			((1<<BIT_SIZE_TEMP_BDR1)-1)
#define BIT_MASK_TEMP_BDR2			((1<<BIT_SIZE_TEMP_BDR2)-1)
#define BIT_MASK_TToFull			((1<<BIT_SIZE_TToFull)-1)
#define BIT_MASK_IndcrPwrResi		((1<<BIT_SIZE_IndcrPwrResi)-1)
#define BIT_MASK_TToEmpty			((1<<BIT_SIZE_TToEmpty)-1)


#define TEMP_BDR2_STAT_PTR 			&GeCanTx_TempBdr2_stat
#define TEMP_BDR1_STAT_PTR 			&GeCanTx_TempBdr1_stat
#define TTOFULL_STAT_PTR 			&GeCanTx_TToFull_stat
#define INDCTRPWRRESI_STAT_PTR 		&GeCanTx_IndcrPwrResi_stat
#define TTOEMPTY_STAT_PTR			&GeCanTx_TToEmpty_stat

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

IFX_EXTERN float GfCanTx_TempBdr2;
IFX_EXTERN float GfCanTx_TempBdr1;
IFX_EXTERN float GfCanTx_TToFull;
IFX_EXTERN float GfCanTx_IndcrPwrResi;
IFX_EXTERN float GfCanTx_TToEmpty;

#endif /* CUSTOMER_ID */

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

IFX_EXTERN void Init_FDBK_3(void);
IFX_EXTERN void Transmit_FDBK_3(void);


#endif /* APP_CAN_FDBK_3_H_ */
