/*
 * FDBK_0.c
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */
#include "FDBK_0.h"

static uint64 FDBK_0_data;

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

/*****************************MSG_06Bh********************************************/
/*********************************************************************************/
bool GbCanTx_Fault00;
bool GbCanTx_Fault01;
bool GbCanTx_Fault02;
bool GbCanTx_Fault03;
bool GbCanTx_Fault04;
bool GbCanTx_Fault05;
bool GbCanTx_Fault06;
bool GbCanTx_Fault07;
bool GbCanTx_Fault08;
bool GbCanTx_Fault09;
bool GbCanTx_Fault10;
bool GbCanTx_Fault11;
bool GbCanTx_Fault12;
bool GbCanTx_Fault13;
bool GbCanTx_Fault14;
bool GbCanTx_Fault15;
bool GbCanTx_Fault16;
bool GbCanTx_Fault17;
bool GbCanTx_Fault18;
bool GbCanTx_Fault19;
bool GbCanTx_Fault20;
bool GbCanTx_Fault21;
bool GbCanTx_Fault22;
bool GbCanTx_Fault23;
bool GbCanTx_Fault24;
bool GbCanTx_Fault25;

bool GbCANTx_FaultREVPOL = FALSE;
bool GbCANTx_FaultMECHFAILURE = FALSE;
bool GbCANTx_FaultELECFAILURE = FALSE;
bool GbCANTx_FaultCANTIMEOUT = FALSE;
bool GbCANTx_FaultUT = FALSE;

float GfCanTx_SpeedFeedback;
signalCfg_t GstCanTx_SpdFdbkCfg;
uint16 Gu16CanTx_SpeedFeedback;
esignal_stat GeCanTx_SpeedFeedback_stat;

float GfCanTx_BusCurrent;
signalCfg_t GstCanTx_BusCurrCfg;
uint8 Gu8CanTx_BusCurrent;
esignal_stat GeCanTx_BusCurrent_stat;

const signalCfg_t kst_SpdFdk 		= {0.02f, 0.0f, 204700.0f, 0.0f, SPD_FDBK_STAT_PTR};
//const signalCfg_t kst_BusCur		= {0.50f, 0.0f, 508.0000f, 0.0f, BUS_CURR_STAT_PTR};
const signalCfg_t kst_BusCur		= {0.25f, -508.0f, 508.0000f, 508.0f, BUS_CURR_STAT_PTR};


void Init_FDBK_0(void)
{
    CAN_Driver_MessageObjectInit(&g_MulticanBasic.drivers.can0SrcMsgObj0, MSG_ID_FDBK_0, IfxMultican_Frame_transmit, TRUE);

	GbCanTx_Fault00 = 0x0;
	GbCanTx_Fault01 = 0x0;
	GbCanTx_Fault02 = 0x0;
	GbCanTx_Fault03 = 0x0;
	GbCanTx_Fault04 = 0x0;
	GbCanTx_Fault05 = 0x0;
	GbCanTx_Fault06 = 0x0;
	GbCanTx_Fault07 = 0x0;
	GbCanTx_Fault08 = 0x0;
	GbCanTx_Fault09 = 0x0;
	GbCanTx_Fault10 = 0x0;
	GbCanTx_Fault11 = 0x0;
	GbCanTx_Fault12 = 0x0;
	GbCanTx_Fault13 = 0x0;
	GbCanTx_Fault14 = 0x0;
	GbCanTx_Fault15 = 0x0;
	GbCanTx_Fault16 = 0x0;
	GbCanTx_Fault17 = 0x0;
	GbCanTx_Fault18 = 0x0;
	GbCanTx_Fault19 = 0x0;
	GbCanTx_Fault20 = 0x0;
	GbCanTx_Fault21 = 0x0;
	GbCanTx_Fault22 = 0x0;
	GbCanTx_Fault23 = 0x0;
	GbCanTx_Fault24 = 0x0;
	GbCanTx_Fault25 = 0x0;

	GstCanTx_SpdFdbkCfg = kst_SpdFdk;
	GstCanTx_BusCurrCfg = kst_BusCur;

	GfCanTx_SpeedFeedback = 0.0f;
	Gu16CanTx_SpeedFeedback = 0;

	GfCanTx_BusCurrent = 0.0f;
	Gu8CanTx_BusCurrent = 0;

	GbCANTx_FaultREVPOL = FALSE;
	GbCANTx_FaultMECHFAILURE = FALSE;
	GbCANTx_FaultELECFAILURE = FALSE;
	GbCANTx_FaultCANTIMEOUT = FALSE;
	GbCANTx_FaultUT = FALSE;
}

static void Process_FDBK_0(void)
{
	memset(&FDBK_0_data, 0, sizeof(uint64));

	/* first bank of faults */
	GbCanTx_Fault00 = GbSTM_IaOCSW;
	GbCanTx_Fault01 = GbSTM_IbOCSW;
	GbCanTx_Fault02 = GbSTM_IcOCSW;
	GbCanTx_Fault03 = GbSTM_IabcOCHW;
	GbCanTx_Fault04 = GbSTM_VdcOVSW;
	GbCanTx_Fault05 = GbSTM_VdcOVHW;
	GbCanTx_Fault06 = GbSTM_IsSUMFltSW;
	GbCanTx_Fault07 = GbSTM_VdcUVSW;
	GbCanTx_Fault08 = GbSTM_V12OVSW;
	GbCanTx_Fault09 = GbSTM_V05OVSW;
	GbCanTx_Fault10 = GbSTM_WrpmLockSW;
	GbCanTx_Fault11 = GbSTM_WrpmOSSW;
	GbCanTx_Fault12 = GbSTM_TempBdr2OTSW;

	/* second bank of faults */
	GbCanTx_Fault13 = GbSTM_TempIMSOTSW;
	GbCanTx_Fault14 = GbSTM_TempBdr1OTSW;
	GbCanTx_Fault15 = GbSTM_TempBdr2OORSW;
	GbCanTx_Fault16 = GbSTM_TempBdr1OORSW;
	GbCanTx_Fault17 = GbSTM_TempIMSOORSW;
	GbCanTx_Fault18 = GbSTM_IaOORSW;
	GbCanTx_Fault19 = GbSTM_IbOORSW;
	GbCanTx_Fault20 = GbSTM_IcOORSW;
	GbCanTx_Fault21 = GbSTM_VdcOORSW;

	/* third bank of faults */
	GbCanTx_Fault22 = GbSTM_TempMotOTSW;
	GbCanTx_Fault23 = GbSTM_TempMotOORSW;
	GbCanTx_Fault24 = GbSTM_WrpmOORSW;
	GbCanTx_Fault25 = GbSTM_TempSens1OTSW;

	FDBK_0_data |= FILL(GbCanTx_Fault00, BIT_MASK_FAULT00, BIT_START_POS_FAULT00);
	FDBK_0_data |= FILL(GbCanTx_Fault01, BIT_MASK_FAULT01, BIT_START_POS_FAULT01);
	FDBK_0_data |= FILL(GbCanTx_Fault02, BIT_MASK_FAULT02, BIT_START_POS_FAULT02);
	FDBK_0_data |= FILL(GbCanTx_Fault03, BIT_MASK_FAULT03, BIT_START_POS_FAULT03);
	FDBK_0_data |= FILL(GbCanTx_Fault04, BIT_MASK_FAULT04, BIT_START_POS_FAULT04);
	FDBK_0_data |= FILL(GbCanTx_Fault05, BIT_MASK_FAULT05, BIT_START_POS_FAULT05);
	FDBK_0_data |= FILL(GbCanTx_Fault06, BIT_MASK_FAULT06, BIT_START_POS_FAULT06);
	FDBK_0_data |= FILL(GbCanTx_Fault07, BIT_MASK_FAULT07, BIT_START_POS_FAULT07);
	FDBK_0_data |= FILL(GbCanTx_Fault08, BIT_MASK_FAULT08, BIT_START_POS_FAULT08);
	FDBK_0_data |= FILL(GbCanTx_Fault09, BIT_MASK_FAULT09, BIT_START_POS_FAULT09);
	FDBK_0_data |= FILL(GbCanTx_Fault10, BIT_MASK_FAULT10, BIT_START_POS_FAULT10);
	FDBK_0_data |= FILL(GbCanTx_Fault11, BIT_MASK_FAULT11, BIT_START_POS_FAULT11);
	FDBK_0_data |= FILL(GbCanTx_Fault12, BIT_MASK_FAULT12, BIT_START_POS_FAULT12);
	FDBK_0_data |= FILL(GbCanTx_Fault13, BIT_MASK_FAULT13, BIT_START_POS_FAULT13);
	FDBK_0_data |= FILL(GbCanTx_Fault14, BIT_MASK_FAULT14, BIT_START_POS_FAULT14);
	FDBK_0_data |= FILL(GbCanTx_Fault15, BIT_MASK_FAULT15, BIT_START_POS_FAULT15);
	FDBK_0_data |= FILL(GbCanTx_Fault16, BIT_MASK_FAULT16, BIT_START_POS_FAULT16);
	FDBK_0_data |= FILL(GbCanTx_Fault17, BIT_MASK_FAULT17, BIT_START_POS_FAULT17);
	FDBK_0_data |= FILL(GbCanTx_Fault18, BIT_MASK_FAULT18, BIT_START_POS_FAULT18);
	FDBK_0_data |= FILL(GbCanTx_Fault19, BIT_MASK_FAULT19, BIT_START_POS_FAULT19);
	FDBK_0_data |= FILL(GbCanTx_Fault20, BIT_MASK_FAULT20, BIT_START_POS_FAULT20);
	FDBK_0_data |= FILL(GbCanTx_Fault21, BIT_MASK_FAULT21, BIT_START_POS_FAULT21);
	FDBK_0_data |= FILL(GbCanTx_Fault22, BIT_MASK_FAULT22, BIT_START_POS_FAULT22);
	FDBK_0_data |= FILL(GbCanTx_Fault23, BIT_MASK_FAULT23, BIT_START_POS_FAULT23);
	FDBK_0_data |= FILL(GbCanTx_Fault24, BIT_MASK_FAULT24, BIT_START_POS_FAULT24);
	FDBK_0_data |= FILL(GbCanTx_Fault25, BIT_MASK_FAULT25, BIT_START_POS_FAULT25);

	FDBK_0_data |= FILL(GbCANTx_FaultREVPOL, BIT_MASK_FAULT_REVPOL, BIT_START_POS_FAULT_REVPOL);
	FDBK_0_data |= FILL(GbCANTx_FaultCANTIMEOUT, BIT_MASK_FAULT_CANTIMEOUT, BIT_START_POS_FAULT_CANTIMEOUT);
	FDBK_0_data |= FILL(GbCANTx_FaultELECFAILURE, BIT_MASK_FAULT_ELECFAILURE, BIT_START_POS_FAULT_ELECFAILURE);
	FDBK_0_data |= FILL(GbCANTx_FaultMECHFAILURE, BIT_MASK_FAULT_MECHFAILURE, BIT_START_POS_FAULT_MECHFAILURE);

	FDBK_0_data |= FILL(GbCANTx_FaultUT, BIT_MASK_FAULT_UT, BIT_START_POS_FAULT_UT);


	if(LIMIT(GfCanTx_SpeedFeedback,GstCanTx_SpdFdbkCfg.min,GstCanTx_SpdFdbkCfg.max))
	{
		Gu16CanTx_SpeedFeedback = (uint16)((GfCanTx_SpeedFeedback * GstCanTx_SpdFdbkCfg.res) + GstCanTx_SpdFdbkCfg.offset);
		GeCanTx_SpeedFeedback_stat = OK;
	}
	else if(GfCanTx_SpeedFeedback > GstCanTx_SpdFdbkCfg.max )
	{
		Gu16CanTx_SpeedFeedback = GstCanTx_SpdFdbkCfg.max;
		GeCanTx_SpeedFeedback_stat = OOR_HIGH;
	}
	else
	{
		Gu16CanTx_SpeedFeedback = GstCanTx_SpdFdbkCfg.min;
		GeCanTx_SpeedFeedback_stat = OOR_LOW;
	}
	FDBK_0_data |= FILL(Gu16CanTx_SpeedFeedback, BIT_MASK_SPEED_FDBK, BIT_START_POS_SPEED_FDBK);
	GeCanTxLink_MsgStat |= GeCanTx_SpeedFeedback_stat;

	Process_TxSignal_u8(GfCanTx_BusCurrent,	GstCanTx_BusCurrCfg, 	&Gu8CanTx_BusCurrent);
	FDBK_0_data |= FILL(Gu8CanTx_BusCurrent, BIT_MASK_BUS_CURR, BIT_START_POS_BUS_CURR);
	GeCanTxLink_MsgStat |= *(GstCanTx_BusCurrCfg.stat_ptr);

}

void Transmit_FDBK_0(void)
{
	uint32 dataLowTx  = 0x00000000;
	uint32 dataHighTx = 0x00000000;
	uint64 datawordTx = 0;
	IfxMultican_Message msg_buf;

	Process_FDBK_0();

	memcpy(&datawordTx, &FDBK_0_data, sizeof(uint64));

	dataLowTx 	= (uint32)datawordTx;
	dataHighTx 	= (uint64)datawordTx >> 32;

	IfxMultican_Message_init(&msg_buf, MSG_ID_FDBK_0, dataLowTx , dataHighTx, IfxMultican_DataLengthCode_8);

	IfxMultican_Can_MsgObj_sendMessage(&g_MulticanBasic.drivers.can0SrcMsgObj0, &msg_buf);
}

#elif (CUSTOMER_ID == AUDI)

/*****************************MSG_0E3h********************************************/
/*********************************************************************************/
bool GbCanTx_DertgFlgSW_Heat;
bool GbCanTx_DertgFlgSW_Temp;
bool GbCanTx_DertgFlgVdc48v;
bool GbCanTx_DertgFlgHW_Temp;
bool GbCanTx_BoostUnderTempFlg;

float GfCanTx_SpeedActual;
signalCfg_t GstCanTx_SpeedActualCfg;
esignal_stat GeCanTx_SpeedActual_stat;

float GfCanTx_DcCurrent;
signalCfg_t GstCanTx_DcCurrentCfg;
esignal_stat GeCanTx_DcCurrent_stat;

float GfCanTx_DcVoltage;
signalCfg_t GstCanTx_DcVoltageCfg;
esignal_stat GeCanTx_DcVoltage_stat;


const signalCfg_t kst_SpeedActual 	= {0.5f, 	0.0f, 		131068.0f, 	0.0f, 	SpeedActual_STAT_PTR};
const signalCfg_t kst_DcCurrent		= {1.0f, 	0.0f, 	    253.0000f, 	0.0f,   DcCurrent_STAT_PTR};
const signalCfg_t kst_DcVoltage 	= {4.0f, 	0.000f, 	63.00f, 	0.000f, DcVoltage_STAT_PTR};


void Init_FDBK_0(void)
{
	GstCanTx_SpeedActualCfg = kst_SpeedActual;
	GstCanTx_DcCurrentCfg = kst_DcCurrent;
	GstCanTx_DcVoltageCfg = kst_DcVoltage;

	GfCanTx_SpeedActual = 0.0f;
	GeCanTx_SpeedActual_stat = OK;

	GfCanTx_DcCurrent = 0.0f;
	GeCanTx_DcCurrent_stat = OK;

	GfCanTx_DcVoltage = 0.0f;
	GeCanTx_DcVoltage_stat = OK;

	GbCanTx_DertgFlgSW_Heat = FALSE;
	GbCanTx_DertgFlgSW_Temp = FALSE;
	GbCanTx_DertgFlgVdc48v = FALSE;
	GbCanTx_DertgFlgHW_Temp = FALSE;
	GbCanTx_BoostUnderTempFlg = FALSE;
}


static void Process_FDBK_0(void)
{
	static uint8 Gu8CanTx_data0;
	static uint16 Gu16CanTx_data0;

	memset(&FDBK_0_data, 0, sizeof(uint64));

	// Copy boolean values for processing to bit fields
	Gu8CanTx_data0 = (uint8)GbCanTx_DertgFlgSW_Heat;
	FDBK_0_data |= FILL(Gu8CanTx_data0, BIT_MASK_DertgFlgSW_Heat, BIT_START_POS_DertgFlgSW_Heat);

	Gu8CanTx_data0 = (uint8)GbCanTx_DertgFlgSW_Temp;
	FDBK_0_data |= FILL(Gu8CanTx_data0, BIT_MASK_DertgFlgSW_Temp, BIT_START_POS_DertgFlgSW_Temp);

	Gu8CanTx_data0 = (uint8)GbCanTx_DertgFlgVdc48v;
	FDBK_0_data |= FILL(Gu8CanTx_data0, BIT_MASK_DertgFlgVdc48v, BIT_START_POS_DertgFlgVdc48v);

	Gu8CanTx_data0 = (uint8)GbCanTx_DertgFlgHW_Temp;
	FDBK_0_data |= FILL(Gu8CanTx_data0, BIT_MASK_DertgFlgHW_Temp, BIT_START_POS_DertgFlgHW_Temp);

	// Process the speed feedback field
	Process_TxSignal_u16(GfCanTx_SpeedActual,	GstCanTx_SpeedActualCfg, &Gu16CanTx_data0);
	FDBK_0_data |= FILL(Gu16CanTx_data0, BIT_MASK_SpeedActual, BIT_START_POS_SpeedActual);
	GeCanTxLink_MsgStat |= *(GstCanTx_SpeedActualCfg.stat_ptr);

	// Process the DC Current field
	Process_TxSignal_u8(GfCanTx_DcCurrent,	GstCanTx_DcCurrentCfg, &Gu8CanTx_data0);
	FDBK_0_data |= FILL(Gu8CanTx_data0, BIT_MASK_DcCurrent, BIT_START_POS_DcCurrent);
	GeCanTxLink_MsgStat |= *(GstCanTx_DcCurrentCfg.stat_ptr);

	// Process the DC Voltage field
	Process_TxSignal_u8(GfCanTx_DcVoltage,	GstCanTx_DcVoltageCfg, &Gu8CanTx_data0);
	FDBK_0_data |= FILL(Gu8CanTx_data0, BIT_MASK_DcVoltage, BIT_START_POS_DcVoltage);
	GeCanTxLink_MsgStat |= *(GstCanTx_DcVoltageCfg.stat_ptr);

	Gu8CanTx_data0 = (uint8)GbCanTx_BoostUnderTempFlg;
	FDBK_0_data |= FILL(Gu8CanTx_data0, BIT_MASK_BoostUnderTempFlg, BIT_START_POS_BoostUnderTempFlg);
}

void Transmit_FDBK_0(void)
{
	uint32 dataLowTx  = 0x00000000;
	uint32 dataHighTx = 0x00000000;
	uint64 datawordTx = 0;
	IfxMultican_Message msg_buf;

	Process_FDBK_0();

	memcpy(&datawordTx, &FDBK_0_data, sizeof(uint64));

	dataLowTx 	= (uint32)datawordTx;
	dataHighTx 	= (uint64)datawordTx >> 32;

	IfxMultican_Message_init(&msg_buf, MSG_ID_FDBK_0, dataLowTx , dataHighTx, IfxMultican_DataLengthCode_8);

	IfxMultican_Can_MsgObj_sendMessage(&g_MulticanBasic.drivers.can0SrcMsgObj0, &msg_buf);
}

#else

void Init_FDBK_0(void)
{
	/* Do nothing */
}

void Transmit_FDBK_0(void)
{
	/* Do nothing */
}

#endif /* CUSTOMER_ID */

