/*
 * FDBK_7.h
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */

#ifndef APP_CAN_FDBK_7_H_
#define APP_CAN_FDBK_7_H_


#include <Ifx_Types.h>
#include <Ifx_TypesReg.h>
#include <string.h>
#include "CAN_Driver.h"
#include "CAN_Interface_Tx.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)
/*****************************MSG_18E14290h***************************************/
/*********************************************************************************/
/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/

#define MSG_ID_FDBK_7					0x18E14290

#define BIT_START_POS_IQ_CMD			0
#define BIT_START_POS_ID_CMD			8
#define BIT_START_POS_IQ_FDBK			16
#define BIT_START_POS_ID_FDBK			24
#define BIT_START_POS_AC_MTR_VLTG		32
#define BIT_START_POS_ETURBO_SN_NUM		40
#define BIT_START_POS_PWM_FREQ			48
#define BIT_START_POS_MOSFET_TEMP_EST	56

#define BIT_SIZE_IQ_CMD					8
#define BIT_SIZE_ID_CMD					8
#define BIT_SIZE_IQ_FDBK				8
#define BIT_SIZE_ID_FDBK				8
#define BIT_SIZE_AC_MTR_VLTG			8
#define BIT_SIZE_ETURBO_SN_NUM			8
#define BIT_SIZE_PWM_FREQ				8
#define BIT_SIZE_MOSFET_TEMP_EST		8

#define BIT_MASK_IQ_CMD					(1<<BIT_SIZE_IQ_CMD)-1
#define BIT_MASK_ID_CMD					(1<<BIT_SIZE_ID_CMD)-1
#define BIT_MASK_IQ_FDBK				(1<<BIT_SIZE_IQ_FDBK)-1
#define BIT_MASK_ID_FDBK				(1<<BIT_SIZE_ID_FDBK)-1
#define BIT_MASK_AC_MTR_VLTG			(1<<BIT_SIZE_AC_MTR_VLTG)-1
#define BIT_MASK_ETURBO_SN_NUM			(1<<BIT_SIZE_ETURBO_SN_NUM)-1
#define BIT_MASK_PWM_FREQ				(1<<BIT_SIZE_PWM_FREQ)-1
#define BIT_MASK_MOSFET_TEMP_EST		(1<<BIT_SIZE_MOSFET_TEMP_EST)-1

#define FDBK_7_SUPPORT_SIGNED_VAL		0

#define IQ_CMD_STAT_PTR 				&GeCanTx_IqCommand_stat
#define ID_CMD_STAT_PTR  				&GeCanTx_IdCommand_stat
#define IQ_FDBK_STAT_PTR				&GeCanTx_IqFdbkCfg_stat
#define ID_FDBK_STAT_PTR				&GeCanTx_IdFdbkCfg_stat
#define AC_MTR_VLTG_STAT_PTR			&GeCanTx_ACMtrVtgCfg_stat
#define TQ_EST_STAT_PTR					&GeCanTx_TorqueEstCfg_stat
#define PWM_FREQ_STAT_PTR				&GeCanTx_PWMFreqCfg_stat
#define MOSFET_TEMP_EST_STAT_PTR		&GeCanTx_MOSFETTempEstCfg_stat

#define HZ_TO_KHZ						0.001
#define DEFAULT_ETURBO_SN				1

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

IFX_EXTERN esignal_stat GeCanTx_IqCommand_stat;
IFX_EXTERN esignal_stat GeCanTx_IdCommand_stat;
IFX_EXTERN esignal_stat GeCanTx_IqFdbkCfg_stat;
IFX_EXTERN esignal_stat GeCanTx_IdFdbkCfg_stat;
IFX_EXTERN esignal_stat GeCanTx_ACMtrVtgCfg_stat;
IFX_EXTERN esignal_stat GeCanTx_TorqueEstCfg_stat;
IFX_EXTERN esignal_stat GeCanTx_PWMFreqCfg_stat;
IFX_EXTERN esignal_stat GeCanTx_MOSFETTempEstCfg_stat;

IFX_EXTERN float GfCanTx_IqCommand;
IFX_EXTERN signalCfg_t GstCanTx_IqCmdCfg;
IFX_EXTERN uint8 Gu8CanTx_IqCommand;

IFX_EXTERN float GfCanTx_IdCommand;
IFX_EXTERN signalCfg_t GstCanTx_IdCmdCfg;
IFX_EXTERN uint8 Gu8CanTx_IdCommand;

IFX_EXTERN float GfCanTx_IqFeedback;
IFX_EXTERN signalCfg_t GstCanTx_IqFdbkCfg;
IFX_EXTERN uint8 Gu8CanTx_IqFeedback;

IFX_EXTERN float GfCanTx_IdFeedback;
IFX_EXTERN signalCfg_t GstCanTx_IdFdbkCfg;
IFX_EXTERN uint8 Gu8CanTx_IdFeedback;

IFX_EXTERN float GfCanTx_ACMotorVoltageVAC;
IFX_EXTERN signalCfg_t GstCanTx_ACMtrVtgCfg;
IFX_EXTERN uint8 Gu8CanTx_ACMotorVoltageVAC;

IFX_EXTERN float GfCanTx_TorqueEstimate;
IFX_EXTERN signalCfg_t GstCanTx_TorqueEstCfg;
IFX_EXTERN uint8 Gu8CanTx_TorqueEstimate;

IFX_EXTERN float GfCanTx_PWMFrequency;
IFX_EXTERN signalCfg_t GstCanTx_PWMFreqCfg;
IFX_EXTERN uint8 Gu8CanTx_PWMFrequency;

IFX_EXTERN float GfCanTx_MOSFETTempEstimate;
IFX_EXTERN signalCfg_t GstCanTx_MOSFETTempEstCfg;
IFX_EXTERN uint8 Gu8CanTx_MOSFETTempEstimate;

#endif /* CUSTOMER_ID */

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
IFX_EXTERN void Init_FDBK_7(void);
IFX_EXTERN void Transmit_FDBK_7(void);


#endif /* APP_CAN_FDBK_7_H_ */
