/*
 * FDBK_2.c
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */
#include "FDBK_2.h"

static uint64 FDBK_2_data;

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)
/*****************************MSG_18E14240h***************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/

signalCfg_t GstCanTx_TempSens2Cfg;
signalCfg_t GstCanTx_TempSens3Cfg;
signalCfg_t GstCanTx_InternalSpdCmdCfg;
signalCfg_t GstCanTx_PBATempCfg;
signalCfg_t GstCanTx_TorqueEstCfg;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
#if (FDBK_2_SUPPORT_SIGNED_VAL == 1)
	const signalCfg_t kst_TqEst			= {100.0f, 	-1.280f, 	1.260f, 	0.000f, TQ_EST_STAT_PTR};
	sint8 Gs8CanTx_TorqueEstimate;
#else
	const signalCfg_t kst_TqEst			= {100.0f, 	-1.270f, 	1.270f,  	1.270f,	TQ_EST_STAT_PTR};
	uint8 Gu8CanTx_TorqueEstimate;
#endif

const signalCfg_t kst_TempSens2 		= {0.50f, 	-254.0f, 	254.0f, 	254.0f, TEMP_SENS2_STAT_PTR};
const signalCfg_t kst_TempSens3			= {0.50f, 	-254.0f,	254.0f, 	254.0f, TEMP_SENS3_STAT_PTR};
const signalCfg_t kst_InternalSpdCmd 	= {0.00125f,0.0000f,	203200.0f,	0.000f,	INT_SPD_CMD_STAT_PTR};
const signalCfg_t kst_PBATemp			= {0.50f, 	0.0000f,	508.0f, 	0.000f, PBA_TEMP_STAT_PTR};


float GfCanTx_TempSens2;
uint8_t Gu8CanTx_TempSens2;
esignal_stat GeCanTx_TempSens2_stat;

float GfCanTx_TempSens3;
uint8_t Gu8CanTx_TempSens3;
esignal_stat GeCanTx_TempSens3_stat;

bool GbCanTx_Warning00;
bool GbCanTx_Warning01;
bool GbCanTx_Warning02;
bool GbCanTx_Warning03;
bool GbCanTx_Warning04;
bool GbCanTx_Warning05;
bool GbCanTx_Warning06;
bool GbCanTx_Warning07;

bool GbCanTx_Limit00;
bool GbCanTx_Limit01;
bool GbCanTx_Limit02;
bool GbCanTx_Limit03;
bool GbCanTx_Limit04;
bool GbCanTx_Limit05;
bool GbCanTx_Limit06;
bool GbCanTx_Limit07;

float GfCanTx_InternalSpeedCommand;
uint8_t Gu8CanTx_InternalSpeedCommand;
esignal_stat GeCanTx_InternalSpeedCommand_stat;

float GfCanTx_TorqueEstimate;
esignal_stat GeCanTx_TorqueEstCfg_stat;

bool GbCanTx_Fault32;
bool GbCanTx_Fault33;
bool GbCanTx_Fault34;
bool GbCanTx_Fault35;
bool GbCanTx_Fault36;
bool GbCanTx_Fault37;
bool GbCanTx_Fault38;
bool GbCanTx_Fault39;

float GfCanTx_PBATemp;
uint8_t Gu8CanTx_PBATemp;
esignal_stat GeCanTx_PBATemp_stat;

bool GbCANTx_DertgFlgSW_Heat = FALSE;
bool GbCANTx_DertgFlgSW_Temp = FALSE;
bool GbCANTx_DertgFlgSW_Vdc = FALSE;
bool GbCANTx_DertgFlgHW_Temp = FALSE;


/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
void Init_FDBK_2(void)
{
    CAN_Driver_MessageObjectInit(&g_MulticanBasic.drivers.can0SrcMsgObj2, MSG_ID_FDBK_2, IfxMultican_Frame_transmit, TRUE);

    GbCanTx_Fault32 = 0x0;
	GbCanTx_Fault33 = 0x0;
	GbCanTx_Fault34 = 0x0;
	GbCanTx_Fault35 = 0x0;
	GbCanTx_Fault36 = 0x0;
	GbCanTx_Fault37 = 0x0;
	GbCanTx_Fault38 = 0x0;
	GbCanTx_Fault39 = 0x0;

	GbCanTx_Limit00 = 0x0;
	GbCanTx_Limit01 = 0x0;
	GbCanTx_Limit02 = 0x0;
	GbCanTx_Limit03 = 0x0;
	GbCanTx_Limit04 = 0x0;
	GbCanTx_Limit05 = 0x0;
	GbCanTx_Limit06 = 0x0;
	GbCanTx_Limit07 = 0x0;

	GbCanTx_Warning00 = 0x0;
	GbCanTx_Warning01 = 0x0;
	GbCanTx_Warning02 = 0x0;
	GbCanTx_Warning03 = 0x0;
	GbCanTx_Warning04 = 0x0;
	GbCanTx_Warning05 = 0x0;
	GbCanTx_Warning06 = 0x0;
	GbCanTx_Warning07 = 0x0;

	GfCanTx_TempSens2 = 0.0f;
	Gu8CanTx_TempSens2 = 0;
	GeCanTx_TempSens2_stat = OK;

	GfCanTx_TempSens3 = 0.0f;
	Gu8CanTx_TempSens3 = 0;
	GeCanTx_TempSens3_stat = OK;

	GfCanTx_InternalSpeedCommand = 0.0f;
	Gu8CanTx_InternalSpeedCommand = 0;
	GeCanTx_InternalSpeedCommand_stat = OK;

#if (FDBK_2_SUPPORT_SIGNED_VAL == 1)
	Gs8CanTx_TorqueEstimate = 0;
#else
	Gu8CanTx_TorqueEstimate = 0;
#endif

	GfCanTx_TorqueEstimate = 0.0f;
	GeCanTx_TorqueEstCfg_stat = OK;

	GfCanTx_PBATemp = 0.0f;
	Gu8CanTx_PBATemp = 0;
	GeCanTx_PBATemp_stat = OK;

	GstCanTx_TempSens2Cfg  		= kst_TempSens2;
	GstCanTx_TempSens3Cfg 		= kst_TempSens3;
	GstCanTx_InternalSpdCmdCfg 	= kst_InternalSpdCmd;
	GstCanTx_TorqueEstCfg		= kst_TqEst;
	GstCanTx_PBATempCfg			= kst_PBATemp;
}

static void Process_FDBK_2(void)
{
	memset(&FDBK_2_data, 0, sizeof(uint64));

	FDBK_2_data |= FILL(GbCanTx_Warning00, BIT_MASK_WARNING00, BIT_START_POS_WARNING00);
	FDBK_2_data |= FILL(GbCanTx_Warning01, BIT_MASK_WARNING01, BIT_START_POS_WARNING01);
	FDBK_2_data |= FILL(GbCanTx_Warning02, BIT_MASK_WARNING02, BIT_START_POS_WARNING02);
	FDBK_2_data |= FILL(GbCanTx_Warning03, BIT_MASK_WARNING03, BIT_START_POS_WARNING03);
	FDBK_2_data |= FILL(GbCanTx_Warning04, BIT_MASK_WARNING04, BIT_START_POS_WARNING04);
	FDBK_2_data |= FILL(GbCanTx_Warning05, BIT_MASK_WARNING05, BIT_START_POS_WARNING05);
	FDBK_2_data |= FILL(GbCanTx_Warning06, BIT_MASK_WARNING06, BIT_START_POS_WARNING06);
	FDBK_2_data |= FILL(GbCanTx_Warning07, BIT_MASK_WARNING07, BIT_START_POS_WARNING07);

	FDBK_2_data |= FILL(GbCanTx_Limit00, BIT_MASK_LIMIT00, BIT_START_POS_LIMIT00);
	FDBK_2_data |= FILL(GbCanTx_Limit01, BIT_MASK_LIMIT01, BIT_START_POS_LIMIT01);
	FDBK_2_data |= FILL(GbCanTx_Limit02, BIT_MASK_LIMIT02, BIT_START_POS_LIMIT02);
	FDBK_2_data |= FILL(GbCanTx_Limit03, BIT_MASK_LIMIT03, BIT_START_POS_LIMIT03);
	/*
	FDBK_2_data |= FILL(GbCanTx_Limit04, BIT_MASK_LIMIT04, BIT_START_POS_LIMIT04);
	FDBK_2_data |= FILL(GbCanTx_Limit05, BIT_MASK_LIMIT05, BIT_START_POS_LIMIT05);
	FDBK_2_data |= FILL(GbCanTx_Limit06, BIT_MASK_LIMIT06, BIT_START_POS_LIMIT06);
	FDBK_2_data |= FILL(GbCanTx_Limit07, BIT_MASK_LIMIT07, BIT_START_POS_LIMIT07);
	*/

	GbCanTx_Fault32 = GbSTM_TempSens1OORSW;
	GbCanTx_Fault33 = GbSTM_TempSens2OORSW;
	GbCanTx_Fault34 = GbSTM_TempSens3OORSW;
	GbCanTx_Fault35 = GbSTM_LOTSW;
	GbCanTx_Fault36 = GbSTM_ExtCoolTempOTSWT1;
	GbCanTx_Fault37 = GbSTM_InletTempOTSWT1;

	FDBK_2_data |= FILL(GbCanTx_Fault32, BIT_MASK_FAULT32, BIT_START_POS_FAULT32);
	FDBK_2_data |= FILL(GbCanTx_Fault33, BIT_MASK_FAULT33, BIT_START_POS_FAULT33);
	FDBK_2_data |= FILL(GbCanTx_Fault34, BIT_MASK_FAULT34, BIT_START_POS_FAULT34);
	FDBK_2_data |= FILL(GbCanTx_Fault35, BIT_MASK_FAULT35, BIT_START_POS_FAULT35);
	FDBK_2_data |= FILL(GbCanTx_Fault36, BIT_MASK_FAULT36, BIT_START_POS_FAULT36);
	FDBK_2_data |= FILL(GbCanTx_Fault37, BIT_MASK_FAULT37, BIT_START_POS_FAULT37);
	FDBK_2_data |= FILL(GbCanTx_Fault38, BIT_MASK_FAULT38, BIT_START_POS_FAULT38);
	FDBK_2_data |= FILL(GbCanTx_Fault39, BIT_MASK_FAULT39, BIT_START_POS_FAULT39);

	FDBK_2_data |= FILL(GbCANTx_DertgFlgSW_Heat, BIT_MASK_DERATE_HEAT, BIT_START_POS_DERATE_HEAT);
	FDBK_2_data |= FILL(GbCANTx_DertgFlgSW_Temp, BIT_MASK_DERATE_SWTEMP, BIT_START_POS_DERATE_SWTEMP);
	FDBK_2_data |= FILL(GbCANTx_DertgFlgSW_Vdc, BIT_MASK_DERATE_VDC, BIT_START_POS_DERATE_VDC);
	FDBK_2_data |= FILL(GbCANTx_DertgFlgHW_Temp, BIT_MASK_DERATE_HWTEMP, BIT_START_POS_DERATE_HWTEMP);

	GfCanTx_TempSens2 = GfMES_TempSens2;
	Process_TxSignal_u8(GfCanTx_TempSens2, GstCanTx_TempSens2Cfg, 		&Gu8CanTx_TempSens2);
	FDBK_2_data |= FILL(Gu8CanTx_TempSens2, BIT_MASK_TEMP_SENS2, BIT_START_POS_TEMP_SENS2);
	GeCanTxLink_MsgStat |= *(GstCanTx_TempSens2Cfg.stat_ptr);

	GfCanTx_TempSens3 = GfMES_TempSens3;
	Process_TxSignal_u8(GfCanTx_TempSens3, GstCanTx_TempSens3Cfg, 		&Gu8CanTx_TempSens3);
	FDBK_2_data |= FILL(Gu8CanTx_TempSens3, BIT_MASK_TEMP_SENS3, BIT_START_POS_TEMP_SENS3);
	GeCanTxLink_MsgStat |= *(GstCanTx_TempSens3Cfg.stat_ptr);

	Process_TxSignal_u8(GfCanTx_InternalSpeedCommand, 	GstCanTx_InternalSpdCmdCfg, &Gu8CanTx_InternalSpeedCommand);
	FDBK_2_data |= FILL(Gu8CanTx_InternalSpeedCommand, BIT_MASK_INT_SPD_CMD, BIT_START_POS_INT_SPD_CMD);
	GeCanTxLink_MsgStat |= *(GstCanTx_InternalSpdCmdCfg.stat_ptr);

#if (FDBK_2_SUPPORT_SIGNED_VAL == 1)
	Process_TxSignal_s8(GfCanTx_TorqueEstimate, 	GstCanTx_TorqueEstCfg, 		&Gs8CanTx_TorqueEstimate);
	FDBK_2_data |= FILL(Gs8CanTx_TorqueEstimate, 	BIT_MASK_TQ_EST, 			BIT_START_POS_TQ_EST);
#else
	Process_TxSignal_u8(GfCanTx_TorqueEstimate, 	GstCanTx_TorqueEstCfg, 		&Gu8CanTx_TorqueEstimate);
	FDBK_2_data |= FILL(Gu8CanTx_TorqueEstimate, 	BIT_MASK_TQ_EST, 			BIT_START_POS_TQ_EST);
	GeCanTxLink_MsgStat |= *(GstCanTx_TorqueEstCfg.stat_ptr);
#endif

	Process_TxSignal_u8(GfCanTx_PBATemp, GstCanTx_PBATempCfg, 		&Gu8CanTx_PBATemp);
	FDBK_2_data |= FILL(Gu8CanTx_PBATemp, BIT_MASK_PBA_TEMP, BIT_START_POS_PBA_TEMP);
	GeCanTxLink_MsgStat |= *(GstCanTx_PBATempCfg.stat_ptr);

}

void Transmit_FDBK_2(void)
{
	uint32 dataLowTx  = 0x00000000;
	uint32 dataHighTx = 0x00000000;
	uint64 datawordTx = 0;
	IfxMultican_Message msg_buf;

	Process_FDBK_2();

	memcpy(&datawordTx, &FDBK_2_data, sizeof(uint64));

	dataLowTx 	= (uint32)datawordTx;
	dataHighTx 	= (uint64)datawordTx >> 32;

	IfxMultican_Message_init(&msg_buf, MSG_ID_FDBK_2, dataLowTx , dataHighTx, IfxMultican_DataLengthCode_8);

	IfxMultican_Can_MsgObj_sendMessage(&g_MulticanBasic.drivers.can0SrcMsgObj2, &msg_buf);
}

#elif (CUSTOMER_ID == AUDI)

/*****************************MSG_18E14280h***************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/

signalCfg_t GstCanTx_IqCmdCfg;
signalCfg_t GstCanTx_IdCmdCfg;
signalCfg_t GstCanTx_IqFeedbackCfg;
signalCfg_t GstCanTx_IdFeedbackCfg;
signalCfg_t GstCanTx_SwVersionCfg;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

float GfCanTx_IqCmd;
esignal_stat GeCanTx_IqCmd_stat;

float GfCanTx_IdCmd;
esignal_stat GeCanTx_IdCmd_stat;

float GfCanTx_IqFeedback;
esignal_stat GeCanTx_IqFeedback_stat;

float GfCanTx_IdFeedback;
esignal_stat GeCanTx_IdFeedback_stat;

float GfCanTx_SwVersion;
esignal_stat GeCanTx_SwVersion_stat;

bool GbCanTx_V05_UV;
bool GbCanTx_V12_UV;
bool GbCanTx_V12_OV_SW;
bool GbCanTx_V05_OV_SW;
bool GbCanTx_Iabc_OC_HW;
bool GbCanTx_Ia_OC_SW;
bool GbCanTx_Ib_OC_SW;
bool GbCanTx_Ic_OC_SW;
bool GbCanTx_Speed_OL_CL;

const signalCfg_t kst_IqCmd			= {4.0f, -508.0f, 508.0f, -508.0f, IqCmd_STAT_PTR};
const signalCfg_t kst_IdCmd			= {4.0f, -508.0f, 508.0f, -508.0f, IdCmd_STAT_PTR};
const signalCfg_t kst_IqFeedback	= {4.0f, -508.0f, 508.0f, -508.0f, IqFeedback_STAT_PTR};
const signalCfg_t kst_IdFeedback	= {4.0f, -508.0f, 508.0f, -508.0f, IdFeedback_STAT_PTR};
const signalCfg_t kst_SwVersion		= {1.0f, 0.0f, 254.0f, 0.0f, SwVersion_STAT_PTR};


static uint64 FDBK_2_data;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

void Init_FDBK_2(void)
{
	GeCanTx_IqCmd_stat = OK;
	GfCanTx_IqCmd = 0.0f;
	GstCanTx_IqCmdCfg = kst_IqCmd;

	GeCanTx_IdCmd_stat = OK;
	GfCanTx_IdCmd = 0.0f;
	GstCanTx_IdCmdCfg = kst_IdCmd;

	GeCanTx_IqFeedback_stat = OK;
	GfCanTx_IqFeedback = 0.0f;
	GstCanTx_IqFeedbackCfg = kst_IqFeedback;

	GeCanTx_IdFeedback_stat = OK;
	GfCanTx_IdFeedback = 0.0f;
	GstCanTx_IdFeedbackCfg = kst_IdFeedback;

	GeCanTx_SwVersion_stat = OK;
	GfCanTx_SwVersion = 0.0f;
	GstCanTx_SwVersionCfg = kst_SwVersion;

	GbCanTx_V05_UV = FALSE;
	GbCanTx_V12_UV = FALSE;
	GbCanTx_V12_OV_SW = FALSE;
	GbCanTx_V05_OV_SW = FALSE;
	GbCanTx_Iabc_OC_HW = FALSE;
	GbCanTx_Ia_OC_SW = FALSE;
	GbCanTx_Ib_OC_SW = FALSE;
	GbCanTx_Ic_OC_SW = FALSE;
	GbCanTx_Speed_OL_CL = FALSE;
}

static void Process_FDBK_2(void)
{
	static uint8 Gu8CanTx_data2;

	memset(&FDBK_2_data, 0, sizeof(uint64));

	Process_TxSignal_u8(GfCanTx_IqCmd,	GstCanTx_IqCmdCfg, 	&Gu8CanTx_data2);
	FDBK_2_data |= FILL(Gu8CanTx_data2, BIT_MASK_IqCmd, BIT_START_POS_IqCmd);
	GeCanTxLink_MsgStat |= *(GstCanTx_IqCmdCfg.stat_ptr);

	Process_TxSignal_u8(GfCanTx_IdCmd,	GstCanTx_IdCmdCfg, 	&Gu8CanTx_data2);
	FDBK_2_data |= FILL(Gu8CanTx_data2, BIT_MASK_IdCmd, BIT_START_POS_IdCmd);
	GeCanTxLink_MsgStat |= *(GstCanTx_IdCmdCfg.stat_ptr);

	Process_TxSignal_u8(GfCanTx_IqFeedback,	GstCanTx_IqFeedbackCfg, 	&Gu8CanTx_data2);
	FDBK_2_data |= FILL(Gu8CanTx_data2, BIT_MASK_IqFeedback, BIT_START_POS_IqFeedback);
	GeCanTxLink_MsgStat |= *(GstCanTx_IqFeedbackCfg.stat_ptr);

	Process_TxSignal_u8(GfCanTx_IdFeedback,	GstCanTx_IdFeedbackCfg, 	&Gu8CanTx_data2);
	FDBK_2_data |= FILL(Gu8CanTx_data2, BIT_MASK_IdFeedback, BIT_START_POS_IdFeedback);
	GeCanTxLink_MsgStat |= *(GstCanTx_IdFeedbackCfg.stat_ptr);

	Process_TxSignal_u8(GfCanTx_SwVersion,	GstCanTx_SwVersionCfg, 	&Gu8CanTx_data2);
	FDBK_2_data |= FILL(Gu8CanTx_data2, BIT_MASK_SwVersion, BIT_START_POS_SwVersion);
	GeCanTxLink_MsgStat |= *(GstCanTx_SwVersionCfg.stat_ptr);

	// Copy boolean values for processing to bit fields
	Gu8CanTx_data2 = (uint8)GbCanTx_V05_UV;
	FDBK_2_data |= FILL(Gu8CanTx_data2, BIT_MASK_V05_UV, BIT_START_POS_V05_UV);

	Gu8CanTx_data2 = (uint8)GbCanTx_V12_UV;
	FDBK_2_data |= FILL(Gu8CanTx_data2, BIT_MASK_V12_UV, BIT_START_POS_V12_UV);

	Gu8CanTx_data2 = (uint8)GbCanTx_V12_OV_SW;
	FDBK_2_data |= FILL(Gu8CanTx_data2, BIT_MASK_V12_OV_SW, BIT_START_POS_V12_OV_SW);

	Gu8CanTx_data2 = (uint8)GbCanTx_V05_OV_SW;
	FDBK_2_data |= FILL(Gu8CanTx_data2, BIT_MASK_V05_OV_SW, BIT_START_POS_V05_OV_SW);

	Gu8CanTx_data2 = (uint8)GbCanTx_Iabc_OC_HW;
	FDBK_2_data |= FILL(Gu8CanTx_data2, BIT_MASK_Iabc_OC_HW, BIT_START_POS_Iabc_OC_HW);

	Gu8CanTx_data2 = (uint8)GbCanTx_Ia_OC_SW;
	FDBK_2_data |= FILL(Gu8CanTx_data2, BIT_MASK_Ia_OC_SW, BIT_START_POS_Ia_OC_SW);

	Gu8CanTx_data2 = (uint8)GbCanTx_Ib_OC_SW;
	FDBK_2_data |= FILL(Gu8CanTx_data2, BIT_MASK_Ib_OC_SW, BIT_START_POS_Ib_OC_SW);

	Gu8CanTx_data2 = (uint8)GbCanTx_Ic_OC_SW;
	FDBK_2_data |= FILL(Gu8CanTx_data2, BIT_MASK_Ic_OC_SW, BIT_START_POS_Ic_OC_SW);

	Gu8CanTx_data2 = (uint8)GbCanTx_Speed_OL_CL;
	FDBK_2_data |= FILL(Gu8CanTx_data2, BIT_MASK_Speed_OL_CL, BIT_START_POS_Speed_OL_CL);
}

void Transmit_FDBK_2(void)
{
	uint32 dataLowTx  = 0x00000000;
	uint32 dataHighTx = 0x00000000;
	uint64 datawordTx = 0;
	IfxMultican_Message msg_buf;

	Process_FDBK_2();

	memcpy(&datawordTx, &FDBK_2_data, sizeof(uint64));

	dataLowTx 	= (uint32)datawordTx;
	dataHighTx 	= (uint64)datawordTx >> 32;

	IfxMultican_Message_init(&msg_buf, MSG_ID_FDBK_2, dataLowTx , dataHighTx, IfxMultican_DataLengthCode_8);

	IfxMultican_Can_MsgObj_sendMessage(&g_MulticanBasic.drivers.can0SrcMsgObj3, &msg_buf);
}

#else

void Init_FDBK_2(void)
{
	/* Do nothing */
}

void Transmit_FDBK_2(void)
{
	/* Do nothing */
}

#endif /* CUSTOMER_ID */

