/*
 * CMD_2.h
 *
 *  Created on: Nov 4, 2018
 *      Author: H297270
 */

#ifndef APP_CAN_CMD_2_H_
#define APP_CAN_CMD_2_H_

#include "CAN_Interface_Rx.h"
#include <string.h>
#include "CAN_Driver.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

/*****************************MSG_29Ch********************************************/
/*********************************************************************************/
#define MSG_ID_CMD_2				0x29C

#define BIT_START_DCOP_CURR_LIMIT	0
#define BIT_SIZE_DCOP_CURR_LIMIT	9
#define BIT_MASK_DCOP_CURR_LIMIT	((1<<(BIT_SIZE_DCOP_CURR_LIMIT))-1)

#define BIT_START_DCOP_PWR_LIMIT	9
#define BIT_SIZE_DCOP_PWR_LIMIT		10
#define BIT_MASK_DCOP_PWR_LIMIT		((1<<(BIT_SIZE_DCOP_PWR_LIMIT))-1)

#define BIT_START_DCIP_PWR_LIMIT	23
#define BIT_SIZE_DCIP_PWR_LIMIT		10
#define BIT_MASK_DCIP_PWR_LIMIT		((1<<(BIT_SIZE_DCIP_PWR_LIMIT))-1)

#define BIT_START_DCIP_CURR_LIMIT	43
#define BIT_SIZE_DCIP_CURR_LIMIT	9
#define BIT_MASK_DCIP_CURR_LIMIT	((1<<(BIT_SIZE_DCIP_CURR_LIMIT))-1)

#define BIT_START_WATER_TEMP		33
#define BIT_SIZE_WATER_TEMP			9
#define BIT_MASK_WATER_TEMP			((1<<(BIT_SIZE_WATER_TEMP))-1)

#define BIT_START_COMP_INLET_TEMP	52
#define BIT_SIZE_COMP_INLET_TEMP	9
#define BIT_MASK_COMP_INLET_TEMP	((1<<(BIT_SIZE_COMP_INLET_TEMP))-1)

#define DCOP_CURR_LIMIT_STAT_PTR	&GeCanRx_DCOutputCurrLimit_stat
#define DCIP_CURR_LIMIT_STAT_PTR	&GeCanRx_DCInputCurrLimit_stat
#define DCOP_PWR_LIMIT_STAT_PTR		&GeCanRx_DCOutputPowerLimit_stat
#define DCIP_PWR_LIMIT_STAT_PTR		&GeCanRx_DCInputPowerLimit_stat
#define WATER_TEMP_STAT_PTR			&GeCanRx_WaterTemp_stat
#define COMP_INLET_TEMP_STAT_PTR	&GeCanRx_CompInletTemp_stat

#define DCPWRLIMIT_SF_INV			2.5			/* 1/0.4 */
#define DCCURRLIMIT_SF_INV			1
#define MAX_SPEED_HEX_VAL_81K		0x654		/* (81Krpm * 0.02 (1/50) ) */

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

IFX_EXTERN float GfCanRx_DCOutputCurrLimit;
IFX_EXTERN float GfCanRx_DCOutputPowerLimit;
IFX_EXTERN float GfCanRx_DCInputPowerLimit;
IFX_EXTERN float GfCanRx_DCInputCurrLimit;
IFX_EXTERN float GfCanRx_WaterTemp;
IFX_EXTERN float GfCanRx_CompInletTemp;

IFX_EXTERN esignal_stat GeCanRx_DCOutputCurrLimit_stat;
IFX_EXTERN esignal_stat GeCanRx_DCOutputPowerLimit_stat;
IFX_EXTERN esignal_stat GeCanRx_DCInputPowerLimit_stat;
IFX_EXTERN esignal_stat GeCanRx_DCInputCurrLimit_stat;
IFX_EXTERN esignal_stat GeCanRx_WaterTemp_stat;
IFX_EXTERN esignal_stat	GeCanRx_CompInletTemp_stat;

#endif /* CUSTOMER_ID */

IFX_EXTERN uint16 Gu16CANRx_CMD2TimeOutCtr;
IFX_EXTERN bool GbCANRx_CMD2TimeOutSWT2;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
IFX_EXTERN void Init_CMD_2(void);
IFX_EXTERN void Receive_CMD_2(void);

#endif /* APP_CAN_CMD_2_H_ */
