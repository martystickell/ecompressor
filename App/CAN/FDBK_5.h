/*
 * FDBK_5.h
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */

#ifndef APP_CAN_FDBK_5_H_
#define APP_CAN_FDBK_5_H_

#include <Ifx_Types.h>
#include <Ifx_TypesReg.h>
#include <string.h>
#include "CAN_Driver.h"
#include "CAN_Interface_Tx.h"
#include "STM.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)
/*****************************MSG_18E14282h***************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/

#define MSG_ID_FDBK_5					0x18E14282

#define BIT_START_POS_TEMP_IMS			0
#define BIT_SIZE_TEMP_IMS				8
#define BIT_MASK_TEMP_IMS				(1<<BIT_SIZE_TEMP_IMS)-1

#define TEMP_IMS_STAT_PTR 				&GeCanTx_TempIMS_stat

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

IFX_EXTERN esignal_stat GeCanTx_TempIMS_stat;
IFX_EXTERN float GfCanTx_TempIMS;

#endif /* CUSTOMER_ID */

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
IFX_EXTERN void Init_FDBK_5(void);
IFX_EXTERN void Transmit_FDBK_5(void);


#endif /* APP_CAN_FDBK_5_H_ */
