/*
 * FDBK_1.h
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */

#ifndef APP_CAN_FDBK_1_H_
#define APP_CAN_FDBK_1_H_

#include <Ifx_Types.h>
#include <Ifx_TypesReg.h>
#include <string.h>
#include "CAN_Driver.h"
#include "CAN_Interface_Tx.h"
#include "STM.h"
#include "CAN_Defs.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)
/*****************************MSG_17Bh********************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/
#define MSG_ID_FDBK_1				0x17B

#define BIT_START_SW_VERSION		0
#define BIT_START_OPERATING_MODE	8
#define BIT_START_POS_FAULT26		13
#define BIT_START_POS_FAULT27		14
#define BIT_START_DERATING			16
#define BIT_START_BUS_VOLTAGE		24
#define BIT_START_INV_TEMP1			32
#define BIT_START_POS_FAULT28		40
#define BIT_START_POS_FAULT29		41
#define BIT_START_POS_FAULT30		42
#define BIT_START_POS_FAULT31		43
#define BIT_START_OPERATING_MODE02	56

#define BIT_SIZE_SW_VERSION			8
#define BIT_SIZE_OPERATING_MODE 	5
#define BIT_SIZE_FAULT26			1
#define BIT_SIZE_FAULT27			1
#define BIT_SIZE_DERATING			8
#define BIT_SIZE_BUS_VOLTAGE		8
#define BIT_SIZE_INV_TEMP1			8
#define BIT_SIZE_FAULT28			1
#define BIT_SIZE_FAULT29			1
#define BIT_SIZE_FAULT30			1
#define BIT_SIZE_FAULT31			1
#define BIT_SIZE_OPERATING_MODE02 	5

#define BIT_MASK_OPERATING_MODE 	(1<<BIT_SIZE_OPERATING_MODE)-1
#define BIT_MASK_SW_VERSION			(1<<BIT_SIZE_SW_VERSION)-1
#define BIT_MASK_FAULT26			(1<<BIT_SIZE_FAULT26)-1
#define BIT_MASK_FAULT27			(1<<BIT_SIZE_FAULT27)-1
#define BIT_MASK_DERATING			(1<<BIT_SIZE_DERATING)-1
#define BIT_MASK_BUS_VOLTAGE		(1<<BIT_SIZE_BUS_VOLTAGE)-1
#define BIT_MASK_INV_TEMP1			(1<<BIT_SIZE_INV_TEMP1)-1
#define BIT_MASK_FAULT28			(1<<BIT_SIZE_FAULT28)-1
#define BIT_MASK_FAULT29			(1<<BIT_SIZE_FAULT29)-1
#define BIT_MASK_FAULT30			(1<<BIT_SIZE_FAULT30)-1
#define BIT_MASK_FAULT31			(1<<BIT_SIZE_FAULT31)-1
#define BIT_MASK_OPERATING_MODE02 	(1<<BIT_SIZE_OPERATING_MODE02)-1

#define BUS_VLTG_STAT_PTR	&Gu8CanTx_BusVoltage_stat
#define INV_TEMP1_STAT_PTR	&Gu8CanTx_InvTemp1_stat
#define DERATING_STAT_PTR	&Gu8CanTx_Derating_stat

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

IFX_EXTERN State GeCanTx_OpertingMode;
IFX_EXTERN uint8 Gu8CanTx_SWVersion;

IFX_EXTERN bool GbCanTx_Fault26;
IFX_EXTERN bool GbCanTx_Fault27;

IFX_EXTERN float GfCanTx_BusVoltage;
IFX_EXTERN uint8 Gu8CanTx_BusVoltage;
IFX_EXTERN esignal_stat Gu8CanTx_BusVoltage_stat;

IFX_EXTERN float GfCanTx_Derating;
IFX_EXTERN uint8_t Gu8CanTx_Derating;
IFX_EXTERN signalCfg_t GstCanTx_Derating;

IFX_EXTERN float GfCanTx_InvTemp1;
IFX_EXTERN uint8 Gu8CanTx_InvTemp1;
IFX_EXTERN signalCfg_t GstCanTx_InvTemp1;

IFX_EXTERN bool GbCanTx_Fault28;
IFX_EXTERN bool GbCanTx_Fault29;
IFX_EXTERN bool GbCanTx_Fault30;
IFX_EXTERN bool GbCanTx_Fault31;

#elif (CUSTOMER_ID == AUDI)

/*****************************MSG_AF95444********************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/
#define MSG_ID_FDBK_1						0xAF95444

#define BIT_START_POS_TpMosfet    			0
#define BIT_START_POS_ePwrDownState			10
#define BIT_START_POS_eCState				12
#define BIT_START_POS_EleFailure			14
#define BIT_START_POS_MechFailure			16
#define BIT_START_POS_CAN_ToutFailure		18
#define BIT_START_POS_OverTempFailure		20
#define BIT_START_POS_OverVoltFailure		22
#define BIT_START_POS_UnderVoltFailure		24
#define BIT_START_POS_Temperature			26

#define BIT_SIZE_TpMosfet					8
#define BIT_SIZE_ePwrDownState				2
#define BIT_SIZE_eCState					2
#define BIT_SIZE_EleFailure					2
#define BIT_SIZE_MechFailure				2
#define BIT_SIZE_CAN_ToutFailure			2
#define BIT_SIZE_OverTempFailure			2
#define BIT_SIZE_OverVoltFailure			2
#define BIT_SIZE_UnderVoltFailure			2
#define BIT_SIZE_Temperature				8

#define BIT_MASK_TpMosfet				((1<<BIT_SIZE_TpMosfet)-1)
#define BIT_MASK_ePwrDownState			((1<<BIT_SIZE_ePwrDownState)-1)
#define BIT_MASK_eCState				((1<<BIT_SIZE_eCState)-1)
#define BIT_MASK_EleFailure  			((1<<BIT_SIZE_EleFailure)-1)
#define BIT_MASK_MechFailure			((1<<BIT_SIZE_MechFailure)-1)
#define BIT_MASK_CAN_ToutFailure		((1<<BIT_SIZE_CAN_ToutFailure)-1)
#define BIT_MASK_OverTempFailure		((1<<BIT_SIZE_OverTempFailure)-1)
#define BIT_MASK_OverVoltFailure		((1<<BIT_SIZE_OverVoltFailure)-1)
#define BIT_MASK_UnderVoltFailure		((1<<BIT_SIZE_UnderVoltFailure)-1)
#define BIT_MASK_Temperature			((1<<BIT_SIZE_Temperature)-1)

#define TpMosfet_STAT_PTR			&GeCanTx_TpMosfet_stat
#define Temperature_STAT_PTR 		&GeCanTx_Temperature_stat

typedef enum {TestNotCompleted=0, TestCompleted_PASS=1, TestCompleted_FAIL=3} eTestStatus;
typedef enum {Ready=0, Other=1, Shutdown=3} ePwrDnState;


#define CAN_STATUS_TEST_NOT_COMPLETED_MAX_COUNT		(1000)

inline eTestStatus GetTestStatus(boolean bStatus, uint16 count )
{
	eTestStatus		status;

	if (count < 1)
	{
		status = TestNotCompleted;
	}
	else if (bStatus)
	{
		status = TestCompleted_FAIL;
	}
	else
	{
		status = TestCompleted_PASS;
	}

	return status;
}

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
IFX_EXTERN ePwrDnState GeCanTx_ePwrDnState;
IFX_EXTERN eCState 	   GeCanTx_eCState;
IFX_EXTERN eTestStatus GeCanTx_EleFailure;
IFX_EXTERN eTestStatus GeCanTx_MechFailure;
IFX_EXTERN eTestStatus GeCanTx_CanToutFailure;
IFX_EXTERN eTestStatus GeCanTx_OverTempFailure;
IFX_EXTERN eTestStatus GeCanTx_OverVoltFailure;
IFX_EXTERN eTestStatus GeCanTx_UnderVoltFailure;

IFX_EXTERN float GfCanTx_TpMosfet;
IFX_EXTERN float GfCanTx_Temperature;

#endif /* CUSTOMER_ID */

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

IFX_EXTERN void Init_FDBK_1(void);
IFX_EXTERN void Transmit_FDBK_1(void);


#endif /* APP_CAN_FDBK_1_H_ */
