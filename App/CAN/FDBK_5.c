/*
 * FDBK_5.c
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */
#include "FDBK_5.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

/*****************************MSG_18E14282h***************************************/
/*********************************************************************************/
static uint64 FDBK_5_data;

/*---------------------------------------------------------------------------------
 * Calibration
 * ------------------------------------------------------------------------------*/
signalCfg_t GstCanTx_TempIMSCfg;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
esignal_stat GeCanTx_TempIMS_stat;
float GfCanTx_TempIMS;
uint8 Gu8CanTx_TempIMS;
const signalCfg_t kst_TempIMS		= {0.50f, -254.0f, 254.0f, 254.0f, TEMP_IMS_STAT_PTR};

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

void Init_FDBK_5(void)
{
    CAN_Driver_MessageObjectInit(&g_MulticanBasic.drivers.can0SrcMsgObj5, MSG_ID_FDBK_5, IfxMultican_Frame_transmit, TRUE);

    GeCanTx_TempIMS_stat = OK;
	GfCanTx_TempIMS = 0.0f;
	Gu8CanTx_TempIMS = 0;

	GstCanTx_TempIMSCfg = kst_TempIMS;
}

static void Process_FDBK_5(void)
{
	memset(&FDBK_5_data, 0, sizeof(uint64));

	GfCanTx_TempIMS = GfMES_TempIMS;
	Process_TxSignal_u8(GfCanTx_TempIMS,	GstCanTx_TempIMSCfg, 	&Gu8CanTx_TempIMS);
	FDBK_5_data |= FILL(Gu8CanTx_TempIMS, BIT_MASK_TEMP_IMS, BIT_START_POS_TEMP_IMS);
	GeCanTxLink_MsgStat |= *(GstCanTx_TempIMSCfg.stat_ptr);
}

void Transmit_FDBK_5(void)
{
	uint32 dataLowTx  = 0x00000000;
	uint32 dataHighTx = 0x00000000;
	uint64 datawordTx = 0;
	IfxMultican_Message msg_buf;

	Process_FDBK_5();

	memcpy(&datawordTx, &FDBK_5_data, sizeof(uint64));

	dataLowTx 	= (uint32)datawordTx;
	dataHighTx 	= (uint64)datawordTx >> 32;

	IfxMultican_Message_init(&msg_buf, MSG_ID_FDBK_5, dataLowTx , dataHighTx, IfxMultican_DataLengthCode_8);
	IfxMultican_Can_MsgObj_sendMessage(&g_MulticanBasic.drivers.can0SrcMsgObj5, &msg_buf);
}

#else

void Init_FDBK_5(void)
{
	/* Do nothing */
}

void Transmit_FDBK_5(void)
{
	/* Do nothing */
}

#endif /* CUSTOMER_ID */

