/*
 * FDBK_8.h
 *
 *  Created on: May 13, 2021
 *
 */

#ifndef APP_CAN_FDBK_8_H_
#define APP_CAN_FDBK_8_H_


#include <Ifx_Types.h>
#include <Ifx_TypesReg.h>
#include <string.h>
#include "CAN_Driver.h"
#include "CAN_Interface_Tx.h"
#include "STM.h"

/*****************************MSG_18E15a78h***************************************/
/*********************************************************************************/

#define MSG_ID_FDBK_8					0x18E142A0

#define BIT_START_POS_PLATFORM_ID		0
#define BIT_START_POS_CUSTOMER_ID		8
#define BIT_START_POS_VERSION_MAJOR		16
#define BIT_START_POS_VERSION_MINOR		24
#define BIT_START_POS_VERSION_REV	   	32
#define BIT_START_POS_VERSION_CUSTOMER	40

#define BIT_SIZE_PLATFORM_ID			8
#define BIT_SIZE_CUSTOMER_ID			8
#define BIT_SIZE_VERSION_MAJOR			8
#define BIT_SIZE_VERSION_MINOR			8
#define BIT_SIZE_VERSION_REV			8
#define BIT_SIZE_VERSION_CUSTOMER		8

#define BIT_MASK_PLATFORM_ID			(1<<BIT_SIZE_PLATFORM_ID)-1
#define BIT_MASK_CUSTOMER_ID			(1<<BIT_SIZE_CUSTOMER_ID)-1
#define BIT_MASK_VERSION_MAJOR			(1<<BIT_SIZE_VERSION_MAJOR)-1
#define BIT_MASK_VERSION_MINOR			(1<<BIT_SIZE_VERSION_MINOR)-1
#define BIT_MASK_VERSION_REV			(1<<BIT_SIZE_VERSION_REV)-1
#define BIT_MASK_VERSION_CUSTOMER		(1<<BIT_SIZE_VERSION_CUSTOMER)-1


#define PLATFORM_ID_STAT_PTR 			&GeCanTx_PlatformId_stat
#define CUSTOMER_ID_STAT_PTR 			&GeCanTx_CustomerId_stat
#define VERSION_MAJOR_STAT_PTR 			&GeCanTx_VersionMajor_stat
#define VERSION_MINOR_STAT_PTR 			&GeCanTx_VersionMinor_stat
#define VERSION_REV_STAT_PTR 			&GeCanTx_VersionRev_stat
#define VERSION_CUSTOMER_STAT_PTR 		&GeCanTx_VersionCustomer_stat

#define FDBK_8_SEND_COUNT_MAX			(3)

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
IFX_EXTERN void Init_FDBK_8(void);
IFX_EXTERN void Transmit_FDBK_8(void);


#endif /* APP_CAN_FDBK_8_H_ */
