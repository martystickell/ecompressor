/*
 * FDBK_4.c
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */
#include "FDBK_4.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

/*****************************MSG_18E14281h***************************************/
/*********************************************************************************/

static uint64 FDBK_4_data;

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/

signalCfg_t GstCanTx_TempSens1Cfg;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

esignal_stat GeCanTx_TempSens1_stat;
float GfCanTx_TempSens1;
uint8 Gu8CanTx_TempSens1;
const signalCfg_t kst_TempSens1		= {0.50f, -254.0f, 254.0f, 254.0f, TEMP_SENS2_STAT_PTR};

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

void Init_FDBK_4(void)
{
    CAN_Driver_MessageObjectInit(&g_MulticanBasic.drivers.can0SrcMsgObj4, MSG_ID_FDBK_4, IfxMultican_Frame_transmit, TRUE);

    GeCanTx_TempSens1_stat = OK;
	GfCanTx_TempSens1 = 0.0f;
	Gu8CanTx_TempSens1 = 0;

	GstCanTx_TempSens1Cfg = kst_TempSens1;
}

static void Process_FDBK_4(void)
{
	memset(&FDBK_4_data, 0, sizeof(uint64));

	GfCanTx_TempSens1 = GfMES_TempSens1;
	Process_TxSignal_u8(GfCanTx_TempSens1,	GstCanTx_TempSens1Cfg, 	&Gu8CanTx_TempSens1);
	FDBK_4_data |= FILL(Gu8CanTx_TempSens1, BIT_MASK_TEMP_SENS2, BIT_START_POS_TEMP_SENS2);
	GeCanTxLink_MsgStat |= *(GstCanTx_TempSens1Cfg.stat_ptr);
}

void Transmit_FDBK_4(void)
{
	uint32 dataLowTx  = 0x00000000;
	uint32 dataHighTx = 0x00000000;
	uint64 datawordTx = 0;
	IfxMultican_Message msg_buf;

	Process_FDBK_4();

	memcpy(&datawordTx, &FDBK_4_data, sizeof(uint64));

	dataLowTx 	= (uint32)datawordTx;
	dataHighTx 	= (uint64)datawordTx >> 32;

	IfxMultican_Message_init(&msg_buf, MSG_ID_FDBK_4, dataLowTx , dataHighTx, IfxMultican_DataLengthCode_8);
	IfxMultican_Can_MsgObj_sendMessage(&g_MulticanBasic.drivers.can0SrcMsgObj4, &msg_buf);
}

#else

void Init_FDBK_4(void)
{
	/* Do nothing */
}

void Transmit_FDBK_4(void)
{
	/* Do nothing */
}

#endif /* CUSTOMER_ID */
