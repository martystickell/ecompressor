/*
 * FDBK_6.c
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */
#include "FDBK_6.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

/*****************************MSG_18E14285h***************************************/
/*********************************************************************************/
static uint64 FDBK_6_data;

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/
signalCfg_t GstCanTx_TempBdr1Cfg;
signalCfg_t GstCanTx_TempBdr2Cfg;
/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
esignal_stat GeCanTx_TempBdr1_stat;
float GfCanTx_TempBdr1;
uint8 Gu8CanTx_TempBdr1;
const signalCfg_t kst_TempBdr1		= {0.50f, -254.0f, 254.0f, 254.0f, TEMP_BDR1_STAT_PTR};

esignal_stat GeCanTx_TempBdr2_stat;
float GfCanTx_TempBdr2;
uint8 Gu8CanTx_TempBdr2;
const signalCfg_t kst_TempBdr2		= {0.50f, -254.0f, 254.0f, 254.0f, TEMP_BDR1_STAT_PTR};


/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
void Init_FDBK_6(void)
{
    CAN_Driver_MessageObjectInit(&g_MulticanBasic.drivers.can0SrcMsgObj6, MSG_ID_FDBK_6, IfxMultican_Frame_transmit, TRUE);

    GeCanTx_TempBdr1_stat = OK;
	GfCanTx_TempBdr1 = 0.0f;
	Gu8CanTx_TempBdr1 = 0;
	GstCanTx_TempBdr1Cfg = kst_TempBdr1;

	GeCanTx_TempBdr2_stat 	= OK;
	GfCanTx_TempBdr2 		= 0.0f;
	Gu8CanTx_TempBdr2 		= 0;
	GstCanTx_TempBdr2Cfg 	= kst_TempBdr2;
}


static void Process_FDBK_6(void)
{
	memset(&FDBK_6_data, 0, sizeof(uint64));

	GfCanTx_TempBdr1 = GfMES_TempBdr1;
	Process_TxSignal_u8(GfCanTx_TempBdr1,	GstCanTx_TempBdr1Cfg, 	&Gu8CanTx_TempBdr1);
	FDBK_6_data |= FILL(Gu8CanTx_TempBdr1, BIT_MASK_TEMP_BDR1, BIT_START_POS_TEMP_BDR1);
	GeCanTxLink_MsgStat |= *(GstCanTx_TempBdr1Cfg.stat_ptr);

	GfCanTx_TempBdr2 = GfMES_TempBdr2;
	Process_TxSignal_u8(GfCanTx_TempBdr2,	GstCanTx_TempBdr2Cfg, 	&Gu8CanTx_TempBdr2);
	FDBK_6_data |= FILL(Gu8CanTx_TempBdr2, BIT_MASK_TEMP_BDR2, BIT_START_POS_TEMP_BDR2);
	GeCanTxLink_MsgStat |= *(GstCanTx_TempBdr2Cfg.stat_ptr);
}


void Transmit_FDBK_6(void)
{
	uint32 dataLowTx  = 0x00000000;
	uint32 dataHighTx = 0x00000000;
	uint64 datawordTx = 0;
	IfxMultican_Message msg_buf;

	Process_FDBK_6();

	memcpy(&datawordTx, &FDBK_6_data, sizeof(uint64));

	dataLowTx 	= (uint32)datawordTx;
	dataHighTx 	= (uint64)datawordTx >> 32;

	IfxMultican_Message_init(&msg_buf, MSG_ID_FDBK_6, dataLowTx , dataHighTx, IfxMultican_DataLengthCode_8);
	IfxMultican_Can_MsgObj_sendMessage(&g_MulticanBasic.drivers.can0SrcMsgObj6, &msg_buf);
}

#else

void Init_FDBK_6(void)
{
	/* Do nothing */
}

void Transmit_FDBK_6(void)
{
	/* Do nothing */
}

#endif /* CUSTOMER_ID */

