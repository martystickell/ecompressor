/*
 * CMD_0.c
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */


#include "CMD_0.h"
#include "CAN_Driver.h"
#include "CAN_Interface_Rx.h"

static void Process_CMD_0(void);

volatile uint64 datawordCMD0;
static IfxMultican_Status GeCanRx_CMD_0_ReadStatus;
uint16 Gu16CANRx_CMD0TimeOutCtr = 0;
bool GbCANRx_CMD0TimeOutSWT2 = FALSE;



#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

/*****************************MSG_06Dh********************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/

signalCfg_t GstCanRx_SpeedCmd;
uint16 ku16CANRx_CMD0TimeOutMax = 100;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

float GfCanRx_SpeedCmd;
esignal_stat GeCanRx_SpeedCmd_stat;
uint16 Gu16CanRx_SpeedCmd;

const signalCfg_t kst_SpdCmd 		= {50.0f, 0.0f, 204700.0f, 0.0f, SPD_CMD_STAT_PTR};

State GeCanRx_CommandBits;
State CommandBit_Max;
State CommandBit_Min;
esignal_stat GeCanRx_CMDBits_stat;

bool GbCanRx_MotoringEnable;
bool GbCanRx_GeneratingEnable;
bool GbCanRx_ResetFaults;
bool GbCanRx_TrackingEnable;
bool GbCanRx_TorqueCmdMode;
bool GbCanRx_MotoringGeneratingEnable;

bool GbCanRx_CMDBit5;
bool GbCanRx_CMDBit6;
bool GbCanRx_CMDBit7;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

void Init_CMD_0(void)
{
    CAN_Driver_MessageObjectInit(&g_MulticanBasic.drivers.can0DstMsgObj0, MSG_ID_CMD_0, IfxMultican_Frame_receive, TRUE);

    GstCanRx_SpeedCmd = kst_SpdCmd;
	Gu16CanRx_SpeedCmd = 0;
	GfCanRx_SpeedCmd = 0;
	GeCanRx_SpeedCmd_stat = OK;

	CommandBit_Max = Fault;
	CommandBit_Min = Off;
	GeCanRx_CommandBits  = Off;
	GeCanRx_CMDBits_stat = OK;

	GbCanRx_CMDBit5 = 0x0;
	GbCanRx_CMDBit6 = 0x0;
	GbCanRx_CMDBit7 = 0x0;

	GbCanRx_MotoringEnable	 			= false;
	GbCanRx_GeneratingEnable 			= false;
	GbCanRx_ResetFaults					= false;
	GbCanRx_MotoringGeneratingEnable 	= false;
	GbCanRx_TrackingEnable 				= false;
	GbCanRx_TorqueCmdMode				= false;

	GbCANRx_CMD0TimeOutSWT2 = FALSE;
	Gu16CANRx_CMD0TimeOutCtr = 0;
}

void Receive_CMD_0(void)
{
	uint32 dataLowRx  = 0x00000000;
	uint32 dataHighRx = 0x00000000;

	IfxMultican_Message msg_buf;

	datawordCMD0 = 0;

	IfxMultican_Message_init(&msg_buf, 0xdead, 0xdeadbeef, 0xdeadbeef, IfxMultican_DataLengthCode_8); /* start with invalid values */
	GeCanRx_CMD_0_ReadStatus = IfxMultican_Can_MsgObj_readMessage(&g_MulticanBasic.drivers.can0DstMsgObj0, &msg_buf);

	/* all the following status messages mean a new message is received */
	if
	(
		GeCanRx_CMD_0_ReadStatus == IfxMultican_Status_newDataButOneLost || \
		GeCanRx_CMD_0_ReadStatus == IfxMultican_Status_newData|| \
		GeCanRx_CMD_0_ReadStatus == IfxMultican_Status_ok
	)
	{
		dataLowRx 		= (uint32)msg_buf.data[0];
		dataHighRx 		= (uint32)msg_buf.data[1];
		datawordCMD0 	= ((uint64)dataHighRx<<32 | (uint64)dataLowRx);

		GbCANRx_CMD0TimeOutSWT2 = FALSE;
		Gu16CANRx_CMD0TimeOutCtr = 0;

		Process_CMD_0();
	}
	else
	{
		if(Gu16CANRx_CMD0TimeOutCtr > ku16CANRx_CMD0TimeOutMax)
		{
			GbCANRx_CMD0TimeOutSWT2 = TRUE;
		}
		else
		{
			Gu16CANRx_CMD0TimeOutCtr++;
		}
	}
}

static void Process_CMD_0(void)
{
	Gu16CanRx_SpeedCmd = (uint16)(EXTRACT(datawordCMD0, BIT_MASK_SPD_CMD, BIT_START_POS_SPD_CMD));
	Process_RxSignal_u16(Gu16CanRx_SpeedCmd, GstCanRx_SpeedCmd, &GfCanRx_SpeedCmd);

	GbCanRx_MotoringEnable 		= (bool)(EXTRACT(datawordCMD0, BIT_MASK_CMD_BIT0, BIT_START_POS_CMD_BIT0));
	GbCanRx_GeneratingEnable 	= (bool)(EXTRACT(datawordCMD0, BIT_MASK_CMD_BIT1, BIT_START_POS_CMD_BIT1));
	GbCanRx_ResetFaults 		= (bool)(EXTRACT(datawordCMD0, BIT_MASK_CMD_BIT2, BIT_START_POS_CMD_BIT2));
	GbCanRx_TrackingEnable		= (bool)(EXTRACT(datawordCMD0, BIT_MASK_CMD_BIT3, BIT_START_POS_CMD_BIT3));
	GbCanRx_TorqueCmdMode		= (bool)(EXTRACT(datawordCMD0, BIT_MASK_CMD_BIT4, BIT_START_POS_CMD_BIT4));
	GbCanRx_CMDBit5 			= (bool)(EXTRACT(datawordCMD0, BIT_MASK_CMD_BIT5, BIT_START_POS_CMD_BIT5));
	GbCanRx_CMDBit6 			= (bool)(EXTRACT(datawordCMD0, BIT_MASK_CMD_BIT6, BIT_START_POS_CMD_BIT6));
	GbCanRx_CMDBit7 			= (bool)(EXTRACT(datawordCMD0, BIT_MASK_CMD_BIT7, BIT_START_POS_CMD_BIT7));

	GeCanRxLink_MsgStat |= *(GstCanRx_SpeedCmd.stat_ptr);
}

#elif (CUSTOMER_ID == AUDI)

/*******************ClampControl_01_XIX_Motor_SUBCAN- 0xAF95445*******************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/

signalCfg_t GstCanRx_SpeedCmd;
uint16 ku16CANRx_CMD0TimeOutMax = 100;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

float GfCanRx_SpeedCmd;
esignal_stat GeCanRx_SpeedCmd_stat;
uint16 Gu16CanRx_SpeedCmd;

eCState GeCanRx_eCState;
uint8 Gu8CanRx_eCState;

const signalCfg_t kst_SpdCmd 		= {2.0f, 0.0f, 131070.0f, 0.0f, SPD_CMD_STAT_PTR};
State GeCanRx_CommandBits;
State CommandBit_Max;
State CommandBit_Min;
esignal_stat GeCanRx_CMDBits_stat;


/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

void Init_CMD_0(void)
{
    CAN_Driver_MessageObjectInit(&g_MulticanBasic.drivers.can0DstMsgObj0, MSG_ID_CMD_0, IfxMultican_Frame_receive, FALSE);

	GstCanRx_SpeedCmd = kst_SpdCmd;
	Gu16CanRx_SpeedCmd = 0;
	GfCanRx_SpeedCmd = 0;
	GeCanRx_SpeedCmd_stat = OK;

	GeCanRx_eCState = eCStateInit;

	CommandBit_Max = Fault;
	CommandBit_Min = Off;
	GeCanRx_CommandBits  = Off;
	GeCanRx_CMDBits_stat = OK;

	GbCANRx_CMD0TimeOutSWT2 = FALSE;
	Gu16CANRx_CMD0TimeOutCtr = 0;
}

void Receive_CMD_0(void)
{
	uint32 dataLowRx  = 0x00000000;
	uint32 dataHighRx = 0x00000000;

	IfxMultican_Message msg_buf;

	datawordCMD0 = 0;

	IfxMultican_Message_init(&msg_buf, 0xdead, 0xdeadbeef, 0xdeadbeef, IfxMultican_DataLengthCode_8); /* start with invalid values */
	GeCanRx_CMD_0_ReadStatus = IfxMultican_Can_MsgObj_readMessage(&g_MulticanBasic.drivers.can0DstMsgObj0, &msg_buf);

	/* all the following status messages mean a new message is received */
	if
	(
		GeCanRx_CMD_0_ReadStatus == IfxMultican_Status_newDataButOneLost || \
		GeCanRx_CMD_0_ReadStatus == IfxMultican_Status_newData|| \
		GeCanRx_CMD_0_ReadStatus == IfxMultican_Status_ok
	)
	{
		dataLowRx 		= (uint32)msg_buf.data[0];
		dataHighRx 		= (uint32)msg_buf.data[1];
		datawordCMD0 	= ((uint64)dataHighRx<<32 | (uint64)dataLowRx);

		GbCANRx_CMD0TimeOutSWT2 = FALSE;
		Gu16CANRx_CMD0TimeOutCtr = 0;

		Process_CMD_0();
	}
	else
	{
		if(Gu16CANRx_CMD0TimeOutCtr > ku16CANRx_CMD0TimeOutMax)
		{
			GbCANRx_CMD0TimeOutSWT2 = TRUE;
		}
		else
		{
			Gu16CANRx_CMD0TimeOutCtr++;
		}
	}
}

static void Process_CMD_0(void)
{
	Gu16CanRx_SpeedCmd 	= (uint16)(EXTRACT(datawordCMD0, BIT_MASK_SPD_CMD, BIT_START_POS_SPD_CMD));
	Process_RxSignal_u16(Gu16CanRx_SpeedCmd, GstCanRx_SpeedCmd, &GfCanRx_SpeedCmd);
	GeCanRxLink_MsgStat |= *(GstCanRx_SpeedCmd.stat_ptr);

#if 0
	GeCanRx_eCState = (eCState)(EXTRACT(datawordCMD0, BIT_MASK_eC_STATE, BIT_START_POS_eC_STATE));
	GeCanRxLink_MsgStat |= OK;
#else
	// Override the CAN State Command per the request of the customer
	GeCanRx_eCState = eCStateActive;
#endif
}

#else

void Init_CMD_0(void)
{
	GbCANRx_CMD0TimeOutSWT2 = FALSE;
	Gu16CANRx_CMD0TimeOutCtr = 0;
}

void Receive_CMD_0(void)
{
	/* Do Nothing */
}

#endif /* CUSTOMER_ID */
