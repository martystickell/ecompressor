/*
 * CMD_2.c
 *
 *  Created on: Nov 4, 2018
 *      Author: H297270
 */
#include "CMD_2.h"

static void Process_CMD_2(void);

volatile uint64 datawordCMD2 = 0;
uint16 Gu16CANRx_CMD2TimeOutCtr = 0;
bool GbCANRx_CMD2TimeOutSWT2 = FALSE;


#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

/*****************************MSG_29Ch********************************************/
/*********************************************************************************/

static IfxMultican_Status GeCanRx_CMD_2_ReadStatus;

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/
signalCfg_t GstCanRx_DCOutputCurrLimitCfg;
signalCfg_t GstCanRx_DCInputCurrLimitCfg;
signalCfg_t GstCanRx_DCOutputPowerLimitCfg;
signalCfg_t GstCanRx_DCInputPowerLimitCfg;
signalCfg_t GstCanRx_WaterTempCfg;
signalCfg_t GstCanRx_CompInletTempCfg;

/* input - motoring */
float kCANRx_DCIPCurrLimit_Default	= 156.250f; 	/* 48v * 156.25A ~ 7.5kw - motoring */
float kCANRx_DCIPPwrLimit_Default	= 7500.0f;

/* output - generating */
float kCANRx_DCOPCurrLimit_Default	= 0.0f;		/* 48v * 156.25A ~ 7.5kw - generating */
float kCANRx_DCOPPwrLimit_Default	= 0.0f;


/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
const signalCfg_t kst_DCOPCurrLimit = {0.4f, 0.0f, 204.000f, 0.0f, 		DCOP_CURR_LIMIT_STAT_PTR};
const signalCfg_t kst_DCIPCurrLimit	= {0.4f, 0.0f, 204.000f, 0.0f, 		DCIP_CURR_LIMIT_STAT_PTR};
const signalCfg_t kst_DCOPPwrLimit	= {10.0f, 0.0f, 10220.0f, 0.0f, 	DCOP_PWR_LIMIT_STAT_PTR};
const signalCfg_t kst_DCIPPwrLimit 	= {10.0f, 0.0f, 10220.0f, 0.0f, 	DCIP_PWR_LIMIT_STAT_PTR};
const signalCfg_t kst_WaterTemp		= {1.0f, -254.0f, 256.00f, -254.0f, WATER_TEMP_STAT_PTR};
const signalCfg_t kst_CompInletTemp = {1.0f, -254.0f, 256.00f, -254.0f, COMP_INLET_TEMP_STAT_PTR};

uint16 Gu16CanRx_DCOutputCurrLimit;
float GfCanRx_DCOutputCurrLimit;
esignal_stat GeCanRx_DCOutputCurrLimit_stat;

uint16 Gu16CanRx_DCInputCurrLimit;
float GfCanRx_DCInputCurrLimit;
esignal_stat GeCanRx_DCInputCurrLimit_stat;

uint16 Gu16CanRx_DCOutputPowerLimit;
float GfCanRx_DCOutputPowerLimit;
esignal_stat GeCanRx_DCOutputPowerLimit_stat;

uint16 Gu16CanRx_DCInputPowerLimit;
float GfCanRx_DCInputPowerLimit;
esignal_stat GeCanRx_DCInputPowerLimit_stat;

uint16 Gu16CanRx_WaterTemp;
float GfCanRx_WaterTemp;
esignal_stat GeCanRx_WaterTemp_stat;

uint16 Gu16CanRx_CompInletTemp;
float GfCanRx_CompInletTemp;
esignal_stat GeCanRx_CompInletTemp_stat;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

void Init_CMD_2(void)
{
    CAN_Driver_MessageObjectInit(&g_MulticanBasic.drivers.can0DstMsgObj2, MSG_ID_CMD_2, IfxMultican_Frame_receive, TRUE);

    Gu16CanRx_DCOutputCurrLimit = (uint16)((kCANRx_DCOPCurrLimit_Default + kst_DCOPCurrLimit.offset) * DCCURRLIMIT_SF_INV);
	GfCanRx_DCOutputCurrLimit = kCANRx_DCOPCurrLimit_Default;
	GeCanRx_DCOutputCurrLimit_stat = OK;
	GstCanRx_DCOutputCurrLimitCfg = kst_DCOPCurrLimit;

	Gu16CanRx_DCInputCurrLimit = (uint16)((kCANRx_DCIPCurrLimit_Default + kst_DCIPCurrLimit.offset) * DCCURRLIMIT_SF_INV);
	GfCanRx_DCInputCurrLimit = kCANRx_DCIPCurrLimit_Default;
	GeCanRx_DCInputCurrLimit_stat  = OK;
	GstCanRx_DCInputCurrLimitCfg = kst_DCIPCurrLimit;

	Gu16CanRx_DCOutputPowerLimit = (uint16)((kCANRx_DCOPPwrLimit_Default + kst_DCOPPwrLimit.offset) * DCPWRLIMIT_SF_INV);
	GfCanRx_DCOutputPowerLimit = kCANRx_DCOPPwrLimit_Default;
	GeCanRx_DCOutputPowerLimit_stat = OK;
	GstCanRx_DCOutputPowerLimitCfg = kst_DCOPPwrLimit;

	Gu16CanRx_DCInputPowerLimit = (uint16)((kCANRx_DCIPPwrLimit_Default + kst_DCIPPwrLimit.offset) * DCPWRLIMIT_SF_INV);
	GfCanRx_DCInputPowerLimit = kCANRx_DCIPPwrLimit_Default;
	GeCanRx_DCInputPowerLimit_stat = OK;
	GstCanRx_DCInputPowerLimitCfg = kst_DCIPPwrLimit;

	Gu16CanRx_WaterTemp = 0;
	GfCanRx_WaterTemp = 0;
	GeCanRx_WaterTemp_stat = OK;
	GstCanRx_WaterTempCfg = kst_WaterTemp;

	Gu16CanRx_CompInletTemp = 0;
	GfCanRx_CompInletTemp = 0;
	GeCanRx_CompInletTemp_stat = OK;
	GstCanRx_CompInletTempCfg = kst_CompInletTemp;

	GbCANRx_CMD2TimeOutSWT2 = FALSE;
	Gu16CANRx_CMD2TimeOutCtr = 0;
}

void Receive_CMD_2(void)
{
	uint32 dataLowRx  = 0x00000000;
	uint32 dataHighRx = 0x00000000;

	IfxMultican_Message msg_buf;

	datawordCMD2 = 0;

	IfxMultican_Message_init(&msg_buf, 0xdead, 0xdeadbeef, 0xdeadbeef, IfxMultican_DataLengthCode_8); /* start with invalid values */
	GeCanRx_CMD_2_ReadStatus = IfxMultican_Can_MsgObj_readMessage(&g_MulticanBasic.drivers.can0DstMsgObj2, &msg_buf);

	/* all the following status messages mean a new message is received */
	if
	(
		GeCanRx_CMD_2_ReadStatus == IfxMultican_Status_newDataButOneLost || \
		GeCanRx_CMD_2_ReadStatus == IfxMultican_Status_newData|| \
		GeCanRx_CMD_2_ReadStatus == IfxMultican_Status_ok
	)
	{
		dataLowRx 	= (uint32)msg_buf.data[0];
		dataHighRx 	= (uint32)msg_buf.data[1];
		datawordCMD2 = ((uint64)dataHighRx<<32 | (uint64)dataLowRx);

		GbCANRx_CMD2TimeOutSWT2 = FALSE;
		Gu16CANRx_CMD2TimeOutCtr = 0;

		Process_CMD_2();
	}
	else
	{
		/* do nothing */
	}
}

static void Process_CMD_2(void)
{

	Gu16CanRx_DCOutputCurrLimit 	= (uint16)(EXTRACT(datawordCMD2, BIT_MASK_DCOP_CURR_LIMIT, BIT_START_DCOP_CURR_LIMIT));
	Process_RxSignal_u16((uint16)Gu16CanRx_DCOutputCurrLimit, GstCanRx_DCOutputCurrLimitCfg, &GfCanRx_DCOutputCurrLimit);
	GeCanRxLink_MsgStat |= *(GstCanRx_DCOutputCurrLimitCfg.stat_ptr);

	Gu16CanRx_DCInputCurrLimit 		= (uint16)(EXTRACT(datawordCMD2, BIT_MASK_DCIP_CURR_LIMIT, BIT_START_DCIP_CURR_LIMIT));
	Process_RxSignal_u16((uint16)Gu16CanRx_DCInputCurrLimit, GstCanRx_DCInputCurrLimitCfg, &GfCanRx_DCInputCurrLimit);
	GeCanRxLink_MsgStat |= *(GstCanRx_DCInputCurrLimitCfg.stat_ptr);

	Gu16CanRx_DCOutputPowerLimit 	= (uint16)(EXTRACT(datawordCMD2, BIT_MASK_DCOP_PWR_LIMIT, BIT_START_DCOP_PWR_LIMIT));
	Process_RxSignal_u16((uint16)Gu16CanRx_DCOutputPowerLimit, GstCanRx_DCOutputPowerLimitCfg, &GfCanRx_DCOutputPowerLimit);
	GeCanRxLink_MsgStat |= *(GstCanRx_DCOutputPowerLimitCfg.stat_ptr);

	Gu16CanRx_DCInputPowerLimit 	= (uint16)(EXTRACT(datawordCMD2, BIT_MASK_DCIP_PWR_LIMIT, BIT_START_DCIP_PWR_LIMIT));
	Process_RxSignal_u16((uint16)Gu16CanRx_DCInputPowerLimit, GstCanRx_DCInputPowerLimitCfg, &GfCanRx_DCInputPowerLimit);
	GeCanRxLink_MsgStat |= *(GstCanRx_DCInputPowerLimitCfg.stat_ptr);

	Gu16CanRx_WaterTemp 	= (uint16)(EXTRACT(datawordCMD2, BIT_MASK_WATER_TEMP, BIT_START_WATER_TEMP));
	Process_RxSignal_u16((uint16)Gu16CanRx_WaterTemp, GstCanRx_WaterTempCfg, &GfCanRx_WaterTemp);
	GeCanRxLink_MsgStat |= *(GstCanRx_WaterTempCfg.stat_ptr);

	Gu16CanRx_CompInletTemp 	= (uint16)(EXTRACT(datawordCMD2, BIT_MASK_COMP_INLET_TEMP, BIT_START_COMP_INLET_TEMP));
	Process_RxSignal_u16((uint16)Gu16CanRx_CompInletTemp, GstCanRx_CompInletTempCfg, &GfCanRx_CompInletTemp);
	GeCanRxLink_MsgStat |= *(GstCanRx_CompInletTempCfg.stat_ptr);
}

#else

void Init_CMD_2(void)
{
	GbCANRx_CMD2TimeOutSWT2 = FALSE;
	Gu16CANRx_CMD2TimeOutCtr = 0;
}

void Receive_CMD_2(void)
{
	/* Do Nothing */
}

#endif /* CUSTOMER_ID */
