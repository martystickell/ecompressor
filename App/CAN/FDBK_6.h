/*
 *FDBK_6.h
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */

#ifndef APP_CAN_FDBK_6_H_
#define APP_CAN_FDBK_6_H_

#include <Ifx_Types.h>
#include <Ifx_TypesReg.h>
#include <string.h>
#include "CAN_Driver.h"
#include "CAN_Interface_Tx.h"
#include "STM.h"
#include "MES.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)
/*****************************MSG_18E14285h***************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/
#define MSG_ID_FDBK_6				0x18E14285

#define BIT_START_POS_TEMP_BDR1		0
#define BIT_SIZE_TEMP_BDR1			8
#define BIT_MASK_TEMP_BDR1			(1<<BIT_SIZE_TEMP_BDR1)-1

#define BIT_START_POS_TEMP_BDR2		8
#define BIT_SIZE_TEMP_BDR2			8
#define BIT_MASK_TEMP_BDR2			(1<<BIT_SIZE_TEMP_BDR2)-1

#define TEMP_BDR1_STAT_PTR 			&GeCanTx_TempBdr1_stat
#define TEMP_BDR2_STAT_PTR 			&GeCanTx_TempBdr2_stat
/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

IFX_EXTERN esignal_stat GeCanTx_TempBdr1_stat;
IFX_EXTERN float GfCanTx_TempBdr1;

IFX_EXTERN esignal_stat GeCanTx_TempBdr2_stat;
IFX_EXTERN float GfCanTx_TempBdr2;

#endif /* CUSTOMER_ID */

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
IFX_EXTERN void Init_FDBK_6(void);
IFX_EXTERN void Transmit_FDBK_6(void);


#endif /* APP_CAN_FDBK_6_H_ */
