/*
 * CAN_Interface.c
 *
 *  Created on: Oct 15, 2018
 *      Author: H297270
 */

#include "CAN_Interface_Tx.h"

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
emsg_stat GeCanTxLink_MsgStat = GOOD;


/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

void Process_TxSignal_u8(float input, signalCfg_t sigConfig, uint8 *result_ptr)
{
	if(result_ptr != NULL)
	{
		if(LIMIT(input,sigConfig.min,sigConfig.max))
		{
			*result_ptr = (uint8)((input + sigConfig.offset) * sigConfig.res);
			*(sigConfig.stat_ptr) = OK;
		}
		else if(input > sigConfig.max)
		{
			*result_ptr = (uint8)((sigConfig.max + sigConfig.offset) * sigConfig.res);
			*(sigConfig.stat_ptr) = OOR_HIGH;
		}
		else
		{
			*result_ptr = (uint8)((sigConfig.min + sigConfig.offset) * sigConfig.res);
			*(sigConfig.stat_ptr) = OOR_LOW;
		}
	}
	else
	{
		/* do nothing */
	}
}

void Process_TxSignal_s8(float input, signalCfg_t sigConfig, sint8 *result_ptr)
{
	if(result_ptr != NULL)
	{
		if(LIMIT(input,sigConfig.min,sigConfig.max))
		{
			*result_ptr = (sint8)((input * sigConfig.res) + sigConfig.offset);
			*(sigConfig.stat_ptr) = OK;
		}
		else if(input > sigConfig.max)
		{
			*result_ptr = (sint8)((sigConfig.max * sigConfig.res) + sigConfig.offset);
			*(sigConfig.stat_ptr) = OOR_HIGH;
		}
		else
		{
			*result_ptr = (sint8)((sigConfig.min * sigConfig.res) + sigConfig.offset);
			*(sigConfig.stat_ptr) = OOR_LOW;
		}
	}
	else
	{
		/* do nothing */
	}
}

void Process_TxSignal_u16(float input, signalCfg_t sigConfig, uint16 *result_ptr)
{
	if(result_ptr != NULL)
	{
		if(LIMIT(input,sigConfig.min,sigConfig.max))
		{
			*result_ptr = (uint16)((input + sigConfig.offset) * sigConfig.res);
			*(sigConfig.stat_ptr) = OK;
		}
		else if(input > sigConfig.max)
		{
			*result_ptr = (uint16)((sigConfig.max + sigConfig.offset) * sigConfig.res);
			*(sigConfig.stat_ptr) = OOR_HIGH;
		}
		else
		{
			*result_ptr = (uint16)((sigConfig.min + sigConfig.offset) * sigConfig.res);
			*(sigConfig.stat_ptr) = OOR_LOW;
		}
	}
	else
	{
		/* do nothing */
	}
}
