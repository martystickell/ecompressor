/*
 * CMD_1.h
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */

#ifndef APP_CAN_CMD_1_H_
#define APP_CAN_CMD_1_H_

#include "CAN_Interface_Rx.h"
#include <string.h>
#include "CAN_Driver.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

/*****************************MSG_18E05841h***************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/

#define MSG_ID_CMD_1					0x18E05841

#define BIT_START_POS_SLEW_RATE_UP		0
#define BIT_SIZE_SLEW_RATE_UP			8
#define BIT_MASK_SLEW_RATE_UP			((1<<(BIT_SIZE_SLEW_RATE_UP))-1)

#define BIT_START_POS_SLEW_RATE_DOWN	8
#define BIT_SIZE_SLEW_RATE_DOWN			8
#define BIT_MASK_SLEW_RATE_DOWN			((1<<(BIT_SIZE_SLEW_RATE_DOWN))-1)

#define BIT_START_POS_MOT_CURR_LIMIT	16
#define BIT_SIZE_MOT_CURR_LIMIT			8
#define BIT_MASK_MOT_CURR_LIMIT			((1<<(BIT_SIZE_MOT_CURR_LIMIT))-1)

#define BIT_START_POS_GEN_CURR_LIMIT	24
#define BIT_SIZE_GEN_CURR_LIMIT			8
#define BIT_MASK_GEN_CURR_LIMIT			((1<<(BIT_SIZE_GEN_CURR_LIMIT))-1)

#define BIT_START_POS_CAN_XMIT_RATE		32
#define BIT_SIZE_CAN_XMIT_RATE			8
#define BIT_MASK_CAN_XMIT_RATE			((1<<(BIT_SIZE_CAN_XMIT_RATE))-1)

#define SLEW_RATE_UP_STAT_PTR			&GeCanRx_SlewRateUp_stat
#define SLEW_RATE_DOWN_STAT_PTR			&GeCanRx_SlewRateDown_stat
#define CAN_XMIT_RATE_STAT_PTR			&GeCanRx_CanXmitRate_stat
#define CMD_BITS_STAT_PTR				&GeCanRx_CMDBits_stat
#define MOT_CURR_LIMIT_STAT_PTR			&GeCanRx_MotoringCurLimit_stat
#define GEN_CURR_LIMIT_PTR				&GeCanRx_GeneratingCurLimit_stat

#define SLEWRATE_SF_INV		 			0.0005					/* 1/2000 */


/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

IFX_EXTERN esignal_stat GeCanRx_SlewRateUp_stat;
IFX_EXTERN esignal_stat GeCanRx_SlewRateDown_stat;
IFX_EXTERN esignal_stat GeCanRx_CanXmitRate_stat;
IFX_EXTERN esignal_stat GeCanRx_CMDBits_stat;
IFX_EXTERN esignal_stat GeCanRx_MotoringCurLimit_stat;
IFX_EXTERN esignal_stat GeCanRx_GeneratingCurLimit_stat;

IFX_EXTERN float GfCANRx_SlewRateUp;
IFX_EXTERN float GfCANRx_SlewRateDown;
IFX_EXTERN float GfCANRx_CanXmitRate;
IFX_EXTERN State Gu8CanRx_CommandBits;
IFX_EXTERN float GfCanRx_MotoringCurLimit;
IFX_EXTERN float GfCanRx_GeneratingCurLimit;

IFX_EXTERN uint8 Gu8CanRx_SlewRateUp;
IFX_EXTERN uint8 Gu8CanRx_SlewRateDown;

#elif (CUSTOMER_ID == AUDI)

/*****************************MSG_12DD54FEh***************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/
#define MSG_ID_CMD_1					0x12DD54FE

#define CMD1_BIT_START_POS_TERMINAL_KL15	17
#define CMD1_BIT_SIZE_TERMINAL_KL15			1
#define CMD1_BIT_MASK_TERMINAL_KL15			((1<<CMD1_BIT_SIZE_TERMINAL_KL15)-1)


/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
IFX_EXTERN bool GbCanRx_TerminalKL15Status;

#endif /* CUSTOMER_ID */


IFX_EXTERN uint16 Gu16CANRx_CMD1TimeOutCtr;
IFX_EXTERN bool GbCANRx_CMD1TimeOutSWT2;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
IFX_EXTERN void Init_CMD_1(void);
IFX_EXTERN void Receive_CMD_1(void);

#endif /* APP_CAN_CMD_1_H_ */
