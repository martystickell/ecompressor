/*
 * CAN_Defs.h
 *
 */
#ifndef APP_CAN_CAN_DEFS_H_
#define APP_CAN_CAN_DEFS_H_


typedef enum {eCStateStandby=0, eCStateActive=1, eCStateInit=2, eCStateFailure=3} eCState;


#endif /* APP_CAN_CAN_DEFS_H_ */
