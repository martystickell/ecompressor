/*
 * CAN_Interface_Rx.h
 *
 *  Created on: Oct 16, 2018
 *      Author: H297270
 */

#ifndef APP_CAN_CAN_INTERFACE_RX_H_
#define APP_CAN_CAN_INTERFACE_RX_H_

#include <Ifx_Types.h>
#include <Ifx_TypesReg.h>
#include "CAN_Driver.h"
#include "STM.h"
#include "CMD_0.h"
#include "CMD_1.h"
#include "CMD_2.h"

/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/
#define EXTRACT(value, mask, start_pos)		(((uint64)(value >> start_pos)) & mask)

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
IFX_EXTERN emsg_stat GeCanRxLink_MsgStat;


/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
IFX_EXTERN void Process_RxSignal_u16(uint16, signalCfg_t, float *);

#endif
