/*
 * CAN_Interface.h
 *
 *  Created on: Oct 15, 2018
 *      Author: H297270
 */

#ifndef APP_CAN_CAN_INTERFACE_TX_H_
#define APP_CAN_CAN_INTERFACE_TX_H_

#include <Ifx_Types.h>
#include <Ifx_TypesReg.h>
#include <string.h>
#include "CAN_Driver.h"
#include "STM.h"
#include "FDBK_0.h"
#include "FDBK_1.h"
#include "FDBK_2.h"
#include "FDBK_3.h"
#include "FDBK_4.h"
#include "FDBK_5.h"
#include "FDBK_6.h"
#include "FDBK_7.h"
#include "FDBK_8.h"

/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/
#define FILL(value, mask, start_pos)		(((uint64)(value & mask)) << start_pos)

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
IFX_EXTERN emsg_stat GeCanTxLink_MsgStat;


/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
IFX_EXTERN void Process_TxSignal_u8(float, signalCfg_t, uint8 *);
IFX_EXTERN void Process_TxSignal_s8(float, signalCfg_t, sint8 *);
IFX_EXTERN void Process_TxSignal_u16(float, signalCfg_t, uint16 *);

#endif /* APP_CAN_CAN_INTERFACE_TX_H_ */
