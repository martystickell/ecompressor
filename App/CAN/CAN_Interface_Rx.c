/*
 * CAN_Interface_Rx.c
 *
 *  Created on: Oct 16, 2018
 *      Author: H297270
 */
#include "CAN_Interface_Rx.h"


/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
emsg_stat GeCanRxLink_MsgStat = GOOD;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
void Process_RxSignal_u16(uint16 input, signalCfg_t sigConfig, float *result_ptr)
{
	if(result_ptr != NULL)
	{
		*result_ptr = (float)((input * sigConfig.res) + sigConfig.offset);
		if(LIMIT(*result_ptr,sigConfig.min,sigConfig.max))
		{
			*(sigConfig.stat_ptr) = OK;
		}
		else if(*result_ptr > sigConfig.max )
		{
			*result_ptr = sigConfig.max;
			*(sigConfig.stat_ptr) = OOR_HIGH;
		}
		else
		{
			*result_ptr = sigConfig.min;
			*(sigConfig.stat_ptr) = OOR_LOW;
		}
	}
	else
	{
		/* do nothing */
	}
}
