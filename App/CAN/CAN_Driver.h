/**
 * \file MulticanBasicDemo.h
 * \brief Demo MulticanBasicDemo
 *
 * \version iLLD_Demos_1_0_1_3_0
 * \copyright Copyright (c) 2014 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_Demo_MulticanBasic_SrcDoc_Main Demo Source
 * \ingroup IfxLld_Demo_MulticanBasic_SrcDoc
 * \defgroup IfxLld_Demo_MulticanBasic_SrcDoc_Main_Interrupt Interrupts
 * \ingroup IfxLld_Demo_MulticanBasic_SrcDoc_Main
 */

#ifndef APP_CAN_CAN_DRIVER_H_
#define APP_CAN_CAN_DRIVER_H_

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/

#include <Ifx_Types.h>
#include <Multican/Can/IfxMultican_Can.h>
#include "MCS.h"

/******************************************************************************/
/*-----------------------------------Macros-----------------------------------*/
/******************************************************************************/
#define LIMIT(n,MIN,MAX) 	(((n) >= MIN && (n) <= (MAX)) ? 1 : 0)
#define LARGEST(a,b,c) 		(a>b?(a>c?a:c):(b>c?b:c))
//#define CAN_BAUD_RATE 		1000000		// 1MB
#define CAN_BAUD_RATE 		500000		// 1MB

#if TRIBOARD_DEV == ENABLE
	#define CAN_MODULE 	MODULE_CAN
	#define TX_PIN		IfxMultican_TXD0_P20_8_OUT
	#define RX_PIN		IfxMultican_RXD0B_P20_7_IN
#else
	#define CAN_MODULE 	MODULE_CAN1
	#define TX_PIN		IfxMultican1_TXD0_P13_0_OUT
	#define RX_PIN		IfxMultican1_RXD0B_P13_1_IN
#endif

/******************************************************************************/
/*--------------------------------Enumerations--------------------------------*/
/******************************************************************************/
typedef enum
{
	OK,
	OOR_HIGH,
	OOR_LOW,
}esignal_stat;

typedef enum
{
	GOOD,
	BAD
}emsg_stat;
/******************************************************************************/
/*-----------------------------Data Structures--------------------------------*/
/******************************************************************************/
/** \brief Asc information */

#define CAN_ACCEPTANCE_MASK				(0x7FFFFFFFUL)

typedef struct
{
    struct
    {
        IfxMultican_Can        can;          	/**< \brief CAN driver handle */
        IfxMultican_Can_Node   can0Node;   		/**< \brief CAN Source Node */

        IfxMultican_Can_MsgObj can0DstMsgObj0; 	/**< \brief CAN Destination Message object */
        IfxMultican_Can_MsgObj can0DstMsgObj1; 	/**< \brief CAN Destination Message object */
        IfxMultican_Can_MsgObj can0DstMsgObj2; 	/**< \brief CAN Destination Message object */

        IfxMultican_Can_MsgObj can0SrcMsgObj0; 	/**< \brief CAN Source Message object */
        IfxMultican_Can_MsgObj can0SrcMsgObj1; 	/**< \brief CAN Source Message object */
        IfxMultican_Can_MsgObj can0SrcMsgObj2; 	/**< \brief CAN Source Message object */
        IfxMultican_Can_MsgObj can0SrcMsgObj3; 	/**< \brief CAN Source Message object */
        IfxMultican_Can_MsgObj can0SrcMsgObj4; 	/**< \brief CAN Source Message object */
        IfxMultican_Can_MsgObj can0SrcMsgObj5; 	/**< \brief CAN Source Message object */
        IfxMultican_Can_MsgObj can0SrcMsgObj6; 	/**< \brief CAN Source Message object */
        IfxMultican_Can_MsgObj can0SrcMsgObj7; 	/**< \brief CAN Source Message object */
        IfxMultican_Can_MsgObj can0SrcMsgObj8; 	/**< \brief CAN Source Message object */
    }drivers;
} App_MulticanBasic;

typedef struct
{
	float res;
	float min;
	float max;
	float offset;
	esignal_stat  *stat_ptr;
}signalCfg_t;

/******************************************************************************/
/*------------------------------Calibrations----------------------------------*/
/******************************************************************************/

extern boolean KbCAN_CANCmdUpdt;
extern float KfCAN_MaxSpeedCmdCAN;

/******************************************************************************/
/*------------------------------Global variables------------------------------*/
/******************************************************************************/

IFX_EXTERN App_MulticanBasic g_MulticanBasic;

extern float GfCAN_CoolantTemp;
extern float GfCAN_InletTemp;

/******************************************************************************/
/*-------------------------Function Prototypes--------------------------------*/
/******************************************************************************/

IFX_EXTERN void CAN_Driver_MessageObjectInit(IfxMultican_Can_MsgObj *msgObj, uint32 messageId, IfxMultican_Frame frame, boolean extendedFrame);
IFX_EXTERN void CAN_Driver_Init(void);

// CAN command update if flag is set
extern void CAN_CmdUpdate(void);

extern void CAN_TxMsgUpdate(void);


#endif
