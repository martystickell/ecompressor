/*
 * FDBK_1.c
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */

#include "FDBK_1.h"

static uint64 FDBK_1_data;

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

/*****************************MSG_178h********************************************/
/*********************************************************************************/
State GeCanTx_OpertingMode;
uint8 Gu8CanTx_SWVersion;

bool GbCanTx_Fault26;
bool GbCanTx_Fault27;

float GfCanTx_BusVoltage;
signalCfg_t GstCanTx_BusVoltage;
uint8_t Gu8CanTx_BusVoltage;
esignal_stat Gu8CanTx_BusVoltage_stat;

float GfCanTx_InvTemp1;
signalCfg_t GstCanTx_InvTemp1;
uint8_t Gu8CanTx_InvTemp1;
esignal_stat Gu8CanTx_InvTemp1_stat;

float GfCanTx_Derating;
signalCfg_t GstCanTx_Derating;
uint8_t Gu8CanTx_Derating;
esignal_stat Gu8CanTx_Derating_stat;

bool GbCanTx_Fault28;
bool GbCanTx_Fault29;
bool GbCanTx_Fault30;
bool GbCanTx_Fault31;

const signalCfg_t kst_BusVtg 		= {3.330f, 0.000f, 	76.276f, 0.000f, BUS_VLTG_STAT_PTR};
const signalCfg_t kst_InvTemp1		= {0.500f, -254.0f, 254.00f, 254.0f, INV_TEMP1_STAT_PTR};
const signalCfg_t kst_Derating		= {200.0f, 0.0f, 	1.270f,  0.0f, 	 DERATING_STAT_PTR};



/*****************************MSG_17Bh********************************************/
/*********************************************************************************/
void Init_FDBK_1(void)
{
    CAN_Driver_MessageObjectInit(&g_MulticanBasic.drivers.can0SrcMsgObj1, MSG_ID_FDBK_1, IfxMultican_Frame_transmit, TRUE);

    GbCanTx_Fault26 = 0x0;
	GbCanTx_Fault27 = 0x0;
	GbCanTx_Fault28 = 0x0;
	GbCanTx_Fault29 = 0x0;
	GbCanTx_Fault30 = 0x0;
	GbCanTx_Fault31 = 0x0;

	GeCanTx_OpertingMode = Off;
	Gu8CanTx_SWVersion = 1;

	GstCanTx_BusVoltage = kst_BusVtg;
	GstCanTx_InvTemp1 = kst_InvTemp1;
	GstCanTx_Derating = kst_Derating;

	GfCanTx_BusVoltage = 0.0f;
	Gu8CanTx_BusVoltage = 0;
	Gu8CanTx_BusVoltage_stat = OK;

	GfCanTx_InvTemp1 = 0.0f;
	Gu8CanTx_InvTemp1 = 0;
	Gu8CanTx_InvTemp1_stat = OK;

	GfCanTx_Derating = 0.0f;
	Gu8CanTx_Derating = 0;
	Gu8CanTx_Derating_stat = OK;

}


static void Process_FDBK_1(void)
{
	memset(&FDBK_1_data, 0, sizeof(uint64));

	FDBK_1_data |= FILL(GeCanTx_OpertingMode, BIT_MASK_OPERATING_MODE, BIT_START_OPERATING_MODE);
	FDBK_1_data |= FILL(Gu8CanTx_SWVersion, BIT_MASK_SW_VERSION, BIT_START_SW_VERSION);

	GbCanTx_Fault26 = GbSTM_TempSens2OTSW;
	GbCanTx_Fault27 = GbSTM_TempSens3OTSW;
	GbCanTx_Fault28 = GbSTM_V12UVSW;
	GbCanTx_Fault29 = GbSTM_V12OORSW;
	GbCanTx_Fault30 = GbSTM_V05UVSW;
	GbCanTx_Fault31 = GbSTM_V05OORSW;

	FDBK_1_data |= FILL(GbCanTx_Fault26, BIT_MASK_FAULT26, BIT_START_POS_FAULT26);
	FDBK_1_data |= FILL(GbCanTx_Fault27, BIT_MASK_FAULT27, BIT_START_POS_FAULT27);
	FDBK_1_data |= FILL(GbCanTx_Fault28, BIT_MASK_FAULT28, BIT_START_POS_FAULT28);
	FDBK_1_data |= FILL(GbCanTx_Fault29, BIT_MASK_FAULT29, BIT_START_POS_FAULT29);
	FDBK_1_data |= FILL(GbCanTx_Fault30, BIT_MASK_FAULT30, BIT_START_POS_FAULT30);
	FDBK_1_data |= FILL(GbCanTx_Fault31, BIT_MASK_FAULT31, BIT_START_POS_FAULT31);
	FDBK_1_data |= FILL(GeCanTx_OpertingMode, BIT_MASK_OPERATING_MODE02, BIT_START_OPERATING_MODE02);

	Process_TxSignal_u8(GfCanTx_BusVoltage,	GstCanTx_BusVoltage, 	&Gu8CanTx_BusVoltage);
	FDBK_1_data |= FILL(Gu8CanTx_BusVoltage, BIT_MASK_BUS_VOLTAGE, BIT_START_BUS_VOLTAGE);
	GeCanTxLink_MsgStat |= *(GstCanTx_BusVoltage.stat_ptr);

	Process_TxSignal_u8(GfCanTx_InvTemp1,	GstCanTx_InvTemp1, 	&Gu8CanTx_InvTemp1);
	FDBK_1_data |= FILL(Gu8CanTx_InvTemp1, BIT_MASK_INV_TEMP1, BIT_START_INV_TEMP1);
	GeCanTxLink_MsgStat |= *(GstCanTx_InvTemp1.stat_ptr);

	Process_TxSignal_u8(GfCanTx_Derating,	GstCanTx_Derating, 	&Gu8CanTx_Derating);
	FDBK_1_data |= FILL(Gu8CanTx_Derating, BIT_MASK_DERATING, BIT_START_DERATING);
	GeCanTxLink_MsgStat |= *(GstCanTx_Derating.stat_ptr);
}

void Transmit_FDBK_1(void)
{
	uint32 dataLowTx  = 0x00000000;
	uint32 dataHighTx = 0x00000000;
	uint64 datawordTx = 0;
	IfxMultican_Message msg_buf;

	Process_FDBK_1();

	memcpy(&datawordTx, &FDBK_1_data, sizeof(uint64));

	dataLowTx 	= (uint32)datawordTx;
	dataHighTx 	= (uint64)datawordTx >> 32;

	IfxMultican_Message_init(&msg_buf, MSG_ID_FDBK_1, dataLowTx , dataHighTx, IfxMultican_DataLengthCode_8);

	IfxMultican_Can_MsgObj_sendMessage(&g_MulticanBasic.drivers.can0SrcMsgObj1, &msg_buf);
}


#elif (CUSTOMER_ID == AUDI)

/*****************************MSG_AF95444h********************************************/
/*********************************************************************************/
ePwrDnState GeCanTx_ePwrDnState;
eCState 	GeCanTx_eCState;
eTestStatus GeCanTx_EleFailure;
eTestStatus GeCanTx_MechFailure;
eTestStatus GeCanTx_CanToutFailure;
eTestStatus GeCanTx_OverTempFailure;
eTestStatus GeCanTx_OverVoltFailure;
eTestStatus GeCanTx_UnderVoltFailure;

float GfCanTx_Temperature;
signalCfg_t GstCanTx_TemperatureCfg;
esignal_stat GeCanTx_Temperature_stat;

float GfCanTx_TpMosfet;
signalCfg_t GstCanTx_TpMosfetCfg;
esignal_stat GeCanTx_TpMosfet_stat;


const signalCfg_t kst_TpMosfet		= {2.0f, 	0.0f, 		 255.5f, 	 0.0f,  TpMosfet_STAT_PTR};
const signalCfg_t kst_Temperature	= {1.0f, 	-40.0f, 	213.00f, 	40.0f, 	Temperature_STAT_PTR};


void Init_FDBK_1(void)
{
	GstCanTx_TpMosfetCfg = kst_TpMosfet;
	GstCanTx_TemperatureCfg = kst_Temperature;

	GeCanTx_ePwrDnState = Ready;
	GeCanTx_eCState = eCStateInit;
	GeCanTx_EleFailure = TestNotCompleted;
	GeCanTx_MechFailure = TestNotCompleted;
	GeCanTx_CanToutFailure = TestNotCompleted;
	GeCanTx_OverTempFailure = TestNotCompleted;
	GeCanTx_OverVoltFailure = TestNotCompleted;
	GeCanTx_UnderVoltFailure = TestNotCompleted;


	GfCanTx_TpMosfet = 0.0f;
	GeCanTx_TpMosfet_stat = OK;

	GfCanTx_Temperature = 0.0f;
	GeCanTx_Temperature_stat = OK;
}

static void Process_FDBK_1(void)
{
	static uint8 Gu8CanTx_data1;

	memset(&FDBK_1_data, 0, sizeof(uint64));

	// Process the TpMosfet field
	Process_TxSignal_u8(GfCanTx_TpMosfet,	GstCanTx_TpMosfetCfg, 	&Gu8CanTx_data1);
	FDBK_1_data |= FILL(Gu8CanTx_data1, BIT_MASK_TpMosfet, BIT_START_POS_TpMosfet);
	GeCanTxLink_MsgStat |= *(GstCanTx_TpMosfetCfg.stat_ptr);

	// Process the Power Down field
	Gu8CanTx_data1 = (uint8)GeCanTx_ePwrDnState;
	FDBK_1_data |= FILL(Gu8CanTx_data1, BIT_MASK_ePwrDownState, BIT_START_POS_ePwrDownState);

	// Process the eC State field
	Gu8CanTx_data1 = (uint8)GeCanTx_eCState;
	FDBK_1_data |= FILL(Gu8CanTx_data1, BIT_MASK_eCState, BIT_START_POS_eCState);

	// Process the eC Electrical Failure field
	Gu8CanTx_data1 = (uint8)GeCanTx_EleFailure;
	FDBK_1_data |= FILL(Gu8CanTx_data1, BIT_MASK_EleFailure, BIT_START_POS_EleFailure);

	// Process the eC Mechanical Failure field
	Gu8CanTx_data1 = (uint8)GeCanTx_MechFailure;
	FDBK_1_data |= FILL(Gu8CanTx_data1, BIT_MASK_MechFailure, BIT_START_POS_MechFailure);

	// Process the eC CAN Timeout Failure field
	Gu8CanTx_data1 = (uint8)GeCanTx_CanToutFailure;
	FDBK_1_data |= FILL(Gu8CanTx_data1, BIT_MASK_CAN_ToutFailure, BIT_START_POS_CAN_ToutFailure);

	// Process the eC Over Temperature Failure field
	Gu8CanTx_data1 = (uint8)GeCanTx_OverTempFailure;
	FDBK_1_data |= FILL(Gu8CanTx_data1, BIT_MASK_OverTempFailure, BIT_START_POS_OverTempFailure);

	// Process the eC Over Voltage Failure field
	Gu8CanTx_data1 = (uint8)GeCanTx_OverVoltFailure;
	FDBK_1_data |= FILL(Gu8CanTx_data1, BIT_MASK_OverVoltFailure, BIT_START_POS_OverVoltFailure);

	// Process the eC Under Voltage Failure field
	Gu8CanTx_data1 = (uint8)GeCanTx_UnderVoltFailure;
	FDBK_1_data |= FILL(Gu8CanTx_data1, BIT_MASK_UnderVoltFailure, BIT_START_POS_UnderVoltFailure);

	// Process the IMS Temperature(C) field
	Process_TxSignal_u8(GfCanTx_Temperature, GstCanTx_TemperatureCfg, &Gu8CanTx_data1);
	FDBK_1_data |= FILL(Gu8CanTx_data1, BIT_MASK_Temperature, BIT_START_POS_Temperature);
	GeCanTxLink_MsgStat |= *(GstCanTx_TemperatureCfg.stat_ptr);
}

void Transmit_FDBK_1(void)
{
	uint32 dataLowTx  = 0x00000000;
	uint32 dataHighTx = 0x00000000;
	uint64 datawordTx = 0;
	IfxMultican_Message msg_buf;

	Process_FDBK_1();

	memcpy(&datawordTx, &FDBK_1_data, sizeof(uint64));

	dataLowTx 	= (uint32)(datawordTx);
	dataHighTx 	= (uint32)(datawordTx >> 32);

	IfxMultican_Message_init(&msg_buf, MSG_ID_FDBK_1, dataLowTx , dataHighTx, IfxMultican_DataLengthCode_8);

	IfxMultican_Can_MsgObj_sendMessage(&g_MulticanBasic.drivers.can0SrcMsgObj1, &msg_buf);
}

#else

void Init_FDBK_1(void)
{
	/* Do nothing */
}

void Transmit_FDBK_1(void)
{
	/* Do nothing */
}

#endif /* CUSTOMER_ID */
