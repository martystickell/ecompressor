/*
 * FDBK_4.h
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */

#ifndef APP_CAN_FDBK_4_H_
#define APP_CAN_FDBK_4_H_

#include <Ifx_Types.h>
#include <Ifx_TypesReg.h>
#include <string.h>
#include "CAN_Driver.h"
#include "CAN_Interface_Tx.h"
#include "STM.h"
#include "MES.h"

#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)
/*****************************MSG_18E14281h***************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Macros
 * ------------------------------------------------------------------------------*/

#define MSG_ID_FDBK_4				0x18E14281

#define BIT_START_POS_TEMP_SENS1	0
#define BIT_SIZE_TEMP_SENS1			8
#define BIT_MASK_TEMP_SENS1			(1<<BIT_SIZE_TEMP_SENS1)-1

#define TEMP_SENS1_STAT_PTR 		&GeCanTx_TempSens1_stat

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

IFX_EXTERN esignal_stat GeCanTx_TempSens1_stat;
IFX_EXTERN float GfCanTx_TempSens1;

#endif /* CUSTOMER_ID */

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
IFX_EXTERN void Init_FDBK_4(void);
IFX_EXTERN void Transmit_FDBK_4(void);


#endif /* APP_CAN_FDBK_4_H_ */
