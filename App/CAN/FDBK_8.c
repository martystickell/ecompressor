/*
 * FDBK_8.c
 *
 *  Created on: May 12, 2021
 */

/*****************************MSG_18E15a78h***************************************/
/*********************************************************************************/
#include "BUILD.h"
#include "FDBK_8.h"
#include "GTM.h"

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
static uint8 u8CanTx_count;

static uint8 u8CanTx_PlatformId;
static uint8 u8CanTx_CustomerId;
static uint8 u8CanTx_VersionMajor;
static uint8 u8CanTx_VersionMinor;
static uint8 u8CanTx_VersionRev;
static uint8 u8CanTx_VersionCustomer;


static uint64 FDBK_8_data = 0;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
void Init_FDBK_8(void)
{
    CAN_Driver_MessageObjectInit(&g_MulticanBasic.drivers.can0SrcMsgObj8, MSG_ID_FDBK_8, IfxMultican_Frame_transmit, TRUE);

    u8CanTx_PlatformId = PLATFORM_ID;
	u8CanTx_CustomerId = CUSTOMER_ID;
	u8CanTx_VersionMajor = BUILD_SW_VER_MAJOR;
	u8CanTx_VersionMinor = BUILD_SW_VER_MINOR;
	u8CanTx_VersionRev = BUILD_SW_VER_REV;
	u8CanTx_VersionCustomer = BUILD_SW_VER_CUSTOMER;

	u8CanTx_count = FDBK_8_SEND_COUNT_MAX;
}

static void Process_FDBK_8(void)
{
	memset(&FDBK_8_data, 0, sizeof(uint64));

	u8CanTx_PlatformId = PLATFORM_ID;
	FDBK_8_data |= FILL(u8CanTx_PlatformId, 		BIT_MASK_PLATFORM_ID, 			BIT_START_POS_PLATFORM_ID);

	u8CanTx_CustomerId = CUSTOMER_ID;
	FDBK_8_data |= FILL(u8CanTx_CustomerId, 		BIT_MASK_CUSTOMER_ID, 			BIT_START_POS_CUSTOMER_ID);

	u8CanTx_VersionMajor = BUILD_SW_VER_MAJOR;
	FDBK_8_data |= FILL(u8CanTx_VersionMajor, 		BIT_MASK_VERSION_MAJOR, 		BIT_START_POS_VERSION_MAJOR);

	u8CanTx_VersionMinor = BUILD_SW_VER_MINOR;
	FDBK_8_data |= FILL(u8CanTx_VersionMinor, 		BIT_MASK_VERSION_MINOR, 		BIT_START_POS_VERSION_MINOR);

	u8CanTx_VersionRev = BUILD_SW_VER_REV;
	FDBK_8_data |= FILL(u8CanTx_VersionRev, 		BIT_MASK_VERSION_REV, 			BIT_START_POS_VERSION_REV);

	u8CanTx_VersionCustomer = BUILD_SW_VER_CUSTOMER;
	FDBK_8_data |= FILL(u8CanTx_VersionCustomer, 	BIT_MASK_VERSION_CUSTOMER, 		BIT_START_POS_VERSION_CUSTOMER);

	GeCanTxLink_MsgStat = OK;
}

void Transmit_FDBK_8(void)
{
	uint32 dataLowTx  = 0x00000000;
	uint32 dataHighTx = 0x00000000;
	uint64 datawordTx = 0;
	IfxMultican_Message msg_buf;

	/* Only send at powerup */
	if (u8CanTx_count > 0)
	{
		u8CanTx_count--;
		Process_FDBK_8();

		memcpy(&datawordTx, &FDBK_8_data, sizeof(uint64));

		dataLowTx 	= (uint32)datawordTx;
		dataHighTx 	= (uint64)datawordTx >> 32;

		IfxMultican_Message_init(&msg_buf, MSG_ID_FDBK_8, dataLowTx , dataHighTx, IfxMultican_DataLengthCode_8);

		IfxMultican_Can_MsgObj_sendMessage(&g_MulticanBasic.drivers.can0SrcMsgObj8, &msg_buf);
	}
}
