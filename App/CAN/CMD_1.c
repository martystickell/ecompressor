/*
 * CMD_1.c
 *
 *  Created on: Nov 2, 2018
 *      Author: H297270
 */
#include "CMD_1.h"

static void Process_CMD_1(void);

volatile uint64 datawordCMD1 = 0;
static IfxMultican_Status GeCanRx_CMD_1_ReadStatus;
uint16 Gu16CANRx_CMD1TimeOutCtr = 0;
bool GbCANRx_CMD1TimeOutSWT2 = FALSE;


#if (CUSTOMER_ID == JOHN_DEERE) || (CUSTOMER_ID == FORD)

/*****************************MSG_18E05841h***************************************/
/*********************************************************************************/

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/

signalCfg_t GstCanRx_SlewRateUpCfg;
signalCfg_t GstCanRx_SlewRateDownCfg;
signalCfg_t GstCanRx_CanXmitRateCfg;
signalCfg_t GstCanRx_MotoringCurLimitCfg;
signalCfg_t GstCanRx_GeneratingCurLimitCfg;

float kCANRx_SlewRateUp_Default		= 508000.0f;
float kCANRx_SlewRateDown_Default	= 508000.0f;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

const signalCfg_t kst_SlewRateUp 	= {2000.0f, 0.0f, 508000.0f, 	0.0f, SLEW_RATE_UP_STAT_PTR};
const signalCfg_t kst_SlewRateDown	= {2000.0f, 0.0f, 508000.0f, 	0.0f, SLEW_RATE_DOWN_STAT_PTR};
const signalCfg_t kst_CANXMitRate	= {10.0f, 	0.0f, 2540.0f, 		0.0f, CAN_XMIT_RATE_STAT_PTR};
const signalCfg_t kst_MotCurrLimit 	= {2.0f, 	0.0f, 508.0f, 		0.0f, MOT_CURR_LIMIT_STAT_PTR};
const signalCfg_t kst_GenCurrLimit	= {2.0f, 	0.0f, 508.0f, 		0.0f, GEN_CURR_LIMIT_PTR};

uint8 Gu8CanRx_SlewRateUp;
float GfCANRx_SlewRateUp;
esignal_stat GeCanRx_SlewRateUp_stat;

uint8 Gu8CanRx_SlewRateDown;
float GfCANRx_SlewRateDown;
esignal_stat GeCanRx_SlewRateDown_stat;

uint8 Gu8CanRx_CanXmitRate;
float GfCANRx_CanXmitRate;
esignal_stat GeCanRx_CanXmitRate_stat;

uint8 Gu8CanRx_MotoringCurLimit;
float GfCanRx_MotoringCurLimit;
esignal_stat GeCanRx_MotoringCurLimit_stat;

uint8 Gu8CanRx_GeneratingCurLimit;
float GfCanRx_GeneratingCurLimit;
esignal_stat GeCanRx_GeneratingCurLimit_stat;


/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

void Init_CMD_1(void)
{
    CAN_Driver_MessageObjectInit(&g_MulticanBasic.drivers.can0DstMsgObj1, MSG_ID_CMD_1, IfxMultican_Frame_receive, TRUE);

    Gu8CanRx_SlewRateUp = (uint8)((kCANRx_SlewRateUp_Default + kst_SlewRateUp.offset) * SLEWRATE_SF_INV);
	GfCANRx_SlewRateUp = kCANRx_SlewRateUp_Default;
	GstCanRx_SlewRateUpCfg = kst_SlewRateUp;
	GeCanRx_SlewRateUp_stat = OK;

	Gu8CanRx_SlewRateDown = (uint8)((kCANRx_SlewRateDown_Default + kst_SlewRateDown.offset) * SLEWRATE_SF_INV);
	GfCANRx_SlewRateDown = kCANRx_SlewRateDown_Default;
	GstCanRx_SlewRateDownCfg = kst_SlewRateDown;
	GeCanRx_SlewRateDown_stat = OK;

	Gu8CanRx_CanXmitRate = 0;
	GfCANRx_CanXmitRate = 0.0f;
	GstCanRx_CanXmitRateCfg = kst_CANXMitRate;
	GeCanRx_CanXmitRate_stat = OK;

	Gu8CanRx_MotoringCurLimit = 0;
	GfCanRx_MotoringCurLimit = 0.0f;
	GstCanRx_MotoringCurLimitCfg = kst_MotCurrLimit;
	GeCanRx_MotoringCurLimit_stat = OK;

	Gu8CanRx_GeneratingCurLimit = 0;
	GfCanRx_GeneratingCurLimit = 0.0f;
	GstCanRx_GeneratingCurLimitCfg = kst_GenCurrLimit;
	GeCanRx_GeneratingCurLimit_stat = OK;

	GbCANRx_CMD1TimeOutSWT2 = FALSE;
	Gu16CANRx_CMD1TimeOutCtr = 0;
}


void Receive_CMD_1(void)
{
	uint32 dataLowRx  = 0x00000000;
	uint32 dataHighRx = 0x00000000;

	IfxMultican_Message msg_buf;

	datawordCMD1 = 0;

	IfxMultican_Message_init(&msg_buf, 0xdead, 0xdeadbeef, 0xdeadbeef, IfxMultican_DataLengthCode_8); /* start with invalid values */
	GeCanRx_CMD_1_ReadStatus = IfxMultican_Can_MsgObj_readMessage(&g_MulticanBasic.drivers.can0DstMsgObj1, &msg_buf);

	/* all the following status messages mean a new message is received */
	if
	(
		GeCanRx_CMD_1_ReadStatus == IfxMultican_Status_newDataButOneLost || \
		GeCanRx_CMD_1_ReadStatus == IfxMultican_Status_newData|| \
		GeCanRx_CMD_1_ReadStatus == IfxMultican_Status_ok
	)
	{
		dataLowRx 		= (uint32)msg_buf.data[0];
		dataHighRx 		= (uint32)msg_buf.data[1];
		datawordCMD1 	= ((uint64)dataHighRx<<32 | (uint64)dataLowRx);

		GbCANRx_CMD1TimeOutSWT2 = FALSE;
		Gu16CANRx_CMD1TimeOutCtr = 0;

		Process_CMD_1();
	}
	else
	{
		/* do nothing */
	}
}

static void Process_CMD_1(void)
{
	Gu8CanRx_SlewRateUp 	= (uint8)(EXTRACT(datawordCMD1, BIT_MASK_SLEW_RATE_UP, BIT_START_POS_SLEW_RATE_UP));
	Process_RxSignal_u16((uint16)Gu8CanRx_SlewRateUp, GstCanRx_SlewRateUpCfg, &GfCANRx_SlewRateUp);
	GeCanRxLink_MsgStat |= *(GstCanRx_SlewRateUpCfg.stat_ptr);

	Gu8CanRx_SlewRateDown 	= (uint8)(EXTRACT(datawordCMD1, BIT_MASK_SLEW_RATE_DOWN, BIT_START_POS_SLEW_RATE_DOWN));
	Process_RxSignal_u16((uint16)Gu8CanRx_SlewRateDown, GstCanRx_SlewRateDownCfg, &GfCANRx_SlewRateDown);
	GeCanRxLink_MsgStat |= *(GstCanRx_SlewRateDownCfg.stat_ptr);

	Gu8CanRx_CanXmitRate 	= (uint8)(EXTRACT(datawordCMD1, BIT_MASK_CAN_XMIT_RATE, BIT_START_POS_CAN_XMIT_RATE));
	Process_RxSignal_u16((uint16)Gu8CanRx_CanXmitRate, GstCanRx_CanXmitRateCfg, &GfCANRx_CanXmitRate);
	GeCanRxLink_MsgStat |= *(GstCanRx_CanXmitRateCfg.stat_ptr);

	Gu8CanRx_MotoringCurLimit 	= (uint8)(EXTRACT(datawordCMD1, BIT_MASK_MOT_CURR_LIMIT, BIT_START_POS_MOT_CURR_LIMIT));
	Process_RxSignal_u16((uint16)Gu8CanRx_MotoringCurLimit, GstCanRx_MotoringCurLimitCfg, &GfCanRx_MotoringCurLimit);
	GeCanRxLink_MsgStat |= *(GstCanRx_MotoringCurLimitCfg.stat_ptr);

	Gu8CanRx_GeneratingCurLimit = (uint8)(EXTRACT(datawordCMD1, BIT_MASK_GEN_CURR_LIMIT, BIT_START_POS_GEN_CURR_LIMIT));
	Process_RxSignal_u16((uint16)Gu8CanRx_GeneratingCurLimit, GstCanRx_GeneratingCurLimitCfg, &GfCanRx_GeneratingCurLimit);
	GeCanRxLink_MsgStat |= *(GstCanRx_GeneratingCurLimitCfg.stat_ptr);
}

#elif (CUSTOMER_ID == AUDI)

/*******************ClampControl_01_XIX_Motor_SUBCAN- 0x12DD54FE******************/
/*********************************************************************************/
uint16 ku16CANRx_CMD1TimeOutMax = 100;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

bool GbCanRx_TerminalKL15Status = FALSE;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

void Init_CMD_1(void)
{
    CAN_Driver_MessageObjectInit(&g_MulticanBasic.drivers.can0DstMsgObj1, MSG_ID_CMD_0, IfxMultican_Frame_receive, TRUE);

	GbCanRx_TerminalKL15Status = TRUE;

	GbCANRx_CMD1TimeOutSWT2 = FALSE;
	Gu16CANRx_CMD1TimeOutCtr = 0;
}

void Receive_CMD_1(void)
{
	uint32 dataLowRx  = 0x00000000;
	uint32 dataHighRx = 0x00000000;

	IfxMultican_Message msg_buf;
	datawordCMD1 = 0;

	IfxMultican_Message_init(&msg_buf, 0xdead, 0xdeadbeef, 0xdeadbeef, IfxMultican_DataLengthCode_4); /* start with invalid values */
	GeCanRx_CMD_1_ReadStatus = IfxMultican_Can_MsgObj_readMessage(&g_MulticanBasic.drivers.can0DstMsgObj1, &msg_buf);

	/* all the following status messages mean a new message is received */
	if
	(
		GeCanRx_CMD_1_ReadStatus == IfxMultican_Status_newDataButOneLost || \
		GeCanRx_CMD_1_ReadStatus == IfxMultican_Status_newData|| \
		GeCanRx_CMD_1_ReadStatus == IfxMultican_Status_ok
	)
	{
		dataLowRx 		= (uint32)msg_buf.data[0];
		dataHighRx 		= (uint32)msg_buf.data[1];
		datawordCMD1 	= ((uint64)dataHighRx<<32 | (uint64)dataLowRx);

		GbCANRx_CMD1TimeOutSWT2 = FALSE;
		Gu16CANRx_CMD1TimeOutCtr = 0;

		Process_CMD_1();
	}
#if 0
	else
	{
		if(Gu16CANRx_CMD1TimeOutCtr > ku16CANRx_CMD1TimeOutMax)
		{
			GbCANRx_CMD1TimeOutSWT2 = TRUE;
		}
		else
		{
			Gu16CANRx_CMD1TimeOutCtr++;
		}

	}
#endif
}

static void Process_CMD_1(void)
{
	GbCanRx_TerminalKL15Status = (bool)(EXTRACT(datawordCMD1, CMD1_BIT_MASK_TERMINAL_KL15, CMD1_BIT_START_POS_TERMINAL_KL15));
	GeCanRxLink_MsgStat |= OK;
}

#else

void Init_CMD_1(void)
{
	GbCANRx_CMD1TimeOutSWT2 = FALSE;
	Gu16CANRx_CMD1TimeOutCtr = 0;
}

void Receive_CMD_1(void)
{
	/* Do Nothing */
}

#endif /* CUSTOMER_ID */
