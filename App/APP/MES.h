/*
 * MES.h
 *
 *  Created on: Oct 19, 2018
 *      Author: H247670
 */

#ifndef APP_APP_MES_H_
#define APP_APP_MES_H_

#define		HZ_TO_RPM		60.0
#define		HZ_TO_RADS		6.283185307
#define		RADS_TO_RPM		9.549296586
#define		PI				3.141592654
#define		VDC_MIN			1.0
#define		INV_VDC_MIN		1.0

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/
extern float KfMES_ScaleIabc, KfMES_OffsetIabc;
extern float KfMES_ScaleVdc, KfMES_OffsetVdc;

extern float KfMES_ScaleVabc, KfMES_OffsetVabc;

extern float KfMES_ScaleV12, KfMES_OffsetV12;
extern float KfMES_ScaleV05, KfMES_OffsetV05;

extern float KfMES_ScaleVTP06, KfMES_OffsetVTP06;
extern float KfMES_ScaleVTP07, KfMES_OffsetVTP07;
extern float KfMES_ScaleVExt, KfMES_OffsetVExt;

extern float KfMES_ScaleTempIMS, KfMES_OffsetTempIMS;
extern float KfMES_ScaleTempBdr, KfMES_OffsetTempBdr;

extern float KfMES_ScaleTempSens, KfMES_OffsetTempSens;

extern float KfMES_k2TempIms;
extern float KfMES_k1TempIms;
extern float KfMES_k0TempIms;

extern float KfMES_k3TempSens;
extern float KfMES_k2TempSens;
extern float KfMES_k1TempSens;
extern float KfMES_k0TempSens;

extern float KfMES_TempThermThrsh;

extern float KfMES_k4TempTherm01;
extern float KfMES_k3TempTherm01;
extern float KfMES_k2TempTherm01;
extern float KfMES_k1TempTherm01;
extern float KfMES_k0TempTherm01;

extern float KfMES_k4TempTherm02;
extern float KfMES_k3TempTherm02;
extern float KfMES_k2TempTherm02;
extern float KfMES_k1TempTherm02;
extern float KfMES_k0TempTherm02;

// For current offset calculation
extern uint16 Ku16MES_CurrentOffsetCNTSet;

// Vdc filter
extern float KfMES_WcVdqMagFilt;

// Angle compensation for current/voltage reading
extern float KfMES_IdqThetarComp;
extern float KfMES_VdqThetarComp;


/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/

typedef enum
{
	NormalRotation,  // Phase sequence ABC
	ReverseRotation  // Phase sequence CBA
} MotorRotDir;

extern volatile MotorRotDir GeMES_MotorRotDir;

extern float GfMES_Ia, GfMES_Ic0In, GfMES_Ic1In, GfMES_Ib, GfMES_Ib0In, GfMES_Ib1In, GfMES_Ic;
extern float GfMES_Vdc;
extern float GfMES_InvVdc;

// Sum of 3-phase current
extern float GfMES_IabcSum;

// For current offset calculation
extern float GfMES_G0RES00Sum;
extern float GfMES_G0RES01Sum;
extern float GfMES_G0RES02Sum;

extern float GfMES_G1RES00Sum;
extern float GfMES_G1RES02Sum;

extern uint16 Gu16MES_CurrentOffsetCNT;
extern boolean GbMES_CurrentoffsetCalcDone;

// Calculated offset values
extern float GfMES_Ic0InOffset;
extern float GfMES_Ic1InOffset;
extern float GfMES_Ia0ffset;
extern float GfMES_Ib0InOffset;
extern float GfMES_Ib1InOffset;

extern float GfMES_Vas, GfMES_Vbs, GfMES_Vcs;

// Measured d- and q-axis voltages
extern float GfMES_Vdr;
extern float GfMES_Vqr;

extern float GfMES_MagVdq;
extern float GfMES_MagVdqOld;

extern float GfMES_MagVdqFilt;

extern float GfMES_TempIMS, GfMES_TempSens1, GfMES_TempSens2, GfMES_TempSens3, GfMES_TempBdr1, GfMES_TempBdr2;
extern float GfMES_V12, GfMES_V05, GfMES_VExt;
extern float GfMES_VTP06, GfMES_VTP07;

// Motor temperature
extern float GfMES_TempMot;

// Idr, Iqr
extern float GfMES_Idr;
extern float GfMES_Iqr;

extern float GfMES_Ids;
extern float GfMES_Iqs;

extern float GfMES_MagIdqr;

extern float GfMES_IdrOld;
extern float GfMES_IqrOld;

extern boolean GbMES_MotorRotDirDone;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

extern void MES_CalcIabcIdq(void);
extern void MES_CalcIabcIdq02(void);
extern void MES_CalcVdc(void);
extern void MES_CalcVabc(void);
extern void MES_CalcTemps(void);

extern float MES_CalcTempIMS(float LfMES_TempVolt);
extern float MES_CalcTempSens(float LfMES_TempVolt);

extern float MES_CalcTempRTD(float LfMES_TempVolt);
extern float MES_CalcTempTherm(float LfMES_TempVolt);

extern void MES_CalcIabcOffset(void);
extern void MES_ResetIabcOffset(void);

extern void MES_CaptureRotationDirection(void);

#endif /* APP_APP_MES_H_ */
