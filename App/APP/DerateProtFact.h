/*
 * DCC.h
 *
 *  Created on: Sep 21, 2020
 *      Author: 122142
 */

#ifndef APP_APP_DERATEPROTFACT_H_
#define APP_APP_DERATEPROTFACT_H_

#include <Ifx_Types.h>

extern float GfDPF_High12vDerate;
extern float GfDPF_Low12vDerate;
extern float GfDPF_High48vDerate;
extern float GfDPF_Low48vDerate;

extern float GfDPF_HighTempIMS_Derate;
extern boolean GbDPF_HighTempIMS_DerateAct;
extern uint8 Gu8DPF_HighTempDerateCntrSet;
extern uint8 Gu8DPF_HighTempDerateCntrClr;

extern float GfDPF_LowTempIMS_Derate;
extern boolean GbDPF_LowTempIMS_DerateAct;
extern uint8 Gu8DPF_LowTempIMS_DerateCntrSet;
extern uint8 Gu8DPF_LowTempIMS_DerateCntrClr;

extern float GfDPF_LowTempBrdDerate;
extern boolean GbDPF_LowTempBrdDerateAct;
extern uint8 Gu8DPF_LowTempBrdDerateCntrSet;
extern uint8 Gu8DPF_LowTempBrdDerateCntrClr;

extern float GfDPF_ExtCoolHighTempDerate;
extern boolean GbDPF_ExtCoolHighTempDerateAct;
extern uint8 Gu8DPF_ExtCoolHighTempDerateCntrSet;
extern uint8 Gu8DPF_ExtCoolHighTempDerateCntrClr;

extern float GfDPF_ExtCoolLowTempDerate;
extern boolean GbDPF_ExtCoolLowTempDerateAct;
extern uint8 Gu8DPF_ExtCoolLowTempDerateCntrSet;
extern uint8 Gu8DPF_ExtCoolLowTempDerateCntrClr;

extern float GfDPF_InletTempDerate;
extern boolean GbDPF_InletTempDerateAct;
extern uint8 Gu8DPF_InletTempDerateCntrSet;
extern uint8 Gu8DPF_InletTempDerateCntrClr;

extern float GfDPF_High12vDerate;
extern boolean GbDPF_High12vDerateAct;
extern uint8 Gu8DPF_High12vDerateCntrSet;
extern uint8 Gu8DPF_High12vDerateCntrClr;

extern float GfDPF_Low12vDerate;
extern boolean GbDPF_Low12vDerateAct;
extern uint8 Gu8DPF_Low12vDerateCntrSet;
extern uint8 Gu8DPF_Low12vDerateCntrClr;

extern float GfDPF_High48vDerate;
extern boolean GbDPF_High48vDerateAct;
extern uint8 Gu8DPF_High48vDerateCntrSet;
extern uint8 Gu8DPF_High48vDerateCntrClr;

extern float GfDPF_Low48vDerate;
extern boolean GbDPF_Low48vDerateAct;
extern uint8 Gu8DPF_Low48vDerateCntrSet;
extern uint8 Gu8DPF_Low48vDerateCntrClr;

extern uint8 Gu8DPF_ExtCoolTempDerateCntrSet;
extern uint8 Gu8DPF_ExtCoolTempDerateCntrClr;
extern boolean GbDPF_ExtCoolTempDerateAct;
extern float GfDPF_ExtCoolTempDerate;

extern void DPF_TempIMS_ThermalDerating(void);

extern void DPF_TempBrdThermalDerating(void);

extern void DPF_ExtCoolantTempDerating(void);

extern void DPF_InletTempDerating(void);

extern void DPF_VoltageDerating(void);

#endif /* APP_APP_DERATEPROTFACT_H_ */
