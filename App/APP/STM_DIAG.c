/*
 * STM_DIAG.c
 *
 *  Created on: Oct 16, 2018
 *      Author: H247670
 */

#include	"STM.h"
#include	"GTM.h"
#include	"MES.h"
#include	"CCC.h"
#include	"math.h"
#include 	"CMD_0.h"
#include 	"CMD_1.h"

void STM_DiagT0(void)
{
	float LfSTM_AbsCalc;
	//float LfSTM_TmpSum;

	// HW failure check
	GbSTM_IabcOCHWT0 = GIO_GetOCHWFlt();
	GbSTM_VdcOVHWT0 = GIO_GetOVHWFlt();

	GbSTM_HWFltT0T0 = GbSTM_IabcOCHWT0 | GbSTM_VdcOVHWT0;

	// SW OC check

	// Y Counter for IaOC
	Gu16STM_IsOCYCNT = Gu16STM_IsOCYCNT + 1;

	if (Gu16STM_IsOCYCNT >= Ku16STM_IsOCYCNTSet)
	{
		// Reset X counters
		Gu16STM_IaOCXCNT = 0;
		Gu16STM_IbOCXCNT = 0;
		Gu16STM_IcOCXCNT = 0;

		// Reset Y counter
		Gu16STM_IsOCYCNT = 0;
	}
	else
	{

	}
	// Ia OC check
	LfSTM_AbsCalc = fabsf(GfMES_Ia);
	if (LfSTM_AbsCalc > KfSTM_IsOCSet)
	{
		Gu16STM_IaOCXCNT = Gu16STM_IaOCXCNT + 1;
		// X counter for IaOC
		if (Gu16STM_IaOCXCNT >= Ku16STM_IsOCXCNTSet)
		{
			GbSTM_IaOCSWT0 = TRUE;
		}
		else
		{
			GbSTM_IaOCSWT0 = FALSE;
		}
	}
	else
	{
		GbSTM_IaOCSWT0 = FALSE;
	}

	// Ib OC check
	LfSTM_AbsCalc = fabsf(GfMES_Ib);
	if (LfSTM_AbsCalc > KfSTM_IsOCSet)
	{
		Gu16STM_IbOCXCNT = Gu16STM_IbOCXCNT + 1;
		// X counter for IbOC
		if (Gu16STM_IbOCXCNT >= Ku16STM_IsOCXCNTSet)
		{
			GbSTM_IbOCSWT0 = TRUE;
		}
		else
		{
			GbSTM_IbOCSWT0 = FALSE;
		}
	}
	else
	{
		GbSTM_IbOCSWT0 = FALSE;
	}

	// IC OC check
	LfSTM_AbsCalc = fabsf(GfMES_Ic);
	if (LfSTM_AbsCalc > KfSTM_IsOCSet)
	{
		Gu16STM_IcOCXCNT = Gu16STM_IcOCXCNT + 1;
		// X counter for IcOC
		if (Gu16STM_IcOCXCNT >= Ku16STM_IsOCXCNTSet)
		{
			GbSTM_IcOCSWT0 = TRUE;
		}
		else
		{
			GbSTM_IcOCSWT0 = FALSE;
		}
	}
	else
	{
		GbSTM_IcOCSWT0 = FALSE;
	}

	// SW Is OOR Check
	LfSTM_AbsCalc = fabsf(GfMES_Ia);
	if (LfSTM_AbsCalc > KfSTM_IsOORSet)
	{
		GbSTM_IaOORSWT0 = TRUE;
	}
	else
	{
		GbSTM_IaOORSWT0 = FALSE;
	}

	LfSTM_AbsCalc = fabsf(GfMES_Ib);
	if (LfSTM_AbsCalc > KfSTM_IsOORSet)
	{
		GbSTM_IbOORSWT0 = TRUE;
	}
	else
	{
		GbSTM_IbOORSWT0 = FALSE;
	}

	LfSTM_AbsCalc = fabsf(GfMES_Ic);
	if (LfSTM_AbsCalc > KfSTM_IsOORSet)
	{
		GbSTM_IcOORSWT0 = TRUE;
	}
	else
	{
		GbSTM_IcOORSWT0 = FALSE;
	}

	// Phase current sum check
	//GfMES_IabcSum = GfMES_Ia + GfMES_Ib + GfMES_Ic;
	if (GfMES_IabcSum > KfSTM_IsSUMFltSet)
	{
		GbSTM_IsSUMFltSWT0 = TRUE;
	}
	else
	{
		GbSTM_IsSUMFltSWT0 = FALSE;
	}

	// Over-speed failure check
	if (GfCCC_Wrpm > KfSTM_WrpmOSSet)
	{
		GbSTM_WrpmOSSWT0 = TRUE;
	}
	else
	{
		GbSTM_WrpmOSSWT0 = FALSE;
	}

	// Motor speed OOR check
	if (GfCCC_Wrpm > KfSTM_WrpmOORSet)
	{
		GbSTM_WrpmOORSWT0 = TRUE;
	}
	else
	{
		GbSTM_WrpmOORSWT0 = FALSE;
	}

	// SW 5V OV Check
	if (GfMES_V05 > KfSTM_V05OVSet)
	{
		GbSTM_V05OVSWT0 = TRUE;
	}
	else
	{
		GbSTM_V05OVSWT0 = FALSE;
	}

	// SW 5V UV Check
	if (GfMES_V05 < KfSTM_V05UVSet)
	{
		GbSTM_V05UVSWT0 = TRUE;
	}
	else
	{
		GbSTM_V05UVSWT0 = FALSE;
	}

	// SW 5V OOR check
	if (GfMES_V05 > KfSTM_V05OORSet)
	{
		GbSTM_V05OORSWT0 = TRUE;
	}
	else
	{
		GbSTM_V05OORSWT0 = FALSE;
	}

	GbSTM_SWFltT0T0 = (GbSTM_IaOCSWT0 |
			GbSTM_IbOCSWT0 |
			GbSTM_IcOCSWT0 |
			GbSTM_IaOORSWT0 |
			GbSTM_IbOORSWT0 |
			GbSTM_IcOORSWT0 |
			GbSTM_IsSUMFltSWT0 |
			GbSTM_WrpmOSSWT0 |
			GbSTM_WrpmOORSWT0 |
			GbSTM_V05OVSWT0 |
			GbSTM_V05UVSWT0 |
			GbSTM_V05OORSWT0) & KbSTM_SWFltEnbl;

	GbSTM_FltT0T0 = (GbSTM_HWFltT0T0 & KbSTM_HWFltEnbl) | (GbSTM_SWFltT0T0 & KbSTM_SWFltEnbl);
}

// Latching T0 fault
void STM_LatchingT0Flt(void)
{
	// HW failure bits
	GbSTM_IabcOCHW = GbSTM_IabcOCHW | GbSTM_IabcOCHWT0;
	GbSTM_VdcOVHW = GbSTM_VdcOVHW | GbSTM_VdcOVHWT0;

	// T0 HW failure indicator
	GbSTM_HWFltT0 = GbSTM_HWFltT0 | GbSTM_HWFltT0T0;

	// Phase current over-current failure bits
	GbSTM_IaOCSW = GbSTM_IaOCSW | GbSTM_IaOCSWT0;
	GbSTM_IbOCSW = GbSTM_IbOCSW | GbSTM_IbOCSWT0;
	GbSTM_IcOCSW = GbSTM_IcOCSW | GbSTM_IcOCSWT0;

	// Phase current out-of-range failure bits
	GbSTM_IaOORSW = GbSTM_IaOORSW | GbSTM_IaOORSWT0;
	GbSTM_IbOORSW = GbSTM_IbOORSW | GbSTM_IbOORSWT0;
	GbSTM_IcOORSW = GbSTM_IcOORSW | GbSTM_IcOORSWT0;

	// Phase current sum check failure bits
	GbSTM_IsSUMFltSW = GbSTM_IsSUMFltSW | GbSTM_IsSUMFltSWT0;

	// Over-speed failure bit
	GbSTM_WrpmOSSW = GbSTM_WrpmOSSW | GbSTM_WrpmOSSWT0;

	// Motor speed out-of-range failure bit
	GbSTM_WrpmOORSW = GbSTM_WrpmOORSW | GbSTM_WrpmOORSWT0;

	// Motor speed lock failure bit
	GbSTM_WrpmLockSW = GbSTM_WrpmLockSW | GbSTM_WrpmLockSWT0;

	// 5V over-voltage failure bits
	GbSTM_V05OVSW = GbSTM_V05OVSW | GbSTM_V05OVSWT0;

	// 5V under-voltage failure bit
	GbSTM_V05UVSW = GbSTM_V05UVSW | GbSTM_V05UVSWT0;

	// 5V out-of-range failure bits
	GbSTM_V05OORSW = GbSTM_V05OORSW | GbSTM_V05OORSWT0;

	// T0 SW failure indicator
	GbSTM_SWFltT0 = GbSTM_SWFltT0 | GbSTM_SWFltT0T0;

	// T0 failure indicator
	GbSTM_FltT0 = GbSTM_FltT0 | GbSTM_FltT0T0;
}

void STM_DiagT1(void)
{
	float LfCCC_AbsErrId = 0.0, LfCCC_AbsErrIq = 0.0;
	boolean LbSTM_CANTimeOutSWT2 = FALSE;

	// Loss of Track diagnostics
	LfCCC_AbsErrId = fabsf(GfCCC_ErrId);
	LfCCC_AbsErrIq = fabsf(GfCCC_ErrIq);

	if ((LfCCC_AbsErrId > KfSTM_ErrIdqLimit) || (LfCCC_AbsErrIq > KfSTM_ErrIdqLimit))
	{
		if (Gu16STM_LOTCNT >= Ku16STM_LOTCNTMax)
		{
			Gu16STM_LOTCNT = Ku16STM_LOTCNTMax;
		}
		else
		{
			Gu16STM_LOTCNT = Gu16STM_LOTCNT + 1;
		}
	}
	else
	{
		if (Gu16STM_LOTCNT > 1)
		{
			Gu16STM_LOTCNT = Gu16STM_LOTCNT - 1;
		}
		else
		{
			Gu16STM_LOTCNT = 0;
		}
	}

	if (Gu16STM_LOTCNT > Ku16STM_LOTCNTSet)
	{
		GbSTM_LOTSWT1 = TRUE;
	}
	else
	{
		GbSTM_LOTSWT1 = FALSE;
	}

	// SW IMS temp. over-temperature Check
	if (GfMES_TempIMS > KfSTM_TempIMSOTSet)
	{
		Gu8STM_TempIMS_OT_FailCntr ++;

		if (Gu8STM_TempIMS_OT_FailCntr > Ku8STM_TempIMS_OT_CntFail)
		{
			GbSTM_TempIMSOTSWT1 = TRUE;
			Gu8STM_TempIMS_OT_FailCntr = (uint8)0;
		}
	}

	if (GfMES_TempIMS <= KfSTM_TempIMSOT_Clear)
	{
		Gu8STM_TempIMS_OT_PassCntr ++;

		if (Gu8STM_TempIMS_OT_PassCntr > Ku8STM_TempIMS_OT_CntPass)
		{
			GbSTM_TempIMSOTSWT1 = FALSE;
			Gu8STM_TempIMS_OT_PassCntr = (uint8)0;
			Gu8STM_TempIMS_OT_FailCntr = (uint8)0;
		}
	}
	else
	{
		Gu8STM_TempIMS_OT_PassCntr = (uint8)0;
	}

	// SW IMS Temp. out-of-range check
	if ((GfMES_TempIMS > KfSTM_TempIMSOORHiSet) || (GfMES_TempIMS < KfSTM_TempIMSOORLoSet))
	{
		GbSTM_TempIMSOORSWT1 = TRUE;
	}
	else
	{
		GbSTM_TempIMSOORSWT1 = FALSE;
	}


	// SW Temp. sensor 1 over-temperature Check
	if (GfMES_TempSens1 > KfSTM_TempSens1OTSet)
	{
		GbSTM_TempSens1OTSWT1 = TRUE;
	}
	else
	{
		GbSTM_TempSens1OTSWT1 = FALSE;
	}

	// SW Temp. sensor 1 out-of-range check
	if ((GfMES_TempSens1 > KfSTM_TempSens1OORHiSet) || (GfMES_TempSens1 < KfSTM_TempSens1OORLoSet))
	{
		GbSTM_TempSens1OORSWT1 = TRUE;
	}
	else
	{
		GbSTM_TempSens1OORSWT1 = FALSE;
	}

	// SW Temp. sensor 2 over-temperature Check
	if (GfMES_TempSens2 > KfSTM_TempSens2OTSet)
	{
		GbSTM_TempSens2OTSWT1 = TRUE;
	}
	else
	{
		GbSTM_TempSens2OTSWT1 = FALSE;
	}

	// SW Temp. sensor 2 out-of-range check
	if ((GfMES_TempSens2 > KfSTM_TempSens2OORHiSet) || (GfMES_TempSens2 < KfSTM_TempSens2OORLoSet))
	{
		GbSTM_TempSens2OORSWT1 = TRUE;
	}
	else
	{
		GbSTM_TempSens2OORSWT1 = FALSE;
	}

	// SW Temp. sensor 3 over-temperature Check
	if (GfMES_TempSens3 > KfSTM_TempSens3OTSet)
	{
		GbSTM_TempSens3OTSWT1 = TRUE;
	}
	else
	{
		GbSTM_TempSens3OTSWT1 = FALSE;
	}

	// SW Temp. sensor 2 out-of-range check
	if ((GfMES_TempSens3 > KfSTM_TempSens3OORHiSet) || (GfMES_TempSens3 < KfSTM_TempSens3OORLoSet))
	{
		GbSTM_TempSens3OORSWT1 = TRUE;
	}
	else
	{
		GbSTM_TempSens3OORSWT1 = FALSE;
	}

	// SW Temp. board 1 over-temperature Check
	if (GfMES_TempBdr1 > KfSTM_TempBdr1OTSet)
	{
		Gu8STM_TempBrd1_OT_FailCntr ++;

		if (Gu8STM_TempBrd1_OT_FailCntr > Ku8STM_TempBrd1_OT_CntFail)
		{
			GbSTM_TempBdr1OTSWT1 = TRUE;
			Gu8STM_TempBrd1_OT_FailCntr = (uint8)0;
		}
	}

	if (GfMES_TempBdr1 <= KfSTM_TempBdr1OT_Clear)
	{
		Gu8STM_TempBrd1_OT_PassCntr ++;

		if (Gu8STM_TempBrd1_OT_PassCntr > Ku8STM_TempBrd1_OT_CntPass)
		{
			GbSTM_TempBdr1OTSWT1 = FALSE;
			Gu8STM_TempBrd1_OT_PassCntr = (uint8)0;
			Gu8STM_TempBrd1_OT_FailCntr = (uint8)0;
		}
	}
	else
	{
		Gu8STM_TempBrd1_OT_PassCntr = (uint8)0;
	}

	// SW Temp. board 1 out-of-range check
	if ((GfMES_TempBdr1 > KfSTM_TempBdr1OORHiSet) || (GfMES_TempBdr1 < KfSTM_TempBdr1OORLoSet))
	{
		GbSTM_TempBdr1OORSWT1 = TRUE;
	}
	else
	{
		GbSTM_TempBdr1OORSWT1 = FALSE;
	}

	// SW Temp. board 2 over-temperature Check
	if (GfMES_TempBdr2 > KfSTM_TempBdr2OTSet)
	{
		Gu8STM_TempBrd2_OT_FailCntr ++;

		if (Gu8STM_TempBrd2_OT_FailCntr > Ku8STM_TempBrd2_OT_CntFail)
		{
			GbSTM_TempBdr2OTSWT1 = TRUE;
			Gu8STM_TempBrd2_OT_FailCntr = (uint8)0;
		}
	}

	if (GfMES_TempBdr2 <= KfSTM_TempBdr2OT_Clear)
	{
		Gu8STM_TempBrd2_OT_PassCntr ++;

		if (Gu8STM_TempBrd2_OT_PassCntr > Ku8STM_TempBrd2_OT_CntPass)
		{
			GbSTM_TempBdr2OTSWT1 = FALSE;
			Gu8STM_TempBrd2_OT_PassCntr = (uint8)0;
			Gu8STM_TempBrd2_OT_FailCntr = (uint8)0;
		}
	}
	else
	{
		Gu8STM_TempBrd2_OT_PassCntr = (uint8)0;
	}

	// SW Temp. board 2 out-of-range check
	if ((GfMES_TempBdr2 > KfSTM_TempBdr2OORHiSet) || (GfMES_TempBdr2 < KfSTM_TempBdr2OORLoSet))
	{
		GbSTM_TempBdr2OORSWT1 = TRUE;
	}
	else
	{
		GbSTM_TempBdr2OORSWT1 = FALSE;
	}

	// SW Motor over-temperature Check
	if (GfMES_TempMot > KfSTM_TempMotOTSet)
	{
		GbSTM_TempMotOTSWT1 = TRUE;
	}
	else
	{
		GbSTM_TempMotOTSWT1 = FALSE;
	}

	// SW Motor out-of-range check
	if ((GfMES_TempMot > KfSTM_TempMotOORHiSet) || (GfMES_TempMot < KfSTM_TempMotOORLoSet))
	{
		GbSTM_TempMotOORSWT1 = TRUE;
	}
	else
	{
		GbSTM_TempMotOORSWT1 = FALSE;
	}

	// SW Temp. UNDER-temperature Check
	if ((GfMES_TempIMS < KfSTM_UnderTempSet) ||
		(GfMES_TempBdr1 < KfSTM_UnderTempSet) ||
		(GfMES_TempBdr2 < KfSTM_UnderTempSet) ||
		(GfCAN_CoolantTemp < KfSTM_UnderTempSet))
	{
		Gu8STM_UnderTempFailCntr ++;

		if (Gu8STM_UnderTempFailCntr > Ku8STM_UnderTempCntFail)
		{
			GbSTM_UnderTempSWT1 = TRUE;
			Gu8STM_UnderTempFailCntr = (uint8)0;
		}
	}

	if ((GfMES_TempIMS >= KfSTM_UnderTempClr) &&
		(GfMES_TempBdr1 >= KfSTM_UnderTempClr) &&
		(GfMES_TempBdr2 >= KfSTM_UnderTempClr) &&
		(GfCAN_CoolantTemp >= KfSTM_UnderTempSet))
	{
		Gu8STM_UnderTempPassCntr ++;

		if (Gu8STM_UnderTempPassCntr > Ku8STM_UnderTempCntPass)
		{
			GbSTM_UnderTempSWT1 = FALSE;
			Gu8STM_UnderTempPassCntr = (uint8)0;
			Gu8STM_UnderTempFailCntr = (uint8)0;
		}
	}
	else
	{
		Gu8STM_UnderTempPassCntr = (uint8)0;
	}

	// SW Temp. board 2 out-of-range check
	if ((GfMES_TempBdr2 > KfSTM_TempBdr2OORHiSet) || (GfMES_TempBdr2 < KfSTM_TempBdr2OORLoSet))
	{
		GbSTM_TempBdr2OORSWT1 = TRUE;
	}
	else
	{
		GbSTM_TempBdr2OORSWT1 = FALSE;
	}


	// Ext Coolant Temp OVER-temperature Check
	if (GfCAN_CoolantTemp > KfSTM_ExtCoolTempOT_Set)
	{
		Gu8STM_ExtCoolTempOT_FailCntr ++;

		if (Gu8STM_ExtCoolTempOT_FailCntr > Ku8STM_ExtCoolTempOT_CntFail)
		{
			GbSTM_ExtCoolTempOTSWT1 = TRUE;
			Gu8STM_ExtCoolTempOT_FailCntr = (uint8)0;
		}
	}

	if (GfCAN_CoolantTemp <= KfSTM_ExtCoolTempOT_Clear)
	{
		Gu8STM_ExtCoolTempOT_PassCntr ++;

		if (Gu8STM_ExtCoolTempOT_PassCntr > Ku8STM_ExtCoolTempOT_CntPass)
		{
			GbSTM_ExtCoolTempOTSWT1 = FALSE;
			Gu8STM_ExtCoolTempOT_PassCntr = (uint8)0;
			Gu8STM_ExtCoolTempOT_FailCntr = (uint8)0;
		}
	}
	else
	{
		Gu8STM_ExtCoolTempOT_PassCntr = (uint8)0;
	}


	// Comp Inlet Temp over-temperature Check
	if (GfCAN_InletTemp > KfSTM_InletTempOT_Set)
	{
		Gu8STM_InletTempOT_FailCntr ++;

		if (Gu8STM_InletTempOT_FailCntr > Ku8STM_InletTempOT_CntFail)
		{
			GbSTM_InletTempOTSWT1 = TRUE;
			Gu8STM_InletTempOT_FailCntr = (uint8)0;
		}
	}

	if (GfCAN_InletTemp <= KfSTM_InletTempOT_Clear)
	{
		Gu8STM_InletTempOT_PassCntr ++;

		if (Gu8STM_InletTempOT_PassCntr > Ku8STM_InletTempOT_CntPass)
		{
			GbSTM_InletTempOTSWT1 = FALSE;
			Gu8STM_InletTempOT_PassCntr = (uint8)0;
			Gu8STM_InletTempOT_FailCntr = (uint8)0;
		}
	}
	else
	{
		Gu8STM_InletTempOT_PassCntr = (uint8)0;
	}
	
	if (KbCAN_CANCmdUpdt == TRUE)
	{
		LbSTM_CANTimeOutSWT2 = GbCANRx_CMD2TimeOutSWT2 | GbCANRx_CMD1TimeOutSWT2 | GbCANRx_CMD0TimeOutSWT2;
	}
	else
	{
		LbSTM_CANTimeOutSWT2 = FALSE;
	}

	// SW 12V OV Check
	if (GfMES_V12 > KfSTM_V12OVSet)
	{
		Gu8STM_V12_OV_FailCntr ++;

		if (Gu8STM_V12_OV_FailCntr > Ku8STM_V12_OV_CntFail)
		{
			GbSTM_V12OVSWT1 = TRUE;
			Gu8STM_V12_OV_FailCntr = (uint8)0;
		}
	}

	if (GfMES_V12 <= KfSTM_V12OV_Clear)
	{
		Gu8STM_V12_OV_PassCntr ++;

		if (Gu8STM_V12_OV_PassCntr > Ku8STM_V12_OV_CntPass)
		{
			GbSTM_V12OVSWT1 = FALSE;
			Gu8STM_V12_OV_PassCntr = (uint8)0;
			Gu8STM_V12_OV_FailCntr = (uint8)0;
		}
	}
	else
	{
		Gu8STM_V12_OV_PassCntr = (uint8)0;
	}


	// SW 12V UV Check
	if (GfMES_V12 < KfSTM_V12UVSet)
	{
		Gu8STM_V12_UV_FailCntr ++;

		if (Gu8STM_V12_UV_FailCntr > Ku8STM_V12_UV_CntFail)
		{
			GbSTM_V12UVSWT1 = TRUE;
			Gu8STM_V12_UV_FailCntr = (uint8)0;
		}
	}

	if (GfMES_V12 >= KfSTM_V12UV_Clear)
	{
		Gu8STM_V12_UV_PassCntr ++;

		if (Gu8STM_V12_UV_PassCntr > Ku8STM_V12_UV_CntPass)
		{
			GbSTM_V12UVSWT1 = FALSE;
			Gu8STM_V12_UV_PassCntr = (uint8)0;
			Gu8STM_V12_UV_FailCntr = (uint8)0;
		}
	}
	else
	{
		Gu8STM_V12_UV_PassCntr = (uint8)0;
	}

	// SW 12V OOR check
	if (GfMES_V12 > KfSTM_V12OORSet)
	{
		GbSTM_V12OORSWT1 = TRUE;
	}
	else
	{
		GbSTM_V12OORSWT1 = FALSE;
	}

	// SW VDC OV Check
	if (GfMES_Vdc > KfSTM_VdcOVSet)
	{
		Gu8STM_VdcOV_FailCntr ++;

		if (Gu8STM_VdcOV_FailCntr > Ku8STM_VdcOV_CntFail)
		{
			GbSTM_VdcOVSWT1 = TRUE;
			Gu8STM_VdcOV_FailCntr = (uint8)0;
		}
	}

	if (GfMES_Vdc <= KfSTM_VdcOV_Clear)
	{
		Gu8STM_VdcOV_PassCntr ++;

		if (Gu8STM_VdcOV_PassCntr > Ku8STM_VdcOV_CntPass)
		{
			GbSTM_VdcOVSWT1 = FALSE;
			Gu8STM_VdcOV_PassCntr = (uint8)0;
			Gu8STM_VdcOV_FailCntr = (uint8)0;
		}
	}
	else
	{
		Gu8STM_VdcOV_PassCntr = (uint8)0;
	}


	// SW VDC UV Check
	if (GfMES_Vdc < KfSTM_VdcUVSet)
	{
		Gu8STM_VdcUV_FailCntr ++;

		if (Gu8STM_VdcUV_FailCntr > Ku8STM_VdcUV_CntFail)
		{
			GbSTM_VdcUVSWT1 = TRUE;
			Gu8STM_VdcUV_FailCntr = (uint8)0;
		}
	}

	if (GfMES_Vdc >= KfSTM_VdcUV_Clear)
	{
		Gu8STM_VdcUV_PassCntr ++;

		if (Gu8STM_VdcUV_PassCntr > Ku8STM_VdcUV_CntPass)
		{
			GbSTM_VdcUVSWT1 = FALSE;
			Gu8STM_VdcUV_PassCntr = (uint8)0;
			Gu8STM_VdcUV_FailCntr = (uint8)0;
		}
	}
	else
	{
		Gu8STM_VdcUV_PassCntr = (uint8)0;
	}

	// SW VDC OOR check
	if (GfMES_Vdc > KfSTM_VdcOORSet)
	{
		GbSTM_VdcOORSWT1 = TRUE;
	}
	else
	{
		GbSTM_VdcOORSWT1 = FALSE;
	}

	GbSTM_FltT1T1 = (GbSTM_LOTSWT1 |
			GbSTM_TempIMSOTSWT1 |
			GbSTM_TempIMSOORSWT1 |
			GbSTM_TempSens1OTSWT1 |
			GbSTM_TempSens1OORSWT1 |
			GbSTM_TempSens2OTSWT1 |
			GbSTM_TempSens2OORSWT1 |
			GbSTM_TempSens3OTSWT1 |
			GbSTM_TempSens3OORSWT1 |
			GbSTM_TempBdr1OTSWT1 |
			GbSTM_TempBdr1OORSWT1 |
			GbSTM_TempBdr2OTSWT1 |
			GbSTM_TempBdr2OORSWT1 |
			GbSTM_UnderTempSWT1 |
			GbSTM_TempMotOTSWT1 |
			GbSTM_TempMotOORSWT1 |
			GbSTM_ExtCoolTempOTSWT1 |
			GbSTM_ExtCoolTempUTSWT1 |
			GbSTM_InletTempOTSWT1 |
			GbSTM_V12OVSWT1 |
			GbSTM_V12UVSWT1 |
			GbSTM_V12OORSWT1 |
			GbSTM_VdcOVSWT1 |
			GbSTM_VdcUVSWT1 |
			GbSTM_VdcOORSWT1 |
			LbSTM_CANTimeOutSWT2) & KbSTM_SWFltEnbl;
}

// Latching T1 faults
void STM_LatchingT1Flt(void)
{
	// LOT
	GbSTM_LOTSW = GbSTM_LOTSW | GbSTM_LOTSWT1;

	// IMS temp. over-temperature failure bit
	GbSTM_TempIMSOTSW = GbSTM_TempIMSOTSW | GbSTM_TempIMSOTSWT1;

	// IMS temp. out-of-range failure bits
	GbSTM_TempIMSOORSW = GbSTM_TempIMSOORSW | GbSTM_TempIMSOORSWT1;

	// Temp. sens. 1 over-temperature failure bit
	GbSTM_TempSens1OTSW = GbSTM_TempSens1OTSW | GbSTM_TempSens1OTSWT1;

	// Temp. sens. 1 out-of-range failure bits
	GbSTM_TempSens1OORSW = GbSTM_TempSens1OORSW | GbSTM_TempSens1OORSWT1;

	// Temp. sens. 2 over-temperature failure bit
	GbSTM_TempSens2OTSW = GbSTM_TempSens2OTSW | GbSTM_TempSens2OTSWT1;

	// Temp. sens. 2 out-of-range failure bits
	GbSTM_TempSens2OORSW = GbSTM_TempSens2OORSW | GbSTM_TempSens2OORSWT1;

	// Temp. sens. 3 over-temperature failure bit
	GbSTM_TempSens3OTSW = GbSTM_TempSens3OTSW | GbSTM_TempSens3OTSWT1;

	// Temp. sens. 3 out-of-range failure bits
	GbSTM_TempSens3OORSW = GbSTM_TempSens3OORSW | GbSTM_TempSens3OORSWT1;

	// Temp. board 1 over-temperature failure bit
	GbSTM_TempBdr1OTSW = GbSTM_TempBdr1OTSW | GbSTM_TempBdr1OTSWT1;

	// Temp. board 1 out-of-range failure bits
	GbSTM_TempBdr1OORSW = GbSTM_TempBdr1OORSW | GbSTM_TempBdr1OORSWT1;

	// Temp. board 2 over-temperature failure bit
	GbSTM_TempBdr2OTSW = GbSTM_TempBdr2OTSW | GbSTM_TempBdr2OTSWT1;

	// Temp. board 2 out-of-range failure bits
	GbSTM_TempBdr2OORSW = GbSTM_TempBdr2OORSW | GbSTM_TempBdr2OORSWT1;

	// Motor over-temperature failure bit
	GbSTM_TempMotOTSW = GbSTM_TempMotOTSW | GbSTM_TempMotOTSWT1;

	// Motor out-of-range failure bits
	GbSTM_TempMotOORSW = GbSTM_TempMotOORSW | GbSTM_TempMotOORSWT1;

	GbSTM_UnderTempSW = GbSTM_UnderTempSW | GbSTM_UnderTempSWT1;

	//Ext Coolant Temp. over-temperature failure bit
	GbSTM_ExtCoolTempOTSW = GbSTM_ExtCoolTempOTSW | GbSTM_ExtCoolTempOTSWT1;

	//Ext Coolant Temp. Under-temperature failure bit
	GbSTM_ExtCoolTempUTSW = GbSTM_ExtCoolTempUTSW | GbSTM_ExtCoolTempUTSWT1;

	//Comp Inlet Temp. over-temperature failure bit
	GbSTM_InletTempOTSW = GbSTM_InletTempOTSW | GbSTM_InletTempOTSWT1;

	// 12V over-voltage failure bits
	GbSTM_V12OVSW = GbSTM_V12OVSW | GbSTM_V12OVSWT1;

	// 12V under-voltage failure bit
	GbSTM_V12UVSW = GbSTM_V12UVSW | GbSTM_V12UVSWT1;

	// 12V out-of-range failure bits
	GbSTM_V12OORSW = GbSTM_V12OORSW | GbSTM_V12OORSWT1;

	// Vdc over-voltage failure bit
	GbSTM_VdcOVSW = GbSTM_VdcOVSW | GbSTM_VdcOVSWT1;
	// Vdc under-voltage failure bit
	GbSTM_VdcUVSW = GbSTM_VdcUVSW | GbSTM_VdcUVSWT1;

	// Vdc out-of-range failure bit
	GbSTM_VdcOORSW = GbSTM_VdcOORSW | GbSTM_VdcOORSWT1;

	//CAN timeout based on CMD_1
	GbSTM_CANCMD1TimeOutSW = GbSTM_CANCMD1TimeOutSW | GbCANRx_CMD1TimeOutSWT2;

	//CAN timeout based on CMD_0
	GbSTM_CANCMD0TimeOutSW = GbSTM_CANCMD0TimeOutSW | GbCANRx_CMD0TimeOutSWT2;

	GbSTM_FltT1 = GbSTM_FltT1 | GbSTM_FltT1T1;
}

void STM_ResetT0Flt(void)
{
	GbSTM_IabcOCHW = FALSE;
	GbSTM_IaOCSW = FALSE;
	GbSTM_IbOCSW = FALSE;
	GbSTM_IcOCSW = FALSE;
	GbSTM_IaOORSW = FALSE;
	GbSTM_IbOORSW = FALSE;
	GbSTM_IcOORSW = FALSE;
	GbSTM_IsSUMFltSW = FALSE;
	GbSTM_VdcOVSW = FALSE;
	GbSTM_VdcUVSW = FALSE;
	GbSTM_VdcOORSW = FALSE;
	GbSTM_WrpmOSSW = FALSE;
	GbSTM_WrpmOORSW = FALSE;
	GbSTM_V12OVSW = FALSE;
	GbSTM_V12UVSW = FALSE;
	GbSTM_V12OORSW = FALSE;
	GbSTM_V05OVSW = FALSE;
	GbSTM_V05UVSW = FALSE;
	GbSTM_V05OORSW = FALSE;

	GbSTM_FltT0 = FALSE;
	GbSTM_HWFltT0 = FALSE;
	GbSTM_SWFltT0 = FALSE;
}

void STM_ResetT1Flt(void)
{
	GbSTM_LOTSW = FALSE;
	GbSTM_TempIMSOTSW = FALSE;
	GbSTM_TempIMSOORSW = FALSE;
	GbSTM_TempSens1OTSW = FALSE;
	GbSTM_TempSens1OORSW = FALSE;
	GbSTM_TempSens2OTSW = FALSE;
	GbSTM_TempSens2OORSW = FALSE;
	GbSTM_TempSens3OTSW = FALSE;
	GbSTM_TempSens3OORSW = FALSE;
	GbSTM_TempBdr1OTSW = FALSE;
	GbSTM_TempBdr1OORSW = FALSE;
	GbSTM_TempBdr2OTSW = FALSE;
	GbSTM_TempBdr2OORSW = FALSE;
	GbSTM_TempMotOTSW = FALSE;
	GbSTM_TempMotOORSW = FALSE;
	GbSTM_UnderTempSW = FALSE;
	GbSTM_ExtCoolTempOTSW = FALSE;
	GbSTM_ExtCoolTempUTSW = FALSE;
	GbSTM_InletTempOTSW = FALSE;
	GbSTM_CANCMD1TimeOutSW = FALSE;
	GbSTM_CANCMD0TimeOutSW = FALSE;

	GbSTM_FltT1 = FALSE;
}

void STM_ThermalWarning(void)
{
	if (GfMES_TempIMS > KfSTM_TempIMS_Warn)
	{
		if (Gu8STM_TempIMS_WarnCntr >= Ku8STM_TempIMS_WarnCnt)
		{
			GbSTM_TempIMS_Warn = TRUE;
			Gu8STM_TempIMS_WarnCntr = (uint8)0;
		}
		else if (GbSTM_TempIMS_Warn == FALSE)
		{
			Gu8STM_TempIMS_WarnCntr ++;
		}

		Gu8STM_TempIMS_NoWarnCntr = (uint8)0;
	}
	else if (GfMES_TempIMS <= KfSTM_TempIMSOT_Clear)
	{
		if (Gu8STM_TempIMS_NoWarnCntr >= Ku8STM_TempIMS_WarnCnt)
		{
			GbSTM_TempIMS_Warn = FALSE;
			Gu8STM_TempIMS_NoWarnCntr = (uint8)0;
		}
		else if (GbSTM_TempIMS_Warn == TRUE)
		{
			Gu8STM_TempIMS_NoWarnCntr ++;
		}
		Gu8STM_TempIMS_WarnCntr = (uint8)0;
	}

	if ((GfMES_TempBdr1 > KfSTM_TempBdr2OT_Warn) ||
		(GfMES_TempBdr2 > KfSTM_TempBdr2OT_Warn))
	{
		if (Gu8STM_BrdTempWarnCntr >= Ku8STM_BrdTempWarnCnt)
		{
			GbSTM_BrdTempWarn = TRUE;
			Gu8STM_BrdTempWarnCntr = (uint8)0;
		}
		else if (GbSTM_BrdTempWarn == FALSE)
		{
			Gu8STM_BrdTempWarnCntr ++;
		}

		Gu8STM_BrdTempNoWarnCntr = (uint8)0;
	}
	else if ((GfMES_TempBdr1 <= KfSTM_TempBdr1OT_Clear) &&
			 (GfMES_TempBdr2 <= KfSTM_TempBdr2OT_Clear))

	{
		if (Gu8STM_BrdTempNoWarnCntr >= Ku8STM_BrdTempWarnCnt)
		{
			GbSTM_BrdTempWarn = FALSE;
			Gu8STM_BrdTempNoWarnCntr = (uint8)0;
		}
		else if (GbSTM_BrdTempWarn == TRUE)
		{
			Gu8STM_BrdTempNoWarnCntr ++;
		}
		Gu8STM_BrdTempWarnCntr = (uint8)0;
	}

	if (GfCAN_CoolantTemp > KfSTM_ExtCoolTempOT_Warn)
	{
		if (Gu8STM_ExtCoolTempWarnCntr >= Ku8STM_ExtCoolTempWarnCnt)
		{
			GbSTM_ExtCoolTempWarn = TRUE;
			Gu8STM_ExtCoolTempWarnCntr = (uint8)0;
		}
		else if (GbSTM_ExtCoolTempWarn == FALSE)
		{
			Gu8STM_ExtCoolTempWarnCntr ++;
		}

		Gu8STM_ExtCoolTempNoWarnCntr = (uint8)0;
	}
	else if (GfCAN_CoolantTemp <= KfSTM_ExtCoolTempOT_Clear)
	{
		if (Gu8STM_ExtCoolTempNoWarnCntr >= Ku8STM_ExtCoolTempWarnCnt)
		{
			GbSTM_ExtCoolTempWarn = FALSE;
			Gu8STM_ExtCoolTempNoWarnCntr = (uint8)0;
		}
		else if (GbSTM_ExtCoolTempWarn == TRUE)
		{
			Gu8STM_ExtCoolTempNoWarnCntr ++;
		}
		Gu8STM_ExtCoolTempWarnCntr = (uint8)0;
	}


	if (GfCAN_InletTemp > KfSTM_InletTempOT_Warn)
	{
		if (Gu8STM_InletTempWarnCntr >= Ku8STM_InletTempWarnCnt)
		{
			GbSTM_InletTempWarn = TRUE;
			Gu8STM_InletTempWarnCntr = (uint8)0;
		}
		else if (GbSTM_InletTempWarn == FALSE)
		{
			Gu8STM_InletTempWarnCntr ++;
		}

		Gu8STM_InletTempNoWarnCntr = (uint8)0;
	}
	else if (GfCAN_InletTemp <= KfSTM_InletTempOT_Clear)
	{
		if (Gu8STM_InletTempNoWarnCntr >= Ku8STM_InletTempWarnCnt)
		{
			GbSTM_InletTempWarn = FALSE;
			Gu8STM_InletTempNoWarnCntr = (uint8)0;
		}
		else if (GbSTM_InletTempWarn == TRUE)
		{
			Gu8STM_InletTempNoWarnCntr ++;
		}
		Gu8STM_InletTempWarnCntr = (uint8)0;
	}
}

void STM_VoltageWarning(void)
{
	if (GfMES_V12 > KfSTM_V12_WarnSetHi)
	{
		if (Gu8STM_V12_WarnCntrHi >= Ku8STM_V12_WarnCntHi)
		{
			GbSTM_V12_WarnHi = TRUE;
			Gu8STM_V12_WarnCntrHi = (uint8)0;
		}
		else if (GbSTM_V12_WarnHi == FALSE)
		{
			Gu8STM_V12_WarnCntrHi ++;
		}

		Gu8STM_V12_NoWarnCntrHi = (uint8)0;
	}
	else if (GfMES_V12 <= KfSTM_V12_WarnClrHi)
	{
		if (Gu8STM_V12_NoWarnCntrHi >= Ku8STM_V12_WarnCntHi)
		{
			GbSTM_V12_WarnHi = FALSE;
			Gu8STM_V12_NoWarnCntrHi = (uint8)0;
		}
		else if (GbSTM_V12_WarnHi == TRUE)
		{
			Gu8STM_V12_NoWarnCntrHi ++;
		}
		Gu8STM_V12_WarnCntrHi = (uint8)0;
	}


	if (GfMES_V12 < KfSTM_V12_WarnSetLo)
	{
		if (Gu8STM_V12_WarnCntrLo >= Ku8STM_V12_WarnCntLo)
		{
			GbSTM_V12_WarnLo = TRUE;
			Gu8STM_V12_WarnCntrLo = (uint8)0;
		}
		else if (GbSTM_V12_WarnLo == FALSE)
		{
			Gu8STM_V12_WarnCntrLo ++;
		}

		Gu8STM_V12_NoWarnCntrLo = (uint8)0;
	}
	else if (GfMES_V12 >= KfSTM_V12_WarnClrLo)
	{
		if (Gu8STM_V12_NoWarnCntrLo >= Ku8STM_V12_WarnCntLo)
		{
			GbSTM_V12_WarnLo = FALSE;
			Gu8STM_V12_NoWarnCntrLo = (uint8)0;
		}
		else if (GbSTM_V12_WarnLo == TRUE)
		{
			Gu8STM_V12_NoWarnCntrLo ++;
		}
		Gu8STM_V12_WarnCntrLo = (uint8)0;
	}


	if (GfMES_Vdc > KfSTM_VdcWarnSetHi)
	{
		if (Gu8STM_VdcWarnCntrHi >= Ku8STM_VdcWarnCntHi)
		{
			GbSTM_VdcWarnHi = TRUE;
			Gu8STM_VdcWarnCntrHi = (uint8)0;
		}
		else if (GbSTM_VdcWarnHi == FALSE)
		{
			Gu8STM_VdcWarnCntrHi ++;
		}

		Gu8STM_VdcNoWarnCntrHi = (uint8)0;
	}
	else if (GfMES_Vdc <= KfSTM_VdcWarnClrHi)
	{
		if (Gu8STM_VdcNoWarnCntrHi >= Ku8STM_VdcWarnCntHi)
		{
			GbSTM_VdcWarnHi = FALSE;
			Gu8STM_VdcNoWarnCntrHi = (uint8)0;
		}
		else if (GbSTM_VdcWarnHi == TRUE)
		{
			Gu8STM_VdcNoWarnCntrHi ++;
		}
		Gu8STM_VdcWarnCntrHi = (uint8)0;
	}

	if (GfMES_Vdc < KfSTM_VdcWarnSetLo)
	{
		if (Gu8STM_VdcWarnCntrLo >= Ku8STM_VdcWarnCntLo)
		{
			GbSTM_VdcWarnLo = TRUE;
			Gu8STM_VdcWarnCntrLo = (uint8)0;
		}
		else if (GbSTM_VdcWarnLo == FALSE)
		{
			Gu8STM_VdcWarnCntrLo ++;
		}

		Gu8STM_VdcNoWarnCntrLo = (uint8)0;
	}
	else if (GfMES_Vdc >= KfSTM_VdcWarnClrLo)
	{
		if (Gu8STM_VdcNoWarnCntrLo >= Ku8STM_VdcWarnCntLo)
		{
			GbSTM_VdcWarnLo = FALSE;
			Gu8STM_VdcNoWarnCntrLo = (uint8)0;
		}
		else if (GbSTM_VdcWarnLo == TRUE)
		{
			Gu8STM_VdcNoWarnCntrLo ++;
		}
		Gu8STM_VdcWarnCntrLo = (uint8)0;
	}

}

// Self-healing condition check
void STM_SelfHealCondChkT1(void)
{
	if ((GbSTM_TempIMSOTSW == TRUE && GbSTM_TempIMSOTSWT1 == FALSE) ||
		(GbSTM_TempBdr1OTSW == TRUE && GbSTM_TempBdr1OTSWT1 == FALSE) ||
		(GbSTM_TempBdr2OTSW == TRUE && GbSTM_TempBdr2OTSWT1 == FALSE) ||
		(GbSTM_UnderTempSW == TRUE && GbSTM_UnderTempSWT1 == FALSE) ||
		(GbSTM_ExtCoolTempOTSW == TRUE && GbSTM_ExtCoolTempOTSWT1 == FALSE) ||
		(GbSTM_ExtCoolTempUTSW == TRUE && GbSTM_ExtCoolTempUTSWT1 == FALSE) ||
		(GbSTM_InletTempOTSW == TRUE && GbSTM_InletTempOTSWT1 == FALSE) ||
		(GbSTM_V12OVSW == TRUE && GbSTM_V12OVSWT1 == FALSE) ||
		(GbSTM_V12UVSW == TRUE && GbSTM_V12UVSWT1 == FALSE) ||
		(GbSTM_VdcOVSW == TRUE && GbSTM_VdcOVSWT1 == FALSE) ||
		(GbSTM_VdcUVSW == TRUE && GbSTM_VdcUVSWT1 == FALSE))
	{
		GbSTM_SelfHealCheckT1 = TRUE;
	}
	else
	{
		GbSTM_SelfHealCheckT1 = FALSE;
	}
}
