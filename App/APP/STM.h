/*
 * STM.h
 *
 *  Created on: Oct 15, 2018
 *      Author: H247670
 */

#ifndef APP_APP_STM_H_
#define APP_APP_STM_H_

#include	"vadc.h"
#include	"stdlib.h"
#include	"math.h"
#include <Ifx_Types.h>
#include	"GPIO.h"
#include    "BUILD.h"

typedef enum
{
	Off,
	TurnOn,
	StandBy,
	Run,
	TurnOff,
	Fault
} State;

typedef enum
{
	OpenLoopVoltage,
	ClosedLoopCurrent,
	DirectDutyCommand,
	SensorlessControl
} TestMode;

typedef enum
{
	StandStill,
	OpenLoop,
	ClosedLoop
} RunSubState;

typedef enum
{
	SpeedControl,
	TorqueControl
} ControlMode;


#define RPM_TO_RADS		0.104719755

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/
// Open-loop control test mode
extern TestMode KeSTM_TestModeCmd;

// Enabling open-loop manual-voltage set
extern boolean KbSTM_EnblManVdcOpenLoop;

extern boolean KbSTM_SelfHealEnbl;

// Manual voltage
extern boolean KfSTM_ManVdcOpenLoop;

// Open-loop control voltage command (V)
extern float KfSTM_MagVoltCmdOpenLoop;

// Open-loop control voltage command Offset (V)
extern float KfSTM_VoltOffsetCmdOpenLoop;

// Open-loop control voltage slew rate limit (V/s)
extern float KfSTM_MagVoltSlewLimOpenLoop;

// Open-loop control voltage command max (V)
extern float KfSTM_MagVoltCmdOpenLoopMax;

// Open-loop control voltage command min (V)
extern float KfSTM_MagVoltCmdOpenLoopMin;

// Open-loop control speed (r/min)
extern float KfSTM_WrpmOpenLoop;

// Open-loop control speed slew limit (r/min/s)
extern float KfSTM_WrpmCmdOpenLoopSlew;

// Open-loop control speed Max. limit (Hz)
extern float KfSTM_WrpmOpenLoopMax;

// Open-loop control speed Min. limit (Hz)
extern float KfSTM_WrpmOpenLoopMin;

// Open-loop control current command (A)
extern float KfSTM_IdCmdOpenLoop;
extern float KfSTM_IqCmdOpenLoop;

// Duty command for direct duty command mode
extern float KfSTM_DutyACmd;
extern float KfSTM_DutyBCmd;
extern float KfSTM_DutyCCmd;


extern State KeSTM_ManStateCmd;

// Control mode command
extern ControlMode KeSTM_ControlModeCmd;

// StandStill time limit
extern uint32 Ku32STM_RunStandStillCNTLim;

// Run-closed loop state transition delay time
extern uint16 Ku16STM_RunClosedCNTSet;

// State transition speed threshold for OL to CL (r/min)
extern float KfSTM_RunOLtoCL;

// State transition speed threshold for CL to OL (r/min)
extern float KfSTM_RunCLtoOL;

// Speed Command in torque control mode
extern float KfSTM_WrpmCmdPosTe;
extern float KfSTM_WrpmCmdNegTe;

// State transition speed threshold for StandStill (r/min)
extern float KfSTM_RunStandStillThrsRPM;

// HW fault mask for SW test
extern boolean KbSTM_HWFltEnbl;

// SW fault enable for SW test
extern boolean KbSTM_SWFltEnbl;

// Over-current level
extern float KfSTM_IsOCSet;

// X-of-Y counter of IsOC
extern uint16 Ku16STM_IsOCXCNTSet;
extern uint16 Ku16STM_IsOCYCNTSet;

// Phase current out-of-range level
extern float KfSTM_IsOORSet;

// Phase current sum check
extern float KfSTM_IsSUMFltSet;

// Vdc over-voltage level
extern float KfSTM_VdcOVSet;
extern float KfSTM_VdcWarnSetHi;
extern float KfSTM_VdcWarnClrHi;
extern float KfSTM_VdcOV_Clear;

extern float Ku8STM_VdcOV_CntFail;
extern float Ku8STM_VdcOV_CntPass;
extern float Ku8STM_VdcWarnCntHi;

// Vdc under-voltage level
extern float KfSTM_VdcUVSet;
extern float KfSTM_VdcWarnSetLo;
extern float KfSTM_VdcWarnClrLo;
extern float KfSTM_VdcUV_Clear;

extern float Ku8STM_VdcUV_CntFail;
extern float Ku8STM_VdcUV_CntPass;
extern float Ku8STM_VdcWarnCntLo;

// Vdc out-of-range level
extern float KfSTM_VdcOORSet;

// Over-speed failure level
extern float KfSTM_WrpmOSSet;

// Motor speed out-of-range failure level
extern float KfSTM_WrpmOORSet;

// Motor speed lock failure: speed difference & Time duration
extern float KfSTM_WrpmLockDiffSet;
extern float KfSTM_WrpmLockTimeSet;

// 12V over-voltage level
extern float KfSTM_V12OVSet;
extern float KfSTM_V12_WarnSetHi;
extern float KfSTM_V12_WarnClrHi;
extern float KfSTM_V12OV_Clear;
extern float Ku8STM_V12_OV_CntFail;
extern float Ku8STM_V12_OV_CntPass;
extern float Ku8STM_V12_WarnCntHi;

// 12V under-voltage level
extern float KfSTM_V12UVSet;
extern float KfSTM_V12_WarnSetLo;
extern float KfSTM_V12_WarnClrLo;
extern float KfSTM_V12UV_Clear;
extern float Ku8STM_V12_UV_CntFail;
extern float Ku8STM_V12_UV_CntPass;
extern float Ku8STM_V12_WarnCntLo;

// 12V out-of-range level
extern float KfSTM_V12OORSet;

// 5V over-voltage level
extern float KfSTM_V05OVSet;

// 5V under-voltage level
extern float KfSTM_V05UVSet;

// 5V out-of-range level
extern float KfSTM_V05OORSet;

// IMS board over-temperature level
extern float KfSTM_TempIMSOTSet;
extern float KfSTM_TempIMS_Warn;
extern float KfSTM_TempIMSOT_Clear;
extern uint8 Ku8STM_TempIMS_OT_CntFail;
extern uint8 Ku8STM_TempIMS_OT_CntPass;
extern uint8 Ku8STM_TempIMS_WarnCnt;

// IMS board out-of-range level
extern float KfSTM_TempIMSOORHiSet;
extern float KfSTM_TempIMSOORLoSet;

// Temp. sens. 1 over-temperature level
extern float KfSTM_TempSens1OTSet;

// Temp. sens. 1 out-of-range level
extern float KfSTM_TempSens1OORHiSet;
extern float KfSTM_TempSens1OORLoSet;

// Temp. sens. 2 over-temperature level
extern float KfSTM_TempSens2OTSet;

// Temp. sens. 2 out-of-range level
extern float KfSTM_TempSens2OORHiSet;
extern float KfSTM_TempSens2OORLoSet;

// Temp. sens. 3 over-temperature level
extern float KfSTM_TempSens3OTSet;

// Temp. sens. 3 out-of-range level
extern float KfSTM_TempSens3OORHiSet;
extern float KfSTM_TempSens3OORLoSet;

// Temp. board 1 over-temperature level
extern float KfSTM_TempBdr1OTSet;
extern float KfSTM_TempBdr1OT_Warn;
extern float KfSTM_TempBdr1OT_Clear;
extern uint8 Ku8STM_TempBrd1_OT_CntFail;
extern uint8 Ku8STM_TempBrd1_OT_CntPass;

extern uint8 Ku8STM_BrdTempWarnCnt;

// Temp. board 1 out-of-range level
extern float KfSTM_TempBdr1OORHiSet;
extern float KfSTM_TempBdr1OORLoSet;

// Temp. board 2 over-temperature level
extern float KfSTM_TempBdr2OTSet;
extern float KfSTM_TempBdr2OT_Warn;
extern float KfSTM_TempBdr2OT_Clear;
extern uint8 Ku8STM_TempBrd2_OT_CntFail;
extern uint8 Ku8STM_TempBrd2_OT_CntPass;

// Temp. board 2 out-of-range level
extern float KfSTM_TempBdr2OORHiSet;
extern float KfSTM_TempBdr2OORLoSet;

// Motor over-temperature level
extern float KfSTM_TempMotOTSet;

// Under Temperature levels
extern float KfSTM_UnderTempSet;
extern float KfSTM_UnderTempClr;
extern uint8 Ku8STM_UnderTempCntFail;
extern uint8 Ku8STM_UnderTempCntPass;

// Motor out-of-range level
extern float KfSTM_TempMotOORHiSet;
extern float KfSTM_TempMotOORLoSet;

// LOT diagnostics
extern float KfSTM_ErrIdqLimit;

// LOT diagnostics counter threshold (1 CNT = 1ms)
extern uint16 Ku16STM_LOTCNTSet;
extern uint16 Ku16STM_LOTCNTMax;

// Ext Coolant Temp.  over-temperature level
extern float KfSTM_ExtCoolTempOT_Set;
extern float KfSTM_ExtCoolTempOT_Warn;
extern float KfSTM_ExtCoolTempOT_Clear;
extern uint8 Ku8STM_ExtCoolTempOT_CntFail;
extern uint8 Ku8STM_ExtCoolTempOT_CntPass;
extern uint8 Ku8STM_ExtCoolTempWarnCnt;

// Ext Coolant Temp.  Under-temperature level
extern float KfSTM_ExtCoolTempUT_Set;
extern float KfSTM_ExtCoolTempUT_Clear;
extern uint8 Ku8STM_ExtCoolTempUT_CntFail;
extern uint8 Ku8STM_ExtCoolTempUT_CntPass;

// Compressor Inlet Temp.  over-temperature level
extern float KfSTM_InletTempOT_Set;
extern float KfSTM_InletTempOT_Warn;
extern float KfSTM_InletTempOT_Clear;
extern uint8 Ku8STM_InletTempOT_CntFail;
extern uint8 Ku8STM_InletTempOT_CntPass;
extern uint8 Ku8STM_InletTempWarnCnt;


// SW version
extern uint16 Ku16STM_SWVersion;


/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
// State Command
extern State GeSTM_StateCmd;

extern State GeSTM_LastStateT0;
extern State GeSTM_CurrentStateT0;
extern State GeSTM_NextStateT0;

extern State GeSTM_LastStateT1;
extern State GeSTM_CurrentStateT1;
extern State GeSTM_NextStateT1;

// SubState of RunState
extern RunSubState GeSTM_RunSubState;
extern RunSubState GeSTM_RunSubStateNext;

// Control mode - speed or torque control
extern ControlMode GeSTM_ControlModeCmd;
extern ControlMode GeSTM_ControlMode;

// CNT variable in StandStill state
extern volatile uint32 Gu32STM_RunStandStillCNT;

// StandStill state ready flag
extern volatile boolean GbSTM_RunStandStillDone;

// State transition delay counter in Run-closed loop state
extern volatile uint16 Gu16STM_RunClosedLoopCNT;

// Run-closed loop state transition delay count set flag
extern volatile boolean GbSTM_RunClosedLoopCNTDone;

// Open loop frequency command (slew-rate limited)
extern volatile float GfSTM_fCmdOpenLoop;

// T0 Start Counter
extern volatile uint32 Gu32STM_T0Cnt0;

// T0 End Counter
extern volatile uint32 Gu32STM_T0Cnt1;

// T0 Throughput counter
extern volatile uint32 Gu32STM_T0ThruPt;

// T2 Start Counter
extern volatile uint32 Gu32STM_T2Cnt0;

// T2 End Counter
extern volatile uint32 Gu32STM_T2Cnt1;

// T2 Throughput counter
extern volatile uint32 Gu32STM_T2ThruPt;


// T0 HW failure indicator
extern volatile boolean GbSTM_HWFltT0;

// T0 SW failure indicator
extern volatile boolean GbSTM_SWFltT0;

// T0 failure indicator
extern volatile boolean GbSTM_FltT0;

// T1 SW failure indicator
extern volatile boolean GbSTM_FltT1;

// Status of Fault Reset
extern volatile boolean GbSTM_StatusFltReset;


// HW failure  bits
extern volatile boolean GbSTM_IabcOCHW;
extern volatile boolean GbSTM_VdcOVHW;

// Phase current over-current failure bits
extern volatile boolean GbSTM_IaOCSW;
extern volatile boolean GbSTM_IbOCSW;
extern volatile boolean GbSTM_IcOCSW;

// Phase current Out-of-Range failure bits
extern volatile boolean GbSTM_IaOORSW;
extern volatile boolean GbSTM_IbOORSW;
extern volatile boolean GbSTM_IcOORSW;

// Phase current sum check failure bits
extern volatile boolean GbSTM_IsSUMFltSW;

// Vdc over-voltage failure bits
extern volatile boolean GbSTM_VdcOVSW;

// Vdc under-voltage failure bit
extern volatile boolean GbSTM_VdcUVSW;

// Vdc out-of-range failure bits
extern volatile boolean GbSTM_VdcOORSW;

// Over-speed failure bit
extern volatile boolean GbSTM_WrpmOSSW;

// Motor speed out-of-range failure bit
extern volatile boolean GbSTM_WrpmOORSW;

// Motor speed lock failure bit
extern volatile boolean GbSTM_WrpmLockSW;

// 12V over-voltage failure bits
extern volatile boolean GbSTM_V12OVSW;

// 12V under-voltage failure bit
extern volatile boolean GbSTM_V12UVSW;

// 12V out-of-range failure bits
extern volatile boolean GbSTM_V12OORSW;

extern volatile uint8 Gu8STM_V12_OV_FailCntr;
extern volatile uint8 Gu8STM_V12_OV_PassCntr;
extern volatile uint8 Gu8STM_V12_UV_FailCntr;
extern volatile uint8 Gu8STM_V12_UV_PassCntr;

// 5V over-voltage failure bits
extern volatile boolean GbSTM_V05OVSW;

// 5V under-voltage failure bit
extern volatile boolean GbSTM_V05UVSW;

// 5V out-of-range failure bits
extern volatile boolean GbSTM_V05OORSW;

// IMS temp. over-temperature failure bit
extern volatile boolean GbSTM_TempIMSOTSW;

// IMS temp. out-of-range failure bits
extern volatile boolean GbSTM_TempIMSOORSW;

// Temp. sens. 1 over-temperature failure bit
extern volatile boolean GbSTM_TempSens1OTSW;

// Temp. sens. 1 out-of-range failure bits
extern volatile boolean GbSTM_TempSens1OORSW;

// Temp. sens. 2 over-temperature failure bit
extern volatile boolean GbSTM_TempSens2OTSW;

// Temp. sens. 2 out-of-range failure bits
extern volatile boolean GbSTM_TempSens2OORSW;

// Temp. sens. 3 over-temperature failure bit
extern volatile boolean GbSTM_TempSens3OTSW;

// Temp. sens. 3 out-of-range failure bits
extern volatile boolean GbSTM_TempSens3OORSW;

// Temp. board 1 over-temperature failure bit
extern volatile boolean GbSTM_TempBdr1OTSW;

// Temp. board 1 out-of-range failure bits
extern volatile boolean GbSTM_TempBdr1OORSW;

// Temp. board 2 over-temperature failure bit
extern volatile boolean GbSTM_TempBdr2OTSW;

// Temp. board 2 out-of-range failure bits
extern volatile boolean GbSTM_TempBdr2OORSW;

// Motor over-temperature failure bit
extern volatile boolean GbSTM_TempMotOTSW;

// Motor out-of-range failure bits
extern volatile boolean GbSTM_TempMotOORSW;

extern boolean GbSTM_UnderTempSWT1;
extern boolean GbSTM_UnderTempSW;
extern uint8 Gu8STM_UnderTempFailCntr;
extern uint8 Gu8STM_UnderTempPassCntr;

// Motor LOT failure bit
extern volatile boolean GbSTM_LOTSW;

// Coolant Temp over-temperature failure bit
extern volatile boolean GbSTM_ExtCoolTempOTSW;

// Coolant Temp Under-temperature failure bit
extern volatile boolean GbSTM_ExtCoolTempUTSW;

// Compressor Inlet Temp. over-temperature failure bit
extern volatile boolean GbSTM_InletTempOTSW;

// CMD1 CAN timeout Fault
extern volatile boolean GbSTM_CANCMD1TimeOutSW;

// CMD0 CAN timeout Fault
extern volatile boolean GbSTM_CANCMD0TimeOutSW;

// Board Power Fault
extern volatile boolean GbSTM_BrdPwrFltSW;

// For instantaneous fault sampling
// T0 HW failure indicator
extern volatile boolean GbSTM_HWFltT0T0;

// T0 SW failure indicator
extern volatile boolean GbSTM_SWFltT0T0;

// T0 failure indicator
extern volatile boolean GbSTM_FltT0T0;

// T1 failure indicator
extern volatile boolean GbSTM_FltT1T1;

// HW failure bits
extern volatile boolean GbSTM_IabcOCHWT0;
extern volatile boolean GbSTM_VdcOVHWT0;

// Phase current over-current failure bits
extern volatile boolean GbSTM_IaOCSWT0;
extern volatile boolean GbSTM_IbOCSWT0;
extern volatile boolean GbSTM_IcOCSWT0;

// X-of-Y counter for OC check
extern volatile uint16 Gu16STM_IsOCYCNT;
extern volatile uint16 Gu16STM_IaOCXCNT;
extern volatile uint16 Gu16STM_IbOCXCNT;
extern volatile uint16 Gu16STM_IcOCXCNT;

// Phase current out-of-range failure bits
extern volatile boolean GbSTM_IaOORSWT0;
extern volatile boolean GbSTM_IbOORSWT0;
extern volatile boolean GbSTM_IcOORSWT0;

// Phase current sum check failure bits
extern volatile boolean GbSTM_IsSUMFltSWT0;

// Vdc over-voltage failure bit
extern volatile boolean GbSTM_VdcOVSWT1;

// Vdc under-voltage failure bit
extern volatile boolean GbSTM_VdcUVSWT1;

// Vdc out-of-range failure bit
extern volatile boolean GbSTM_VdcOORSWT1;

extern volatile uint8 Gu8STM_VdcOV_FailCntr;
extern volatile uint8 Gu8STM_VdcOV_PassCntr;
extern volatile uint8 Gu8STM_VdcUV_FailCntr;
extern volatile uint8 Gu8STM_VdcUV_PassCntr;

// Over-speed failure bit
extern volatile boolean GbSTM_WrpmOSSWT0;

// Motor speed out-of-range failure bit
extern volatile boolean GbSTM_WrpmOORSWT0;

// Motor speed lock failure bit
extern volatile boolean GbSTM_WrpmLockSWT0;

// 12V over-voltage failure bits
extern volatile boolean GbSTM_V12OVSWT1;

// 12V under-voltage failure bit
extern volatile boolean GbSTM_V12UVSWT1;

// 12V out-of-range failure bits
extern volatile boolean GbSTM_V12OORSWT1;

// 5V over-voltage failure bits
extern volatile boolean GbSTM_V05OVSWT0;

// 5V under-voltage failure bit
extern volatile boolean GbSTM_V05UVSWT0;

// 5V out-of-range failure bits
extern volatile boolean GbSTM_V05OORSWT0;

// IMS temp. over-temperature failure bit
extern volatile boolean GbSTM_TempIMSOTSWT1;
extern volatile uint8 Gu8STM_TempIMS_OT_FailCntr;
extern volatile uint8 Gu8STM_TempIMS_OT_PassCntr;

// IMS temp. out-of-range failure bits
extern volatile boolean GbSTM_TempIMSOORSWT1;

// Temp. sens. 1 over-temperature failure bit
extern volatile boolean GbSTM_TempSens1OTSWT1;

// Temp. sens. 1 out-of-range failure bits
extern volatile boolean GbSTM_TempSens1OORSWT1;

// Temp. sens. 2 over-temperature failure bit
extern volatile boolean GbSTM_TempSens2OTSWT1;

// Temp. sens. 2 out-of-range failure bits
extern volatile boolean GbSTM_TempSens2OORSWT1;

// Temp. sens. 3 over-temperature failure bit
extern volatile boolean GbSTM_TempSens3OTSWT1;

// Temp. sens. 3 out-of-range failure bits
extern volatile boolean GbSTM_TempSens3OORSWT1;

// Temp. board 1 over-temperature failure bit
extern volatile boolean GbSTM_TempBdr1OTSWT1;

// Temp. board 1 out-of-range failure bits
extern volatile boolean GbSTM_TempBdr1OORSWT1;

// Temp. board 2 over-temperature failure bit
extern volatile boolean GbSTM_TempBdr2OTSWT1;

// Temp. board 2 out-of-range failure bits
extern volatile boolean GbSTM_TempBdr2OORSWT1;

extern volatile uint8 Gu8STM_TempBrd1_OT_FailCntr;
extern volatile uint8 Gu8STM_TempBrd1_OT_PassCntr;

extern volatile uint8 Gu8STM_TempBrd2_OT_FailCntr;
extern volatile uint8 Gu8STM_TempBrd2_OT_PassCntr;

// Motor over-temperature failure bit
extern volatile boolean GbSTM_TempMotOTSWT1;

// Motor out-of-range failure bits
extern volatile boolean GbSTM_TempMotOORSWT1;

// Motor LOT failure bit
extern volatile boolean GbSTM_LOTSWT1;

// Motor LOT counter
extern volatile uint16 Gu16STM_LOTCNT;

// Ext Coolant Temp over-temperature failure bit
extern volatile boolean GbSTM_ExtCoolTempOTSWT1;
extern volatile uint8 Gu8STM_ExtCoolTempOT_FailCntr;
extern volatile uint8 Gu8STM_ExtCoolTempOT_PassCntr;

// Ext Coolant Temp under-temperature failure bit
extern volatile boolean GbSTM_ExtCoolTempUTSWT1;
extern volatile uint8 Gu8STM_ExtCoolTempUT_FailCntr;
extern volatile uint8 Gu8STM_ExtCoolTempUT_PassCntr;

// Compressor Inlet Temp over-temperature failure bit
extern volatile boolean GbSTM_InletTempOTSWT1;
extern volatile uint8 Gu8STM_InletTempOT_FailCntr;
extern volatile uint8 Gu8STM_InletTempOT_PassCntr;

extern volatile uint8 Gu8STM_BrdTempWarnCntr;
extern volatile uint8 Gu8STM_BrdTempNoWarnCntr;
extern volatile uint8 Gu8STM_TempIMS_WarnCntr;
extern volatile uint8 Gu8STM_TempIMS_NoWarnCntr;

extern volatile uint8 Gu8STM_ExtCoolTempWarnCntr;
extern volatile uint8 Gu8STM_ExtCoolTempNoWarnCntr;
extern volatile uint8 Gu8STM_InletTempWarnCntr;
extern volatile uint8 Gu8STM_InletTempNoWarnCntr;

extern volatile uint8 Gu8STM_V12_WarnCntrHi;
extern volatile uint8 Gu8STM_V12_NoWarnCntrHi;
extern volatile uint8 Gu8STM_V12_WarnCntrLo;
extern volatile uint8 Gu8STM_V12_NoWarnCntrLo;

extern volatile uint8 Gu8STM_VdcWarnCntrHi;
extern volatile uint8 Gu8STM_VdcNoWarnCntrHi;

extern volatile uint8 Gu8STM_VdcWarnCntrLo;
extern volatile uint8 Gu8STM_VdcNoWarnCntrLo;

extern volatile boolean GbSTM_BrdTempWarn;

extern volatile boolean GbSTM_TempIMS_Warn;

extern volatile boolean GbSTM_ExtCoolTempWarn;

extern volatile boolean GbSTM_InletTempWarn;

extern volatile boolean GbSTM_V12_WarnHi;

extern volatile boolean GbSTM_V12_WarnLo;

extern volatile boolean GbSTM_VdcWarnHi;

extern volatile boolean GbSTM_VdcWarnLo;

extern volatile boolean GbSTM_SelfHealCheckT1;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
// T0 diagnostics
extern void STM_DiagT0(void);

// Latching T0 fault
extern void STM_LatchingT0Flt(void);

// T1 diagnostics
extern void STM_DiagT1(void);
extern void STM_ThermalWarning(void);
extern void STM_VoltageWarning(void);

extern void STM_SelfHealCondChkT1(void);

// Latching T1 faults
extern void STM_LatchingT1Flt(void);

extern void STM_ResetT0Flt(void);
extern void STM_ResetT1Flt(void);

extern void STM_OffT0(void);
extern void STM_TurnOnT0(void);
extern void STM_StandByT0(void);
extern void STM_RunT0(void);
extern void STM_TurnOffT0(void);
extern void STM_FaultT0(void);

extern void STM_OffT1(void);
extern void STM_TurnOnT1(void);
extern void STM_StandByT1(void);
extern void STM_RunT1(void);
extern void STM_TurnOffT1(void);
extern void STM_FaultT1(void);

extern void STM_OffT2(void);
extern void STM_TurnOnT2(void);
extern void STM_StandByT2(void);
extern void STM_RunT2(void);
extern void STM_TurnOffT2(void);
extern void STM_FaultT2(void);

// State machine for open-loop test
extern void STM_StandByOLT0(void);
extern void STM_RunOLT0(void);

extern void STM_StandByOLT1(void);
extern void STM_RunOLT1(void);

// Speed calculation in open-loop sub-state
extern float STM_OpenLoopSpeedRPMCalc(float LfSTM_Wrpm, float LfSTM_WrpmCmd, float LfSTM_Tsamp);
extern float STM_OpenLoopThetarCalc(float LfSTM_Thetar, float LfSTM_Wr, float LfSTM_Tsamp);

// Speed & torque command update
extern void STM_SpdTrqCmdUpdtCL(void);
extern void STM_SpdTrqCmdUpdtOL(void);



#endif /* APP_APP_STM_H_ */
