/*
 * MES.c
 *
 *  Created on: Oct 19, 2018
 *      Author: H247670
 */
#include	"vadc.h"
#include	"CCC.h"
#include	"math.h"
#include	"STM.h"
#include	"MES.h"
#include	"GTM.h"

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/

// Calibrations for B sample
float KfMES_ScaleIabc = -0.223418424, KfMES_OffsetIabc = 457.3375139;
float KfMES_ScaleVdc = 0.021707193, KfMES_OffsetVdc = -2.669984702;

float KfMES_ScaleVabc = 0.018727322, KfMES_OffsetVabc = -2.494460548;

float KfMES_ScaleV12 = 0.003611722, KfMES_OffsetV12 = 0.0;
float KfMES_ScaleV05 = 0.001343101, KfMES_OffsetV05 = 0.0;

float KfMES_ScaleVExt = 0.001221001, KfMES_OffsetVExt = 0.0;

float KfMES_ScaleVTP06 = 0.001221001, KfMES_OffsetVTP06 = 0.0;
float KfMES_ScaleVTP07 = 0.001221001, KfMES_OffsetVTP07 = 0.0;

float KfMES_ScaleTempIMS = 0.001221001, KfMES_OffsetTempIMS = 0.0;
float KfMES_ScaleTempBdr = 0.001221001, KfMES_OffsetTempBdr = 0.0;

float KfMES_ScaleTempSens = 0.001221001, KfMES_OffsetTempSens = 0.0;

float KfMES_k2TempIms = -2.67880;
float KfMES_k1TempIms = -82.46871;
float KfMES_k0TempIms = 184.97063;

float KfMES_k3TempSens = -4.818735;
float KfMES_k2TempSens = 44.12394;
float KfMES_k1TempSens = -165.3222;
float KfMES_k0TempSens = 315.4611;

float KfMES_k2TempRTD = 1.0243;
float KfMES_k1TempRTD = 79.172;
float KfMES_k0TempRTD = -68.334;

float KfMES_TempThermThrsh = 1.411;

float KfMES_k4TempTherm01 = 76.55030;
float KfMES_k3TempTherm01 = -336.4479;
float KfMES_k2TempTherm01 = 570.8831;
float KfMES_k1TempTherm01 = -515.4510;
float KfMES_k0TempTherm01 = 364.1244;

float KfMES_k4TempTherm02 = -69.85013;
float KfMES_k3TempTherm02 = 563.0697;
float KfMES_k2TempTherm02 = -1683.151;
float KfMES_k1TempTherm02 = 2149.869;
float KfMES_k0TempTherm02 = -857.2977;

// For current offset calculation
uint16 Ku16MES_CurrentOffsetCNTSet = 20;

// Vdc filter
float KfMES_WcVdqMagFilt = 6.283185307;

// Angle compensation for current/voltage reading
float KfMES_IdqThetarComp = 0.0;
float KfMES_VdqThetarComp = 0.0;

// Enables Manual Override for Rotating Direction
boolean KbMES_ManOvrdRotDirEnbl = FALSE;

// Selects Motor Rotation Direction
MotorRotDir KeMES_MotorRotDirOvrd = NormalRotation;

// Rotation Direction Sample counts
uint8 Ku8MES_RotDirSmplCounts = (uint8)10;

// Normal Rotation Direction counts
uint8 Ku8MES_NormalRotDirCounts = (uint8)7;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
float GfMES_Ia = 0.0, GfMES_Ic0In = 0.0, GfMES_Ic1In = 0.0, GfMES_Ib = 0.0, GfMES_Ib0In = 0.0, GfMES_Ib1In = 0.0, GfMES_Ic = 0.0;
float GfMES_Ia0In = 0.0;
float GfMES_Ia1In = 0.0;

float GfMES_Vdc = 0.0;
float GfMES_InvVdc = 0.0;

// Sum of 3-phase current
float GfMES_IabcSum = 0.0;

// For current offset calculation
float GfMES_G0RES00Sum = 0.0;
float GfMES_G0RES01Sum = 0.0;
float GfMES_G0RES02Sum = 0.0;

float GfMES_G1RES00Sum = 0.0;
float GfMES_G1RES02Sum = 0.0;

uint16 Gu16MES_CurrentOffsetCNT = 0;
boolean GbMES_CurrentoffsetCalcDone = FALSE;

// Calculated offset values
float GfMES_Ic0ffset = 0.0;
float GfMES_Ic0InOffset = 0.0;
float GfMES_Ic1InOffset = 0.0;
float GfMES_Ia0ffset = 0.0;
float GfMES_Ia0InOffset = 0.0;
float GfMES_Ia1InOffset = 0.0;
float GfMES_Ib0InOffset = 0.0;
float GfMES_Ib1InOffset = 0.0;

float GfMES_Vas = 0.0, GfMES_Vbs = 0.0, GfMES_Vcs = 0.0;

// Measured d- and q-axis voltages
float GfMES_Vdr = 0.0;
float GfMES_Vqr = 0.0;

float GfMES_MagVdq = 0.0;
float GfMES_MagVdqOld = 0.0;

float GfMES_MagVdqFilt = 0.0;

float GfMES_TempIMS = 0.0, GfMES_TempSens1 = 0.0, GfMES_TempSens2 = 0.0, GfMES_TempSens3 = 0.0, GfMES_TempBdr1 = 0.0, GfMES_TempBdr2 = 0.0;
float GfMES_V12 = 0.0, GfMES_V05 = 0.0, GfMES_VExt = 0.0;
float GfMES_VTP06 = 0.0, GfMES_VTP07 = 0.0;

// Motor temperature
float GfMES_TempMot = 0.0;

// Idr, Iqr
float GfMES_Idr = 0.0;
float GfMES_Iqr = 0.0;

float GfMES_Ids = 0.0;
float GfMES_Iqs = 0.0;


float GfMES_MagIdqr = 0.0;

float GfMES_IdrOld = 0.0;
float GfMES_IqrOld = 0.0;

volatile MotorRotDir GeMES_MotorRotDir = NormalRotation;

uint8 Gu8MES_NormalRotDirCntr = (uint8)0;
uint8 Gu8MES_RotDirSmplCntr = (uint8)0;

boolean GbMES_MotorRotDirDone = FALSE;


/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
// Calculation Iabc, Idq - B sample
void MES_CalcIabcIdq(void)
{
	float LfMES_Thetar = 0.0;

	LfMES_Thetar = GfCCC_Thetar + KfMES_IdqThetarComp * GfCCC_Wr;

	LfMES_Thetar = CCC_ThetarLim(LfMES_Thetar);

	if (GeMES_MotorRotDir == NormalRotation)
	{
		GfMES_Ic0In 	= (float)Gu16ADC_G0RES00 * KfMES_ScaleIabc - GfMES_Ic0InOffset;
		GfMES_Ia 	= (float)Gu16ADC_G0RES01 * KfMES_ScaleIabc - GfMES_Ia0ffset;
		GfMES_Ic1In 	= (float)Gu16ADC_G0RES02 * KfMES_ScaleIabc - GfMES_Ic1InOffset;

		GfMES_Ic = 0.5 * (GfMES_Ic0In + GfMES_Ic1In);

		GfMES_Ib0In 	= (float)Gu16ADC_G1RES00 * KfMES_ScaleIabc - GfMES_Ib0InOffset;
		GfMES_Ib1In 	= (float)Gu16ADC_G1RES02 * KfMES_ScaleIabc - GfMES_Ib1InOffset;

		GfMES_Ib = 0.5 * (GfMES_Ib0In + GfMES_Ib1In);
	}
	else
	{
		GfMES_Ia0In 	= (float)Gu16ADC_G0RES00 * KfMES_ScaleIabc - GfMES_Ia0InOffset;
		GfMES_Ic 	= (float)Gu16ADC_G0RES01 * KfMES_ScaleIabc - GfMES_Ic0ffset;
		GfMES_Ia1In 	= (float)Gu16ADC_G0RES02 * KfMES_ScaleIabc - GfMES_Ia1InOffset;

		GfMES_Ia = 0.5 * (GfMES_Ia0In + GfMES_Ia1In);

		GfMES_Ib0In 	= (float)Gu16ADC_G1RES00 * KfMES_ScaleIabc - GfMES_Ib0InOffset;
		GfMES_Ib1In 	= (float)Gu16ADC_G1RES02 * KfMES_ScaleIabc - GfMES_Ib1InOffset;

		GfMES_Ib = 0.5 * (GfMES_Ib0In + GfMES_Ib1In);
	}

	GfMES_IabcSum = GfMES_Ia + GfMES_Ib + GfMES_Ic;

	GfMES_Ids = ONE_OVER_THREE * (2 * GfMES_Ia - GfMES_Ib - GfMES_Ic);
	GfMES_Iqs = ONE_OVER_SQRT_THREE * (GfMES_Ib - GfMES_Ic);

	GfMES_IdrOld = GfMES_Idr;
	GfMES_IqrOld = GfMES_Iqr;

	GfMES_Idr = GfMES_Ids * cosf(LfMES_Thetar) + GfMES_Iqs * sinf(LfMES_Thetar);
	GfMES_Iqr = -GfMES_Ids * sinf(LfMES_Thetar) + GfMES_Iqs * cosf(LfMES_Thetar);

	GfMES_MagIdqr = sqrtf(GfMES_Idr * GfMES_Idr + GfMES_Iqr * GfMES_Iqr);
}

void MES_CalcIabcIdq02(void)
{
	float LfMES_Thetar = 0.0;

	LfMES_Thetar = GfCCC_Thetar + KfMES_IdqThetarComp * GfCCC_Wr;

	LfMES_Thetar = CCC_ThetarLim(LfMES_Thetar);

	GfMES_Idr = GfMES_Ids * cosf(LfMES_Thetar) + GfMES_Iqs * sinf(LfMES_Thetar);
	GfMES_Iqr = -GfMES_Ids * sinf(LfMES_Thetar) + GfMES_Iqs * cosf(LfMES_Thetar);
}

// For B sample
void MES_CalcVdc(void)
{
	// Reading Vdc or setting it to manual value
	if (KbSTM_EnblManVdcOpenLoop == FALSE)
	{
		GfMES_Vdc 	= (float)Gu16ADC_G1RES01 * KfMES_ScaleVdc + KfMES_OffsetVdc;
	}
	else
	{
		GfMES_Vdc = KfSTM_ManVdcOpenLoop;
	}

	if (GfMES_Vdc > INV_VDC_MIN)
	{
		GfMES_InvVdc = 1.0 / GfMES_Vdc;
	}
	else
	{
		GfMES_InvVdc = VDC_MIN;
	}
}

void MES_CalcVabc(void)
{

	float LfMES_Vds = 0.0, LfMES_Vqs = 0.0;
	float LfMES_k0filt = 0.0, LfMES_k1filt = 0.0, LfMES_Tmpfilt0 = 0.0, LfMES_Tmpfilt1 = 0.0, LfMES_Tmpfilt2 = 0.0;

	float LfMES_Thetar = 0.0;

	LfMES_Thetar = GfCCC_Thetar + KfMES_VdqThetarComp * GfCCC_Wr;

	LfMES_Thetar = CCC_ThetarLim(LfMES_Thetar);

	if (GeMES_MotorRotDir == NormalRotation)
	{
		GfMES_Vcs = (float)Gu16ADC_G0RES03 * KfMES_ScaleVabc + KfMES_OffsetVabc;
		GfMES_Vas = (float)Gu16ADC_G0RES04 * KfMES_ScaleVabc + KfMES_OffsetVabc;

		GfMES_Vbs = (float)Gu16ADC_G1RES03 * KfMES_ScaleVabc + KfMES_OffsetVabc;
	}
	else
	{
		GfMES_Vas = (float)Gu16ADC_G0RES03 * KfMES_ScaleVabc + KfMES_OffsetVabc;
		GfMES_Vcs = (float)Gu16ADC_G0RES04 * KfMES_ScaleVabc + KfMES_OffsetVabc;

		GfMES_Vbs = (float)Gu16ADC_G1RES03 * KfMES_ScaleVabc + KfMES_OffsetVabc;
	}

	LfMES_Vds = ONE_OVER_THREE * (2 * GfMES_Vas - GfMES_Vbs - GfMES_Vcs);
	LfMES_Vqs = ONE_OVER_SQRT_THREE * (GfMES_Vbs - GfMES_Vcs);

	GfMES_Vdr = LfMES_Vds * cosf(LfMES_Thetar) + LfMES_Vqs * sinf(LfMES_Thetar);
	GfMES_Vqr = -LfMES_Vds * sinf(LfMES_Thetar) + LfMES_Vqs * cosf(LfMES_Thetar);

	GfMES_MagVdq = sqrtf(GfMES_Vdr * GfMES_Vdr + GfMES_Vqr * GfMES_Vqr);

	LfMES_Tmpfilt0 = 0.5 * GfGTM_T0 * KfMES_WcVdqMagFilt;

	LfMES_Tmpfilt1 = (1 + LfMES_Tmpfilt0);
	LfMES_Tmpfilt2 = (1 - LfMES_Tmpfilt0);

	LfMES_k1filt = LfMES_Tmpfilt2 / LfMES_Tmpfilt1;
	LfMES_k0filt = LfMES_Tmpfilt0 / LfMES_Tmpfilt1;

	GfMES_MagVdqFilt = LfMES_k1filt * GfMES_MagVdqFilt + LfMES_k0filt * GfMES_MagVdq + LfMES_k0filt * GfMES_MagVdqOld;

	GfMES_MagVdqOld = GfMES_MagVdq;
}

// For B sample
void MES_CalcTemps(void)
{
	float LfMES_TempVolt = 0.0;

	LfMES_TempVolt = (float)Gu16ADC_G0RES05 * KfMES_ScaleTempIMS + KfMES_OffsetTempIMS;
	GfMES_TempIMS = MES_CalcTempIMS(LfMES_TempVolt);

	LfMES_TempVolt = (float)Gu16ADC_G0RES06 * KfMES_ScaleTempSens + KfMES_OffsetTempSens;
	GfMES_TempSens1 = MES_CalcTempSens(LfMES_TempVolt);

	LfMES_TempVolt = (float)Gu16ADC_G0RES07 * KfMES_ScaleTempSens + KfMES_OffsetTempSens;
	GfMES_TempSens2 = MES_CalcTempSens(LfMES_TempVolt);

	GfMES_V12 = (float)Gu16ADC_G0RES08 * KfMES_ScaleV12 + KfMES_OffsetV12;
	GfMES_VTP06 = (float)Gu16ADC_G0RES09 * KfMES_ScaleVTP06 + KfMES_OffsetVTP06;

	LfMES_TempVolt = (float)Gu16ADC_G1RES04 * KfMES_ScaleTempBdr + KfMES_OffsetTempBdr;
	GfMES_TempBdr1 = MES_CalcTempIMS(LfMES_TempVolt);

	LfMES_TempVolt = (float)Gu16ADC_G1RES05 * KfMES_ScaleTempBdr + KfMES_OffsetTempBdr;
	GfMES_TempBdr2 = MES_CalcTempIMS(LfMES_TempVolt);

	LfMES_TempVolt = (float)Gu16ADC_G1RES06 * KfMES_ScaleTempSens + KfMES_OffsetTempSens;
	GfMES_TempSens3 = MES_CalcTempSens(LfMES_TempVolt);

	GfMES_VExt = (float)Gu16ADC_G1RES07 * KfMES_ScaleVExt + KfMES_OffsetVExt;
	GfMES_V05 = (float)Gu16ADC_G1RES08 * KfMES_ScaleV05 + KfMES_OffsetV05;
	GfMES_VTP07 = (float)Gu16ADC_G1RES09 * KfMES_ScaleVTP07 + KfMES_OffsetVTP07;
}

float MES_CalcTempIMS(float LfMES_TempVolt)
{
	float LfMES_TempVoltSq = 0.0;
	float LfMES_TempResult = 0.0;

	LfMES_TempVoltSq = LfMES_TempVolt * LfMES_TempVolt;

	LfMES_TempResult = KfMES_k2TempIms * LfMES_TempVoltSq
			+ KfMES_k1TempIms * LfMES_TempVolt
			+ KfMES_k0TempIms;

	return LfMES_TempResult;
}

float MES_CalcTempSens(float LfMES_TempVolt)
{
	float LfMES_TempVoltSq = 0.0;
	float LfMES_TempVoltCu = 0.0;
	float LfMES_TempResult = 0.0;

	LfMES_TempVoltSq = LfMES_TempVolt * LfMES_TempVolt;
	LfMES_TempVoltCu = LfMES_TempVoltSq * LfMES_TempVolt;

	LfMES_TempResult = KfMES_k3TempSens * LfMES_TempVoltCu
			+ KfMES_k2TempSens * LfMES_TempVoltSq
			+ KfMES_k1TempSens * LfMES_TempVolt
			+ KfMES_k0TempSens;

	return LfMES_TempResult;
}

// For Hybrid A sample
float MES_CalcTempRTD(float LfMES_TempVolt)
{
	float LfMES_TempVoltSq = 0.0;
	float LfMES_TempResult = 0.0;

	LfMES_TempVoltSq = LfMES_TempVolt * LfMES_TempVolt;

	LfMES_TempResult = KfMES_k2TempRTD * LfMES_TempVoltSq
			+ KfMES_k1TempRTD * LfMES_TempVolt
			+ KfMES_k0TempRTD;

	return LfMES_TempResult;
}

// For Hybrid A sample
float MES_CalcTempTherm(float LfMES_TempVolt)
{
	float LfMES_TempVoltSq = 0.0;
	float LfMES_TempVoltCu = 0.0;
	float LfMES_TempResult = 0.0;

	LfMES_TempVoltSq = LfMES_TempVolt * LfMES_TempVolt;
	LfMES_TempVoltCu = LfMES_TempVoltSq * LfMES_TempVolt;

	if (LfMES_TempVolt < KfMES_TempThermThrsh)
	{

		LfMES_TempResult = KfMES_k4TempTherm01 * LfMES_TempVoltCu * LfMES_TempVolt
				+ KfMES_k3TempTherm01 * LfMES_TempVoltCu
				+ KfMES_k2TempTherm01 * LfMES_TempVoltSq
				+ KfMES_k1TempTherm01 * LfMES_TempVolt
				+ KfMES_k0TempTherm01;
	}
	else
	{
		LfMES_TempResult = KfMES_k4TempTherm02 * LfMES_TempVoltCu * LfMES_TempVolt
				+ KfMES_k3TempTherm02 * LfMES_TempVoltCu
				+ KfMES_k2TempTherm02 * LfMES_TempVoltSq
				+ KfMES_k1TempTherm02 * LfMES_TempVolt
				+ KfMES_k0TempTherm02;
	}

	return LfMES_TempResult;
}

// For B sample
void MES_CalcIabcOffset(void)
{
	float LfMES_InvCurrentOffsetCNTSet = 0.0;

	if (Gu16MES_CurrentOffsetCNT < Ku16MES_CurrentOffsetCNTSet)
	{
		GbMES_CurrentoffsetCalcDone = FALSE;

		GfMES_G0RES00Sum = GfMES_G0RES00Sum + (float)Gu16ADC_G0RES00;
		GfMES_G0RES01Sum = GfMES_G0RES01Sum + (float)Gu16ADC_G0RES01;
		GfMES_G0RES02Sum = GfMES_G0RES02Sum + (float)Gu16ADC_G0RES02;
		GfMES_G1RES00Sum = GfMES_G1RES00Sum + (float)Gu16ADC_G1RES00;
		GfMES_G1RES02Sum = GfMES_G1RES02Sum + (float)Gu16ADC_G1RES02;

		Gu16MES_CurrentOffsetCNT = Gu16MES_CurrentOffsetCNT + 1;
	}
	else
	{
		GbMES_CurrentoffsetCalcDone = TRUE;
		Gu16MES_CurrentOffsetCNT = 0;

		LfMES_InvCurrentOffsetCNTSet = 1.0 / Ku16MES_CurrentOffsetCNTSet;

		if (GeMES_MotorRotDir == NormalRotation)
		{
			GfMES_Ic0InOffset = GfMES_G0RES00Sum * LfMES_InvCurrentOffsetCNTSet * KfMES_ScaleIabc;
			GfMES_Ia0ffset = GfMES_G0RES01Sum * LfMES_InvCurrentOffsetCNTSet * KfMES_ScaleIabc;
			GfMES_Ic1InOffset = GfMES_G0RES02Sum * LfMES_InvCurrentOffsetCNTSet * KfMES_ScaleIabc;
			GfMES_Ib0InOffset = GfMES_G1RES00Sum * LfMES_InvCurrentOffsetCNTSet * KfMES_ScaleIabc;
			GfMES_Ib1InOffset = GfMES_G1RES02Sum * LfMES_InvCurrentOffsetCNTSet * KfMES_ScaleIabc;
		}
		else
		{
			GfMES_Ia0InOffset = GfMES_G0RES00Sum * LfMES_InvCurrentOffsetCNTSet * KfMES_ScaleIabc;
			GfMES_Ic0ffset = GfMES_G0RES01Sum * LfMES_InvCurrentOffsetCNTSet * KfMES_ScaleIabc;
			GfMES_Ia1InOffset = GfMES_G0RES02Sum * LfMES_InvCurrentOffsetCNTSet * KfMES_ScaleIabc;
			GfMES_Ib0InOffset = GfMES_G1RES00Sum * LfMES_InvCurrentOffsetCNTSet * KfMES_ScaleIabc;
			GfMES_Ib1InOffset = GfMES_G1RES02Sum * LfMES_InvCurrentOffsetCNTSet * KfMES_ScaleIabc;
		}
		GfMES_G0RES00Sum = 0.0;
		GfMES_G0RES01Sum = 0.0;
		GfMES_G0RES02Sum = 0.0;
		GfMES_G1RES00Sum = 0.0;
		GfMES_G1RES02Sum = 0.0;
	}
}

void MES_ResetIabcOffset(void)
{
	Gu16MES_CurrentOffsetCNT = 0;

	GbMES_CurrentoffsetCalcDone = FALSE;

	GfMES_G0RES00Sum = 0.0;
	GfMES_G0RES01Sum = 0.0;
	GfMES_G0RES02Sum = 0.0;
	GfMES_G1RES00Sum = 0.0;
	GfMES_G1RES02Sum = 0.0;
}


void MES_CaptureRotationDirection(void)
{
	if (KbMES_ManOvrdRotDirEnbl == TRUE)
	{
		GeMES_MotorRotDir = KeMES_MotorRotDirOvrd;

		GbMES_MotorRotDirDone = TRUE;

		// Clear Rotation direction counters
		Gu8MES_NormalRotDirCntr = (uint8)0;
		Gu8MES_RotDirSmplCntr = (uint8)0;
	}
	else
	{
		Gu8MES_RotDirSmplCntr = Gu8MES_RotDirSmplCntr + (uint8)1;

		if (GIO_GetMotRotDir() == TRUE)
		{
			Gu8MES_NormalRotDirCntr = Gu8MES_NormalRotDirCntr + (uint8)1;
		}

		if (Gu8MES_RotDirSmplCntr >= Ku8MES_RotDirSmplCounts)
		{
			if (Gu8MES_NormalRotDirCntr >= Ku8MES_NormalRotDirCounts)
			{
				GeMES_MotorRotDir = NormalRotation;
			}
			else
			{
				GeMES_MotorRotDir = ReverseRotation;
			}

			GbMES_MotorRotDirDone = TRUE;

			Gu8MES_NormalRotDirCntr = (uint8)0;
			Gu8MES_RotDirSmplCntr = (uint8)0;
		}
	}
}
