/*
 * SVM.c
 *
 *  Created on: Oct 18, 2018
 *      Author: H247670
 */
#include	"vadc.h"
#include	"MES.h"
#include	"math.h"
#include	"CCC.h"
#include	"SVM.h"
#include	"GTM.h"

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/
// dqr to ABC angle advance constant
float KfSVM_AngleAdvCoeff = 1.5;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
// ABC phase voltage command
float GfSVM_VasCmd = 0.0;
float GfSVM_VbsCmd = 0.0;
float GfSVM_VcsCmd = 0.0;

// ABC pole voltage command
float GfSVM_VanCmd = 0.0;
float GfSVM_VbnCmd = 0.0;
float GfSVM_VcnCmd = 0.0;

// dqr to ABC conversion angle
float GfSVM_ThetarAdv = 0.0;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
void SVM_SVPWM(void)
{
	float LfSVM_VsnCmd = 0.0;
	float LfSVM_VCmdMax = 0.0;
	float LfSVM_VCmdMin = 0.0;
	float LfSVM_HalfVdc = 0.0;

	if (GfSVM_VasCmd > GfSVM_VbsCmd)
	{
		LfSVM_VCmdMax = GfSVM_VasCmd;
		LfSVM_VCmdMin = GfSVM_VbsCmd;
	}
	else
	{
		LfSVM_VCmdMax = GfSVM_VbsCmd;
		LfSVM_VCmdMin = GfSVM_VasCmd;
	}

	if (GfSVM_VcsCmd > LfSVM_VCmdMax)
	{
		LfSVM_VCmdMax = GfSVM_VcsCmd;
	}
	else if (GfSVM_VcsCmd < LfSVM_VCmdMin)
	{
		LfSVM_VCmdMin = GfSVM_VcsCmd;
	}

	LfSVM_VsnCmd = -0.5 * (LfSVM_VCmdMax + LfSVM_VCmdMin);

	GfSVM_VanCmd = GfSVM_VasCmd + LfSVM_VsnCmd;
	GfSVM_VbnCmd = GfSVM_VbsCmd + LfSVM_VsnCmd;
	GfSVM_VcnCmd = GfSVM_VcsCmd + LfSVM_VsnCmd;

	LfSVM_HalfVdc = 0.5 * GfMES_Vdc;

	if (GfSVM_VanCmd > LfSVM_HalfVdc)
	{
		GfSVM_VanCmd = LfSVM_HalfVdc;
	}
	else if (GfSVM_VanCmd < -LfSVM_HalfVdc)
	{
		GfSVM_VanCmd = -LfSVM_HalfVdc;
	}

	if (GfSVM_VbnCmd > LfSVM_HalfVdc)
	{
		GfSVM_VbnCmd = LfSVM_HalfVdc;
	}
	else if (GfSVM_VbnCmd < -LfSVM_HalfVdc)
	{
		GfSVM_VbnCmd = -LfSVM_HalfVdc;
	}

	if (GfSVM_VcnCmd > LfSVM_HalfVdc)
	{
		GfSVM_VcnCmd = LfSVM_HalfVdc;
	}
	else if (GfSVM_VcnCmd < -LfSVM_HalfVdc)
	{
		GfSVM_VcnCmd = -LfSVM_HalfVdc;
	}

	if (GeMES_MotorRotDir == NormalRotation)
	{
		GsGTM_OutputPWM.dutyCycleA = (GfSVM_VcnCmd + LfSVM_HalfVdc) * GfMES_InvVdc;
		GsGTM_OutputPWM.dutyCycleB = (GfSVM_VbnCmd + LfSVM_HalfVdc) * GfMES_InvVdc;
		GsGTM_OutputPWM.dutyCycleC = (GfSVM_VanCmd + LfSVM_HalfVdc) * GfMES_InvVdc;
	}
	else
	{
		GsGTM_OutputPWM.dutyCycleA = (GfSVM_VanCmd + LfSVM_HalfVdc) * GfMES_InvVdc;
		GsGTM_OutputPWM.dutyCycleB = (GfSVM_VbnCmd + LfSVM_HalfVdc) * GfMES_InvVdc;
		GsGTM_OutputPWM.dutyCycleC = (GfSVM_VcnCmd + LfSVM_HalfVdc) * GfMES_InvVdc;
	}
}

void SVM_VdqToVabc(void)
{
	float LfSVM_VdsCmd = 0.0, LfSVM_VqsCmd = 0.0;

	GfSVM_ThetarAdv = GfCCC_Thetar + KfSVM_AngleAdvCoeff * GfGTM_T0 * GfCCC_Wr;

	if (GfSVM_ThetarAdv > TWO_PI)
	{
		GfSVM_ThetarAdv = GfSVM_ThetarAdv - TWO_PI;
	}
	else if (GfSVM_ThetarAdv < -TWO_PI)
	{
		GfSVM_ThetarAdv = GfSVM_ThetarAdv + TWO_PI;
	}

	LfSVM_VdsCmd = cosf(GfSVM_ThetarAdv) * GfCCC_VdCmd - sinf(GfSVM_ThetarAdv) * GfCCC_VqCmd;
	LfSVM_VqsCmd = sinf(GfSVM_ThetarAdv) * GfCCC_VdCmd + cosf(GfSVM_ThetarAdv) * GfCCC_VqCmd;

	GfSVM_VasCmd = LfSVM_VdsCmd;
	GfSVM_VbsCmd = -0.5 * (LfSVM_VdsCmd - SQRT_THREE * LfSVM_VqsCmd);
	GfSVM_VcsCmd = -(GfSVM_VasCmd + GfSVM_VbsCmd);
}
