/*
 * STM_OpenLoopTest.c
 *
 *  Created on: Oct 25, 2018
 *      Author: H247670
 */
#include	"STM.h"
#include	"GTM.h"
#include	"CCC.h"
#include	"MES.h"
#include	"SVM.h"

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
// State machine for open-loop test
void STM_StandByOLT0(void)
{
	// Disable PWM.
	GTM_PWMDisable();

	// Clear failures if state is changed from Fault.
	if ((GeSTM_LastStateT0 == Fault) || (GeSTM_LastStateT0 == TurnOn) || (GeSTM_LastStateT0 == TurnOff))
	{
		// Reset SW faults
		STM_ResetT0Flt();
		STM_ResetT1Flt();
	}
	// If not, run T0 diagnostics.
	else
	{
		STM_DiagT0LAT();
	}

	// Disable PWM if there is any fault: may not need it in StandBy, but reserve this part for testing.
	if (GbSTM_FltT0 || GbSTM_FltT1)
	{
		GTM_PWMDisable();
	}

	// Triggering ADC queue 3 for Vabc reading
	vadc_trig_group0_q3();
	vadc_trig_group1_q3();

	// Reading ADC queue 0 & calculation
	ADC_ReadIabcVdc();
	MES_CalcIabcIdq();
	MES_CalcVdc();

	// Triggering ADC background scan for Temperature reading
	vadc_trig_group0_scan();
	vadc_trig_group1_scan();

	// Read ADC queue 3 & calculate
	ADC_ReadVabc();
	MES_CalcVabc();

	// Reset Open-loop test angle
	GfCCC_Thetar = 0.0;
}

void STM_RunOLT0(void)
{
	float LfSTM_ThetarTmp = 0.0;
	float LfSTM_ErrMagVoltCmdOpenLoop = 0.0;
	float LfSTM_MagVoltCmdOpenLoopSlewT1 = 0.0;
	float LfSTM_MagVoltCmdOpenLoopTmp = 0.0;

	// Reading ADC queue 0 & calculation
	ADC_ReadIabcVdc();
	MES_CalcIabcIdq();
	MES_CalcVdc();

	// Triggering ADC background scan for Temperature reading
	vadc_trig_group0_scan();
	vadc_trig_group1_scan();

	// Enable PWM if the state is changed from StandBy.
	if (GeSTM_LastStateT0 == StandBy)
	{
		GTM_PWMEnable();
	}

	// Run diagnostics.
	STM_DiagT0LAT();

	// Disable PWM if there is any fault.
	if (GbSTM_FltT0 || GbSTM_FltT1)
	{
		GTM_PWMDisable();
	}

	// Open-loop angle calculation
	LfSTM_ThetarTmp = GfCCC_Thetar + GfGTM_T0 * GfCCC_Wr;

	if (LfSTM_ThetarTmp > PI)
	{
		GfCCC_Thetar = LfSTM_ThetarTmp - TWO_PI;
	}
	else if (LfSTM_ThetarTmp < -PI)
	{
		GfCCC_Thetar = LfSTM_ThetarTmp + TWO_PI;
	}
	else
	{
		GfCCC_Thetar = LfSTM_ThetarTmp;
	}

	if (KeSTM_TestModeCmd == OpenLoopVoltage)
	{
		GfCCC_VdCmd = 0.0;

		// Frequency calculation in open-loop test mode
		LfSTM_MagVoltCmdOpenLoopSlewT1 = KfSTM_MagVoltSlewLimOpenLoop * GfGTM_T1;
		LfSTM_ErrMagVoltCmdOpenLoop = KfSTM_MagVoltCmdOpenLoop - GfCCC_VqCmd;

		if (LfSTM_ErrMagVoltCmdOpenLoop > LfSTM_MagVoltCmdOpenLoopSlewT1)
		{
			LfSTM_MagVoltCmdOpenLoopTmp = GfCCC_VqCmd + LfSTM_MagVoltCmdOpenLoopSlewT1;
		}
		else if (LfSTM_ErrMagVoltCmdOpenLoop < -LfSTM_MagVoltCmdOpenLoopSlewT1)
		{
			LfSTM_MagVoltCmdOpenLoopTmp = GfCCC_VqCmd - LfSTM_MagVoltCmdOpenLoopSlewT1;
		}
		else
		{
			LfSTM_MagVoltCmdOpenLoopTmp = GfCCC_VqCmd + LfSTM_ErrMagVoltCmdOpenLoop;
		}

		// Min. and Max. limit of frequency command
		if (LfSTM_MagVoltCmdOpenLoopTmp > KfSTM_MagVoltCmdOpenLoopMax)
		{
			GfCCC_VqCmd = KfSTM_MagVoltCmdOpenLoopMax;
		}
		else if (LfSTM_MagVoltCmdOpenLoopTmp < KfSTM_MagVoltCmdOpenLoopMin)
		{
			GfCCC_VqCmd = KfSTM_MagVoltCmdOpenLoopMin;
		}
		else
		{
			GfCCC_VqCmd = LfSTM_MagVoltCmdOpenLoopTmp;
		}

		SVM_VdqToVabc();
		SVM_SVPWM();
	}
	else if (KeSTM_TestModeCmd == ClosedLoopCurrent)
	{
		//CCC_FluxWeakening();
		CCC_CurCntr();
		SVM_VdqToVabc();
		SVM_SVPWM();
	}
	else if (KeSTM_TestModeCmd == DirectDutyCommand)
	{
		GsGTM_OutputPWM.dutyCycleA = KfSTM_DutyACmd;
		GsGTM_OutputPWM.dutyCycleB = KfSTM_DutyBCmd;
		GsGTM_OutputPWM.dutyCycleC = KfSTM_DutyCCmd;
	}
	else if (KeSTM_TestModeCmd == SensorlessControl)
	{
		GsGTM_OutputPWM.dutyCycleA = 0.5;
		GsGTM_OutputPWM.dutyCycleB = 0.5;
		GsGTM_OutputPWM.dutyCycleC = 0.5;
	}
	else
	{
		GsGTM_OutputPWM.dutyCycleA = 0.5;
		GsGTM_OutputPWM.dutyCycleB = 0.5;
		GsGTM_OutputPWM.dutyCycleC = 0.5;
	}

	GTM_UpdatePWMOutputs(GsGTM_OutputPWM, Gu16GTM_T0CNT);
}

void STM_StandByOLT1(void)
{
	// Read ADC background scan & calculate
	ADC_ReadT1Values();
	MES_CalcTemps();

	// Clear T1 failure if state is changed from Fault.
	if ((GeSTM_LastStateT1 == Fault) || (GeSTM_LastStateT1 == TurnOn) || (GeSTM_LastStateT1 == TurnOff))
	{
		STM_ResetT1Flt();
		GIO_EnblResetHWFaults();

		GbSTM_StatusFltReset = TRUE;
	}
	// If not, run T1 diagnostics.
	else
	{
		STM_DiagT1LAT();
		GIO_DisbResetHWFaults();

		GbSTM_StatusFltReset = FALSE;
	}

	/*
	if (KeSTM_TestModeCmd != SensorlessControl)
	{
		// Reset frequency command
		GfSTM_fCmdOpenLoop = 0.0;
		GfCCC_Wr = 0.0;
		GfCCC_Wrpm = 0.0;
	}
	*/

	// Calculate T0 period
	GTM_T0Calc();

	// Controller Re-initialization
	CCC_ReInitInvs();
	CCC_ReInitCurtCtrl();
	CCC_ReInitSpdCtrl();
	CCC_ReInitFlxEst();
	CCC_ReInitSpdEst();

	CCC_ResetCurCntr();

	// State transition
	if (GbSTM_StatusFltReset == FALSE)
	{
		if (GbSTM_FltT0 || GbSTM_FltT1)
		{
			GeSTM_NextStateT1 = Fault;
		}
		else if (KeSTM_ManStateCmd == Run)
		{
			GeSTM_NextStateT1 = Run;
		}
		else if (KeSTM_ManStateCmd == TurnOn)
		{
			GbMES_CurrentoffsetCalcDone = FALSE;
			GeSTM_NextStateT1 = TurnOn;
		}
		else if ((KeSTM_ManStateCmd == TurnOff) || (KeSTM_ManStateCmd == Off))
		{
			GeSTM_NextStateT1 = TurnOff;
		}
	}
	else
	{
		GeSTM_NextStateT1 = StandBy;
	}

	/*
	// State transition
	if (GbSTM_StatusFltReset == FALSE)
	{
		if (GbSTM_FltT0 || GbSTM_FltT1)
		{
			GeSTM_NextStateT1 = Fault;
		}
		else if (KeSTM_ManStateCmd == Run)
		{
			GeSTM_NextStateT1 = Run;
		}
	}
	else
	{
		GeSTM_NextStateT1 = StandBy;
	}
	*/
}

void STM_RunOLT1(void)
{
	float LfSTM_ErrfCmdOpenLoop = 0.0;
	float LfSTM_fCmdOpenLoopSlewT1 = 0.0;
	float LfSTM_fCmdOpenLoopTmp = 0.0;

	// Read ADC background scan & calculate
	ADC_ReadT1Values();
	MES_CalcTemps();

	// Run T1 diagnostics
	STM_DiagT1LAT();

	// Controller Re-initialization
	CCC_ReInitInvs();
	CCC_ReInitCurtCtrl();
	CCC_ReInitSpdCtrl();
	CCC_ReInitFlxEst();
	CCC_ReInitSpdEst();

	// dq current command in open-loop test mode
	GfCCC_IdCmd = KfSTM_IdCmdOpenLoop;
	GfCCC_IqCmd = KfSTM_IqCmdOpenLoop;

	// Frequency calculation in open-loop test mode
	LfSTM_fCmdOpenLoopSlewT1 = KfSTM_fCmdOpenLoopSlew * GfGTM_T1;
	LfSTM_ErrfCmdOpenLoop = KfSTM_fCmdOpenLoop - GfSTM_fCmdOpenLoop;

	if (LfSTM_ErrfCmdOpenLoop > LfSTM_fCmdOpenLoopSlewT1)
	{
		LfSTM_fCmdOpenLoopTmp = GfSTM_fCmdOpenLoop + LfSTM_fCmdOpenLoopSlewT1;
	}
	else if (LfSTM_ErrfCmdOpenLoop < -LfSTM_fCmdOpenLoopSlewT1)
	{
		LfSTM_fCmdOpenLoopTmp = GfSTM_fCmdOpenLoop - LfSTM_fCmdOpenLoopSlewT1;
	}
	else
	{
		LfSTM_fCmdOpenLoopTmp = GfSTM_fCmdOpenLoop + LfSTM_ErrfCmdOpenLoop;
	}

	// Min. and Max. limit of frequency command
	if (LfSTM_fCmdOpenLoopTmp > KfSTM_fCmdOpenLoopMax)
	{
		GfSTM_fCmdOpenLoop = KfSTM_fCmdOpenLoopMax;
	}
	else if (LfSTM_fCmdOpenLoopTmp < KfSTM_fCmdOpenLoopMin)
	{
		GfSTM_fCmdOpenLoop = KfSTM_fCmdOpenLoopMin;
	}
	else
	{
		GfSTM_fCmdOpenLoop = LfSTM_fCmdOpenLoopTmp;
	}

	// Speed calculation in open-loop test mode
	GfCCC_Wr = GfSTM_fCmdOpenLoop * HZ_TO_RADS;
	GfCCC_Wrpm = GfCCC_Wr * RADS_TO_RPM;

	// T0 Period calc
	GTM_T0Calc();

	// State transition
	if ((GbSTM_FltT0 || GbSTM_FltT1) || (KeSTM_ManStateCmd == Fault))
	{
		GeSTM_NextStateT1 = Fault;
	}
	else if ((KeSTM_ManStateCmd == StandBy) || (KeSTM_ManStateCmd == TurnOff) || (KeSTM_ManStateCmd == TurnOn) || (KeSTM_ManStateCmd == Off))
	{
		GeSTM_NextStateT1 = StandBy;
	}
	else
	{
		GeSTM_NextStateT1 = Run;
	}
}
