/*
 * CCC.c
 *
 *  Created on: Oct 17, 2018
 *      Author: H247670
 */
#include <DerateProtFact.h>
#include	"CCC.h"
#include	"GTM.h"
#include	"VADC.h"
#include	"MES.h"
#include	"STM.h"
#include 	"ThermProtn_Ecoe_2019a.h"

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/
// Motor resistor (Ohm)
float KfCCC_Rs = 0.00264;

// Motor inductance (H)
float KfCCC_Ld = 6.20e-6;
float KfCCC_Lq = 6.20e-6;

// Magnetic flux density from PM (V-s)
float KfCCC_LamPM = 3.0e-3;

// Moment of inertia (kg-m^2)
float KfCCC_Jm = 4.0e-5;

// Voltage usage pct
float KfCCC_MagVdqLimRatio = 1.0;

// Flux-weakening enabling speed (r/min)
float KfCCC_WrpmFW = 40000;

// Flux-weakening voltage command ratio
float KfCCC_FWCmdRatio = 0.5484827756;

// Flux weakening controller gain
float KfCCC_KiFW = 5000000.0;
float KfCCC_KpFW = 2.0;

// FW d current command max (A)
float KfCCC_FWIdMax = 130.0;

// Current controller bandwidth (Hz)
float KfCCC_fcCC = 1000.0;

// Speed controller bandwidth (Hz)
float KfCCC_fcSC = 2.0;

// Speed controller I gain coeff.
float KfCCC_KiFactorSC = 5.0;

// Speed estimator bandwidth (Hz)
float KfCCC_fc1SpdEst = 100.0;
float KfCCC_fc2SpdEst = 10.0;
float KfCCC_fc3SpdEst = 10.0;

// Speed estimator internal limits
float KfCCC_delAlMax = 10.0;
float KfCCC_delWrMax = 10.0;
float KfCCC_delThetarMax = PI_OVER_TWO;

// Flux estimator bandwidth (Hz)
float KfCCC_fcFlxEst = 1000.0;

// Flux estimator damping coeff.
float KfCCC_DampFlxEst = 0.707106781;

// Current maximum (Apk)
float KfCCC_IsMax = 300.0;

#if CUSTOMER_ID == JOHN_DEERE

// Power maximum - motoring (W)
float KfCCC_PeMotMax = 7800.0;
float KfCCC_PdcMaxMot = 7800.0;		// Max Motoring Power
float KfCCC_IdcMaxMot = 160.5;		// 9000W / 48V

// Power maximum - generating (W)
float KfCCC_PeGenMax = 7800.0;
float KfCCC_PdcMaxGen = 7800.0;
float KfCCC_IdcMaxGen = 160.0;

#elif CUSTOMER_ID == FORD

float KfCCC_PeMotMax = 11000.0;
float KfCCC_PdcMaxMot = 11000.0;	// Max Motoring Power
float KfCCC_IdcMaxMot = 230.0;		// 11000W / 48V

// Power maximum - generating (W)
float KfCCC_PeGenMax = 11000.0;
float KfCCC_PdcMaxGen = 11000.0;
float KfCCC_IdcMaxGen = 230.0;

#elif CUSTOMER_ID == AUDI

// Power maximum - motoring (W)
float KfCCC_PeMotMax = 9000.0;
float KfCCC_PdcMaxMot = 9000.0;		// Max Motoring Power
float KfCCC_IdcMaxMot = 187.5;		// 9000W / 48V

// Power maximum - generating (W)
float KfCCC_PeGenMax = 0.0;
float KfCCC_PdcMaxGen = 0.0;
float KfCCC_IdcMaxGen = 0.0;

#endif /* CUSTOMER */


// Inverse of system efficiency
float KfCCC_SysEff = 1.0;

// CAN Command
float KfCCC_WrpmCmd = 0.0;
float KfCCC_WrpmCmdSlewDn = 500000.0;
float KfCCC_WrpmCmdSlewUp = 500000.0;

float KfCCC_TrqCmdSlewDn = 2000.0;
float KfCCC_TrqCmdSlewUp = 2000.0;

// Temporary variable for de-rating
float KfCCC_DerateMot = 1.0;

// Derating calibrations
uint16 Ku16CCC_DerateCNTSet01 = 2000;
uint16 Ku16CCC_DerateCNTSet02 = 2001;
uint16 Ku16CCC_DerateCNTSet03 = 4000;

float KfCCC_DeratePwrSet01 = 1.0;
float KfCCC_DeratePwrSet02 = 1.0;

float KfCCC_DerateIsSet = 150.0;

// Derating - based on temperature
float KfCCC_DerateEnblTemp = 10000.0;
float KfCCC_DerateTempCmd = 10000.0;

float KfCCC_KpDerateTemp = 0.0;
float KfCCC_KiDerateTemp = 0.0;

// Manual torque command
float KfCCC_TeCmd = 0.0;

// Speed command to torque command conversion
//float KfCCC_TeCmdScale = 0.00073278;
//float KfCCC_TeCmdOffset = -1.5;

float KfCCC_TeCmdScale = 0.001;
float KfCCC_TeCmdOffset = -2.047;

float KfCCC_TeCmdSlewScale = 5.0;
float KfCCC_TeCmdSlewOffset = 0.0;

// Temperature estimation - imaginary
float KfCCC_WcTempEst = 1.0;
float KfCCC_KcTempEst = 5.477225575;

// Derating - Thermal estimation (CoV)
float KfCCC_DerateThermEstSet = 112.0/300.0; // Max current: 300Apk, Derating current: 112Apk

// Derating - input flag
boolean KbCCC_DerateThermInput = TRUE;	// 0 - CAN Water Temp, 1 - TempSens2

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
// Temporary variables
// motor speed
float GfCCC_Wr = 0.0;
float GfCCC_Wrpm = 0.0;
float GfCCC_WrCmdSlew = 0.0;
float GfCCC_WrpmCmdSlew = 0.0;

// Inverse of motor speed
float GfCCC_InvWr = 0.0;

// Motor angle
float GfCCC_Thetar = 0.0;

// Inverse of LamPM
float GfCCC_InvLamPM = 6.004;

// dq current command (Consumed in T0)
float GfCCC_IdCmd = 0.0;
float GfCCC_IqCmd = 0.0;

// dq current command (Created in T1)
float GfCCC_IdCmdT1 = 0.0;
float GfCCC_IqCmdT1 = 0.0;


// dq voltage command
float GfCCC_VdCmd = 0.0;
float GfCCC_VqCmd = 0.0;

// dq voltage command (not limited)
float GfCCC_VdCmdNoLim = 0.0;
float GfCCC_VqCmdNoLim = 0.0;

// dq voltage command feed-forward (not limited)
float GfCCC_VdCmdFFNoLim = 0.0;
float GfCCC_VqCmdFFNoLim = 0.0;

// dq voltage command feed-back (not limited)
float GfCCC_VdCmdFBNoLim = 0.0;
float GfCCC_VqCmdFBNoLim = 0.0;

// I term in current controller
float GfCCC_IntegErrId = 0.0;
float GfCCC_IntegErrIq = 0.0;

// dq current error
float GfCCC_ErrId = 0.0;
float GfCCC_ErrIq = 0.0;

// Current controller bandwidth
float GfCCC_WcCC = 8482.300165;

// Kp gain
float GfCCC_KpdCC = 0.0848;
float GfCCC_KpqCC = 0.0848;

// Ki gain
float GfCCC_KidCC = 59.376;
float GfCCC_KiqCC = 59.376;

// Ka gain
float GfCCC_KadCC = 11.789;
float GfCCC_KaqCC = 11.789;

// Lamd, Lamq
float GfCCC_Lamd = 0.0;
float GfCCC_Lamq = 0.0;

// Voltage magnitude (not limited)
float GfCCC_MagVdqCmdNoLim = 0.0;

// Voltage magnitude limit
float GfCCC_MagVdqLim = 0.0;

// Voltage command for flux-weakening
float GfCCC_FWCmd = 0.0;

// Integrator in FW
float GfCCC_IntegErrFW = 0.0;

// FW output (not limited)
float GfCCC_deltaINoLim = 0.0;

// FW output (limited)
float GfCCC_deltaI = 0.0;

// dq current command temp.
float GfCCC_IdCmdPre = 0.0;
float GfCCC_IqCmdPre = 0.0;

// dq current command magnitude
float GfCCC_MagIdqCmd = 0.0;

// Te command (not limited)
float GfCCC_TeCmdPre = 0.0;

// Te command (limited)
float GfCCC_TeCmd = 0.0;

// Te Command CAN
float GfCCC_TeCmdCAN = 0.0;

// Te Command SlewUP CAN
float GfCCC_TeCmdSlewUPCAN = 1000.0;

// Te Command SlewDN CAN
float GfCCC_TeCmdSlewDNCAN = 1000.0;

// Pdc estimation
float GfCCC_PdcEst = 0.0;

// Idc estimation
float GfCCC_IdcEst = 0.0;

// Derate to Power Stage Temperature
float GfCCC_DeratePwr = 1.0;

// Derating activation flag
boolean GbCCC_DerateActv = FALSE;

// Derating timer counter
uint16 Gu16CCC_DerateCNT = 0;

// Derate to Motor Temperature
float GfCCC_DerateMot = 1.0;

// Final derate value
float GfCCC_DerateMin = 1.0;

// Maximum current limit
float GfCCC_IsMax = 0.0;

// Iq cmd max
float GfCCC_IqCmdMax = 0.0;

// Iq cmd min
float GfCCC_IqCmdMin = 0.0;

// Iq cmd max (from Pe limit)
float GfCCC_IqCmdPeMax = 0.0;

// Iq cmd min (from Pe limit)
float GfCCC_IqCmdPeMin = 0.0;

// Iq cmd max (from CAN limit)
float GfCCC_IqCmdPdcCANMax = 0.0;

// Iq cmd min (from CAN limit)
float GfCCC_IqCmdPdcCANMin = 0.0;

// SC Integrator
float GfCCC_IntegErrWr = 0.0;

// SC gains
float GfCCC_WcSC = 12.56637061;

float GfCCC_KaSC = 1454.798383;
float GfCCC_KpSC = 6.87E-04;
float GfCCC_KiSC = 3.44E-04;

// Inverse of Ld Lq
float GfCCC_InvLd = 1e5;
float GfCCC_InvLq = 1e5;

// Flux estimator - states
float GfCCC_IdEstNew = 0.0;
float GfCCC_IqEstNew = 0.0;
float GfCCC_IdEst = 0.0;
float GfCCC_IqEst = 0.0;

float GfCCC_EdEstNew = 0.0;
float GfCCC_EqEstNew = 0.0;
float GfCCC_EdEst = 0.0;
float GfCCC_EqEst = 0.0;

// Flux estimator - gains
float GfCCC_WcFlxEst = 9424.777961;

float GfCCC_K11FlxEst = 12628.0;
float GfCCC_K12FlxEst = 0.0;
float GfCCC_K21FlxEst = 0.0;
float GfCCC_K22FlxEst = 12628.0;
float GfCCC_K31FlxEst = 888.0;
float GfCCC_K42FlxEst = -888.0;

// Speed estimator - states
float GfCCC_AlEstNew = 0.0;
float GfCCC_AlEst = 0.0;
float GfCCC_WrEstNew = 0.0;
float GfCCC_WrEst = 0.0;
float GfCCC_ThetarEstNew = 0.0;
float GfCCC_ThetarEst = 0.0;

// Angle error
float GfCCC_ErrThetar = 0.0;

// Speed estimator - gains
float GfCCC_Wc1SpdEst = 628.3185307;
float GfCCC_Wc2SpdEst = 62.83185307;
float GfCCC_Wc3SpdEst = 62.83185307;

float GfCCC_K1SpdEst = 0.0;
float GfCCC_K2SpdEst = 0.0;
float GfCCC_K3SpdEst = 0.0;

// Inverse of inertia
float GfCCC_InvJm = 1.83E+04;

// CAN variables
float GfCCC_WrpmCmd = 0.0;
float GfCCC_WrpmCmdCAN = 0.0;
float GfCCC_IsMaxGenCAN = 0.0;
float GfCCC_IsMaxMotCAN = 0.0;
float GfCCC_WrpmCmdSlewDnCAN = 0.0;
float GfCCC_WrpmCmdSlewUpCAN = 0.0;
float GfCCC_TrqCmdSlewDnCAN = 0.0;
float GfCCC_TrqCmdSlewUpCAN = 0.0;

float GfCCC_IdcMinCAN = 0.0;
float GfCCC_PdcMinCAN = 0.0;
float GfCCC_IdcMaxCAN = 0.0;
float GfCCC_PdcMaxCAN = 0.0;

// Temperature estimation - imaginary
float GfCCC_InvTempEst = 0.0;

// Derating - based on temperature
float GfCCC_IntegErrTemp = 0.0;
float GfCCC_delIsDerate = 0.0;

float GfCCC_DerateTemp = 1.0;

// Derating - Thermal Estimator (CoV)
boolean GbCCC_DerateThermEstActv = FALSE;
float GfCCC_DerateThermEst = 1.0;
float GfCCC_InvTempEst02 = 0.0;



/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

// Reset dq current command
void CCC_ResetCurCmd(void)
{
	GfCCC_IdCmd = 0.0;
	GfCCC_IqCmd = 0.0;
	GfCCC_IdCmdPre = 0.0;
	GfCCC_IqCmdPre = 0.0;
}

void CCC_CurCntr(void)
{
	// Voltage error
	float LfCCC_ErrVd = 0.0, LfCCC_ErrVq = 0.0;

	// Tmp variable to calculate voltage limit
	float LfCCC_MagVdqRatio = 0.0;

	GfCCC_ErrId = GfCCC_IdCmd - GfMES_Idr;
	GfCCC_ErrIq = GfCCC_IqCmd - GfMES_Iqr;

	LfCCC_ErrVd = GfCCC_VdCmdNoLim - GfCCC_VdCmd;
	LfCCC_ErrVq = GfCCC_VqCmdNoLim - GfCCC_VqCmd;

	GfCCC_IntegErrId = GfCCC_IntegErrId + GfGTM_T0 * (GfCCC_ErrId - GfCCC_KadCC * LfCCC_ErrVd);
	GfCCC_IntegErrIq = GfCCC_IntegErrIq + GfGTM_T0 * (GfCCC_ErrIq - GfCCC_KaqCC * LfCCC_ErrVq);

	GfCCC_Lamd = KfCCC_Ld * GfMES_Idr + KfCCC_LamPM;
	GfCCC_Lamq = KfCCC_Lq * GfMES_Iqr;

	GfCCC_VdCmdFFNoLim = -GfCCC_Wr * GfCCC_Lamq;
	GfCCC_VqCmdFFNoLim = GfCCC_Wr * GfCCC_Lamd;

	GfCCC_VdCmdFBNoLim = GfCCC_KpdCC * GfCCC_ErrId + GfCCC_KidCC * GfCCC_IntegErrId;
	GfCCC_VqCmdFBNoLim = GfCCC_KpqCC * GfCCC_ErrIq + GfCCC_KiqCC * GfCCC_IntegErrIq;

	GfCCC_VdCmdNoLim = GfCCC_VdCmdFBNoLim + GfCCC_VdCmdFFNoLim;
	GfCCC_VqCmdNoLim = GfCCC_VqCmdFBNoLim + GfCCC_VqCmdFFNoLim;

	GfCCC_MagVdqCmdNoLim = sqrtf(GfCCC_VdCmdNoLim * GfCCC_VdCmdNoLim + GfCCC_VqCmdNoLim * GfCCC_VqCmdNoLim);

	GfCCC_MagVdqLim = ONE_OVER_SQRT_THREE * KfCCC_MagVdqLimRatio * GfMES_Vdc;			// limit = vdc / SQRT(3)

	if (GfCCC_MagVdqCmdNoLim < MIN_DIVISION)							// avoid divide by zero when GfCCC_MagVdqCmdNoLim is near zero
	{
		LfCCC_MagVdqRatio = 1;
	}
	else
	{
		LfCCC_MagVdqRatio = GfCCC_MagVdqLim / GfCCC_MagVdqCmdNoLim;		// if <1 the voltage vector is too large and needs limiting
	}

	if (LfCCC_MagVdqRatio > 1.0)
	{
		LfCCC_MagVdqRatio = 1.0;		// saturate to 1.0
	}
	else
	{
		GfCCC_MagVdqCmdNoLim = GfCCC_MagVdqLim;
	}

	GfCCC_VdCmd = GfCCC_VdCmdNoLim * LfCCC_MagVdqRatio;
	GfCCC_VqCmd = GfCCC_VqCmdNoLim * LfCCC_MagVdqRatio;

	GfMES_MagVdqFilt = GfCCC_MagVdqCmdNoLim;
}

// Reset current controller
void CCC_ResetCurCntr(void)
{
	GfCCC_VdCmdNoLim = 0.0;
	GfCCC_VqCmdNoLim = 0.0;

	GfCCC_VdCmd = 0.0;
	GfCCC_VqCmd = 0.0;

	GfCCC_IntegErrId = 0.0;
	GfCCC_IntegErrIq = 0.0;

	GfCCC_Lamd = KfCCC_LamPM;
	GfCCC_Lamq = 0.0;

	GfCCC_VdCmdFFNoLim = 0.0;
	GfCCC_VqCmdFFNoLim = 0.0;

	GfCCC_VdCmdFBNoLim = 0.0;
	GfCCC_VqCmdFBNoLim = 0.0;

	GfCCC_MagVdqCmdNoLim = 0.0;
	GfCCC_MagVdqLim = 0.0;
}

// Set current controller. (if speed is not zero)
void CCC_SetCurCntr(void)
{
	float LfCCC_WrLamPM;

	LfCCC_WrLamPM = GfCCC_Wr * KfCCC_LamPM;

	GfCCC_VdCmdNoLim = 0.0;
	GfCCC_VqCmdNoLim = LfCCC_WrLamPM;

	GfCCC_VdCmd = 0.0;
	GfCCC_VqCmd = LfCCC_WrLamPM;

	GfCCC_IntegErrId = 0.0;
	GfCCC_IntegErrIq = 0.0;

	GfCCC_Lamd = KfCCC_LamPM;
	GfCCC_Lamq = 0.0;

	GfCCC_VdCmdFFNoLim = LfCCC_WrLamPM;
	GfCCC_VqCmdFFNoLim = 0.0;

	GfCCC_VdCmdFBNoLim = 0.0;
	GfCCC_VqCmdFBNoLim = 0.0;

	GfCCC_MagVdqCmdNoLim = LfCCC_WrLamPM;
	GfCCC_MagVdqLim = LfCCC_WrLamPM;
}


// Flux-weakening controller
void CCC_FluxWeakening(void)
{
	// FW error calculation
	float LfCCC_ErrFW = 0.0;

	// Flux-weakening controller
	if (GfCCC_Wrpm > KfCCC_WrpmFW)
	{
		GfCCC_FWCmd = GfMES_Vdc * KfCCC_FWCmdRatio;

		LfCCC_ErrFW = -(GfCCC_FWCmd - GfCCC_MagVdqCmdNoLim) * GfCCC_InvWr;

		GfCCC_IntegErrFW = GfCCC_IntegErrFW + KfCCC_KiFW * GfGTM_T0 * LfCCC_ErrFW;

		GfCCC_deltaINoLim = KfCCC_KpFW * LfCCC_ErrFW + GfCCC_IntegErrFW;

		if (GfCCC_deltaINoLim > GfCCC_IsMax)
		{
			GfCCC_deltaI = GfCCC_IsMax;
			GfCCC_IntegErrFW = GfCCC_IsMax - KfCCC_KpFW * LfCCC_ErrFW;
		}
		else
		{
			GfCCC_deltaI = GfCCC_deltaINoLim;
		}

		GfCCC_IdCmdPre = -GfCCC_deltaI;

		GfCCC_MagIdqCmd = sqrtf(GfCCC_IdCmdPre * GfCCC_IdCmdPre + GfCCC_IqCmdPre * GfCCC_IqCmdPre);

		if (GfCCC_deltaI < 0.0)
		{
			GfCCC_deltaI = 0.0;
			GfCCC_IntegErrFW = -KfCCC_KpFW * LfCCC_ErrFW;

			GfCCC_IdCmd = 0.0;
			GfCCC_IqCmd = GfCCC_IqCmdPre;
		}
		else if (GfCCC_deltaI < KfCCC_FWIdMax)
		{
			if (GfCCC_MagIdqCmd > GfCCC_IsMax)
			{
				GfCCC_IdCmd = GfCCC_IdCmdPre;

				if (GfCCC_IqCmdPre > 0)
				{
					GfCCC_IqCmd = sqrtf(GfCCC_IsMax * GfCCC_IsMax - GfCCC_IdCmd * GfCCC_IdCmd);
				}
				else
				{
					GfCCC_IqCmd = -sqrtf(GfCCC_IsMax * GfCCC_IsMax - GfCCC_IdCmd * GfCCC_IdCmd);
				}
			}
			else
			{
				GfCCC_IdCmd = GfCCC_IdCmdPre;
				GfCCC_IqCmd = GfCCC_IqCmdPre;
			}
		}
		else
		{
			GfCCC_IdCmd = -KfCCC_FWIdMax;

			if (GfCCC_IqCmdPre > 0)
			{
				GfCCC_IqCmd = GfCCC_IqCmdPre - (GfCCC_deltaI - KfCCC_FWIdMax);
			}
			else
			{
				GfCCC_IqCmd = -GfCCC_IqCmdPre + (GfCCC_deltaI - KfCCC_FWIdMax);
			}
		}
	}
	else
	{
		CCC_ResetFW();

		GfCCC_IdCmd = 0.0;
		GfCCC_IqCmd = GfCCC_IqCmdPre;
	}

	GfCCC_TeCmd = THREE_OVER_TWO * KfCCC_LamPM * GfCCC_IqCmd;

	GfCCC_PdcEst = GfCCC_TeCmd * GfCCC_Wr;
	GfCCC_IdcEst = GfCCC_PdcEst * GfMES_InvVdc;
}

// Reset FW
void CCC_ResetFW(void)
{
	GfCCC_FWCmd = 0.0;
	GfCCC_IntegErrFW = 0.0;
	GfCCC_deltaINoLim = 0.0;
	GfCCC_deltaI = 0.0;
	GfCCC_IdCmdPre = 0.0;
	GfCCC_MagIdqCmd = 0.0;

	GfCCC_PdcEst = 0.0;
	GfCCC_IdcEst = 0.0;
}

// Speed controller
void CCC_SpdCntr(void)
{
	// Speed error
	float LfCCC_ErrWr = 0.0;

	// Torque error
	float LfCCC_ErrTeCmd = 0.0;

	// Iq command (temp)
	float LfCCC_IqCmdPreTmp = 0.0;

	LfCCC_ErrWr = GfCCC_WrCmdSlew - GfCCC_Wr;

	LfCCC_ErrTeCmd = GfCCC_TeCmdPre - GfCCC_TeCmd;

	GfCCC_IntegErrWr = GfCCC_IntegErrWr + GfGTM_T1 * (LfCCC_ErrWr - GfCCC_KaSC * LfCCC_ErrTeCmd);
	GfCCC_TeCmdPre = GfCCC_KpSC * LfCCC_ErrWr + GfCCC_KiSC * GfCCC_IntegErrWr;

	LfCCC_IqCmdPreTmp = TWO_OVER_THREE * GfCCC_InvLamPM * GfCCC_TeCmdPre;

	if (LfCCC_IqCmdPreTmp > GfCCC_IqCmdMax)
	{
		GfCCC_IqCmdPre = GfCCC_IqCmdMax;
	}
	else if (LfCCC_IqCmdPreTmp < GfCCC_IqCmdMin)
	{
		GfCCC_IqCmdPre = GfCCC_IqCmdMin;
	}
	else
	{
		GfCCC_IqCmdPre = LfCCC_IqCmdPreTmp;
	}
}

// Speed controller reset
void CCC_ResetSpdCntr(void)
{
	GfCCC_TeCmdPre = 0.0;
	GfCCC_TeCmd = 0.0;

	GfCCC_IntegErrWr = 0.0;
}

// Current controller re-initialization
void CCC_ReInitCurtCtrl(void)
{
	GfCCC_WcCC = TWO_PI * KfCCC_fcCC;
	GfCCC_KpdCC = KfCCC_Ld * GfCCC_WcCC;
	GfCCC_KidCC = KfCCC_Rs * GfCCC_WcCC;
	GfCCC_KadCC = 1.0 / GfCCC_KpdCC;
	GfCCC_KpqCC = KfCCC_Lq * GfCCC_WcCC;
	GfCCC_KiqCC = KfCCC_Rs * GfCCC_WcCC;
	GfCCC_KaqCC = 1.0 / GfCCC_KpqCC;
}

// Speed Controller re-initialization
void CCC_ReInitSpdCtrl(void)
{
	GfCCC_WcSC = TWO_PI * KfCCC_fcSC;
	GfCCC_KpSC = KfCCC_Jm * GfCCC_WcSC;
	GfCCC_KiSC = GfCCC_KpSC * KfCCC_KiFactorSC;

	GfCCC_KaSC = 1.0 / GfCCC_KpSC;
}

// Inverse value calculation
void CCC_ReInitInvs(void)
{
	GfCCC_InvLamPM = 1.0 / KfCCC_LamPM;
	GfCCC_InvLd = 1.0 / KfCCC_Ld;
	GfCCC_InvLq = 1.0 / KfCCC_Lq;
	GfCCC_InvJm = 1.0 / KfCCC_Jm;
}

// Power/Torque/Current Limit Function
void CCC_IdqCmdLim(void)
{
	float LfCCC_IqCmdMaxTmp = 0.0;
	float LfCCC_IqCmdMinTmp = 0.0;

	float LfCCC_PdcMaxCalc = 0.0;
	float LfCCC_PdcMinCalc = 0.0;

	float LfCCC_PdcMaxCAN = 0.0;
	float LfCCC_PdcMinCAN = 0.0;

	// Maximum current limit
	GfCCC_IsMax = KfCCC_IsMax * GfCCC_DerateMin;
	//GfCCC_IsMax = GfCCC_IsMaxMotCAN * GfCCC_DerateMin;

	// Iq cmd limit from Power Limit
	GfCCC_IqCmdPeMax = KfCCC_PeMotMax * GfCCC_InvWr * TWO_OVER_THREE * GfCCC_InvLamPM;
	GfCCC_IqCmdPeMin = -KfCCC_PeGenMax * GfCCC_InvWr * TWO_OVER_THREE * GfCCC_InvLamPM;

	// Input power limit from CAN
	LfCCC_PdcMaxCalc = GfCCC_IdcMaxCAN * GfMES_Vdc;
	LfCCC_PdcMinCalc = GfCCC_IdcMinCAN * GfMES_Vdc;

	if (LfCCC_PdcMaxCalc > GfCCC_PdcMaxCAN)
	{
		LfCCC_PdcMaxCAN = GfCCC_PdcMaxCAN;
	}
	else
	{
		LfCCC_PdcMaxCAN = LfCCC_PdcMaxCalc;

	}

	if (LfCCC_PdcMinCalc > GfCCC_PdcMinCAN)
	{
		LfCCC_PdcMinCAN = LfCCC_PdcMinCalc;
	}
	else
	{
		LfCCC_PdcMinCAN = GfCCC_PdcMinCAN;
	}

	GfCCC_IqCmdPdcCANMax = LfCCC_PdcMaxCAN * GfCCC_InvWr * KfCCC_SysEff * TWO_OVER_THREE * GfCCC_InvLamPM;
	GfCCC_IqCmdPdcCANMin = LfCCC_PdcMinCAN * GfCCC_InvWr * KfCCC_SysEff * TWO_OVER_THREE * GfCCC_InvLamPM;

	// Arbitration
	if (GfCCC_IqCmdPeMax > GfCCC_IsMax)
	{
		if (GfCCC_IqCmdPdcCANMax > GfCCC_IsMax)
		{
			LfCCC_IqCmdMaxTmp = GfCCC_IsMax;
		}
		else
		{
			LfCCC_IqCmdMaxTmp = GfCCC_IqCmdPdcCANMax;
		}
	}
	else
	{
		if (GfCCC_IqCmdPdcCANMax > GfCCC_IqCmdPeMax)
		{
			LfCCC_IqCmdMaxTmp = GfCCC_IqCmdPeMax;
		}
		else
		{
			LfCCC_IqCmdMaxTmp = GfCCC_IqCmdPdcCANMax;
		}
	}

	if (GfCCC_IqCmdPeMin > -GfCCC_IsMax)
	{
		if (GfCCC_IqCmdPdcCANMin > GfCCC_IqCmdPeMin)
		{
			LfCCC_IqCmdMinTmp = GfCCC_IqCmdPdcCANMin;
		}
		else
		{
			LfCCC_IqCmdMinTmp = GfCCC_IqCmdPeMin;
		}
	}
	else
	{
		if (GfCCC_IqCmdPdcCANMin > -GfCCC_IsMax)
		{
			LfCCC_IqCmdMinTmp = GfCCC_IqCmdPdcCANMin;
		}
		else
		{
			LfCCC_IqCmdMinTmp = -GfCCC_IsMax;
		}
	}

	GfCCC_IqCmdMax = LfCCC_IqCmdMaxTmp;
	GfCCC_IqCmdMin = LfCCC_IqCmdMinTmp;
}

// Slew rate limit
float CCC_SlewLimit(float LfCCC_Input, float LfCCC_Cmd, float LfCCC_SlewUpLim, float LfCCC_SlewDnLim, float LfCCC_Tsamp)
{
	float LfCCC_Output = 0.0;
	float LfCCC_Err = 0.0;
	float LfCCC_SlewTsampUp = 0.0;
	float LfCCC_SlewTsampDn = 0.0;

	LfCCC_SlewTsampUp = LfCCC_SlewUpLim * LfCCC_Tsamp;
	LfCCC_SlewTsampDn = LfCCC_SlewDnLim * LfCCC_Tsamp;
	LfCCC_Err = LfCCC_Cmd - LfCCC_Input;

	if (LfCCC_Err > LfCCC_SlewTsampUp)
	{
		LfCCC_Output = LfCCC_Input + LfCCC_SlewTsampUp;
	}
	else if (LfCCC_Err < -LfCCC_SlewTsampDn)
	{
		LfCCC_Output = LfCCC_Input - LfCCC_SlewTsampDn;
	}
	else
	{
		LfCCC_Output = LfCCC_Input + LfCCC_Err;
	}

	return LfCCC_Output;
}

// De-rating calculation
void CCC_Derating(void)
{
	float LfCCC_DerateSlope = 0.0;
	float LfCCC_TempDiff = 0.0;
	float LfCCC_IsCmdSqr = 0.0;
	float LfCCC_ErrTemp = 0.0;

	DPF_TempIMS_ThermalDerating();
	DPF_TempBrdThermalDerating();
	DPF_VoltageDerating();
	DPF_ExtCoolantTempDerating();
	DPF_InletTempDerating();

	ThermProtnEcoeDerating();

	// Derating to motor capacity is now disabled.
	GfCCC_DerateMot = KfCCC_DerateMot;

	// Derating to power stage capacity - implemented in the simplest way.
	if (GbCCC_DerateActv == TRUE)
	{
		if (Gu16CCC_DerateCNT >= Ku16CCC_DerateCNTSet03)
		{
			Gu16CCC_DerateCNT = 0;
			GbCCC_DerateActv = FALSE;
			GfCCC_DeratePwr = KfCCC_DeratePwrSet02;
		}
		else if (Gu16CCC_DerateCNT >= Ku16CCC_DerateCNTSet02)
		{
			Gu16CCC_DerateCNT = Gu16CCC_DerateCNT + 1;
			GbCCC_DerateActv = TRUE;
			GfCCC_DeratePwr = KfCCC_DeratePwrSet02;
		}
		else if (Gu16CCC_DerateCNT >= Ku16CCC_DerateCNTSet01)
		{
			Gu16CCC_DerateCNT = Gu16CCC_DerateCNT + 1;
			GbCCC_DerateActv = TRUE;
			LfCCC_DerateSlope = (KfCCC_DeratePwrSet01 - 1.0) / ((float)Ku16CCC_DerateCNTSet02 - (float)Ku16CCC_DerateCNTSet01);
			GfCCC_DeratePwr = 1.0 + LfCCC_DerateSlope * ((float)Gu16CCC_DerateCNT - (float)Ku16CCC_DerateCNTSet01);
		}
		else
		{
			Gu16CCC_DerateCNT = Gu16CCC_DerateCNT + 1;
			GbCCC_DerateActv = TRUE;
			GfCCC_DeratePwr = 1.0;
		}
	}
	else
	{
		if (GfMES_MagIdqr > KfCCC_DerateIsSet)
		{
			GbCCC_DerateActv = TRUE;
		}
		else
		{
			GbCCC_DerateActv = FALSE;
		}
		Gu16CCC_DerateCNT = 0;
		GfCCC_DeratePwr = 1.0;
	}

	if (GbCCC_DerateThermEstActv == TRUE)
	{
		GfCCC_DerateThermEst = KfCCC_DerateThermEstSet;
	}
	else
	{
		GfCCC_DerateThermEst = 1.0;
	}

	// Temperature estimation - imaginary
	LfCCC_IsCmdSqr = sqrtf(sqrtf(GfCCC_IdCmd * GfCCC_IdCmd + GfCCC_IqCmd * GfCCC_IqCmd));

    LfCCC_TempDiff = KfCCC_WcTempEst * (KfCCC_KcTempEst * LfCCC_IsCmdSqr - GfCCC_InvTempEst);
    GfCCC_InvTempEst = GfCCC_InvTempEst + GfGTM_T1 * LfCCC_TempDiff;

    if (GfCCC_InvTempEst > KfCCC_DerateEnblTemp)
    {
        LfCCC_ErrTemp = KfCCC_DerateTempCmd - GfCCC_InvTempEst;

        GfCCC_IntegErrTemp = GfCCC_IntegErrTemp + GfGTM_T1 * LfCCC_ErrTemp;

        GfCCC_delIsDerate = KfCCC_KpDerateTemp * LfCCC_ErrTemp + KfCCC_KiDerateTemp * GfCCC_IntegErrTemp;

        if (GfCCC_delIsDerate > 0.0)
        {
            GfCCC_delIsDerate = 0.0;

            //GfCCC_IntegErrTemp = -KfCCC_KpDerateTemp*LfCCC_ErrTemp / KfCCC_KiDerateTemp;
            GfCCC_IntegErrTemp = 0.0;		//set to 0.0 to prevent division by zero (KfCCC_KiDerateTemp is set to 0)
        }
        else if (GfCCC_delIsDerate < -1.0)
        {
            GfCCC_delIsDerate = -1.0;

            //GfCCC_IntegErrTemp = -1.0 - KfCCC_KpDerateTemp * LfCCC_ErrTemp / KfCCC_KiDerateTemp;
            GfCCC_IntegErrTemp = 0.0;		//set to 0.0 to prevent division by zero (KfCCC_KiDerateTemp is set to 0)
        }
    }
    else
    {
//        LfCCC_ErrTemp = KfCCC_DerateTempCmd - GfCCC_InvTempEst;
        GfCCC_delIsDerate = 0.0;

        //GfCCC_IntegErrTemp = -KfCCC_KpDerateTemp * LfCCC_ErrTemp / KfCCC_KiDerateTemp;
        GfCCC_IntegErrTemp = 0.0;		//set to 0.0 to prevent division by zero (KfCCC_KiDerateTemp is set to 0)
    }

    GfCCC_DerateTemp = 1.0 + GfCCC_delIsDerate;

    /*
	// Derating arbitration
	if (GfCCC_DerateTemp > GfCCC_DeratePwr)
	{
		GfCCC_DerateMin = GfCCC_DeratePwr;
	}
	else
	{
		GfCCC_DerateMin = GfCCC_DerateTemp;
	}
	*/

	// Derating arbitration (Thermal estimator - CoV)
	if (GfCCC_DerateTemp > GfCCC_DerateThermEst)
	{
		GfCCC_DerateMin = GfCCC_DerateThermEst;
	}
	else
	{
		GfCCC_DerateMin = GfCCC_DerateTemp;
	}

    /*
	// Derating arbitration
	if (GfCCC_DerateMot > GfCCC_DerateTemp)
	{
		GfCCC_DerateMin = GfCCC_DerateTemp;
	}
	else
	{
		GfCCC_DerateMin = GfCCC_DerateMot;
	}
	*/

	/* High IMS Temp Derating */
	if (GfCCC_DerateMin > GfDPF_HighTempIMS_Derate)
	{
		GfCCC_DerateMin = GfDPF_HighTempIMS_Derate;
	}

	/* Low IMS Temp Derating */
	if (GfCCC_DerateMin > GfDPF_LowTempIMS_Derate)
	{
		GfCCC_DerateMin = GfDPF_LowTempIMS_Derate;
	}

	/* Low Board Temp Derating */
	if (GfCCC_DerateMin > GfDPF_LowTempBrdDerate)
	{
		GfCCC_DerateMin = GfDPF_LowTempBrdDerate;
	}

	/* Low and High Coolant Temp Derating */
	if (GfCCC_DerateMin > GfDPF_ExtCoolHighTempDerate)
	{
		GfCCC_DerateMin = GfDPF_ExtCoolHighTempDerate;
	}
	if (GfCCC_DerateMin > GfDPF_ExtCoolLowTempDerate)
	{
		GfCCC_DerateMin = GfDPF_ExtCoolLowTempDerate;
	}

	/* Inlet Temp Derating */
	if (GfCCC_DerateMin > GfDPF_InletTempDerate)
	{
		GfCCC_DerateMin = GfDPF_InletTempDerate;
	}

	/* 12V High Derating */
	if (GfCCC_DerateMin > GfDPF_High12vDerate)
	{
		GfCCC_DerateMin = GfDPF_High12vDerate;
	}

	/* 12V Low Derating */
	if (GfCCC_DerateMin > GfDPF_Low12vDerate)
	{
		GfCCC_DerateMin = GfDPF_Low12vDerate;
	}

	/* 48V High Derating */
	if (GfCCC_DerateMin > GfDPF_High48vDerate)
	{
		GfCCC_DerateMin = GfDPF_High48vDerate;
	}

	/* 48V Low Derating */
	if (GfCCC_DerateMin > GfDPF_Low48vDerate)
	{
		GfCCC_DerateMin = GfDPF_Low48vDerate;
	}

}


// Reset de-rating calculation
void CCC_ResetDerating(void)
{
	GfCCC_DerateMot = 1.0;
	GfCCC_DeratePwr = 1.0;
	GfCCC_DerateTemp = 1.0;

	GfCCC_DerateMin = 1.0;
	Gu16CCC_DerateCNT = 0;
	GbCCC_DerateActv = FALSE;

	GfDPF_HighTempIMS_Derate = 1.0;
	GbDPF_HighTempIMS_DerateAct = FALSE;
	Gu8DPF_HighTempDerateCntrSet = 0;
	Gu8DPF_HighTempDerateCntrClr = 0;

	GfDPF_LowTempIMS_Derate = 1.0;
	GbDPF_LowTempIMS_DerateAct = FALSE;
	Gu8DPF_LowTempIMS_DerateCntrSet = 0;
	Gu8DPF_LowTempIMS_DerateCntrClr = 0;

	GfDPF_LowTempBrdDerate = 1.0;
	GbDPF_LowTempBrdDerateAct = FALSE;
	Gu8DPF_LowTempBrdDerateCntrSet = 0;
	Gu8DPF_LowTempBrdDerateCntrClr = 0;

	GfDPF_ExtCoolHighTempDerate = 1.0;
	GbDPF_ExtCoolHighTempDerateAct = FALSE;
	Gu8DPF_ExtCoolHighTempDerateCntrSet = 0;
	Gu8DPF_ExtCoolHighTempDerateCntrClr = 0;

	GfDPF_ExtCoolLowTempDerate = 1.0;
	GbDPF_ExtCoolLowTempDerateAct = FALSE;
	Gu8DPF_ExtCoolLowTempDerateCntrSet = 0;
	Gu8DPF_ExtCoolLowTempDerateCntrClr = 0;

	GfDPF_InletTempDerate = 1.0;
	GbDPF_InletTempDerateAct = FALSE;
	Gu8DPF_InletTempDerateCntrSet = 0;
	Gu8DPF_InletTempDerateCntrClr = 0;

	GfDPF_High12vDerate = 1.0;
	GbDPF_High12vDerateAct = FALSE;
	Gu8DPF_High12vDerateCntrSet = 0;
	Gu8DPF_High12vDerateCntrClr = 0;

	GfDPF_Low12vDerate = 1.0;
	GbDPF_Low12vDerateAct = FALSE;
	Gu8DPF_Low12vDerateCntrSet = 0;
	Gu8DPF_Low12vDerateCntrClr = 0;

	GfDPF_High48vDerate = 1.0;
	GbDPF_High48vDerateAct = FALSE;
	Gu8DPF_High48vDerateCntrSet = 0;
	Gu8DPF_High48vDerateCntrClr = 0;

	GfDPF_Low48vDerate = 1.0;
	GbDPF_Low48vDerateAct = FALSE;
	Gu8DPF_Low48vDerateCntrSet = 0;
	Gu8DPF_Low48vDerateCntrClr = 0;
}


