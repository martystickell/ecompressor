/*
 * SVM.h
 *
 *  Created on: Oct 18, 2018
 *      Author: H247670
 */

#ifndef APP_APP_SVM_H_
#define APP_APP_SVM_H_

#define		SQRT_THREE		1.732050808

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/
// dqr to ABC angle advance constant
extern float KfSVM_AngleAdvCoeff;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
// ABC phase voltage command
extern float GfSVM_VasCmd;
extern float GfSVM_VbsCmd;
extern float GfSVM_VcsCmd;

// ABC pole voltage command
extern float GfSVM_VanCmd;
extern float GfSVM_VbnCmd;
extern float GfSVM_VcnCmd;


/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
// SVPWM
extern void SVM_SVPWM(void);

// Vdq to Vabc conversion
extern void SVM_VdqToVabc(void);


#endif /* APP_APP_SVM_H_ */
