#ifndef RTW_HEADER_ThermProtn_Ecoe_2019a_h_
#define RTW_HEADER_ThermProtn_Ecoe_2019a_h_
#include "rtwtypes.h"
#include <math.h>
#ifndef ThermProtn_Ecoe_2019a_COMMON_INCLUDES_
# define ThermProtn_Ecoe_2019a_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

typedef struct tag_RTM RT_MODEL;
typedef struct {
  real_T Min;
  real_T Min_e;
  real_T Delay_DSTATE;
  real_T UnitDelay_DSTATE;
  real_T Delay3_DSTATE;
  real_T UnitDelay_DSTATE_c;
  real_T Delay1_DSTATE;
  real_T Delay_DSTATE_a;
  uint8_T icLoad;
  uint8_T icLoad_c;
  boolean_T Subsystem1_MODE;
  boolean_T Subsystem_MODE;
} DW;

typedef struct {
  real_T TpCoolt;
  real_T CurDrct;
  real_T CurQuad;
} ExtU;

typedef struct {
  real_T DertgFlg;
  real_T IndcrPwrResi;
  real_T TToFull;
  real_T TToEmpty;
  real_T TpMosfet;
} ExtY;

#define NOF_INV_HEAT_PWR_AT_125A_VALUES (5)
#define NOF_INV_HEAT_PWR_AT_300A_VALUES (5)
#define NOF_TOFF_VALUES                 (7)
#define NOF_TON_VALUES                  (7)
#define NOF_TPCOOLT_TOFF_VALUES         (7)
#define NOF_TPCOOLT_TON_VALUES          (7)
#define NOF_TPCOOLT_PWR_VALUES          (5)
#define NOF_RDS_ON_COEF_VALUES			(3)

struct P_ {
  real_T C;
  real_T CurDrctMax;
  real_T CurDrctMin;
  real_T CurQuadMax;
  real_T CurQuadMin;
  real_T CurrRef;
  real_T DcToAcPeakCon;
  real_T DertgFlgCanMax;
  real_T DertgFlgCanMin;
  real_T DertgPeakCur;
  real_T IndcrPwrResiCanMax;
  real_T IndcrPwrResiCanMin;
  real_T InvHeatPwrAt125A_M[NOF_INV_HEAT_PWR_AT_125A_VALUES];
  real_T InvHeatPwrAt300A_M[NOF_INV_HEAT_PWR_AT_300A_VALUES];
  real_T P_ref;
  real_T PeakCurFilEna;
  real_T PeakCurFilTCOn;
  real_T PwrIntglLowThd;
  real_T PwrIntglSatMaxThd;
  real_T R;
  real_T TOff_M[NOF_TOFF_VALUES];
  real_T TOn_M[NOF_TON_VALUES];
  real_T TToEmptyCanMax;
  real_T TToEmptyCanMin;
  real_T TToFullCanMax;
  real_T TToFullCanMin;
  real_T TpCooltCOn;
  real_T TpCooltFilEna;
  real_T TpCooltMax;
  real_T TpCooltMin;
  real_T TpCooltPwr_A[NOF_TPCOOLT_PWR_VALUES];
  real_T TpCooltTOff_A[NOF_TPCOOLT_TOFF_VALUES];
  real_T TpCooltTOn_A[NOF_TPCOOLT_TON_VALUES];
  real_T TpMosfetCanMax;
  real_T TpMosfetCanMin;
  real_T TpMosfetHiThd;
  real_T TpMosfetLowThd;
  real_T TpMosfetMax;
  real_T TpMosfetMin;
  real_T TpRef;
  real_T a;
  real_T b;
  real_T Constant9_Value;
  real_T Constant3_Value;
  real_T TtoEmpty_Y0;
  real_T Constant_Value;
  real_T TtoFull_Y0;
  real_T Constant1_Value;
  real_T Constant_Value_d;
  real_T Constant_Value_c;
  real_T WeightedSampleTimeMath_WtEt;
  real_T Constant1_Value_g;
  real_T UnitDelay_InitialCondition;
  real_T Delay3_InitialCondition;
  real_T Saturation2_UpperSat;
  real_T Saturation2_LowerSat;
  real_T UnitDelay_InitialCondition_l;
  real_T Delay1_InitialCondition;
  real_T Saturation1_UpperSat;
  real_T Saturation1_LowerSat;
  real_T Gain_Gain;
  real_T Constant1_Value_b;
  real_T Saturation_UpperSat;
  real_T Saturation_LowerSat;
  real_T Saturation1_UpperSat_g;
  real_T Saturation1_LowerSat_m;
  real_T Saturation2_UpperSat_h;
  real_T Saturation2_LowerSat_j;
  real_T Gain_Gain_g;
  real_T Constant_Value_d0;
  real_T WeightedSampleTimeMath_WtEt_j;
  real_T Constant1_Value_f;
  real_T RdsON_Coefs[NOF_RDS_ON_COEF_VALUES];
  real_T RdsON1_Coefs[NOF_RDS_ON_COEF_VALUES];
  real_T Constant_Value_n;
  real_T WeightedSampleTimeMath_WtEt_a;
  real_T Saturation_LowerSat_j;
  real_T WeightedSampleTime_WtEt;
  uint32_T Delay_DelayLength;
  uint32_T InterpolationUsingPrelookup1_ma;
  uint32_T InterpolationUsingPrelookup2_ma;
  uint32_T InterpolationUsingPrelookup_max;
  uint32_T InterpolationUsingPrelookup1__l;
  uint32_T Delay3_DelayLength;
  uint32_T Delay1_DelayLength;
  uint32_T Delay_DelayLength_c;
};

typedef struct P_ P;
struct tag_RTM {
  const char_T * volatile errorStatus;
};

extern P rtP;
extern DW rtDW;
extern ExtU rtU;
extern ExtY rtY;
extern void ThermProtnEcoeDerating( void );
extern RT_MODEL *const rtM;

#endif
