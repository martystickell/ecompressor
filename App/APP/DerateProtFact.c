/*
 * DCC.c
 *
 *  Created on: Sep 21, 2020
 *      Author: 122142
 */

#include <DerateProtFact.h>
#include "MES.h"
#include "CAN_Driver.h"

// High TempSens Derating
float KfDPF_HighTempIMS_DerateMin = 130;
float KfDPF_HighTempIMS_DerateMax = 140;

float KfDPF_HighTempIMS_DerateSet = 130;
float KfDPF_HighTempIMS_DerateClr = 115;

// Low Temp IMS Derating
float KfDPF_LowTempIMS_DerateMin = -30;
float KfDPF_LowTempIMS_DerateMax = -20;

float KfDPF_LowTempIMS_DerateSet = -20;
float KfDPF_LowTempIMS_DerateClr = -20;

uint8 Ku8DPF_TempIMS_DerateCnts = 50;

// Low Board Temp Derating
float KfDPF_LowBrdTempDerateMin = -30;
float KfDPF_LowBrdTempDerateMax = -20;

float KfDPF_LowBrdTempDerateSet = -20;
float KfDPF_LowBrdTempDerateClr = -20;

uint8 Ku8DPF_BrdTempDerateCnts = 50;

uint8 Ku8DPF_ExtTempDerateCnts = 50;

// High 12 volts Derating
float KfDPF_High12vDerateMin = 17.0;
float KfDPF_High12vDerateMax = 18.0;

float KfDPF_High12vDerateSet = 17.0;
float KfDPF_High12vDerateClr = 15.0;

// Low 12 volts Derating
float KfDPF_Low12vDerateMin = 8.0;
float KfDPF_Low12vDerateMax = 9.0;

float KfDPF_Low12vDerateSet = 9.0;
float KfDPF_Low12vDerateClr = 11.0;

// High 48 volts Derating
float KfDPF_High48vDerateMin = 54.0;
float KfDPF_High48vDerateMax = 60.0;

float KfDPF_High48vDerateSet = 54.0;
float KfDPF_High48vDerateClr = 53.9;

// Low 48 volts Derating
float KfDPF_Low48vDerateMin = 20.0;
float KfDPF_Low48vDerateMax = 24.0;

float KfDPF_Low48vDerateSet = 24.0;
float KfDPF_Low48vDerateClr = 24.1;

uint8 Ku8DPF_VoltageDerateCnts = 50;

// Ext Coolant High Temp Derating
float KfDPF_ExtCoolHighTempDerateMax = 95;
float KfDPF_ExtCoolHighTempDerateMin = 85;
float KfDPF_ExtCoolHighTempDerateSet = 85;
float KfDPF_ExtCoolHighTempDerateClr = 75;

// Ext Coolant Low Temp Derating
float KfDPF_ExtCoolLowTempDerateMax = -20;
float KfDPF_ExtCoolLowTempDerateMin = -30;
float KfDPF_ExtCoolLowTempDerateSet = -20;
float KfDPF_ExtCoolLowTempDerateClr = -20;

// Ext Inlet Temp Derating
float KfDPF_InletTempDerateMax = 160;
float KfDPF_InletTempDerateMin = 150;
float KfDPF_InletTempDerateSet = 150;
float KfDPF_InletTempDerateClr = 135;

float GfDPF_HighTempIMS_Derate = 1.0;
boolean GbDPF_HighTempIMS_DerateAct = FALSE;
uint8 Gu8DPF_HighTempDerateCntrSet = 0;
uint8 Gu8DPF_HighTempDerateCntrClr = 0;

float GfDPF_LowTempIMS_Derate = 1.0;
boolean GbDPF_LowTempIMS_DerateAct = FALSE;
uint8 Gu8DPF_LowTempIMS_DerateCntrSet = 0;
uint8 Gu8DPF_LowTempIMS_DerateCntrClr = 0;

float GfDPF_LowTempBrdDerate = 1.0;
boolean GbDPF_LowTempBrdDerateAct = FALSE;
uint8 Gu8DPF_LowTempBrdDerateCntrSet = 0;
uint8 Gu8DPF_LowTempBrdDerateCntrClr = 0;

float GfDPF_High12vDerate = 1.0;
boolean GbDPF_High12vDerateAct = FALSE;
uint8 Gu8DPF_High12vDerateCntrSet = 0;
uint8 Gu8DPF_High12vDerateCntrClr = 0;

float GfDPF_Low12vDerate = 1.0;
boolean GbDPF_Low12vDerateAct = FALSE;
uint8 Gu8DPF_Low12vDerateCntrSet = 0;
uint8 Gu8DPF_Low12vDerateCntrClr = 0;

float GfDPF_High48vDerate = 1.0;
boolean GbDPF_High48vDerateAct = FALSE;
uint8 Gu8DPF_High48vDerateCntrSet = 0;
uint8 Gu8DPF_High48vDerateCntrClr = 0;

float GfDPF_Low48vDerate = 1.0;
boolean GbDPF_Low48vDerateAct = FALSE;
uint8 Gu8DPF_Low48vDerateCntrSet = 0;
uint8 Gu8DPF_Low48vDerateCntrClr = 0;

uint8 Gu8DPF_ExtCoolHighTempDerateCntrSet = 0;
uint8 Gu8DPF_ExtCoolHighTempDerateCntrClr = 0;
boolean GbDPF_ExtCoolHighTempDerateAct = FALSE;
float GfDPF_ExtCoolHighTempDerate = 0;

uint8 Gu8DPF_ExtCoolLowTempDerateCntrSet = 0;
uint8 Gu8DPF_ExtCoolLowTempDerateCntrClr = 0;
boolean GbDPF_ExtCoolLowTempDerateAct = FALSE;
float GfDPF_ExtCoolLowTempDerate = 0;

uint8 Gu8DPF_InletTempDerateCntrSet = 0;
uint8 Gu8DPF_InletTempDerateCntrClr = 0;
boolean GbDPF_InletTempDerateAct = FALSE;
float GfDPF_InletTempDerate = 0;

float DPF_Rescale(float LfDPF_Input, float LfDPF_Max, float LfDPF_Min)
{
	float LfDPF_Result = 0.0;

	if (LfDPF_Input >= LfDPF_Max)
	{
		LfDPF_Result = 1.0;
	}
	else if (LfDPF_Input >= LfDPF_Min)
	{
		LfDPF_Result = (LfDPF_Input - LfDPF_Min) / (LfDPF_Max - LfDPF_Min);
	}
	else
	{
		LfDPF_Result = 0.0;
	}
	return (LfDPF_Result);
}



void DPF_TempIMS_ThermalDerating(void)
{
	float LfDPF_TempIMS;

	LfDPF_TempIMS = GfMES_TempIMS;

	/* High Temperature Derating */
	if (LfDPF_TempIMS > KfDPF_HighTempIMS_DerateSet)
	{
		if (Gu8DPF_HighTempDerateCntrSet >= Ku8DPF_TempIMS_DerateCnts)
		{
			GbDPF_HighTempIMS_DerateAct = TRUE;
			Gu8DPF_HighTempDerateCntrSet = (uint8)0;
		}
		else if (GbDPF_HighTempIMS_DerateAct == FALSE)
		{
			Gu8DPF_HighTempDerateCntrSet ++;
		}

		Gu8DPF_HighTempDerateCntrClr = (uint8)0;
	}
	else if (LfDPF_TempIMS <= KfDPF_HighTempIMS_DerateClr)
	{
		if (Gu8DPF_HighTempDerateCntrClr >= Ku8DPF_TempIMS_DerateCnts)
		{
			GbDPF_HighTempIMS_DerateAct = FALSE;
			Gu8DPF_HighTempDerateCntrClr = (uint8)0;
		}
		else if (GbDPF_HighTempIMS_DerateAct == TRUE)
		{
			Gu8DPF_HighTempDerateCntrClr ++;
		}
		Gu8DPF_HighTempDerateCntrSet = (uint8)0;
	}
	else
	{

	}

	if (GbDPF_HighTempIMS_DerateAct == TRUE)
	{
		GfDPF_HighTempIMS_Derate = 1.0 - DPF_Rescale(LfDPF_TempIMS,
				                                     KfDPF_HighTempIMS_DerateMax,
													 KfDPF_HighTempIMS_DerateMin);
	}

	/* Low Temperature Derating */
	if (LfDPF_TempIMS < KfDPF_LowTempIMS_DerateSet)
	{
		if (Gu8DPF_LowTempIMS_DerateCntrSet >= Ku8DPF_TempIMS_DerateCnts)
		{
			GbDPF_LowTempIMS_DerateAct = TRUE;
			Gu8DPF_LowTempIMS_DerateCntrSet = (uint8)0;
		}
		else if (GbDPF_LowTempIMS_DerateAct == FALSE)
		{
			Gu8DPF_LowTempIMS_DerateCntrSet ++;
		}

		Gu8DPF_LowTempIMS_DerateCntrClr = (uint8)0;
	}
	else if (LfDPF_TempIMS >= KfDPF_LowTempIMS_DerateClr)
	{
		if (Gu8DPF_LowTempIMS_DerateCntrClr >= Ku8DPF_TempIMS_DerateCnts)
		{
			GbDPF_LowTempIMS_DerateAct = FALSE;
			Gu8DPF_LowTempIMS_DerateCntrClr = (uint8)0;
		}
		else if (GbDPF_LowTempIMS_DerateAct == TRUE)
		{
			Gu8DPF_LowTempIMS_DerateCntrClr ++;
		}
		Gu8DPF_LowTempIMS_DerateCntrSet = (uint8)0;
	}

    if (GbDPF_LowTempIMS_DerateAct == TRUE)
    {
    	GfDPF_LowTempIMS_Derate = DPF_Rescale(LfDPF_TempIMS,
    			                              KfDPF_LowTempIMS_DerateMax,
											  KfDPF_LowTempIMS_DerateMin);
    }
}


void DPF_TempBrdThermalDerating(void)
{
	float LfDPF_MinTemp;

	if (GfMES_TempBdr1 < GfMES_TempBdr2)
	{
		LfDPF_MinTemp = GfMES_TempBdr1;
	}
	else
	{
		LfDPF_MinTemp = GfMES_TempBdr2;
	}

	/* Low Temperature Derating */
	if (LfDPF_MinTemp < KfDPF_LowBrdTempDerateSet)
	{
		if (Gu8DPF_LowTempBrdDerateCntrSet >= Ku8DPF_BrdTempDerateCnts)
		{
			GbDPF_LowTempBrdDerateAct = TRUE;
			Gu8DPF_LowTempBrdDerateCntrSet = (uint8)0;
		}
		else if (GbDPF_LowTempBrdDerateAct == FALSE)
		{
			Gu8DPF_LowTempBrdDerateCntrSet ++;
		}

		Gu8DPF_LowTempBrdDerateCntrClr = (uint8)0;
	}
	else if (LfDPF_MinTemp >= KfDPF_LowBrdTempDerateClr)
	{
		if (Gu8DPF_LowTempBrdDerateCntrClr >= Ku8DPF_BrdTempDerateCnts)
		{
			GbDPF_LowTempBrdDerateAct = FALSE;
			Gu8DPF_LowTempBrdDerateCntrClr = (uint8)0;
		}
		else if (GbDPF_LowTempBrdDerateAct == TRUE)
		{
			Gu8DPF_LowTempBrdDerateCntrClr ++;
		}
		Gu8DPF_LowTempBrdDerateCntrSet = (uint8)0;
	}

    if (GbDPF_LowTempBrdDerateAct == TRUE)
    {
    	GfDPF_LowTempBrdDerate = DPF_Rescale(LfDPF_MinTemp,
    			                              KfDPF_LowBrdTempDerateMax,
											  KfDPF_LowBrdTempDerateMin);
    }

}


void DPF_ExtCoolantTempDerating(void)
{
	float LfDPF_CoolTemp;
	LfDPF_CoolTemp = GfCAN_CoolantTemp;

	/* High External Coolant Temperature Derating */
	if (LfDPF_CoolTemp > KfDPF_ExtCoolHighTempDerateSet)
	{
		if (Gu8DPF_ExtCoolHighTempDerateCntrSet >= Ku8DPF_ExtTempDerateCnts)
		{
			GbDPF_ExtCoolHighTempDerateAct = TRUE;
			Gu8DPF_ExtCoolHighTempDerateCntrSet = (uint8)0;
		}
		else if (GbDPF_ExtCoolHighTempDerateAct == FALSE)
		{
			Gu8DPF_ExtCoolHighTempDerateCntrSet ++;
		}

		Gu8DPF_ExtCoolHighTempDerateCntrClr = (uint8)0;
	}
	else if (LfDPF_CoolTemp <= KfDPF_ExtCoolHighTempDerateClr)
	{
		if (Gu8DPF_ExtCoolHighTempDerateCntrClr >= Ku8DPF_ExtTempDerateCnts)
		{
			GbDPF_ExtCoolHighTempDerateAct = FALSE;
			Gu8DPF_ExtCoolHighTempDerateCntrClr = (uint8)0;
		}
		else if (GbDPF_ExtCoolHighTempDerateAct == TRUE)
		{
			Gu8DPF_ExtCoolHighTempDerateCntrClr ++;
		}
		Gu8DPF_ExtCoolHighTempDerateCntrSet = (uint8)0;
	}

    if (GbDPF_ExtCoolHighTempDerateAct == TRUE)
    {
    	GfDPF_ExtCoolHighTempDerate = 1.0 - DPF_Rescale(LfDPF_CoolTemp,
    			                              KfDPF_ExtCoolHighTempDerateMax,
											  KfDPF_ExtCoolHighTempDerateMin);
    }

	/* Low External Coolant Temperature Derating */
	if (LfDPF_CoolTemp < KfDPF_ExtCoolLowTempDerateSet)
	{
		if (Gu8DPF_ExtCoolLowTempDerateCntrSet >= Ku8DPF_ExtTempDerateCnts)
		{
			GbDPF_ExtCoolLowTempDerateAct = TRUE;
			Gu8DPF_ExtCoolLowTempDerateCntrSet = (uint8)0;
		}
		else if (GbDPF_ExtCoolHighTempDerateAct == FALSE)
		{
			Gu8DPF_ExtCoolLowTempDerateCntrSet ++;
		}

		Gu8DPF_ExtCoolLowTempDerateCntrClr = (uint8)0;
	}
	else if (LfDPF_CoolTemp >= KfDPF_ExtCoolLowTempDerateClr)
	{
		if (Gu8DPF_ExtCoolLowTempDerateCntrClr >= Ku8DPF_ExtTempDerateCnts)
		{
			GbDPF_ExtCoolLowTempDerateAct = FALSE;
			Gu8DPF_ExtCoolLowTempDerateCntrClr = (uint8)0;
		}
		else if (GbDPF_ExtCoolLowTempDerateAct == TRUE)
		{
			Gu8DPF_ExtCoolLowTempDerateCntrClr ++;
		}
		Gu8DPF_ExtCoolLowTempDerateCntrSet = (uint8)0;
	}

    if (GbDPF_ExtCoolLowTempDerateAct == TRUE)
    {
    	GfDPF_ExtCoolLowTempDerate = DPF_Rescale(LfDPF_CoolTemp,
    			                              KfDPF_ExtCoolLowTempDerateMax,
											  KfDPF_ExtCoolLowTempDerateMin);
    }

}


void DPF_InletTempDerating(void)
{
	float LfDPF_InletTemp;
	LfDPF_InletTemp = GfCAN_InletTemp;

	/* Low Temperature Derating */
	if (LfDPF_InletTemp > KfDPF_InletTempDerateSet)
	{
		if (Gu8DPF_InletTempDerateCntrSet >= Ku8DPF_ExtTempDerateCnts)
		{
			GbDPF_InletTempDerateAct = TRUE;
			Gu8DPF_InletTempDerateCntrSet = (uint8)0;
		}
		else if (GbDPF_InletTempDerateAct == FALSE)
		{
			Gu8DPF_InletTempDerateCntrSet ++;
		}

		Gu8DPF_InletTempDerateCntrClr = (uint8)0;
	}
	else if (LfDPF_InletTemp <= KfDPF_InletTempDerateClr)
	{
		if (Gu8DPF_InletTempDerateCntrClr >= Ku8DPF_ExtTempDerateCnts)
		{
			GbDPF_InletTempDerateAct = FALSE;
			Gu8DPF_InletTempDerateCntrClr = (uint8)0;
		}
		else if (GbDPF_InletTempDerateAct == TRUE)
		{
			Gu8DPF_InletTempDerateCntrClr ++;
		}
		Gu8DPF_InletTempDerateCntrSet = (uint8)0;
	}

    if (GbDPF_InletTempDerateAct == TRUE)
    {
    	GfDPF_InletTempDerate = 1.0 - DPF_Rescale(LfDPF_InletTemp,
    			                            KfDPF_InletTempDerateMax,
										    KfDPF_InletTempDerateMin);
    }
}

void DPF_VoltageDerating(void)
{
	float LfDPF_Volts;

	LfDPF_Volts = GfMES_V12;

	/* High 12 volt Derating */
	if (LfDPF_Volts > KfDPF_High12vDerateSet)
	{
		if (Gu8DPF_High12vDerateCntrSet >= Ku8DPF_VoltageDerateCnts)
		{
			GbDPF_High12vDerateAct = TRUE;
			Gu8DPF_High12vDerateCntrSet = (uint8)0;
		}
		else if (GbDPF_High12vDerateAct == FALSE)
		{
			Gu8DPF_High12vDerateCntrSet ++;
		}

		Gu8DPF_High12vDerateCntrClr = (uint8)0;
	}
	else if (LfDPF_Volts <= KfDPF_High12vDerateClr)
	{
		if (Gu8DPF_High12vDerateCntrClr >= Ku8DPF_VoltageDerateCnts)
		{
			GbDPF_High12vDerateAct = FALSE;
			Gu8DPF_High12vDerateCntrClr = (uint8)0;
		}
		else if (GbDPF_High12vDerateAct == TRUE)
		{
			Gu8DPF_High12vDerateCntrClr ++;
		}
		Gu8DPF_High12vDerateCntrSet = (uint8)0;
	}
	else
	{

	}

	if (GbDPF_High12vDerateAct == TRUE)
	{
		GfDPF_High12vDerate = 1.0 - DPF_Rescale(LfDPF_Volts,
				                                KfDPF_High12vDerateMax,
												KfDPF_High12vDerateMin);
	}

	/* Low 12 volts Derating */
	if (LfDPF_Volts < KfDPF_Low12vDerateSet)
	{
		if (Gu8DPF_Low12vDerateCntrSet >= Ku8DPF_VoltageDerateCnts)
		{
			GbDPF_Low12vDerateAct = TRUE;
			Gu8DPF_Low12vDerateCntrSet = (uint8)0;
		}
		else if (GbDPF_Low12vDerateAct == FALSE)
		{
			Gu8DPF_Low12vDerateCntrSet ++;
		}

		Gu8DPF_Low12vDerateCntrClr = (uint8)0;
	}
	else if (LfDPF_Volts >= KfDPF_Low12vDerateClr)
	{
		if (Gu8DPF_Low12vDerateCntrClr >= Ku8DPF_VoltageDerateCnts)
		{
			GbDPF_Low12vDerateAct = FALSE;
			Gu8DPF_Low12vDerateCntrClr = (uint8)0;
		}
		else if (GbDPF_Low12vDerateAct == TRUE)
		{
			Gu8DPF_Low12vDerateCntrClr ++;
		}
		Gu8DPF_Low12vDerateCntrSet = (uint8)0;
	}
	else
	{

	}

    if (GbDPF_Low12vDerateAct == TRUE)
    {
    	GfDPF_Low12vDerate = DPF_Rescale(LfDPF_Volts,
    			                         KfDPF_Low12vDerateMax,
										 KfDPF_Low12vDerateMin);
    }


	LfDPF_Volts = GfMES_Vdc;

	/* High 48 volt Derating */
	if (LfDPF_Volts > KfDPF_High48vDerateSet)
	{
		if (Gu8DPF_High48vDerateCntrSet >= Ku8DPF_VoltageDerateCnts)
		{
			GbDPF_High48vDerateAct = TRUE;
			Gu8DPF_High48vDerateCntrSet = (uint8)0;
		}
		else if (GbDPF_High48vDerateAct == FALSE)
		{
			Gu8DPF_High48vDerateCntrSet ++;
		}

		Gu8DPF_High48vDerateCntrClr = (uint8)0;
	}
	else if (LfDPF_Volts <= KfDPF_High48vDerateClr)
	{
		if (Gu8DPF_High48vDerateCntrClr >= Ku8DPF_VoltageDerateCnts)
		{
			GbDPF_High48vDerateAct = FALSE;
			Gu8DPF_High48vDerateCntrClr = (uint8)0;
		}
		else if (GbDPF_High48vDerateAct == TRUE)
		{
			Gu8DPF_High48vDerateCntrClr ++;
		}
		Gu8DPF_High48vDerateCntrSet = (uint8)0;
	}
	else
	{

	}

	if (GbDPF_High48vDerateAct == TRUE)
	{
		GfDPF_High48vDerate = 1.0 - DPF_Rescale(LfDPF_Volts,
				                                KfDPF_High48vDerateMax,
												KfDPF_High48vDerateMin);
	}

	/* Low 48 volts Derating */
	if (LfDPF_Volts < KfDPF_Low48vDerateSet)
	{
		if (Gu8DPF_Low48vDerateCntrSet >= Ku8DPF_VoltageDerateCnts)
		{
			GbDPF_Low48vDerateAct = TRUE;
			Gu8DPF_Low48vDerateCntrSet = (uint8)0;
		}
		else if (GbDPF_Low48vDerateAct == FALSE)
		{
			Gu8DPF_Low48vDerateCntrSet ++;
		}

		Gu8DPF_Low48vDerateCntrClr = (uint8)0;
	}
	else if (LfDPF_Volts >= KfDPF_Low48vDerateClr)
	{
		if (Gu8DPF_Low48vDerateCntrClr >= Ku8DPF_VoltageDerateCnts)
		{
			GbDPF_Low48vDerateAct = FALSE;
			Gu8DPF_Low48vDerateCntrClr = (uint8)0;
		}
		else if (GbDPF_Low48vDerateAct == TRUE)
		{
			Gu8DPF_Low48vDerateCntrClr ++;
		}
		Gu8DPF_Low48vDerateCntrSet = (uint8)0;
	}
	else
	{

	}

    if (GbDPF_Low48vDerateAct == TRUE)
    {
    	GfDPF_Low48vDerate = DPF_Rescale(LfDPF_Volts,
    			                         KfDPF_Low48vDerateMax,
										 KfDPF_Low48vDerateMin);
    }

}
