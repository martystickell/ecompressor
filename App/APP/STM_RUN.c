/*
 * STM_OpenLoopTest.c
 *
 *  Created on: Oct 25, 2018
 *      Author: H247670
 */
#include	"STM.h"
#include	"GTM.h"
#include	"CCC.h"
#include	"MES.h"
#include	"SVM.h"
#include	"CAN_Driver.h"

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/

void STM_RunT0(void)
{
	// Enable PWM if the state is changed from StandBy.
	if (GeSTM_LastStateT0 == StandBy)
	{
		GTM_PWMEnable();
	}

	// Copy the next state value to current.
	GeSTM_RunSubState = GeSTM_RunSubStateNext;

	// Reading ADC queue 0 & calculation
	ADC_ReadIabcVdc();
	MES_CalcIabcIdq();
	MES_CalcVdc();

	// Triggering ADC background scan for Temperature reading
	vadc_trig_group0_scan();
	vadc_trig_group1_scan();

	// Run diagnostics.
	STM_DiagT0();

	// Latching T0 diagnostics
	STM_LatchingT0Flt();

	// Disable PWM if there is any fault.
	if (GbSTM_FltT0 || GbSTM_FltT1)
	{
		GTM_PWMDisable();
	}

	switch(GeSTM_RunSubState)
	{
		case StandStill:

			GfCCC_Wr = 0.0;
			GfCCC_Wrpm = 0.0;
			GfCCC_Thetar = 0.0;
			GfCCC_InvWr = INV_MIN_DIVISION;

			CCC_CurCntr();
			SVM_VdqToVabc();
			SVM_SVPWM();

			break;

		case OpenLoop:

			CCC_FlxEst();

			GfCCC_Wrpm = STM_OpenLoopSpeedRPMCalc(GfCCC_Wrpm, GfCCC_WrpmCmdSlew, GfGTM_T0);
			GfCCC_Wr = GfCCC_Wrpm * RPM_TO_RADS;

			if (GfCCC_Wr > MIN_DIVISION)
			{
				GfCCC_InvWr = 1.0 / GfCCC_Wr;
			}
			else
			{
				GfCCC_InvWr = INV_MIN_DIVISION;
			}

			GfCCC_Thetar = STM_OpenLoopThetarCalc(GfCCC_Thetar, GfCCC_Wr, GfGTM_T0);

			GfCCC_ThetarEst = GfCCC_Thetar;
			GfCCC_ThetarEstNew = GfCCC_Thetar;

			GfCCC_WrEstNew = GfCCC_Wr;
			GfCCC_WrEst = GfCCC_Wr;

			GfCCC_AlEst = 0.0;
			GfCCC_AlEstNew = 0.0;

			// dq current command in open-loop test mode
			GfCCC_IdCmd = KfSTM_IdCmdOpenLoop;
			GfCCC_IqCmd = KfSTM_IqCmdOpenLoop;

			CCC_CurCntr();
			SVM_VdqToVabc();
			SVM_SVPWM();

			break;

		case ClosedLoop:

			// SubState transition
			CCC_FlxEst();
			CCC_SpdEst();

			GfCCC_Wr = GfCCC_WrEstNew;
			GfCCC_Wrpm = GfCCC_Wr * RADS_TO_RPM;

			if (GfCCC_Wr > MIN_DIVISION)
			{
				GfCCC_InvWr = 1.0 / GfCCC_Wr;
			}
			else
			{
				GfCCC_InvWr = INV_MIN_DIVISION;
			}

			GfCCC_Thetar = GfCCC_ThetarEstNew;

			MES_CalcIabcIdq02();
			CCC_FluxWeakening();
			CCC_CurCntr();
			SVM_VdqToVabc();
			SVM_SVPWM();

			break;
	}
	GTM_UpdatePWMOutputs(GsGTM_OutputPWM, Gu16GTM_T0CNT);
}

void STM_RunT1(void)
{
	// Read ADC background scan & calculate
	ADC_ReadT1Values();
	MES_CalcTemps();

	// Run T1 diagnostics
	STM_DiagT1();
	STM_ThermalWarning();
	STM_VoltageWarning();

	// Latching T1 faults
	STM_LatchingT1Flt();

	// Controller Re-initialization
	CCC_ReInitInvs();
	CCC_ReInitCurtCtrl();
	CCC_ReInitSpdCtrl();
	CCC_ReInitFlxEst();
	CCC_ReInitSpdEst();

	// T0 Period calc
	GTM_T0Calc();

	// CAN value reading
	CAN_CmdUpdate();

	// Control mode decision
	GeSTM_ControlMode = GeSTM_ControlModeCmd;

	// De-rating calculation
	CCC_Derating();

	// Current command max calculation
	CCC_IdqCmdLim();

	switch(GeSTM_RunSubState)
	{
		case StandStill:
			// Speed command conversion
			GfCCC_WrpmCmdSlew = 0.0;
			GfCCC_WrCmdSlew = 0.0;

			CCC_ResetSpdCntr();

			// dq current command in open-loop control mode
			GfCCC_IdCmd = KfSTM_IdCmdOpenLoop;
			GfCCC_IqCmd = KfSTM_IqCmdOpenLoop;

			Gu16STM_RunClosedLoopCNT = 0;
			GbSTM_RunClosedLoopCNTDone = FALSE;

			// SubState transition
			if (Gu32STM_RunStandStillCNT > Ku32STM_RunStandStillCNTLim)
			{
				GbSTM_RunStandStillDone = TRUE;
				GeSTM_RunSubStateNext = OpenLoop;

				Gu32STM_RunStandStillCNT = 0;
			}
			else
			{
				GbSTM_RunStandStillDone = FALSE;
				GeSTM_RunSubStateNext = StandStill;

				Gu32STM_RunStandStillCNT = Gu32STM_RunStandStillCNT + 1;
			}

			break;

		case OpenLoop:

			// Reset StandStill state counter
			Gu32STM_RunStandStillCNT = 0;

			// Speed-torque command update
			STM_SpdTrqCmdUpdtOL();

			// dq current command in open-loop control mode
			GfCCC_IdCmd = KfSTM_IdCmdOpenLoop;
			GfCCC_IqCmd = KfSTM_IqCmdOpenLoop;

			Gu16STM_RunClosedLoopCNT = 0;
			GbSTM_RunClosedLoopCNTDone = FALSE;

			// SubState transition
			if (GfCCC_Wrpm > KfSTM_RunOLtoCL)
			{
				GeSTM_RunSubStateNext = ClosedLoop;
			}
			else
			{
				GeSTM_RunSubStateNext = OpenLoop;
			}
			break;

		case ClosedLoop:

			// Reset stand still state counter
			Gu32STM_RunStandStillCNT = 0;

			// Speed & torque command update
			STM_SpdTrqCmdUpdtCL();

			// Delay counter for CL-OL state transition
			if (Gu16STM_RunClosedLoopCNT > Ku16STM_RunClosedCNTSet)
			{
				Gu16STM_RunClosedLoopCNT = Ku16STM_RunClosedCNTSet;
				GbSTM_RunClosedLoopCNTDone = TRUE;
			}
			else
			{
				Gu16STM_RunClosedLoopCNT = Gu16STM_RunClosedLoopCNT + 1;
				GbSTM_RunClosedLoopCNTDone = FALSE;
			}

			if ((GfCCC_Wrpm < KfSTM_RunCLtoOL) && (GbSTM_RunClosedLoopCNTDone == TRUE))
			{
				GeSTM_RunSubStateNext = OpenLoop;

				Gu16STM_RunClosedLoopCNT = 0;
				GbSTM_RunClosedLoopCNTDone = FALSE;
			}
			else
			{
				GeSTM_RunSubStateNext = ClosedLoop;
			}

			break;
	}

	// State transition
	if ((GbSTM_FltT0 || GbSTM_FltT1) || (GeSTM_StateCmd == Fault))
	{
		GeSTM_NextStateT1 = Fault;
	}
	else if ((GeSTM_StateCmd == StandBy) || (GeSTM_StateCmd == TurnOff) || (GeSTM_StateCmd == TurnOn) || (GeSTM_StateCmd == Off))
	{
		GeSTM_NextStateT1 = StandBy;
	}
	else
	{
		GeSTM_NextStateT1 = Run;
	}
}

// Speed calculation in open-loop sub-state
float STM_OpenLoopSpeedRPMCalc(float LfSTM_Wrpm, float LfSTM_WrpmCmd, float LfSTM_Tsamp)
{
	float LfSTM_WrpmOpenLoopTmp = 0.0;

	LfSTM_WrpmOpenLoopTmp = CCC_SlewLimit(LfSTM_Wrpm, LfSTM_WrpmCmd, KfSTM_WrpmCmdOpenLoopSlew, KfSTM_WrpmCmdOpenLoopSlew, LfSTM_Tsamp);

	// Min. and Max. limit of frequency command
	if (LfSTM_WrpmOpenLoopTmp > KfSTM_WrpmOpenLoopMax)
	{
		LfSTM_Wrpm = KfSTM_WrpmOpenLoopMax;
	}
	else if (LfSTM_WrpmOpenLoopTmp < KfSTM_WrpmOpenLoopMin)
	{
		LfSTM_Wrpm = KfSTM_WrpmOpenLoopMin;
	}
	else
	{
		LfSTM_Wrpm = LfSTM_WrpmOpenLoopTmp;
	}

	return LfSTM_Wrpm;
}

// Open-loop angle calculation
float STM_OpenLoopThetarCalc(float LfSTM_Thetar, float LfSTM_Wr, float LfSTM_Tsamp)
{
	float LfSTM_ThetarTmp = 0.0;

	// Open-loop angle calculation
	LfSTM_ThetarTmp = LfSTM_Thetar + LfSTM_Tsamp * LfSTM_Wr;

	LfSTM_Thetar = CCC_ThetarLim(LfSTM_ThetarTmp);

	return LfSTM_Thetar;
}

// Speed & torque command update (Closed-loop)
void STM_SpdTrqCmdUpdtCL(void)
{
	float LfCCC_IqCmdPreTmp = 0.0;

	// Torque control vs. speed control
	if (GeSTM_ControlMode == TorqueControl)
	{
		// Reset speed controller
		GfCCC_IntegErrWr = 0.0;

		// Torque command update
		GfCCC_TeCmdPre = CCC_SlewLimit(GfCCC_TeCmdPre, GfCCC_TeCmdCAN, GfCCC_TrqCmdSlewDnCAN, GfCCC_TrqCmdSlewUpCAN, GfGTM_T1);

		LfCCC_IqCmdPreTmp = TWO_OVER_THREE * GfCCC_InvLamPM * GfCCC_TeCmdPre;

		if (LfCCC_IqCmdPreTmp > GfCCC_IqCmdMax)
		{
			GfCCC_IqCmdPre = GfCCC_IqCmdMax;
		}
		else if (LfCCC_IqCmdPreTmp < GfCCC_IqCmdMin)
		{
			GfCCC_IqCmdPre = GfCCC_IqCmdMin;
		}
		else
		{
			GfCCC_IqCmdPre = LfCCC_IqCmdPreTmp;
		}

		// Speed command set for OL transition
		if (GfCCC_IqCmdPre > 0.0)
		{
			GfCCC_WrpmCmd = KfSTM_WrpmCmdPosTe;
		}
		else
		{
			GfCCC_WrpmCmd = KfSTM_WrpmCmdNegTe;
		}

		// Speed command conversion
		GfCCC_WrpmCmdSlew = CCC_SlewLimit(GfCCC_WrpmCmdSlew, GfCCC_WrpmCmd, GfCCC_WrpmCmdSlewDnCAN, GfCCC_WrpmCmdSlewUpCAN, GfGTM_T1);
		GfCCC_WrCmdSlew = GfCCC_WrpmCmdSlew * RPM_TO_RADS;
	}
	else
	{
		// Speed command update
		GfCCC_WrpmCmd = GfCCC_WrpmCmdCAN;

		// Speed command conversion
		GfCCC_WrpmCmdSlew = CCC_SlewLimit(GfCCC_WrpmCmdSlew, GfCCC_WrpmCmd, GfCCC_WrpmCmdSlewDnCAN, GfCCC_WrpmCmdSlewUpCAN, GfGTM_T1);
		GfCCC_WrCmdSlew = GfCCC_WrpmCmdSlew * RPM_TO_RADS;

		// Run speed controller
		CCC_SpdCntr();
	}
}

// Speed & torque command update (Open-loop)
void STM_SpdTrqCmdUpdtOL(void)
{
	// Torque control vs. speed control
	if (GeSTM_ControlMode == TorqueControl)
	{
		// Reset speed controller
		GfCCC_IntegErrWr = 0.0;

		// Torque command update
		GfCCC_TeCmdPre = 0.0;
		GfCCC_TeCmd = 0.0;

		// Speed command set for OL transition
		if (GfCCC_TeCmdCAN > 0.0)
		{
			GfCCC_WrpmCmd = KfSTM_WrpmCmdPosTe;
		}
		else
		{
			GfCCC_WrpmCmd = KfSTM_WrpmCmdNegTe;
		}

		// Speed command conversion
		GfCCC_WrpmCmdSlew = CCC_SlewLimit(GfCCC_WrpmCmdSlew, GfCCC_WrpmCmd, GfCCC_WrpmCmdSlewDnCAN, GfCCC_WrpmCmdSlewUpCAN, GfGTM_T1);
		GfCCC_WrCmdSlew = GfCCC_WrpmCmdSlew * RPM_TO_RADS;
	}
	else
	{
		// Speed command update
		GfCCC_WrpmCmd = GfCCC_WrpmCmdCAN;

		// Speed command conversion
		GfCCC_WrpmCmdSlew = CCC_SlewLimit(GfCCC_WrpmCmdSlew, GfCCC_WrpmCmd, GfCCC_WrpmCmdSlewDnCAN, GfCCC_WrpmCmdSlewUpCAN, GfGTM_T1);
		GfCCC_WrCmdSlew = GfCCC_WrpmCmdSlew * RPM_TO_RADS;

		CCC_ResetSpdCntr();
	}
}
