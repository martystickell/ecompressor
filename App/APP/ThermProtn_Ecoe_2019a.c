#include "CAN_Driver.h"
#include "CAN_Interface_Tx.h"
#include "CCC.h"
#include "MES.h"
#include "ThermProtn_Ecoe_2019a.h"

//DW rtDW;
ExtU rtU;
ExtY rtY;

//ExtU Input;
static uint32_T plook_bincag(real_T u, const real_T bp[], uint32_T maxIndex,
  real_T *fraction);
static real_T intrp1d_la(uint32_T bpIndex, real_T frac, const real_T table[],
  uint32_T maxIndex);
static uint32_T plook_binca(real_T u, const real_T bp[], uint32_T maxIndex,
  real_T *fraction);
static uint32_T binsearch_u32d(real_T u, const real_T bp[], uint32_T startIndex,
  uint32_T maxIndex);
static uint32_T plook_bincag(real_T u, const real_T bp[], uint32_T maxIndex,
  real_T *fraction)
{
  uint32_T bpIndex;
  if (u < bp[maxIndex]) {
    bpIndex = binsearch_u32d(u, bp, maxIndex >> 1U, maxIndex);
    *fraction = (u - bp[bpIndex]) / (bp[bpIndex + 1U] - bp[bpIndex]);
  } else {
    bpIndex = maxIndex;
    *fraction = 0.0;
  }

  return bpIndex;
}

static real_T intrp1d_la(uint32_T bpIndex, real_T frac, const real_T table[],
  uint32_T maxIndex)
{
  real_T y;

  if (bpIndex == maxIndex) {
    y = table[bpIndex];
  } else {
    y = (table[bpIndex + 1U] - table[bpIndex]) * frac + table[bpIndex];
  }

  return y;
}

static uint32_T plook_binca(real_T u, const real_T bp[], uint32_T maxIndex,
  real_T *fraction)
{
  uint32_T bpIndex;
  if (u <= bp[0U]) {
    bpIndex = 0U;
    *fraction = 0.0;
  } else if (u < bp[maxIndex]) {
    bpIndex = binsearch_u32d(u, bp, maxIndex >> 1U, maxIndex);
    *fraction = (u - bp[bpIndex]) / (bp[bpIndex + 1U] - bp[bpIndex]);
  } else {
    bpIndex = maxIndex;
    *fraction = 0.0;
  }

  return bpIndex;
}

static uint32_T binsearch_u32d(real_T u, const real_T bp[], uint32_T startIndex,
  uint32_T maxIndex)
{
  uint32_T bpIndex;
  uint32_T iRght;
  uint32_T bpIdx;
  bpIdx = startIndex;
  bpIndex = 0U;
  iRght = maxIndex;
  while (iRght - bpIndex > 1U) {
    if (u < bp[bpIdx]) {
      iRght = bpIdx;
    } else {
      bpIndex = bpIdx;
    }

    bpIdx = (iRght + bpIndex) >> 1U;
  }

  return bpIndex;
}

P rtP = {

  0.14,												// C

  500.0,											// CurDrctMax

  -500.0,											// CurDrctMin

  500.0,											// CurQuadMax

  -500.0,											// CurQuadMin

  300.0,											// CurrRef

  1.0,												// DcToAcPeakCon

  3.0,												// DertgFlgCanMax

  0.0,												// DertgFlgCanMin

  125.0,											// DertgPeakCur

  255.0,											// IndcrPwrResiCanMax

  0.0,												// IndcrPwrResiCanMin


  { 158.2688, 161.9313, 166.2851, 171.3368, 173.1768 }, // InvHeatPwrAt125A_M


  { 500.7972, 531.2445, 567.42, 609.7282, 625.2705 },	// InvHeatPwrAt300A_M

  577.0,											// P_ref

  1.0,												// PeakCurFilEna

  0.1,												// PeakCurFilTCOn

  0.01,												// PwrIntglLowThd

  20460.0,											// PwrIntglSatMaxThd

  0.135,											// R

#if CUSTOMER_ID == AUDI
  { 15.0, 15.0, 5.0, 0.1, 0.0, 0.0, 0.0 },			// TOff_M

  { 15.0, 15.0, 5.0, 0.1, 0.0, 0.0, 0.0 },			// TOn_M
#else
  { 8.0, 8.0, 8.0, 8.0, 6.0, 2.0, 0.2 },			// TOff_M

  { 4.0, 4.0, 4.0, 4.0, 2.0, 1.0, 0.1 },			// TOn_M
#endif /* CUSTOMER_ID */

  203.7,											// TToEmptyCanMax

  -1.0,												// TToEmptyCanMin

  203.7,											// TToFullCanMax

  -1.0,												// TToFullCanMin

  8.0,												// TpCooltCOn

  1.0,												// TpCooltFilEna

  200.0,											// TpCooltMax

  -20.0,											// TpCooltMin


  { 0.0, 30.0, 60.0, 90.0, 100.0 },					// TpCooltPwr_A


  { 0.0, 60.0, 85.0, 95.0, 105.0, 110.0, 115.0 },	// TpCooltTOff_A


  { 0.0, 60.0, 85.0, 95.0, 105.0, 110.0, 115.0 },	// TpCooltTOn_A

  255.5,											// TpMosfetCanMax

  0.0,												// TpMosfetCanMin

  140.0,											// TpMosfetHiThd

  120.0,											// TpMosfetLowThd

  3000.0,											// TpMosfetMax

  -20.0,											// TpMosfetMin

  145.0,											// TpRef

  0.45,												// a

  0.55,												// b

  0.0,												// Constant9_Value

  0.0,												// Constant3_Value

  -1.0,												// TtoEmpty_Y0

  0.0,												// Constant_Value

  -1.0,												// TtoFull_Y0

  0.0,												// Constant1_Value

  0.0,												// Constant_Value_d

  1.0E-5,											// Constant_Value_c

  0.001,											// WeightedSampleTimeMath_WtEt

  1.0,												// Constant1_Value_g

  0.0,												// UnitDelay_InitialCondition

  0.0,												// Delay3_InitialCondition

  1.0,												// Saturation2_UpperSat

  0.0,												// Saturation2_LowerSat

  0.0,												// UnitDelay_InitialCondition_l

  0.0,												// Delay1_InitialCondition

  1.0,												// Saturation1_UpperSat

  0.0,												// Saturation1_LowerSat

  2.0,												// Gain_Gain

  0.0,												// Constant1_Value_b

  1.0,												// Saturation_UpperSat

  0.0,												// Saturation_LowerSat

  1.0,												// Saturation1_UpperSat_g

  0.0,												// Saturation2_LowerSat_m

  1.0,												// Saturation2_UpperSat_h

  0.0,												// Saturation2_LowerSat_j

  100.0,											// Gain_Gain_g

  1.0E-5,											// Constant_Value_d0

  0.001,											// WeightedSampleTimeMath_WtEt_j

  1.0,												// Constant1_Value_f


  { 1.4E-5, 0.003593, 0.918127 },					// RdsON_Coefs


  { 1.4E-5, 0.003593, 0.918127 },					// RdsON1_Coefs

  0.0,												// Constant_Value_n

  0.001,											// WeightedSampleTimeMath_WtEt_a

  0.0,												// Saturation_LowerSat_j

  0.001,											// WeightedSampleTime_WtEt

  1U,												// Delay_DelayLength

  3U,												// InterpolationUsingPrelookup1_ma

  3U,												// InterpolationUsingPrelookup2_ma

  4U,												// InterpolationUsingPrelookup_max

  4U,												// InterpolationUsingPrelookup1__l

  1U,												// Delay3_DelayLength

  1U,												// Delay1_DelayLength

  1U												// Delay_DelayLength_c
};


DW rtDW = {
       -1,
       -1,
        0,
        0,
        0,
        0,
        0,
        0,
        1U,
        1U,
        0,
        0
};

  real_T testpar;
static void ThermProtn_Ecoe_2019a_step(void)
{ 

  real_T CcrPwrOfs;
  uint32_T rtb_Prelookup1_o1;
  real_T rtb_Add1_f;
   real_T rtb_Saturation3;
  real_T rtb_WeightedSampleTime;
  real_T rtb_Sum;
  real_T rtb_Saturation1;
  real_T rtb_Gain1_p;
  boolean_T rtb_RelationalOperator_f;
  real_T rtb_Avg;
  real_T rtb_Avg_m;
  real_T rtb_InterpolationUsingPrelookup;
  real_T rtb_InterpolationUsingPrelook_i;
  real_T rtb_InterpolationUsingPrelook_p;
  real_T u0;



 

 
  
  
 if (rtU.TpCoolt > rtP.TpCooltMax) {
    rtb_Saturation3 = rtP.TpCooltMax;
  } else if (rtU.TpCoolt < rtP.TpCooltMin) {
    rtb_Saturation3 = rtP.TpCooltMin;
  } else {
    rtb_Saturation3 = rtU.TpCoolt;
  }

 if (rtDW.icLoad != 0) {
    rtDW.Delay_DSTATE = rtb_Saturation3;
 }
  
 rtb_Add1_f = 1.0 / fmax(rtP.TpCooltCOn, rtP.Constant_Value_c);
   rtb_WeightedSampleTime = fmin(rtb_Add1_f * rtP.WeightedSampleTimeMath_WtEt,
    rtP.Constant1_Value_g);
    
  rtb_Avg = (1.0 - rtb_WeightedSampleTime) * rtDW.Delay_DSTATE +
    rtb_WeightedSampleTime * rtb_Saturation3;
 
  
  
  if (rtP.TpCooltFilEna != 0.0) {
    rtb_WeightedSampleTime = rtb_Avg;
  } else {
    rtb_WeightedSampleTime = rtb_Saturation3;
  }
  
 
  
    rtb_Prelookup1_o1 = plook_bincag(rtb_WeightedSampleTime, rtP.TpCooltTOn_A, NOF_TPCOOLT_TON_VALUES-1,
    &rtb_Add1_f);
  rtb_InterpolationUsingPrelookup = intrp1d_la(rtb_Prelookup1_o1, rtb_Add1_f,
    rtP.TOn_M, NOF_TON_VALUES-1);
  rtb_Prelookup1_o1 = plook_bincag(rtb_WeightedSampleTime, rtP.TpCooltTOff_A, NOF_TPCOOLT_TOFF_VALUES-1,
    &rtb_Add1_f);
  rtb_InterpolationUsingPrelook_i = intrp1d_la(rtb_Prelookup1_o1, rtb_Add1_f,
    rtP.TOff_M, NOF_TOFF_VALUES-1);
  rtb_Prelookup1_o1 = plook_binca(rtb_WeightedSampleTime, rtP.TpCooltPwr_A, NOF_TPCOOLT_PWR_VALUES-1,
    &rtb_Add1_f);
  rtb_InterpolationUsingPrelook_p = intrp1d_la(rtb_Prelookup1_o1, rtb_Add1_f,
    rtP.InvHeatPwrAt300A_M, NOF_INV_HEAT_PWR_AT_300A_VALUES-1);
  rtb_Prelookup1_o1 = plook_bincag(rtb_WeightedSampleTime, rtP.TpCooltPwr_A, NOF_TPCOOLT_PWR_VALUES-1,
    &rtb_Add1_f);
  rtb_Saturation3 = rtP.DertgPeakCur;
  rtb_InterpolationUsingPrelook_p = fmin(fmax(rtb_InterpolationUsingPrelook_p,
    1.0E-5), 100000.0);
  if (rtb_InterpolationUsingPrelook_i == 0.0) {
    rtb_InterpolationUsingPrelook_i = 0.001;
  }

  if (rtP.DertgPeakCur == 0.0) {
    rtb_Saturation3 = 0.001;
  }

  rtb_Sum = rtP.PwrIntglSatMaxThd / rtb_InterpolationUsingPrelook_p - 0.5;
  if (rtb_InterpolationUsingPrelookup >= rtb_Sum) {
    rtb_InterpolationUsingPrelookup = rtb_Sum;
  }

  rtb_InterpolationUsingPrelookup = (rtb_InterpolationUsingPrelookup *
    rtb_InterpolationUsingPrelook_p);
  
  
  
  
  if (rtb_InterpolationUsingPrelookup < 0.0) {
    rtb_InterpolationUsingPrelookup=-rtb_InterpolationUsingPrelookup;
  }


  
  CcrPwrOfs = -rtb_InterpolationUsingPrelookup / rtb_InterpolationUsingPrelook_i;
  if (rtDW.UnitDelay_DSTATE < rtP.PwrIntglLowThd) {
    rtb_Sum = rtP.Constant9_Value;
  } else {
    rtb_Sum = rtDW.Delay3_DSTATE;
  }

  rtb_InterpolationUsingPrelook_i = (real_T)(rtb_InterpolationUsingPrelookup <
    rtDW.UnitDelay_DSTATE) + rtb_Sum;
  if (rtb_InterpolationUsingPrelook_i > rtP.Saturation2_UpperSat) {
    rtb_InterpolationUsingPrelook_i = rtP.Saturation2_UpperSat;
  } else {
    if (rtb_InterpolationUsingPrelook_i < rtP.Saturation2_LowerSat) {
      rtb_InterpolationUsingPrelook_i = rtP.Saturation2_LowerSat;
    }
  }

  if (rtDW.UnitDelay_DSTATE_c > rtP.TpMosfetMax) {
    rtb_InterpolationUsingPrelook_p = rtP.TpMosfetMax;
  } else if (rtDW.UnitDelay_DSTATE_c < rtP.TpMosfetMin) {
    rtb_InterpolationUsingPrelook_p = rtP.TpMosfetMin;
  } else {
    rtb_InterpolationUsingPrelook_p = rtDW.UnitDelay_DSTATE_c;
  }

  rtb_Sum = rtb_WeightedSampleTime + rtb_InterpolationUsingPrelook_p;
  if (rtb_Sum < rtP.TpMosfetLowThd) {
    rtb_WeightedSampleTime = rtP.Constant3_Value;
  } else {
    rtb_WeightedSampleTime = rtDW.Delay1_DSTATE;
  }

  rtb_Saturation1 = (real_T)(rtP.TpMosfetHiThd < rtb_Sum) +
    rtb_WeightedSampleTime;
  if (rtb_Saturation1 > rtP.Saturation1_UpperSat) {
    rtb_Saturation1 = rtP.Saturation1_UpperSat;
  } else {
    if (rtb_Saturation1 < rtP.Saturation1_LowerSat) {
      rtb_Saturation1 = rtP.Saturation1_LowerSat;
    }
  }

  rtb_WeightedSampleTime = rtP.Gain_Gain * rtb_Saturation1 +
    rtb_InterpolationUsingPrelook_i;
  if (rtb_WeightedSampleTime > rtP.DertgFlgCanMax) {
    rtY.DertgFlg = rtP.DertgFlgCanMax;
  } else if (rtb_WeightedSampleTime < rtP.DertgFlgCanMin) {
    rtY.DertgFlg = rtP.DertgFlgCanMin;
  } else {
    rtY.DertgFlg = rtb_WeightedSampleTime;
  }

  if (rtb_WeightedSampleTime > rtP.Saturation1_UpperSat_g) {
    rtb_WeightedSampleTime = rtP.Saturation1_UpperSat_g;
  } else {
    if (rtb_WeightedSampleTime < rtP.Saturation1_LowerSat_m) {
      rtb_WeightedSampleTime = rtP.Saturation1_LowerSat_m;
    }
  }

  rtb_WeightedSampleTime = 1.0 - rtb_WeightedSampleTime;
  u0 = 1.0 - fmax(rtP.Constant1_Value_b, rtb_Sum - rtP.TpMosfetLowThd) /
    (rtP.TpMosfetHiThd - rtP.TpMosfetLowThd);
  rtb_Gain1_p = rtDW.UnitDelay_DSTATE / ((real_T)
    (rtb_InterpolationUsingPrelookup == 0.0) * -2.2204460492503131E-16 +
    rtb_InterpolationUsingPrelookup);
  if (u0 > rtP.Saturation_UpperSat) {
    u0 = rtP.Saturation_UpperSat;
  } else {
    if (u0 < rtP.Saturation_LowerSat) {
      u0 = rtP.Saturation_LowerSat;
    }
  }

  if (rtb_Gain1_p > rtP.Saturation2_UpperSat_h) {
    rtb_Gain1_p = rtP.Saturation2_UpperSat_h;
  } else {
    if (rtb_Gain1_p < rtP.Saturation2_LowerSat_j) {
      rtb_Gain1_p = rtP.Saturation2_LowerSat_j;
    }
  }

  u0 = fmin(fmin(u0, rtb_WeightedSampleTime), 1.0 - rtb_Gain1_p) *
    rtP.Gain_Gain_g;
  if (u0 > rtP.IndcrPwrResiCanMax) {
    rtY.IndcrPwrResi = rtP.IndcrPwrResiCanMax;
  } else if (u0 < rtP.IndcrPwrResiCanMin) {
    rtY.IndcrPwrResi = rtP.IndcrPwrResiCanMin;
  } else {
    rtY.IndcrPwrResi = u0;
  }

  if (rtU.CurDrct > rtP.CurDrctMax) {
    rtb_Gain1_p = rtP.CurDrctMax;
  } else if (rtU.CurDrct < rtP.CurDrctMin) {
    rtb_Gain1_p = rtP.CurDrctMin;
  } else {
    rtb_Gain1_p = rtU.CurDrct;
  }

  rtb_WeightedSampleTime = rtb_Gain1_p * rtb_Gain1_p;
  if (rtU.CurQuad > rtP.CurQuadMax) {
    rtb_Gain1_p = rtP.CurQuadMax;
  } else if (rtU.CurQuad < rtP.CurQuadMin) {
    rtb_Gain1_p = rtP.CurQuadMin;
  } else {
    rtb_Gain1_p = rtU.CurQuad;
  }

  rtb_Gain1_p = sqrt(rtb_Gain1_p * rtb_Gain1_p + rtb_WeightedSampleTime) *
    rtP.DcToAcPeakCon;
 if (rtDW.icLoad_c != 0) {
    rtDW.Delay_DSTATE_a = rtb_Gain1_p;
  }

  rtb_WeightedSampleTime = fmin(1.0 / fmax(rtP.PeakCurFilTCOn,
    rtP.Constant_Value_d0) * rtP.WeightedSampleTimeMath_WtEt_j,
    rtP.Constant1_Value_f);
  rtb_Avg_m = (1.0 - rtb_WeightedSampleTime) * rtDW.Delay_DSTATE_a +
    rtb_WeightedSampleTime * rtb_Gain1_p;
  if (rtP.PeakCurFilEna != 0.0) {
    rtb_WeightedSampleTime = rtb_Avg_m;
  } else {
    rtb_WeightedSampleTime = rtb_Gain1_p;
  }

  rtb_Gain1_p = 1.0 / rtP.CurrRef * rtb_WeightedSampleTime;
  rtb_Gain1_p = (((rtP.RdsON_Coefs[0] * rtb_Sum + rtP.RdsON_Coefs[1]) * rtb_Sum
                  + rtP.RdsON_Coefs[2]) / ((rtP.RdsON1_Coefs[0] * rtP.TpRef +
    rtP.RdsON1_Coefs[1]) * rtP.TpRef + rtP.RdsON1_Coefs[2]) * (rtb_Gain1_p *
    rtb_Gain1_p * rtP.a) + rtP.b * rtb_Gain1_p) * rtP.P_ref;
  if (rtb_WeightedSampleTime > rtP.DertgPeakCur) {
    rtb_WeightedSampleTime = rtb_Gain1_p;
  } else {
    rtb_WeightedSampleTime = fmin(fmax((-intrp1d_la(rtb_Prelookup1_o1,
      rtb_Add1_f, rtP.InvHeatPwrAt125A_M, NOF_INV_HEAT_PWR_AT_125A_VALUES-1) - CcrPwrOfs) / rtb_Saturation3,
      1.0E-5), 100000.0) * rtb_WeightedSampleTime + CcrPwrOfs;
  }

  rtb_Add1_f = (real_T)(rtb_WeightedSampleTime == 0.0) * -2.2204460492503131E-16
    + rtb_WeightedSampleTime;
  rtb_RelationalOperator_f = (rtb_WeightedSampleTime >= rtP.Constant_Value_n);
  if (!rtb_RelationalOperator_f) {
    if (!rtDW.Subsystem1_MODE) {
      rtDW.Subsystem1_MODE = true;
    }

    rtDW.Min = fmax((rtP.Constant1_Value - rtDW.UnitDelay_DSTATE) / rtb_Add1_f,
                    rtP.Constant_Value_d);
  } else {
    if (rtDW.Subsystem1_MODE) {
      rtDW.Min = rtP.TtoFull_Y0;
      rtDW.Subsystem1_MODE = false;
    }
  }

  if (rtDW.Min > rtP.TToFullCanMax) {
    rtY.TToFull = rtP.TToFullCanMax;
  } else if (rtDW.Min < rtP.TToFullCanMin) {
    rtY.TToFull = rtP.TToFullCanMin;
  } else {
    rtY.TToFull = rtDW.Min;
  }

  if (rtb_RelationalOperator_f) {
    if (!rtDW.Subsystem_MODE) {
      rtDW.Subsystem_MODE = true;
    }

    rtDW.Min_e = fmax((rtb_InterpolationUsingPrelookup - rtDW.UnitDelay_DSTATE) /
                      rtb_Add1_f, rtP.Constant_Value);
  } else {
    if (rtDW.Subsystem_MODE) {
      rtDW.Min_e = rtP.TtoEmpty_Y0;
      rtDW.Subsystem_MODE = false;
    }
  }

  if (rtDW.Min_e > rtP.TToEmptyCanMax) {
    rtY.TToEmpty = rtP.TToEmptyCanMax;
  } else if (rtDW.Min_e < rtP.TToEmptyCanMin) {
    rtY.TToEmpty = rtP.TToEmptyCanMin;
  } else {
    rtY.TToEmpty = rtDW.Min_e;
  }

  if (rtb_Sum > rtP.TpMosfetCanMax) {
    rtY.TpMosfet = rtP.TpMosfetCanMax;
  } else if (rtb_Sum < rtP.TpMosfetCanMin) {
    rtY.TpMosfet = rtP.TpMosfetCanMin;
  } else {
    rtY.TpMosfet = rtb_Sum;
  }

  rtDW.icLoad = 0U;
  rtDW.Delay_DSTATE = rtb_Avg;
  u0 = rtb_WeightedSampleTime * rtP.WeightedSampleTimeMath_WtEt_a +
    rtDW.UnitDelay_DSTATE;
  if (u0 > rtP.PwrIntglSatMaxThd) {
    rtDW.UnitDelay_DSTATE = rtP.PwrIntglSatMaxThd;
  } else if (u0 < rtP.Saturation_LowerSat_j) {
    rtDW.UnitDelay_DSTATE = rtP.Saturation_LowerSat_j;
  } else {
    rtDW.UnitDelay_DSTATE = u0;
  }

  rtDW.Delay3_DSTATE = rtb_InterpolationUsingPrelook_i;
  rtDW.UnitDelay_DSTATE_c = (rtP.R * rtb_Gain1_p -
    rtb_InterpolationUsingPrelook_p) * rtP.C * rtP.WeightedSampleTime_WtEt +
    rtb_InterpolationUsingPrelook_p;
  rtDW.Delay1_DSTATE = rtb_Saturation1;
  rtDW.icLoad_c = 0U;
  rtDW.Delay_DSTATE_a = rtb_Avg_m;
}


void ThermProtnEcoeDerating(void)
{
	if (KbCCC_DerateThermInput == TRUE)
	{
		rtU.TpCoolt = (real_T) GfCanTx_TempBdr2;
	}
	else
	{
		rtU.TpCoolt = (real_T) GfCAN_CoolantTemp;
	}
	rtU.CurDrct = GfMES_Idr;
	rtU.CurQuad = GfMES_Iqr;

	ThermProtn_Ecoe_2019a_step();

	GbCCC_DerateThermEstActv = rtY.DertgFlg ? TRUE : FALSE;
	GfCCC_InvTempEst02 = rtY.TpMosfet;
}
