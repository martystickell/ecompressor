/*
 * STM_OpenLoopTest.c
 *
 *  Created on: Oct 25, 2018
 *      Author: H247670
 */
#include	"STM.h"
#include	"GTM.h"
#include	"CCC.h"
#include	"MES.h"
#include	"SVM.h"
#include	"CAN_Driver.h"

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
// State machine
void STM_StandByT0(void)
{
	// Disable PWM.
	GTM_PWMDisable();

	// Clear failures if state is changed from Fault.
	if ((GeSTM_LastStateT0 == Fault) || (GeSTM_LastStateT0 == TurnOn) || (GeSTM_LastStateT0 == TurnOff))
	{
		// Reset SW faults
		STM_ResetT0Flt();
		STM_ResetT1Flt();
	}
	// If not, run T0 diagnostics.
	else
	{
		STM_DiagT0();
		STM_LatchingT0Flt();
	}

	// Triggering ADC queue 3 for Vabc reading
	vadc_trig_group0_q3();
	vadc_trig_group1_q3();

	// Reading ADC queue 0 & calculation
	ADC_ReadIabcVdc();
	MES_CalcIabcIdq();
	MES_CalcVdc();

	// Triggering ADC background scan for Temperature reading
	vadc_trig_group0_scan();
	vadc_trig_group1_scan();

	// Read ADC queue 3 & calculate
	ADC_ReadVabc();
	MES_CalcVabc();

	Gu32STM_RunStandStillCNT = 0;

	// Current error calculation
	GfCCC_ErrId = GfCCC_IdCmd - GfMES_Idr;
	GfCCC_ErrIq = GfCCC_IqCmd - GfMES_Iqr;

	if (GfMES_MagVdqFilt < KfCCC_LamPM * KfSTM_RunStandStillThrsRPM * RPM_TO_RADS)
	{
		GeSTM_RunSubStateNext = StandStill;
		GeSTM_RunSubState = StandStill;

		CCC_ResetFlxEst();
		CCC_ResetSpdEst();
		CCC_ResetSpdCntr();
		CCC_ResetFW();
		CCC_ResetCurCntr();

		GfCCC_VdCmd = 0.0;
		GfCCC_VqCmd = 0.0;

		GfCCC_Wr = 0.0;
		GfCCC_Wrpm = 0.0;
		GfCCC_Thetar = 0.0;
	}
	else
	{
		GfCCC_VdCmd = GfMES_Vdr;
		GfCCC_VqCmd = GfMES_Vqr;

		CCC_FlxEst();
		//CCC_ResetFlxEst();
		//GfCCC_ErrThetar = atan2f(GfMES_Vdr, GfMES_Vqr);
		//GfCCC_ErrThetar = atan2f(GfMES_Vqr, GfMES_Vdr);

		//CCC_SetFlxEst();
		CCC_SpdEst();

		GfCCC_Wr = GfCCC_WrEstNew;
		GfCCC_Wrpm = GfCCC_Wr * RADS_TO_RPM;
		GfCCC_Thetar = GfCCC_ThetarEstNew;

		if (GfCCC_Wr > MIN_DIVISION)
		{
			GfCCC_InvWr = 1.0 / GfCCC_Wr;
		}
		else
		{
			GfCCC_InvWr = INV_MIN_DIVISION;
		}

		if (GfCCC_Wrpm > KfSTM_RunCLtoOL)
		{
			GeSTM_RunSubStateNext = ClosedLoop;
			GeSTM_RunSubState = ClosedLoop;
		}
		else
		{
			GeSTM_RunSubStateNext = OpenLoop;
			GeSTM_RunSubState = OpenLoop;
		}

		CCC_ResetSpdCntr();
		CCC_ResetFW();
		CCC_SetCurCntr();
	}
}

void STM_StandByT1(void)
{
	// Read ADC background scan & calculate
	ADC_ReadT1Values();
	MES_CalcTemps();


	// Clear T1 failure if state is changed from Fault.
	if ((GeSTM_LastStateT1 == Fault) || (GeSTM_LastStateT1 == TurnOn) || (GeSTM_LastStateT1 == TurnOff))
	{
		STM_ResetT1Flt();
		GIO_EnblResetHWFaults();

		GbSTM_StatusFltReset = TRUE;
	}
	// If not, run T1 diagnostics.
	else
	{
		GIO_DisbResetHWFaults();

		STM_DiagT1();
		STM_ThermalWarning();
		STM_VoltageWarning();
		STM_LatchingT1Flt();

		GbSTM_StatusFltReset = FALSE;
	}

	// Calculate T0 period
	GTM_T0Calc();

	// CAN value reading
	CAN_CmdUpdate();

	// Control mode decision
	GeSTM_ControlMode = GeSTM_ControlModeCmd;

	// Reset speed command
	GfCCC_WrCmdSlew = GfCCC_Wr;
	GfCCC_WrpmCmdSlew = GfCCC_WrCmdSlew * RADS_TO_RPM;

	// Controller Re-initialization
	CCC_ReInitInvs();
	CCC_ReInitCurtCtrl();
	CCC_ReInitSpdCtrl();
	CCC_ReInitFlxEst();
	CCC_ReInitSpdEst();

	Gu16STM_RunClosedLoopCNT = 0;
	GbSTM_RunClosedLoopCNTDone = FALSE;

	// De-rating calculation
	CCC_Derating();

	// Calculate current limit.
	CCC_IdqCmdLim();

	// Reset dq current command.
	CCC_ResetCurCmd();

	// State transition
	if (GbSTM_StatusFltReset == FALSE)
	{
		if (GbSTM_FltT0 || GbSTM_FltT1)
		{
			GeSTM_NextStateT1 = Fault;
		}
		else if (GeSTM_StateCmd == Run)
		{
			GeSTM_NextStateT1 = Run;
		}
		else if (GeSTM_StateCmd == TurnOn)
		{
			GbMES_CurrentoffsetCalcDone = FALSE;
			GbMES_MotorRotDirDone = FALSE;

			GeSTM_NextStateT1 = TurnOn;
		}
		else
		{
			GeSTM_NextStateT1 = StandBy;
		}
	}
	else
	{
		GeSTM_NextStateT1 = StandBy;
	}
}
