/*
 * STM.c
 *
 *  Created on: Oct 15, 2018
 *      Author: H247670
 */
#include	"STM.h"
#include	"GTM.h"
#include	"CCC.h"
#include	"MES.h"
#include	"SVM.h"
#include	"CAN_Driver.h"

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/
// Open-loop control test mode
TestMode KeSTM_TestModeCmd = OpenLoopVoltage;

// Enabling open-loop manual-voltage set
boolean KbSTM_EnblManVdcOpenLoop = FALSE;

boolean KbSTM_SelfHealEnbl = TRUE;

// Manual voltage
boolean KfSTM_ManVdcOpenLoop = 48.0;

// Open-loop control voltage command Magnitude(V)
float KfSTM_MagVoltCmdOpenLoop = 0.0;

// Open-loop control voltage command Offset (V)
float KfSTM_VoltOffsetCmdOpenLoop = 0.0;

// Open-loop control voltage slew rate limit (V/s)
float KfSTM_MagVoltSlewLimOpenLoop = 20.0;

// Open-loop control voltage command max (V)
float KfSTM_MagVoltCmdOpenLoopMax = 27.71281292;

// Open-loop control voltage command min (V)
float KfSTM_MagVoltCmdOpenLoopMin = 0.0;

// Open-loop control speed command (r/min)
float KfSTM_WrpmOpenLoop = 0.0;

// Open-loop control speed slew limit (r/min/s)
float KfSTM_WrpmCmdOpenLoopSlew = 10000.0;

// Open-loop control speed Max. limit (r/min)
float KfSTM_WrpmOpenLoopMax = 10000.0;

// Open-loop control speed Min. limit (r/min)
float KfSTM_WrpmOpenLoopMin = 0.0;

// Open-loop control current command (A)
float KfSTM_IdCmdOpenLoop = 30.0;
float KfSTM_IqCmdOpenLoop = 0.0;

// Duty command for direct duty command mode
float KfSTM_DutyACmd = 0.5;
float KfSTM_DutyBCmd = 0.5;
float KfSTM_DutyCCmd = 0.5;

//State KeSTM_ManStateCmd = Off;
State KeSTM_ManStateCmd = StandBy;

// Control mode command
ControlMode KeSTM_ControlModeCmd = SpeedControl;
//ControlMode KeSTM_ControlModeCmd = TorqueControl;

// StandStill time limit
uint32 Ku32STM_RunStandStillCNTLim = 100;

// Run-closed loop state transition delay time
uint16 Ku16STM_RunClosedCNTSet = 100;

// State transition speed threshold for OL to CL (r/min)
float KfSTM_RunOLtoCL = 4900.0;

// State transition speed threshold for CL to OL (r/min)
float KfSTM_RunCLtoOL = 3000.0;

// Speed Command in torque control mode
float KfSTM_WrpmCmdPosTe = 6000.0;
float KfSTM_WrpmCmdNegTe = 1000.0;

// State transition speed threshold for StandStill (r/min)
float KfSTM_RunStandStillThrsRPM = 200.0;

// HW fault enable for SW test
boolean KbSTM_HWFltEnbl = TRUE;

// SW fault enable for SW test
boolean KbSTM_SWFltEnbl = TRUE;

// Phase current over-current level
float KfSTM_IsOCSet = 350.0;

// X-of-Y counter of IsOC
uint16 Ku16STM_IsOCXCNTSet = 3;
uint16 Ku16STM_IsOCYCNTSet = 10;

// phase current out-of-range level
float KfSTM_IsOORSet = 500.0;

// Phase current sum check
float KfSTM_IsSUMFltSet = 120.0;

// Vdc over-voltage level
float KfSTM_VdcOVSet = 57.0;
float KfSTM_VdcWarnSetHi = 52.0;
float KfSTM_VdcWarnClrHi = 51.9;
float KfSTM_VdcOV_Clear = 55.0;
float Ku8STM_VdcOV_CntFail = 1;
float Ku8STM_VdcOV_CntPass = 2;
float Ku8STM_VdcWarnCntHi = 50.0;

// Vdc under-voltage level
float KfSTM_VdcUVSet = 20.0;
float KfSTM_VdcWarnSetLo = 36.0;
float KfSTM_VdcWarnClrLo = 36.1;
float KfSTM_VdcUV_Clear = 21.0;
float Ku8STM_VdcUV_CntFail = 50;
float Ku8STM_VdcUV_CntPass = 100;
float Ku8STM_VdcWarnCntLo = 50.0;

// Vdc out-of-range level
float KfSTM_VdcOORSet = 100.0;

// Over-speed failure level
#if CUSTOMER_ID == FORD
float KfSTM_WrpmOSSet = 130000.0;
#else
float KfSTM_WrpmOSSet = 95000.0;
#endif /* CUSTOMER_ID */

// Motor speed out-of-range failure level
float KfSTM_WrpmOORSet = 200000.0;

// Motor speed lock failure: speed difference & Time duration
float KfSTM_WrpmLockDiffSet = 10000;
float KfSTM_WrpmLockTimeSet = 5;

// 12V over-voltage level
float KfSTM_V12OVSet = 18.0;
float KfSTM_V12_WarnSetHi = 16.0;
float KfSTM_V12_WarnClrHi = 15.0;
float KfSTM_V12OV_Clear = 16.0;
float Ku8STM_V12_OV_CntFail = 50;
float Ku8STM_V12_OV_CntPass = 100;
float Ku8STM_V12_WarnCntHi = 50.0;

// 12V under-voltage level
//float KfSTM_V12UVSet = 9.0;
float KfSTM_V12UVSet = 8.0;
float KfSTM_V12_WarnSetLo = 10.0;
float KfSTM_V12_WarnClrLo = 12.0;
float KfSTM_V12UV_Clear = 9.0;
float Ku8STM_V12_UV_CntFail = 50;
float Ku8STM_V12_UV_CntPass = 100;
float Ku8STM_V12_WarnCntLo = 50.0;

// 12V out-of-range level
float KfSTM_V12OORSet = 20.0;

// 5V over-voltage level
float KfSTM_V05OVSet = 6;

// 5V under-voltage level
float KfSTM_V05UVSet = 4;

// 5V out-of-range level
float KfSTM_V05OORSet = 7;

// IMS board over-temperature level
//float KfSTM_TempIMSOTSet = 170.0;
float KfSTM_TempIMSOTSet = 140.0;
float KfSTM_TempIMS_Warn = 125.0;
float KfSTM_TempIMSOT_Clear = 115.0;

uint8 Ku8STM_TempIMS_OT_CntFail = 50;
uint8 Ku8STM_TempIMS_OT_CntPass = 100;
uint8 Ku8STM_TempIMS_WarnCnt = 50;

// IMS board out-of-range level
//float KfSTM_TempIMSOORHiSet = 250.0;
//float KfSTM_TempIMSOORLoSet = -60;

float KfSTM_TempIMSOORHiSet = 500.0;
float KfSTM_TempIMSOORLoSet = -500;

// Temp. sens. 1 over-temperature level
//float KfSTM_TempSens1OTSet = 170.0;
float KfSTM_TempSens1OTSet = 140.0;

// Temp. sens. 1 out-of-range level
float KfSTM_TempSens1OORHiSet = 250.0;
float KfSTM_TempSens1OORLoSet = -60.0;

// Temp. sens. 2 over-temperature level
//float KfSTM_TempSens2OTSet = 170.0;
float KfSTM_TempSens2OTSet = 140.0;

// Temp. sens. 2 out-of-range level
float KfSTM_TempSens2OORHiSet = 250.0;
float KfSTM_TempSens2OORLoSet = -60;

// Temp. sens. 3 over-temperature level
//float KfSTM_TempSens3OTSet = 170.0;
float KfSTM_TempSens3OTSet = 140.0;

// Temp. sens. 3 out-of-range level
float KfSTM_TempSens3OORHiSet = 250.0;
float KfSTM_TempSens3OORLoSet = -60;

// Temp. board 1 over-temperature level
float KfSTM_TempBdr1OTSet = 124.0;
float KfSTM_TempBdr1OT_Warn = 120.0;
float KfSTM_TempBdr1OT_Clear = 110.0;
uint8 Ku8STM_TempBrd1_OT_CntFail = 50;
uint8 Ku8STM_TempBrd1_OT_CntPass = 50;

uint8 Ku8STM_BrdTempWarnCnt = 50;

// Temp. board 1 out-of-range level
float KfSTM_TempBdr1OORHiSet = 250.0;
float KfSTM_TempBdr1OORLoSet = -60;

// Temp. board 2 over-temperature level
float KfSTM_TempBdr2OTSet = 124.0;
float KfSTM_TempBdr2OT_Warn = 120.0;
float KfSTM_TempBdr2OT_Clear = 110.0;

uint8 Ku8STM_TempBrd2_OT_CntFail = 50;
uint8 Ku8STM_TempBrd2_OT_CntPass = 50;

// Temp. board 2 out-of-range level
float KfSTM_TempBdr2OORHiSet = 250.0;
float KfSTM_TempBdr2OORLoSet = -60;

// Motor over-temperature level
float KfSTM_TempMotOTSet = 170.0;

// Motor out-of-range level
float KfSTM_TempMotOORHiSet = 250.0;
float KfSTM_TempMotOORLoSet = -60;

// Under Temperature levels
float KfSTM_UnderTempSet = -30.0;
float KfSTM_UnderTempClr = -20.0;
uint8 Ku8STM_UnderTempCntFail = 50;
uint8 Ku8STM_UnderTempCntPass = 100;


// LOT diagnostics threshold (Apk)
float KfSTM_ErrIdqLimit = 100.0;

// LOT diagnostics counter threshold (1 CNT = 1ms)
uint16 Ku16STM_LOTCNTSet = 500;
uint16 Ku16STM_LOTCNTMax = 550;

// Ext Coolant Temp.  over-temperature level
float KfSTM_ExtCoolTempOT_Set = 95.0;
float KfSTM_ExtCoolTempOT_Warn = 80.0;
float KfSTM_ExtCoolTempOT_Clear = 75.0;
uint8 Ku8STM_ExtCoolTempOT_CntFail = 50;
uint8 Ku8STM_ExtCoolTempOT_CntPass = 100;
uint8 Ku8STM_ExtCoolTempWarnCnt = 50;

// Ext Coolant Temp.  Under-temperature level
float KfSTM_ExtCoolTempUT_Set = -30.0;
float KfSTM_ExtCoolTempUT_Clear = -20.0;
uint8 Ku8STM_ExtCoolTempUT_CntFail = 50;
uint8 Ku8STM_ExtCoolTempUT_CntPass = 100;

// Compressor Inlet Temp.  over-temperature level
float KfSTM_InletTempOT_Set = 160.0;
float KfSTM_InletTempOT_Warn = 140.0;
float KfSTM_InletTempOT_Clear = 135.0;
uint8 Ku8STM_InletTempOT_CntFail = 50;
uint8 Ku8STM_InletTempOT_CntPass = 100;
uint8 Ku8STM_InletTempWarnCnt = 50;

// SW version
uint16 Ku16STM_SWVersion = 107;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
// State Command
State GeSTM_StateCmd = StandBy;

State GeSTM_LastStateT0 = Off;
State GeSTM_CurrentStateT0 = Off;
State GeSTM_NextStateT0 = Off;

State GeSTM_LastStateT1 = Off;
State GeSTM_CurrentStateT1 = Off;
State GeSTM_NextStateT1 = Off;

// SubState of RunState
RunSubState GeSTM_RunSubState = StandStill;
RunSubState GeSTM_RunSubStateNext = StandStill;

// Control mode - speed or torque control
ControlMode GeSTM_ControlModeCmd = SpeedControl;
ControlMode GeSTM_ControlMode = SpeedControl;

// CNT variable in StandStill state
volatile uint32 Gu32STM_RunStandStillCNT = 0;

// StandStill state ready flag
volatile boolean GbSTM_RunStandStillDone = FALSE;

// State transition delay counter in Run-closed loop state
volatile uint16 Gu16STM_RunClosedLoopCNT = 0;

// Run-closed loop state transition delay count set flag
volatile boolean GbSTM_RunClosedLoopCNTDone = FALSE;

// Open loop frequency command (slew-rate limited)
volatile float GfSTM_fCmdOpenLoop = 0.0;

// T0 Start Counter
volatile uint32 Gu32STM_T0Cnt0 = 0;

// T0 End Counter
volatile uint32 Gu32STM_T0Cnt1 = 0;

// T0 Throughput counter
volatile uint32 Gu32STM_T0ThruPt = 0;

// T2 Start Counter
volatile uint32 Gu32STM_T2Cnt0 = 0;

// T2 End Counter
volatile uint32 Gu32STM_T2Cnt1 = 0;

// T2 Throughput counter
volatile uint32 Gu32STM_T2ThruPt = 0;


// T0 HW failure indicator
volatile boolean GbSTM_HWFltT0 = FALSE;

// T0 SW failure indicator
volatile boolean GbSTM_SWFltT0 = FALSE;

// T0 failure indicator
volatile boolean GbSTM_FltT0 = FALSE;

// T1 failure indicator
volatile boolean GbSTM_FltT1 = FALSE;

// Status of Fault Reset
volatile boolean GbSTM_StatusFltReset = FALSE;

// HW failure bits
volatile boolean GbSTM_IabcOCHW = FALSE;
volatile boolean GbSTM_VdcOVHW = FALSE;

// Phase current over-current failure bits
volatile boolean GbSTM_IaOCSW = FALSE;
volatile boolean GbSTM_IbOCSW = FALSE;
volatile boolean GbSTM_IcOCSW = FALSE;

// Phase current out-of-range failure bits
volatile boolean GbSTM_IaOORSW = FALSE;
volatile boolean GbSTM_IbOORSW = FALSE;
volatile boolean GbSTM_IcOORSW = FALSE;

// Phase current sum check failure bits
volatile boolean GbSTM_IsSUMFltSW = FALSE;

// Vdc over-voltage failure bit
volatile boolean GbSTM_VdcOVSW = FALSE;

// Vdc under-voltage failure bit
volatile boolean GbSTM_VdcUVSW = FALSE;

// Vdc out-of-range failure bit
volatile boolean GbSTM_VdcOORSW = FALSE;

// Over-speed failure bit
volatile boolean GbSTM_WrpmOSSW = FALSE;

// Motor speed out-of-range failure bit
volatile boolean GbSTM_WrpmOORSW = FALSE;

// Motor speed lock failure bit
volatile boolean GbSTM_WrpmLockSW = FALSE;

// 12V over-voltage failure bits
volatile boolean GbSTM_V12OVSW = FALSE;

// 12V under-voltage failure bit
volatile boolean GbSTM_V12UVSW = FALSE;

// 12V out-of-range failure bits
volatile boolean GbSTM_V12OORSW = FALSE;

// 5V over-voltage failure bits
volatile boolean GbSTM_V05OVSW = FALSE;

// 5V under-voltage failure bit
volatile boolean GbSTM_V05UVSW = FALSE;

// 5V out-of-range failure bits
volatile boolean GbSTM_V05OORSW = FALSE;

// IMS temp. over-temperature failure bit
volatile boolean GbSTM_TempIMSOTSW = FALSE;

// IMS temp. out-of-range failure bits
volatile boolean GbSTM_TempIMSOORSW = FALSE;

// Temp. sens. 1 over-temperature failure bit
volatile boolean GbSTM_TempSens1OTSW = FALSE;

// Temp. sens. 1 out-of-range failure bits
volatile boolean GbSTM_TempSens1OORSW = FALSE;

// Temp. sens. 2 over-temperature failure bit
volatile boolean GbSTM_TempSens2OTSW = FALSE;

// Temp. sens. 2 out-of-range failure bits
volatile boolean GbSTM_TempSens2OORSW = FALSE;

// Temp. sens. 3 over-temperature failure bit
volatile boolean GbSTM_TempSens3OTSW = FALSE;

// Temp. sens. 3 out-of-range failure bits
volatile boolean GbSTM_TempSens3OORSW = FALSE;

// Temp. board 1 over-temperature failure bit
volatile boolean GbSTM_TempBdr1OTSW = FALSE;

// Temp. board 1 out-of-range failure bits
volatile boolean GbSTM_TempBdr1OORSW = FALSE;

// Temp. board 2 over-temperature failure bit
volatile boolean GbSTM_TempBdr2OTSW = FALSE;

// Temp. board 2 out-of-range failure bits
volatile boolean GbSTM_TempBdr2OORSW = FALSE;

// Motor over-temperature failure bit
volatile boolean GbSTM_TempMotOTSW = FALSE;

// Motor out-of-range failure bits
volatile boolean GbSTM_TempMotOORSW = FALSE;

// Under Temperature failure bits
boolean GbSTM_UnderTempSWT1 = FALSE;
boolean GbSTM_UnderTempSW = FALSE;
uint8 Gu8STM_UnderTempFailCntr = 0;
uint8 Gu8STM_UnderTempPassCntr = 0;

// Motor LOT failure bit
volatile boolean GbSTM_LOTSW = FALSE;

// Coolant Temp over-temperature failure bit
volatile boolean GbSTM_ExtCoolTempOTSW = FALSE;

// Coolant Temp Under-temperature failure bit
volatile boolean GbSTM_ExtCoolTempUTSW = FALSE;

// Compressor Inlet Temp. over-temperature failure bit
volatile boolean GbSTM_InletTempOTSW = FALSE;

// CMD1 CAN timeout Fault
volatile boolean GbSTM_CANCMD1TimeOutSW = FALSE;

// CMD0 CAN timeout Fault
volatile boolean GbSTM_CANCMD0TimeOutSW = FALSE;

// Board Power Fault
volatile boolean GbSTM_BrdPwrFltSW = FALSE;

// For instantaneous fault sampling
// T0 HW failure indicator
volatile boolean GbSTM_HWFltT0T0 = FALSE;

// T0 SW failure indicator
volatile boolean GbSTM_SWFltT0T0 = FALSE;

// T0 failure indicator
volatile boolean GbSTM_FltT0T0 = FALSE;

// T1 failure indicator
volatile boolean GbSTM_FltT1T1 = FALSE;

// HW failure bits
volatile boolean GbSTM_IabcOCHWT0 = FALSE;
volatile boolean GbSTM_VdcOVHWT0 = FALSE;

// Phase current over-current failure bits
volatile boolean GbSTM_IaOCSWT0 = FALSE;
volatile boolean GbSTM_IbOCSWT0 = FALSE;
volatile boolean GbSTM_IcOCSWT0 = FALSE;

// X-of-Y counter for OC check
volatile uint16 Gu16STM_IsOCYCNT = 0;
volatile uint16 Gu16STM_IaOCXCNT = 0;
volatile uint16 Gu16STM_IbOCXCNT = 0;
volatile uint16 Gu16STM_IcOCXCNT = 0;

// Phase current out-of-range failure bits
volatile boolean GbSTM_IaOORSWT0 = FALSE;
volatile boolean GbSTM_IbOORSWT0 = FALSE;
volatile boolean GbSTM_IcOORSWT0 = FALSE;

// Phase current sum check failure bits
volatile boolean GbSTM_IsSUMFltSWT0 = FALSE;

// Vdc over-voltage failure bit
volatile boolean GbSTM_VdcOVSWT1 = FALSE;

// Vdc under-voltage failure bit
volatile boolean GbSTM_VdcUVSWT1 = FALSE;

// Vdc out-of-range failure bit
volatile boolean GbSTM_VdcOORSWT1 = FALSE;

volatile uint8 Gu8STM_VdcOV_FailCntr = (uint8)0;
volatile uint8 Gu8STM_VdcOV_PassCntr = (uint8)0;
volatile uint8 Gu8STM_VdcUV_FailCntr = (uint8)0;
volatile uint8 Gu8STM_VdcUV_PassCntr = (uint8)0;

// Over-speed failure bit
volatile boolean GbSTM_WrpmOSSWT0 = FALSE;

// Motor speed out-of-range failure bit
volatile boolean GbSTM_WrpmOORSWT0 = FALSE;

// Motor speed lock failure bit
volatile boolean GbSTM_WrpmLockSWT0 = FALSE;

// 12V over-voltage failure bits
volatile boolean GbSTM_V12OVSWT1 = FALSE;

// 12V under-voltage failure bit
volatile boolean GbSTM_V12UVSWT1 = FALSE;

// 12V out-of-range failure bits
volatile boolean GbSTM_V12OORSWT1 = FALSE;

volatile uint8 Gu8STM_V12_OV_FailCntr = (uint8)0;
volatile uint8 Gu8STM_V12_OV_PassCntr = (uint8)0;
volatile uint8 Gu8STM_V12_UV_FailCntr = (uint8)0;
volatile uint8 Gu8STM_V12_UV_PassCntr = (uint8)0;

// 5V over-voltage failure bits
volatile boolean GbSTM_V05OVSWT0 = FALSE;

// 5V under-voltage failure bit
volatile boolean GbSTM_V05UVSWT0 = FALSE;

// 5V out-of-range failure bits
volatile boolean GbSTM_V05OORSWT0 = FALSE;

// IMS temp. over-temperature failure bit
volatile boolean GbSTM_TempIMSOTSWT1 = FALSE;

volatile uint8 Gu8STM_TempIMS_OT_FailCntr = (uint8)0;
volatile uint8 Gu8STM_TempIMS_OT_PassCntr = (uint8)0;

// IMS temp. out-of-range failure bits
volatile boolean GbSTM_TempIMSOORSWT1 = FALSE;

// Temp. sens. 1 over-temperature failure bit
volatile boolean GbSTM_TempSens1OTSWT1 = FALSE;

// Temp. sens. 1 out-of-range failure bits
volatile boolean GbSTM_TempSens1OORSWT1 = FALSE;

// Temp. sens. 2 over-temperature failure bit
volatile boolean GbSTM_TempSens2OTSWT1 = FALSE;

// Temp. sens. 2 out-of-range failure bits
volatile boolean GbSTM_TempSens2OORSWT1 = FALSE;

// Temp. sens. 3 over-temperature failure bit
volatile boolean GbSTM_TempSens3OTSWT1 = FALSE;

// Temp. sens. 3 out-of-range failure bits
volatile boolean GbSTM_TempSens3OORSWT1 = FALSE;

// Temp. board 1 over-temperature failure bit
volatile boolean GbSTM_TempBdr1OTSWT1 = FALSE;

// Temp. board 1 out-of-range failure bits
volatile boolean GbSTM_TempBdr1OORSWT1 = FALSE;

// Temp. board 2 over-temperature failure bit
volatile boolean GbSTM_TempBdr2OTSWT1 = FALSE;

// Temp. board 2 out-of-range failure bits
volatile boolean GbSTM_TempBdr2OORSWT1 = FALSE;

volatile uint8 Gu8STM_TempBrd1_OT_FailCntr = (uint8)0;
volatile uint8 Gu8STM_TempBrd1_OT_PassCntr = (uint8)0;

volatile uint8 Gu8STM_TempBrd2_OT_FailCntr = (uint8)0;
volatile uint8 Gu8STM_TempBrd2_OT_PassCntr = (uint8)0;

// Motor over-temperature failure bit
volatile boolean GbSTM_TempMotOTSWT1 = FALSE;

// Motor out-of-range failure bit
volatile boolean GbSTM_TempMotOORSWT1 = FALSE;

// Motor LOT failure bit
volatile boolean GbSTM_LOTSWT1 = FALSE;

// Motor LOT counter
volatile uint16 Gu16STM_LOTCNT = 0;

// Ext Coolant Temp over-temperature failure bit
volatile boolean GbSTM_ExtCoolTempOTSWT1 = FALSE;

volatile uint8 Gu8STM_ExtCoolTempOT_FailCntr = (uint8)0;
volatile uint8 Gu8STM_ExtCoolTempOT_PassCntr = (uint8)0;

// Ext Coolant Temp under-temperature failure bit
volatile boolean GbSTM_ExtCoolTempUTSWT1 = FALSE;

volatile uint8 Gu8STM_ExtCoolTempUT_FailCntr = (uint8)0;
volatile uint8 Gu8STM_ExtCoolTempUT_PassCntr = (uint8)0;

// Compressor Inlet Temp over-temperature failure bit
volatile boolean GbSTM_InletTempOTSWT1 = FALSE;
volatile uint8 Gu8STM_InletTempOT_FailCntr = (uint8)0;
volatile uint8 Gu8STM_InletTempOT_PassCntr = (uint8)0;

volatile uint8 Gu8STM_BrdTempWarnCntr = (uint8)0;
volatile uint8 Gu8STM_BrdTempNoWarnCntr = (uint8)0;
volatile uint8 Gu8STM_TempIMS_WarnCntr = (uint8)0;
volatile uint8 Gu8STM_TempIMS_NoWarnCntr = (uint8)0;
volatile uint8 Gu8STM_ExtCoolTempWarnCntr = (uint8)0;
volatile uint8 Gu8STM_ExtCoolTempNoWarnCntr = (uint8)0;
volatile uint8 Gu8STM_InletTempWarnCntr = (uint8)0;
volatile uint8 Gu8STM_InletTempNoWarnCntr = (uint8)0;

volatile uint8 Gu8STM_V12_WarnCntrHi = (uint8)0;
volatile uint8 Gu8STM_V12_NoWarnCntrHi = (uint8)0;

volatile uint8 Gu8STM_V12_WarnCntrLo = (uint8)0;
volatile uint8 Gu8STM_V12_NoWarnCntrLo = (uint8)0;
volatile uint8 Gu8STM_VdcWarnCntrHi = (uint8)0;
volatile uint8 Gu8STM_VdcNoWarnCntrHi = (uint8)0;

volatile uint8 Gu8STM_VdcWarnCntrLo = (uint8)0;
volatile uint8 Gu8STM_VdcNoWarnCntrLo = (uint8)0;


volatile boolean GbSTM_BrdTempWarn = FALSE;

volatile boolean GbSTM_TempIMS_Warn = FALSE;

volatile boolean GbSTM_ExtCoolTempWarn = FALSE;

volatile boolean GbSTM_InletTempWarn = FALSE;

volatile boolean GbSTM_V12_WarnHi = FALSE;

volatile boolean GbSTM_V12_WarnLo = FALSE;

volatile boolean GbSTM_VdcWarnHi = FALSE;

volatile boolean GbSTM_VdcWarnLo = FALSE;

volatile boolean GbSTM_SelfHealCheckT1 = FALSE;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
#pragma section code "ISR_RAMCODE"
void STM_OffT0(void)
{
	// Disable PWM.
	GTM_PWMDisable();
}

void STM_TurnOnT0(void)
{
	// Disable PWM.
	GTM_PWMDisable();

	// Calculate current offset
	if ((GbMES_CurrentoffsetCalcDone == FALSE) && (GbMES_MotorRotDirDone == TRUE))
	{
		ADC_ReadIabcVdc();
		MES_CalcIabcOffset();
	}
}

void STM_TurnOffT0(void)
{
	// Disable PWM.
	GTM_PWMDisable();
}

void STM_FaultT0(void)
{
	// Disable PWM.
	GTM_PWMDisable();

	// Triggering ADC queue 3 for Vabc reading
	vadc_trig_group0_q3();
	vadc_trig_group1_q3();

	// Reading ADC queue 0 & calculation
	ADC_ReadIabcVdc();
	MES_CalcIabcIdq();
	MES_CalcVdc();

	// Triggering ADC background scan for Temperature reading
	vadc_trig_group0_scan();
	vadc_trig_group1_scan();

	// CAN value reading
	CAN_CmdUpdate();

	// Run Diagnostics
	STM_DiagT0();

	// Latch faults
	STM_LatchingT0Flt();

	// Read ADC queue 3 & calculate
	ADC_ReadVabc();
	MES_CalcVabc();

	// Current error calculation
	GfCCC_ErrId = GfCCC_IdCmd - GfMES_Idr;
	GfCCC_ErrIq = GfCCC_IqCmd - GfMES_Iqr;

	if (GfMES_MagVdqFilt < KfCCC_LamPM * KfSTM_RunStandStillThrsRPM * RPM_TO_RADS)
	{
		GeSTM_RunSubStateNext = StandStill;

		GfCCC_VdCmd = 0.0;
		GfCCC_VqCmd = 0.0;

		CCC_ResetFlxEst();
		CCC_ResetSpdEst();

		GfCCC_Wr = 0.0;
		GfCCC_Wrpm = 0.0;
		GfCCC_Thetar = 0.0;
	}
	else
	{
		CCC_SetFlxEst();
		CCC_SpdEst();

		GfCCC_Wr = GfCCC_WrEstNew;
		GfCCC_Wrpm = GfCCC_Wr * RADS_TO_RPM;
		GfCCC_Thetar = GfCCC_ThetarEstNew;

		if (GfCCC_Wr > MIN_DIVISION)
		{
			GfCCC_InvWr = 1.0 / GfCCC_Wr;
		}
		else
		{
			GfCCC_InvWr = INV_MIN_DIVISION;
		}

		if (GfCCC_Wrpm > KfSTM_RunCLtoOL)
		{
			GeSTM_RunSubStateNext = ClosedLoop;
		}
		else
		{
			GeSTM_RunSubStateNext = OpenLoop;
		}

		CCC_ResetSpdCntr();
		CCC_ResetFW();
		CCC_SetCurCntr();
	}
}


void STM_OffT1(void)
{
	// Reset current offset calculation
	MES_ResetIabcOffset();

	// Reset Rotation Direction
	GbMES_MotorRotDirDone = FALSE;

	// State transition
	if ((GeSTM_StateCmd == TurnOn) || (GeSTM_StateCmd == StandBy) || (GeSTM_StateCmd == Run))
	{
		GeSTM_NextStateT1 = TurnOn;
	}
	else
	{
		GeSTM_NextStateT1 = Off;
	}
}

void STM_TurnOnT1(void)
{
	// Reset derating
	CCC_ResetDerating();

	// CAN value reading
	CAN_CmdUpdate();

   // Selects Rotation Direction via HWIO Pin
	if (GbMES_MotorRotDirDone == FALSE)
	{
	MES_CaptureRotationDirection();
	}

	// State transition
	if ((GbMES_CurrentoffsetCalcDone == TRUE) && (GbMES_MotorRotDirDone == TRUE) && (GeSTM_StateCmd == StandBy))
	{
		GeSTM_NextStateT1 = StandBy;
	}
	else
	{
		GeSTM_NextStateT1 = TurnOn;
	}
}

void STM_TurnOffT1(void)
{
	if (GeSTM_StateCmd == Off)
	{
		GeSTM_NextStateT1 = Off;
	}
	else
	{
		GeSTM_NextStateT1 = TurnOff;
	}
}

void STM_FaultT1(void)
{
	// Read ADC background scan & calculate
	ADC_ReadT1Values();
	MES_CalcTemps();

	// Run T1 diagnostics
	STM_DiagT1();
	STM_ThermalWarning();
	STM_VoltageWarning();

	// Latching T1 faults
	STM_LatchingT1Flt();

	// Self-healing condition check
	STM_SelfHealCondChkT1();

	// Calculate T0
	GTM_T0Calc();

	// De-rating calculation
	CCC_Derating();

	// Calculate current limit.
	CCC_IdqCmdLim();

	// Reset dq current command.
	CCC_ResetCurCmd();

//	if ((GbSTM_FltT1T1 == TRUE)) || (GbSTM_FltT0T0 == TRUE))

	if ((GbSTM_FltT1T1 == TRUE) || (GbSTM_SWFltT0T0 == TRUE))
	{
		if ((KbSTM_SelfHealEnbl == TRUE) &&
			(GbSTM_SelfHealCheckT1 == TRUE))
		{
			GeSTM_NextStateT1 = StandBy;
		}
		else
		{
			GeSTM_NextStateT1 = Fault;
		}
	}
	else
	{
		if ((KbSTM_SelfHealEnbl == TRUE) &&
			(GbSTM_SelfHealCheckT1 == TRUE))
		{
			GeSTM_NextStateT1 = StandBy;
		}
		else
		{
			if (GeSTM_StateCmd == StandBy)
			{
				GeSTM_NextStateT1 = StandBy;
			}
			else
			{
				GeSTM_NextStateT1 = Fault;
			}
		}
	}
}

void STM_OffT2(void)
{

}

void STM_TurnOnT2(void)
{

}

void STM_StandByT2(void)
{

}

void STM_RunT2(void)
{

}

void STM_TurnOffT2(void)
{

}

void STM_FaultT2(void)
{

}
