/*
 * CCC_SpdEst.c
 *
 *  Created on: Oct 18, 2018
 *      Author: H247670
 */

#include 	"CCC.h"
#include 	"vadc.h"
#include 	"GTM.h"
#include	"MES.H"

// Flux estimator
void CCC_FlxEst(void)
{
	float LfCCC_ErrThetarTmp = 0.0;

	GfCCC_K12FlxEst = GfCCC_Wr;
	GfCCC_K21FlxEst = -GfCCC_Wr;

	GfCCC_IdEstNew = GfCCC_IdEst + GfGTM_T0 * (-KfCCC_Rs * GfCCC_InvLd * GfCCC_IdEst + GfCCC_Wr * GfCCC_IqEst + GfCCC_InvLd * GfCCC_EdEst + GfCCC_InvLd * GfCCC_VdCmd + GfCCC_K11FlxEst * (GfMES_IdrOld - GfCCC_IdEst) + GfCCC_K12FlxEst * (GfMES_IqrOld - GfCCC_IqEst));
    GfCCC_IqEstNew = GfCCC_IqEst + GfGTM_T0 * (-GfCCC_Wr * GfCCC_IdEst - KfCCC_Rs * GfCCC_InvLq * GfCCC_IqEst - GfCCC_InvLq * GfCCC_EqEst + GfCCC_InvLq * GfCCC_VqCmd + GfCCC_K21FlxEst * (GfMES_IdrOld - GfCCC_IdEst) + GfCCC_K22FlxEst * (GfMES_IqrOld - GfCCC_IqEst));

    GfCCC_EdEstNew = GfCCC_EdEst + GfGTM_T0 * (GfCCC_K31FlxEst * (GfMES_IdrOld - GfCCC_IdEst));
	GfCCC_EqEstNew = GfCCC_EqEst + GfGTM_T0 * (GfCCC_K42FlxEst * (GfMES_IqrOld - GfCCC_IqEst));

	LfCCC_ErrThetarTmp = atan2f(GfCCC_EdEstNew, GfCCC_EqEstNew);

	if (LfCCC_ErrThetarTmp > PI_OVER_TWO)
	{
		GfCCC_ErrThetar = PI_OVER_TWO;
	}
	else if (LfCCC_ErrThetarTmp < -PI_OVER_TWO)
	{
		GfCCC_ErrThetar = -PI_OVER_TWO;
	}
	else
	{
		GfCCC_ErrThetar = LfCCC_ErrThetarTmp;
	}

	GfCCC_IdEst = GfCCC_IdEstNew;
    GfCCC_IqEst = GfCCC_IqEstNew;

    GfCCC_EdEst = GfCCC_EdEstNew;
	GfCCC_EqEst = GfCCC_EqEstNew;
}

// Flux estimator reset
void CCC_ResetFlxEst(void)
{
	GfCCC_IdEstNew = 0.0;
	GfCCC_IqEstNew = 0.0;
	GfCCC_IdEst = 0.0;
	GfCCC_IqEst = 0.0;

	GfCCC_EdEstNew = 0.0;
	GfCCC_EqEstNew = 0.0;
	GfCCC_EdEst = 0.0;
	GfCCC_EqEst = 0.0;
}

// Flux estimator set
void CCC_SetFlxEst(void)
{
	GfCCC_IdEstNew = GfMES_Idr;
	GfCCC_IqEstNew = GfMES_Iqr;
	GfCCC_IdEst = GfMES_Idr;
	GfCCC_IqEst = GfMES_Iqr;

	GfCCC_EdEstNew = GfMES_Vdr;
	GfCCC_EqEstNew = GfMES_Vqr;
	GfCCC_EdEst = GfMES_Vdr;
	GfCCC_EqEst = GfMES_Vqr;

	GfCCC_ErrThetar = atan2f(GfMES_Vqr, GfMES_Vdr);
}

// Speed estimator
void CCC_SpdEst(void)
{
	float LfCCC_ThetarEstNewTmp = 0.0;
	float LfCCC_delAl = 0.0;
	float LfCCC_delWr = 0.0;
	float LfCCC_delThetar = 0.0;

	/*
	GfCCC_AlEstNew = GfCCC_AlEst + GfGTM_T0 * GfCCC_K1SpdEst * GfCCC_ErrThetar;
	GfCCC_WrEstNew = GfCCC_WrEst + GfGTM_T0 * GfCCC_InvJm * GfCCC_AlEst + (GfCCC_K1SpdEst * GfGTM_T0 * GfGTM_T0 * GfCCC_InvJm + GfCCC_K2SpdEst * GfGTM_T0) * GfCCC_ErrThetar;
	LfCCC_ThetarEstNewTmp = GfCCC_ThetarEst + GfGTM_T0 * GfGTM_T0 * GfCCC_InvJm * GfCCC_AlEst + GfGTM_T0 * GfCCC_WrEst + (GfGTM_T0 * GfCCC_K1SpdEst * GfGTM_T0 * GfCCC_InvJm * GfGTM_T0 + GfGTM_T0 * GfCCC_K2SpdEst * GfGTM_T0 + GfCCC_K3SpdEst * GfGTM_T0) * GfCCC_ErrThetar;

	GfCCC_ThetarEstNew = CCC_ThetarLim(LfCCC_ThetarEstNewTmp);
	*/

	LfCCC_delAl = GfGTM_T0 * GfCCC_K1SpdEst * GfCCC_ErrThetar;

	if (LfCCC_delAl > KfCCC_delAlMax)
	{
		GfCCC_AlEstNew = GfCCC_AlEst + KfCCC_delAlMax;
	}
	else if (LfCCC_delAl < -KfCCC_delAlMax)
	{
		GfCCC_AlEstNew = GfCCC_AlEst - KfCCC_delAlMax;
	}
	else
	{
		GfCCC_AlEstNew = GfCCC_AlEst + LfCCC_delAl;
	}

	LfCCC_delWr = GfGTM_T0 * GfCCC_InvJm * (GfCCC_AlEst + GfCCC_TeCmd) + (GfCCC_K1SpdEst * GfGTM_T0 * GfGTM_T0 * GfCCC_InvJm + GfCCC_K2SpdEst * GfGTM_T0) * GfCCC_ErrThetar;

	if (LfCCC_delWr > KfCCC_delWrMax)
	{
		GfCCC_WrEstNew = GfCCC_WrEst + KfCCC_delWrMax;
	}
	else if (LfCCC_delWr < -KfCCC_delWrMax)
	{
		GfCCC_WrEstNew = GfCCC_WrEst - KfCCC_delWrMax;
	}
	else
	{
		GfCCC_WrEstNew = GfCCC_WrEst + LfCCC_delWr;
	}

	LfCCC_delThetar = GfGTM_T0 * GfGTM_T0 * GfCCC_InvJm * (GfCCC_AlEst + GfCCC_TeCmd) + GfGTM_T0 * GfCCC_WrEst + (GfGTM_T0 * GfCCC_K1SpdEst * GfGTM_T0 * GfCCC_InvJm * GfGTM_T0 + GfGTM_T0 * GfCCC_K2SpdEst * GfGTM_T0 + GfCCC_K3SpdEst * GfGTM_T0) * GfCCC_ErrThetar;

	if (LfCCC_delThetar > KfCCC_delThetarMax)
	{
		LfCCC_ThetarEstNewTmp = GfCCC_ThetarEst + KfCCC_delThetarMax;
	}
	else if (LfCCC_delThetar < -KfCCC_delThetarMax)
	{
		LfCCC_ThetarEstNewTmp = GfCCC_ThetarEst - KfCCC_delThetarMax;
	}
	else
	{
		LfCCC_ThetarEstNewTmp = GfCCC_ThetarEst + LfCCC_delThetar;
	}

	GfCCC_ThetarEstNew = CCC_ThetarLim(LfCCC_ThetarEstNewTmp);


	GfCCC_AlEst = GfCCC_AlEstNew;
	GfCCC_WrEst = GfCCC_WrEstNew;
	GfCCC_ThetarEst = GfCCC_ThetarEstNew;
}

// Speed estimator reset
void CCC_ResetSpdEst(void)
{
	GfCCC_AlEstNew = 0.0;
	GfCCC_AlEst = 0.0;
	GfCCC_WrEstNew = 0.0;
	GfCCC_WrEst = 0.0;
	GfCCC_ThetarEstNew = 0.0;
	GfCCC_ThetarEst = 0.0;
}

// Speed estimator re-initialization
void CCC_ReInitSpdEst(void)
{
	GfCCC_Wc1SpdEst = TWO_PI * KfCCC_fc1SpdEst;
	GfCCC_Wc2SpdEst = TWO_PI * KfCCC_fc2SpdEst;
	GfCCC_Wc3SpdEst = TWO_PI * KfCCC_fc3SpdEst;

	GfCCC_K1SpdEst = GfCCC_Wc1SpdEst * GfCCC_Wc2SpdEst * GfCCC_Wc3SpdEst * KfCCC_Jm;
	GfCCC_K2SpdEst = GfCCC_Wc1SpdEst * GfCCC_Wc2SpdEst + GfCCC_Wc2SpdEst * GfCCC_Wc3SpdEst + GfCCC_Wc3SpdEst * GfCCC_Wc1SpdEst;
	GfCCC_K3SpdEst = GfCCC_Wc1SpdEst + GfCCC_Wc2SpdEst + GfCCC_Wc3SpdEst;
}

// Flux estimator re-initialization
void CCC_ReInitFlxEst(void)
{
	GfCCC_WcFlxEst = TWO_PI * KfCCC_fcFlxEst;
	GfCCC_K11FlxEst = -KfCCC_Rs * GfCCC_InvLd + 2.0 * KfCCC_DampFlxEst * GfCCC_WcFlxEst;
	GfCCC_K22FlxEst = -KfCCC_Rs * GfCCC_InvLq + 2.0 * KfCCC_DampFlxEst * GfCCC_WcFlxEst;
	GfCCC_K31FlxEst = GfCCC_WcFlxEst * GfCCC_WcFlxEst * KfCCC_Ld;
	GfCCC_K42FlxEst = -GfCCC_WcFlxEst * GfCCC_WcFlxEst * KfCCC_Lq;
}

// Angle limit between -PI and PI
float CCC_ThetarLim(float LfCCC_Thetar)
{
	float LfCCC_ThetarOut = 0.0;

	if (LfCCC_Thetar > PI)
	{
		LfCCC_ThetarOut = LfCCC_Thetar - TWO_PI;
	}
	else if (LfCCC_Thetar < -PI)
	{
		LfCCC_ThetarOut = LfCCC_Thetar + TWO_PI;
	}
	else
	{
		LfCCC_ThetarOut = LfCCC_Thetar;
	}

	return LfCCC_ThetarOut;
}
