/*
 * CCC.h
 *
 *  Created on: Oct 17, 2018
 *      Author: H247670
 */

#ifndef APP_APP_CCC_H_
#define APP_APP_CCC_H_

#include <Ifx_Types.h>

#define		ONE_OVER_SQRT_THREE		0.577350269
#define		MIN_DIVISION			0.00001
#define		INV_MIN_DIVISION		1000.0
#define		KRPM_TO_RPM				1000.0
#define		TWO_OVER_THREE			0.666666667
#define		THREE_OVER_TWO			1.5
#define		TWO_PI					6.283185307
#define		PI_OVER_TWO				1.570796327

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/
// Motor resistor (Ohm)
extern float KfCCC_Rs;

// Motor inductance (H)
extern float KfCCC_Ld;
extern float KfCCC_Lq;

// Magnetic flux density from PM (V-s)
extern float KfCCC_LamPM;

// Moment of inertia (kg-m^2)
extern float KfCCC_Jm;

// Voltage usage pct
extern float KfCCC_MagVdqLimRatio;

// Flux-weakening enabling speed (r/min)
extern float KfCCC_WrpmFW;

// Flux-weakening voltage command ratio
extern float KfCCC_FWCmdRatio;

// Flux weakening controller gain
extern float KfCCC_KiFW;
extern float KfCCC_KpFW;

// FW d current command max (A)
extern float KfCCC_FWIdMax;

// Current controller bandwidth (Hz)
extern float KfCCC_fcCC;

// Speed controller bandwidth (Hz)
extern float KfCCC_fcSC;

// Speed controller I gain coeff.
extern float KfCCC_KiFactorSC;

// Speed estimator bandwidth (Hz)
extern float KfCCC_fc1SpdEst;
extern float KfCCC_fc2SpdEst;
extern float KfCCC_fc3SpdEst;

// Speed estimator internal limits
extern float KfCCC_delAlMax;
extern float KfCCC_delWrMax;
extern float KfCCC_delThetarMax;

// Flux estimator bandwidth (Hz)
extern float KfCCC_fcFlxEst;

// Flux estimator damping coeff.
extern float KfCCC_DampFlxEst;

// Current maximum
extern float KfCCC_IsMax;

// Power maximum - motoring (W)
extern float KfCCC_PeMotMax;

// Power maximum - generating (W)
extern float KfCCC_PeGenMax;

// Inverse of system efficiency
extern float KfCCC_SysEff;

// CAN Command
extern float KfCCC_WrpmCmd;
extern float KfCCC_WrpmCmdSlewDn;
extern float KfCCC_WrpmCmdSlewUp;


extern float KfCCC_TrqCmdSlewDn;
extern float KfCCC_TrqCmdSlewUp;

extern float KfCCC_IdcMaxGen;
extern float KfCCC_PdcMaxGen;
extern float KfCCC_IdcMaxMot;
extern float KfCCC_PdcMaxMot;

// Temporary variable for de-rating
extern float KfCCC_DerateMot;

// Derating calibrations
// Derate time set: 1 CNT = 1ms
extern uint16 Ku16CCC_DerateCNTSet01;
extern uint16 Ku16CCC_DerateCNTSet02;
extern uint16 Ku16CCC_DerateCNTSet03;

// Derating number set: 1.0 = no derating, 0.0 = full derating
extern float KfCCC_DeratePwrSet01;
extern float KfCCC_DeratePwrSet02;

// Minimum current to trigger derating (Apk)
extern float KfCCC_DerateIsSet;

// Derating - based on temperature
extern float KfCCC_DerateEnblTemp;
extern float KfCCC_DerateTempCmd;

extern float KfCCC_KpDerateTemp;
extern float KfCCC_KiDerateTemp;

// Manual torque command
extern float KfCCC_TeCmd;

// Speed command to torque command conversion
extern float KfCCC_TeCmdScale;
extern float KfCCC_TeCmdOffset;

extern float KfCCC_TeCmdSlewScale;
extern float KfCCC_TeCmdSlewOffset;

// Temperature estimation - imaginary
extern float KfCCC_WcTempEst;
extern float KfCCC_KcTempEst;

// Derating - Thermal estimation (CoV)
extern float KfCCC_DerateThermEstSet;

// Derating - input flag
extern boolean KbCCC_DerateThermInput;

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
// Temporary variables
// motor speed
extern float GfCCC_Wr;
extern float GfCCC_Wrpm;
extern float GfCCC_WrCmdSlew;
extern float GfCCC_WrpmCmdSlew;

// Inverse of motor speed
extern float GfCCC_InvWr;

extern float GfCCC_Thetar;

// Inverse of LamPM
extern float GfCCC_InvLamPM;

// dq current command (Consumed in T0)
extern float GfCCC_IdCmd;
extern float GfCCC_IqCmd;

// dq current command (Created in T1)
extern float GfCCC_IdCmdT1;
extern float GfCCC_IqCmdT1;

// dq voltage command
extern float GfCCC_VdCmd;
extern float GfCCC_VqCmd;

// dq voltage command (not limited)
extern float GfCCC_VdCmdNoLim;
extern float GfCCC_VqCmdNoLim;

// dq voltage command feed-forward (not limited)
extern float GfCCC_VdCmdFFNoLim;
extern float GfCCC_VqCmdFFNoLim;

// dq voltage command feed-back (not limited)
extern float GfCCC_VdCmdFBNoLim;
extern float GfCCC_VqCmdFBNoLim;

// I term in current controller
extern float GfCCC_IntegErrId;
extern float GfCCC_IntegErrIq;

// dq current error
extern float GfCCC_ErrId;
extern float GfCCC_ErrIq;

// Current controller bandwidth
extern float GfCCC_WcCC;

// Kp gain
extern float GfCCC_KpdCC;
extern float GfCCC_KpqCC;

// Ki gain
extern float GfCCC_KidCC;
extern float GfCCC_KiqCC;

// Ka gain
extern float GfCCC_KadCC;
extern float GfCCC_KaqCC;

// Lamd, Lamq
extern float GfCCC_Lamd;
extern float GfCCC_Lamq;

// Voltage magnitude (not limited)
extern float GfCCC_MagVdqCmdNoLim;


// Voltage magnitude limit
extern float GfCCC_MagVdqLim;

// Voltage command for flux-weakening
extern float GfCCC_FWCmd;

// Integrator in FW
extern float GfCCC_IntegErrFW;

// FW output (not limited)
extern float GfCCC_deltaINoLim;

// FW output (limited)
extern float GfCCC_deltaI;

// dq current command temp.
extern float GfCCC_IdCmdPre;
extern float GfCCC_IqCmdPre;

// dq current command magnitude
extern float GfCCC_MagIdqCmd;

// Te command (not limited)
extern float GfCCC_TeCmdPre;

// Te command (limited)
extern float GfCCC_TeCmd;

// Te Command CAN
extern float GfCCC_TeCmdCAN;

// Te Command SlewUP CAN
extern float GfCCC_TeCmdSlewUPCAN;

// Te Command SlewDN CAN
extern float GfCCC_TeCmdSlewDNCAN;

// Pdc estimation
extern float GfCCC_PdcEst;

// Idc estimation
extern float GfCCC_IdcEst;

// Derate to Power Stage Temperature
extern float GfCCC_DeratePwr;

// Derating activation flag
extern boolean GbCCC_DerateActv;

// Derating timer counter
extern uint16 Gu16CCC_DerateCNT;

// Derate to Motor Temperature
extern float GfCCC_DerateMot;

// Final derate value
extern float GfCCC_DerateMin;

// Maximum current limit
extern float GfCCC_IsMax;

// Iq cmd max
extern float GfCCC_IqCmdMax;

// Iq cmd min
extern float GfCCC_IqCmdMin;

// Iq cmd max (from Pe limit)
extern float GfCCC_IqCmdPeMax;

// Iq cmd min (from Pe limit)
extern float GfCCC_IqCmdPeMin;

// Iq cmd max (from CAN limit)
extern float GfCCC_IqCmdPdcCANMax;

// Iq cmd min (from CAN limit)
extern float GfCCC_IqCmdPdcCANMin;


// SC Integrator
extern float GfCCC_IntegErrWr;

// SC gains
extern float GfCCC_WcSC;

extern float GfCCC_KaSC;
extern float GfCCC_KpSC;
extern float GfCCC_KiSC;

// Inverse of Ld Lq
extern float GfCCC_InvLd;
extern float GfCCC_InvLq;

// Flux estimator - states
extern float GfCCC_IdEstNew;
extern float GfCCC_IqEstNew;
extern float GfCCC_IdEst;
extern float GfCCC_IqEst;

extern float GfCCC_EdEstNew;
extern float GfCCC_EqEstNew;
extern float GfCCC_EdEst;
extern float GfCCC_EqEst;

// Flux estimator - gains
extern float GfCCC_WcFlxEst;

extern float GfCCC_K11FlxEst;
extern float GfCCC_K12FlxEst;
extern float GfCCC_K21FlxEst;
extern float GfCCC_K22FlxEst;
extern float GfCCC_K31FlxEst;
extern float GfCCC_K42FlxEst;

// Speed estimator - states
extern float GfCCC_AlEstNew;
extern float GfCCC_AlEst;
extern float GfCCC_WrEstNew;
extern float GfCCC_WrEst;
extern float GfCCC_ThetarEstNew;
extern float GfCCC_ThetarEst;

// Speed estimator - gains
extern float GfCCC_Wc1SpdEst;
extern float GfCCC_Wc2SpdEst;
extern float GfCCC_Wc3SpdEst;

extern float GfCCC_K1SpdEst;
extern float GfCCC_K2SpdEst;
extern float GfCCC_K3SpdEst;

// Angle error
extern float GfCCC_ErrThetar;

// Inverse of inertia
extern float GfCCC_InvJm;

// CAN variables
extern float GfCCC_WrpmCmd;
extern float GfCCC_WrpmCmdCAN;
extern float GfCCC_IsMaxGenCAN;
extern float GfCCC_IsMaxMotCAN;
extern float GfCCC_WrpmCmdSlewDnCAN;
extern float GfCCC_WrpmCmdSlewUpCAN;
extern float GfCCC_TrqCmdSlewDnCAN;
extern float GfCCC_TrqCmdSlewUpCAN;

extern float GfCCC_IdcMinCAN;
extern float GfCCC_PdcMinCAN;
extern float GfCCC_IdcMaxCAN;
extern float GfCCC_PdcMaxCAN;

// Temperature estimation - imaginary
extern float GfCCC_InvTempEst;

// Derating - based on temperature
extern float GfCCC_IntegErrTemp;
extern float GfCCC_delIsDerate;

extern float GfCCC_DerateTemp;

// Derating - Thermal estimator (CoV)
extern boolean GbCCC_DerateThermEstActv;
extern float GfCCC_DerateThermEst;
extern float GfCCC_InvTempEst02;

/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
// Reset dq current command
extern void CCC_ResetCurCmd(void);

// Current controller
extern void CCC_CurCntr(void);

// Reset current controller
extern void CCC_ResetCurCntr(void);

// Set current controller. (if speed is not zero)
extern void CCC_SetCurCntr(void);

// Flux-weakening
extern void CCC_FluxWeakening(void);

// Reset FW
extern void CCC_ResetFW(void);

// Speed controller
extern void CCC_SpdCntr(void);

// Speed controller reset
extern void CCC_ResetSpdCntr(void);

// Flux estimator
extern void CCC_FlxEst(void);

// Flux estimator reset
extern void CCC_ResetFlxEst(void);

// Flux estimator set
extern void CCC_SetFlxEst(void);

// Speed estimator
extern void CCC_SpdEst(void);

// Speed estimator reset
extern void CCC_ResetSpdEst(void);

// SVPWM
extern void CCC_SVPWM(void);

// Current controller re-initialization
extern void CCC_ReInitCurtCtrl(void);

// Speed Controller re-initialization
extern void CCC_ReInitSpdCtrl(void);

// Speed estimator re-initialization
extern void CCC_ReInitSpdEst(void);

// Flux estimator re-initialization
extern void CCC_ReInitFlxEst(void);

// Angle limit between -PI and PI
extern float CCC_ThetarLim(float LfCCC_Thetar);

// Inverse value calculation
extern void CCC_ReInitInvs(void);

// Power/Torque/Current Limit Function
extern void CCC_IdqCmdLim(void);

// Slew rate limit
extern float CCC_SlewLimit(float LfCCC_Input, float LfCCC_Cmd, float LfCCC_SlewUpLim, float LfCCC_SlewDnLim, float LfCCC_Tsamp);

// De-rating calculation
extern void CCC_Derating(void);

// Reset de-rating calculation
extern void CCC_ResetDerating(void);


#endif /* APP_APP_CCC_H_ */
