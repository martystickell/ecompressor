
#include "vadc.h"
#include "math.h"
#include "CCC.h"


/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
uint16 Gu16ADC_G0RES00 = 0;
uint16 Gu16ADC_G0RES01 = 0;
uint16 Gu16ADC_G0RES02 = 0;
uint16 Gu16ADC_G0RES03 = 0;
uint16 Gu16ADC_G0RES04 = 0;
uint16 Gu16ADC_G0RES05 = 0;
uint16 Gu16ADC_G0RES06 = 0;
uint16 Gu16ADC_G0RES07 = 0;
uint16 Gu16ADC_G0RES08 = 0;
uint16 Gu16ADC_G0RES09 = 0;
uint16 Gu16ADC_G0RES10 = 0;
uint16 Gu16ADC_G0RES11 = 0;

uint16 Gu16ADC_G1RES00 = 0;
uint16 Gu16ADC_G1RES01 = 0;
uint16 Gu16ADC_G1RES02 = 0;
uint16 Gu16ADC_G1RES03 = 0;
uint16 Gu16ADC_G1RES04 = 0;
uint16 Gu16ADC_G1RES05 = 0;
uint16 Gu16ADC_G1RES06 = 0;
uint16 Gu16ADC_G1RES07 = 0;
uint16 Gu16ADC_G1RES08 = 0;
uint16 Gu16ADC_G1RES09 = 0;
uint16 Gu16ADC_G1RES10 = 0;
uint16 Gu16ADC_G1RES11 = 0;


/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
void Vadc_Init(void)
{
	volatile unsigned int uwTemp;
	uint16  endinit_pw;
	endinit_pw = IfxScuWdt_getCpuWatchdogPassword(); //Get the password for the current CPU

	//Enable the Clock
	//-------------------------------------------------------------------------
	//Clear the Endinit Function to access the protected Registers
	IfxScuWdt_clearCpuEndinit(endinit_pw);
		//A. Enable the GTM Module:
		VADC_CLC.U   = 0x00000000;   // load clock control register
		uwTemp         = VADC_CLC.U; // dummy read to avoid pipeline effects
		while ((VADC_CLC.U & 0x00000002 )== 0x00000002);  //wait until module is enabled
		//SET the Endinit Function after access to the protected Registers
    IfxScuWdt_setCpuEndinit(endinit_pw);
	//-------------------------------------------------------------------------


    /*Configure Queue Source*/
    	//1st Enable converter
    	//2nd Set divider factor for analog internal clock and start calibration
    	//3rd Enable arbitration slot 0 (queue source) and assign priority
    	//4th Wait for calibration to complete (can be done later)
    	//5th Select triggering and gating inputs
    	//6th Enable triggering and gating
    	//7th Select channels to be considered for scan
    	//8th Map channel result to result register


	//Arbitration Configuration Register, Group x (GxARBCFG)
    //------------------------------------------------------
	//ANONC (Analog Converter Control) Converter ON/OFF
    //Notes: Converter must be started before writing to SUCAL
    VADC_G0ARBCFG.U =	(0x3 /*ANONC*/) |
    					(0x1 << 4 /* ARBRND */);

    VADC_G1ARBCFG.U =	(0x3 /*ANONC*/) |
    					(0x1 << 4 /* ARBRND */);




	//Global Configuration Register (GLOBCFG)
	//---------------------------------------
	//DIVA (Divider Factor for the Analog Internal Clock)
	//DIVWC (Write Control for Divider Parameters)
	//SUCAL (Start-Up Calibration) Start calibration, status indicated by GxARBCFG.CAL
	VADC_GLOBCFG.U = (0x5     /*DIVA*/ )|
					 (0x1<<15 /*DIVWC*/);


	//Arbitration Priority Register, Group x (GxARBPR)
	//------------------------------------------------
	//PRIOx (Priority of Request Source x)
	//ASENy (Arbitration Slot y Enable) Group Queued source has slot 0
	VADC_G0ARBPR.U = (0x3       	/*PRIO0*/)|
					 (0x1 << 24 	/*ASEN0*/)|
					 (0x3 << 12 	/*PRIO1*/)| // auto scan priority 3
 					 (0x1 << 25 	/*ASEN1*/)| // auto scan arbitration slot enable
					 (0x2 << 12 	/*PRIO3*/)| // queue 3 priority
					 (0x1 << 27 	/*ASEN3*/); // queue 3 arbitration slot enable

	//PRIOx (Priority of Request Source x)
	//ASENy (Arbitration Slot y Enable) Group Queued source has slot 0
	VADC_G1ARBPR.U = (0x3       	/*PRIO0*/)|
					 (0x1 << 24 	/*ASEN0*/)|
					 (0x3 << 12 	/*PRIO1*/)| // auto scan priority 3
					 (0x1 << 25 	/*ASEN1*/)| // auto scan arbitration slot enable
					 (0x2 << 12 	/*PRIO3*/)| // queue 3 priority
					 (0x1 << 27 	/*ASEN3*/); // queue 3 arbitration slot enable


	VADC_GLOBCFG.U |= (0x1<<31 /*SUCAL*/);	//Begin start-up calibration

	while((VADC_G0ARBCFG.U & 0x30000000) != 0x20000000);  //Wait for start-up calibration to complete
	while((VADC_G1ARBCFG.U & 0x30000000) != 0x20000000);  //Wait for start-up calibration to complete

	init_queue0_group0();
	init_queue3_group0();
	init_scan_group0();
	init_queue0_group1();
	init_queue3_group1();
	init_scan_group1();

}


void init_queue0_group0(void)
{

	//Queue 0 Input Register, Group x (GxQINR0)
	//-----------------------------------------
	//Add Channels 0,1,2 to Group 0 Queue Source
	//REQCHNR (Request Channel Number)
	//RF (Refill) Reload queue entry once conversion started
	//ENSI (Enable Source Interrupt)
	//EXTR (External Trigger)

	//sequence 012 Queue 0
	VADC_G0QINR0.U =    (0x0  /*REQCHNR*/ )|
						(0x1 << 5 /*RF*/  )|
						(0x1 << 7 /*EXTR*/); 	/* this is required on the first channel of the queue to receive the trigger and start the conversion */

	VADC_G0QINR0.U =    (0x1 /*REQCHNR*/)|
						(0x1 << 5 /*RF*/);

	VADC_G0QINR0.U =    (0x2 /*REQCHNR*/)|
						(0x1 << 5 /*RF*/)|
						(0x1 << 6 /*ENSI*/);	/* this bit enables the generation of interrupt when the conversion of the three channels is finished */


	//Source Control Register, Group x  (GxQCTRL0)
	//--------------------------------------------
    //SRCRESREG (Source-specific Result Register) -- Use GxCHCTRy.RESREG to select result register for channels in group.
    //XTSEL     (External Trigger Input Selection) -- Use CCU60_SR3
    //XTMODE    (Trigger Operating Mode) -- Trigger event upon rising edge
    //XTWC      (Write Control for Trigger Configuration) -- Bitfields XTMODE and XTSEL can be written
	//GTSEL		(Gate Input Selection) -- Use CCU60_SR3
	//GTWC      (Write Control for Gate Configuration) -- Bitfield GTSEL can be written

	/* group 0 queue 0 */
	VADC_G0QCTRL0.U = 	(0x0    /*SRCRESREG*/)|
    			 (CTRL_XTSEL << 8  /*XTSEL*/ )|   	// gate is selected as trigger source
    					(0x1 << 13 /*XTMODE*/)|	 	// falling edge on tom channel 3
    					(0x1 << 15 /*XTWC*/  )|		// write enable
    			 (CTRL_GTSEL << 16 /*GTSEL*/ )|
    					(0x1 << 23 /*GTWC*/  );


	//Mode Register, Group x (GxQMR0)
	//-------------------------------
	//ENGT (Enable Gate) Conversion request issued if element in queue0 register or backup register
	//ENTR (Enable External Trigger) Trigger input REQTR (selected by XTSEL) generates the trigger event

	VADC_G0QMR0.U =   (0x1      /*ENGT*/)|
					  (0x1 << 2 /*ENTR*/);

	//Channel Control Register, Group x Channel y (GxCHCTRy)
	//------------------------------------------------------
	//RESREG (Result Register) Store result from channel y to specified group result register
	//RESPOS (Result Position) Store result right-aligned

	/* group 0 queue 0 */
	VADC_G0CHCTR0.U = (0x0 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR1.U = (0x1 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR2.U = (0x2 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

}


void init_queue3_group0(void)
{
	//sequence 34 queue 3
	VADC_G0QINR3.U =    (0x3  		/*REQCHNR*/ )|
						(0x1 << 5 	/*RF*/  )|
						(0x1 << 7 	/*EXTR*/); 	/* this is required on the first channel of the queue to receive the trigger and start the conversion */


	VADC_G0QINR3.U =	(0x4 		/*REQCHNR*/)|
						(0x1 << 5 	/*RF*/)|
						(0x1 << 6 	/*ENSI*/);	/* this bit enables the generation of interrupt when the conversion of the three channels is finished */

	VADC_G0QMR3.U = 	(0x1      /*ENGT*/)|
						(0x1 << 2 /*ENTR*/);

	/* group 0 queue 1 */
	VADC_G0CHCTR3.U = 	(0x3 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR4.U = 	(0x4 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);
}


void init_scan_group0(void)
{

	//Autoscan Source Mode Register, Group x (GxASMR)
	//-----------------------------------------------
	//ENGT (Enable Gate) Issue conversion request if pending bits are set
	//ENTR (Enable External Trigger) ON/OFF, Trigger selected by XTSEL
	VADC_G0ASMR.U = 	(0x1 << 0 /*ENGT*/)|
						(0x1 << 2 /*ENTR*/);
	//-----------------------------------------------


	//Autoscan Source Channel Select Register, Group x (GxASSEL)
	//----------------------------------------------------------
	//CHSEL (Channel Selection) Set corresponding bits to include channels in scan sequence
	//Notes: Select channels 5,6,7,8,9 of Group 0 to be included in scan sequence
	VADC_G0ASSEL.U = (0x000003E0 /*CHSEL*/);
	//----------------------------------------------------------

	VADC_G0CHCTR5.U = 	(0x5 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR6.U = 	(0x6 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR7.U = 	(0x7 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR8.U = 	(0x8 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR9.U = 	(0x9 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);
}


void init_queue0_group1(void)
{

	//Queue 0 Input Register, Group x (GxQINR0)
	//-----------------------------------------
	//Add Channels 0,1,2 to Group 0 Queue Source
	//REQCHNR (Request Channel Number)
	//RF (Refill) Reload queue entry once conversion started
	//ENSI (Enable Source Interrupt)
	//EXTR (External Trigger)

	//sequence 012 Queue 0
	VADC_G1QINR0.U =    (0x0  /*REQCHNR*/ )|
						(0x1 << 5 /*RF*/  )|
						(0x1 << 7 /*EXTR*/); 	/* this is required on the first channel of the queue to receive the trigger and start the conversion */

	VADC_G1QINR0.U =    (0x1 /*REQCHNR*/)|
						(0x1 << 5 /*RF*/);

	VADC_G1QINR0.U =    (0x2 /*REQCHNR*/)|
						(0x1 << 5 /*RF*/)|
						(0x1 << 6 /*ENSI*/);	/* this bit enables the generation of interrupt when the conversion of the three channels is finished */


	//Source Control Register, Group x  (GxQCTRL0)
	//--------------------------------------------
    //SRCRESREG (Source-specific Result Register) -- Use GxCHCTRy.RESREG to select result register for channels in group.
    //XTSEL     (External Trigger Input Selection) -- Use CCU60_SR3
    //XTMODE    (Trigger Operating Mode) -- Trigger event upon rising edge
    //XTWC      (Write Control for Trigger Configuration) -- Bitfields XTMODE and XTSEL can be written
	//GTSEL		(Gate Input Selection) -- Use CCU60_SR3
	//GTWC      (Write Control for Gate Configuration) -- Bitfield GTSEL can be written

	/* group 1 queue 0 */
	VADC_G1QCTRL0.U = 	(0x0    /*SRCRESREG*/)|
    			 (CTRL_XTSEL << 8  /*XTSEL*/ )|   	// gate is selected as trigger source
    					(0x1 << 13 /*XTMODE*/)|	 	// falling edge on tom channel 3
    					(0x1 << 15 /*XTWC*/  )|		// write enable
    			 (CTRL_GTSEL << 16 /*GTSEL*/ )|
    					(0x1 << 23 /*GTWC*/  );


	//Mode Register, Group x (GxQMR0)
	//-------------------------------
	//ENGT (Enable Gate) Conversion request issued if element in queue0 register or backup register
	//ENTR (Enable External Trigger) Trigger input REQTR (selected by XTSEL) generates the trigger event

	VADC_G1QMR0.U =   (0x1      /*ENGT*/)|
					  (0x1 << 2 /*ENTR*/);

	//Channel Control Register, Group x Channel y (GxCHCTRy)
	//------------------------------------------------------
	//RESREG (Result Register) Store result from channel y to specified group result register
	//RESPOS (Result Position) Store result right-aligned

	/* group 0 queue 0 */
	VADC_G1CHCTR0.U = (0x0 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR1.U = (0x1 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR2.U = (0x2 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

}


void init_queue3_group1(void)
{
	//sequence 3 queue 3
	VADC_G1QINR3.U =    (0x3  		/*REQCHNR*/ )|
						(0x1 << 5 	/*RF*/  )|
						(0x1 << 7 	/*EXTR*/)| /* this is required on the first channel of the queue to receive the trigger and start the conversion */
						(0x1 << 6 	/*ENSI*/);

	VADC_G1QMR3.U = 	(0x1      /*ENGT*/)|
						(0x1 << 2 /*ENTR*/);

	/* group 1 queue 3 */
	VADC_G1CHCTR3.U = 	(0x3 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);
}


void init_scan_group1(void)
{

	//Autoscan Source Mode Register, Group x (GxASMR)
	//-----------------------------------------------
	//ENGT (Enable Gate) Issue conversion request if pending bits are set
	//ENTR (Enable External Trigger) ON/OFF, Trigger selected by XTSEL
	VADC_G1ASMR.U = 	(0x1 << 0 /*ENGT*/)|
						(0x1 << 2 /*ENTR*/);
	//-----------------------------------------------


	//Autoscan Source Channel Select Register, Group x (GxASSEL)
	//----------------------------------------------------------
	//CHSEL (Channel Selection) Set corresponding bits to include channels in scan sequence
	//Notes: Select channels 4,5,6,7,8,9 of Group 1 to be included in scan sequence
	VADC_G1ASSEL.U = (0x000003F0 /*CHSEL*/);
	//----------------------------------------------------------

	VADC_G1CHCTR4.U = 	(0x4 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR5.U = 	(0x5 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR6.U = 	(0x6 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR7.U = 	(0x7 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR8.U = 	(0x8 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR9.U = 	(0x9 << 16 /*RESREG*/)|
						(0x1 << 21 /*RESPOS*/);
}


void init_queue0_group0_test(void)
{

	//Queue 0 Input Register, Group x (GxQINR0)
	//-----------------------------------------
	//Add Channels 0,1,2 to Group 0 Queue Source
	//REQCHNR (Request Channel Number)
	//RF (Refill) Reload queue entry once conversion started
	//ENSI (Enable Source Interrupt)
	//EXTR (External Trigger)

	//sequence 012 Queue 0
	VADC_G0QINR0.U =    (0x2  /*REQCHNR*/ )|
						(0x1 << 5 /*RF*/  )|
						(0x1 << 7 /*EXTR*/); 	/* this is required on the first channel of the queue to receive the trigger and start the conversion */

	VADC_G0QINR0.U =    (0x3 /*REQCHNR*/)|
						(0x1 << 5 /*RF*/);

	VADC_G0QINR0.U =    (0x4 /*REQCHNR*/)|
						(0x1 << 5 /*RF*/)|
						(0x1 << 6 /*ENSI*/);	/* this bit enables the generation of interrupt when the conversion of the three channels is finished */


	//Source Control Register, Group x  (GxQCTRL0)
	//--------------------------------------------
    //SRCRESREG (Source-specific Result Register) -- Use GxCHCTRy.RESREG to select result register for channels in group.
    //XTSEL     (External Trigger Input Selection) -- Use CCU60_SR3
    //XTMODE    (Trigger Operating Mode) -- Trigger event upon rising edge
    //XTWC      (Write Control for Trigger Configuration) -- Bitfields XTMODE and XTSEL can be written
	//GTSEL		(Gate Input Selection) -- Use CCU60_SR3
	//GTWC      (Write Control for Gate Configuration) -- Bitfield GTSEL can be written

	/* group 0 queue 0 */
	VADC_G0QCTRL0.U = 	(0x0    /*SRCRESREG*/)|
    			 (CTRL_XTSEL << 8  /*XTSEL*/ )|   	// gate is selected as trigger source
    					(0x1 << 13 /*XTMODE*/)|	 	// falling edge on tom channel 3
    					(0x1 << 15 /*XTWC*/  )|		// write enable
    			 (CTRL_GTSEL << 16 /*GTSEL*/ )|
    					(0x1 << 23 /*GTWC*/  );


	//Mode Register, Group x (GxQMR0)
	//-------------------------------
	//ENGT (Enable Gate) Conversion request issued if element in queue0 register or backup register
	//ENTR (Enable External Trigger) Trigger input REQTR (selected by XTSEL) generates the trigger event

	VADC_G0QMR0.U =   (0x1      /*ENGT*/)|
					  (0x1 << 2 /*ENTR*/);

	//Channel Control Register, Group x Channel y (GxCHCTRy)
	//------------------------------------------------------
	//RESREG (Result Register) Store result from channel y to specified group result register
	//RESPOS (Result Position) Store result right-aligned

	/* group 0 queue 0 */
	VADC_G0CHCTR0.U = (0x0 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR1.U = (0x1 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR2.U = (0x2 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR3.U = (0x3 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

	VADC_G0CHCTR4.U = (0x4 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

}


void init_queue0_group1_test(void)
{

	//Queue 0 Input Register, Group x (GxQINR0)
	//-----------------------------------------
	//Add Channels 0,1,2 to Group 0 Queue Source
	//REQCHNR (Request Channel Number)
	//RF (Refill) Reload queue entry once conversion started
	//ENSI (Enable Source Interrupt)
	//EXTR (External Trigger)

	//sequence 012 Queue 0
	VADC_G1QINR0.U =    (0x2  /*REQCHNR*/ )|
						(0x1 << 5 /*RF*/  )|
						(0x1 << 7 /*EXTR*/); 	/* this is required on the first channel of the queue to receive the trigger and start the conversion */

	VADC_G1QINR0.U =    (0x3 /*REQCHNR*/)|
						(0x1 << 5 /*RF*/);

	VADC_G1QINR0.U =    (0x4 /*REQCHNR*/)|
						(0x1 << 5 /*RF*/)|
						(0x1 << 6 /*ENSI*/);	/* this bit enables the generation of interrupt when the conversion of the three channels is finished */


	//Source Control Register, Group x  (GxQCTRL0)
	//--------------------------------------------
    //SRCRESREG (Source-specific Result Register) -- Use GxCHCTRy.RESREG to select result register for channels in group.
    //XTSEL     (External Trigger Input Selection) -- Use CCU60_SR3
    //XTMODE    (Trigger Operating Mode) -- Trigger event upon rising edge
    //XTWC      (Write Control for Trigger Configuration) -- Bitfields XTMODE and XTSEL can be written
	//GTSEL		(Gate Input Selection) -- Use CCU60_SR3
	//GTWC      (Write Control for Gate Configuration) -- Bitfield GTSEL can be written

	/* group 0 queue 0 */
	VADC_G1QCTRL0.U = 	(0x0    /*SRCRESREG*/)|
    			 (CTRL_XTSEL << 8  /*XTSEL*/ )|   	// gate is selected as trigger source
    					(0x1 << 13 /*XTMODE*/)|	 	// falling edge on tom channel 3
    					(0x1 << 15 /*XTWC*/  )|		// write enable
    			 (CTRL_GTSEL << 16 /*GTSEL*/ )|
    					(0x1 << 23 /*GTWC*/  );


	//Mode Register, Group x (GxQMR0)
	//-------------------------------
	//ENGT (Enable Gate) Conversion request issued if element in queue0 register or backup register
	//ENTR (Enable External Trigger) Trigger input REQTR (selected by XTSEL) generates the trigger event

	VADC_G1QMR0.U =   (0x1      /*ENGT*/)|
					  (0x1 << 2 /*ENTR*/);

	//Channel Control Register, Group x Channel y (GxCHCTRy)
	//------------------------------------------------------
	//RESREG (Result Register) Store result from channel y to specified group result register
	//RESPOS (Result Position) Store result right-aligned

	/* group 0 queue 0 */
	VADC_G1CHCTR0.U = (0x0 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR1.U = (0x1 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR2.U = (0x2 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR3.U = (0x3 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);

	VADC_G1CHCTR4.U = (0x4 << 16 /*RESREG*/)|
					  (0x1 << 21 /*RESPOS*/);
}


void vadc_trig_group0_q3(void)
{
	VADC_G0QMR3.B.TREV = 1;  	// trigger q3
}

void vadc_trig_group0_scan(void)
{
	VADC_G0ASMR.B.LDEV = 1;		// trigger scan
}

void vadc_trig_group1_q3(void)
{
	VADC_G1QMR3.B.TREV = 1;  	// trigger q3
}

void vadc_trig_group1_scan(void)
{
	VADC_G1ASMR.B.LDEV = 1;		// trigger scan
}

void ADC_ReadIabcVdc(void)
{
	Gu16ADC_G0RES00 = VADC_G0RES0.B.RESULT;
	Gu16ADC_G0RES01 = VADC_G0RES1.B.RESULT;
	Gu16ADC_G0RES02 = VADC_G0RES2.B.RESULT;

	Gu16ADC_G1RES00 = VADC_G1RES0.B.RESULT;
	Gu16ADC_G1RES01 = VADC_G1RES1.B.RESULT;
	Gu16ADC_G1RES02 = VADC_G1RES2.B.RESULT;
}

void ADC_ReadVabc(void)
{
	Gu16ADC_G0RES03 = VADC_G0RES3.B.RESULT;
	Gu16ADC_G0RES04 = VADC_G0RES4.B.RESULT;

	Gu16ADC_G1RES03 = VADC_G1RES3.B.RESULT;
}

void ADC_ReadT1Values(void)
{
	Gu16ADC_G0RES05 = VADC_G0RES5.B.RESULT;
	Gu16ADC_G0RES06 = VADC_G0RES6.B.RESULT;
	Gu16ADC_G0RES07 = VADC_G0RES7.B.RESULT;
	Gu16ADC_G0RES08 = VADC_G0RES8.B.RESULT;
	Gu16ADC_G0RES09 = VADC_G0RES9.B.RESULT;

	Gu16ADC_G1RES04 = VADC_G1RES4.B.RESULT;
	Gu16ADC_G1RES05 = VADC_G1RES5.B.RESULT;
	Gu16ADC_G1RES06 = VADC_G1RES6.B.RESULT;
	Gu16ADC_G1RES07 = VADC_G1RES7.B.RESULT;
	Gu16ADC_G1RES08 = VADC_G1RES8.B.RESULT;
	Gu16ADC_G1RES09 = VADC_G1RES9.B.RESULT;
}
