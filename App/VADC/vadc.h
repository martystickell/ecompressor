#ifndef VADC_H
#define VADC_H

#include <Ifx_Types.h>
#include "IfxScuWdt.h"
#include "IfxVadc_reg.h"
#include "IfxCpu_IntrinsicsTasking.h"

#define 	ONE_OVER_THREE			0.333333333
#define 	ONE_OVER_SQRT_THREE		0.577350269

/*---------------------------------------------------------------------------------
 * Calibrations
 * ------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------
 * Variables
 * ------------------------------------------------------------------------------*/
extern uint16 Gu16ADC_G0RES00;
extern uint16 Gu16ADC_G0RES01;
extern uint16 Gu16ADC_G0RES02;
extern uint16 Gu16ADC_G0RES03;
extern uint16 Gu16ADC_G0RES04;
extern uint16 Gu16ADC_G0RES05;
extern uint16 Gu16ADC_G0RES06;
extern uint16 Gu16ADC_G0RES07;
extern uint16 Gu16ADC_G0RES08;
extern uint16 Gu16ADC_G0RES09;
extern uint16 Gu16ADC_G0RES10;
extern uint16 Gu16ADC_G0RES11;

extern uint16 Gu16ADC_G1RES00;
extern uint16 Gu16ADC_G1RES01;
extern uint16 Gu16ADC_G1RES02;
extern uint16 Gu16ADC_G1RES03;
extern uint16 Gu16ADC_G1RES04;
extern uint16 Gu16ADC_G1RES05;
extern uint16 Gu16ADC_G1RES06;
extern uint16 Gu16ADC_G1RES07;
extern uint16 Gu16ADC_G1RES08;
extern uint16 Gu16ADC_G1RES09;
extern uint16 Gu16ADC_G1RES10;
extern uint16 Gu16ADC_G1RES11;


/*---------------------------------------------------------------------------------
 * Functions
 * ------------------------------------------------------------------------------*/
extern void Vadc_Init(void);
extern void init_queue3_group0(void);
extern void init_queue0_group0(void);
extern void init_scan_group0(void);
extern void init_queue0_group1(void);
extern void init_queue3_group1(void);
extern void init_scan_group1(void);

extern void init_queue0_group0_test(void);
extern void init_queue0_group1_test(void);

extern void vadc_trig_group0_q3(void);
extern void vadc_trig_group0_scan(void);
extern void vadc_trig_group1_q3(void);
extern void vadc_trig_group1_scan(void);

extern void ADC_ReadIabcVdc(void);
extern void ADC_ReadVabc(void);
extern void ADC_ReadT1Values(void);

#define CTRL_XTSEL 0xF	//Selected gating signal is trigger
#define CTRL_GTSEL 0

#endif
