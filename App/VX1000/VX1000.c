/*------------------------------------------------------------------------------- */
/* VX1000.c                                                                       */
/* Vector VX1000 Application Driver for Infineon Tricore with DAP1/2 or DigRf I/F */
/* Release: 03_06                    Build: 596          Modification: 21.10.2017 */
/* Vector Informatik GmbH                                                         */
/*                                                                                */
/* Don't modify this file, settings are defined in VX1000_cfg.h                   */
/*------------------------------------------------------------------------------- */


/* ----------------------------------------------------------------------------------------------------------------------- /
/ Status of MISRA conformance:                                                                                             /
/ ---------------------------                                                                                              /
/  * advisory rule 19.1 "location of file inclusions"                                                                      /
/     - violated because there exists no MISRA-conform workaround without loosing essential functionality                  /
/     - see individual justifications of the particular violations in the code                                             /
/                                                                                                                          /
/  * advisory and required rules 11.3 "cast between pointers and integral types"                                           /
/     - violated because copying data from and to arbitrary locations is the core feature of this module                   /
/     - see individual justifications of the particular violations in the code                                             /
/                                                                                                                          /
/  * required rule 12.4 "condition check with side effects"                                                                /
/     - just a phantom detection by the tool (the reported user callback is specified to have NO side effect).             /
/     - see individual justifications of the particular violations in the code                                             /
/                                                                                                                          /
/  * required rule 14.2 "code without effect"                                                                              /
/     - violated only by user callback "VX1000_DUMMYREAD" whose purpose exactly is to have no effect.                      /
/     - see individual justifications of the particular violations in the code                                             /
/                                                                                                                          /
/  * required rule 16.1 "usage of variadic functions"                                                                      /
/     - violated only if the user explicitly enables the debug print feature during development phase.                     /
/     - see individual justifications of the particular violations in the code                                             /
/                                                                                                                          /
/  * required rule 19.15 "repeated file inclusion"                                                                         /
/     - just a phantom detection by the checker tool, no real violation                                                    /
/                                                                                                                          /
/  * required rule 20.9 "inclusion of bad libraries"                                                                       /
/     - violated only if the user explicitly enables the maximum debug print feature using STDIO library/header.           /
/     - see individual justifications of the particular violations in the code                                             /
/                                                                                                                          /
/  * required rule 13.7 "boolean operations with invariant results"                                                        /
/     - may violate because an operator in the operation resorts to a user-defined callback macro which may (but does not  /
/       necessarily have to) return a result within a certain range that even at compile time is guaranteed to be constant /
/                                                                                                                          /
/  * required rule 14.1 "unreachable code"                                                                                 /
/     - may violate where conditions that result from errors supposed never to occur are handled because in some cases     /
/       the chances of those conditions to occur depend on user-defined callback macros which even at compile time are     /
/       found to be zero                                                                                                   /
/                                                                                                                          /
/                                                                            (checked in PowerPC build 529)                /
/                                                                                                                          /
/------------------------------------------------------------------------------------------------------------------------ */


#include "VX1000.h" /* PRQA S 0883*/ /* Actually not violating MISRA rule 19.15: this file is included only once into this module */

#if defined(VX1000_TARGET_XC2000)
#elif defined(VX1000_TARGET_POWERPC)
#elif defined(VX1000_TARGET_SH2)
#elif defined(VX1000_TARGET_TMS570)
#elif defined(VX1000_TARGET_TRICORE)
#elif defined(VX1000_TARGET_X850)
#else /* !VX1000_TARGET_<target> */
#define VX1000IF_CFG_NO_TARGET
#endif /* !VX1000_TARGET_<target> */
#if defined(VX1000IF_CFG_NO_TARGET)
#error You must define the appropriate VX1000_TARGET_<target> in VX1000_cfg.h!
#endif /* !VX1000IF_CFG_NO_TARGET */

#if defined(VX1000_MAILBOX_PRINTF)
#include <stdarg.h>
#if !defined(VX1000_PRINTF_MINIMAL)
#include <stdio.h> /* this violates MISRA rule 20.9, but the user is responsible to turn off printf in production code while the features still are very useful in debug code */
#endif /* !VX1000_PRINTF_MINIMAL */
#endif /* VX1000_MAILBOX_PRINTF */

#define VX1000_APPDRIVER_BUILDNUMBER_C 596UL

#if ((!defined(VX1000_APPDRIVER_BUILDNUMBER)) || (!defined(VX1000_APPDRIVER_BUILDNUMBER_C))) || (!defined(VX1000_APPDRIVER_BUILDNUMBER_H))
#error AppDriver files are corrupted. Please fetch a new and consistent file set!
#endif /* ! VX1000_APPDRIVER_BUILDNUMBERxxx */
#if (((VX1000_APPDRIVER_BUILDNUMBER) != (VX1000_APPDRIVER_BUILDNUMBER_C)) || ((VX1000_APPDRIVER_BUILDNUMBER) != (VX1000_APPDRIVER_BUILDNUMBER_H)))
#error Invalid mixture of incompatible AppDriver files. Please fetch a new and consistent file set!
#endif /* VX1000_APPDRIVER_BUILDNUMBERxxx mismatch */


#if defined(VX1000_DISABLE_INSTRUMENTATION)

/* Some compilers (e.g. cosmic) fail creating completely empty object files. Allow the user to provide a workaround: */
VX1000_EMPTYFILE_DUMMYDECL

#else /* !VX1000_DISABLE_INSTRUMENTATION */


/* include user-defined lines with optional section pragmas to force individual linkage of VX1000 code and/or data: */
#define VX1000_BEGSECT_VXMODULE_C
#include "VX1000_cfg.h"  /* PRQA S 0883*/ /* Willingly violating MISRA rule 19.15: this file is included multiple times because it contains multiple disjunct parts that each are filtered exactly once by preprocessor conditions */ /* PRQA S 5087*/ /* Willingly violating MISRA rule 19.1: this file is included multiple at exactly those lines where it is needed to expand to the desired lines. Inclusion/expansion of those lines at the top of this file would not make sense and lead to compile errors */
#define VX1000_BEGSECT_VXMODULE_C_UNDO


/* --- local prototypes --- */
#if defined(VX1000_OLDA) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
static void VX1000_SUFFUN(vx1000_OldaInit)( void );
#endif /* VX1000_OLDA & !VX1000_COMPILED_FOR_SLAVECORES */
#if defined(VX1000_MAILBOX) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
static void VX1000_SUFFUN(vx1000_MailboxInit)(void);
#endif /* VX1000_MAILBOX & !VX1000_COMPILED_FOR_SLAVECORES */
#if defined(VX1000_RES_MGMT) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
static void VX1000_SUFFUN(vx1000_ResMgmtInit)( void );
#endif /* VX1000_RES_MGMT & !VX1000_COMPILED_FOR_SLAVECORES */
#if defined(VX1000_STIM)
#if !defined(VX1000_COMPILED_FOR_SLAVECORES)
static void VX1000_SUFFUN(vx1000_StimInit)( void );
#endif /* !VX1000_COMPILED_FOR_SLAVECORES */
#if defined(VX1000_STIM_BENCHMARK)
#if !defined(VX1000_COMPILED_FOR_SLAVECORES)
static void VX1000_SUFFUN(vx1000_StimBenchmarkInit)( void );
#endif /* !VX1000_COMPILED_FOR_SLAVECORES */
static void VX1000_SUFFUN(vx1000_StimBenchmarkStimEnd)( VX1000_UINT8 stim_event, VX1000_UINT8 timeout_flag );
static void VX1000_SUFFUN(vx1000_StimBenchmarkStimCheck)( void );
#endif /* VX1000_STIM_BENCHMARK */
#endif /* VX1000_STIM */
#if (defined(VX1000_OVERLAY) && (!defined(VX1000_COMPILED_FOR_SLAVECORES)))
static void VX1000_SUFFUN(vx1000_OverlayInit)( void );
#if defined(VX1000_MAILBOX_OVERLAY_CONTROL)
static VX1000_UINT8 gVX1000_XCP_CalPage;
static VX1000_UINT8 gVX1000_ECU_CalPage;
#endif /* VX1000_MAILBOX_OVERLAY_CONTROL */
static void VX1000_SUFFUN(vx1000_EmemHdrInit)( void );
#if defined(VX1000_INIT_CAL_PAGE_INTERNAL)
static void VX1000_SUFFUN(vx1000_OverlayHwInit)( void );
#endif /* VX1000_INIT_CAL_PAGE_INTERNAL */
#if (defined(VX1000_OVERLAY_TLB) || (defined(VX1000_OVERLAY_DESCR_IDX)) || (defined(VX1000_MPC56xCRAM_BASE_ADDR) || defined(VX1000_SH2_FCU_BASE_ADDR))) && (!defined(VX1000_OVERLAY_VX_CONFIGURABLE))
static VX1000_UINT8 VX1000_SUFFUN(vx1000_MapCalRam)(VX1000_UINT32 size, VX1000_UINT32 target, VX1000_UINT32 source1, VX1000_UINT32 source2 );
#endif /* (VX1000_OVERLAY_TLB | VX1000_OVERLAY_DESCR_IDX | VX1000_MPC56xCRAM_BASE_ADDR | VX1000_SH2_FCU_BASE_ADDR) & !VX1000_OVERLAY_VX_CONFIGURABLE */
#if (defined(VX1000_MTDPP0) && defined(VX1000_MTDPP1)) && (defined(VX1000_MTDPP2) && defined(VX1000_MTDPP3))
static VX1000_UINT8 VX1000_SUFFUN(vx1000_MapCalRam)(VX1000_UINT32 size, VX1000_UINT32 target, VX1000_UINT32 source1, VX1000_UINT32 source2 );
#endif /* VX1000_MTDPP0 & VX1000_MTDPP1 & VX1000_MTDPP2 & VX1000_MTDPP3 */
#endif /* VX1000_OVERLAY & !VX1000_COMPILED_FOR_SLAVECORES */
#if (defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_OVERLAY_CONTROL)) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
#if defined(VX1000_GET_CAL_PAGE_INTERNAL)
static VX1000_UINT8 VX1000_SUFFUN(vx1000_GetCalPage)( VX1000_UINT8 segment, VX1000_UINT8 mode );
#endif /* VX1000_GET_CAL_PAGE_INTERNAL */
static VX1000_UINT8 VX1000_SUFFUN(vx1000_SetCalPage)( VX1000_UINT8 segment, VX1000_UINT8 page, VX1000_UINT8 mode, VX1000_UINT8 onStartup );
#endif  /* VX1000_MAILBOX && VX1000_MAILBOX_OVERLAY_CONTROL && !VX1000_COMPILED_FOR_SLAVECORES */
#if (defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_OVERLAY_CONTROL)) && ((!defined(VX1000_COMPILED_FOR_SLAVECORES)) && (!defined(VX1000_OVERLAY_VX_CONFIGURABLE)))
#if defined(VX1000_COPY_CAL_PAGE_INTERNAL)
static VX1000_UINT8 VX1000_SUFFUN(vx1000_CopyCalPage)( VX1000_UINT8 srcSeg, VX1000_UINT8 srcPage, VX1000_UINT8 dstSeg, VX1000_UINT8 dstPage );
#endif /* VX1000_COPY_CAL_PAGE_INTERNAL */
#endif  /* VX1000_MAILBOX && VX1000_MAILBOX_OVERLAY_CONTROL && !VX1000_COMPILED_FOR_SLAVECORES && !VX1000_OVERLAY_VX_CONFIGURABLE */
#if (defined(VX1000_OVERLAY)) && (defined(VX1000_OVERLAY_VX_CONFIGURABLE)) && (defined(VX1000_OVLENBL_REGWRITE_VIA_MX)) && (defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_OVERLAY_CONTROL))
static VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlayWriteEcuDescr)( void );
static VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlayReadEcuDescr)( void );
static VX1000_UINT8 VX1000_SUFFUN(vx1000_OvlWritEcuDescrMxHandler)(VX1000_UINT8 cmdMode, VX1000_UINT8 ovlIdx, VX1000_UINT32 ovccon );
#endif /* VX1000_OVERLAY & VX1000_OVERLAY_VX_CONFIGURABLE & VX1000_OVLENBL_REGWRITE_VIA_MX & VX1000_MAILBOX & VX1000_MAILBOX_OVERLAY_CONTROL*/

#if (defined(VX1000_MAILBOX) && (!defined(VX1000_COMPILED_FOR_SLAVECORES)))
#if (defined(VX1000_MAILBOX_OVERLAY_CONTROL) || defined(VX1000_MAILBOX_FLASH))
/* in current version, only accessed by one function, so made local to this function: static volatile VX1000_UINT32 gVX1000_MX_mta; */
#endif /* VX1000_MAILBOX_OVERLAY_CONTROL || VX1000_MAILBOX_FLASH */
#if defined(VX1000_MAILBOX_PRINTF)
static void VX1000_SUFFUN(vx1000_MailboxPutchar)( VX1000_CHAR character );
#endif /* VX1000_MAILBOX_PRINTF */
#endif /* VX1000_MAILBOX & !VX1000_COMPILED_FOR_SLAVECORES */

#if defined(VX1000_STIM)
static VX1000_UINT32 VX1000_SUFFUN(vx1000_SetTimeoutUs)(VX1000_UINT32 t);
static VX1000_UINT8 VX1000_SUFFUN(vx1000_CheckTimeout)(VX1000_UINT32 timeout);
#if defined(VX1000_STIM_FORCE_V1)
static VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassCheckCounters)(VX1000_UINT8 stim_event);
#if defined(VX1000_STIM_BY_OLDA)
static VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassCheckBuffer)(VX1000_UINT8 stim_event);
static VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassCopyBuffer)(VX1000_UINT8 stim_event);
#if defined(VX1000_BYPASS_ALL_CHANS_STIMD)
static VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassAreAllChansStimd)(void);
#endif /* VX1000_BYPASS_ALL_CHANS_STIMD */
#endif /* VX1000_STIM_BY_OLDA */
#endif /* VX1000_STIM_FORCE_V1 */
#if defined(VX1000_HOOK_BASED_BYPASSING)
static VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassHbbHookValid)(VX1000_UINT32 HookID);
#endif /* VX1000_HOOK_BASED_BYPASSING */
#endif /* VX1000_STIM */
#if defined(VX1000_DETECTION) || defined(VX1000_TARGET_TRICORE)
static void VX1000_SUFFUN(vx1000_DetectVxAsyncStart)(void);
static VX1000_UINT8 VX1000_SUFFUN(vx1000_DetectVxAsyncEnd)(void);
#endif /* VX1000_DETECTION || VX1000_TARGET_TRICORE */

#define VX1000_CALPAGE_RAM             1
#define VX1000_CALPAGE_FLASH           0


/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_InitAsyncStart                                                                                       */
/* API name:      VX1000_INIT_ASYNC_START                                                                                     */
/* Wrapper API:   VX1000If_InitAsyncStart                                                                                     */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:    This function must not be interrupted by any vx1000_* function.                                             */
/* Termination:   May leave internal data in an inconsistent state.                                                           */
/*                No problems on reactivation.                                                                                */
/* Precondition1: The timer used by VX1000_CLOCK() must be up and running.                                                    */
/* Description:   Initialises all global VX1000 data structures.                                                              */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
void VX1000_SUFFUN(vx1000_InitAsyncStart)(void)
{
#if defined(VX1000_TARGET_TRICORE) && (!defined(VX1000_SW_RSTOUT_AVAILABLE))
  VX1000_UINT8 InitAsyncReStarted = 0; /* TriCore may retain memory content over resets. This flag allows to skip re-initialisation of the AppDriver after resets */
#endif /* VX1000_TARGET_TRICORE && !VX1000_SW_RSTOUT_AVAILABLE*/

#if !defined(VX1000_COMPILED_FOR_SLAVECORES)
  /* optional todo: check HW reset state: if no hard reset, assume VX1000-data already/still being valid and skip initialisation ... */
#if defined(VX1000_RUNNING_ON_MAINCORE)
  if (VX1000_RUNNING_ON_MAINCORE()) /* always true on single-core MCU, otherwise needs compiler specific assembly */
#endif /* VX1000_RUNNING_ON_MAINCORE */
  {
#if defined(VX1000_TARGET_TRICORE)
    /* Tricore specific trace hardware initialisation: */

#if !defined(VX1000_SW_RSTOUT_AVAILABLE)  /* TODO: this config switch shall be renamed because it actually specifies whether the RSTOUT communication between ECU and FW works or not */
    /* Check if last Reset was a SW Reset */
    VX1000_UINT32 scuRstatMasked;
    if (VX1000_ECU_IS_AURIX()) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    {
      scuRstatMasked = (VX1000_ADDR_TO_PTR2VU32(0xF0036050UL))[0] & 0x00000010UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
    else
    {
      scuRstatMasked = (VX1000_ADDR_TO_PTR2VU32(0xF0000550UL))[0] & 0xFFFFFFFEUL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
    if (scuRstatMasked == 0x00000010UL)
    {
      /* Check if the gVX1000 data structure is still valid */
      if ((gVX1000.MagicId == (VX1000_STRUCT_MAGIC)) && (gVX1000.Version == (VX1000_STRUCT_VERSION)))
      {
        VX1000_UINT32 t, n;

        /* Re-enable OLDA trace */
        /* Disable OLDA write trap generation (derivative dependent) */
        switch ((VX1000_MCREG_CBS_JTAGID) & (VX1000_JTAGID_PN_MASK)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        {
          case VX1000_JTAGID_PN_TC172x:
          case VX1000_JTAGID_PN_TC1798:
            (VX1000_ADDR_TO_PTR2VU32(0xF8000520UL))[0] = 0x00000003UL; /* AudoMax trace control register OVRCON */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            break;
          case VX1000_JTAGID_PN_TC1387:
          case VX1000_JTAGID_PN_TC1767:
          case VX1000_JTAGID_PN_TC178x:
          case VX1000_JTAGID_PN_TC1797:
            (VX1000_ADDR_TO_PTR2VU32(0xF8700804UL))[0] = 0x00000003UL; /* TriCore Memory Control Register register LMU_MEMCON */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            break;
          case VX1000_JTAGID_PN_TC32x:
          case VX1000_JTAGID_PN_TC33x:
          case VX1000_JTAGID_PN_TC33xED:
          case VX1000_JTAGID_PN_TC35x:
          case VX1000_JTAGID_PN_TC36x:
          case VX1000_JTAGID_PN_TC37x:
          case VX1000_JTAGID_PN_TC37xED:
          case VX1000_JTAGID_PN_TC38x:
          case VX1000_JTAGID_PN_TC39x:
            (VX1000_ADDR_TO_PTR2VU32((((VX1000_MCREG_CBS_JTAGID) & (0xF0000000UL)) == (0x10000000UL)) ? 0xF8708430UL : 0xF8700430UL))[0] |= 0x00000001UL; /* AurixPlus Bridge Control Register BRCON */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            break;
          default:
            (VX1000_ADDR_TO_PTR2VU32(0xF8700820UL))[0]  = 0x00000003UL; /* Aurix Memory Control Register LMU_MEMCON */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            break;
        }

        VX1000_SPECIAL_EVENT((VX1000_ECU_EVT_SW_RESET)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* macro expansion contains VX1000_ADDR_TO_PTR2XXX: cannot avoid violating MISRA rule 11.3 because in a few configurations a peripheral register at a fix HW address must be accessed via pointer to update the HW value */
        gVX1000.ToolDetectState |= VX1000_TDS_APPRST;

        /* Wait 10ms */
        /* First create enough trace traffic to empty the tile buffer */
        /* Give VX a chance to set the timestamp offset */
        t = VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        n = 0;
        while (((VX1000_CLOCK()) - t) < ((VX1000_CLOCK_TICKS_PER_MS) * 10UL)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        {
          for (n++; n < 0x1000UL; n++)  /* Generate max. 64K Trace data */
          {
            VX1000_TIMESTAMP() /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            VX1000_TIMESTAMP() /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            VX1000_TIMESTAMP() /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            VX1000_TIMESTAMP() /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            VX1000_TIMESTAMP() /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
        }
        InitAsyncReStarted = 1;
      }
      else
      {
        VX1000_SPECIAL_EVENT((VX1000_ECU_EVT_SW_RESET_FAIL)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      }
    }
#endif /* !VX1000_SW_RSTOUT_AVAILABLE */
#if !defined(VX1000_SW_RSTOUT_AVAILABLE)
    if (0==InitAsyncReStarted)
#endif /* !VX1000_SW_RSTOUT_AVAILABLE */
#endif /* VX1000_TARGET_TRICORE */
    {
#if defined(VX1000_LFAST_BASEADDR)
#if defined(VX1000_SIPI_BASEADDR)
      (VX1000_ADDR_TO_PTR2VU32((VX1000_SIPI_BASEADDR ) + 0x9CUL /*MCR  */))[0]  = 0x00000001UL; /* Soft-reset the SIPI module  */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* !VX1000_SIPI_BASEADDR */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x30UL /*TIER */))[0]  = 0x00000000UL; /* Disable any interrupt generation by LFAST based on Tx activities */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x34UL /*RIER */))[0]  = 0x00000000UL; /* Disable any interrupt generation by LFAST based on Rx activities */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x38UL /*RIIER*/))[0]  = 0x00000000UL; /* Disable any interrupt generation by LFAST based on master commands */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x00UL /*MCR  */))[0] |= 0x00000002UL; /* Soft-reset the LFAST module */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      VX1000_SIPI_CLK_TXN_TXP_RXN_RXP()                                                         /* Let the user perform the derivative dependent initialisation of the LFAST CLKOUT pin and the four LVDS data pins */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x14UL /*RCDCR*/))[0]  = 0x000F0000UL; /* Use maximum cycles for each clock change */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x18UL /*SLCR */))[0]  = 0x12015F02UL; /* Use moderate cycles for each power mode change */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x00UL /*MCR  */))[0] &=~0x00008000UL; /* Disable LFAST and switch to slow speed to allow changing the high speed frequency */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x3CUL /*PLLCR*/))[0] |= 0x00020000UL; /* Power off the VCO before changing settings */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if !defined(VX1000_LFAST_FORCE_LOWSPEED)
#if defined(VX1000_LFAST_REFCLOCK_IS_10MHZ)                                                     /* FSL explicitly discourages usage of 13 or 26 MHZ, so only 10 or 20 MHz are supported */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x3CUL /*PLLCR*/))[0]  = 0x00004000UL | ((32-(VX1000_LFAST_PLLMULOFS)) << 2); /* Multiply reference by 32 for fast clock */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else  /* !VX1000_LFAST_REFCLOCK_IS_10MHZ (--> 20MHz) */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x3CUL /*PLLCR*/))[0]  = 0x00004000UL | ((16-(VX1000_LFAST_PLLMULOFS)) << 2); /* Multiply reference by 16 for fast clock */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* !VX1000_LFAST_REFCLOCK_IS_10MHZ (--> 20MHz) */
#if defined(VX1000_LFAST_HIGHSPEED_DIV_BY_2)                                                    /* Undocumented feature: on some STM derivatives the PLL/communication is said to support slow-down from standard 320MHz to 160MHz for less bit errors */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x3CUL /*PLLCR*/))[0] |= 0x00000001UL; /* Patch input divider */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#elif defined(VX1000_LFAST_HIGHSPEED_DIV_BY_4)                                                  /* Undocumented feature: on some STM derivatives the PLL/communication is said to support slow-down from standard 320MHz to 80MHz for even less bit errors */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x3CUL /*PLLCR*/))[0] |= 0x00000003UL; /* Patch input divider */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#elif defined(VX1000_LFAST_HIGHSPEED_DIV_BY_3)                                                  /* Undocumented and discouraged feature: if 80MHz works well but is too slow but 160MHz does not work, these 106.667MHz MIGHT be a tradeoff but this odd value is not officially mentioned so try on your own risk */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x3CUL /*PLLCR*/))[0] |= 0x00000002UL; /* Patch input divider */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_LFAST_HIGHSPEED_DIV_BY_X */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x3CUL /*PLLCR*/))[0] |= 0x00010000UL; /* Power on the VCO with the new, consistent settings */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* !VX1000_LFAST_FORCE_LOWSPEED */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x40UL /*LCR  */))[0]  = 0x0000502CUL; /* Enable LVDS Tx output, pad reference, Rx termination, Rx bias */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x00UL /*MCR  */))[0] &=~0x80000000UL; /* Select slave mode (hint: dual mode is always active, so registers are writeable) */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x00UL /*MCR  */))[0] &=~0x00000008UL; /* Do not use CTS method */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x00UL /*MCR  */))[0] |= 0x00000001UL; /* Allow data transmission */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if defined(VX1000_LFAST_REFCLOCK_IS_10MHZ)
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x00UL /*MCR  */))[0] &=~0x00010000UL; /* Divide reference by 2 for slow clock */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else  /* !VX1000_LFAST_REFCLOCK_IS_10MHZ (--> 20MHz) */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x00UL /*MCR  */))[0] |= 0x00010000UL; /* Divide reference by 4 for slow clock */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* !VX1000_LFAST_REFCLOCK_IS_10MHZ (--> 20MHz) */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x00UL /*MCR  */))[0] |= 0x00008000UL; /* Re-enable LFAST */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x00UL /*MCR  */))[0] |= 0x00006000UL; /* Enable the receiver and the transmitter */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x20UL /*PICR */))[0] |= 0x00008000UL; /* Enable auto-ping */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x04UL /*SCR  */))[0] &=~0x00010000UL; /* Disable remote control of clock to allow writing the speed bits */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if defined(VX1000_LFAST_FORCE_LOWSPEED)
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x04UL /*SCR  */))[0] &=~0x00000101UL; /* Select slow clock for both Rx and Tx */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else  /* !VX1000_LFAST_FORCE_LOWSPEED */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x04UL /*SCR  */))[0] |= 0x00000101UL; /* Select fast clock for both Rx and Tx */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* !VX1000_LFAST_FORCE_LOWSPEED */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0x00UL /*MCR  */))[0] &=~0x00000010UL; /* Start handling SIPI messages */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_LFAST_BASEADDR) + 0xA0UL /*RIISR*/))[0]  = 0xFFFFFFFFUL; /* Clear any pending LFAST interrupt requests */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if defined(VX1000_SIPI_BASEADDR)
      (VX1000_ADDR_TO_PTR2VU32((VX1000_SIPI_BASEADDR ) + 0x9CUL /*MCR  */))[0] |= 0x00000002UL; /* Enable the SIPI module */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_SIPI_BASEADDR ) + 0x9CUL /*MCR  */))[0] |= 0x00000008UL; /* Enable SIPI target functionality */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_SIPI_BASEADDR ) + 0x9CUL /*MCR  */))[0] |= 0x00000004UL; /* Switch to INIT mode to allow changing the configuration bits */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_SIPI_BASEADDR ) + 0x40UL /*CCR2 */))[0] |= 0x00000020UL; /* Enable channel 2 */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      (VX1000_ADDR_TO_PTR2VU32((VX1000_SIPI_BASEADDR ) + 0x9CUL /*MCR  */))[0] &=~0x00000004UL; /* Switch back to normal mode */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* !VX1000_SIPI_BASEADDR */
#endif /* VX1000_LFAST_BASEADDR */

      /* Initialise the gVX1000 data structure */
      gVX1000.MagicId = 0UL;
      gVX1000.EventTimestamp = VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      /* note that gVX1000.EventNumber must NOT be touched here, to be sure no event is triggered */
      gVX1000.Version = VX1000_STRUCT_VERSION; /* note: bits 8..31 are checked by old FW versions to be 0x000000 */
      gVX1000.GetIdPtr = VX1000_ECUID_PTR; /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because this integer variable either contains a pointer to an actual object or an integral magic number for "invalid" */  /* PRQA S 0309 */ /* not violating MISRA rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
      gVX1000.GetIdLen = VX1000_ECUID_LEN;
      gVX1000.XcpMailboxPtr = VX1000_MAILBOX_PTR; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */ /* PRQA S 0309 */ /* not violating MISRA rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
      gVX1000.StimCtrlPtr = VX1000_STIM_PTR; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */ /* PRQA S 0309 */ /* not violating MISRA rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
      gVX1000.RamSynchField = 0UL;
      gVX1000.ToolDetectState = 0UL;
      gVX1000.OldaPtr = VX1000_OLDA_PTR; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */ /* PRQA S 0309 */ /* not violating MISRA rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
      gVX1000.OvlPtr = VX1000_OVL_PTR; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */ /* PRQA S 0309 */ /* not violating MISRA rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
      gVX1000.ResMgmtPtr = VX1000_RES_MGMT_PTR; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */ /* PRQA S 0309 */ /* not violating MISRA rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
#if defined(VX1000_TARGET_TRICORE)
      gVX1000.OldaEventNumber = 0xF00004A0UL; /* on TriCore, we support ONLY the TrigReg methods (via VX1000_MCREG_OCDS_TRIGS), not the RAM method */
#elif defined(VX1000_OLDA_AUDMBR_REG_ADDR)
      gVX1000.OldaEventNumber = VX1000_OLDA_AUDMBR_REG_ADDR; /* on SHx, we support conditionally the TrigReg method (via AUD_MBR) */
#elif defined(VX1000_OLDA_DTS_BASE_ADDR)
      gVX1000.OldaEventNumber = VX1000_OLDA_DTS_BASE_ADDR;
#else  /* !VX1000_TARGET_TRICORE & !VX1000_OLDA_AUDMBR_REG_ADDR & !VX1000_OLDA_DTS_BASE_ADDR */
      gVX1000.OldaEventNumber = 0UL;
#endif /* !VX1000_TARGET_TRICORE & !VX1000_OLDA_AUDMBR_REG_ADDR & !VX1000_OLDA_DTS_BASE_ADDR */
      gVX1000.CalPtr = 0xFFFFFFFFUL ^ gVX1000.OldaEventNumber; /* note: unused CalPtr abused for OLDA in v3.x */
      gVX1000.ToolCtrlState = 0UL;
      gVX1000.FileVersion = 0x79A7U; /* not used anymore starting with release 3.0, but still evaluated by old FW versions */
      gVX1000.ErrLogAddr = VX1000_ERRLOG_ADDR; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      gVX1000.ErrLogSize = VX1000_ERRLOG_SIZE;
      gVX1000.ErrLogIndex = 0;
#if VX1000_ERRLOG_SIZE != 0   /* signal that the ring buffer index has not wrapped, yet: */
      (VX1000_ADDR_TO_PTR2U16(VX1000_ERRLOG_ADDR)  )[(VX1000_ERRLOG_SIZE) - 1] = 0U; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_ERRLOG_SIZE */
      /* "slow clock support dropped to be able to support GHz clocks ..."
      /if ((VX1000_CLOCK_TICKS_PER_US) > 4294967UL)
      /{
      /  VX1000_ERRLOGGER(VX1000_ERRLOG_TM_RESO_TOO_HIGH)
      /  gVX1000.TimerFreqHz = 0; // resolution cannot be expressed in 32 bits
      /}
      /else
      */
      {
        gVX1000.TimerFreqkHz = VX1000_CLOCK_TICKS_PER_MS;
      }
      gVX1000.TimerAddr = VX1000_CLOCK_TIMER_ADDR; /* zero is interpreted as "not accessible" */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      gVX1000.TimerAddrRegion = (VX1000_CLOCK_TIMER_ADDRREGION) | ((VX1000_CLOCK_TIMER_REVERSE) << 6); /* the VX FW currently ignores this an uses always 0="memory-mapped hardware register in standard region" */
      gVX1000.TimerSize = VX1000_CLOCK_TIMER_SIZE; /* zero is interpreted as "not accessible" */
      gVX1000.TimerOffset = 0; /* the VX FW currently supports only 0="the LSB of the 32bit-value at TimerAddr is also the LSB of the actual count" */
      gVX1000.TimerDirection = 0;/* the VX FW currently supports only up-counting timers */
      gVX1000.StimDurArrayPtr = VX1000_STIM_DURARRAY_PTR;  /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      gVX1000.OldaDurArrayPtr = VX1000_OLDA_DURARRAY_PTR;  /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      gVX1000.FeatureFlags0 = 0UL
#if defined(VX1000_FKL_SUPPORT_ADDR)
                            | (VX1000_ECUFEAT_FKLINSTRUMENTED)
#endif /* VX1000_FKL_SUPPORT_ADDR */
#if defined(VX1000_STIM) && defined(VX1000_STIMWAIT_BENCHMARK)
                            | (VX1000_ECUFEAT_STIMWT_DURATION)
#endif /* VX1000_STIM && VX1000_STIMWAIT_BENCHMARK */
#if defined(VX1000_STIM) && defined(VX1000_STIM_BENCHMARK)
                            | (VX1000_ECUFEAT_STIMRT_DURATION)
#endif /* VX1000_STIM && VX1000_STIM_BENCHMARK */
#if (defined(VX1000_STIM) && defined(VX1000_STIM_BENCHMARK)) && defined(VX1000_STIM_HISTOGRAM)
                            | (VX1000_ECUFEAT_STIMRT_HISTOGR)
#endif /* VX1000_STIM && VX1000_STIM_BENCHMARK && VX1000_STIM_HISTOGRAM*/
#if defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_OVERLAY_CONTROL)
                            | (VX1000_ECUFEAT_MX_CALPAGE_GSC)
#endif /* VX1000_MAILBOX && VX1000_MAILBOX_OVERLAY_CONTROL */
#if defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_CAL_READ_WRITE)
                            | (VX1000_ECUFEAT_MX_CALPAGE_RW)
#endif /* VX1000_MAILBOX && VX1000_MAILBOX_CAL_READ_WRITE */
#if defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_FLASH)
                            | (VX1000_ECUFEAT_MX_FLASHSTART)
#endif /* VX1000_MAILBOX && VX1000_MAILBOX_FLASH */
#if defined(VX1000_ADDONS_FEATUREFLAGS0)
                            | VX1000_ADDONS_FEATUREFLAGS0
#endif /* VX1000_ADDONS_FEATUREFLAGS0 */
                            ;
      if((((0
      || ( (sizeof(VX1000_CHAR     )) != 1))
      || (((sizeof(VX1000_INT8     )) != 1 )
      || ( (sizeof(VX1000_INT16    )) != 2)))
      ||((((sizeof(VX1000_INT32    )) != 4 )
      || ( (sizeof(VX1000_UINT8    )) != 1))
      || (((sizeof(VX1000_UINT16   )) != 2 )
      || ( (sizeof(VX1000_UINT32   )) != 4))))
      ||((((sizeof(VX1000_VUFUNCP_T)) != 4 )
      || ( (sizeof(VX1000_VVFUNCP_T)) != 4))
#if defined(VX1000_UINT64)
      || (((sizeof(VX1000_UINT64   )) != 8))
#endif /* VX1000_UINT64 */
      ))
      {
        gVX1000.ToolDetectState |= VX1000_TDS_ERROR;
        VX1000_ERRLOGGER(VX1000_ERRLOG_TYPESIZE_INVAL)
      }
      if ( (VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.ResMgmtPtr)) != (0x40UL + VX1000_PTR2VVX_TO_ADDRESS(&gVX1000)) ) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      {
        gVX1000.ToolDetectState |= VX1000_TDS_ERROR;
        VX1000_ERRLOGGER(VX1000_ERRLOG_STRUCTS_PADDED)
      }
      gVX1000.StaticLutAddr = 0;
      gVX1000.DynamicAddressSignalsCnt = 0;
      gVX1000.DynamicLutAddr = 0;
      gVX1000.MemsyncLutAddr = 0;
#if defined(VX1000_OLDA)
      VX1000_SUFFUN(vx1000_OldaInit)();
#endif /* VX1000_OLDA */
#if defined(VX1000_MAILBOX) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
      VX1000_SUFFUN(vx1000_MailboxInit)();
#endif /* VX1000_MAILBOX & !VX1000_COMPILED_FOR_SLAVECORES */
#if defined(VX1000_OVERLAY)
      VX1000_SUFFUN(vx1000_OverlayInit)();
#endif /* VX1000_OVERLAY */
#if defined(VX1000_RES_MGMT)
      VX1000_SUFFUN(vx1000_ResMgmtInit)();
#endif /* VX1000_RES_MGMT */
#if defined(VX1000_ADDONS_INIT)
      VX1000_ADDONS_INIT();
#endif /* VX1000_ADDONS_INIT */
#if defined(VX1000_STIM)
      VX1000_SUFFUN(vx1000_StimInit)();
#if defined(VX1000_STIM_BENCHMARK)
      VX1000_SUFFUN(vx1000_StimBenchmarkInit)();
#endif /* VX1000_STIM_BENCHMARK */
      gVX1000.Stim.TimeoutCtr = 0;
      gVX1000.Stim.TimeoutCtr2 = 0;
#endif /* VX1000_STIM */
      if ((VX1000_IS_BYPASS_RESUME_ALLOWED()) == 0) /* PRQA S 3325 */ /* cannot avoid violating MISRA rule 14.1 because of dependency on user-defined callback not guaranteed to be evaluable at compile time */
      { /* PRQA S 3201 */ /* cannot avoid violating MISRA rule 14.1 because of dependency on user-defined callback not guaranteed to be evaluable at compile time */
        gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_NO_BYP_RESUME);
      }
      gVX1000.MemSyncTrigPtr = VX1000_MEMSYNC_TRIGGER_PTR; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      gVX1000.Padding32      = 0UL;
      gVX1000.MemSyncCpyPtr  = VX1000_MEMSYNC_COPY_PTR; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      {
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0 )
        VX1000_UINT32 i;
#if ((VX1000_MEMSYNC_COPY_COUNT) > 0 )
        volatile VX1000_MEMSYNC_CPY_T *p2 = VX1000_ADDR_TO_PTR2VMC(gVX1000.MemSyncCpyPtr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_MEMSYNC_COPY_COUNT */
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 1 )
        volatile VX1000_UINT32 *p = VX1000_ADDR_TO_PTR2VU32(gVX1000.MemSyncTrigPtr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 1 */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 1)
        p[0] = (VX1000_UINT32)(VX1000_MEMSYNC_TRIGGER_PTR0);
        p[1] = (VX1000_UINT32)(VX1000_MEMSYNC_TRIGGER_PTR1);
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 1 */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 2)
        p[2] = (VX1000_UINT32)(VX1000_MEMSYNC_TRIGGER_PTR2);
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 2 */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 3)
        p[3] = (VX1000_UINT32)(VX1000_MEMSYNC_TRIGGER_PTR3);
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 3 */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 4)
        p[4] = (VX1000_UINT32)(VX1000_MEMSYNC_TRIGGER_PTR4);
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 4 */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 5)
        p[5] = (VX1000_UINT32)(VX1000_MEMSYNC_TRIGGER_PTR5);
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 5 */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 6)
        p[6] = (VX1000_UINT32)(VX1000_MEMSYNC_TRIGGER_PTR6);
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 6 */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 7)
        p[7] = (VX1000_UINT32)(VX1000_MEMSYNC_TRIGGER_PTR7);
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 7 */
        for (i = 0; i < (VX1000_MEMSYNC_TRIGGER_COUNT); ++i)
        {
#if (VX1000_MEMSYNC_TRIGGER_COUNT == 1)
          volatile VX1000_MEMSYNC_TRIG_T *pMSTstruct = VX1000_ADDR_TO_PTR2VMT(gVX1000.MemSyncTrigPtr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else  /* VX1000_MEMSYNC_TRIGGER_COUNT > 1 */
          volatile VX1000_MEMSYNC_TRIG_T *pMSTstruct = VX1000_ADDR_TO_PTR2VMT(p[i]); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 1 */
          pMSTstruct[0].Reserved1 = 0U;
          pMSTstruct[0].Reserved0 = 0;
          pMSTstruct[0].StructVersion = 0;
          pMSTstruct[0].MagicId = (VX1000_UINT32)(VX1000_STRUCT_MAGIC_MEMSYNCTRIG);
        }
#if ((VX1000_MEMSYNC_COPY_COUNT) > 0 )
        for (i = 0; i < (VX1000_MEMSYNC_COPY_COUNT); ++i)
        {
          p2[i].SyncChunkSize = 0UL;
          p2[i].SyncWinSize = 0UL;
          p2[i].SyncWinStart = 0UL;
          p2[i].SyncTriggerId = 0;
          p2[i].SyncMasterId = 0;
          p2[i].SyncWinStatus = 0;
          p2[i].StructVersion = 0;
          p2[i].MagicId = (VX1000_UINT32)(VX1000_STRUCT_MAGIC_MEMSYNCCPY);
        }
#endif /* VX1000_MEMSYNC_COPY_COUNT */
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */
      }

      gVX1000.Padding8        = 0;
      gVX1000.MemSyncTrigCnt  = (VX1000_UINT8)(VX1000_MEMSYNC_TRIGGER_COUNT);
      gVX1000.Padding16       = 0;
      gVX1000.MemSyncCpyCnt   = (VX1000_UINT8)(VX1000_MEMSYNC_COPY_COUNT);
      gVX1000.ReleaseNameHead = VX1000_RELEASECODEH(VX1000_RELEASENAMEARRAY);
      gVX1000.ReleaseNameBody = VX1000_RELEASECODEB(VX1000_RELEASENAMEARRAY);
      gVX1000.ReleaseNameTail = VX1000_RELEASECODET(VX1000_RELEASENAMEARRAY);
      gVX1000.BuildNumber = (VX1000_APPDRIVER_BUILDNUMBER) | (((VX1000_UINT32)(VX1000_APPDRIVER_CONFIGVERSION)) << 24);

#if defined(VX1000_TARGET_TRICORE)
      /* TriCore: Lock DAP in 2 PIN mode to improve tool communication during coldstart */
      if (VX1000_ECU_IS_AURIX()) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      {
        /* Register is ENDINIT protected! */
        /* ?! VX1000_MCREG_OCDS_OIFM =  0x00000000UL; ?! */ /* Two-pin DAP (legacy) */
        ; /* Semicolon here for MISRA */
      }
      else
      {
        /* This is only possible in 2 PIN mode with the VX1542 FPGA POD */
        VX1000_MCREG_OCDS_OCNTRL = 0x00000007UL;  /* DJMODE = 01 Two-pin DAP with disabled BYPASS */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      }
#endif /* VX1000_TARGET_TRICORE */
      gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_INIT);
#if defined(VX1000_SFR_ADDR_FOR_MAGIG_ID)
      gVX1000.MagicIdSfrAddr = (VX1000_UINT32)(VX1000_SFR_ADDR_FOR_MAGIG_ID);
      ((VX1000_ADDR_TO_PTR2VU32(gVX1000.MagicIdSfrAddr)))[0] = (VX1000_UINT32)(VX1000_STRUCT_MAGIC); /* standard pointers of the used memory model not necessarily reach the SFRs, so instead of simple "VX1000_ADDR_TO_PTR2VU32" a more complex cast like "((volatile VX1000_UINT32 VX1000_DECL_PTR *)" MIGHT be required */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else  /* !VX1000_SFR_ADDR_FOR_MAGIG_ID */
      gVX1000.MagicIdSfrAddr = 0xFFFFFFFFUL;
#endif /* !VX1000_SFR_ADDR_FOR_MAGIG_ID */
      gVX1000.MagicId = (VX1000_UINT32)(VX1000_STRUCT_MAGIC);
#if defined(VX1000_DETECTION) || defined(VX1000_TARGET_TRICORE)
      VX1000_SUFFUN(vx1000_DetectVxAsyncStart)();
#endif /* VX1000_DETECTION || VX1000_TARGET_TRICORE */
      VX1000_SPECIAL_EVENT(VX1000_EVENT_STRUCT_INIT) /* PRQA S 0303 */ /* cannot avoid violating MISRA rule 11.3 because in a few configurations a peripheral register at a fix HW address must be accessed via pointer to update the HW value */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */ /* PRQA S 0501 */ /* Violation of MISRA rule 12.8 because 64-bit data types are known to be not MISRA-conformant. This is just an example to be overruled by a user-defined implementation of an atomic 64-bit-store operation. */
    }
  }
#if defined(VX1000_RUNNING_ON_MAINCORE)
  else
#endif /* VX1000_RUNNING_ON_MAINCORE */
#endif /* !VX1000_COMPILED_FOR_SLAVECORES */
#if defined(VX1000_RUNNING_ON_MAINCORE) || defined(VX1000_COMPILED_FOR_SLAVECORES)
  {
    while (gVX1000.MagicId != (VX1000_UINT32)VX1000_STRUCT_MAGIC)
    {
      ; /* busy wait until another core initialised VX1000_DATA */
    }
  }
#endif /* VX1000_RUNNING_ON_MAINCORE | VX1000_COMPILED_FOR_SLAVECORES */

  /* optional: VX1000_BreakpointInit(); /-* has to be performed per core */
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_InitAsyncEnd                                                                                         */
/* API name:      VX1000_INIT_ASYNC_END                                                                                       */
/* Wrapper API:   VX1000If_InitAsyncEnd                                                                                       */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:    This function must not be interrupted by any vx1000_* function.                                             */
/* Termination:   May leave internal data in an inconsistent state.                                                           */
/*                No problems on reactivation.                                                                                */
/* Precondition1: vx1000_InitAsyncStart must have been called.                                                                */
/* Description:   Waits for end of start-up handshake with VX; Synchronises instrumentation on the cores.                     */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
void VX1000_SUFFUN(vx1000_InitAsyncEnd)(void)
{
#if defined(VX1000_RUNNING_ON_MAINCORE) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
  if (0==VX1000_RUNNING_ON_MAINCORE())
#endif /* VX1000_RUNNING_ON_MAINCORE & !VX1000_COMPILED_FOR_SLAVECORES */
#if defined(VX1000_RUNNING_ON_MAINCORE) || defined(VX1000_COMPILED_FOR_SLAVECORES)
  {
    if ((gVX1000.ToolDetectState & (VX1000_UINT32)((VX1000_TDS_COLDSTART_DELAY_REQ) | (VX1000_TDS_COLDSTART_DELAY))) != 0)
    {
      while (0 == (gVX1000.ToolDetectState & (VX1000_UINT32) ((VX1000_TDS_COLDSTART_DONE) | (VX1000_TDS_COLDSTART_TIMEOUT))))
      {
        ; /* busy wait for the main core to complete the handshake */
      }
    }
#if defined(VX1000_OVLENBL_HW_INIT_PER_CORE)
#if defined(VX1000_OVERLAY) && (!defined(VX1000_OVERLAY_VX_CONFIGURABLE))
#if defined(VX1000_INIT_CAL_PAGE_INTERNAL)
    VX1000_SUFFUN(vx1000_OverlayHwInit)();
#elif defined(VX1000_INIT_CAL_PAGE)
    VX1000_INIT_CAL_PAGE()
#endif /* VX1000_INIT_CAL_PAGE && !VX1000_INIT_CAL_PAGE_INTERNAL */
#endif /* VX1000_OVERLAY && !VX1000_OVERLAY_VX_CONFIGURABLE */
#endif /* VX1000_OVLENBL_HW_INIT_PER_CORE */
  }
#endif /* VX1000_RUNNING_ON_MAINCORE | VX1000_COMPILED_FOR_SLAVECORES */
#if defined(VX1000_RUNNING_ON_MAINCORE) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
  else
#endif /* VX1000_RUNNING_ON_MAINCORE & !VX1000_COMPILED_FOR_SLAVECORES */
#if !defined(VX1000_COMPILED_FOR_SLAVECORES)
#if defined(VX1000_DETECTION) || defined(VX1000_TARGET_TRICORE)
  if (VX1000_SUFFUN(vx1000_DetectVxAsyncEnd)() != 0) /* complete the VX1000 handshake; "0" means "no VX detected" */
  {
    ; /* If tool detection was successful, no action. Semicolon here for MISRA */
  }
  else
#endif /* VX1000_DETECTION || VX1000_TARGET_TRICORE */
  {
#if defined(VX1000_TARGET_TRICORE)
    /* Lock DAP in the user selected pin mode */
    if (VX1000_ECU_IS_AURIX()) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    {
      /* Register is ENDINIT protected! */
      /* ?! VX1000_MCREG_OCDS_OIFM = VX1000_OCDS_OIFM; ?! */
      ; /* Semicolon here for MISRA */
    }
    else
    {
      VX1000_MCREG_OCDS_OCNTRL = VX1000_OCDS_OCNTRL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
#else  /* !VX1000_TARGET_TRICORE */
    ; /* If tool detection was not successful, no action. Semicolon here for MISRA */
#endif /* !VX1000_TARGET_TRICORE */
  }
#if defined(VX1000_RUNNING_ON_MAINCORE)
  if (VX1000_RUNNING_ON_MAINCORE() != 0)
#endif /* VX1000_RUNNING_ON_MAINCORE */
  {
#if defined(VX1000_OVERLAY) && (!defined(VX1000_OVERLAY_VX_CONFIGURABLE))
#if defined(VX1000_INIT_CAL_PAGE_INTERNAL)
    VX1000_SUFFUN(vx1000_OverlayHwInit)();
#elif defined(VX1000_INIT_CAL_PAGE)
    VX1000_INIT_CAL_PAGE()
#endif /* VX1000_INIT_CAL_PAGE && !VX1000_INIT_CAL_PAGE_INTERNAL */
#endif /* VX1000_OVERLAY && !VX1000_OVERLAY_VX_CONFIGURABLE */
  }
#endif /* !VX1000_COMPILED_FOR_SLAVECORES */
}


/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_Init                                                                                                 */
/* API name:      VX1000_INIT                                                                                                 */
/* Wrapper API:   VX1000If_Init                                                                                               */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:    This function must not be interrupted by any vx1000_* function.                                             */
/* Termination:   May leave internal data in an inconsistent state.                                                           */
/*                No problems on reactivation.                                                                                */
/* Precondition1: The timer used by VX1000_CLOCK() must be up and running.                                                    */
/* Description:   Initialises all global VX1000 data structures and performs the VX startup handshaking.                      */
/* Devel state:   Deprecated (replaced by vx1000_InitAsyncStart)                                                              */
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Should not be used in new designs */
/* Only for compatibility in existing projects */
void VX1000_SUFFUN(vx1000_Init)(void)
{
  VX1000_INIT_ASYNC_START()
  VX1000_INIT_ASYNC_END()
}


/*------------------------------------------------------------------------------- */
/* Pre-restart handshake                                                          */
#if defined(VX1000_SOFTRESET_TIMEOUT_MS)

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_PrepareSoftreset                                                                                     */
/* API name:      VX1000_PREPARE_SOFTRESET (expression) / VX1000_PREPARE_SOFTRESET_VOID (statement)                           */
/* Wrapper API:   VX1000If_PrepareSoftreset (expression) / VX1000If_PrepareSoftresetVoid (statement)                          */
/* Return value:  0: reset procedure confirmed by tool / 1: handshake failed (measurement won't be resumed after the reset)   */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_InitAsyncEnd() must have been called.                                                                */
/* Description:   Informs the VX1000 base module about a planned software reset.                                              */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_PrepareSoftreset)(void)
{
  VX1000_UINT32 t0, dt;
  VX1000_UINT8 retVal = 0; /* 0="accepted", 1="initiated", 2="acknowledged", 3="handled", 4="confirmed", {5,6}="timeout" */

  if ((gVX1000.ToolCtrlState & (VX1000_TCS_PRESENT)) != 0)  /* Handshake is only needed when a VX is connected */
  {
    gVX1000.EventTimestamp = VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    t0 = gVX1000.EventTimestamp;
    dt = (VX1000_SOFTRESET_TIMEOUT_MS) * (VX1000_CLOCK_TICKS_PER_MS);
#if (VX1000_CLOCK_TIMER_SIZE < 32) && (VX1000_CLOCK_TIMER_SIZE != 0)
    if ( dt > ((1UL << (VX1000_CLOCK_TIMER_SIZE)) - 1) )
    {
      dt = (1UL << (VX1000_CLOCK_TIMER_SIZE)) - 1;
      VX1000_ERRLOGGER(VX1000_ERRLOG_TM_DTSR_TOO_LONG)
    }
#endif /* VX1000_CLOCK_TIMER_SIZE */

    /* trigger a special event */
    VX1000_SPECIAL_EVENT(VX1000_ECU_EVT_SW_RESET_PREP) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */ /* PRQA S 0501 */ /* Violation of MISRA rule 12.8 because 64-bit data types are known to be not MISRA-conformant. This is just an example to be overruled by a user-defined implementation of an atomic 64-bit-store operation. */
    retVal = 1;

    /* wait with timeout until the VX1000 acknowledges the special event by setting VX1000_TCS_SOFTRESET_PREP */
    while (retVal == 1)
    {
      if ((gVX1000.ToolCtrlState & (VX1000_UINT32)(VX1000_TCS_SOFTRESET_PREP)) != 0)
      {
        /* retVal = 2; */
        gVX1000.MagicId = 0;   /* destroy the MagicId */
        retVal = 3;

        /* wait with timeout until the VX1000 also confirmed that it saw the destroyed MagicId by resetting VX1000_TCS_SOFTRESET_PREP */
        while (retVal == 3)
        {
          if (0==(gVX1000.ToolCtrlState & (VX1000_UINT32)(VX1000_TCS_SOFTRESET_PREP)))
          {
            retVal = 4;
          }
          else if ((gVX1000.EventTimestamp - t0) >= dt)
          {
            retVal = 5; /* means "timeout" */
          }
          else
          {
            ; /* empty else is only here for MISRA */
          }
          gVX1000.EventTimestamp = VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        } /* while() */
      }
      else if ((gVX1000.EventTimestamp - t0) >= dt)
      {
        retVal = 6; /* means "timeout" or "rejected" */
      }
      else
      {
        ; /* empty else is only here for MISRA */
      }
      gVX1000.EventTimestamp = VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    } /* while() */
    if (retVal == 4) /* reduce the return values to a simplified, boolean meaning */
    {
      retVal = 0;
    }
    else
    {
      retVal = 1;
    }
  }
  return retVal;
}
#endif /* VX1000_SOFTRESET_TIMEOUT_MS */

/*------------------------------------------------------------------------------- */
/* Tool detection                                                                 */

#if defined(VX1000_DETECTION) || defined(VX1000_TARGET_TRICORE) /* on TriCore, this function is always present because it is needed in DAP init decision */

#if defined(VX1000_COLDSTART_CALIBRATION) /* obsolete feature */
/*todo: add check for existence && NZ of the defines and then move everything into the consistency check section in the header*/
#if (VX1000_OLDA_MEMORY_SIZE) < (((VX1000_COLDSTART_FLASH_SIZE) / (VX1000_COLDSTART_CHS_BLOCK_SIZE)) + 4) * 4
#error "VX1000_MEMORY_SIZE too small for FLASH checksum array"
#endif /* VX1000_OLDA_MEMORY_SIZE < ... */
#if ((VX1000_COLDSTART_FLASH_SIZE) % (VX1000_COLDSTART_CHS_BLOCK_SIZE)) != 0
#error "VX1000_COLDSTART_FLASH_SIZE % VX1000_COLDSTART_CHS_BLOCK_SIZE != 0"
#endif /* VX1000_COLDSTART_FLASH_SIZE % VX1000_COLDSTART_CHS_BLOCK_SIZE */

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_CalcFlashChecksum                                                                                    */
/* API name:      None                                                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:    TODO visza                                                                                                  */
/* Termination:   TODO visza                                                                                                  */
/* Precondition1: TODO visza                                                                                                  */
/* Description:   TODO visza                                                                                                  */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
/* todo: add the used global variables to the end of this module plus to the header plus add VX1000_COLDSTART_CHECKSUM_MAGIC */
static void VX1000_SUFFUN(vx1000_CalcFlashChecksum)( void ); /* todo: move up to prototype section */
static void VX1000_SUFFUN(vx1000_CalcFlashChecksum)( void )
{
  VX1000_UINT32 adr = (VX1000_UINT32)(VX1000_COLDSTART_FLASH_ADDR);
  VX1000_UINT32 i, *p;

  p = VX1000_ADDR_TO_PTR2U32(gVX1000.Olda.MemoryAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  p[0] = (VX1000_UINT32)(VX1000_COLDSTART_CHS_MAGIC);
  p[1] = (VX1000_UINT32)(VX1000_COLDSTART_FLASH_ADDR);
  p[2] = (VX1000_UINT32)(VX1000_COLDSTART_FLASH_SIZE);
  p[3] = (VX1000_UINT32)(VX1000_COLDSTART_CHS_BLOCK_SIZE);

  p = VX1000_ADDR_TO_PTR2U32(gVX1000.Olda.MemoryAddr + 4*4); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  for (i = 0; i < ((VX1000_COLDSTART_FLASH_SIZE) / (VX1000_COLDSTART_CHS_BLOCK_SIZE)); i++)
  {
    VX1000_UINT32 sum32 = 0;
    VX1000_UINT32 k, *data = VX1000_ADDR_TO_PTR2U32(adr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    for (k = 0; k <= (VX1000_COLDSTART_CHS_BLOCK_SIZE); k += 4UL * 8UL)
    {
      sum32 += data[0] + data[1] + data[2] + data[3] + data[4] + data[5] + data[6] + data[7];
      data = &data[8];
    }
    p[i] = sum32;
    adr += VX1000_COLDSTART_CHS_BLOCK_SIZE;
  }
}
#endif /* VX1000_COLDSTART_CALIBRATION */

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_DetectVxAsyncStart                                                                                   */
/* API name:      None (only used internally)                                                                                 */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: The timer used by VX1000_CLOCK() must be up and running.                                                    */
/* Description:   Detect, whether a VX1000 is connected; if yes, initiate tool handshake.                                     */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
static void VX1000_SUFFUN(vx1000_DetectVxAsyncStart)( void )
{
#if !defined(VX1000_COMPILED_FOR_SLAVECORES)
#if defined(VX1000_TARGET_TRICORE)
  VX1000_UINT32 tricoreToolCtrlState = 0UL; /* dummy initialisation to avoid compiler/MISRA warnings (value is always overwritten before usage) */
  VX1000_DUMMYREAD(tricoreToolCtrlState)    /* dummy usage to avoid compiler/MISRA warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
#endif /* VX1000_TARGET_TRICORE */
#if defined(VX1000_DETECT_XCP)
  /* Check whether there is a XCP on CAN connected and disable VX OLDA in that case */
  /* (Otherwise the probably-shared memory for the display tables would be overwritten by VX coldstart setup!)*/
  if ((xcp.SessionStatus & (SS_CONNECTED)) != 0)
  {
    gVX1000.Olda.MagicId = 0; /* XCP connected: Disable OLDA */
  }
  else
  {
    gVX1000.Olda.MagicId = VX1000_OLDA_MAGIC; /* VX is alone: Re-enable OLDA */
    /* todo: the VX will not start using olda now by itself -> we have to request this somehow */
  }
#endif /* VX1000_DETECT_XCP */

  /* Check if 1st call after reset (&& on multi-core systems, whether we run on the main core) */
  if ((0==(gVX1000.ToolDetectState & ((VX1000_UINT32)VX1000_TDS_DETECT))) /* PRQA S 3415*/ /* Actually not violating MISRA rule 12.4 because VX1000_RUNNING_ON_MAINCORE() by definition cannot have any desired side effect */
#if defined(VX1000_RUNNING_ON_MAINCORE)
  &&  (VX1000_RUNNING_ON_MAINCORE()) /* TODO: when not running on the main core, return gVX1000.ToolDetectState only if VX1000_DATA looks "already initialised"! */
#endif /* VX1000_RUNNING_ON_MAINCORE */
  )
  {
#if ((VX1000_CLOCK_TICKS_BASE_NS) < 100000UL) /* was: "#if ((VX1000_CLOCK_TICKS_PER_US) > 10)", but CLOCK_TICKS may be a function! */
    VX1000_UINT32 dt = (VX1000_DETECTION_TIMEOUT_US) * (VX1000_CLOCK_TICKS_PER_US);
#else /* VX1000_CLOCK_TICKS_BASE_NS */
    VX1000_UINT32 dt = ((VX1000_DETECTION_TIMEOUT_US) * (VX1000_CLOCK_TICKS_PER_MS)) / 1000UL;
#endif /* VX1000_CLOCK_TICKS_BASE_NS */
#if (VX1000_CLOCK_TIMER_SIZE < 32) && (VX1000_CLOCK_TIMER_SIZE != 0)
    if ( dt > ((1UL << (VX1000_CLOCK_TIMER_SIZE)) - 1) )
    {
      dt = (1UL << (VX1000_CLOCK_TIMER_SIZE)) - 1;
      VX1000_ERRLOGGER(VX1000_ERRLOG_TM_DTDT_TOO_LONG)
    }
#endif /* VX1000_CLOCK_TIMER_SIZE */
    gVX1000.EventTimestamp = VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    gVX1000.ToolDetectState |= (VX1000_UINT32)VX1000_TDS_DETECT;
    gVX1000_DETECT_StartTime = gVX1000.EventTimestamp;

#if defined(VX1000_COLDSTART_BENCHMARK) && defined(VX1000_OLDA)
    gVX1000_DETECT_StartTimeAsyncEnd = 0;
    gVX1000_DETECT_ToolDetectTime = 0;
    gVX1000_DETECT_ChecksumDoneTime = 0;
    gVX1000_DETECT_EndTime = 0;
    gVX1000_DETECT_EndTimeAsyncStart = 0;
#endif /* VX1000_COLDSTART_BENCHMARK && VX1000_OLDA */

    /* Wait some time for tool presence indication, when ECU was reset, the tool may need some time to react */
    /* Check timeout VX1000_DETECTION_TIMEOUT_US */
    do
    {
#if defined(VX1000_TARGET_TRICORE)
      /* Thanks to TriCore's DAP interface, VX1000_DETECTION_TIMEOUT_US may be as small as 1 us */
      /* (VX can access the TRIG register even if it can't access memory (gVX1000.ToolCtrlState/DetectState), yet) */
      if (VX1000_ECU_IS_AURIX()) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      {
        /* Phase I: Check tool presence (NIOS -> ECU: bitvector mode with Aurix indices; ECU -> NIOS: Aurix magic number mode) */
        switch ((VX1000_MCREG_CBS_JTAGID) & (VX1000_JTAGID_PN_MASK)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        {
          case VX1000_JTAGID_PN_TC21x:
          case VX1000_JTAGID_PN_TC22x:
          case VX1000_JTAGID_PN_TC23x:
          case VX1000_JTAGID_PN_TC32x:
          case VX1000_JTAGID_PN_TC33x:
          case VX1000_JTAGID_PN_TC33xED:
          case VX1000_JTAGID_PN_TC35x:
          case VX1000_JTAGID_PN_TC36x:
          case VX1000_JTAGID_PN_TC37x:
          case VX1000_JTAGID_PN_TC37xED:
          case VX1000_JTAGID_PN_TC38x:
          case VX1000_JTAGID_PN_TC39x:
            tricoreToolCtrlState = VX1000_MCREG_OCDS_TRIG5; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            break;
          default:
            tricoreToolCtrlState = VX1000_MCREG_OCDS_TRIG20; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            break;
        }
        /* Convert Aurix non-standard request bit indices to standard bit indices */
        tricoreToolCtrlState = (((tricoreToolCtrlState) & 0x00000003UL) | ((tricoreToolCtrlState << 4U) & 0x00000040UL)) | ((tricoreToolCtrlState << 3U) & 0x00000080UL);
      }
      else
      {
        /* Phase I: Check tool presence (NIOS -> ECU: TriCore magic number mode; ECU -> NIOS: TriCore magic number mode) */
        tricoreToolCtrlState = VX1000_MCREG_OCDS_TRIG; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        if ((tricoreToolCtrlState & 0x30636556UL) != 0x30636556UL) /* distinguish random VX1000_EVENT()s noise from requests with high reliability */
        {
          tricoreToolCtrlState = 0UL; /* the magic base pattern was not included, so clear all phantom requests */
        }
        /* Convert TriCore non-standard request bit indices to standard bit indices */
        tricoreToolCtrlState = ((tricoreToolCtrlState >> 24U) & 0x00000003UL) | ((tricoreToolCtrlState >> 20U) & 0x000000C0UL);
      }
      if ((tricoreToolCtrlState & ((VX1000_TCS_PRESENT) | (VX1000_TCS_COLDSTART_DELAY))) != 0) /* Check tool presence */
#else  /* !VX1000_TARGET_TRICORE */
      /* The following delays were measured in the NIOS until VX1000_TCS_PRESENT was written to */
      /* ToolCtrlState after a reset had been detected: */
      /*  * PowerPC: 1.5 ms at 1 MHz Nexus Clock */
      /*  * RH850:   todo */
      /*  * SH2:     todo */
      /*  * TMS570:  todo */
      /*  * V850:    todo */
      /*  * XC2000:  todo */
      if ((gVX1000.ToolCtrlState & ((VX1000_TCS_PRESENT) | (VX1000_TCS_COLDSTART_DELAY))) != 0) /* Check tool presence */
#endif /* !VX1000_TARGET_TRICORE */
      {
#if defined(VX1000_OVLENBL_KEEP_AWAKE)
#if defined(VX1000_TARGET_TRICORE)
        /* CBS_OSTATE.OJC[3..1]  0x001 for No_Powerfail, 0x011 for Powerfail */
        if ((tricoreToolCtrlState & (VX1000_TCS_CAL_WAKEUP)) != 0)
#else  /* !VX1000_TARGET_TRICORE */
        if (VX1000_CAL_WAKEUP_REQUESTED())
#endif /* !VX1000_TARGET_TRICORE */
        {
#if defined(VX1000_OVLENBL_RST_ON_CALWAKEUP)
          VX1000_OVL_RST_ON_CAL_WAKEUP_CB()
#endif /* VX1000_OVLENBL_RST_ON_CALWAKEUP */
          /* Decrement the presenceCounter so that cal_wakeup_active returns 1 when called next time */
          gVX1000.Ovl.ecuLastPresenceCounter--;
        }
#endif /* VX1000_OVLENBL_KEEP_AWAKE */

        /* Acknowledge tool detect */
        gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_DETECTED);
#if defined(VX1000_TARGET_TRICORE)
        if (VX1000_ECU_IS_AURIX()) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        {
          /* Also acknowledge tool detect in Aurix TRIGS register */
          switch ((VX1000_MCREG_CBS_JTAGID) & (VX1000_JTAGID_PN_MASK)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          {
            case VX1000_JTAGID_PN_TC21x:
            case VX1000_JTAGID_PN_TC22x:
            case VX1000_JTAGID_PN_TC23x:
            case VX1000_JTAGID_PN_TC32x:
            case VX1000_JTAGID_PN_TC33x:
            case VX1000_JTAGID_PN_TC33xED:
            case VX1000_JTAGID_PN_TC35x:
            case VX1000_JTAGID_PN_TC36x:
            case VX1000_JTAGID_PN_TC37x:
            case VX1000_JTAGID_PN_TC37xED:
            case VX1000_JTAGID_PN_TC38x:
            case VX1000_JTAGID_PN_TC39x:
              VX1000_MCREG_OCDS_TRIGS = ((10UL) + (VX1000_AURIX_TRIG_REG_OFS_V3)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
              break;
            default:
              VX1000_MCREG_OCDS_TRIGS = ((10UL) + (VX1000_AURIX_TRIG_REG_OFS)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
              break;
          }
        }
        else
        {
          /* Also acknowledge tool detect in TriCore TRIG registers */
          VX1000_MCREG_OCDS_TRIGC = 0xFFFFFFFFUL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          VX1000_MCREG_OCDS_TRIGS = 0x00005555UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        }
        if ((tricoreToolCtrlState & (VX1000_TCS_COLDSTART_DELAY)) != 0)
#else  /* !VX1000_TARGET_TRICORE */
        if ((gVX1000.ToolCtrlState & (VX1000_TCS_COLDSTART_DELAY)) != 0)
#endif /* !VX1000_TARGET_TRICORE */
        {
          gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_COLDSTART_DELAY_REQ);
        }
#if defined(VX1000_COLDSTART_BENCHMARK) && defined(VX1000_OLDA)
        gVX1000_DETECT_ToolDetectTime = gVX1000.EventTimestamp;
#endif /* VX1000_COLDSTART_BENCHMARK && VX1000_OLDA */

        break; /* note: this "early-out"-break can be removed safely if constant runtime is preferred (or for MISRA) */
      } /* Tool present */
      gVX1000.EventTimestamp = VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    } while ((gVX1000.EventTimestamp - gVX1000_DETECT_StartTime) < dt);
    gVX1000.ToolDetectState |= (VX1000_UINT32)VX1000_TDS_DETECT_DONE;
#if defined(VX1000_COLDSTART_BENCHMARK) && defined(VX1000_OLDA)
    gVX1000_DETECT_EndTimeAsyncStart = VX1000_CLOCK();  /* here, gVX1000.EventTimestamp may be outdated (break's path) */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_COLDSTART_BENCHMARK && VX1000_OLDA */
  } /* 1st call */
  else /* warmstart connect for all targets handled by the FW in standard bitvector mode (not by nios magic numbers for TriCore) */
  {
    /* Just Check whether the VX is connected now and update the state accordingly */
    if ((gVX1000.ToolCtrlState & ((VX1000_UINT32)(VX1000_TCS_PRESENT))) != 0)
    {
      gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_DETECTED); /* VX1000 connected */
    }
    else
    {
      gVX1000.ToolDetectState &= (VX1000_UINT32)~(VX1000_TDS_DETECTED); /* not connected */
    }
  }
#endif /* !VX1000_COMPILED_FOR_SLAVECORES */
}


/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_DetectVxAsyncEnd                                                                                     */
/* API name:      None (only used internally)                                                                                 */
/* Return value:  VX1000_TDS_DETECTED if detected; 0 otherwise.                                                               */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_DetectVxAsyncStart must have been called.                                                            */
/* Description:   Update connection status and stall for remaining delay if required by tool (for downloading the             */
/*                coldstart measurement setup).                                                                               */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_DetectVxAsyncEnd)( void )
{
#if defined(VX1000_RUNNING_ON_MAINCORE) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
  if (0==(VX1000_RUNNING_ON_MAINCORE()))
#endif /* VX1000_RUNNING_ON_MAINCORE & !VX1000_COMPILED_FOR_SLAVECORES */
#if defined(VX1000_RUNNING_ON_MAINCORE) || defined(VX1000_COMPILED_FOR_SLAVECORES)
  {
    if ((gVX1000.ToolDetectState & (VX1000_UINT32)((VX1000_TDS_COLDSTART_DELAY_REQ) | (VX1000_TDS_COLDSTART_DELAY))) != 0)
    {
      while (0 == (gVX1000.ToolDetectState & (VX1000_UINT32) ((VX1000_TDS_COLDSTART_DONE) | (VX1000_TDS_COLDSTART_TIMEOUT))))
      {
        ; /* busy wait for the main core to complete the handshake */
      }
    }
  }
#endif  /* VX1000_RUNNING_ON_MAINCORE | VX1000_COMPILED_FOR_SLAVECORES */
#if defined(VX1000_RUNNING_ON_MAINCORE) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
  else
#endif /* VX1000_RUNNING_ON_MAINCORE & !VX1000_COMPILED_FOR_SLAVECORES */
#if !defined(VX1000_COMPILED_FOR_SLAVECORES)
  if ((gVX1000.ToolDetectState & (VX1000_UINT32)(VX1000_TDS_COLDSTART_DELAY_REQ)) != 0)
  {
    VX1000_UINT32 dt = (VX1000_COLDSTART_TIMEOUT_MS) * (VX1000_CLOCK_TICKS_PER_MS);
#if (VX1000_CLOCK_TIMER_SIZE < 32) && (VX1000_CLOCK_TIMER_SIZE != 0)
    if ( dt > ((1UL << (VX1000_CLOCK_TIMER_SIZE)) - 1) )
    {
      dt = (1UL << (VX1000_CLOCK_TIMER_SIZE)) - 1;
      VX1000_ERRLOGGER(VX1000_ERRLOG_TM_DTCS_TOO_LONG)
    }
#endif /* VX1000_CLOCK_TIMER_SIZE */
    gVX1000.EventTimestamp = VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */

#if defined(VX1000_COLDSTART_BENCHMARK) && defined(VX1000_OLDA)
    gVX1000_DETECT_StartTimeAsyncEnd = gVX1000.EventTimestamp;
#endif /* VX1000_COLDSTART_BENCHMARK && VX1000_OLDA */

    /* removed: gVX1000.ToolDetectState &= ~(VX1000_UINT32)VX1000_TDS_COLDSTART_DELAY_REQ; */
    gVX1000.ToolDetectState |= (VX1000_UINT32)VX1000_TDS_COLDSTART_DELAY;

    /* Calculate checksums over the calibration areas and store in Olda memory */
    /* VX1000 may use this to optimize calibration download */
#if defined(VX1000_COLDSTART_CALIBRATION)
    gVX1000.ToolDetectState |= (VX1000_UINT32)VX1000_TDS_COLDSTART_CHS_BUSY;
    VX1000_SUFFUN(vx1000_CalcFlashChecksum)();
    gVX1000.ToolDetectState |= (VX1000_UINT32)VX1000_TDS_COLDSTART_CHS_DONE;
#if defined(VX1000_COLDSTART_BENCHMARK) && defined(VX1000_OLDA)
    gVX1000_DETECT_ChecksumDoneTime = VX1000_CLOCK();  /* here, gVX1000.EventTimestamp is outdated */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_COLDSTART_BENCHMARK && VX1000_OLDA */
#endif /* VX1000_COLDSTART_CALIBRATION */

    /* Phase III: Check/wait for tool requests (FW -> ECU: standard bitvector mode; ECU -> FW: standard bitvector mode) */
    for (gVX1000.ToolDetectState &= (VX1000_UINT32)~((VX1000_TDS_COLDSTART_DONE) | (VX1000_TDS_COLDSTART_TIMEOUT));
    0 == (gVX1000.ToolDetectState & (VX1000_UINT32) ((VX1000_TDS_COLDSTART_DONE) | (VX1000_TDS_COLDSTART_TIMEOUT)));)
    {
      if ((gVX1000.ToolCtrlState & ((VX1000_UINT32)(VX1000_TCS_COLDSTART_DONE))) != 0)
      {
        /* Init acknowledge */
#if (defined(VX1000_OVERLAY)) && (defined(VX1000_OVERLAY_VX_CONFIGURABLE)) && (defined(VX1000_OVLENBL_REGWRITE_VIA_MX)) && (defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_OVERLAY_CONTROL))
        VX1000_SUFFUN(vx1000_OverlayWriteEcuDescr)();
#endif /* VX1000_OVERLAY & VX1000_OVERLAY_VX_CONFIGURABLE & VX1000_OVLENBL_REGWRITE_VIA_MX & VX1000_MAILBOX & VX1000_MAILBOX_OVERLAY_CONTROL*/
        if ((gVX1000.ToolCtrlState & ((VX1000_UINT32)(VX1000_TCS_SWITCH_TO_WP))) != 0)
        {
#if (defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_OVERLAY_CONTROL))
          if (VX1000_WRP_SET_CAL_PAGE(0, VX1000_CALPAGE_RAM, (VX1000_CAL_ECU) | (VX1000_CAL_XCP), 1) != 0)
#endif /* VX1000_MAILBOX & VX1000_MAILBOX_OVERLAY_CONTROL */
          {
            gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_ERROR);
          }
        }
#if defined(VX1000_OVERLAY_DESCR_IDX) && defined(VX1000_TARGET_TRICORE)
        else
        {
          (void)VX1000_SUFFUN(vx1000_MapCalRam)(VX1000_OVERLAY_SIZE, VX1000_OVERLAY_ADDR, 1, VX1000_CALRAM_PHYSADDR);
        }
#endif /* VX1000_OVERLAY_DESCR_IDX && VX1000_TARGET_TRICORE */
        gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_COLDSTART_DONE);
      }
      else if ((gVX1000.EventTimestamp - gVX1000_DETECT_StartTime) >= dt)
      {
        /* Timeout acknowledge */
        gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_COLDSTART_TIMEOUT);
      }
      else
      {
        ; /* empty else only here for MISRA */
      }
      gVX1000.EventTimestamp = VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    } /* for(;;) */

#if defined(VX1000_COLDSTART_BENCHMARK) && defined(VX1000_OLDA)
    gVX1000_DETECT_EndTime = VX1000_CLOCK();  /* here, gVX1000.EventTimestamp may be outdated (break's path) */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_COLDSTART_BENCHMARK && VX1000_OLDA */
  }
#if defined(VX1000_RUNNING_ON_MAINCORE)
  else
  {
    ; /* this dummy else case is here only for MISRA */
  }
#endif /* VX1000_RUNNING_ON_MAINCORE */
#endif /* !VX1000_COMPILED_FOR_SLAVECORES */

  return (VX1000_UINT8)(gVX1000.ToolDetectState & (VX1000_UINT32)(VX1000_TDS_DETECTED));
}
#endif /* VX1000_DETECTION | VX1000_TARGET_TRICORE */



/*------------------------------------------------------------------------------- */
/* Breakpoint triggered actions                                                   */

/* optional: breakpoint externals and implementation */



#if defined(VX1000_OLDA)
#if defined(VX1000_OLDA_FORCE_V8)
#define VX1000_EVENTLIST_DAT400        /* empty */
#define VX1000_TRSFRLIST_DAT400        /* empty */
#if (((VX1000_OLDA_EVENT_COUNT) & 0x200) == 0x200)
#define VX1000_EVENTLIST_DAT200        VX1000_EVENTLIST_DAT400 VX1000_EVENTLIST_INIT200((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x400))
#define VX1000_TRSFRLIST_DAT200        VX1000_TRSFRLIST_DAT400 VX1000_TRSFRLIST_INIT200((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x400))
#else  /* !(VX1000_OLDA_EVENT_COUNT & 0x200) */
#define VX1000_EVENTLIST_DAT200        VX1000_EVENTLIST_DAT400
#define VX1000_TRSFRLIST_DAT200        VX1000_TRSFRLIST_DAT400
#endif /* !(VX1000_OLDA_EVENT_COUNT & 0x200) */
#if (((VX1000_OLDA_EVENT_COUNT) & 0x100) == 0x100)
#define VX1000_EVENTLIST_DAT100        VX1000_EVENTLIST_DAT200 VX1000_EVENTLIST_INIT100((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x200))
#define VX1000_TRSFRLIST_DAT100        VX1000_TRSFRLIST_DAT200 VX1000_TRSFRLIST_INIT100((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x200))
#else  /* !(VX1000_OLDA_EVENT_COUNT & 0x100) */
#define VX1000_EVENTLIST_DAT100        VX1000_EVENTLIST_DAT200
#define VX1000_TRSFRLIST_DAT100        VX1000_TRSFRLIST_DAT200
#endif /* !(VX1000_OLDA_EVENT_COUNT & 0x100) */
#if (((VX1000_OLDA_EVENT_COUNT) & 0x080) == 0x080)
#define VX1000_EVENTLIST_DAT080        VX1000_EVENTLIST_DAT100 VX1000_EVENTLIST_INIT080((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x100))
#define VX1000_TRSFRLIST_DAT080        VX1000_TRSFRLIST_DAT100 VX1000_TRSFRLIST_INIT080((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x100))
#else  /* !(VX1000_OLDA_EVENT_COUNT & 0x080) */
#define VX1000_EVENTLIST_DAT080        VX1000_EVENTLIST_DAT100
#define VX1000_TRSFRLIST_DAT080        VX1000_TRSFRLIST_DAT100
#endif /* !(VX1000_OLDA_EVENT_COUNT & 0x080) */
#if (((VX1000_OLDA_EVENT_COUNT) & 0x040) == 0x040)
#define VX1000_EVENTLIST_DAT040        VX1000_EVENTLIST_DAT080 VX1000_EVENTLIST_INIT040((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x080))
#define VX1000_TRSFRLIST_DAT040        VX1000_TRSFRLIST_DAT080 VX1000_TRSFRLIST_INIT040((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x080))
#else  /* !(VX1000_OLDA_EVENT_COUNT & 0x040) */
#define VX1000_EVENTLIST_DAT040        VX1000_EVENTLIST_DAT080
#define VX1000_TRSFRLIST_DAT040        VX1000_TRSFRLIST_DAT080
#endif /* !(VX1000_OLDA_EVENT_COUNT & 0x040) */
#if (((VX1000_OLDA_EVENT_COUNT) & 0x020) == 0x020)
#define VX1000_EVENTLIST_DAT020        VX1000_EVENTLIST_DAT040 VX1000_EVENTLIST_INIT020((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x040))
#define VX1000_TRSFRLIST_DAT020        VX1000_TRSFRLIST_DAT040 VX1000_TRSFRLIST_INIT020((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x040))
#else  /* !(VX1000_OLDA_EVENT_COUNT & 0x020) */
#define VX1000_EVENTLIST_DAT020        VX1000_EVENTLIST_DAT040
#define VX1000_TRSFRLIST_DAT020        VX1000_TRSFRLIST_DAT040
#endif /* !(VX1000_OLDA_EVENT_COUNT & 0x020) */
#if (((VX1000_OLDA_EVENT_COUNT) & 0x010) == 0x010)
#define VX1000_EVENTLIST_DAT010        VX1000_EVENTLIST_DAT020 VX1000_EVENTLIST_INIT010((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x020))
#define VX1000_TRSFRLIST_DAT010        VX1000_TRSFRLIST_DAT020 VX1000_TRSFRLIST_INIT010((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x020))
#else  /* !(VX1000_OLDA_EVENT_COUNT & 0x010) */
#define VX1000_EVENTLIST_DAT010        VX1000_EVENTLIST_DAT020
#define VX1000_TRSFRLIST_DAT010        VX1000_TRSFRLIST_DAT020
#endif /* !(VX1000_OLDA_EVENT_COUNT & 0x010) */
#if (((VX1000_OLDA_EVENT_COUNT) & 0x008) == 0x008)
#define VX1000_EVENTLIST_DAT008        VX1000_EVENTLIST_DAT010 VX1000_EVENTLIST_INIT008((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x010))
#define VX1000_TRSFRLIST_DAT008        VX1000_TRSFRLIST_DAT010 VX1000_TRSFRLIST_INIT008((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x010))
#else  /* !(VX1000_OLDA_EVENT_COUNT & 0x008) */
#define VX1000_EVENTLIST_DAT008        VX1000_EVENTLIST_DAT010
#define VX1000_TRSFRLIST_DAT008        VX1000_TRSFRLIST_DAT010
#endif /* !(VX1000_OLDA_EVENT_COUNT & 0x008) */
#if (((VX1000_OLDA_EVENT_COUNT) & 0x004) == 0x004)
#define VX1000_EVENTLIST_DAT004        VX1000_EVENTLIST_DAT008 VX1000_EVENTLIST_INIT004((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x008))
#define VX1000_TRSFRLIST_DAT004        VX1000_TRSFRLIST_DAT008 VX1000_TRSFRLIST_INIT004((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x008))
#else  /* !(VX1000_OLDA_EVENT_COUNT & 0x004) */
#define VX1000_EVENTLIST_DAT004        VX1000_EVENTLIST_DAT008
#define VX1000_TRSFRLIST_DAT004        VX1000_TRSFRLIST_DAT008
#endif /* !(VX1000_OLDA_EVENT_COUNT & 0x004) */
#if (((VX1000_OLDA_EVENT_COUNT) & 0x002) == 0x002)
#define VX1000_EVENTLIST_DAT002        VX1000_EVENTLIST_DAT004 VX1000_EVENTLIST_INIT002((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x004))
#define VX1000_TRSFRLIST_DAT002        VX1000_TRSFRLIST_DAT004 VX1000_TRSFRLIST_INIT002((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x004))
#else  /* !(VX1000_OLDA_EVENT_COUNT & 0x002) */
#define VX1000_EVENTLIST_DAT002        VX1000_EVENTLIST_DAT004
#define VX1000_TRSFRLIST_DAT002        VX1000_TRSFRLIST_DAT004
#endif /* !(VX1000_OLDA_EVENT_COUNT & 0x002) */
#if (((VX1000_OLDA_EVENT_COUNT) & 0x001) == 0x001)
#define VX1000_EVENTLIST_DAT001        VX1000_EVENTLIST_DAT002 VX1000_EVENTLIST_INIT001((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x002))
#define VX1000_TRSFRLIST_DAT001        VX1000_TRSFRLIST_DAT002 VX1000_TRSFRLIST_INIT001((VX1000_OLDA_EVENT_COUNT) & (0x10000-0x002))
#else  /* !(VX1000_OLDA_EVENT_COUNT & 0x001) */
#define VX1000_EVENTLIST_DAT001        VX1000_EVENTLIST_DAT002
#define VX1000_TRSFRLIST_DAT001        VX1000_TRSFRLIST_DAT002
#endif /* !(VX1000_OLDA_EVENT_COUNT & 0x001) */
#define VX1000_EVENTLIST_DAT           VX1000_EVENTLIST_INIT(0x000) VX1000_EVENTLIST_DAT001
#define VX1000_TRSFRLIST_DAT           VX1000_TRSFRLISTSPC_INIT(0x000) VX1000_TRSFRLIST_DAT001
#endif /* VX1000_OLDA_FORCE_V8 */

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OldaInit                                                                                             */
/* API name:      None                                                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Reset all Olda-related VX1000 structure data.                                                               */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
static void VX1000_SUFFUN(vx1000_OldaInit)( void )
{
#if defined(VX1000_OLDA_FORCE_V8)
  static const VX1000_UINT32 vx1000TransferListInit[((VX1000_EVENTDESCR_SPCSIZ) + ((VX1000_OLDA_EVENT_COUNT) * (VX1000_EVENTDESCR_DAQSIZ)))] = { VX1000_TRSFRLIST_DAT };/* PRQA S 0303 */ /* PRQA S 0306 */ /* macro expansion contains VX1000_ADDR_TO_PTR2XXX: cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are provided as integer types by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX requires a typecast */
  static const VX1000_UINT32 vx1000EventListInit[4 * (1 + (VX1000_OLDA_EVENT_COUNT))] = { VX1000_EVENTLIST_DAT };
#endif /* VX1000_OLDA_FORCE_V8 */
  gVX1000.Olda.Version = VX1000_OLDA_VERSION;
  gVX1000.Olda.Running = 0;
#if !defined(VX1000_OLDA_MEMORY_ADDR)
  gVX1000.Olda.MemoryAddr = VX1000_PTR2VUWS_TO_ADDRESS(&gVX1000.Olda.Data[0]); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */ /* PRQA S 0309 */ /* not violating MISRA rule 11.3 with respect to the type sizes: 32 bit are sufficient to hold either a 0:32 flat or a 10:16 paged pointer */
#else /* VX1000_OLDA_MEMORY_ADDR */
  gVX1000.Olda.MemoryAddr = VX1000_OLDA_MEMORY_ADDR;
#endif /* VX1000_OLDA_MEMORY_ADDR */
  gVX1000.Olda.MemorySize = VX1000_OLDA_MEMORY_SIZE;
  gVX1000.Olda.EventCount = 0;
  gVX1000.Olda.EventList = 0;
  gVX1000.Olda.TransferList = 0;
  gVX1000.Olda.SizeLengthNOffset = (VX1000_OLDA_SIZE_OFFSET) | ((VX1000_OLDA_SIZE_LENGTH) << 5);
  gVX1000.Olda.SizeSwapValue = VX1000_OLDA_SIZE_SWAP_VALUE;
  gVX1000.Olda.OldaFeatures = (((((((((((((((0U
                            | (VX1000_FEAT_OLDA_STIM))
                            | (VX1000_FEAT_OLDA_HBB))
                            | (VX1000_FEAT_OLDA_MULTIBUFFER))
                            | (VX1000_FEAT_OLDA_V5_DYNSIZE))
                            | (VX1000_FEAT_OLDA_V7_ASMDAQBOOKE))
                            | (VX1000_FEAT_OLDA_V7_ASMDAQVLE))
                            | (VX1000_FEAT_OLDA_V7_COMPRESS))
                            | (VX1000_FEAT_OLDA_V7_BYTEDAQ)) | (VX1000_FEAT_OLDA_V7_MEMSY_SEL))
                            | (VX1000_FEAT_OLDA_BENCHMARK))
                            | (VX1000_FEAT_OLDA_OVERLOADDETECT))
                            | (VX1000_FEAT_OLDA_512EVENTS))
                            | (VX1000_FEAT_OLDA_UNUSED_FLAG11))
                            | (VX1000_FEAT_OLDA_UNUSED_FLAG12))
                            | (VX1000_FEAT_OLDA_UNUSED_FLAG13))
#if !defined(VX1000_TARGET_TRICORE)
                            | (VX1000_FEAT_OLDA_TRIGGER)
                            ;
#else  /* VX1000_TARGET_TRICORE */
                            ;
  if (VX1000_ECU_IS_AURIX()) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  {
    switch ((VX1000_MCREG_CBS_JTAGID) & (VX1000_JTAGID_PN_MASK)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    {
      case VX1000_JTAGID_PN_TC21x:
      case VX1000_JTAGID_PN_TC22x:
      case VX1000_JTAGID_PN_TC23x:
      case VX1000_JTAGID_PN_TC32x:
      case VX1000_JTAGID_PN_TC33x:
      case VX1000_JTAGID_PN_TC33xED:
      case VX1000_JTAGID_PN_TC35x:
      case VX1000_JTAGID_PN_TC36x:
      case VX1000_JTAGID_PN_TC37x:
      case VX1000_JTAGID_PN_TC37xED:
      case VX1000_JTAGID_PN_TC38x:
      case VX1000_JTAGID_PN_TC39x:
        gVX1000.Olda.OldaFeatures |= (VX1000_FEAT_OLDA_TRIGGER_TYPE3);
        break;
      default:
        gVX1000.Olda.OldaFeatures |= (VX1000_FEAT_OLDA_TRIGGER_TYPE2);
        break;
    }
  }
  else
  {
    gVX1000.Olda.OldaFeatures |= (VX1000_FEAT_OLDA_TRIGGER_TYPE1);
  }
#endif /* VX1000_TARGET_TRICORE */
  gVX1000.Olda.OldaFeatures2 = ((((((((((0U
                             | (VX1000_FEAT_OLDA_V8CMD_USRCPY))
                             | (VX1000_FEAT_OLDA_V8CMD_SP8N))
                             | (VX1000_FEAT_OLDA_V8CMD_CP8N))
                             | (VX1000_FEAT_OLDA_V8CMD_CP16N))
                             | (VX1000_FEAT_OLDA_V8CMD_CP32N))
                             | (VX1000_FEAT_OLDA_V8CMD_CP64N))
                             | (VX1000_FEAT_OLDA_V8CMD_LEGACYVR))
                             | (VX1000_FEAT_OLDA_V8CMD_WAIT))
                             | (VX1000_FEAT_OLDA_V8CMD_CALLJITA))
                             | (VX1000_FEAT_OLDA_V8CMD_SUBEVT))
                             ;
  gVX1000.Olda.OldaBenchmarkCnt = VX1000_OLDA_BENCHMARK_CNT;
  gVX1000.Olda.Res2 = 0UL;

#if defined(VX1000_OLDA_BENCHMARK) && (VX1000_OLDA_BENCHMARK_CNT > 0)
  {
    VX1000_UINT32 i;
    for (i = 0; i < (VX1000_OLDA_BENCHMARK_CNT); i++) { gVX1000_OLDA_Duration[i] = 0; }
  }
#endif /* VX1000_OLDA_BENCHMARK && VX1000_OLDA_BENCHMARK_CNT */

  if ( (VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.Olda.MemoryAddr)) != (0x4UL + VX1000_PTR2VO_TO_ADDRESS(&gVX1000.Olda)) ) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  {
    gVX1000.ToolDetectState |= VX1000_TDS_ERROR;
    VX1000_ERRLOGGER(VX1000_ERRLOG_STRUCTS_PADDED)
  }

  gVX1000.Olda.MagicId = VX1000_OLDA_MAGIC;
#if defined(VX1000_OLDA_FORCE_V8)
  gVX1000.Olda.TransferList = VX1000_PTR2CU32_TO_ADDRESS(vx1000TransferListInit); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  gVX1000.Olda.EventList = VX1000_PTR2CU32_TO_ADDRESS(vx1000EventListInit); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  gVX1000.Olda.EventCount = 1 + (VX1000_OLDA_EVENT_COUNT);
  gVX1000.Olda.Running = 1; /* set Running to 1 (must: specialEvents are handled by olda / possible: VX supports dynamic reprogramming the descriptors while measurement is running) */
#endif /* VX1000_OLDA_FORCE_V8 */
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OldaEvent                                                                                            */
/* API name:      VX1000_EVENT (user) / VX1000_OLDA_EVENT (internal only)                                                     */
/* Wrapper API:   VX1000If_Event                                                                                              */
/* Return value:  None                                                                                                        */
/* Parameter1:    eventNumber E [0,gVX1000.Olda.EventCount)                                                                   */
/*                Validity ensured by internal silent abort.                                                                  */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_OldaInit() must have been called.                                                                    */
/* Precondition2: The MMU must be programmed such that the source memory and the destination memory are visible.              */
/* Precondition3: The MPU must be programmed such that the source memory is readable and the destination memory is writeable. */
/* Description:   Processes all transfer descriptors assigned to parameter1, copies DAQ data to the olda buffer.              */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE)
/* force the optimisation level to >0/DEBUG because in O0 the parameters are passed on the stack and the assembly code would crash */
/* Note: this pragma is only available on the HighTec branch --> only activate the feature when VLE is used */
#pragma GCC push_options
#pragma GCC optimize ("O3")
#endif /* VX1000_SUPPORT_OLDA7_ASMGNUVLE */
#if defined(VX1000_OLDA_FORCE_V8)
void VX1000_SUFFUN(vx1000_OldaEvent)( VX1000_UINT16 eventNumber, VX1000_UINT32 extendedParameter )
#else /* !VX1000_OLDA_FORCE_V8 */
void VX1000_SUFFUN(vx1000_OldaEvent)( VX1000_UINT16 eventNumber )
#endif /* !VX1000_OLDA_FORCE_V8 */
{
  VX1000_OLDA_EVENT_T *pEventList = VX1000_ADDR_TO_PTR2OE(gVX1000.Olda.EventList); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  VX1000_UINT32 *pTransferList = (VX1000_ADDR_TO_PTR2U32(gVX1000.Olda.TransferList)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if defined(VX1000_OLDA_BENCHMARK)
  VX1000_UINT32 byteTransferCount = 0;
#endif /* VX1000_OLDA_BENCHMARK */
#if defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8)
  VX1000_UINT8 useOldaVersion = 8;
#endif /* VX1000_SUPPORT_OLDA8CMD_LEGACYVR && VX1000_OLDA_FORCE_V8 */
  VX1000_UINT32 t0, sz, dstAddr, srcAddr;
#if (defined(VX1000_OLDA_FORCE_V8) || defined(VX1000_OLDA_FORCE_V7))
  volatile VX1000_UINT32 *dst32 = VX1000_ADDR_TO_PTR2VU32(0), *src32 = VX1000_ADDR_TO_PTR2VU32(0); /* dummy initialisations to prevent MISRA from complaining about (dummy)reading uninitialised variables */
#endif /* (VX1000_OLDA_FORCE_V8 || VX1000_OLDA_FORCE_V7) */
#if (defined(VX1000_OLDA_FORCE_V8) || (defined(VX1000_SUPPORT_OLDA7_BYTEDAQ) && defined(VX1000_OLDA_FORCE_V7)))
  volatile VX1000_UINT8  *dst8 = VX1000_ADDR_TO_PTR2VU8(0), *src8 = VX1000_ADDR_TO_PTR2VU8(0); /* dummy initialisations to prevent MISRA from complaining about (dummy)reading uninitialised variables */
#endif /* (VX1000_OLDA_FORCE_V8 || (VX1000_SUPPORT_OLDA7_BYTEDAQ && VX1000_OLDA_FORCE_V7) */
  VX1000_UINT32 k = 0;
  VX1000_OLDA_EVENT_T *event;
  VX1000_UINT16 descriptorCount, descriptorIndex;
#if defined(VX1000_SUPPORT_OLDA7_BYTEDAQ) && (defined(VX1000_OLDA_FORCE_V7) || (defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8)))
  VX1000_UINT8 useByteMode = 0;
#endif /* VX1000_SUPPORT_OLDA7_BYTEDAQ && (VX1000_OLDA_FORCE_V7 || (VX1000_SUPPORT_OLDA8CMD_LEGACYVR && VX1000_OLDA_FORCE_V8) */
#if defined(VX1000_OLDA_FORCE_V7) || defined(VX1000_OLDA_FORCE_V8)
  VX1000_UINT32 sizeList = 0;
#if defined(VX1000_OLDA_FORCE_V7) || defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR)
  VX1000_UINT8 sizeListIndex;
#endif /* VX1000_OLDA_FORCE_V7 || VX1000_SUPPORT_OLDA8CMD_LEGACYVR */
#endif /* VX1000_OLDA_FORCE_V7 || VX1000_OLDA_FORCE_V8 */
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
  VX1000_UINT32 evsync;
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 1)
  volatile VX1000_UINT32 *memSyncTrigPtr = VX1000_ADDR_TO_PTR2VU32(gVX1000.MemSyncTrigPtr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  VX1000_UINT8 evprop = VX1000_CURRENT_CORE_IDX();
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 1 */
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */

  VX1000_OLDA_NOPS()

#if defined(VX1000_OLDA_FORCE_V7) || defined(VX1000_OLDA_FORCE_V8)
  VX1000_DUMMYREAD(sizeList)   /* dummy usage to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
#endif /* VX1000_OLDA_FORCE_V7 || VX1000_OLDA_FORCE_V8 */

  VX1000_DUMMYREAD(k)   /* dummy usage to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */

#if (defined(VX1000_OLDA_FORCE_V8) || (defined(VX1000_SUPPORT_OLDA7_BYTEDAQ) && defined(VX1000_OLDA_FORCE_V7)))
  VX1000_DUMMYREAD(src8)   /* dummy usage to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_DUMMYREAD(dst8)   /* dummy usage to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
#endif /* (VX1000_OLDA_FORCE_V8 || (VX1000_SUPPORT_OLDA7_BYTEDAQ && VX1000_OLDA_FORCE_V7)) */
#if (defined(VX1000_OLDA_FORCE_V8) || defined(VX1000_OLDA_FORCE_V7))
  VX1000_DUMMYREAD(src32)   /* dummy usage to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_DUMMYREAD(dst32)   /* dummy usage to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
#endif /* (VX1000_OLDA_FORCE_V8 || VX1000_OLDA_FORCE_V7) */


  /* Take the timestamp first to have maximum precision.  */
  /* This timestamp will be needed for data trace as well */
  t0 = VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */

#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 1)
  if (evprop >= (VX1000_MEMSYNC_TRIGGER_COUNT)) /* PRQA S 3356 */ /* PRQA S 3359 */ /* cannot avoid violating MISRA rule 13.7 because this logical operation/control expression is actually an assertion that by design must always evaluate to FALSE  */
  { /* PRQA S 3201 */ /* cannot avoid violating MISRA rule 14.1 because this block contains code to handle an error supposed never to occur */
    evprop = 0;
    VX1000_ERRLOGGER(VX1000_ERRLOG_TOO_MANY_CORES)
  }
  evsync = memSyncTrigPtr[evprop];
#else /* VX1000_MEMSYNC_TRIGGER_COUNT <= 1 */
  evsync = gVX1000.MemSyncTrigPtr;
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT <= 1 */
  evsync = (evsync + 0x0C) & ~7UL;   /* seek MemSync_1 but fall back to MemSync_0 if odd */
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 0 */

#if !defined(VX1000_OLDA_FORCE_V8)
  gVX1000.EventTimestamp = t0;
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
  {
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE) || defined(VX1000_SUPPORT_OLDA7_ASMGNUBOOKE)
    {
      register VX1000_UINT32 tAddr = VX1000_PTR2U32_TO_ADDRESS(&gVX1000.EventTimestamp);
      register VX1000_UINT32 tData = t0;
      register VX1000_UINT32 tScratch = 0; /* dummy initialisation to force unique register allocation of the compiler */
      register VX1000_UINT32 tPtr = evsync;
      __asm__ volatile
      (
        "\n  or       %0, 30, 30"
        "\n  or       30, %1, %1"
        "\n  or       %1, 31, 31"
        "\n  or       31, %2, %2"
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE)
        "\n  e_stmw   30, 0(%3)"
#elif defined(VX1000_SUPPORT_OLDA7_ASMGNUBOOKE)
        "\n  stmw     30, 0(%3)"
#endif /* VX1000_SUPPORT_OLDA7_ASMGNUBOOKE */
        "\n  or       31, %1, %1"
        "\n  or       30, %0, %0"
        "\n" :
        "=""r"(tScratch),     /* %0 */
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
        "=""r"(tData),        /* %1 */
        "=""r"(tAddr),        /* %2 */
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
        "=""r"(tAddr),        /* %1 */
        "=""r"(tData),        /* %2 */
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
        "=""r"(tPtr)          /* %3 */
        :
        "0"(tScratch),     /* %0 */
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
        "1"(tData),        /* %1 */
        "2"(tAddr),        /* %2 */
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
        "1"(tAddr),        /* %1 */
        "2"(tData),        /* %2 */
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
        "3"(tPtr)          /* %3 */
        : "30", "31"
      );
      tScratch++; tData++; tAddr++; tPtr++; /* dummy usage to avoid compiler mis-optimisation */
      tScratch++; tData++; tAddr++; tPtr++; /* dummy usage to avoid compiler mis-optimisation */
    }
#else /* !VX1000_SUPPORT_OLDA7_ASMGNUVLE && !VX1000_SUPPORT_OLDA7_ASMGNUBOOKE */
    VX1000_STORE64(evsync, (VX1000_UINT32)&gVX1000.EventTimestamp, t0); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if defined(VX1000_ARM_DSB)
    VX1000_ARM_DSB()
#endif /* VX1000_ARM_DSB */
#endif /* !VX1000_SUPPORT_OLDA7_ASMGNUVLE && !VX1000_SUPPORT_OLDA7_ASMGNUBOOKE */
  }
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */
#endif /* !VX1000_OLDA_FORCE_V8 */

  if ((gVX1000.Olda.Running == 1) && (gVX1000.Olda.MagicId == VX1000_OLDA_MAGIC))
  {
    /* OLDA is running */
    if (eventNumber < gVX1000.Olda.EventCount)
    {
      /* the eventNumber is valid */
      event = &(pEventList[eventNumber]);

#if defined(VX1000_OLDA_BENCHMARK)
#if VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT
      if (eventNumber < (VX1000_OLDA_BENCHMARK_CNT)) /* OLDA event numbers are in the range 0..VX1000_OLDA_EVENT_COUNT-1 */
#endif /* VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT */
      {
#if !defined(VX1000_OLDA_FORCE_V8)
        if (event->TransferCount == 0)
        {
          gVX1000_OLDA_Duration[eventNumber] = 0; /* clear statistic of inactive events */
        }
        else
#endif /* !VX1000_OLDA_FORCE_V8 */
        {
          gVX1000_OLDA_TransferSize[eventNumber]  = 0; /* reset benchmark upon start of active event */
          gVX1000_OLDA_TransferCount[eventNumber] = event->TransferCount; /* ? */
        }
      }
#endif /* VX1000_OLDA_BENCHMARK */

#if defined(VX1000_OLDA_FORCE_V8)
      descriptorCount = 0xFFFFU; /* all events are valid; pre-charge dummy count (events are finished by DONE command) but allow switch to legacy OLDA at runtime */
#else  /* !VX1000_OLDA_FORCE_V8 */
      descriptorCount = event->TransferCount;
      if (descriptorCount != 0)
#endif /* !VX1000_OLDA_FORCE_V8 */
      {
        /* eventNumber is active, there's data to be copied (or at least an event to be triggered) */

        /* Take an individual copy of the timestamp for each event, gVX1000.EventTimestamp mustn't have changed meanwhile */
        event->EventTimestamp = t0;
#if defined(VX1000_OLDA_OVERLOAD_DETECTION)
        event->EventCounter++; /* Pre: increase the Event-Counter to detect Overruns */
#endif /* VX1000_OLDA_OVERLOAD_DETECTION */

        /* Copy data to an intermediate location */
        descriptorIndex = event->TransferIndex;
        dstAddr = event->TransferDest;
#if defined(VX1000_OLDA_FORCE_V8) && defined(VX1000_SUPPORT_OLDA_MULTIBUFFER)
        dstAddr += (((event->EventCounter >> 1) & (VX1000_OLDA_BUFFERINDEX_MASK)) * event->TransferCount);
#if defined(VX1000_OLDA_OVERLOAD_DETECTION)
        /* todo: if ((event->EventCounter ^ event->EventCounterAck) & VX1000_OLDA_BUFFERINDEX_MASK) is zero, the VX has not freed yet the next buffer -> decrement EvenCounter, increment OverrunCounter, leave function */
#endif /* VX1000_OLDA_OVERLOAD_DETECTION */
#endif /* VX1000_OLDA_FORCE_V8 && VX1000_SUPPORT_OLDA_MULTIBUFFER */
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0) && (defined(VX1000_OLDA_FORCE_V7) || (defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8)))
        if (0==pTransferList[descriptorIndex])  /* a first invalid size(-List) entry in memsync mode indicates that the descriptors of this event are to be handled in standard olda mode */
        {
          evsync = 0;
          ++descriptorIndex;
        }
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT & VX1000_OLDA_FORCE_V7 */
#if (defined(VX1000_OLDA_FORCE_V7) || (defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8))) && defined(VX1000_SUPPORT_OLDA7_BYTEDAQ)
        if (0==pTransferList[descriptorIndex])  /* a first invalid size(-List) entry indicates that the descriptors of this event are encoded in byte-mode */
        {
          ++descriptorIndex;
          useByteMode = 1;
          sizeList = 0UL;         /* force initial loading of the sizeList upon next opportunity */
        }
#endif /* VX1000_OLDA_FORCE_V7 && VX1000_SUPPORT_OLDA7_BYTEDAQ */
        while (descriptorCount > 0)
        {
          VX1000_UINT32 descriptor = pTransferList[descriptorIndex];
          descriptorCount--;
          descriptorIndex++;
#if defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8)
          if (useOldaVersion == 7)
          {
#endif /* VX1000_SUPPORT_OLDA8CMD_LEGACYVR && VX1000_OLDA_FORCE_V8 */
#if !defined(VX1000_OLDA_FORCE_V8) || (defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8))
#if defined(VX1000_OLDA_FORCE_V7) || (defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8))
#if defined(VX1000_SUPPORT_OLDA7_BYTEDAQ)
            if (useByteMode != 0)
            {
              /* TransferList entry consists of up to 4 transfer sizes (8Bit), followed by up to 4 pure transfer address entries (32Bit) */
              if (0==sizeList)
              {
                sizeList = descriptor;
                descriptor = pTransferList[descriptorIndex];
                ++descriptorIndex;
              }
              sz = (VX1000_UINT8)(sizeList & 0x000000FFUL); /* copy 0..255 bytes (this differs from normal DAQ which uses words) */
              sizeList >>= 8U;
#if defined(VX1000_OLDA_BENCHMARK)
              byteTransferCount += sz;
#endif /* VX1000_OLDA_BENCHMARK */
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE)  /* hint: (sz is in the range 0..255; data-type is BYTE) */
              {
                /* NOTE: we have to force/use long versions of the VLE instructions because in O3, the compiler allocates also registers 8..23 that cannot be accessed by all short instructions! */
                /* NOTE: dst8 either must not be modified OR we may return the post-copy-destination but have to skip the advancement in the C-part then --> use the second variant */
                /* NOTE: src8 may be destroyed but we must tell the compiler so */
                /* NOTE: sz may be destroyed but we should tell the compiler so */
                register VX1000_UINT32 tBytes = (VX1000_UINT32)sz;
                register VX1000_UINT32 tSource = descriptor;
                register VX1000_UINT32 tDest = dstAddr;
                register VX1000_UINT32 tScratch1=0; /* dummy initialisation to force unique register allocation of the compiler */
                register VX1000_UINT32 tScratch2=1; /* dummy initialisation to force unique register allocation of the compiler */
                __asm__ volatile
                (
                  "\n  e_cmpi   0, %2, 0"
                  "\n  mfspr    %0, 9"       /* save CTR (because the compiler does not know that we use it) */
                  "\n  e_add16i %4, %4, -1"
                  "\n  beq      9f"
                  "\n  e_add16i %3, %3, -1"
                  "\n  mtspr    9, %2"       /* initialise HW-iterator */
                  "\n8:"
                  "\n  e_lbzu   %1, 1(%3)"
                  "\n  e_stbu   %1, 1(%4)"
                  "\n  bdnz     8b"
                  "\n9:"
                  "\n  e_add16i %4, %4, 1"   /* post-increment the destination pointer */
                  "\n  mtspr    9, %0"       /* restore CTR */
                  "\n" ::
                  /*"=" -- does not compile*/"r"(tScratch1),    /* %0 */
                  /*"=" -- does not compile*/"r"(tScratch2),    /* %1 */
                  /*"+" -- does not compile*/"r"(tBytes),       /* %2 */
                  /*"+" -- does not compile*/"r"(tSource),      /* %3 */
                  /*"+" -- does not compile*/"r"(tDest)         /* %4 */
                );
                dstAddr = tDest;
              }
#elif defined(VX1000_SUPPORT_OLDA7_ASMGNUBOOKE)
              {
                /* NOTE: cannot be compiled with -O0 because without optimisation, the compiler wants to pass parameters on stack while the code is written for -O1/-O2/-O3/-Os which use registers for passing */
                register VX1000_UINT32 tBytes = (VX1000_UINT32)sz;
                register VX1000_UINT32 tSource = descriptor;
                register VX1000_UINT32 tDest = dstAddr;
                register VX1000_UINT32 tScratch1=0; /* dummy initialisation to force unique register allocation of the compiler */
                register VX1000_UINT32 tScratch2=1; /* dummy initialisation to force unique register allocation of the compiler */
                __asm__ volatile
                (
                  "\n  or.     %2, %2, %2"
                  "\n  mfspr   %0, 9"       /* save CTR (because the compiler does not know that we use it) */
                  "\n  addi    %4, %4, -1"
                  "\n  beq     9f"
                  "\n  addi    %3, %3, -1"
                  "\n  mtspr   9, %2"       /* initialise HW-iterator */
                  "\n8:"
                  "\n  lbzu    %1, 1(%3)"
                  "\n  stbu    %1, 1(%4)"
                  "\n  bdnz    8b"
                  "\n9:"
                  "\n  addi    %4, %4, 1"   /* post-increment the destination pointer */
                  "\n  mtspr   9, %0"       /* restore CTR */
                  "\n" :
                  "=""r"(tScratch1),        /* %0 */
                  "=""r"(tScratch2),        /* %1 */
                  "=""r"(tBytes),           /* %2 */
                  "=""r"(tSource),          /* %3 */
                  "=""r"(tDest)             /* %4 */
                  :
                  "0"(tScratch1),           /* %0 */
                  "1"(tScratch2),           /* %1 */
                  "2"(tBytes),              /* %2 */
                  "3"(tSource),             /* %3 */
                  "4"(tDest)                /* %4 */
                  : "0"
                );
                tScratch1++; tScratch2++; tBytes++; tSource++; /* dummy usage to avoid compiler mis-optimisation */
                tScratch1++; tScratch2++; tBytes++; tSource++; /* dummy usage to avoid compiler mis-optimisation */
                dstAddr = tDest;
              }
#else /* !VX1000_SUPPORT_OLDA7_ASMGNUVLE && !VX1000_SUPPORT_OLDA7_ASMGNUBOOKE */
              src8 = VX1000_ADDR_TO_PTR2VU8(descriptor); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
              dst8 = VX1000_ADDR_TO_PTR2VU8(dstAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
              for (k = 0; k < sz; ++k) { dst8[k] = src8[k]; }
              dstAddr += (VX1000_UINT32)sz;
#endif /* !VX1000_SUPPORT_OLDA7_ASMGNUVLE && !VX1000_SUPPORT_OLDA7_ASMGNUBOOKE */
            }
            else
#endif /* VX1000_SUPPORT_OLDA7_BYTEDAQ */
            {
              sz = ((VX1000_UINT8)(descriptor & 0x00000003UL));
              if ( 0==sz )
              {
                /* TransferList entry consists of up to 5 transfer sizes [31..2] and size list identifier [1..0]==0 */
                sizeList = descriptor >> 2;
                descriptorCount++; /* undo decrement of remaining transfers because size list entry isn't a transfer */
                for (sizeListIndex = 0; (sizeListIndex < 5) && (descriptorCount > 0); sizeListIndex++)
                {
                  sz = (VX1000_UINT8)(sizeList & 0x3FUL);
                  sizeList >>= 6U;
                  descriptorCount--;
                  descriptor = pTransferList[descriptorIndex];
                  srcAddr = descriptor;
                  descriptorIndex++;
#if defined(VX1000_OLDA_BENCHMARK)
                  byteTransferCount += sz << 2; /* Size of all transfers in DWORDs */
#endif /* VX1000_OLDA_BENCHMARK */
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE) /* hint: (sz is in the range 0..63; data-type is DWORD) */
                  {
                    register VX1000_UINT32 tWords = (VX1000_UINT32)sz;
                    register VX1000_UINT32 tSource = srcAddr;
                    register VX1000_UINT32 tDest = dstAddr;
                    register VX1000_UINT32 tScratch1=0; /* dummy initialisation to force unique register allocation of the compiler */
                    register VX1000_UINT32 tScratch2=1; /* dummy initialisation to force unique register allocation of the compiler */
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
                    if (evsync != 0)
                    {
#if !defined(VX1000_CLASSIMEMSY) /* !FW < 2016-07-20: set VX1000_CLASSIMEMSY; for later FW leave undefined! */
                      register VX1000_UINT32 tBuf = dstAddr;
                      dstAddr += sz << 2;
#endif /* !VX1000_CLASSIMEMSY */
                      tDest = VX1000_ADDR_TO_PTR2U32(evsync); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                      __asm__ volatile
                      (
                        "\n  e_cmpi   0, %2, 2"
                        "\n  mfspr    %0, 9"      /* save CTR (because the compiler does not know that we use it) */
                        "\n  beq      9f"
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
                        "\n  or       %1, 31, 31"
#if !defined(VX1000_CLASSIMEMSY)
                        "\n  e_add16i 31, %5, -4"
                        "\n  e_add16i %3, %3, -4"
#else  /* VX1000_CLASSIMEMSY */
                        "\n  e_add16i 31, %3, -4"
#endif /* VX1000_CLASSIMEMSY */
                        "\n  or       %3, 30, 30"
                        "\n  mtspr    9, %2"      /* initialise HW-iterator */
                        "\n8:"
#if !defined(VX1000_CLASSIMEMSY)
                        "\n  e_add16i 31, r31, 4"
                        "\n  e_lwzu   30, 4(%3)"
#else  /* VX1000_CLASSIMEMSY */
                        "\n  e_lwzu   30, 4(31)"
#endif /* VX1000_CLASSIMEMSY */
                        "\n  e_stmw   30, 0(%4)"
                        "\n  bdnz     8b"
                        "\n  or       30, %3, %3"
                        "\n  or       31, %1, %1"
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
                        "\n  or       %1, 30, 30"
#if !defined(VX1000_CLASSIMEMSY)
                        "\n  e_add16i 30, %5, -4"
                        "\n  e_add16i %3, %3, -4"
#else  /* VX1000_CLASSIMEMSY */
                        "\n  e_add16i 30, %3, -4"
#endif /* VX1000_CLASSIMEMSY */
                        "\n  or       %3, 31, 31"
                        "\n  mtspr    9, %2"      /* initialise HW-iterator */
                        "\n8:"
#if !defined(VX1000_CLASSIMEMSY)
                        "\n  e_add16i 30, r30, 4"
                        "\n  e_lwzu   31, 4(%3)"
#else  /* VX1000_CLASSIMEMSY */
                        "\n  e_lwzu   31, 4(30)"
#endif /* VX1000_CLASSIMEMSY */
                        "\n  e_stmw   30, 0(%4)"
                        "\n  bdnz     8b"
                        "\n  or       31, %3, %3"
                        "\n  or       30, %1, %1"
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
                        "\n9:"
                        "\n  mtspr    9, %0"      /* restore CTR */
                        "\n" :
                        "=""r"(tScratch1),        /* %0 */
                        "=""r"(tScratch2),        /* %1 */
                        "=""r"(tWords),           /* %2 */
                        "=""r"(tSource),          /* %3 */
                        "=""r"(tDest)             /* %4 */
#if !defined(VX1000_CLASSIMEMSY)
                       ,"=""r"(tBuf)              /* %5 */
#endif /* !VX1000_CLASSIMEMSY */
                        :
                        "0"(tScratch1),           /* %0 */
                        "1"(tScratch2),           /* %1 */
                        "2"(tWords),              /* %2 */
                        "3"(tSource),             /* %3 */
                        "4"(tDest)                /* %4 */
#if !defined(VX1000_CLASSIMEMSY)
                       ,"5"(tBuf)                 /* %5 */
#endif /* !VX1000_CLASSIMEMSY */
                        : "30", "31"
                      );
                      tScratch1++; tScratch2++; tWords++; tSource++; tDest++; /* dummy usage to avoid compiler mis-optimisation */
                      tScratch1++; tScratch2++; tWords++; tSource++; tDest++; /* dummy usage to avoid compiler mis-optimisation */
                    }
                    else
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */
                    {
                      register VX1000_UINT32 tScratch3=2; /* dummy initialisation to force unique register allocation of the compiler */
                      register VX1000_UINT32 tScratch4=3; /* dummy initialisation to force unique register allocation of the compiler */
                      register VX1000_UINT32 tScratch5=4; /* dummy initialisation to force unique register allocation of the compiler */
                      __asm__ volatile
                      (
                        "\n  e_andi.  %1, %5, 1"
                        "\n  e_add16i %7, %7, -4"
                        "\n  e_add16i %6, %6, -4"
                        "\n  beq      1f"
                        "\n  e_lwzu   %1, 4(%6)"
                        "\n  e_stwu   %1, 4(%7)"
                        "\n1:"
                        "\n  e_andi.  %2, %5, 2"
                        "\n  beq      2f"
                        "\n  e_lwz    %1, 4(%6)"
                        "\n  e_lwzu   %2, 8(%6)"
                        "\n  e_stw    %1, 4(%7)"
                        "\n  e_stwu   %2, 8(%7)"
                        "\n2:"
                        "\n  e_srwi.  %5, %5, 2"
                        "\n  mfspr    %0, 9"      /* save CTR (because the compiler does not know that we use it) */
                        "\n  beq      9f"
                        "\n  mtspr    9, %5"      /* initialise HW-iterator */
                        "\n8:"
                        "\n  e_lwz    %1,  4(%6)"
                        "\n  e_lwz    %2,  8(%6)"
                        "\n  e_lwz    %3, 12(%6)"
                        "\n  e_lwzu   %4, 16(%6)"
                        "\n  e_stw    %1,  4(%7)"
                        "\n  e_stw    %2,  8(%7)"
                        "\n  e_stw    %3, 12(%7)"
                        "\n  e_stwu   %4, 16(%7)"
                        "\n  bdnz     8b"
                        "\n9:"
                        "\n  e_add16i %7, %7, 4"  /* post-increment the destination pointer */
                        "\n  mtspr    9, %0"      /* restore CTR */
                        "\n" ::
                        /*"=" -- does not compile*/"r"(tScratch1),    /* %0 */
                        /*"=" -- does not compile*/"r"(tScratch2),    /* %1 */
                        /*"=" -- does not compile*/"r"(tScratch3),    /* %2 */
                        /*"=" -- does not compile*/"r"(tScratch4),    /* %3 */
                        /*"=" -- does not compile*/"r"(tScratch5),    /* %4 */
                        /*"+" -- does not compile*/"r"(tWords),       /* %5 */
                        /*"+" -- does not compile*/"r"(tSource),      /* %6 */
                        /*"+" -- does not compile*/"r"(tDest)         /* %7 */
                      );
                    }
                  }
#elif defined(VX1000_SUPPORT_OLDA7_ASMGNUBOOKE)
                  {
                    register VX1000_UINT32 tWords = (VX1000_UINT32)sz;
                    register VX1000_UINT32 tSource = srcAddr;
                    register VX1000_UINT32 tDest = dstAddr;
                    register VX1000_UINT32 tScratch1=0; /* dummy initialisation to force unique register allocation of the compiler */
                    register VX1000_UINT32 tScratch2=1; /* dummy initialisation to force unique register allocation of the compiler */
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
                    if (evsync != 0)
                    {
#if !defined(VX1000_CLASSIMEMSY)
                      register VX1000_UINT32 tBuf = dstAddr;
                      dstAddr += sz << 2;
#endif /* !VX1000_CLASSIMEMSY */
                      tDest = (VX1000_UINT32 /* * */)evsync;
                      __asm__ volatile
                      (
                        "\n  or.      %2, %2, %2"
                        "\n  mfspr    %0, 9"      /* save CTR (because the compiler does not know that we use it) */
                        "\n  beq      9f"
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
                        "\n  or       %1, 31, 31"
#if !defined(VX1000_CLASSIMEMSY)
                        "\n  addi     31, %5, -4"
                        "\n  addi     %3, %3, -4"
#else  /* VX1000_CLASSIMEMSY */
                        "\n  addi     31, %3, -4"
#endif /* VX1000_CLASSIMEMSY */
                        "\n  or       %3, 30, 30"
                        "\n  mtspr    9, %2"      /* initialise HW-iterator */
                        "\n8:"
#if !defined(VX1000_CLASSIMEMSY)
                        "\n  addi     31, 31, 4"
                        "\n  lwzu     30, 4(%3)"
#else  /* VX1000_CLASSIMEMSY */
                        "\n  lwzu     30, 4(31)"
#endif /* VX1000_CLASSIMEMSY */
                        "\n  stmw     30, 0(%4)"
                        "\n  bdnz     8b"
                        "\n  or       30, %3, %3"
                        "\n  or       31, %1, %1"
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
                        "\n  or       %1, 30, 30"
#if !defined(VX1000_CLASSIMEMSY)
                        "\n  addi     30, %5, -4"
                        "\n  addi     %3, %3, -4"
#else  /* VX1000_CLASSIMEMSY */
                        "\n  addi     30, %3, -4"
#endif /* VX1000_CLASSIMEMSY */
                        "\n  or       %3, 31, 31"
                        "\n  mtspr    9, %2"      /* initialise HW-iterator */
                        "\n8:"
#if !defined(VX1000_CLASSIMEMSY)
                        "\n  addi     30, 30, 4"
                        "\n  lwzu     31, 4(%3)"
#else  /* VX1000_CLASSIMEMSY */
                        "\n  lwzu     31, 4(30)"
#endif /* VX1000_CLASSIMEMSY */
                        "\n  stmw     30, 0(%4)"
                        "\n  bdnz     8b"
                        "\n  or       31, %3, %3"
                        "\n  or       30, %1, %1"
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
                        "\n9:"
                        "\n  mtspr    9, %0"      /* restore CTR */
                        "\n" :
                        "=""r"(tScratch1),        /* %0 */
                        "=""r"(tScratch2),        /* %1 */
                        "=""r"(tWords),           /* %2 */
                        "=""r"(tSource),          /* %3 */
                        "=""r"(tDest)             /* %4 */
#if !defined(VX1000_CLASSIMEMSY)
                       ,"=""r"(tBuf)              /* %5 */
#endif /* !VX1000_CLASSIMEMSY */
                        :
                        "0"(tScratch1),           /* %0 */
                        "1"(tScratch2),           /* %1 */
                        "2"(tWords),              /* %2 */
                        "3"(tSource),             /* %3 */
                        "4"(tDest)                /* %4 */
#if !defined(VX1000_CLASSIMEMSY)
                       ,"5"(tBuf)                 /* %5 */
#endif /* !VX1000_CLASSIMEMSY */
                        : "30", "31"
                      );
                      tScratch1++; tScratch2++; tWords++; tSource++; tDest++; /* dummy usage to avoid compiler mis-optimisation */
                      tScratch1++; tScratch2++; tWords++; tSource++; tDest++; /* dummy usage to avoid compiler mis-optimisation */
                    }
                    else
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */
                    {
                      __asm__ volatile
                      (
                        "\n  or.      %2, %2, %2"
                        "\n  mfspr    %0, 9"      /* save CTR (because the compiler does not know that we use it) */
                        "\n  addi     %4, %4, -4"
                        "\n  beq      9f"
                        "\n  addi     %3, %3, -4"
                        "\n  mtspr    9, %2"      /* initialise HW-iterator */
                        "\n8:"
                        "\n lwz %1, 4(%3)\n addi %3, %3, 4" /* the original line "\n  lwzu     %1, 4(%3)" may produce "invalid register operand when updating" errors for some compiler settings */
                        "\n  stwu     %1, 4(%4)"
                        "\n  bdnz     8b"
                        "\n9:"
                        "\n  addi     %4, %4, 4"  /* post-increment the destination pointer */
                        "\n  mtspr    9, %0"      /* restore CTR */
                        "\n" ::
                        /*"=" -- does not compile*/"r"(tScratch1),    /* %0 */
                        /*"=" -- does not compile*/"r"(tScratch2),    /* %1 */
                        /*"+" -- does not compile*/"r"(tWords),       /* %2 */
                        /*"+" -- does not compile*/"r"(tSource),      /* %3 */
                        /*"+" -- does not compile*/"r"(tDest)         /* %4 */
                      );
                      dstAddr = tDest;
                    }
                  }
#else /* !VX1000_SUPPORT_OLDA7_ASMGNUVLE && !VX1000_SUPPORT_OLDA7_ASMGNUBOOKE */
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
                  if (evsync != 0)
                  {
                    while (sz != 0)
                    {
#if !defined(VX1000_CLASSIMEMSY)
                      VX1000_STORE64(evsync, dstAddr, VX1000_ADDR_TO_PTR2U32(srcAddr)[0]); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                      dstAddr += 4UL;
#else  /* VX1000_CLASSIMEMSY */
                      VX1000_STORE64(evsync, srcAddr, VX1000_ADDR_TO_PTR2U32(srcAddr)[0]); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_CLASSIMEMSY */
#if defined(VX1000_ARM_DSB)
                      VX1000_ARM_DSB()
#endif /* VX1000_ARM_DSB */
                      srcAddr += 4UL;
                      sz--;
                    } /* while sz */
                  }
                  else
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */
                  {
                    src32 = VX1000_ADDR_TO_PTR2VU32(srcAddr);  /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    dst32 = VX1000_ADDR_TO_PTR2VU32(dstAddr);  /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    for (k = 0; k < sz; ++k) { dst32[k] = src32[k]; }
                    dstAddr += (sz << 2);
                  }
#endif /* !VX1000_SUPPORT_OLDA7_ASMGNUVLE && !VX1000_SUPPORT_OLDA7_ASMGNUBOOKE */
                }
              }
              else
              {
#if defined(VX1000_OLDA_BENCHMARK)
                byteTransferCount += sz << 2; /* Size of all transfers in DWORDs */
#endif /* VX1000_OLDA_BENCHMARK */
                /* TransferList entry consists of source address [31..2] and transfer size [1..0] counted in 32-bit-words */
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE) /* hint: (sz is in the range 1,2,3; data-type is DWORD) */
                {
                  register VX1000_UINT32 tWords = sz;
                  register VX1000_UINT32 tSource = descriptor;
                  register VX1000_UINT32 tDest = dstAddr;
                  register VX1000_UINT32 tScratch1=0;  /* dummy initialisation needed only to avoid compiler warnings */
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
                  if (evsync != 0)
                  {
#if !defined(VX1000_CLASSIMEMSY)
                    register VX1000_UINT32 tBuf = dstAddr;
                    dstAddr += (sz << 2);
#endif /* !VX1000_CLASSIMEMSY */
                    tDest = VX1000_ADDR_TO_PTR2U32(evsync); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    __asm__ volatile
                    (
                      "\n  e_cmpi   0, %1, 1"
                      "\n  xor      %2, %1, %2"
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
                      "\n  or       %0, 31, 31"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  e_add16i 31, %4, -4"
                      "\n  e_add16i %2, %2, -4"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  e_add16i 31, %2, -4"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  or       %2, 30, 30"
                      "\n  beq      9f"
                      "\n  e_cmpi   0, %3, 2"
                      "\n  beq      8f"
                      "\n7:"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  e_add16i 31, 31, 4"
                      "\n  e_lwzu   30, 4(%2)"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  e_lwzu   30, 4(31)"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  e_stmw   30, 0(%3)"
                      "\n8:"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  e_add16i 31, 31, 4"
                      "\n  e_lwzu   30, 4(%2)"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  e_lwzu   30, 4(31)"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  e_stmw   30, 0(%3)"
                      "\n9:"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  e_add16i 31, 31, 4"
                      "\n  e_lwzu   30, 4(%2)"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  e_lwzu   30, 4(31)"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  e_stmw   30, 0(%3)"
                      "\n  or       30, %2, %2"
                      "\n  or       31, %0, %0"
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
                      "\n  or       %0, 30, 30"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  e_add16i 30, %4, -4"
                      "\n  e_add16i %2, %2, -4"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  e_add16i 30, %2, -4"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  or       %2, 31, 31"
                      "\n  beq      9f"
                      "\n  e_cmpi   0, %3, 2"
                      "\n  beq      8f"
                      "\n7:"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  e_add16i 30, 30, 4"
                      "\n  e_lwzu   31, 4(%2)"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  e_lwzu   31, 4(30)"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  e_stmw   30, 0(%3)"
                      "\n8:"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  e_add16i 30, 30, 4"
                      "\n  e_lwzu   31, 4(%2)"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  e_lwzu   31, 4(30)"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  e_stmw   30, 0(%3)"
                      "\n9:"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  e_add16i 30, 30, 4"
                      "\n  e_lwzu   31, 4(%2)"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  e_lwzu   31, 4(30)"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  e_stmw   30, 0(%3)"
                      "\n  or       31, %2, %2"
                      "\n  or       30, %0, %0"
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
                      "\n" :
                      "=""r"(tScratch1),        /* %0 */
                      "=""r"(tWords),           /* %1 */
                      "=""r"(tSource),          /* %2 */
                      "=""r"(tDest)             /* %3 */
#if !defined(VX1000_CLASSIMEMSY)
                     ,"=""r"(tBuf)              /* %4 */
#endif /* !VX1000_CLASSIMEMSY */
                      :
                      "0"(tScratch1),           /* %0 */
                      "1"(tWords),              /* %1 */
                      "2"(tSource),             /* %2 */
                      "3"(tDest)                /* %3 */
#if !defined(VX1000_CLASSIMEMSY)
                     ,"4"(tBuf)                 /* %4 */
#endif /* !VX1000_CLASSIMEMSY */
                      : "30", "31"
                    );
                    tScratch1++; tWords++; tSource++; tDest++; /* dummy usage to avoid compiler mis-optimisation */
                    tScratch1++; tWords++; tSource++; tDest++; /* dummy usage to avoid compiler mis-optimisation */
                  }
                  else
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */
                  {
                    __asm__ volatile
                    (
                      "\n  e_cmpi   0, %1, 1"
                      "\n  xor      %2, %1, %2"
                      "\n  e_add16i %3, %3, -4"
                      "\n  e_add16i %2, %2, -4"
                      "\n  beq      9f"
                      "\n  e_cmpi   0, %1, 2"
                      "\n  beq      8f"
                      "\n7:"
                      "\n  e_lwzu   %0, 4(%2)"
                      "\n  e_stwu   %0, 4(%3)"
                      "\n8:"
                      "\n  e_lwzu   %0, 4(%2)"
                      "\n  e_stwu   %0, 4(%3)"
                      "\n9:"
                      "\n  e_lwzu   %0, 4(%2)"
                      "\n  e_stwu   %0, 4(%3)"
                      "\n  e_add16i %3, %3, 4"  /* post-increment the destination pointer */
                      "\n" ::
                      /*"=" -- does not compile*/"r"(tScratch1),    /* %0 */
                      /*"+" -- does not compile*/"r"(tWords),       /* %1 */
                      /*"+" -- does not compile*/"r"(tSource),      /* %2 */
                      /*"+" -- does not compile*/"r"(tDest)         /* %3 */
                    );
                    dstAddr = tDest;
                  }
                }
#elif defined(VX1000_SUPPORT_OLDA7_ASMGNUBOOKE)
                {
                  register VX1000_UINT32 tWords = sz;
                  register VX1000_UINT32 tSource = descriptor;
                  register VX1000_UINT32 tDest = dstAddr;
                  register VX1000_UINT32 tScratch1 = 0; /* dummy initialisation to force unique register allocation of the compiler */
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
                  if (evsync != 0)
                  {
#if !defined(VX1000_CLASSIMEMSY)
                    register VX1000_UINT32 tBuf = dstAddr;
                    dstAddr += (sz << 2);
#endif /* !VX1000_CLASSIMEMSY */
                    tDest = evsync;
                    __asm__ volatile
                    (
                      "\n  cmpi   0, %1, 1"
                      "\n  xor    %2, %1, %2"
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
                      "\n  or     %0, 31, 31"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  addi   31, %4, -4"
                      "\n  addi   %2, %2, -4"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  addi   31, %2, -4"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  or     %2, 30, 30"
                      "\n  beq    9f"
                      "\n  cmpi   0, %3, 2"
                      "\n  beq    8f"
                      "\n7:"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  addi   31, 31, 4"
                      "\n  lwzu   30, 4(%2)"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  lwzu   30, 4(31)"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  stmw   30, 0(%3)"
                      "\n8:"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  addi   31, 31, 4"
                      "\n  lwzu   30, 4(%2)"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  lwzu   30, 4(31)"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  stmw   30, 0(%3)"
                      "\n9:"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  addi   31, 31, 4"
                      "\n  lwzu   30, 4(%2)"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  lwzu   30, 4(31)"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  stmw   30, 0(%3)"
                      "\n  or     30, %2, %2"
                      "\n  or     31, %0, %0"
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
                      "\n  or     %0, 30, 30"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  addi   30, %4, -4"
                      "\n  addi   %2, %2, -4"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  addi   30, %2, -4"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  or     %2, 31, 31"
                      "\n  beq    9f"
                      "\n  cmpi   0, %3, 2"
                      "\n  beq    8f"
                      "\n7:"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  addi   30, 30, 4"
                      "\n  lwzu   31, 4(%2)"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  lwzu   31, 4(30)"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  stmw   30, 0(%3)"
                      "\n8:"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  addi   30, 30, 4"
                      "\n  lwzu   31, 4(%2)"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  lwzu   31, 4(30)"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  stmw   30, 0(%3)"
                      "\n9:"
#if !defined(VX1000_CLASSIMEMSY)
                      "\n  addi   30, 30, 4"
                      "\n  lwzu   31, 4(%2)"
#else  /* VX1000_CLASSIMEMSY */
                      "\n  lwzu   31, 4(30)"
#endif /* VX1000_CLASSIMEMSY */
                      "\n  stmw   30, 0(%3)"
                      "\n  or     31, %2, %2"
                      "\n  or     30, %0, %0"
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
                      "\n" :
                      "=""r"(tScratch1),        /* %0 */
                      "=""r"(tWords),           /* %1 */
                      "=""r"(tSource),          /* %2 */
                      "=""r"(tDest)             /* %3 */
#if !defined(VX1000_CLASSIMEMSY)
                     ,"=""r"(tBuf)              /* %4 */
#endif /* !VX1000_CLASSIMEMSY */
                      :
                      "0"(tScratch1),           /* %0 */
                      "1"(tWords),              /* %1 */
                      "2"(tSource),             /* %2 */
                      "3"(tDest)                /* %3 */
#if !defined(VX1000_CLASSIMEMSY)
                     ,"4"(tBuf)                 /* %4 */
#endif /* !VX1000_CLASSIMEMSY */
                      : "30", "31"
                    );
                    tScratch1++; tWords++; tSource++; tDest++; /* dummy usage to avoid compiler mis-optimisation */
                    tScratch1++; tWords++; tSource++; tDest++; /* dummy usage to avoid compiler mis-optimisation */
                  }
                  else
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */
                  {
                    __asm__ volatile
                    (
                      "\n  cmpi   0, %1, 1"
                      "\n  xor    %2, %1, %2"
                      "\n  addi   %3, %3, -4"
                      "\n  addi   %2, %2, -4"
                      "\n  beq    9f"
                      "\n  cmpi   0, %1, 2"
                      "\n  beq    8f"
                      "\n7:"
                      "\n  lwzu   %0, 4(%2)"
                      "\n  stwu   %0, 4(%3)"
                      "\n8:"
                      "\n  lwzu   %0, 4(%2)"
                      "\n  stwu   %0, 4(%3)"
                      "\n9:"
                      "\n  lwzu   %0, 4(%2)"
                      "\n  stwu   %0, 4(%3)"
                      "\n  addi   %3, %3, 4"  /* post-increment the destination pointer */
                      "\n" :
                      "=""r"(tScratch1),        /* %0 */
                      "=""r"(tWords),           /* %1 */
                      "=""r"(tSource),          /* %2 */
                      "=""r"(tDest)             /* %3 */
                      :
                      "0"(tScratch1),           /* %0 */
                      "1"(tWords),              /* %1 */
                      "2"(tSource),             /* %2 */
                      "3"(tDest)                /* %3 */
                      : "0"
                    );
                    dstAddr = tDest;
                    tScratch1++; tWords++; tSource++; /* dummy usage to avoid compiler mis-optimisation */
                    tScratch1++; tWords++; tSource++; /* dummy usage to avoid compiler mis-optimisation */
                  }
                }
#else /* !VX1000_SUPPORT_OLDA7_ASMGNUVLE && !VX1000_SUPPORT_OLDA7_ASMGNUBOOKE */
                srcAddr = (descriptor ^ sz);
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
                if (evsync != 0)
                {
                  while (sz != 0)
                  {
#if !defined(VX1000_CLASSIMEMSY)
                    VX1000_STORE64(evsync, dstAddr, VX1000_ADDR_TO_PTR2U32(srcAddr)[0]); /* PRQA S 0303 */ /* PRQA S 0306 */ /* VX1000_ADDR_TO_PTR2XXX: cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    dstAddr += 4UL;
#else  /* VX1000_CLASSIMEMSY */
                    VX1000_STORE64(evsync, srcAddr, VX1000_ADDR_TO_PTR2U32(srcAddr)[0]); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_CLASSIMEMSY */
                    VX1000_ARM_DSB()
                    srcAddr += 4UL;
                    sz--;
                  } /* while sz */
                }
                else
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */
                {
                  src32 = VX1000_ADDR_TO_PTR2VU32(srcAddr);  /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                  dst32 = VX1000_ADDR_TO_PTR2VU32(dstAddr);  /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                  for (k = 0; k < sz; ++k) { dst32[k] = src32[k]; }
                  dstAddr += (sz << 2);
                }
#endif /* !VX1000_SUPPORT_OLDA7_ASMGNUVLE && !VX1000_SUPPORT_OLDA7_ASMGNUBOOKE */
              }
            }
#if defined(VX1000_OLDA_FORCE_V8)
#if defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR)
            if (0U==descriptorCount)
            {
              descriptorCount = 0xFFFFU;  /* upon end of legacy mode, re-charge dummy count before switching back to v8 (there, events are finished explicitly by DONE command) */
              useOldaVersion = 8;
              descriptor = pTransferList[descriptorIndex];
              descriptorIndex++;
            }
#endif /* VX1000_SUPPORT_OLDA8CMD_LEGACYVR */
          }
#endif /* VX1000_OLDA_FORCE_V8 */
#endif /* VX1000_OLDA_FORCE_V7 || (VX1000_OLDA_FORCE_V8 && VX1000_SUPPORT_OLDA8CMD_LEGACYVR) */
#if (!defined(VX1000_OLDA_FORCE_V7)) || (defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8))
#if defined(VX1000_OLDA_FORCE_V8)
          if (useOldaVersion == 6)
          {
#endif /* VX1000_OLDA_FORCE_V8 */
            sz = (VX1000_UINT8)(descriptor >> (VX1000_OLDA_SIZE_OFFSET)) & ((1<<(VX1000_OLDA_SIZE_LENGTH))-1);
            srcAddr = ( descriptor & (~(VX1000_OLDA_SIZE_MASK)) ) | (VX1000_OLDA_SIZE_REPLACEMENT);
#if defined(VX1000_OLDA_BENCHMARK)
            byteTransferCount += sz << (VX1000_WORDSIZESHIFT);
#endif /* VX1000_OLDA_BENCHMARK */
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
            if (evsync != 0)
            {
              while (sz != 0)
              {
#if !defined(VX1000_CLASSIMEMSY)
                VX1000_STORE64(evsync, dstAddr, VX1000_ADDR_TO_PTR2U32(srcAddr)[0]); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                dstAddr += 4UL;
#else  /* VX1000_CLASSIMEMSY */
                VX1000_STORE64(evsync, srcAddr, VX1000_ADDR_TO_PTR2U32(srcAddr)[0]); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_CLASSIMEMSY */
                VX1000_ARM_DSB()
                srcAddr += 4UL;
                sz--;
              } /* while sz */
            }
            else
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */
            {
              /* note: XC2000 counts in 16bit words, all other targets in 32bit dWords */
              volatile VX1000_UINTWS *srcW = VX1000_ADDR_TO_PTR2VUWS(srcAddr);  /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
              volatile VX1000_UINTWS *dstW = VX1000_ADDR_TO_PTR2VUWS(dstAddr);  /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
              for (k = 0; k < sz; ++k) { dstW[k] = srcW[k]; }
              dstAddr += (sz << VX1000_WORDSIZESHIFT);
            }
#endif /* !VX1000_OLDA_FORCE_V7 */
#endif /* !VX1000_OLDA_FORCE_V8 */
#if defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8)
            if (0U==descriptorCount)
            {
              descriptorCount = 0xFFFFU;  /* upon end of legacy mode, re-charge dummy count before switching back to v8 (there, events are finished explicitly by DONE command) */
              useOldaVersion = 8;
              descriptor = pTransferList[descriptorIndex];
              descriptorIndex++;
            }
          }
          if (useOldaVersion == 8)
          {
#endif /* VX1000_SUPPORT_OLDA8CMD_LEGACYVR && VX1000_OLDA_FORCE_V8 */
#if defined(VX1000_OLDA_FORCE_V8)
            switch ((VX1000_UINT8)(descriptor & 0x000000FFUL))
            {
#if defined(VX1000_SUPPORT_OLDA8CMD_SP8N)
              case 0:
                for (sizeList = descriptor & 0x0000FF00UL; sizeList != 0; sizeList -= 0x00000100UL)
                {
                  sz = ((pTransferList[descriptorIndex] >> 24) & 0x000000FFUL);
                  ++descriptorIndex;
                  dstAddr += sz;
                }
                break;
#endif   /* VX1000_SUPPORT_OLDA8CMD_SP8N */
#if defined(VX1000_SUPPORT_OLDA8CMD_CP8N)
              case 1:
                {
                  /*volatile VX1000_UINT8 *src8;*/
                  srcAddr = descriptor;
                  for (sizeList = descriptor & 0x0000FF00UL; sizeList != 0; sizeList -= 0x00000100UL)
                  {
                    sz = ((pTransferList[descriptorIndex] >> 24) & 0x000000FFUL);
                    srcAddr = (srcAddr & 0xFF000000UL) | (pTransferList[descriptorIndex] & 0x00FFFFFFUL);
                    ++descriptorIndex;
#if defined(VX1000_OLDA_BENCHMARK)
                    byteTransferCount += sz << 0; /* Size of all transfers in BYTEs */
#endif /* VX1000_OLDA_BENCHMARK */
                    src8 = VX1000_ADDR_TO_PTR2VU8(srcAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    dst8 = VX1000_ADDR_TO_PTR2VU8(dstAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    for (k = 0; k < sz; ++k) { dst8[k] = src8[k]; }
                    dstAddr += sz;
                  }
                break;
                }
#endif   /* VX1000_SUPPORT_OLDA8CMD_CP8N */
#if defined(VX1000_SUPPORT_OLDA8CMD_CP16N)
              case 2:
                {
                  srcAddr = descriptor;
                  for (sizeList = descriptor & 0x0000FF00UL; sizeList != 0; sizeList -= 0x00000100UL)
                  {
                    sz = ((pTransferList[descriptorIndex] >> 24) & 0x000000FFUL);
                    srcAddr = (srcAddr & 0xFF000000UL) | (pTransferList[descriptorIndex] & 0x00FFFFFEUL);
                    ++descriptorIndex;
#if defined(VX1000_OLDA_BENCHMARK)
                    byteTransferCount += sz << 1; /* Size of all transfers in BYTEs */
#endif /* VX1000_OLDA_BENCHMARK */
                    {
                      volatile VX1000_UINT16 *src16 = VX1000_ADDR_TO_PTR2VU16(srcAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                      volatile VX1000_UINT16 *dst16 = VX1000_ADDR_TO_PTR2VU16(dstAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                      for (k = 0; k < sz; ++k) { dst16[k] = src16[k]; }
                    }
                    dstAddr += (sz << 1);
                  }
                  break;
                }
#endif   /* VX1000_SUPPORT_OLDA8CMD_CP16N */
#if defined(VX1000_SUPPORT_OLDA8CMD_CP32N)
              case 3:
                {
                  /*volatile VX1000_UINT32 *src32, *dst32;*/
                  srcAddr = descriptor;
                  for (sizeList = descriptor & 0x0000FF00UL; sizeList != 0; sizeList -= 0x00000100UL)
                  {
                    sz = ((pTransferList[descriptorIndex] >> 24) & 0x000000FFUL);
                    srcAddr = (srcAddr & 0xFF000000UL) | (pTransferList[descriptorIndex] & 0x00FFFFFCUL);
                    ++descriptorIndex;
#if defined(VX1000_OLDA_BENCHMARK)
                    byteTransferCount += sz << 2; /* Size of all transfers in BYTEs */
#endif /* VX1000_OLDA_BENCHMARK */
                    src32 = VX1000_ADDR_TO_PTR2VU32(srcAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    dst32 = VX1000_ADDR_TO_PTR2VU32(dstAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    for (k = 0; k < sz; ++k) { dst32[k] = src32[k]; }
                    dstAddr += (sz << 2);
                  }
                  break;
                }
#endif   /* VX1000_SUPPORT_OLDA8CMD_CP32N */
#if defined(VX1000_SUPPORT_OLDA8CMD_CP64N)
              case 4:
                {
                  volatile VX1000_UINT64  *src64, *dst64;
                  srcAddr = descriptor;
                  for (sizeList = descriptor & 0x0000FF00UL; sizeList != 0; sizeList -= 0x00000100UL)
                  {
                    sz = ((pTransferList[descriptorIndex] >> 24) & 0x000000FFUL);
                    srcAddr = (srcAddr & 0xFF000000UL) | (pTransferList[descriptorIndex] & 0x00FFFFF8UL);
                    ++descriptorIndex;
#if defined(VX1000_OLDA_BENCHMARK)
                    byteTransferCount += sz << 3; /* Size of all transfers in BYTEs */
#endif /* VX1000_OLDA_BENCHMARK */
                    src64 = VX1000_ADDR_TO_PTR2VU64(srcAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    dst64 = VX1000_ADDR_TO_PTR2VU64(dstAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    for (k = 0; k < sz; ++k) { dst64[k] = src64[k]; }
                    dstAddr += (sz << 3);
                  }
                  break;
                }
#endif   /* VX1000_SUPPORT_OLDA8CMD_CP64N */
              case 5: /* VX1000_SUPPORT_OLDA8CMD_DONE */
                descriptorCount = 0;
                break;
#if defined(VX1000_SUPPORT_OLDA8CMD_SUBEVT)
              case 6:
                /* Trigger a sub-event so the VX1000 starts to fetch the existing data while the ECU in parallel prepares the remaining data */
#if defined(VX1000_OLDA_OVERLOAD_DETECTION)
                event->EventCounter += (descriptor >> 16) & 0x00000001UL; /* For the final event, increase the Event-Counter to detect Overruns */
#endif /* VX1000_OLDA_OVERLOAD_DETECTION */
                /* note that sub-events are NEVER used together with memsync-events so we do not need to support this combination here */
#if defined(VX1000_TARGET_TRICORE)
                if (VX1000_ECU_IS_AURIX()) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                {
                  VX1000_MCREG_OCDS_TRIGS = ((descriptor >> 8) & 0x000000FFUL); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                }
                else
                {
                  VX1000_MCREG_OCDS_TRIGS = (VX1000_UINT32)(1UL << ((descriptor >> 8) & 0x000000FFUL)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                }
#elif defined(VX1000_OLDA_AUDMBR_REG_ADDR)
                (VX1000_ADDR_TO_PTR2VU16(VX1000_OLDA_AUDMBR_REG_ADDR))[0] = ((descriptor >> 8) & 0x000000FFUL); /* PRQA S 0303 */ /* Cannot avoid violating MISRA rule 11.3 because manipulating the memory-mapped IO at the given address is a core feature of this module */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#elif defined(VX1000_OLDA_DTS_BASE_ADDR)
                VX1000_ADD_MPC_TRIGS((descriptor >> 8) & 0x000000FFUL); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else  /* !VX1000_TARGET_TRICORE & !VX1000_OLDA_AUDMBR_REG_ADDR & !VX1000_OLDA_DTS_BASE_ADDR */
                VX1000_ATOMIC_XOR32X2((/*(VX1000_UINT32)*/&gVX1000.OldaEventNumber), (VX1000_UINT32)(1UL << ((descriptor >> 8) & 0x000000FFUL)));
#endif /* !VX1000_TARGET_TRICORE & !VX1000_OLDA_AUDMBR_REG_ADDR & !VX1000_OLDA_DTS_BASE_ADDR */
                break;
#endif   /* VX1000_SUPPORT_OLDA8CMD_SUBEVT */
              /* case 7 is used exclusively by STIM */
              case 8: /* VX1000_SUPPORT_OLDA8CMD_SPEC32N */
                /* hint: bits 0x00400000UL an 0x00200000UL are still available for generic modifiers */
                for (sizeList = descriptor & 0x0000FF00UL; sizeList != 0; sizeList -= 0x00000100UL)
                {
                  VX1000_UINT32 alternDstAddr = dstAddr, inc = 4UL;
                  if ((descriptor & 0x00800000UL) != 0UL) /* Store elsewhere instead of classic storage into OLDA buffer */
                  {
                    inc = 0UL;
                    alternDstAddr = pTransferList[descriptorIndex];
                    ++descriptorIndex;
                  }
                  switch ((descriptor >> 16) & 0x1FU)
                  {
                    case 0x00: VX1000_ADDR_TO_PTR2VU32(alternDstAddr)[0]  = pTransferList[descriptorIndex]; ++descriptorIndex;                                         break;   /* 32bit-constant from tool - overwrite *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x01: VX1000_ADDR_TO_PTR2VU32(alternDstAddr)[0] |= pTransferList[descriptorIndex]; ++descriptorIndex;                                         break;   /* 32bit-constant from tool - set bits  *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x02: VX1000_ADDR_TO_PTR2VU32(alternDstAddr)[0] &= pTransferList[descriptorIndex]; ++descriptorIndex;                                         break;   /* 32bit-constant from tool - clear bits *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x03: VX1000_ADDR_TO_PTR2VU32(alternDstAddr)[0] ^= pTransferList[descriptorIndex]; ++descriptorIndex;                                         break;   /* 32bit-constant from tool - toggle bits *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x04: VX1000_ADDR_TO_PTR2VU16(alternDstAddr)[0]  = (VX1000_UINT16)(0xFFFFUL & pTransferList[descriptorIndex]); ++descriptorIndex; inc >>= 1;  break;   /* 16bit-constant from tool - overwrite *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x05: VX1000_ADDR_TO_PTR2VU16(alternDstAddr)[0] |= (VX1000_UINT16)(0xFFFFUL & pTransferList[descriptorIndex]); ++descriptorIndex; inc >>= 1;  break;   /* 16bit-constant from tool - set bits  *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x06: VX1000_ADDR_TO_PTR2VU16(alternDstAddr)[0] &= (VX1000_UINT16)(0xFFFFUL & pTransferList[descriptorIndex]); ++descriptorIndex; inc >>= 1;  break;   /* 16bit-constant from tool - clear bits *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x07: VX1000_ADDR_TO_PTR2VU16(alternDstAddr)[0] ^= (VX1000_UINT16)(0xFFFFUL & pTransferList[descriptorIndex]); ++descriptorIndex; inc >>= 1;  break;   /* 16bit-constant from tool - toggle bits *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x08: VX1000_ADDR_TO_PTR2VU8( alternDstAddr)[0]  = (VX1000_UINT8)(0xFFUL & (descriptor >> 24)); inc >>= 2;                                    break;   /* 8bit-constant from tool - overwrite *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x09: VX1000_ADDR_TO_PTR2VU8( alternDstAddr)[0] |= (VX1000_UINT8)(0xFFUL & (descriptor >> 24)); inc >>= 2;                                    break;   /* 8bit-constant from tool - set bits  *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x0A: VX1000_ADDR_TO_PTR2VU8( alternDstAddr)[0] &= (VX1000_UINT8)(0xFFUL & (descriptor >> 24)); inc >>= 2;                                    break;   /* 8bit-constant from tool - clear bits *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x0B: VX1000_ADDR_TO_PTR2VU8( alternDstAddr)[0] ^= (VX1000_UINT8)(0xFFUL & (descriptor >> 24)); inc >>= 2;                                    break;   /* 8bit-constant from tool - toggle bits *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x0C: VX1000_ADDR_TO_PTR2VU8( alternDstAddr)[0] += (VX1000_UINT8)(0xFFUL & (descriptor >> 24)); inc >>= 2;                                    break;   /* 8bit-constant from tool - add *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x0D: VX1000_ADDR_TO_PTR2VU16(alternDstAddr)[0] += (VX1000_UINT16)(0xFFFFUL & (descriptor >> 24)); inc >>= 1;                                 break;   /* Add16 signed 8bit-constant from tool *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x0E: VX1000_ADDR_TO_PTR2VU32(alternDstAddr)[0] += ((descriptor >> 24) & 0x7FUL) - ((descriptor >> 24) & 0x80UL);                             break;   /* Add32 signed 8bit-constant from tool */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x0F: VX1000_ADDR_TO_PTR2VU32(alternDstAddr)[0] += pTransferList[descriptorIndex]; ++descriptorIndex;                                         break;   /* Add 32bit-constant from tool *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x10: VX1000_ADDR_TO_PTR2VU32(alternDstAddr)[0]  = extendedParameter;                                                                         break;   /* 32bit-constant from application *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if defined(VX1000_TARGET_TRICORE)
                    case 0x11: VX1000_ADDR_TO_PTR2VU32(alternDstAddr)[0] = VX1000_ADDR_TO_PTR2VU32(alternDstAddr)[0]  = t0;                                            break;   /* 32bit-timestamp of call to VX1000_EVENT() */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else  /* !VX1000_TARGET_TRICORE */
                    case 0x11: VX1000_ADDR_TO_PTR2VU32(alternDstAddr)[0] = t0;                                                                                         break;   /* 32bit-timestamp of call to VX1000_EVENT() */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* !VX1000_TARGET_TRICORE */
                    case 0x12: VX1000_ADDR_TO_PTR2VU16(alternDstAddr)[0] = (VX1000_UINT16)(0xFFFFUL & extendedParameter); inc >>= 1;                                   break;   /* 16bit-constant from application */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if defined(VX1000_TARGET_TRICORE)
                    case 0x13: VX1000_ADDR_TO_PTR2VU16(alternDstAddr)[0] = VX1000_ADDR_TO_PTR2VU16(alternDstAddr)[0] = (VX1000_UINT16)(0xFFFFUL & t0); inc >>= 1;      break;   /* 16bit-timestamp */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else  /* !VX1000_TARGET_TRICORE */
                    case 0x13: VX1000_ADDR_TO_PTR2VU16(alternDstAddr)[0] = (VX1000_UINT16)(0xFFFFUL & t0); inc >>= 1;                                                  break;   /* 16bit-timestamp */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* !VX1000_TARGET_TRICORE */
                    case 0x14: VX1000_ADDR_TO_PTR2VU8( alternDstAddr)[0] = (VX1000_UINT8)(0xFFUL & extendedParameter); inc >>= 2;                                      break;   /* 8bit-constant from application */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if defined(VX1000_TARGET_TRICORE)
                    case 0x15: VX1000_ADDR_TO_PTR2VU8( alternDstAddr)[0] = VX1000_ADDR_TO_PTR2VU8(alternDstAddr)[0] = (VX1000_UINT8)(0xFFUL & t0); inc >>= 2;          break;   /* 8bit-timestamp */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else  /* !VX1000_TARGET_TRICORE */
                    case 0x15: VX1000_ADDR_TO_PTR2VU8( alternDstAddr)[0] = (VX1000_UINT8)(0xFFUL & t0); inc >>= 2;                                                     break;   /* 8bit-timestamp */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* !VX1000_TARGET_TRICORE */
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0 )
                    case 0x16: if (evsync != 0) { VX1000_STORE64(evsync, alternDstAddr, pTransferList[descriptorIndex+1]); } ++descriptorIndex; inc = 0;               break;   /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x17: if (evsync != 0) { VX1000_STORE64(evsync, alternDstAddr, extendedParameter); } inc = 0;                                                 break;   /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x18: if (evsync != 0) { VX1000_STORE64(evsync, alternDstAddr, t0); } inc = 0;                                                                break;   /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    case 0x19: if (evsync != 0) { VX1000_STORE64(evsync, alternDstAddr, VX1000_CLOCK()); } inc = 0;                                                    break;   /* current 32bit-timestamp for internal profiling of different OLDA sequences */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */
                    case 0x1A: VX1000_ADDR_TO_PTR2VU32(alternDstAddr)[0] = VX1000_CLOCK(); /* no special VX1000_TARGET_TRICORE-handling needed here */                 break;   /* current 32bit-timestamp for internal profiling of different OLDA sequences *//* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    default:   VX1000_ERRLOGGER(VX1000_ERRLOG_OLDA_UNIMPLSUB)                                                                                          break;
                  }
#if defined(VX1000_OLDA_BENCHMARK)
                  byteTransferCount += inc; /* Size of all transfers in BYTEs */
#endif /* VX1000_OLDA_BENCHMARK */
                  dstAddr += inc;
                }
                break;
              case 9: /* VX1000_SUPPORT_OLDA8CMD_JUMP */
                switch ((descriptor >> 8) & 0xFFU)
                {
                  case 0x00U: descriptorIndex += (VX1000_UINT16)(((descriptor >> 16) & 0x7FFFUL) - ((descriptor >> 16) & 0x8000UL));                                   break; /* relative jump (+/-) to another descriptor */
                  default:    VX1000_ERRLOGGER(VX1000_ERRLOG_OLDA_UNIMPLSUB)                                                                                           break;
                }
                break;
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0 ) /* #if defined(VX1000_SUPPORT_OLDA8CMD_SYNC32N) */
              case 10:
                srcAddr = descriptor;
                for (sizeList = descriptor & 0x0000FF00UL; sizeList != 0; sizeList -= 0x00000100UL)
                {
                  sz = ((pTransferList[descriptorIndex] >> 24) & 0x000000FFUL);
                  srcAddr = (srcAddr & 0xFF000000UL) | (pTransferList[descriptorIndex] & 0x00FFFFFCUL);
                  ++descriptorIndex;
#if defined(VX1000_OLDA_BENCHMARK)
                  byteTransferCount += sz << 2; /* Size of all transfers in BYTEs */
#endif /* VX1000_OLDA_BENCHMARK */
                  if (evsync != 0)
                  {
                    volatile VX1000_UINT32 *src = VX1000_ADDR_TO_PTR2VU32(srcAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    for (k = 0; k < sz; ++k)
                    {
#if !defined(VX1000_CLASSIMEMSY)
                      VX1000_STORE64(evsync, dstAddr + (k << 2), src[k]); VX1000_ARM_DSB() /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else  /* VX1000_CLASSIMEMSY */
                      VX1000_STORE64(evsync, srcAddr + (k << 2), src[k]); VX1000_ARM_DSB() /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_CLASSIMEMSY */
                    }
#if !defined(VX1000_CLASSIMEMSY)
                    dstAddr += (sz << 2);
#endif /* !VX1000_CLASSIMEMSY */
                  }
                }
                break;
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */ /* #endif // VX1000_SUPPORT_OLDA8CMD_SYNC32N */
#if defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR)
              case 11:
                useOldaVersion = (VX1000_UINT8)((descriptor & 0x00000F00UL) >> 8); /* interpret the rest of the descriptors as an older olda version */
                descriptorCount = (VX1000_UINT16)(descriptor >> 16); /* the legacy olda version will have to handle n descriptors */
                break;
#endif /* VX1000_SUPPORT_OLDA8CMD_LEGACYVR */
#if defined(VX1000_MEMCPY)
              case 12: /* VX1000_OLDA8CMD_USRCPY */
                {
                  VX1000_UINT8 flags = (VX1000_UINT8)(0xFUL & (descriptor >> 20));
                  srcAddr = descriptor;
                  for (sizeList = descriptor & 0x000FFF00UL; sizeList != 0; sizeList -= 0x00000100UL)
                  {
                    sz = ((pTransferList[descriptorIndex] >> 24) & 0x000000FFUL);
                    srcAddr = (srcAddr & 0xFF000000UL) | (pTransferList[descriptorIndex] & 0x00FFFFFFUL);
                    if ((flags & 0x1) != 0) { srcAddr = VX1000_ADDR_TO_PTR2VU32(srcAddr)[0]; } /* indirection */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                    ++descriptorIndex;
#if defined(VX1000_OLDA_BENCHMARK)
                    byteTransferCount += sz << 0; /* Size of all transfers in BYTEs */
#endif /* VX1000_OLDA_BENCHMARK */
                    VX1000_MEMCPY(VX1000_ADDR_TO_PTR2U8(dstAddr), VX1000_ADDR_TO_PTR2U8(srcAddr), sz << 0); /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because this callback function may be declared either with or without memory qualifiers by the user */
                    dstAddr += sz;
                  }
                }
                break;
#endif /* VX1000_MEMCPY */
              case 13:
                switch ((descriptor & 0x00000F00UL) >> 8)
                {
                  case 0x0: VX1000_ENTER_SPINLOCK()                            break;
                  case 0x1: VX1000_LEAVE_SPINLOCK()                            break;
                  default:  VX1000_ERRLOGGER(VX1000_ERRLOG_OLDA_UNIMPLSUB)     break;
                }
                break;
              /* cases 14..19 are used exclusively by STIM */
#if defined(VX1000_ADDONS_DASDAQ_OLDACMD)
              case 20:
                VX1000_ADDONS_DASDAQ_OLDACMD(eventNumber, &descriptorIndex, &sizeList, &descriptor, &dstAddr);
                break;
#endif /* VX1000_ADDONS_DASDAQ_OLDACMD */
#if defined(VX1000_SUPPORT_OLDA8CMD_CALLJITA)
              case 21:
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
                descriptorIndex += (VX1000_UINT16)((VX1000_ADDR_TO_PTR2OCB(pTransferList[descriptorIndex]))(dstAddr, t0, evsync)); /* PRQA S 0305 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else  /* !VX1000_MEMSYNC_TRIGGER_COUNT */
                descriptorIndex += (VX1000_UINT16)((VX1000_ADDR_TO_PTR2OCB(pTransferList[descriptorIndex]))(dstAddr, t0, 0UL)); /* PRQA S 0305 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* !VX1000_MEMSYNC_TRIGGER_COUNT */
                break;
#endif /* VX1000_SUPPORT_OLDA8CMD_CALLJITA */
              /* case 22 used exclusively by STIM */
              default:
                VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(sizeList), 0/*(src32)*/, 0/*(dst32)*/, 0/*(sz)*/, 0))  /* dummy accesses to prevent compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
                VX1000_ERRLOGGER(VX1000_ERRLOG_OLDA_UNIMPLCMD)
                break;
            }
#endif /* VX1000_OLDA_FORCE_V8 */
#if defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8)
          }
#endif /* VX1000_SUPPORT_OLDA8CMD_LEGACYVR && VX1000_OLDA_FORCE_V8 */
        } /* while descriptorCount*/
#if !defined(VX1000_OLDA_FORCE_V8)
#if defined(VX1000_OLDA_OVERLOAD_DETECTION)
        event->EventCounter++; /* Increase the Event-Counter to detect Overruns */
#endif /* VX1000_OLDA_OVERLOAD_DETECTION */

        /* Trigger the VX1000 to copy the data */
#if defined(VX1000_SUPPRESS_TRACE_SUPPORT) || defined(VX1000_TARGET_TRICORE)
#if defined(VX1000_OLDA_AUDMBR_REG_ADDR)
        if (eventNumber < 15) /* prevent triggering false special or DAQ events */
#else  /* !VX1000_OLDA_AUDMBR_REG_ADDR */
        if (eventNumber < 31) /* prevent triggering false special or DAQ events */
#endif /* !VX1000_OLDA_AUDMBR_REG_ADDR */
        {
#if defined(VX1000_TARGET_XC2000)
          gVX1000.OldaEventNumber ^= (VX1000_UINT32)(1UL << eventNumber);
          gVX1000.CalPtr          ^= (VX1000_UINT32)(1UL << eventNumber);
#elif defined(VX1000_TARGET_TRICORE)
          if (VX1000_ECU_IS_AURIX()) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          {
            VX1000_MCREG_OCDS_TRIGS = (eventNumber); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          else
          {
            VX1000_MCREG_OCDS_TRIGS = (1UL << eventNumber); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
#elif defined(VX1000_OLDA_AUDMBR_REG_ADDR)
          (VX1000_ADDR_TO_PTR2VU16(VX1000_OLDA_AUDMBR_REG_ADDR))[0] = (1U << eventNumber); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#elif defined(VX1000_OLDA_DTS_BASE_ADDR)
          VX1000_ADD_MPC_TRIGS(eventNumber); /* PRQA S 0303 */ /* PRQA S 0306 */ /* macro expansion contains VX1000_ADDR_TO_PTR2XXX: cannot avoid violating MISRA rule 11.3 because the fix HW address of the peripheral (it's not an "allocated object") is an integer while we need a pointer to access its value */
#else  /* !VX1000_TARGET_XC2000 & !VX1000_TARGET_TRICORE & !VX1000_OLDA_AUDMBR_REG_ADDR & !VX1000_OLDA_DTS_BASE_ADDR */
          VX1000_ATOMIC_XOR32X2((/*(VX1000_UINT32)*/&gVX1000.OldaEventNumber), (VX1000_UINT32)(1UL << eventNumber));
#endif /* !VX1000_TARGET_XC2000 & !VX1000_TARGET_TRICORE & !VX1000_OLDA_AUDMBR_REG_ADDR & !VX1000_OLDA_DTS_BASE_ADDR */
        }
#endif /* VX1000_SUPPRESS_TRACE_SUPPORT || VX1000_TARGET_TRICORE */
#endif /* !VX1000_OLDA_FORCE_V8 */
#if defined(VX1000_OLDA_BENCHMARK)
/* #if VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT */ /* This check has been removed in order to avoid a MISRA warning */
        if (eventNumber < (VX1000_OLDA_BENCHMARK_CNT)) /* trace event numbers may be in the range 0..511 ! */
/* #endif */ /* VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT */
        {
          gVX1000_OLDA_Duration[eventNumber] = (VX1000_CLOCK()) - t0; /* Timing measurement */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        }
#endif /* VX1000_OLDA_BENCHMARK */
      } /* if descriptorCount */
    } /* if eventNumber */
  } /* if OLDA */
#if (!defined(VX1000_OLDA_FORCE_V8)) || (defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8))
#if defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8)
  if (useOldaVersion < 8)
#endif /* VX1000_SUPPORT_OLDA8CMD_LEGACYVR && VX1000_OLDA_FORCE_V8 */
  {
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
    if (evsync != 0)
    {
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE) || defined(VX1000_SUPPORT_OLDA7_ASMGNUBOOKE)
      {
        register VX1000_UINT32 tData = t0;
        register VX1000_UINT32 tAddr = VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp);
        register VX1000_UINT32 tEventNumber = eventNumber;
        register VX1000_UINT32 tPtr = evsync;
        register VX1000_UINT32 tScratch = 0; /* dummy initialisation to force unique register allocation of the compiler */
        __asm__ volatile
        (
          "\n  or       %0, 30, 30"
          "\n  or       30, %1, %1"
          "\n  or       %1, 31, 31"
          "\n  or       31, %2, %2"
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE)
          "\n  e_stmw   30, 0(%3)"
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
          "\n  e_add16i 31, 31, -4"
          "\n  or       30, %4, %4"
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
          "\n  e_add16i 30, 30, -4"
          "\n  or       31, %4, %4"
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
          "\n  e_stmw   30, 0(%3)"
#elif defined(VX1000_SUPPORT_OLDA7_ASMGNUBOOKE)
          "\n  stmw     30, 0(%3)"
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
          "\n  addi     31, 31, -4"
          "\n  or       30, %4, %4"
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
          "\n  addi     30, 30, -4"
          "\n  or       31, %4, %4"
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
          "\n  stmw     30, 0(%3)"
#endif /* VX1000_SUPPORT_OLDA7_ASMGNUBOOKE */
          "\n  or       31, %1, %1"
          "\n  or       30, %0, %0"
          "\n" :
          "=""r"(tScratch),     /* %0 */
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
          "=""r"(tData),        /* %1 */
          "=""r"(tAddr),        /* %2 */
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
          "=""r"(tAddr),        /* %1 */
          "=""r"(tData),        /* %2 */
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
          "=""r"(tPtr),         /* %3 */
          "=""r"(tEventNumber)  /* %4 */
          :
          "0"(tScratch),        /* %0 */
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
          "1"(tData),           /* %1 */
          "2"(tAddr),           /* %2 */
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
          "1"(tAddr),           /* %1 */
          "2"(tData),           /* %2 */
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
          "3"(tPtr),            /* %3 */
          "4"(tEventNumber)     /* %4 */
          : "30", "31"
        );
        tScratch++; tData++; tAddr++; tPtr++; tEventNumber++;/* dummy usage to avoid compiler mis-optimisation */
        tScratch++; tData++; tAddr++; tPtr++; tEventNumber++;/* dummy usage to avoid compiler mis-optimisation */
      }
#else /* !VX1000_SUPPORT_OLDA7_ASMxxx */
      VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp), t0); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if defined(VX1000_TARGET_TRICORE)
      VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp), t0); /* on TriCore we have to write this four   */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp), t0); /* times to ensure that both time and      */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp), t0); /* address are not run over by eventNumber */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_TARGET_TRICORE */
      VX1000_ARM_DSB()
      VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventNumber), eventNumber); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      VX1000_ARM_DSB()
#endif /* !VX1000_SUPPORT_OLDA7_ASMxxx */
    }
#else /* !VX1000_MEMSYNC_TRIGGER_COUNT */
    /* change request 2016-04-22 visgn: the events shall ALWAYS be triggered via memSync, so trigger normal trace event ONLY if memSync is completely disabled */
#if !defined(VX1000_SUPPRESS_TRACE_SUPPORT)
#if defined(VX1000_TARGET_TRICORE)
    gVX1000.EventTimestamp = t0;       /* Increase probability that timestamp reaches VX before eventNumber does because TriCore has multiple parallel trace FIFOs */
#endif /* VX1000_TARGET_TRICORE */
    gVX1000.EventNumber = eventNumber; /* Had been removed due to EM00034754 but was re-added due to EM00035042; as interims workaround for FW 1.8 it is configurable now via VX1000_SUPPRESS_TRACE_SUPPORT -- On V850/RH850, mailbox polling is so slow that this must be disabled */
    VX1000_ARM_DSB()
#endif /* !VX1000_SUPPRESS_TRACE_SUPPORT */
#endif /* !VX1000_MEMSYNC_TRIGGER_COUNT */
  }
#endif /* !VX1000_OLDA_FORCE_V8 | VX1000_SUPPORT_OLDA8CMD_LEGACYVR */
#if defined(VX1000_OLDA_BENCHMARK)
  if (eventNumber < (VX1000_OLDA_BENCHMARK_CNT))
  {
#if defined(VX1000_OLDA_FORCE_V8)
    gVX1000_OLDA_TransferSize[eventNumber] += byteTransferCount; /* size of all transfers in bytes */
#elif defined(VX1000_TARGET_XC2000) && (!defined(VX1000_OLDA_FORCE_V7))
    gVX1000_OLDA_TransferSize[eventNumber] += byteTransferCount >> 1; /* size of all transfers in words */
#else  /* VX1000_OLDA_FORCE_V7 */
    gVX1000_OLDA_TransferSize[eventNumber] += (byteTransferCount + 2UL) >> 2; /* rounded size of all transfers in DWORDs */
#endif  /* VX1000_OLDA_FORCE_V7 */
  }
#endif /* VX1000_OLDA_BENCHMARK */
  return;
}
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE)
/* restore the optimisation level to the user-selected value (only available on the HighTec branch --> only activate when VLE is used) */
#pragma GCC pop_options
#endif /* VX1000_SUPPORT_OLDA7_ASMGNUVLE */

#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0) && (!defined(VX1000_OLDA_FORCE_V8))
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OldaSpecialEvent                                                                                     */
/* API name:      None                                                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    eventNumber E [0,2^32)                                                                                      */
/*                Validity automatically ensured by data type.                                                                */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_Init() must have been called.                                                                        */
/* Precondition2: The MMU must be programmed such that the source memory and the destination memory are visible.              */
/* Precondition3: The MPU must be programmed such that the source memory is readable and the destination memory is writeable. */
/* Description:   Trigger a special event to transmit 32bit eventNumber to the tool.                                          */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE)
/* force the optimisation level to >0/DEBUG because in O0 the parameters are passed on the stack and the assembly code would crash */
/* Note: this pragma is only available on the HighTec branch --> only activate the feature when VLE is used */
#pragma GCC push_options
#pragma GCC optimize ("O3")
#endif /* VX1000_SUPPORT_OLDA7_ASMGNUVLE */
void VX1000_SUFFUN(vx1000_OldaSpecialEvent)( VX1000_UINT32 eventNumber )
{
  VX1000_UINT32 evsync;
  {
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 1)
    volatile VX1000_UINT32 *memSyncTrigPtr = VX1000_ADDR_TO_PTR2VU32(gVX1000.MemSyncTrigPtr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    VX1000_UINT8 evprop = VX1000_CURRENT_CORE_IDX();
    if (evprop >= (VX1000_MEMSYNC_TRIGGER_COUNT)) /* PRQA S 3356 */ /* PRQA S 3359 */ /* cannot avoid violating MISRA rule 13.7 because this logical operation/control expression is actually an assertion that by design must always evaluate to FALSE  */
    { /* PRQA S 3201 */ /* cannot avoid violating MISRA rule 14.1 because this block contains code to handle an error supposed never to occur */
      evprop = 0;
      VX1000_ERRLOGGER(VX1000_ERRLOG_TOO_MANY_CORES)
    }
    evsync = memSyncTrigPtr[evprop];
#else /* VX1000_MEMSYNC_TRIGGER_COUNT <= 1 */
    evsync = gVX1000.MemSyncTrigPtr;
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT <= 1 */
    evsync = (evsync + 0x0C) & ~7UL;   /* seek MemSync_1 but fall back to MemSync_0 if odd */
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE) || defined(VX1000_SUPPORT_OLDA7_ASMGNUBOOKE)
    {
      register VX1000_UINT32 tData = (VX1000_UINT32)VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      register VX1000_UINT32 tAddr = VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      register VX1000_UINT32 tEventNumber = eventNumber;
      register VX1000_UINT32 tPtr = evsync;
      register VX1000_UINT32 tScratch = 0; /* dummy initialisation to force unique register allocation of the compiler */
      __asm__ volatile
      (
        "\n  or       %0, 30, 30"
        "\n  or       30, %1, %1"
        "\n  or       %1, 31, 31"
        "\n  or       31, %2, %2"
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE)
        "\n  e_stmw   30, 0(%3)"
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
        "\n  e_add16i 31, 31, -4"
        "\n  or       30, %4, %4"
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
        "\n  e_add16i 30, 30, -4"
        "\n  or       31, %4, %4"
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
        "\n  e_stmw   30, 0(%3)"
#elif defined(VX1000_SUPPORT_OLDA7_ASMGNUBOOKE)
        "\n  stmw     30, 0(%3)"
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
        "\n  addi     31, 31, -4"
        "\n  or       30, %4, %4"
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
        "\n  addi     30, 30, -4"
        "\n  or       31, %4, %4"
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
        "\n  stmw     30, 0(%3)"
#endif /* VX1000_SUPPORT_OLDA7_ASMGNUBOOKE */
        "\n  or       31, %1, %1"
        "\n  or       30, %0, %0"
        "\n" :
        "=""r"(tScratch),     /* %0 */
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
        "=""r"(tData),        /* %1 */
        "=""r"(tAddr),        /* %2 */
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
        "=""r"(tAddr),        /* %1 */
        "=""r"(tData),        /* %2 */
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
        "=""r"(tPtr),         /* %3 */
        "=""r"(tEventNumber)  /* %4 */
        :
        "0"(tScratch),        /* %0 */
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
        "1"(tData),           /* %1 */
        "2"(tAddr),           /* %2 */
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
        "1"(tAddr),           /* %1 */
        "2"(tData),           /* %2 */
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
        "3"(tPtr),            /* %3 */
        "4"(tEventNumber)     /* %4 */
        : "30", "31"
      );
      tScratch++; tData++; tAddr++; tPtr++; tEventNumber++;/* dummy usage to avoid compiler mis-optimisation */
      tScratch++; tData++; tAddr++; tPtr++; tEventNumber++;/* dummy usage to avoid compiler mis-optimisation */
    }
#else /* !VX1000_SUPPORT_OLDA7_ASMxxx */
    VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp), VX1000_CLOCK()); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if defined(VX1000_TARGET_TRICORE)
    VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp), VX1000_CLOCK()); /* on TriCore we have to write this four */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp), VX1000_CLOCK()); /* times to ensure that both time and */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp), VX1000_CLOCK()); /* address are not run over by eventnumber */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_TARGET_TRICORE */
    VX1000_ARM_DSB()
    VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventNumber), eventNumber); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    VX1000_ARM_DSB()
#endif /* !VX1000_SUPPORT_OLDA7_ASMxxx */
  }
}
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE)
/* restore the optimisation level to the user-selected value (only available on the HighTec branch --> only activate when VLE is used) */
#pragma GCC pop_options
#endif /* VX1000_SUPPORT_OLDA7_ASMGNUVLE */
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT & !VX1000_OLDA_FORCE_V8 */

#if defined(VX1000_STIM_BY_OLDA)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OldaStimRequestEvent                                                                                 */
/* API name:      VX1000_STIM_REQUEST_EVENT (internal)                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    eventNumber E [0,gVX1000.Olda.EventCount)                                                                   */
/*                Validity ensured by internal silent abort.                                                                  */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_OldaInit() must have been called.                                                                    */
/* Description:   Sends an event without copying any data (the associated STIM data is handled later by vx1000_StimTransfer). */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
#if defined(VX1000_OLDA_FORCE_V8)
void VX1000_SUFFUN(vx1000_OldaStimRequestEvent)( VX1000_UINT16 eventNumber ) /* -- TODO: exploit vx1000_OldaEvent for this */
#else  /* !VX1000_OLDA_FORCE_V8 */
void VX1000_SUFFUN(vx1000_OldaStimRequestEvent)( VX1000_UINT8 eventNumber )
#endif /* !VX1000_OLDA_FORCE_V8 */
{
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
  volatile VX1000_UINT32 *pTransferList = VX1000_ADDR_TO_PTR2VU32(gVX1000.Olda.TransferList); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 1)
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 1 */
  VX1000_UINT32 evsync = 0;
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */
  VX1000_UINT32 t0 = VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */

  if ((gVX1000.Olda.Running == 1) && (gVX1000.Olda.MagicId == VX1000_OLDA_MAGIC))
  {
    /* OLDA is running */
    if (eventNumber < gVX1000.Olda.EventCount)
    {
      /* eventNumber is valid */
      VX1000_OLDA_EVENT_T *pEventList = VX1000_ADDR_TO_PTR2OE(gVX1000.Olda.EventList); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
      /* The existence of a first dummy descriptor informs the AppDriver that the FW despite memSync requests classic OLDA event triggers */
      if (pTransferList[ pEventList[eventNumber].TransferIndex ] != 0x00000000UL)
      {
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 1)
        volatile VX1000_UINT32 *memSyncTrigPtr = VX1000_ADDR_TO_PTR2VU32(gVX1000.MemSyncTrigPtr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        VX1000_UINT8 evprop = VX1000_CURRENT_CORE_IDX();
        if (evprop >= (VX1000_MEMSYNC_TRIGGER_COUNT)) /* PRQA S 3356 */ /* PRQA S 3359 */ /* cannot avoid violating MISRA rule 13.7 because this logical operation/control expression is actually an assertion that by design must always evaluate to FALSE  */
        { /* PRQA S 3201 */ /* cannot avoid violating MISRA rule 14.1 because this block contains code to handle an error supposed never to occur */
          evprop = 0;
          VX1000_ERRLOGGER(VX1000_ERRLOG_TOO_MANY_CORES)
        }
        evsync = memSyncTrigPtr[evprop];
#else /* VX1000_MEMSYNC_TRIGGER_COUNT <= 1 */
        evsync = gVX1000.MemSyncTrigPtr;
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT <= 1 */
        evsync = (evsync + 0x0C) & ~7UL;   /* seek MemSync_1 but fall back to MemSync_0 if odd */
      }
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */
#if !defined(VX1000_STIM_FORCE_V1)
      /* Take an individual copy of the timestamp for each event */
      pEventList[eventNumber].EventTimestamp = t0;
#else /* VX1000_STIM_FORCE_V1 */
      /* Do NOT copy the timestamp in this case, as the gVX1000.EventTimestamp  */
      /* field is used to store the location of the first timestamp in the OLDA */
      /* buffer. The timestamp is not explicitly needed for stim request events */
#endif /* VX1000_STIM_FORCE_V1 */
#if defined(VX1000_OLDA_OVERLOAD_DETECTION)
      pEventList[eventNumber].EventCounter += 2; /* Increase the Event-Counter by two for Overrun detection */
#endif /* VX1000_OLDA_OVERLOAD_DETECTION */

      /* Trigger the VX1000 to copy the data */
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
      if (evsync != 0)
      {
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE) || defined(VX1000_SUPPORT_OLDA7_ASMGNUBOOKE)
        {
          register VX1000_UINT32 tData = t0;
          register VX1000_UINT32 tAddr = VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp);
          register VX1000_UINT32 tEventNumber = eventNumber;
          register VX1000_UINT32 tPtr = evsync;
          register VX1000_UINT32 tScratch = 0; /* dummy initialisation to force unique register allocation of the compiler */
          __asm__ volatile
          (
            "\n  or       %0, 30, 30"
            "\n  or       30, %1, %1"
            "\n  or       %1, 31, 31"
            "\n  or       31, %2, %2"
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE)
            "\n  e_stmw   30, 0(%3)"
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
            "\n  e_add16i 31, 31, -4"
            "\n  or       30, %4, %4"
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
            "\n  e_add16i 30, 30, -4"
            "\n  or       31, %4, %4"
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
            "\n  e_stmw   30, 0(%3)"
#elif defined(VX1000_SUPPORT_OLDA7_ASMGNUBOOKE)
            "\n  stmw     30, 0(%3)"
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
            "\n  addi     31, 31, -4"
            "\n  or       30, %4, %4"
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
            "\n  addi     30, 30, -4"
            "\n  or       31, %4, %4"
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
            "\n  stmw     30, 0(%3)"
#endif /* VX1000_SUPPORT_OLDA7_ASMGNUBOOKE */
            "\n  or       31, %1, %1"
            "\n  or       30, %0, %0"
            "\n" :
            "=""r"(tScratch),     /* %0 */
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
            "=""r"(tData),        /* %1 */
            "=""r"(tAddr),        /* %2 */
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
            "=""r"(tAddr),        /* %1 */
            "=""r"(tData),        /* %2 */
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
            "=""r"(tPtr),         /* %3 */
            "=""r"(tEventNumber)  /* %4 */
            :
            "0"(tScratch),        /* %0 */
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
            "1"(tData),           /* %1 */
            "2"(tAddr),           /* %2 */
#else  /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
            "1"(tAddr),           /* %1 */
            "2"(tData),           /* %2 */
#endif /* !VX1000_OVERLAY_IS_LITTLE_ENDIAN */
            "3"(tPtr),            /* %3 */
            "4"(tEventNumber)     /* %4 */
            : "30", "31"
          );
          tScratch++; tData++; tAddr++; tPtr++; tEventNumber++;/* dummy usage to avoid compiler mis-optimisation */
          tScratch++; tData++; tAddr++; tPtr++; tEventNumber++;/* dummy usage to avoid compiler mis-optimisation */
        }
#else /* !VX1000_SUPPORT_OLDA7_ASMxxx */
        VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp), t0); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if defined(VX1000_TARGET_TRICORE)
        VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp), t0); /* on TriCore we have to write this four */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp), t0); /* times to ensure that both time and */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventTimestamp), t0); /* address are not run over by eventnumber */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_TARGET_TRICORE */
#if defined(VX1000_ARM_DSB)
        VX1000_ARM_DSB()
#endif /* VX1000_ARM_DSB */
        VX1000_STORE64(evsync, VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.EventNumber), eventNumber); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if defined(VX1000_ARM_DSB)
        VX1000_ARM_DSB()
#endif /* VX1000_ARM_DSB */
#endif /* !VX1000_SUPPORT_OLDA7_ASMxxx */
      }
      else
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */
      {
        gVX1000.EventTimestamp = t0;
#if defined(VX1000_SUPPRESS_TRACE_SUPPORT) || defined(VX1000_TARGET_TRICORE)
#if defined(VX1000_OLDA_AUDMBR_REG_ADDR)
        if (eventNumber < 15) /* prevent triggering false special or DAQ events */
#else  /* !VX1000_OLDA_AUDMBR_REG_ADDR */
        if (eventNumber < 31) /* prevent triggering false special or DAQ events */
#endif /* !VX1000_OLDA_AUDMBR_REG_ADDR */
        {
#if defined(VX1000_TARGET_TRICORE)
          if (VX1000_ECU_IS_AURIX()) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          {
            VX1000_MCREG_OCDS_TRIGS = (eventNumber); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          else
          {
            VX1000_MCREG_OCDS_TRIGS = (1UL << eventNumber); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
#elif defined(VX1000_OLDA_AUDMBR_REG_ADDR)
          (VX1000_ADDR_TO_PTR2VU16(VX1000_OLDA_AUDMBR_REG_ADDR))[0] = (1U << eventNumber); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#elif defined(VX1000_OLDA_DTS_BASE_ADDR)
          VX1000_ADD_MPC_TRIGS(eventNumber); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#elif defined(VX1000_TARGET_XC2000)
          gVX1000.OldaEventNumber ^= (VX1000_UINT32)(1UL << eventNumber);
          gVX1000.CalPtr          ^= (VX1000_UINT32)(1UL << eventNumber);
#else  /* !VX1000_TARGET_TRICORE & !VX1000_TARGET_XC2000 & !VX1000_OLDA_AUDMBR_REG_ADDR & !VX1000_OLDA_DTS_BASE_ADDR */
          VX1000_ATOMIC_XOR32X2((/*(VX1000_UINT32)*/&gVX1000.OldaEventNumber), (VX1000_UINT32)(1UL << eventNumber));
#endif /* !VX1000_TARGET_TRICORE & !VX1000_TARGET_XC2000 & !VX1000_OLDA_AUDMBR_REG_ADDR & !VX1000_OLDA_DTS_BASE_ADDR */
        }
#endif /* VX1000_SUPPRESS_TRACE_SUPPORT || VX1000_TARGET_TRICORE */
#if !defined(VX1000_SUPPRESS_TRACE_SUPPORT)
#if defined(VX1000_TARGET_TRICORE)
        gVX1000.EventTimestamp = t0;       /* Increase probability that timestamp reaches VX before eventNumber does because TriCore has multiple parallel trace FIFOs */
#endif /* VX1000_TARGET_TRICORE */
        gVX1000.EventNumber = eventNumber;
#if defined(VX1000_ARM_DSB)
        VX1000_ARM_DSB()
#endif /* VX1000_ARM_DSB */
#endif /* !VX1000_SUPPRESS_TRACE_SUPPORT */
      }
    } /* eventNumber valid */
  } /* OLDA running */
}

#endif /* VX1000_STIM_BY_OLDA */

#endif /* VX1000_OLDA */


#if defined(VX1000_STIM)

#if defined(VX1000_STIM_FORCE_V1)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_BypassCheckCounters                                                                                  */
/* API name:      None                                                                                                        */
/* Return value:  1 if the counters are consistent with new data available                                                    */
/*                2 if no new data are available                                                                              */
/* Parameter1:    eventNumber E [0,gVX1000.Olda.EventCount)                                                                   */
/* Precondition1: stim_event must be a valid stim event in the range above                                                    */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Description:   Checks whether new stim data have been copied into the ECU                                                  */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassCheckCounters)( VX1000_UINT8 stim_event )
{
  VX1000_UINT8 retVal = VX1000_STIM_RET_TIMEOUT;

  if ((VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].EventType == VX1000_BYPASS_TYPE_DIRECT)
  {
    if ((VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].Ctr
    ==  (VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].RqCtr)
    {
      retVal = VX1000_STIM_RET_SUCCESS;
    }
  }
#if defined(VX1000_STIM_BY_OLDA)
  else
  {
    retVal = VX1000_SUFFUN(vx1000_BypassCheckBuffer)(stim_event);
  }
#endif /* VX1000_STIM_BY_OLDA */

  return retVal;
}
#endif /* VX1000_STIM_FORCE_V1 */

#if defined(VX1000_STIM_BY_OLDA)

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_StimTransfer                                                                                         */
/* API name:      None                                                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    eventNumber E [0,gVX1000.Olda.EventCount)                                                                   */
/*                Validity ensured by internal silent abort. // or: {internal assertion, caller }.                            */
/* Preemption:    This function must not be interrupted by any vx1000_* function.                                             */
/*                This function should not interrupt and should not be interrupted by code that reads the stimulation data.   */
/* Termination:   May leave the destination data in an inconsistent state.                                                    */
/*                Internal data stays valid, no problems on reactivation.                                                     */
/* Precondition1: vx1000_OldaInit() must have been called successfully.                                                       */
/* Precondition2: The MMU must be programmed such that the source memory and the destination memory are visible.              */
/* Precondition3: The MPU must be programmed such that the source memory is readable and the destination memory is writeable. */
/* Description:   Processes all transfer descriptors assigned to parameter1, copies data from olda buffer to the destinations.*/
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
#if defined(VX1000_OLDA_FORCE_V8)
static void VX1000_SUFFUN(vx1000_StimTransfer)( VX1000_UINT16 eventNumber );
static void VX1000_SUFFUN(vx1000_StimTransfer)( VX1000_UINT16 eventNumber ) /* -- TODO: try to exploit vx1000_OldaEvent for this */
#else  /* !VX1000_OLDA_FORCE_V8 */
static void VX1000_SUFFUN(vx1000_StimTransfer)( VX1000_UINT8 eventNumber );
static void VX1000_SUFFUN(vx1000_StimTransfer)( VX1000_UINT8 eventNumber )
#endif /* !VX1000_OLDA_FORCE_V8 */
{
  VX1000_UINT32 srcAddr, *pTransferList = VX1000_ADDR_TO_PTR2U32(gVX1000.Olda.TransferList); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  VX1000_OLDA_EVENT_T VX1000_DECL_PTR *event;
  volatile VX1000_UINT8 *src8;
  VX1000_UINT32 descriptor;
  VX1000_UINT16 descriptorIndex;
  VX1000_UINT8 sz;
  VX1000_UINT32 k;
#if defined(VX1000_OLDA_FORCE_V7)
  VX1000_UINT8 sizeListIndex = 0;
#endif /* VX1000_OLDA_FORCE_V7 */
#if defined(VX1000_OLDA_FORCE_V7) || defined(VX1000_OLDA_FORCE_V8)
  VX1000_UINT32 sizeList = 0; /* just a dummy initialisation to avoid compiler warnings. Actually sizeList IS initialised to the correct value in the first iteration because j is initialised to zero */
#endif /* VX1000_OLDA_FORCE_V7 || VX1000_OLDA_FORCE_V8 */
  VX1000_UINT16 descriptorCount;
#if defined(VX1000_OLDA_BENCHMARK) /* Timing measurement */
  VX1000_UINT32 t0;

  t0 = VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_OLDA_BENCHMARK */
  if ((gVX1000.Olda.Running == 1) && (gVX1000.Olda.MagicId == VX1000_OLDA_MAGIC))
  {
    /* OLDA is running */
    if (eventNumber < gVX1000.Olda.EventCount)
    {
      /* eventNumber is valid */
      VX1000_OLDA_EVENT_T *pEventList = VX1000_ADDR_TO_PTR2OE(gVX1000.Olda.EventList); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      event = &(pEventList[eventNumber]);
      srcAddr = event->TransferDest; /* yes: srcAddr is loaded from dst entry for stim */
      descriptorCount = event->TransferCount;
#if defined(VX1000_OLDA_BENCHMARK)
#if VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT
      if (eventNumber < VX1000_OLDA_BENCHMARK_CNT)
#endif /* VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT */
      {
        gVX1000_OLDA_TransferSize[eventNumber]  = 0; /* Size of all transfers  */
        gVX1000_OLDA_TransferCount[eventNumber] = descriptorCount; /* Count of the transfers */
      }
#endif /* VX1000_OLDA_BENCHMARK */
      descriptorIndex = event->TransferIndex;
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
#if !defined(VX1000_OLDA_FORCE_V8)
      if (pTransferList[descriptorIndex] == 0x00000000UL)
      {
        /* skip the first dummy descriptor (it just informs the AppDriver that the FW despite memSync requests classic OLDA event triggers) */
        descriptorIndex++;
        if (descriptorCount > 0) { descriptorCount--; }
      }
#endif /* !VX1000_OLDA_FORCE_V8 */
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT */
      while (descriptorCount > 0)
      {
        /* eventNumber is active: data is to be copied */
#if !defined(VX1000_OLDA_FORCE_V8)
#if defined(VX1000_OLDA_FORCE_V7)
        /* TransferList entry consists of up to 4 transfer sizes, followed by up to 4 pure transfer address entries */
        if (sizeListIndex==0)
        {
          sizeListIndex = 4;
          sizeList = pTransferList[descriptorIndex];
        }
        else
        { /* opening brace for OLDAv7 condition - it is closed after code sections for other OLDA versions */
          --sizeListIndex;
          descriptor = pTransferList[descriptorIndex];
          sz = (VX1000_UINT8)(sizeList & 0x000000FFUL); /* copy 0..255 bytes (this differs from DAQ which uses words) */
          sizeList >>= 8U;
#else /* !VX1000_OLDA_FORCE_V7 */
        /* TransferList entry contains merged size and address information for exactly one transfer */
        { /* opening brace for OLDAv6 condition - it is closed after code sections for other OLDA versions */
          sz = (VX1000_UINT8)(pTransferList[descriptorIndex] >> (VX1000_OLDA_SIZE_OFFSET)) & ((1<<(VX1000_OLDA_SIZE_LENGTH))-1);
          descriptor = (pTransferList[descriptorIndex] & (~(VX1000_OLDA_SIZE_MASK)) ) | (VX1000_OLDA_SIZE_REPLACEMENT);
#endif /* !VX1000_OLDA_FORCE_V7 */
#if defined(VX1000_OLDA_BENCHMARK)
#if VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT
          if (eventNumber < VX1000_OLDA_BENCHMARK_CNT)
#endif /* VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT */
          {
            gVX1000_OLDA_TransferSize[eventNumber] += (VX1000_UINT32)sz; /* Size of all transfers in BYTEs */
          }
#endif /* VX1000_OLDA_BENCHMARK */
          src8 = VX1000_ADDR_TO_PTR2U8(srcAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          for (k = 0; k < sz; ++k) { VX1000_ADDR_TO_PTR2VU8(descriptor)[k] = src8[k]; } /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          srcAddr += (VX1000_UINT32)sz;
#else /* VX1000_OLDA_FORCE_V8 */
        { /* opening brace for OLDAv8 condition - it is closed after code sections for other OLDA versions */
          descriptor = pTransferList[descriptorIndex];
          ++descriptorIndex;
          switch ((VX1000_UINT8)(descriptor & 0x000000FFUL))
          {
            case 5: /* OLDA8CMD_DONE */
              descriptorCount = 1;
              break;
#if defined(VX1000_SUPPORT_OLDA8CMD_WAIT)
            case 7:
              /* Wait for flag that the remaining part of the data is already valid */
#error The semantic of the address and index bits for this command is not specified, yet
              break;
#endif   /* VX1000_SUPPORT_OLDA8CMD_WAIT */
            case 9: /* OLDA8CMD_JUMP */
              switch ((descriptor >> 8) & 0xFFU)
              {
                case 0x00U: descriptorIndex += (VX1000_INT16)(descriptor >> 16);  break;  /* relative jump (+/-) to another descriptor */
                default:    VX1000_ERRLOGGER(VX1000_ERRLOG_OLDA_UNIMPLSUB) break;
              }
              break;
#if defined(VX1000_SUPPORT_OLDA8CMD_LEGACY)
            case 11:
              /* todo KNM: interpret the rest of the descriptors as an older olda version X (see a solution in vx1000_oldaEvent) and then remove this entire line */ VX1000_ERRLOGGER(VX1000_ERRLOG_OLDA_UNIMPLCMD) descriptorCount = 1; /* this aborts the current stim transfer because all following descriptors would be misinterpreted */
              break;
#endif /* VX1000_SUPPORT_OLDA8CMD_LEGACY */
#if defined(VX1000_SUPPORT_OLDA8CMD_SP8N)
            case 14:
              for (sizeList = descriptor & 0x0000FF00UL; sizeList != 0; sizeList -= 0x00000100UL)
              {
                sz = ((VX1000_UINT8) ((pTransferList[descriptorIndex] >> 24) & 0x000000FFUL));
                ++descriptorIndex;
                srcAddr += sz;
              }
              break;
#endif   /* VX1000_SUPPORT_OLDA8CMD_SP8N */
#if defined(VX1000_SUPPORT_OLDA8CMD_CP8N)
            case 15:
              for (sizeList = descriptor & 0x0000FF00UL; sizeList != 0; sizeList -= 0x00000100UL)
              {
                sz = ((VX1000_UINT8) ((pTransferList[descriptorIndex] >> 24) & 0x000000FFUL));
                descriptor = (descriptor & 0xFF000000UL) | (pTransferList[descriptorIndex] & 0x00FFFFFFUL);
                ++descriptorIndex;
#if defined(VX1000_OLDA_BENCHMARK)
#if VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT
                if (eventNumber < (VX1000_OLDA_BENCHMARK_CNT))
#endif /* VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT */
                {
                  gVX1000_OLDA_TransferSize[eventNumber] += (VX1000_UINT32)sz << 0; /* Size of all transfers in BYTEs */
                }
#endif /* VX1000_OLDA_BENCHMARK */
                src8 = VX1000_ADDR_TO_PTR2VU8(srcAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                for (k = 0; k < sz; ++k) { VX1000_ADDR_TO_PTR2VU8(descriptor)[k] = src8[k]; } /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                srcAddr += (VX1000_UINT32)sz;
              }
              break;
#endif   /* VX1000_SUPPORT_OLDA8CMD_CP8N */
#if defined(VX1000_SUPPORT_OLDA8CMD_CP16N)
            case 16:
              for (sizeList = descriptor & 0x0000FF00UL; sizeList != 0; sizeList -= 0x00000100UL)
              {
                volatile VX1000_UINT16 *src16 = VX1000_ADDR_TO_PTR2VU16(srcAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                sz = ((VX1000_UINT8) ((pTransferList[descriptorIndex] >> 24) & 0x000000FFUL));
                descriptor = (descriptor & 0xFF000000UL) | (pTransferList[descriptorIndex] & 0x00FFFFFEUL);
                ++descriptorIndex;
#if defined(VX1000_OLDA_BENCHMARK)
#if VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT
                if (eventNumber < (VX1000_OLDA_BENCHMARK_CNT))
#endif /* VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT */
                {
                  gVX1000_OLDA_TransferSize[eventNumber] += (VX1000_UINT32)sz << 1; /* Size of all transfers in BYTEs */
                }
#endif /* VX1000_OLDA_BENCHMARK */
                for (k = 0; k < sz; ++k) { VX1000_ADDR_TO_PTR2VU16(descriptor)[k] = src16[k]; } /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                srcAddr += ((VX1000_UINT32)sz << 1);
              }
              break;
#endif   /* VX1000_SUPPORT_OLDA8CMD_CP16N */
#if defined(VX1000_SUPPORT_OLDA8CMD_CP32N)
            case 17:
              for (sizeList = descriptor & 0x0000FF00UL; sizeList != 0; sizeList -= 0x00000100UL)
              {
                volatile VX1000_UINT32 *src32 = VX1000_ADDR_TO_PTR2VU32(srcAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                sz = ((VX1000_UINT8) ((pTransferList[descriptorIndex] >> 24) & 0x000000FFUL));
                descriptor = (descriptor & 0xFF000000UL) | (pTransferList[descriptorIndex] & 0x00FFFFFCUL);
                ++descriptorIndex;
#if defined(VX1000_OLDA_BENCHMARK)
#if VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT
                if (eventNumber < (VX1000_OLDA_BENCHMARK_CNT))
#endif /* VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT */
                {
                  gVX1000_OLDA_TransferSize[eventNumber] += (VX1000_UINT32)sz << 2; /* Size of all transfers in BYTEs */
                }
#endif /* VX1000_OLDA_BENCHMARK */
                for (k = 0; k < sz; ++k) { VX1000_ADDR_TO_PTR2VU32(descriptor)[k] = src32[k]; } /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                srcAddr += ((VX1000_UINT32)sz << 2);
              }
              break;
#endif   /* VX1000_SUPPORT_OLDA8CMD_CP32N */
#if defined(VX1000_SUPPORT_OLDA8CMD_CP64N)
            case 18:
              for (sizeList = descriptor & 0x0000FF00UL; sizeList != 0; sizeList -= 0x00000100UL)
              {
                volatile VX1000_UINT64 *src64 = VX1000_ADDR_TO_PTR2VU64(srcAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                sz = ((VX1000_UINT8) ((pTransferList[descriptorIndex] >> 24) & 0x000000FFUL));
                descriptor = (descriptor & 0xFF000000UL) | (pTransferList[descriptorIndex] & 0x00FFFFF8UL);
                ++descriptorIndex;
#if defined(VX1000_OLDA_BENCHMARK)
#if VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT
                if (eventNumber < (VX1000_OLDA_BENCHMARK_CNT))
#endif /* VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT */
                {
                  gVX1000_OLDA_TransferSize[eventNumber] += (VX1000_UINT32)sz << 3; /* Size of all transfers in BYTEs */
                }
#endif /* VX1000_OLDA_BENCHMARK */
                for (k = 0; k < sz; ++k) { VX1000_ADDR_TO_PTR2VU64(descriptor)[k] = src64[k]; } /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
                srcAddr += ((VX1000_UINT32)sz << 3);
              }
              break;
#endif   /* VX1000_SUPPORT_OLDA8CMD_CP64N */
#if defined(VX1000_ADDONS_DASSTIM_OLDACMD)
            case 19:
              VX1000_ADDONS_DASSTIM_OLDACMD(eventNumber, &descriptorIndex, &sizeList, &srcAddr, &descriptor);
              break;
#endif /* VX1000_ADDONS_DASSTIM_OLDACMD */
            /* cases 20 and 21 used exclusively by DAQ */
#if defined(VX1000_SUPPORT_OLDA8CMD_CALLJITA)
            case 22:
              descriptorIndex += (VX1000_UINT16)((VX1000_ADDR_TO_PTR2OCB(pTransferList[descriptorIndex]))(srcAddr, t0, 0UL)); /* PRQA S 0305 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
              break;
#endif /* VX1000_SUPPORT_OLDA8CMD_CALLJITA */
            default:
              VX1000_ERRLOGGER(VX1000_ERRLOG_OLDA_UNIMPLCMD)
              break;
          }
          --descriptorIndex;
#endif /* VX1000_OLDA_FORCE_V8 */
          descriptorCount--;
#if !defined(VX1000_OLDA_FORCE_V8)
#if defined(VX1000_OLDA_FORCE_V7)
        } /* closing brace for OLDAv7 condition - just replicated to help editors with syntax highlighting and auto-indentation */
#else  /* !VX1000_OLDA_FORCE_V7 */
        } /* closing brace for OLDAv6 condition - just replicated to help editors with syntax highlighting and auto-indentation */
#endif /* !VX1000_OLDA_FORCE_V7 */
#else  /* VX1000_OLDA_FORCE_V8 */
        } /* closing brace for OLDAv8 condition - just replicated to help editors with syntax highlighting and auto-indentation */
#endif /* VX1000_OLDA_FORCE_V8 */
        descriptorIndex++;
      } /* while */
    }
#if defined(VX1000_OLDA_BENCHMARK)
#if VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT
    if (eventNumber < VX1000_OLDA_BENCHMARK_CNT)
#endif /* VX1000_OLDA_BENCHMARK_CNT < VX1000_OLDA_EVENT_COUNT */
    {
      gVX1000_OLDA_Duration[eventNumber] = (VX1000_CLOCK()) - t0; /* Timing measurement */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
#endif /* VX1000_OLDA_BENCHMARK */
  }
  return;
}

#if defined(VX1000_STIM_FORCE_V1)

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_BypassCheckBuffer                                                                                    */
/* API name:      None                                                                                                        */
/* Return value:  1 if the buffers is consistent with data from the last DAQ event                                            */
/*                2 if no new data is available or the buffer is being copied to                                              */
/* Parameter1:    eventNumber E [0,gVX1000.Olda.EventCount)                                                                   */
/* Precondition1: stim_event must be a valid stim event in the range above                                                    */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Description:   Checks whether the stim buffer for the given event contains data consistent with the last DAQ event         */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassCheckBuffer)( VX1000_UINT8 stim_event )
{
  VX1000_UINT8 retVal = VX1000_STIM_RET_TIMEOUT;
  VX1000_OLDA_EVENT_T *pEventList = VX1000_ADDR_TO_PTR2OE(gVX1000.Olda.EventList); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */

  VX1000_UINT32 lastEcuTimestamp = pEventList[(VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].DaqEvent].EventTimestamp;
  VX1000_UINT32 endTimestamp = (VX1000_ADDR_TO_PTR2U32(pEventList[stim_event].TransferDest - VX1000_BP_TIMESTAMP_SIZE))[0]; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  VX1000_UINT32 startTimestamp = (VX1000_ADDR_TO_PTR2U32(pEventList[stim_event].EventTimestamp))[0]; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */

  if ((lastEcuTimestamp == endTimestamp) && (lastEcuTimestamp == startTimestamp))
  {
    retVal = VX1000_STIM_RET_SUCCESS;
  }

  return retVal;
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_BypassCopyBuffer                                                                                     */
/* API name:      None                                                                                                        */
/* Return value:  1 if the data have been successfully copied                                                                 */
/*                2 if data are inconsistent: a serious error has occurred, such as writing to the buffer during copying      */
/* Parameter1:    eventNumber E [0,gVX1000.Olda.EventCount)                                                                   */
/* Precondition1: Stimulation must be active and a DAQ event should have been sent before copying.                            */
/* Precondition2: BypassCheckBuffer must have been called before hand to check the buffer data are valid.                     */
/* Precondition3: stim_event must be a valid stim event in the range above                                                    */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Description:   Checks whether the stim buffer for the given event contains data consistent with the last DAQ event         */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassCopyBuffer)( VX1000_UINT8 stim_event )
{
  VX1000_OLDA_EVENT_T *pEventList = VX1000_ADDR_TO_PTR2OE(gVX1000.Olda.EventList); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  VX1000_UINT8 retVal /*= VX1000_STIM_RET_INACTIVE*/;      /* 0 means "Bypass inactive, enable the bypassed code" */
#if defined(VX1000_OLDA_BENCHMARK) /* Timing measurement */
  VX1000_UINT32 t0 = VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_OLDA_BENCHMARK */

  /* Do not copy the timestamps at the start and end of the buffers */
  VX1000_UINT32* src = VX1000_ADDR_TO_PTR2U32(pEventList[stim_event].EventTimestamp + VX1000_BP_TIMESTAMP_SIZE); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  VX1000_UINT32* dst = VX1000_ADDR_TO_PTR2U32(pEventList[stim_event].TransferDest); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  /* Size of the buffer in bytes */
  VX1000_UINT32 n, siz = pEventList[stim_event].TransferDest - pEventList[stim_event].EventTimestamp - 2 * VX1000_BP_TIMESTAMP_SIZE;
#if defined(VX1000_OLDA_BENCHMARK)
  gVX1000_OLDA_TransferSize[stim_event]  = siz; /* Size of all transfers */
  gVX1000_OLDA_TransferCount[stim_event] = 0;   /* Count of the transfers */
#endif /* VX1000_OLDA_BENCHMARK */

  /* Copy data using memcopy if available */
  /* TODO: depending on OLDA mode, the byte count must be used sometimes! */
  siz >>= 2; /* Convert buffer size into 32-bit words */
  for (n = 0; n < siz; ++n) { dst[n] = src[n]; }

  /* Check the timestamp again: */
  if (VX1000_SUFFUN(vx1000_BypassCheckBuffer)(stim_event) == VX1000_STIM_RET_SUCCESS)
  {
    retVal = VX1000_STIM_RET_SUCCESS;
  }
  else
  {
    retVal = VX1000_STIM_RET_ERROR;
  }

#if defined(VX1000_OLDA_BENCHMARK)
  gVX1000_OLDA_Duration[stim_event] = (VX1000_CLOCK()) - t0; /* Timing measurement */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_OLDA_BENCHMARK */

  return retVal;
}

#if defined(VX1000_BYPASS_ALL_CHANS_STIMD)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_BypassAreAllChansStimd                                                                               */
/* API name:      None                                                                                                        */
/* Return value:  TRUE (active) or FALSE (inactive)                                                                           */
/* Precondition1:                                                                                                             */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Description:   Returns true if all channels have received valid stim data                                                  */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassAreAllChansStimd)(void)
{
  VX1000_UINT8 invalidCount = 0;
  VX1000_UINT8 retCode = 0;
  VX1000_UINT8 i;

  /* First check global enable if all channels flag already set */
  if (gVX1000.Stim.Enable != VX1000_STIM_GLOBAL_ALL_CHAN)
  {
    /* If not, loop over all stim events */
    for (i = 0; i < VX1000_STIM_EVENT_COUNT; i++)
    {
      /* Has the stim channel been activated by the VX - ie do we care if it has stim data? */
      if (((VX1000_STIMEVENT_ARRAYNAME)[i].Enable > VX1000_STIM_INACTIVE)
      /* True if the channel has not yet received valid stim data */
      &&  ((VX1000_STIMEVENT_ARRAYNAME)[i].Enable < VX1000_STIM_BUFFER_VALID) )
      {
        ++invalidCount;
        break;
      }
    }
    if (invalidCount == 0)
    {
      gVX1000.Stim.Enable = VX1000_STIM_GLOBAL_ALL_CHAN;
      retCode = 1;
    }
  }
  else
  {
    retCode = 1;
  }

  return retCode;
}
#endif /* VX1000_BYPASS_ALL_CHANS_STIMD */

#endif /* VX1000_STIM_FORCE_V1 */

#endif /* VX1000_STIM_BY_OLDA */

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_SetTimeoutUs                                                                                         */
/* API name:      none (only used internally)                                                                                 */
/* Return value:  The absolute timeout time as clock value in [ticks].                                                        */
/* Parameter1:    t: the relative timeout time in [us].                                                                       */
/*                No invalid input possible.                                                                                  */
/* Preemption:    No problem inside this function, but the API must not hit into an existing SetTimeoutUs-CheckTimeout frame. */
/* Termination:   No effect                                                                                                   */
/* Precondition1: the timer used by VX1000_CLOCK() must already running (with the expected speed).                            */
/* Description:   Calculates the absolute due time.                                                                           */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT32 VX1000_SUFFUN(vx1000_SetTimeoutUs)(VX1000_UINT32 t)
{
#if ((VX1000_CLOCK_TICKS_BASE_NS) < 100000UL) /* was: "#if ((VX1000_CLOCK_TICKS_PER_US) > 10)", but CLOCK_TICKS may be a function! */
  VX1000_UINT32 dt = t * (VX1000_CLOCK_TICKS_PER_US);
#else /* VX1000_CLOCK_TICKS_BASE_NS */
  VX1000_UINT32 dt = (t * (VX1000_CLOCK_TICKS_PER_MS)) / 1000UL;
#endif /* VX1000_CLOCK_TICKS_BASE_NS */
  if ( dt >= ((1UL << (((VX1000_CLOCK_TIMER_SIZE) - 1) & 0x1F)) /* dummy operation needed for MISRA: */ + ( 0UL & t) ) )
  {
    dt = (1UL << (((VX1000_CLOCK_TIMER_SIZE) - 1) & 0x1F)) - 1UL;
    VX1000_ERRLOGGER(VX1000_ERRLOG_TM_DTST_TOO_LONG)
  }
  return (VX1000_CLOCK()) + dt; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_CheckTimeout                                                                                         */
/* API name:      none (only used internally)                                                                                 */
/* Return value:  true if action timed out, false otherwise.                                                                  */
/* Parameter1:    timeout: the deadline in [ticks].                                                                           */
/*                No invalid input possible.                                                                                  */
/* Preemption:    No problem                                                                                                  */
/* Termination:   No effect                                                                                                   */
/* Precondition1: vx1000_SetTimeoutUs() must have been called before for an absolute time max. 0x7FFFFFFF ticks in the past.  */
/* Description:   checks whether the precomputed deadline has be passed already or not.                                       */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_CheckTimeout)(VX1000_UINT32 timeout)
{
  VX1000_UINT8 retVal = 0;  /* "no timeout, yet" */
  if (((timeout - (VX1000_CLOCK())) & (1UL << (((VX1000_CLOCK_TIMER_SIZE) - 1) & 0x1F))) != 0) { retVal = 1; } /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  return retVal;
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_StimSkip                                                                                             */
/* API name:      VX1000_STIM_SKIP                                                                                            */
/* Wrapper API:   VX1000If_StimSkip                                                                                           */
/* Return value:  None                                                                                                        */
/* Parameter1:    eventNumber E [0,gVX1000.Olda.EventCount)                                                                   */
/* Precondition1:                                                                                                             */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Description:   Sends a stim skip event to tell the VX not to stimulate the next cycle (in effect a dummy STIM request)     */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
void VX1000_SUFFUN(vx1000_StimSkip)( VX1000_UINT8 stim_event )
{
  if (VX1000_STIM_ACTIVE(stim_event) != 0)
  {
    VX1000_SPECIAL_EVENT(VX1000_ECU_EVT_STIM_SKIP( stim_event )) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */ /* PRQA S 0501 */ /* Violation of MISRA rule 12.8 because 64-bit data types are known to be not MISRA-conformant. This is just an example to be overruled by a user-defined implementation of an atomic 64-bit-store operation. */
  }
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_StimInit                                                                                             */
/* API name:      None                                                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:    This function should not interrupt and should not be interrupted by code that reads the stimulation data.   */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_InitAsyncStart must have been called.                                                                */
/* Description:   Initialises the stimulation control structures. Called in VX1000_INIT_ASYNC_START() and every time STIM     */
/*                is turned off                                                                                               */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
static void VX1000_SUFFUN(vx1000_StimInit)(void)
{
  VX1000_UINT32 i;

  gVX1000.Stim.Control = 0;
  gVX1000.Stim.EvtOffset = VX1000_STIM_EVENT_OFFSET;
  gVX1000.Stim.EvtNumber = VX1000_STIM_EVENT_COUNT;

#if !defined(VX1000_STIM_FORCE_V1)
  gVX1000.Stim.Version = 0;
  for (i = 0; i < VX1000_STIM_EVENT_COUNT; i++)
  {
    (VX1000_STIMEVENT_ARRAYNAME)[i].Ctr = 0;
    (VX1000_STIMEVENT_ARRAYNAME)[i].RqCtr = 0;
    (VX1000_STIMEVENT_ARRAYNAME)[i].Enable = 0;
#if !defined(VX1000_STIM_BY_OLDA)
    gVX1000.Stim.Event[i].Copying = 0;
#endif /* !VX1000_STIM_BY_OLDA */
  }
  gVX1000.Stim.Enable = 0;
#else /* VX1000_STIM_FORCE_V1 */
  gVX1000.Stim.Version = 1;
  gVX1000.Stim.EventPointer = VX1000_PTR2SE_TO_ADDRESS(&gVX1000.Stim.Events);  /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if defined(VX1000_HOOK_BASED_BYPASSING)
  gVX1000.Stim.hbbLUTNumber = VX1000_BYPASS_HBB_LUT_ENTRIES;
  gVX1000.Stim.hbbLUTPointer = (VX1000_UINT32)( VX1000_BYPASS_HBB_LUT_ADDR ); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  gVX1000.Stim.hbbLUTVXPointer = 0;
#endif /* VX1000_HOOK_BASED_BYPASSING */
  gVX1000.Stim.Events.MagicId = VX1000_STIM_EVENT_MAGIC;
  gVX1000.Stim.Events.Version = 1;

  for (i = 0; i < VX1000_STIM_EVENT_COUNT; i++)
  {
    (VX1000_STIMEVENT_ARRAYNAME)[i].DaqEvent = 0;
    (VX1000_STIMEVENT_ARRAYNAME)[i].Enable = VX1000_STIM_INACTIVE;
    (VX1000_STIMEVENT_ARRAYNAME)[i].EventType = VX1000_BYPASS_TYPE_DIRECT;
    (VX1000_STIMEVENT_ARRAYNAME)[i].Copying = 0;
    (VX1000_STIMEVENT_ARRAYNAME)[i].Ctr = 0;
    (VX1000_STIMEVENT_ARRAYNAME)[i].RqCtr = 0;
  }
  VX1000_SET_STIM_INFO()
  gVX1000.Stim.Enable = VX1000_STIM_GLOBAL_INACTIVE;
#endif /* VX1000_STIM_FORCE_V1 */
  if ( (VX1000_PTR2VU8_TO_ADDRESS(&gVX1000.Stim.EvtOffset)) != (0x8UL + VX1000_STIM_PTR) ) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  {
    gVX1000.ToolDetectState |= VX1000_TDS_ERROR;
    VX1000_ERRLOGGER(VX1000_ERRLOG_STRUCTS_PADDED)
  }
  gVX1000.Stim.MagicId = (VX1000_UINT32)(VX1000_STIM_MAGIC);
}


#if defined(VX1000_STIM_BENCHMARK)

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_StimBenchmarkInit                                                                                    */
/* API name:      None                                                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: None                                                                                                        */
/* Description:   Initialises the STIM benchmark data.                                                                        */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
static void VX1000_SUFFUN(vx1000_StimBenchmarkInit)( void )
{
  VX1000_UINT32 i;

  for (i = 0; i < VX1000_STIM_EVENT_COUNT; i++)
  {
    gVX1000_STIM_Begin[i] = 0;
    gVX1000_STIM_Duration[i] = 0;
  }

#if defined(VX1000_STIM_HISTOGRAM)
  for (i = 0; i < 256; i++) { gVX1000_STIM_Histogram[i] = 0; }
  for (i = 0; i < 16; i++) { gVX1000_STIM_Histogram2[i] = 0; }
#endif /* VX1000_STIM_HISTOGRAM */
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_StimBenchmarkStimEnd                                                                                 */
/* API name:      None                                                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    eventNumber E [VX1000_STIM_EVENT_OFFSET,gVX1000.Olda.EventCount)                                            */
/*                Validity has to be ensured by caller.                                                                       */
/* Parameter2:    flag signalling whether there was a stim timeout (TRUE) or not (FALSE)                                      */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: gVX1000_STIM_Begin shall have been initialised for the same event before.                                   */
/* Description:   Record de duration of the current stim transfer; also update the stim histogram.                            */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
static void VX1000_SUFFUN(vx1000_StimBenchmarkStimEnd)( VX1000_UINT8 stim_event, VX1000_UINT8 timeout_flag )
{
  VX1000_UINT32 t0,t1,dt;

  t0 = gVX1000_STIM_Begin[stim_event - (VX1000_STIM_EVENT_OFFSET)];
  if (t0 != 0)
  {
    gVX1000_STIM_Begin[stim_event - (VX1000_STIM_EVENT_OFFSET)] = 0;
    t1 = VX1000_CLOCK(); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    dt =  t1 - t0;
    gVX1000_STIM_Duration[stim_event - (VX1000_STIM_EVENT_OFFSET)] = dt; /* Last delay for each individual event */

    /* Build the histograms for event VX1000_STIM_HISTOGRAM */
#if defined(VX1000_STIM_HISTOGRAM)
    if (stim_event == (VX1000_STIM_HISTOGRAM))
    {
      gVX1000_STIM_Histogram2[0]++; /* Index 0 is the cycles with 0 timeouts counter ! */

      if (0==timeout_flag)
      {
        /* Build the delay histogram */
        /* 20us resolution, 0..5100us:     */
        if ((VX1000_CLOCK_TICKS_PER_MS) >= 50) /* note: this cannot be replaced by a preprocessor condition because the user may define VX1000_CLOCK_TICKS_PER_MS to a runtime evaluated formula/function */
        {
          t0 = (VX1000_UINT32)(dt / (VX1000_UINT32)((VX1000_CLOCK_TICKS_PER_MS) / 50UL));
        }
        else
        {
          t0 = (VX1000_UINT32)((VX1000_UINT32)(50UL * dt) / (VX1000_CLOCK_TICKS_PER_MS));
        }
        if (t0 >= 256UL) { t0 = 255UL; }
        gVX1000_STIM_Histogram[t0]++;
      }
      else
      {
        /* Build the timeout burst count histogram */
        /* 0..15 */
        t0 = gVX1000.Stim.TimeoutCtr2;
        if (t0 <= 15)
        {
          gVX1000_STIM_Histogram2[t0]++;
          gVX1000_STIM_Histogram2[t0-1]--; /* t0 is always >0 when timeout_flag is set */
        }
      }
    }
#else /* !VX1000_STIM_HISTOGRAM */
    VX1000_DUMMYREAD(timeout_flag) /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
#endif /* !VX1000_STIM_HISTOGRAM */
  }
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_StimBenchmarkStimCheck                                                                               */
/* API name:      None                                                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_StimInit must have been called.                                                                      */
/* Description:                                                                                                               */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static void VX1000_SUFFUN(vx1000_StimBenchmarkStimCheck)( void )
{
#if defined(VX1000_STIM_HISTOGRAM)
  if (VX1000_STIM_ACTIVE(VX1000_STIM_HISTOGRAM) != 0)
  {
#if !defined(VX1000_STIM_FORCE_V1)
    if ((VX1000_STIMEVENT_ARRAYNAME)[(VX1000_STIM_HISTOGRAM) - (VX1000_STIM_EVENT_OFFSET)].Ctr
    ==  (VX1000_STIMEVENT_ARRAYNAME)[(VX1000_STIM_HISTOGRAM) - (VX1000_STIM_EVENT_OFFSET)].RqCtr)
#else /* VX1000_STIM_FORCE_V1 */
    if (VX1000_SUFFUN(vx1000_BypassCheckCounters)(VX1000_STIM_HISTOGRAM) == VX1000_STIM_RET_SUCCESS)
#endif /* VX1000_STIM_FORCE_V1 */
    {
      VX1000_SUFFUN(vx1000_StimBenchmarkStimEnd)( VX1000_STIM_HISTOGRAM, 0 );
    }
  }
#endif /* VX1000_STIM_HISTOGRAM */
}


#endif /* VX1000_STIM_BENCHMARK */

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_StimControl                                                                                          */
/* API name:      VX1000_STIM_CONTROL / VX1000_BYPASS_CONTROL                                                                 */
/* Wrapper API:   VX1000If_StimControl / VX1000If_BypassControl                                                               */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_StimInit must have been called.                                                                      */
/* Description:   The Stim-Keep-Alive-Handler (to be called cyclically by the application).                                   */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
void VX1000_SUFFUN(vx1000_StimControl)(void)
{
  if ((gVX1000.Stim.Control != 0) && (0==gVX1000.Stim.Enable))
  {

    /* Clear benchmark data */
#if defined(VX1000_STIM_BENCHMARK)
    VX1000_SUFFUN(vx1000_StimBenchmarkInit)();
#endif /* VX1000_STIM_BENCHMARK */

    gVX1000.Stim.TimeoutCtr = 0;
    gVX1000.Stim.TimeoutCtr2 = 0;

#if defined(VX1000_STIM_TYPE_0)
    VX1000_SPECIAL_EVENT(VX1000_EVENT_STIM_ACK) /* Send an acknowledge STIM event when the ECU is ready for STIM */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_STIM_TYPE_0 */

    gVX1000.Stim.Enable = VX1000_STIM_GLOBAL_VX_ENABLE;
  }
  else
  {
    /* Re-initialise STIM when VX1000 turns STIM off (but hold the timeout counters) */
    if ((0==gVX1000.Stim.Control) && (gVX1000.Stim.Enable != VX1000_STIM_GLOBAL_INACTIVE)) { VX1000_SUFFUN(vx1000_StimInit)(); }
  }

#if defined(VX1000_STIM_BENCHMARK)
  /* STIM Benchmark: Specific measurements for a single event from request to acknowledge */
  VX1000_SUFFUN(vx1000_StimBenchmarkStimCheck)();
#endif /* VX1000_STIM_BENCHMARK */
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_StimActive                                                                                           */
/* API name:      VX1000_STIM_ACTIVE                                                                                          */
/* Wrapper API:   VX1000If_StimActive                                                                                         */
/* Return value:  TRUE (active) or FALSE (inactive)                                                                           */
/* Parameter1:    eventNumber E [VX1000_STIM_EVENT_OFFSET,gVX1000.Olda.EventCount)                                            */
/*                Validity has to be ensured by caller!                                                                       */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_StimInit must have been called.                                                                      */
/* Description:   Returns true if stim is active both globally and for a specific event that is of type STIM.                 */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_StimActive)( VX1000_UINT8 stim_event )
{
  VX1000_UINT8 retVal = 0;  /* "inactive" */
  if (((((gVX1000.Stim.Control) != 0) && (gVX1000.Stim.Enable != (VX1000_STIM_GLOBAL_INACTIVE)))
#if (0==(VX1000_STIM_EVENT_OFFSET))
  && ((stim_event) < (VX1000_STIM_EVENT_COUNT)))
#else /* VX1000_STIM_EVENT_OFFSET */
  && (((stim_event) >= (VX1000_STIM_EVENT_OFFSET)) && ((stim_event) < ((VX1000_STIM_EVENT_OFFSET) + (VX1000_STIM_EVENT_COUNT)))))
#endif /* VX1000_STIM_EVENT_OFFSET */
  && ((VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].Enable != 0))
  {
    retVal = 1;  /* "active" */
  }
  return retVal;
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_StimRequest                                                                                          */
/* API name:      VX1000_STIM_REQUEST                                                                                         */
/* Wrapper API:   VX1000If_StimRequest                                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    eventNumber E [VX1000_STIM_EVENT_OFFSET,gVX1000.Olda.EventCount)                                            */
/*                Validity has to be ensured by caller!                                                                       */
/* Preemption:    This function must not interrupt and not be interrupted by vx1000_* functions operating on the same event.  */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Request a specific STIM data set associated to event stim_event.                                            */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
void VX1000_SUFFUN(vx1000_StimRequest)( VX1000_UINT8 stim_event )
{
#if defined(VX1000_STIM_BENCHMARK)
  gVX1000_STIM_Begin[stim_event - (VX1000_STIM_EVENT_OFFSET)] = VX1000_CLOCK(); /* Timing measurement */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_STIM_BENCHMARK */
#if defined(VX1000_STIM_FORCE_V1)
  /* Only if the event is "direct stimulation" instead of stim-by-olda, send a stim request */
  if ((VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].EventType == VX1000_BYPASS_TYPE_DIRECT)
#endif /* VX1000_STIM_FORCE_V1 */
  {
    (VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].RqCtr++;
    VX1000_STIM_REQUEST_EVENT(stim_event) /* PRQA S 0303 */ /* PRQA S 0306 */ /* macro expansion contains VX1000_ADDR_TO_PTR2XXX: cannot avoid violating MISRA rule 11.3 because in a few configurations a peripheral register at a fix HW address must be accessed via pointer to update the HW value */ /* PRQA S 0501 */ /* Violation of MISRA rule 12.8 because 64-bit data types are known to be not MISRA-conformant. This is just an example to be overruled by a user-defined implementation of an atomic 64-bit-store operation. */
  }
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_StimWait                                                                                             */
/* API name:      VX1000_STIM_WAIT (expression) / VX1000_STIM_WAIT_VOID (statement) /                                         */
/*                VX1000_BYPASS_STIM (expression) / VX1000_BYPASS_STIM_VOID (statement)                                       */
/* Wrapper API:   VX1000If_StimWait (expression) / VX1000If_StimWaitVoid (statement) /                                        */
/*                VX1000If_BypassStim (expression) / VX1000If_BypassStimVoid  (statement)                                     */
/* Return value:  0 (data arrived before timeout or timeout but data still copied successfully)                               */
/*                1 (timeout, no new data arrived or error during copying and destination data corrupted)                     */
/* Parameter1:    eventNumber E [VX1000_STIM_EVENT_OFFSET,gVX1000.Olda.EventCount)                                            */
/*                Validity has to be ensured by caller!                                                                       */
/* Parameter2:    flag: 0 (don't copy data / 1 (copy olda data)                                                               */
/*                Validity has to be ensured by caller!                                                                       */
/* Parameter3:    timeout in microseconds, starting from related call to VX1000_STIM_REQUEST                                  */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_StimInit must have been called.                                                                      */
/* Precondition2: VX1000_STIM_REQUEST must have been called recently for the same event.                                      */
/* Description:   Busy wait until a specific STIM request is fulfilled.                                                       */
/*                Optionally processes all transfer descriptors assigned to parameter1 (only available with olda).            */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_StimWait)( VX1000_UINT8 stim_event, VX1000_UINT8 copy_enable, VX1000_UINT32 timeout_us )
{
  VX1000_UINT8 errorcode = 0U;
#if ((!defined(VX1000_STIM_BY_OLDA)) || defined(VX1000_STIM_FORCE_V1))
  VX1000_UINT8 lastChance = 0U;
#endif /* !VX1000_STIM_BY_OLDA || VX1000_STIM_FORCE_V1 */
  VX1000_UINT8 keepLooping = 1U;
#if (!defined(VX1000_RESET_STIM_TIMEOUT)) || (!defined(VX1000_CHECK_STIM_TIMEOUT))
  static volatile VX1000_UINT32 gVX1000_timeout[VX1000_STIM_EVENT_COUNT]; /* can be per core and static */
#endif /* !VX1000_RESET_STIM_TIMEOUT || !VX1000_CHECK_STIM_TIMEOUT */

#if !defined(VX1000_STIM_FORCE_V1)
  gVX1000_timeout[(stim_event) - (VX1000_STIM_EVENT_OFFSET)] = VX1000_SUFFUN(vx1000_SetTimeoutUs)((VX1000_UINT32)(timeout_us)); /* reset STIM timeout */
    while (keepLooping != 0U) /* Busy wait with timeout until direct stimulation is done (or olda stimulation data arrived) */
    {
      VX1000_UINT8 timedOut;
#if !defined(VX1000_STIM_BY_OLDA)
      VX1000_UINT8 copying;
#endif /* !VX1000_STIM_BY_OLDA */
      VX1000_UINT8 haveData;
#if defined(VX1000_TARGET_POWERPC)
    /* For parallel pods perform dummy writes inside a trace window that is active for the executing core. */
    /* This way, while waiting for the STIM data, stuck event triggers will be flushed off the Nexus FIFO. */
#if (!defined(VX1000_OLDA)) && (!defined(VX1000_SUPPRESS_TRACE))
#if (VX1000_MEMSYNC_TRIGGER_COUNT == 0)
    gVX1000.EventTimestamp = VX1000_CLOCK(); /* gVX1000 is always in a trace window visible by all cores, so this write will produce the desired trace load and as a side effect allow the NIOS to better estimate the FIFO fill level */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#elif (VX1000_MEMSYNC_TRIGGER_COUNT == 1)
    /* A byte store into the sync area s guaranteed to produce the desired trace load but as byte will NOT be (mis)interpreted by the decoder because ii only processes 64bit stores */
    (VX1000_ADDR_TO_PTR2U8(gVX1000.MemSyncTrigPtr + 12UL))[0] = 0; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else /* VX1000_MEMSYNC_TRIGGER_COUNT > 1 */
    {
      VX1000_UINT32 core;
      for (core = 0; core < (VX1000_MEMSYNC_TRIGGER_COUNT); ++core)
      {
        /* at least one trace window of at least 8 bytes will be active for this core. A byte store into that area will produce the desired trace load but will NOT be (mis)interpreted by the decoder */
        (VX1000_ADDR_TO_PTR2U8(((VX1000_ADDR_TO_PTR2VU32(gVX1000.MemSyncTrigPtr))[core]) + 12UL))[0] = 0; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      }
    }
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 1 */
#endif /* !VX1000_OLDA & !VX1000_SUPPRESS_TRACE */
#endif /* VX1000_TARGET_POWERPC */
    /*
    Note that in order to avoid race conditions, the order of the following
    three assignments must not be changed under any circumstances!!!
    */
    timedOut = VX1000_SUFFUN(vx1000_CheckTimeout)(gVX1000_timeout[stim_event - (VX1000_STIM_EVENT_OFFSET)]);
#if !defined(VX1000_STIM_BY_OLDA)
    copying = (VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].Copying;
#endif /* !VX1000_STIM_BY_OLDA */
    haveData = ((VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].Ctr == (VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].RqCtr) ? 1U : 0U;
    if (haveData != 0U)
    {
      /* data has arrived */
      keepLooping = 0U;
#if !defined(VX1000_STIM_BY_OLDA)
      if (lastChance != 0U)
      {
        errorcode = 1U; /* timeout, but new data already valid */
      }
#endif /* !VX1000_STIM_BY_OLDA */
    }
    else if (timedOut != 0U)
    {
#if !defined(VX1000_STIM_BY_OLDA)
      if ((lastChance == 0U) && (copying != 0U))
      {
        /* If timeout happened, but copying data has already started,       */
        /* concede a second chance to complete within the next millisecond: */
        lastChance = 1U;
        gVX1000_timeout[(stim_event) - (VX1000_STIM_EVENT_OFFSET)] = VX1000_SUFFUN(vx1000_SetTimeoutUs)((VX1000_UINT32)(1000U)); /* reset STIM timeout */
      }
      else
#endif /* !VX1000_STIM_BY_OLDA */
      {
        keepLooping = 0U;
        errorcode = 2U; /* timeout, no new data or data incomplete */
#if !defined(VX1000_STIM_BY_OLDA)
        if (lastChance == 0U)
#endif /* !VX1000_STIM_BY_OLDA */
        {
          if (VX1000_STIM_ACTIVE(stim_event) != 0U)
          {
            VX1000_SPECIAL_EVENT(VX1000_EVENT_STIM_TIMEOUT(stim_event)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
        }
#if !defined(VX1000_STIM_BY_OLDA)
        else
        {
          VX1000_SPECIAL_EVENT(VX1000_EVENT_STIM_ERR(stim_event)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        }
#endif /* !VX1000_STIM_BY_OLDA */
      }
    }
    else
    {
      ; /* empty else is only here for MISRA */
    }
  } /* while */

  if (errorcode == 2) /* Timeout (no new valid data) --> increment timeout counter and timeout burst counter */
  {
    gVX1000.Stim.TimeoutCtr++;
    gVX1000.Stim.TimeoutCtr2++;
  }
  else /* either no timeout at all or timeout, but still all data arrived successfully */
  {
    gVX1000.Stim.TimeoutCtr2 = 0; /* Clear the timeout burst counter */
#if defined(VX1000_STIM_BY_OLDA)
    if (copy_enable != 0)
    {
      /* Copy the STIM data to final memory locations */
      VX1000_SUFFUN(vx1000_StimTransfer)(stim_event);
    }
#else  /* !VX1000_STIM_BY_OLDA */
    VX1000_DUMMYREAD(copy_enable) /* only here to prevent compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
#endif /* !VX1000_STIM_BY_OLDA */
  }
  errorcode >>= 1;
#if defined(VX1000_STIM_BENCHMARK)
  VX1000_SUFFUN(vx1000_StimBenchmarkStimEnd)(stim_event, errorcode);
#endif /* VX1000_STIM_BENCHMARK */
  return errorcode;
#else /* VX1000_STIM_FORCE_V1 */
  VX1000_UINT8 retVal = 2;

  if ((VX1000_STIM_ACTIVE(stim_event) != VX1000_STIM_GLOBAL_INACTIVE)
  &&  ((VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].Enable >= VX1000_STIM_DAQ_SENT))
  {
    VX1000_INT32 specialEvent = -1;
    gVX1000_timeout[(stim_event) - (VX1000_STIM_EVENT_OFFSET)] = VX1000_SUFFUN(vx1000_SetTimeoutUs)((VX1000_UINT32)(timeout_us)); /* reset STIM timeout */

    /* Step 1: Busy wait to check for valid STIM data */
    /* Check whether new data have arrived - in the case of direct stim, counters are checked, for OLDA, the buffer timestamps */
    while (keepLooping != 0)
    {
      VX1000_UINT8 timedOut;
      VX1000_UINT8 copying;
      VX1000_UINT8 haveData;
#if defined(VX1000_TARGET_POWERPC)
      /* For parallel pods perform dummy writes inside a trace window that is active for the executing core. */
      /* This way, while waiting for the STIM data, stuck event triggers will be flushed off the Nexus FIFO. */
#if (!defined(VX1000_OLDA)) && (!defined(VX1000_SUPPRESS_TRACE))
#if (VX1000_MEMSYNC_TRIGGER_COUNT == 0)
      gVX1000.EventTimestamp = VX1000_CLOCK(); /* gVX1000 is always in a trace window visible by all cores, so this write will produce the desired trace load and as a side effect allow the NIOS to better estimate the FIFO fill level */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#elif (VX1000_MEMSYNC_TRIGGER_COUNT == 1)
      /* A byte store into the sync area s guaranteed to produce the desired trace load but as byte will NOT be (mis)interpreted by the decoder because ii only processes 64bit stores */
      (VX1000_ADDR_TO_PTR2U8(gVX1000.MemSyncTrigPtr + 12UL))[0] = 0; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else /* VX1000_MEMSYNC_TRIGGER_COUNT > 1 */
      {
        VX1000_UINT32 core;
        for (core = 0; core < (VX1000_MEMSYNC_TRIGGER_COUNT); ++core)
        {
          /* at least one trace window of at least 8 bytes will be active for this core. A byte store into that area will produce the desired trace load but will NOT be (mis)interpreted by the decoder */
          (VX1000_ADDR_TO_PTR2U8(((VX1000_ADDR_TO_PTR2VU32(gVX1000.MemSyncTrigPtr))[core]) + 12UL))[0] = 0; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        }
      }
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 1 */
#endif /* !VX1000_OLDA & !VX1000_SUPPRESS_TRACE */
#endif /* VX1000_TARGET_POWERPC */
      /*
      Note that in order to avoid race conditions, the order of the following
      three assignments must not be changed under any circumstances!!!
      */
      timedOut = VX1000_SUFFUN(vx1000_CheckTimeout)(gVX1000_timeout[stim_event - (VX1000_STIM_EVENT_OFFSET)]);
      copying = (VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].Copying;
      haveData = (VX1000_SUFFUN(vx1000_BypassCheckCounters)(stim_event) == VX1000_STIM_RET_SUCCESS) ? 1U : 0U;
      if (haveData != 0U)
      {
        /* data has arrived */
        keepLooping = 0U;
        if (lastChance != 0U)
        {
          errorcode = 1U; /* timeout, but new data already valid */
        }
      }
      else if (timedOut != 0U)
      {
        if ((lastChance == 0U) && (copying != 0U) && ((VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].EventType == VX1000_BYPASS_TYPE_DIRECT))
        {
          /* If timeout happened, but copying data has already started,   */
          /* concede a second chance to complete within the next millisecond: */
          lastChance = 1;
          gVX1000_timeout[(stim_event) - (VX1000_STIM_EVENT_OFFSET)] = VX1000_SUFFUN(vx1000_SetTimeoutUs)((VX1000_UINT32)(1000U)); /* reset STIM timeout */
        }
        else
        {
          keepLooping = 0U;
          if (lastChance != 0U)
          {
            specialEvent = ((VX1000_INT32) VX1000_EVENT_STIM_ERR(stim_event));
            errorcode = 3U; /* timeout, data incomplete */
            (VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].Enable = VX1000_STIM_BUFFER_INVALID; /* Data invalid */
          }
          else
          {
            /* Do not send a timeout for OLDA-based stimulation */
            errorcode = 2U; /* timeout, no new data */
            if ((VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].EventType == VX1000_BYPASS_TYPE_DIRECT)
            {
              specialEvent = ((VX1000_INT32) VX1000_EVENT_STIM_TIMEOUT(stim_event));
            }
          }
        }
      }
      else
      {
        ; /* this empty else case and semicolon are only here for MISRA */
      }
      if (specialEvent != -1)
      {
        VX1000_SPECIAL_EVENT((VX1000_UINT32)specialEvent) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      }
    } /* while */

    /* Step 2: Check the errorcode and copy data in the case of OLDA */
    if (errorcode < 2) /* todo: care for "The result of this logical operation is always 'false'." compiler warning */
    {
      /* Set data valid flag for the event */
      (VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].Enable = VX1000_STIM_BUFFER_VALID;
#if defined(VX1000_STIM_BY_OLDA)
      if ((VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].EventType != VX1000_BYPASS_TYPE_DIRECT)
      {
        /* Copy data from VX-write to ECU-read buffer */
        if (VX1000_SUFFUN(vx1000_BypassCopyBuffer)(stim_event) != VX1000_STIM_RET_SUCCESS)
        {
          /* Flag errors during copying */
          (VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].Enable = VX1000_STIM_BUFFER_INVALID;
          errorcode = 3;
        }
        /* Copy the STIM data to final memory locations */
        else if ((copy_enable != 0)
        && ((VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].EventType == VX1000_BYPASS_TYPE_OLDA))
        {
#if defined(VX1000_BYPASS_ALL_CHANS_STIMD)
#if defined(VX1000_IS_INSTANT_BYP_ALLOWED)
          VX1000_UINT8 bypassAreAllChansStimd = VX1000_SUFFUN(vx1000_BypassAreAllChansStimd)();
          /* Only copy if this channel does not need to wait for other channels OR if valid stim data received from all other channels */
          if (((VX1000_IS_INSTANT_BYP_ALLOWED(stim_event)) && ((VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].Enable == VX1000_STIM_BUFFER_VALID)) || (bypassAreAllChansStimd == 1)) /* PRQA S 3356 */ /* cannot avoid violating MISRA rule 13.7 because of dependency on user-defined callback not guaranteed to be evaluable at compile time */
#else /* !VX1000_IS_INSTANT_BYP_ALLOWED */
          /* Only copy if valid stim data received from all other channels first */
          if (VX1000_SUFFUN(vx1000_BypassAreAllChansStimd)() == 1)
#endif /* !VX1000_IS_INSTANT_BYP_ALLOWED */
#else /* VX1000_BYPASS_ALL_CHANS_STIMD */
          if ((VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].Enable == VX1000_STIM_BUFFER_VALID)
#endif /* VX1000_BYPASS_ALL_CHANS_STIMD */
          {
            VX1000_SUFFUN(vx1000_StimTransfer)(stim_event);
          }
        } else
        {
          /* this else block is here only to satisfy MISRA*/
        }
      }
#else /* !VX1000_STIM_BY_OLDA */
    VX1000_DUMMYREAD(copy_enable)      /* dummy usage to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
#endif /* !VX1000_STIM_BY_OLDA */
    }

    /* Step 3: Check the errorcode for timeouts and errors */
    /* todo: care for the "The result of this logical operation is always 'true'." compiler warning */
    if (errorcode >= 2) /* Timeout (no new valid data) --> increment timeout counter and timeout burst counter */
    {
      gVX1000.Stim.TimeoutCtr++;
      gVX1000.Stim.TimeoutCtr2++;
      if (errorcode == 3)
      {
        retVal = 3;
      }
      else
      {
        retVal = 2;
      }
    }
    else /* either no timeout at all or timeout, but still all data arrived successfully */
    {
      gVX1000.Stim.TimeoutCtr2 = 0; /* Clear the timeout burst counter */
      retVal = 0;
    }
    errorcode >>= 1;
#if defined(VX1000_STIM_BENCHMARK)
    VX1000_SUFFUN(vx1000_StimBenchmarkStimEnd)(stim_event, errorcode);
#endif /* VX1000_STIM_BENCHMARK */
  } /* STIM event active & DAQ event sent */

  return retVal >> 1; /* turn the verbose internal status into the bool of the API */
#endif /* VX1000_STIM_FORCE_V1 */
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_Bypass                                                                                               */
/* API name:      VX1000_BYPASS (expression) / VX1000_BYPASS_VOID (statement)                                                 */
/* Wrapper API:   VX1000If_Bypass (expression) / VX1000If_BypassVoid (statement)                                              */
/* Return value:  status: 0 (bypassed code shall be activated because bypassing is not active)                                */
/*                1 (everything done, bypassed code shall be disabled)                                                        */
/*                2 (bypassing failed; it's up to the application design whether executing the bypassed code makes sense here)*/
/* Parameter1:    daq_event E [0,gVX1000.Olda.EventCount)                                                                     */
/*                Validity has to be ensured by caller!                                                                       */
/* Parameter2:    stim_event E [VX1000_STIM_EVENT_OFFSET,gVX1000.Olda.EventCount)                                             */
/*                Validity has to be ensured by caller!                                                                       */
/* Parameter3:    timeout in microseconds, starting right now.                                                                */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Initiate a bypass by sending DAQ data to the tool and implicitly requesting a stimulation (daq_event),      */
/*                then busy wait with timeout for the tool to complete the stimulation (stim_event).                          */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_Bypass)( VX1000_UINT8 daq_event, VX1000_UINT8 stim_event, VX1000_UINT32 timeout_us)
{
  VX1000_UINT8 retVal = 0;    /* 0 means "Bypass inactive, enable the bypassed code" */

#if !defined(VX1000_STIM_FORCE_V1)
  if ((VX1000_STIM_ACTIVE(stim_event)) != 0)
  {
    VX1000_EVENT(daq_event) /* PRQA S 0303 */ /* PRQA S 0306 */ /* macro expansion contains VX1000_ADDR_TO_PTR2XXX: cannot avoid violating MISRA rule 11.3 because in a few configurations a peripheral register at a fix HW address must be accessed via pointer to update the HW value */ /* PRQA S 0501 */ /* Violation of MISRA rule 12.8 because 64-bit data types are known to be not MISRA-conformant. This is just an example to be overruled by a user-defined implementation of an atomic 64-bit-store operation. */
    VX1000_STIM_REQUEST(stim_event)
    if ((VX1000_STIM_WAIT(stim_event, timeout_us)) != 0)
#else /* VX1000_STIM_FORCE_V1 */
  if ((VX1000_BYPASS_DAQ(daq_event, stim_event)) == VX1000_STIM_RET_SUCCESS)
  {
    if ((VX1000_STIM_WAIT(stim_event, timeout_us)) != 0) /* note: do NOT use VX1000_STIM_RET_SUCCESS here because STIM_WAIT's return codes differ from STIM_EVENT's! */
#endif /* VX1000_STIM_FORCE_V1 */
    {
      retVal = 2;           /* "Bypass active, timeout" */
    }
    else
    {
      retVal = 1;           /* "Bypass active, OK, disable the bypassed code" */
    }
  }
  return retVal;
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_BypassTrigger                                                                                        */
/* API name:      VX1000_BYPASS_TRIGGER (expression) / VX1000_BYPASS_TRIGGER_VOID (statement)                                 */
/* Wrapper API:   VX1000If_BypassTrigger (expression) / VX1000If_BypassTriggerVoid (statement)                                */
/* Return value:  status: 0 (bypassed code shall be activated because bypassing is not active)                                */
/*                1 (everything done, bypassed code shall be disabled)                                                        */
/* Parameter1:    daq_event E [0,gVX1000.Olda.EventCount)                                                                     */
/*                Validity has to be ensured by caller!                                                                       */
/* Parameter2:    stim_event E [VX1000_STIM_EVENT_OFFSET,gVX1000.Olda.EventCount)                                             */
/*                Validity has to be ensured by caller!                                                                       */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Initiate a bypass by sending DAQ data to the tool and implicitly requesting a stimulation (daq_event).      */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassTrigger)( VX1000_UINT8 daq_event, VX1000_UINT8 stim_event )
{
  VX1000_UINT8 retVal = VX1000_STIM_RET_INACTIVE;  /* 0 means "Bypass inactive" */

  if (VX1000_STIM_ACTIVE(stim_event) != 0)
  {
    VX1000_EVENT(daq_event) /* PRQA S 0303 */ /* PRQA S 0306 */ /* macro expansion contains VX1000_ADDR_TO_PTR2XXX: cannot avoid violating MISRA rule 11.3 because in a few configurations a peripheral register at a fix HW address must be accessed via pointer to update the HW value */ /* PRQA S 0501 */ /* Violation of MISRA rule 12.8 because 64-bit data types are known to be not MISRA-conformant. This is just an example to be overruled by a user-defined implementation of an atomic 64-bit-store operation. */
#if defined(VX1000_STIM_FORCE_V1)
    if ((VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].Enable < VX1000_STIM_DAQ_SENT)
    {
      (VX1000_STIMEVENT_ARRAYNAME)[stim_event - (VX1000_STIM_EVENT_OFFSET)].Enable = VX1000_STIM_DAQ_SENT;
    }
#endif /* VX1000_STIM_FORCE_V1 */
    retVal = VX1000_STIM_RET_SUCCESS;              /* "Bypass active" */
  }
  return retVal;
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_BypassDaq                                                                                            */
/* API name:      VX1000_BYPASS_DAQ (expression) / VX1000_BYPASS_DAQ_VOID (statement)                                         */
/* Wrapper API:   VX1000If_BypassDaq (expression) / VX1000If_BypassDaqVoid (statement)                                        */
/* Return value:  status: 0 (bypassed code shall be activated because bypassing is not active)                                */
/*                1 (everything done, bypassed code shall be disabled)                                                        */
/* Parameter1:    daq_event E [0,gVX1000.Olda.EventCount)                                                                     */
/*                Validity has to be ensured by caller!                                                                       */
/* Parameter2:    stim_event E [VX1000_STIM_EVENT_OFFSET,gVX1000.Olda.EventCount)                                             */
/*                Validity has to be ensured by caller!                                                                       */
/* Description:   Initiate a bypass by sending a DAQ event followed by a stim request                                         */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassDaq)( VX1000_UINT8 daq_event, VX1000_UINT8 stim_event )
{
  VX1000_UINT8 retVal = VX1000_BYPASS_TRIGGER(daq_event, stim_event);
  if (retVal != (VX1000_STIM_RET_INACTIVE))
  {
    VX1000_STIM_REQUEST(stim_event)
  }
  return retVal;
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_BypassWait                                                                                           */
/* API name:      VX1000_BYPASS_WAIT (expression) / VX1000_BYPASS_WAIT_VOID (statement)                                       */
/* Wrapper API:   VX1000If_BypassWait (expression) / VX1000If_BypassWaitVoid (statement)                                      */
/* Return value:  status: 0 (bypassed code shall be activated because bypassing is not active)                                */
/*                1 (everything done, bypassed code shall be disabled)                                                        */
/*                2 (bypassing failed; it's up to the application design whether executing the bypassed code makes sense here)*/
/* Parameter1:    stim_event E [VX1000_STIM_EVENT_OFFSET,gVX1000.Olda.EventCount)                                             */
/*                Validity has to be ensured by caller!                                                                       */
/* Parameter2:    timeout in microseconds, starting right now.                                                                */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Busy wait with timeout for the tool to complete a stimulation (stim_event) that has been initiated          */
/*                beforehand by an appropriate call to VX1000_BYPASS_TRIGGER.                                                 */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassWait)( VX1000_UINT8 stim_event, VX1000_UINT32 timeout_us )
{
  VX1000_UINT8 retVal = 0;    /* 0 means "Bypass inactive, enable the bypassed code" */

  if (VX1000_STIM_ACTIVE(stim_event) != 0)
  {
    VX1000_STIM_REQUEST(stim_event)
    if ((VX1000_STIM_WAIT(stim_event, timeout_us)) != 0) /* note: do NOT use VX1000_STIM_RET_SUCCESS here because STIM_WAIT's return codes differ from STIM_EVENT's! */
    {
      retVal = 2;           /* "Bypass active, timeout. It may be too late to enable the bypassed code" */
    }
    else
    {
      retVal = 1;           /* "Bypass active, ok, disable the bypassed code" */
    }
  }
  return retVal;
}

#if defined(VX1000_HOOK_BASED_BYPASSING)

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_BypassHbbHookValid                                                                                   */
/* API name:      None                                                                                                        */
/* Return value:  status: 0 (the given hook is not valid: invalid ID, stim inactive, invalid LUT address, no valid stim data) */
/*                1 (the given hook is valid and has valid data ready)                                                        */
/* Parameter1:    HookID H [0,VX1000_BYPASS_HBB_LUT_ENTRIES)                                                                  */
/*                Validity checked by the function                                                                            */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Check whether valid data corresponding to the given Hook ID is present in the buffer                        */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassHbbHookValid)(VX1000_UINT32 HookID)
{
  VX1000_UINT8 retVal = 0;
  /* First check whether the hook table has a valid pointer and if the hook is valid */
  if ( (gVX1000.Stim.hbbLUTVXPointer != 0) && (HookID < VX1000_BYPASS_HBB_LUT_ENTRIES) )
  {
    /* Is the stim event valid and the olda address set */
    if ( (VX1000_STIM_ACTIVE((VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent)) && ((VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress != 0xFFFF) ) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    {
#if defined(VX1000_BYPASS_ALL_CHANS_STIMD) && defined(VX1000_STIM_FORCE_V1)
#if defined(VX1000_IS_INSTANT_BYP_ALLOWED)
      VX1000_UINT8 bypassAreAllChansStimd = VX1000_SUFFUN(vx1000_BypassAreAllChansStimd)();
      /* Does this channel not need to wait for other channels OR have all channels been stimulated */
      if (((VX1000_IS_INSTANT_BYP_ALLOWED((VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent)) && (gVX1000.Stim.Events.Event[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent - (VX1000_STIM_EVENT_OFFSET)].Enable == VX1000_STIM_BUFFER_VALID)) || (bypassAreAllChansStimd == 1)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */ /* PRQA S 3356 */ /* cannot avoid violating MISRA rule 13.7 because of dependency on user-defined callback not guaranteed to be evaluable at compile time */
#else /* !VX1000_IS_INSTANT_BYP_ALLOWED */
      /* Have all channels been stimulated */
      if (VX1000_SUFFUN(vx1000_BypassAreAllChansStimd)() == 1)
#endif /* !VX1000_IS_INSTANT_BYP_ALLOWED */
#else /* !VX1000_BYPASS_ALL_CHANS_STIMD || !VX1000_STIM_FORCE_V1 */
      /* Has this single channel been stimulated */
      if (gVX1000.Stim.Events.Event[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent - (VX1000_STIM_EVENT_OFFSET)].Enable == VX1000_STIM_BUFFER_VALID) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* !VX1000_BYPASS_ALL_CHANS_STIMD || !VX1000_STIM_FORCE_V1 */
      {
        {
          retVal = 1;
        }
      }
    }
  }
  return retVal;
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_BypassHbbGetVal_8                                                                                    */
/* API name:      VX1000_BYPASS_HBB_GETVAL_8 (expression)                                                                     */
/* Wrapper API:   VX1000If_BypassHbbGetval8  (expression)                                                                     */
/* Return value:  VX1000_UINT8 corresponding to the stimulated value if hook is valid and data available                      */
/*                DefaultValue is returned otherwise                                                                          */
/* Parameter1:    HookID H [0,VX1000_BYPASS_HBB_LUT_ENTRIES)                                                                  */
/*                Validity checked by the function                                                                            */
/* Parameter2:    DefaultValue D to be returned if hook is not valid                                                          */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Check whether valid data corresponding to the given Hook ID is present in the buffer                        */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassHbbGetVal_8)(VX1000_UINT32 HookID, VX1000_UINT8 DefaultValue)
{
  if ((VX1000_SUFFUN(vx1000_BypassHbbHookValid)(HookID)) != 0)
  {
    VX1000_OLDA_EVENT_T *pEventList = VX1000_ADDR_TO_PTR2OE(gVX1000.Olda.EventList); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    VX1000_UINT8 *src = VX1000_ADDR_TO_PTR2U8(pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    DefaultValue = src[0];
  }
  return DefaultValue;
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_BypassHbbGetVal_16                                                                                   */
/* API name:      VX1000_BYPASS_HBB_GETVAL_16 (expression)                                                                    */
/* Wrapper API:   VX1000If_BypassHbbGetval16  (expression)                                                                    */
/* Return value:  VX1000_UINT16 corresponding to the stimulated value if hook is valid and data available                     */
/*                DefaultValue is returned otherwise                                                                          */
/* Parameter1:    HookID H [0,VX1000_BYPASS_HBB_LUT_ENTRIES)                                                                  */
/*                Validity checked by the function                                                                            */
/* Parameter2:    DefaultValue D to be returned if hook is not valid                                                          */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Check whether valid data corresponding to the given Hook ID is present in the buffer                        */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT16 VX1000_SUFFUN(vx1000_BypassHbbGetVal_16)(VX1000_UINT32 HookID, VX1000_UINT16 DefaultValue)
{
  volatile VX1000_UINT16 newValue = DefaultValue; /* take a volatile copy because some compilers do not realise that we modify it via pointers and thus return the original value */
  if ((VX1000_SUFFUN(vx1000_BypassHbbHookValid)(HookID)) != 0)
  {
    VX1000_OLDA_EVENT_T *pEventList = VX1000_ADDR_TO_PTR2OE(gVX1000.Olda.EventList); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    if (((pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress) & (sizeof(DefaultValue) - 1UL)) != 0UL) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    {
      volatile VX1000_UINT8 *dst = VX1000_ADDR_TO_PTR2VU8(VX1000_PTR2VU16_TO_ADDRESS(&newValue)), *src = VX1000_ADDR_TO_PTR2VU8(pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      VX1000_UINT8 i;
      for (i = 0; i < sizeof(DefaultValue); ++i)
      {
        dst[i] = src[i];
      }
    }
    else
    {
      newValue = VX1000_ADDR_TO_PTR2VU16(pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress)[0];/* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
  }
  return newValue;
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_BypassHbbGetVal_32                                                                                   */
/* API name:      VX1000_BYPASS_HBB_GETVAL_32 (expression)                                                                    */
/* Wrapper API:   VX1000If_BypassHbbGetval32  (expression)                                                                    */
/* Return value:  VX1000_UINT32 corresponding to the stimulated value if hook is valid and data available                     */
/*                DefaultValue is returned otherwise                                                                          */
/* Parameter1:    HookID H [0,VX1000_BYPASS_HBB_LUT_ENTRIES)                                                                  */
/*                Validity checked by the function                                                                            */
/* Parameter2:    DefaultValue D to be returned if hook is not valid                                                          */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Check whether valid data corresponding to the given Hook ID is present in the buffer                        */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT32 VX1000_SUFFUN(vx1000_BypassHbbGetVal_32)(VX1000_UINT32 HookID, VX1000_UINT32 DefaultValue)
{
  volatile VX1000_UINT32 newValue = DefaultValue; /* take a volatile copy because some compilers do not realise that we modify it via pointers and thus return the original value */
  if ((VX1000_SUFFUN(vx1000_BypassHbbHookValid)(HookID)) != 0)
  {
    VX1000_OLDA_EVENT_T *pEventList = VX1000_ADDR_TO_PTR2OE(gVX1000.Olda.EventList); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    if (((pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress) & (sizeof(DefaultValue) - 1UL)) != 0UL) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    {
      volatile VX1000_UINT8 *dst = VX1000_ADDR_TO_PTR2VU8(VX1000_PTR2VU32_TO_ADDRESS(&newValue)), *src = VX1000_ADDR_TO_PTR2VU8(pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      VX1000_UINT8 i;
      for (i = 0; i < sizeof(DefaultValue); ++i)
      {
        dst[i] = src[i];
      }
    }
    else
    {
      newValue = VX1000_ADDR_TO_PTR2VU32(pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress)[0];/* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
  }
  return newValue;
}

#if defined(VX1000_UINT64)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_BypassHbbGetVal_64                                                                                   */
/* API name:      VX1000_BYPASS_HBB_GETVAL_64 (expression)                                                                    */
/* Wrapper API:   VX1000If_BypassHbbGetval64  (expression)                                                                    */
/* Return value:  VX1000_UINT64 corresponding to the stimulated value if hook is valid and data available                     */
/*                DefaultValue is returned otherwise                                                                          */
/* Parameter1:    HookID H [0,VX1000_BYPASS_HBB_LUT_ENTRIES)                                                                  */
/*                Validity checked by the function                                                                            */
/* Parameter2:    DefaultValue D to be returned if hook is not valid                                                          */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Check whether valid data corresponding to the given Hook ID is present in the buffer                        */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT64 VX1000_SUFFUN(vx1000_BypassHbbGetVal_64)(VX1000_UINT32 HookID, VX1000_UINT64 DefaultValue)
{
  volatile VX1000_UINT64 newValue = DefaultValue; /* take a volatile copy because some compilers do not realise that we modify it via pointers and thus return the original value */
  if ((VX1000_SUFFUN(vx1000_BypassHbbHookValid)(HookID)) != 0)
  {
    VX1000_OLDA_EVENT_T *pEventList = VX1000_ADDR_TO_PTR2OE(gVX1000.Olda.EventList); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    if (((pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress) & (sizeof(DefaultValue) - 1UL)) != 0UL) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    {
      volatile VX1000_UINT8 *dst = VX1000_ADDR_TO_PTR2VU8(VX1000_PTR2VU64_TO_ADDRESS(&newValue)), *src = VX1000_ADDR_TO_PTR2VU8(pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      VX1000_UINT8 i;
      for (i = 0; i < sizeof(DefaultValue); ++i)
      {
        dst[i] = src[i];
      }
    }
    else
    {
      newValue = VX1000_ADDR_TO_PTR2VU64(pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress)[0];/* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
  }
  return newValue;
}
#endif /* VX1000_UINT64 */

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_BypassHbbGetVal_Float                                                                                */
/* API name:      VX1000_BYPASS_HBB_GETVAL_FLOAT (expression)                                                                 */
/* Wrapper API:   VX1000If_BypassHbbGetvalFloat  (expression)                                                                 */
/* Return value:  VX1000_FLOAT corresponding to the stimulated value if hook is valid and data available                      */
/*                DefaultValue is returned otherwise                                                                          */
/* Parameter1:    HookID H [0,VX1000_BYPASS_HBB_LUT_ENTRIES)                                                                  */
/*                Validity checked by the function                                                                            */
/* Parameter2:    DefaultValue D to be returned if hook is not valid                                                          */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Check whether valid data corresponding to the given Hook ID is present in the buffer                        */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_FLOAT VX1000_SUFFUN(vx1000_BypassHbbGetVal_Float)(VX1000_UINT32 HookID, VX1000_FLOAT DefaultValue)
{
  volatile VX1000_FLOAT newValue = DefaultValue; /* take a volatile copy because some compilers do not realise that we modify it via pointers and thus return the original value */
  if ((VX1000_SUFFUN(vx1000_BypassHbbHookValid)(HookID)) != 0)
  {
    VX1000_OLDA_EVENT_T *pEventList = VX1000_ADDR_TO_PTR2OE(gVX1000.Olda.EventList); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    if (((pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress) & (sizeof(DefaultValue) - 1UL)) != 0UL) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    {
      volatile VX1000_UINT8 *dst = VX1000_ADDR_TO_PTR2VU8(VX1000_PTR2VF_TO_ADDRESS(&newValue)), *src = VX1000_ADDR_TO_PTR2VU8(pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      VX1000_UINT8 i;
      for (i = 0; i < sizeof(DefaultValue); ++i)
      {
        dst[i] = src[i];
      }
    }
    else
    {
      newValue = VX1000_ADDR_TO_PTR2VF(pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress)[0];/* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
  }
  return newValue;
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_BypassHbbGetVal_Double                                                                               */
/* API name:      VX1000_BYPASS_HBB_GETVAL_DOUBLE (expression)                                                                */
/* Wrapper API:   VX1000If_BypassHbbGetvalDouble  (expression)                                                                */
/* Return value:  VX1000_DOUBLE corresponding to the stimulated value if hook is valid and data available                     */
/*                DefaultValue is returned otherwise                                                                          */
/* Parameter1:    HookID H [0,VX1000_BYPASS_HBB_LUT_ENTRIES)                                                                  */
/*                Validity checked by the function                                                                            */
/* Parameter2:    DefaultValue D to be returned if hook is not valid                                                          */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Check whether valid data corresponding to the given Hook ID is present in the buffer                        */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_DOUBLE VX1000_SUFFUN(vx1000_BypassHbbGetVal_Double)(VX1000_UINT32 HookID, VX1000_DOUBLE DefaultValue)
{
  volatile VX1000_DOUBLE newValue = DefaultValue; /* take a volatile copy because some compilers do not realise that we modify it via pointers and thus return the original value */
  if ((VX1000_SUFFUN(vx1000_BypassHbbHookValid)(HookID)) != 0)
  {
    VX1000_OLDA_EVENT_T *pEventList = VX1000_ADDR_TO_PTR2OE(gVX1000.Olda.EventList); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    if (((pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress) & (sizeof(DefaultValue) - 1UL)) != 0UL) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    {
      volatile VX1000_UINT8 *dst = VX1000_ADDR_TO_PTR2VU8(VX1000_PTR2VD_TO_ADDRESS(&newValue)), *src = VX1000_ADDR_TO_PTR2VU8(pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      VX1000_UINT8 i;
      for (i = 0; i < sizeof(DefaultValue); ++i)
      {
        dst[i] = src[i];
      }
    }
    else
    {
      newValue = VX1000_ADDR_TO_PTR2VD(pEventList[(VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].StimEvent].TransferDest + (VX1000_ADDR_TO_PTR2VTE(gVX1000.Stim.hbbLUTVXPointer))[HookID].OldaAddress)[0];/* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
  }
  return newValue;
}
#endif /* VX1000_HOOK_BASED_BYPASSING */


#if defined(VX1000_STIM_BY_OLDA)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_Stimulate                                                                                            */
/* API name:      VX1000_STIMULATE (expression) / VX1000_STIMULATE_VOID (statement)                                           */
/* Wrapper API:   VX1000If_Stimulate (expression) / VX1000If_StimulateVoid (statement)                                        */
/* Return value:  status: 0 (bypassed code shall be activated because bypassing is not active)                                */
/*                1 (everything done, bypassed code shall be disabled)                                                        */
/*                2 (bypassing failed; it's up to the application design whether executing the bypassed code makes sense here)*/
/* Parameter1:    stim_trigger_event E [0,gVX1000.Olda.EventCount)                                                            */
/*                Validity has to be ensured by caller!                                                                       */
/* Parameter2:    stim_event E [VX1000_STIM_EVENT_OFFSET,gVX1000.Olda.EventCount)                                             */
/*                Validity has to be ensured by caller!                                                                       */
/* Parameter3:    cycle_delay specifies the number of cycles between the triggering and the associated stimulation.           */
/*                During first cycle_delay cycles there's no stimulation in the ECU; instead VX device fills its STIM FIFO.   */
/* Parameter4:    timeout in microseconds, starting right now.                                                                */
/*                Hint: the timeout may be chosen as small as zero when cycle_delay > 0                                       */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Initiate a bypass by requesting a stimulation (stim_trigger_event) without sending DAQ data,                */
/*                and after the first cycle_delay calls, also busy wait with timeout for the tool to actually complete        */
/*                the stimulation (stim_event). In the meantime, the requested data stay in the VX internal pipeline          */
/*                and are used later. This pipeline depth / initial delay has to be considered when generating the stim data. */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_Stimulate)( VX1000_UINT8 stim_trigger_event, VX1000_UINT8 stim_event,
                                              VX1000_UINT8 cycle_delay, VX1000_UINT32 timeout_us)
{
  VX1000_UINT8 retVal = 0; /* means "Stim not active" */

  if (0==cycle_delay)
  {
    /* If cycle_delay is 0, stimulation has normal bypass behaviour */
    retVal = VX1000_SUFFUN(vx1000_Bypass)(stim_trigger_event, stim_event, timeout_us);
  }
  else
  {
    if (VX1000_STIM_ACTIVE(stim_event) != 0)
    {
      if (0==((VX1000_STIMEVENT_ARRAYNAME)[(stim_event) - (VX1000_STIM_EVENT_OFFSET)].Copying)) /* (Pipelined STIM feature reuses the events copying flag as cycle delay counter) */
      {
        /* Delay period has expired */
        /* Wait for stimulation data available in the OLDA buffer and stimulate */
#if !defined(VX1000_STIM_FORCE_V1)
        if ((VX1000_STIM_WAIT(stim_event, timeout_us)) != 0)
        {
          /* We cannot distinguish the cases "EOF_stimfile reached, no more data" and    */
          /* "timeout requirement just too hard, stim data still waiting in the queue".  */
          /* In either case we do NOT try to pre-fetch further data and we always signal */
          /* "Stim (still) active, Timeout" to the caller: */
          retVal = 2;
        }
        else
        {
          VX1000_STIM_REQUEST(stim_event)     /* Request stimulation data */
          VX1000_EVENT(stim_trigger_event)    /* Trigger a stimulation */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* macro expansion contains VX1000_ADDR_TO_PTR2XXX: cannot avoid violating MISRA rule 11.3 because in a few configurations a peripheral register at a fix HW address must be accessed via pointer to update the HW value */ /* PRQA S 0501 */ /* Violation of MISRA rule 12.8 because 64-bit data types are known to be not MISRA-conformant. This is just an example to be overruled by a user-defined implementation of an atomic 64-bit-store operation. */
          retVal = 1;                         /* Stim active, OK */
        }
      }
      else
      {
        (VX1000_STIMEVENT_ARRAYNAME)[(stim_event) - (VX1000_STIM_EVENT_OFFSET)].Copying--; /* (Pipelined STIM feature reuses the events copying flag as cycle delay counter) */
        VX1000_EVENT(stim_trigger_event)      /* Trigger a stimulation */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* macro expansion contains VX1000_ADDR_TO_PTR2XXX: cannot avoid violating MISRA rule 11.3 because in a few configurations a peripheral register at a fix HW address must be accessed via pointer to update the HW value */ /* PRQA S 0501 */ /* Violation of MISRA rule 12.8 because 64-bit data types are known to be not MISRA-conformant. This is just an example to be overruled by a user-defined implementation of an atomic 64-bit-store operation. */
        if (0==((VX1000_STIMEVENT_ARRAYNAME)[(stim_event) - (VX1000_STIM_EVENT_OFFSET)].Copying)) /* (Pipelined STIM feature reuses the events copying flag as cycle delay counter) */
        {
          VX1000_STIM_REQUEST(stim_event)     /* Request stimulation data */
#else /* VX1000_STIM_FORCE_V1 */
        if ((VX1000_STIM_WAIT(stim_event, timeout_us)) != 0)
        {
          /* We cannot distinguish the cases "EOF_stimfile reached, no more data" and    */
          /* "timeout requirement just too hard, stim data still waiting in the queue".  */
          /* In either case we do NOT try to pre-fetch further data and we always signal */
          /* "Stim (still) active, Timeout" to the caller: */
          retVal = 2;
        }
        else
        {
          VX1000_BYPASS_TRIGGER_VOID(stim_trigger_event, stim_event)   /* Trigger a stimulation */
          VX1000_STIM_REQUEST_EVENT(stim_event)                        /* Request stimulation data */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          retVal = 1;                                                  /* Stim active, OK */
        }
      }
      else
      {
        (VX1000_STIMEVENT_ARRAYNAME)[(stim_event) - (VX1000_STIM_EVENT_OFFSET)].Copying--;        /* (Pipelined STIM feature reuses the events copying flag as cycle delay counter) */
        VX1000_BYPASS_TRIGGER_VOID(stim_trigger_event, stim_event)                                /* Trigger a stimulation */
        if (0==((VX1000_STIMEVENT_ARRAYNAME)[(stim_event) - (VX1000_STIM_EVENT_OFFSET)].Copying)) /* (Pipelined STIM feature reuses the events copying flag as cycle delay counter) */
        {
          VX1000_STIM_REQUEST_EVENT(stim_event)                                                   /* Request stimulation data */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_STIM_FORCE_V1 */
        } /* common closing brace for STIMv0 and STIMv1 */
        /* Stim not active, start-up delay still in progress */
      }
    } /* if Stim active */
    else
    {
      if (cycle_delay > 100) { cycle_delay = 100; } /* Silently ensures to not exceed the VX's maximum queue size */
      (VX1000_STIMEVENT_ARRAYNAME)[(stim_event) - (VX1000_STIM_EVENT_OFFSET)].Copying = cycle_delay; /* (Pipelined STIM feature reuses the events copying flag as cycle delay counter) */
    } /* if Stim not active */
  } /* if cycle_delay > 0 */

  return retVal;
}
#endif /* VX1000_STIM_BY_OLDA */
#endif /* VX1000_STIM */


#if (defined(VX1000_OVERLAY) && (!defined(VX1000_COMPILED_FOR_SLAVECORES)))
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlayInit                                                                                          */
/* API name:      None                                                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Initialises the global variables that are used by the overlay functions.                                    */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static void VX1000_SUFFUN(vx1000_OverlayInit)(void)
{
  gVX1000.Ovl.presenceCounter = 0;
  gVX1000.Ovl.ovlConfigValue  = 0;
  gVX1000.Ovl.ovlConfigMask   = 0;
  gVX1000.Ovl.calFeaturesEnable = 0
#if defined(VX1000_OVLENBL_KEEP_AWAKE)
                                | (VX1000_OVLFEAT_KEEP_AWAKE)
#endif /* VX1000_OVLENBL_KEEP_AWAKE */
#if defined(VX1000_OVLENBL_SYNC_PAGESWITCH)
                                | (VX1000_OVLFEAT_SYNC_PAGESWITCH)
#endif /* VX1000_OVLENBL_SYNC_PAGESWITCH */
#if defined(VX1000_OVLENBL_PERSISTENT_EMEM)
                                | (VX1000_OVLFEAT_PERSISTENT_EMEM)
#endif /* VX1000_OVLENBL_PERSISTENT_EMEM */
#if defined(VX1000_OVLENBL_RST_ON_CALWAKEUP)
                                | (VX1000_OVLFEAT_RST_ON_CALWAKEUP)
#endif /* VX1000_OVLENBL_RST_ON_CALWAKEUP */
#if defined(VX1000_OVLENBL_USE_VX_EPK_TRANS)
                                | (VX1000_OVLFEAT_USE_VX_EPK_TRANS)
#endif /* VX1000_OVLENBL_USE_VX_EPK_TRANS */
#if defined(VX1000_OVLENBL_VALIDATE_PAGESW)
                                | (VX1000_OVLFEAT_VALIDATE_PAGESW)
#endif /* VX1000_OVLENBL_VALIDATE_PAGESW */
#if defined(VX1000_OVLENBL_CORE_SYNC_PAGESW)
                                | (VX1000_OVLFEAT_CORE_SYNC_PAGESW)
#endif /* VX1000_OVLENBL_CORE_SYNC_PAGESW */
#if defined(VX1000_OVERLAY_VX_CONFIGURABLE)
                                | (VX1000_OVLFEAT_VX)
#elif defined(VX1000_OVERLAY_USERMANAGED)
                                | (VX1000_OVLFEAT_USER)
#else /* any of the built-in methods of the driver*/
                                | (VX1000_OVLFEAT_DRIVER)
#endif /* !VX1000_OVERLAY_VX_CONFIGURABLE && !VX1000_OVERLAY_USERMANAGED */
#if (defined(VX1000_OVERLAY_VX_CONFIGURABLE)) && (defined(VX1000_OVLENBL_REGWRITE_VIA_MX)) && (defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_OVERLAY_CONTROL))
                                | (VX1000_OVLFEAT_ECU_REGS_VIA_MX)
#endif /* VX1000_OVERLAY_VX_CONFIGURABLE & VX1000_OVLENBL_REGWRITE_VIA_MX & VX1000_MAILBOX & VX1000_MAILBOX_OVERLAY_CONTROL */
#if defined(VX1000_OVLENBL_RST_ON_SNCPAGESW)
                                | (VX1000_OVLFEAT_RST_ON_SNCPAGESW)
#endif /* VX1000_OVLENBL_RST_ON_SNCPAGESW */
                                ;

  gVX1000.Ovl.persistentECUEmemHeaderPtr = VX1000_EMEM_HDR_PTR;
#if defined(VX1000_OVLENBL_CORE_SYNC_PAGESW) || defined(VX1000_OVLENBL_SYNC_PAGESWITCH)
  gVX1000.Ovl.ovlBusMasterMask = VX1000_OVL_CAL_BUS_MASTER;
#endif /* VX1000_OVLENBL_CORE_SYNC_PAGESW */
  gVX1000.Ovl.ecuLastPresenceCounter = 0;
#if defined(VX1000_OVLENBL_USE_VX_EPK_TRANS)
  gVX1000.Ovl.ovlEPKLength = (VX1000_UINT16)(VX1000_OVL_EPK_LENGTH);
  gVX1000.Ovl.ovlReferencePageDataEPKAddress = ((VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32) (VX1000_OVL_EPK_REFPAGE_ADDR));
  gVX1000.Ovl.ovlWorkingPageDataEPKAddress = 0xFFFFFFFFUL;
#else /* !VX1000_OVLENBL_USE_VX_EPK_TRANS */
  gVX1000.Ovl.ovlEPKLength = 0U;
#if (defined(VX1000_OVERLAY_ADDR) && defined(VX1000_OVERLAY_SIZE)) && defined(VX1000_CALRAM_ADDR)
  for (gVX1000.Ovl.ovlReferencePageDataEPKAddress = VX1000_OVERLAY_SIZE; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
       gVX1000.Ovl.ovlReferencePageDataEPKAddress > 0x0800UL;
       gVX1000.Ovl.ovlReferencePageDataEPKAddress >>= 1)
  {
    gVX1000.Ovl.ovlEPKLength += 0x1000U;
  }
  gVX1000.Ovl.ovlEPKLength |= (VX1000_UINT16)gVX1000.Ovl.ovlReferencePageDataEPKAddress;
  gVX1000.Ovl.ovlReferencePageDataEPKAddress = ((VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32) (VX1000_OVERLAY_ADDR)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  gVX1000.Ovl.ovlWorkingPageDataEPKAddress = ((VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32) VX1000_CALRAM_PHYSADDR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else /* !VX1000_OVERLAY_ADDR || !VX1000_OVERLAY_SIZE || !VX1000_CALRAM_ADDR */
  gVX1000.Ovl.ovlReferencePageDataEPKAddress = 0UL;
  gVX1000.Ovl.ovlWorkingPageDataEPKAddress = 0UL;
#endif /* !VX1000_OVERLAY_ADDR || !VX1000_OVERLAY_SIZE || !VX1000_CALRAM_ADDR */
#endif /* !VX1000_OVLENBL_USE_VX_EPK_TRANS */
  gVX1000.Ovl.syncCalSwitchDataPtr = (VX1000_PTR2SPS_TO_ADDRESS(&gVX1000.Ovl.syncCalData));  /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  gVX1000.Ovl.version = VX1000_OVL_VERSION;

  if (gVX1000.Ovl.persistentECUEmemHeaderPtr != 0)
  {
    vx1000_EmemHdrInit();
  }

#if defined(VX1000_MAILBOX_OVERLAY_CONTROL)
  gVX1000_XCP_CalPage = VX1000_CALPAGE_FLASH;
  gVX1000_ECU_CalPage = VX1000_CALPAGE_FLASH;
#endif /* VX1000_MAILBOX_OVERLAY_CONTROL */

#if (defined(VX1000_OVERLAY)) && (defined(VX1000_OVERLAY_VX_CONFIGURABLE)) && (defined(VX1000_OVLENBL_REGWRITE_VIA_MX)) && (defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_OVERLAY_CONTROL))
  gVX1000.Ovl.ovlConfigRegsPtr = (VX1000_PTR2OCR_TO_ADDRESS(&gVX1000.Ovl.ovlConfigRegs)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  if (VX1000_SUFFUN(vx1000_OverlayReadEcuDescr)() != 0)
  {
    gVX1000.Ovl.ovlConfigRegsPtr = 0;  /* the feature seems not to be supported, yet. The error is already logged by the callee */
  }
#else /* !VX1000_OVERLAY || !VX1000_OVERLAY_VX_CONFIGURABLE || !VX1000_OVLENBL_REGWRITE_VIA_MX || !VX1000_MAILBOX || !VX1000_MAILBOX_OVERLAY_CONTROL */
  gVX1000.Ovl.ovlConfigRegsPtr = 0;    /* the feature is disabled */
#endif /* !VX1000_OVERLAY || !VX1000_OVERLAY_VX_CONFIGURABLE || !VX1000_OVLENBL_REGWRITE_VIA_MX || !VX1000_MAILBOX || !VX1000_MAILBOX_OVERLAY_CONTROL */

  gVX1000.ToolDetectState &= ~(VX1000_TDS_WORKING_PAGE);
  if ( (VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.Ovl.ovlConfigValue)) != (0x8UL + VX1000_PTR2VOVL_TO_ADDRESS(&gVX1000.Ovl)) ) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  {
    gVX1000.ToolDetectState |= VX1000_TDS_ERROR;
    VX1000_ERRLOGGER(VX1000_ERRLOG_STRUCTS_PADDED)
  }
  gVX1000.Ovl.syncCalData.pageSwitchRequested = 0;
  gVX1000.Ovl.magicId = VX1000_OVL_MAGIC;
}


/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_EmemHdrInit                                                                                          */
/* API name:      None                                                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Reset the EmemHdr related VX1000 structure data                                                             */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static void VX1000_SUFFUN(vx1000_EmemHdrInit)(void)
{
  VX1000_EMEM_HDR_T *hdr = VX1000_ADDR_TO_PTR2EH(gVX1000.Ovl.persistentECUEmemHeaderPtr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  volatile VX1000_UINT32 * const addr =  (VX1000_ADDR_TO_PTR2U32(gVX1000.Ovl.persistentECUEmemHeaderPtr)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  /* dummy access the memory of the magic ID, the version and reserved fields by 32bit-writes to initialise the ECC hardware: */
  addr[0] = addr[1] = 0UL;

  /* now the hardware will accept smaller-then-32bit accesses to the same memory. Do the actual initialisation */
  hdr->version = VX1000_EMEM_HDR_VERSION;
  hdr->reserved = 0; /* Initialise the reserved field for CRC-reasons */
  hdr->magicId = VX1000_EMEM_HDR_MAGIC;
}

#endif /* VX1000_OVERLAY & !VX1000_COMPILED_FOR_SLAVECORES */


#if defined(VX1000_RES_MGMT) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_ResMgmtInit                                                                                          */
/* API name:      None                                                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Reset the Resource Management related VX1000 structure data                                                 */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static void VX1000_SUFFUN(vx1000_ResMgmtInit)(void)
{
  gVX1000.ResMgmt.resMgmtEnable = 0UL; /* PRQA S 3198 */ /* this global volatile variable is accessed by debug tool in the background */

  gVX1000.ResMgmt.version = VX1000_RES_MGMT_VERSION;
  gVX1000.ResMgmt.ovlConfigItemStart  = (VX1000_UINT8)(VX1000_RES_MGMT_CFG_ITEM_START);
  gVX1000.ResMgmt.ovlConfigItemLength = (VX1000_UINT8)(VX1000_RES_MGMT_CFG_ITEM_LEN);

  gVX1000.ResMgmt.ovlRamStart   = (VX1000_UINT32)(VX1000_RES_MGMT_RAM_START);
  gVX1000.ResMgmt.ovlRamSize    = (VX1000_UINT32)(VX1000_RES_MGMT_RAM_SIZE);

  gVX1000.ResMgmt.resMgmtEnable = (VX1000_RES_MGMT_ENBLVAL_CFG_ITEM)
                                | (VX1000_RES_MGMT_ENBLVAL_OVL_RAM);
  if ( (VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.ResMgmt.resMgmtEnable)) != (0x8UL + (VX1000_RES_MGMT_PTR)) ) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  {
    gVX1000.ToolDetectState |= VX1000_TDS_ERROR;
    VX1000_ERRLOGGER(VX1000_ERRLOG_STRUCTS_PADDED)
  }
  gVX1000.ResMgmt.magicId = VX1000_RES_MGMT_MAGIC;
}


/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_DisableAccess                                                                                        */
/* API name:      VX1000_DISABLE_ACCESS (expression) / VX1000_DISABLE_ACCESS_VOID (statement)                                 */
/* Wrapper API:   VX1000If_DisableAccess (expression) / VX1000If_DisableAccessVoid (statement)                                */
/* Return value:  0 Disabled access successfully                                                                              */
/*                1 VX1000 already detected. VX1000 access not disabled                                                       */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Disables the access of the VX1000 to the ECU resources. The VX1000 will prevent the MC from accessing       */
/*                the microcontroller when the VX1000 access is disabled                                                      */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_DisableAccess)( void )
{
  VX1000_UINT8 retVal = 0;
  if (VX1000_DETECTED())
  {
    retVal = 1;
  }
  else
  {
    VX1000_INVALIDATE_EMEM()
    gVX1000.ToolDetectState |= VX1000_TDS_VX_ACCESS_DISABLED;
  }
  return retVal;
}

#endif /* VX1000_RES_MGMT & !VX1000_COMPILED_FOR_SLAVECORES */


/*------------------------------------------------------------------------------- */
/* Calibration (_classic_ version with fixed settings from VX1000_cfg.h)          */

#if defined(VX1000_OVERLAY) && (!defined(VX1000_OVERLAY_VX_CONFIGURABLE))

#if defined(VX1000_INIT_CAL_PAGE_INTERNAL)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlayHwInit                                                                                        */
/* API name:      None (note: may be overloaded by user via VX1000_INIT_CAL_PAGE)                                             */
/* Return value:  None                                                                                                        */
/* Parameter1:    bit0: allow update of the status variables;                                                                 */
/*                bit1: allow RAM initialisation;                                                                             */
/*                bit2: allow reconfiguration of the remap registers                                                          */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:                                                                                                               */
/* Devel state:   Idea                                                                                                        */
/*----------------------------------------------------------------------------------------------------------------------------*/
static void VX1000_SUFFUN(vx1000_OverlayHwInit)(void)
{
  const VX1000_UINT32 overlaySize = (VX1000_UINT32)(VX1000_OVERLAY_SIZE);      /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  const VX1000_UINT32 overlayAddr = (VX1000_UINT32)(VX1000_OVERLAY_ADDR);      /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  const VX1000_UINT32 overlayPhys = (VX1000_UINT32)(VX1000_OVERLAY_PHYSADDR);  /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  const VX1000_UINT32 calRamAddr  = (VX1000_UINT32)(VX1000_CALRAM_ADDR);       /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  /* ensure that the mapping is not already/still active (even if not needed on all derivatives) */

#if defined(VX1000_DETECTION) || defined(VX1000_TARGET_TRICORE)
  if ((!VX1000_DETECTED()) || ((gVX1000.ToolCtrlState & (VX1000_UINT32)(VX1000_TCS_SKIP_WP_INIT)) == 0)) /* skip internal overlay initialisation if a VX is connected and external overlay control has been requested */
#endif /* VX1000_DETECTION || VX1000_TARGET_TRICORE */
  {
    /* Initialise CALRAM: copy original flash content to RAM, then map RAM over flash (with cache inhibit etc.) */
#if (defined(VX1000_OVERLAY_TLB) || defined(VX1000_OVERLAY_DESCR_IDX)) || (defined(VX1000_MPC56xCRAM_BASE_ADDR) || defined(VX1000_SH2_FCU_BASE_ADDR))
#if defined(VX1000_RUNNING_ON_MAINCORE) && (!defined(VX1000_OVLENBL_HW_INIT_PER_CORE))
    if (VX1000_RUNNING_ON_MAINCORE() != 0)
#endif /* VX1000_RUNNING_ON_MAINCORE && !VX1000_OVLENBL_HW_INIT_PER_CORE */
    {
#if defined(VX1000_OVLENBL_HW_INIT_PER_CORE) || (!defined(VX1000_COMPILED_FOR_SLAVECORES))
      (void)VX1000_SUFFUN(vx1000_MapCalRam)(overlaySize, overlayAddr, overlayAddr, overlayPhys); /* ensure that the mapping is not already/still active (even if not needed on all derivatives) */
#endif /* VX1000_OVLENBL_HW_INIT_PER_CORE || !VX1000_COMPILED_FOR_SLAVECORES */
    }
#endif /* VX1000_OVERLAY_TLB | VX1000_OVERLAY_DESCR_IDX | VX1000_MPC56xCRAM_BASE_ADDR | VX1000_SH2_FCU_BASE_ADDR */
#if defined(VX1000_RUNNING_ON_MAINCORE) && (!defined(VX1000_OVLENBL_MEMCPY_PER_CORE)) /* hint: considering VX1000_OVLENBL_MEMCPY_PER_CORE actually not needed up to now (self-addresses can't be measured anyway) */
    if (VX1000_RUNNING_ON_MAINCORE() != 0)
#endif /* VX1000_RUNNING_ON_MAINCORE && !VX1000_OVLENBL_MEMCPY_PER_CORE */
    {
#if defined(VX1000_OVLENBL_MEMCPY_PER_CORE) || (!defined(VX1000_COMPILED_FOR_SLAVECORES))
#if defined(VX1000_TARGET_XC2000)
      VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)
#else /* !VX1000_TARGET_XC2000 */
      if ((((overlaySize | overlayAddr) | calRamAddr) & 0x3UL) != 0UL)
      {
        VX1000_UINT32 cnt;
        volatile const VX1000_CHAR *pSrc = VX1000_ADDR_TO_PTR2VCC(overlayAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        volatile VX1000_CHAR *pDst = VX1000_ADDR_TO_PTR2VC(calRamAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        for (cnt = 0; cnt < overlaySize; cnt++)
        {
          pDst[cnt] = pSrc[cnt];
        }
      }
      else
      {
        VX1000_UINT32 cnt;
        volatile const VX1000_UINT32 *pSrc = VX1000_ADDR_TO_PTR2VCU32(overlayAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        volatile VX1000_UINT32 *pDst = VX1000_ADDR_TO_PTR2VU32(calRamAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        for (cnt = 0; cnt < (overlaySize >> 2U); cnt++)
        {
          pDst[cnt] = pSrc[cnt];
        }
      }
#endif /* !VX1000_TARGET_XC2000 */
#endif /* VX1000_OVLENBL_MEMCPY_PER_CORE || !VX1000_COMPILED_FOR_SLAVECORES */
    }
#if (defined(VX1000_OVERLAY_TLB) || defined(VX1000_OVERLAY_DESCR_IDX)) || (defined(VX1000_MPC56xCRAM_BASE_ADDR) || defined(VX1000_SH2_FCU_BASE_ADDR))
#if defined(VX1000_RUNNING_ON_MAINCORE) && (!defined(VX1000_OVLENBL_HW_INIT_PER_CORE))
    if (VX1000_RUNNING_ON_MAINCORE() != 0)
#endif /* VX1000_RUNNING_ON_MAINCORE && !VX1000_OVLENBL_HW_INIT_PER_CORE */
    {
#if defined(VX1000_OVLENBL_HW_INIT_PER_CORE) || (!defined(VX1000_COMPILED_FOR_SLAVECORES))
      VX1000_WRP_SET_CAL_PAGE_VOID(0, VX1000_CALPAGE_RAM, ((VX1000_CAL_ECU) | (VX1000_CAL_XCP)), 1U)
#endif /* VX1000_OVLENBL_HW_INIT_PER_CORE || !VX1000_COMPILED_FOR_SLAVECORES */
    }
#endif /* VX1000_OVERLAY_TLB | VX1000_OVERLAY_DESCR_IDX | VX1000_MPC56xCRAM_BASE_ADDR | VX1000_SH2_FCU_BASE_ADDR */
  }
}
#endif /* VX1000_INIT_CAL_PAGE_INTERNAL */

#if defined(VX1000_OVERLAY_TLB)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MapCalRam - may be overloaded internally for the used hardware                                       */
/* API name:      None                                                                                                        */
/* Return value:  status:                                                                                                     */
/*                0: MMU not changed                                                                                          */
/*                1: mapping complete                                                                                         */
/* Parameter1:    Overlay window size                                                                                         */
/* Parameter2:    properly aligned virtual flash access address                                                               */
/* Parameter3:    overlay storage address (not used on MPC55xx/MPC56xx)                                                       */
/* Parameter4:    physical address of Parameter2 or Parameter3                                                                */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Derivative-specific method for enabling/disabling a single overlay window using MPC555x MMU features.       */
/*                Note: The driver is not responsible for the boot-up-initialisation of the TLB entry. The application's      */
/*                boot code must have preinitialised all TLB entries in a way that the driver only needs to re-initialise     */
/*                the TLB entry that is specified in VX1000_cfg.h.                                                            */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_MapCalRam)( VX1000_UINT32 size, VX1000_UINT32 target, VX1000_UINT32 source1, VX1000_UINT32 source2 )
{
  VX1000_UINT8 n = (VX1000_UINT8)(VX1000_OVERLAY_TLB); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  VX1000_UINT32 ea = target;
  VX1000_UINT32 ra = source2;
  volatile VX1000_UINT32 pagesize = size; /* the volatile is here to avoid compiler warnings because some compilers see that this static function is called with identical size parameter always */
  VX1000_UINT8 properties  = 0x00
#if defined(VX1000_OVERLAY_IS_LITTLE_ENDIAN)
                           | 0x01    /* interpret data in this page as little instead of big endian */
#endif /* VX1000_OVERLAY_IS_LITTLE_ENDIAN */
                           | 0x00    /* NO suppression of speculative or out-of-order processing */
                           | 0x00    /* NO force of coherence because e200 MMU does not support it */
                           | 0x08    /* cache_inhibit to enable coherent views from all clients */
                           | 0x00    /* NO cache_writethrough because it may not be set together with cache_inhibit on e200 MMUs! */
#if defined(VX1000_OVERLAY_CONTAINS_VLECODE)
                           | 0x20    /* interpret code from this page as VLE instead of BookE */
#endif /* VX1000_OVERLAY_CONTAINS_VLECODE */
                           ;
  VX1000_UINT8 permissions = 0x3F;
  VX1000_UINT32 rpn, epn;
  VX1000_UINT8 pagetype, pageshift, retVal = 0;

  switch (pagesize) /* note: supported sizes depend on the derivative (but if TLBs are supported, then at least 4K,16K,64K and 256K will work) */
  {
    case 0x001000UL: pagetype = 1U; pageshift = 12U; break; /*   4 K */
    case 0x004000UL: pagetype = 2U; pageshift = 14U; break; /*  16 K */
    case 0x010000UL: pagetype = 3U; pageshift = 16U; break; /*  64 K */
    case 0x040000UL: pagetype = 4U; pageshift = 18U; break; /* 256 K */
    case 0x100000UL: pagetype = 5U; pageshift = 20U; break; /*   1 M */
    default: pagetype = 0U; pageshift = 0U; break;        /* invalid */
  }
  if (0==pagetype)
  {
    VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_INVALID_SIZE)
    VX1000_DUMMYREAD(source1)                             /* just to avoid compiler "unused" warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  }
  else if (((ra | ea) & (pagesize - 1)) != 0)
  {
    VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_MISALIGNED)
  }
  else
  {
    rpn = ra / pagesize;
    epn = ea / pagesize;
    VX1000_MPC_MMU_MTMAS0(0x10000000UL | (VX1000_UINT32)(((VX1000_UINT32)n) << 16U)) /* Select TLB entry */
    VX1000_MPC_MMU_MTMAS1(0x80000000UL | (VX1000_UINT32)(((VX1000_UINT32)pagetype) << 8U))  /* VALID=1, IPROT=0, TID=0, TS=0, TSIZE=pagetype */
    VX1000_MPC_MMU_MTMAS2((epn << pageshift) | (VX1000_UINT32)(0x3FU & properties))
    VX1000_MPC_MMU_MTMAS3((rpn << pageshift) | (VX1000_UINT32)(0x3FU & permissions)) /* RPN = , U0:3=0, UX/SX=0, UR/SR=1, UW/SW=1 */
    VX1000_MPC_MMU_TLBWE()  /* Make pre-charged entry from MAS0..MAS3 to valid as an MMU TLB */
    retVal = 1;
  }
  return retVal;
}

#elif defined(VX1000_OVERLAY_DESCR_IDX)

#if defined(VX1000_TARGET_POWERPC)

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MapCalRam - may be overloaded internally for the used hardware                                       */
/* API name:      None                                                                                                        */
/* Return value:  status:                                                                                                     */
/*                0: requested overlay not possible                                                                           */
/*                1: mapping complete                                                                                         */
/* Parameter1:    Overlay window size                                                                                         */
/* Parameter2:    properly aligned virtual flash access address                                                               */
/* Parameter3:    overlay memory address (hard-wired on this hardware)                                                        */
/*                If Parameter2 == Parameter3, the overlay is turned off completely.                                          */
/* Parameter4:    physical address of Parameter3 (currently not used on MPC57xxX)                                             */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Derivative-specific method for enabling/disabling a single overlay window using MPC57xxM PFLASH features.   */
/*                Note: The driver assumes exclusive ownership of the overlay unit.                                           */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_MapCalRam)(VX1000_UINT32 size, VX1000_UINT32 target, VX1000_UINT32 source1, VX1000_UINT32 source2)
{
  VX1000_UINT8 retVal = 1U; /* return "mapping complete" by default */

  if ((size > (8UL * (1024UL * 1024UL))) || ((size & (size - 1)) != 0))
  {
    VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_INVALID_SIZE)
    retVal = 0; /* unsupported size specified */
    VX1000_DUMMYREAD(source2) /* just to avoid compiler "unused" warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  }
  else if (((target | source1) & (size - 1UL)) != 0UL)
  {
    VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_MISALIGNED)
    retVal = 0; /* misaligned address specified */
  }
  else if (target==source1) /* turn mapping off */
  {
#if ((VX1000_OVERLAY_DESCR_IDX) < 16)
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0108UL + (((VX1000_OVERLAY_DESCR_IDX) + 16) << 4)))[0] = /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0108UL + (( VX1000_OVERLAY_DESCR_IDX      ) << 4)))[0] = 0x00000000UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else /* VX1000_OVERLAY_DESCR_IDX >= 16 */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0108UL + (((VX1000_OVERLAY_DESCR_IDX) & 31) << 4)))[0] = 0x00000000UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_OVERLAY_DESCR_IDX >= 16 */
  }
  else /* install the mapping */
  {
    VX1000_UINT32 ld_size;
    for (ld_size = 0UL; (ld_size == ld_size) && ((1UL << ld_size) < size); ld_size++) { /* intentionally empty */ }
#if !defined (VX1000_OVLENBL_CORE_SYNC_PAGESW)
    ld_size |= 0xFFFF0000UL; /* assign the overlay to any available bus masters */
#endif /* !VX1000_OVLENBL_CORE_SYNC_PAGESW */

    if (0==(source1 & 0x01000000UL)) /* attention: this is MPC5746M/MPC5777M specific! */
    {
      (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0008UL))[0] |= 1 << 20; /* PFCR3.BDRM -> enable read accesses of buddy RAM because it's not an internal address */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x000CUL))[0] = 0xFFFFffffUL; /* PFAPR -> enable flash RW-accesses for all masters */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#if ((VX1000_OVERLAY_DESCR_IDX) < 16)
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0010UL))[0] = 0x00000111UL; /* PFCRCR -> enable overlay feature (secure mode) */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0100UL + ((((VX1000_OVERLAY_DESCR_IDX) + 16)) << 4)))[0] = /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0100UL + ((( VX1000_OVERLAY_DESCR_IDX      )) << 4)))[0] = target; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0104UL + ((((VX1000_OVERLAY_DESCR_IDX) + 16)) << 4)))[0] = /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0104UL + ((( VX1000_OVERLAY_DESCR_IDX      )) << 4)))[0] = source1; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0108UL + ((((VX1000_OVERLAY_DESCR_IDX) + 16)) << 4)))[0] = /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0108UL + ((( VX1000_OVERLAY_DESCR_IDX      )) << 4)))[0] = ld_size; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0014UL))[0] |= (1UL << (31 - ((VX1000_OVERLAY_DESCR_IDX) + 16))) | (1UL << (31 - (VX1000_OVERLAY_DESCR_IDX))); /* PFCRDE -> set descr valid */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else /* VX1000_OVERLAY_DESCR_IDX >= 16 */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0010UL))[0] = 0x00000011UL; /* PFCRCR -> enable overlay feature (big mode) */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0100UL + (((VX1000_OVERLAY_DESCR_IDX) & 31) << 4)))[0] = target; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0104UL + (((VX1000_OVERLAY_DESCR_IDX) & 31) << 4)))[0] = source1; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0108UL + (((VX1000_OVERLAY_DESCR_IDX) & 31) << 4)))[0] = ld_size; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x0014UL))[0] |= (1UL << (31 - ((VX1000_OVERLAY_DESCR_IDX) & 31))); /* PFCRDE -> set descr valid */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_OVERLAY_DESCR_IDX >= 16 */
  }
  return retVal;
}

#elif defined(VX1000_TARGET_TRICORE)

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MapCalRam - may be overloaded internally for the used hardware                                       */
/* API name:      None                                                                                                        */
/* Return value:  status:                                                                                                     */
/*                0: requested overlay not possible                                                                           */
/*                1: mapping complete                                                                                         */
/* Parameter1:    Overlay window size                                                                                         */
/* Parameter2:    properly aligned uncached flash access address                                                              */
/* Parameter3:    overlay storage address (not used on TriCore)                                                               */
/* Parameter4:    physical address of Parameter2 or Parameter3                                                                */
/*                If RAM address == flash address, the overlay is turned off completely.                                      */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Derivative-specific method for enabling/disabling a single overlay window using TriCore overlay features.   */
/*                Note: The driver assumes exclusive ownership of the overlay unit.                                           */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_MapCalRam)( VX1000_UINT32 size, VX1000_UINT32 target, VX1000_UINT32 source1, VX1000_UINT32 source2 )
{
  VX1000_UINT8 retVal = 1U; /* return "mapping complete" by default */
  VX1000_UINT8 sfrModel = 0U; /* dummy initialisation to prevent compiler warnings */
  VX1000_UINT32 validWindowSizes = 0x00000000UL;
  VX1000_UINT32 rabrSpecBits = 0x00000000UL;

  VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,0UL, (source1),(retVal),(sfrModel),(0)))  /* dummy accesses to avoid compiler/MISRA "unused"/"overwritten without prior usage" warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  switch ((VX1000_MCREG_CBS_JTAGID) & (VX1000_JTAGID_PN_MASK)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  {
    case VX1000_JTAGID_PN_TC172x: /* FutureMax */
      /* fallthrough to VX1000_JTAGID_PN_TC1797 */
    case VX1000_JTAGID_PN_TC1387: /* AudoS */
      /* fallthrough to VX1000_JTAGID_PN_TC1797 */
    case VX1000_JTAGID_PN_TC178x: /* AudoMax */
      /* fallthrough to VX1000_JTAGID_PN_TC1797 */
    case VX1000_JTAGID_PN_TC1767: /* AudoFuture */
      /* fallthrough to VX1000_JTAGID_PN_TC1797 */
    case VX1000_JTAGID_PN_TC1797: /* AudoFuture */
      /* note: we had to guess the RABR bits from the documentation of incompatible devices ... */
      if (source2 >= 0xAFF00000UL) /* is it in EMEM (e.g. 512 Kb)? */
      {
        validWindowSizes = 0x0003FC00UL;
        rabrSpecBits = 0xA0000000UL ^ 0xC0000000UL;  /* ?!? the manual spouts twaddle and the FW uses "0x4FF00000UL" instead ... */
      }
      else if (source2 >= 0xAFE80000UL) /* is it in PMU space (e.g. 8 Kb OVRAM)? */
      {
        validWindowSizes = 0x00000FF0UL;
        rabrSpecBits = 0xA0000000UL ^ 0x80000000UL;  /* ?!? the manual spouts twaddle and the FW uses "0x4FF00000UL" instead ... */
      }
      else if (source2 >= 0xA0800000UL) /* is it in EBU space (external memory; size depends on board)? */
      {
        validWindowSizes = 0x0003FC00UL;
        rabrSpecBits = 0xA0000000UL ^ 0xE0000000UL;  /* ?!? the manual spouts twaddle and the FW ignores it completely ... */
      }
      else
      {
        ; /* dummy else with empty statement only here for MISRA */
      }
      break;
    case VX1000_JTAGID_PN_TC1798: /* AudoMax */
      if (source2 >= 0xB1000000UL) /* is it in EMEM (768 Kb)? */  /* ?!? the FW uses "0xBF000000UL" instead ... */
      {
        validWindowSizes = 0x0003FFE0UL;
        rabrSpecBits = 0xB1000000UL ^ 0xC1000000UL;  /* ?!? the FW uses "0x4F000000UL" instead ... */
      }
      else if (source2 >= 0xB0000000UL) /* is it in LMU-SRAM (128 Kb)? */
      {
        validWindowSizes = 0x0003FFE0UL;
        rabrSpecBits = 0xB0000000UL ^ 0x80000000UL;
      }
      else if (source2 >= 0xA3000000UL) /* is it in EBU space (external memory; size depends on board)? */
      {
        validWindowSizes = 0x0003FFE0UL;
        rabrSpecBits = 0xA3000000UL ^ 0xA3000000UL;
      }
      else
      {
        ; /* dummy else with empty statement only here for MISRA */
      }
      break;
    case VX1000_JTAGID_PN_TC21x:   /* ?just a guess: "Aurix single core with same properties as all other TC2xx derivatives"? */
      /* fallthrough to VX1000_JTAGID_PN_TC22x */
    case VX1000_JTAGID_PN_TC22x:   /* Aurix single core */
      /* fallthrough to VX1000_JTAGID_PN_TC23x */
    case VX1000_JTAGID_PN_TC23x:   /* Aurix single core */
      /* fallthrough to VX1000_JTAGID_PN_TC24x */
    case VX1000_JTAGID_PN_TC24x:   /* Aurix single core */
      /* fallthrough to VX1000_JTAGID_PN_TC26x */
    case VX1000_JTAGID_PN_TC26x:   /* Aurix dual core */
      /* fallthrough to VX1000_JTAGID_PN_TC27xTC2Dx */
    case VX1000_JTAGID_PN_TC27xTC2Dx:  /* Aurix triple core */
      /* fallthrough to VX1000_JTAGID_PN_TC29x */
    case VX1000_JTAGID_PN_TC29x:  /* Aurix triple core */
      if (source2 >= 0xBF000000UL) /* is it in EMEM (e.g. 1024 Kb)? */
      {
        validWindowSizes = 0x0003FFE0UL;
        rabrSpecBits = 0xBF000000UL ^ 0x87000000UL;
      }
      else if (source2 >= 0xB0000000UL) /* is it in LMURAM (32 Kb)? */
      {
        validWindowSizes = 0x0000FFE0UL; /* hint: NOT available on VX1000_JTAGID_PN_TC26x */
        rabrSpecBits = 0xB0000000UL ^ 0x86000000UL;
      }
      else if (source2 >= 0xA3000000UL) /* is it in EBU space (external memory; size depends on board)? */
      {
        validWindowSizes = 0x0003FFE0UL; /* hint: ONLY available on VX1000_JTAGID_PN_TC29x */
        rabrSpecBits = 0xA3000000UL ^ 0x85000000UL;
      }
      else if (source2 >= 0x90000000UL) /* is it in LMURAM (32 Kb)? */
      {
        validWindowSizes = 0x0000FFE0UL; /* hint: NOT available on VX1000_JTAGID_PN_TC26x */
        rabrSpecBits = 0x90000000UL ^ 0x86000000UL;
      }
      else if (source2 >= 0x70100000UL) /* is it in PSPR0 (16 Kb)? */
      {
        validWindowSizes = 0x00007FE0UL;
        rabrSpecBits = 0x70000000UL ^ 0x80000000UL;
      }
      else if (source2 >= 0x70000000UL) /* is it in DSPR0 (72 kBbyte whose first 8 Kb are however reserved for multi core)? */
      {
        validWindowSizes = 0x0001FFE0UL;
        rabrSpecBits = 0x70000000UL ^ 0x80000000UL;
      }
      else if (source2 >= 0x60100000UL) /* is it in PSPR1 (16 Kb)? */
      {
        validWindowSizes = 0x00007FE0UL; /* hint: NOT available on single core derivatives */
        rabrSpecBits = 0x60000000UL ^ 0x81000000UL;
      }
      else if (source2 >= 0x60000000UL) /* is it in DSPR1 (72 kBbyte whose first 8 Kb are however reserved for multi core)? */
      {
        validWindowSizes = 0x0001FFE0UL; /* hint: NOT available on single core derivatives */
        rabrSpecBits = 0x60000000UL ^ 0x81000000UL;
      }
      else if (source2 >= 0x50100000UL) /* is it in PSPR2 (16 Kb)? */
      {
        validWindowSizes = 0x00007FE0UL; /* hint: ONLY available on triple core derivatives */
        rabrSpecBits = 0x50000000UL ^ 0x82000000UL;
      }
      else if (source2 >= 0x50000000UL) /* is it in DSPR2 (72 kBbyte whose first 8 Kb are however reserved for multi core)? */
      {
        validWindowSizes = 0x0001FFE0UL; /* hint: ONLY available on triple core derivatives */
        rabrSpecBits = 0x50000000UL ^ 0x82000000UL;
      }
      else
      {
        ; /* dummy else with empty statement only here for MISRA */
      }
      break;
    case VX1000_JTAGID_PN_TC32x:
      /* fallthrough to VX1000_JTAGID_PN_TC33x */
    case VX1000_JTAGID_PN_TC33x:
      /* fallthrough to VX1000_JTAGID_PN_TC33xED */
    case VX1000_JTAGID_PN_TC33xED:
      /* fallthrough to VX1000_JTAGID_PN_TC35x */
    case VX1000_JTAGID_PN_TC35x:
      /* fallthrough to VX1000_JTAGID_PN_TC36x */
    case VX1000_JTAGID_PN_TC36x:
      /* fallthrough to VX1000_JTAGID_PN_TC37x */
    case VX1000_JTAGID_PN_TC37x:
      /* fallthrough to VX1000_JTAGID_PN_TC37xED */
    case VX1000_JTAGID_PN_TC37xED:
      /* fallthrough to VX1000_JTAGID_PN_TC38x */
    case VX1000_JTAGID_PN_TC38x:
      /* fallthrough to VX1000_JTAGID_PN_TC39x */
    case VX1000_JTAGID_PN_TC39x:
      if (source2 >= 0xB9000000UL) /* is it in EMEM (e.g. 4 Mb)? */
      {
        validWindowSizes = 0x003FFFE0UL; /* hint: ONLY available on ADAS, ED and TC39x devices */
        rabrSpecBits = 0xB9000000UL ^ 0x89000000UL;
      }
      else if (source2 >= 0xB0000000UL) /* is it in LMURAM (e.g. 1 Mb)? */
      {
        validWindowSizes = 0x001FFFE0UL; /* hint: ONLY available on TC39x + TC38x */
        rabrSpecBits = 0xB0000000UL ^ 0x88000000UL;
      }
      else if (source2 >= 0xA1000000UL) /* is it in EBU space (external memory; size depends on board)? */
      {
        validWindowSizes = 0x003FFFE0UL; /* hint: ONLY available on TC39x */
        rabrSpecBits = 0xA1000000UL ^ 0x8A000000UL;
      }
      else if (source2 >= 0x99000000UL) /* is it in EMEM (e.g. 4 Mb)? */
      {
        validWindowSizes = 0x003FFFE0UL; /* hint: ONLY available on ADAS, ED and TC39x devices */
        rabrSpecBits = 0x99000000UL ^ 0x89000000UL;
      }
      else if (source2 >= 0x90000000UL) /* is it in LMURAM (e.g. 1 Mb)? */
      {
        validWindowSizes = 0x001FFFE0UL; /* hint: ONLY available on TC39x + TC38x */
        rabrSpecBits = 0x90000000UL ^ 0x88000000UL;
      }
      else if (source2 >= 0x81000000UL) /* is it in EBU space (external memory; size <= 112Mb depends on board)? */
      {
        validWindowSizes = 0x003FFFE0UL; /* hint: ONLY available on TC39x */
        rabrSpecBits = 0x81000000UL ^ 0x8A000000UL;
      }
      else if (source2 >= 0x70100000UL) /* is it in PSPR0 (64 Kb)? */
      {
        validWindowSizes = 0x0001FFE0UL;
        rabrSpecBits = 0x70000000UL ^ 0x80000000UL;
      }
      else if (source2 >= 0x70000000UL) /* is it in DSPR0 (96 Kb)? */
      {
        validWindowSizes = 0x0001FFE0UL;
        rabrSpecBits = 0x70000000UL ^ 0x80000000UL;
      }
      else if (source2 >= 0x60100000UL) /* is it in PSPR1 (64 Kb)? */
      {
        validWindowSizes = 0x0001FFE0UL;
        rabrSpecBits = 0x60000000UL ^ 0x81000000UL;
      }
      else if (source2 >= 0x60000000UL) /* is it in DSPR1 (96 Kb)? */
      {
        validWindowSizes = 0x0001FFE0UL;
        rabrSpecBits = 0x60000000UL ^ 0x81000000UL;
      }
      else if (source2 >= 0x50100000UL) /* is it in PSPR2 (64 Kb)? */
      {
        validWindowSizes = 0x0001FFE0UL;
        rabrSpecBits = 0x50000000UL ^ 0x82000000UL;
      }
      else if (source2 >= 0x50000000UL) /* is it in DSPR2 (96 Kb)? */
      {
        validWindowSizes = 0x0001FFE0UL;
        rabrSpecBits = 0x50000000UL ^ 0x82000000UL;
      }
      else if (source2 >= 0x40100000UL) /* is it in PSPR3 (64 Kb)? */
      { /* PRQA S 0715 */ /* If the user's compiler only supports the bare ISO-5.2.4.1 translation limits of maximum 15 scopes or "else if" levels, the user has to disable the ECU-defined overlay feature */
        validWindowSizes = 0x0001FFE0UL;
        rabrSpecBits = 0x40000000UL ^ 0x83000000UL;
      }
      else if (source2 >= 0x40000000UL) /* is it in DSPR3 (96 Kb)? */ /* PRQA S 0715 */ /* If the user's compiler only supports the bare ISO-5.2.4.1 translation limits of maximum 15 scopes or "else if" levels, the user has to disable the ECU-defined overlay feature */
      {
        validWindowSizes = 0x0001FFE0UL;
        rabrSpecBits = 0x40000000UL ^ 0x83000000UL;
      }
      else if (source2 >= 0x30100000UL) /* is it in PSPR4 (64 Kb)? */
      {
        validWindowSizes = 0x0001FFE0UL;
        rabrSpecBits = 0x30000000UL ^ 0x84000000UL;
      }
      else if (source2 >= 0x30000000UL) /* is it in DSPR4 (96 Kb)? */
      {
        validWindowSizes = 0x0001FFE0UL;
        rabrSpecBits = 0x30000000UL ^ 0x84000000UL;
      }
      else if (source2 >= 0x10100000UL) /* is it in PSPR5 (64 Kb)? */
      {
        validWindowSizes = 0x0001FFE0UL;
        rabrSpecBits = 0x10000000UL ^ 0x85000000UL;
      }
      else if (source2 >= 0x10000000UL) /* is it in DSPR5 (96 Kb)? */
      {
        validWindowSizes = 0x0001FFE0UL;
        rabrSpecBits = 0x10000000UL ^ 0x85000000UL;
      }
      else
      {
        ; /* dummy else with empty statement only here for MISRA */
      }
      break;
    default:
      break;
  }
  if (target == source2)
  {
    validWindowSizes = 0xFFFFFFFFUL; /* allow any window size to be turned _off_ */
  }

  if ((0==(size & validWindowSizes)) || ((size & (size - 1)) != 0))
  {
    VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_INVALID_SIZE)
    retVal = 0; /* unsupported size specified */
  }
  else if (((target | source2) & (size - 1UL)) != 0UL)
  {
    VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_MISALIGNED)
    retVal = 0; /* misaligned address specified */
  }
  else
  {
    switch ((VX1000_MCREG_CBS_JTAGID) & (VX1000_JTAGID_PN_MASK)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    {
      case VX1000_JTAGID_PN_TC172x:       sfrModel = 0U; break; /* FutureMax : not sure about the sfrModel */
      case VX1000_JTAGID_PN_TC1387:       sfrModel = 0U; break; /* AudoS */
      case VX1000_JTAGID_PN_TC178x:       sfrModel = 0U; break; /* AudoMax */
      case VX1000_JTAGID_PN_TC1767:       sfrModel = 0U; break; /* AudoFuture */
      case VX1000_JTAGID_PN_TC1797:       sfrModel = 0U; break; /* AudoFuture */
      case VX1000_JTAGID_PN_TC1798:       sfrModel = 1U; break; /* AudoMax */
      case VX1000_JTAGID_PN_TC21x:        sfrModel = 2U; break; /* just a guess: "Aurix single core with same properties as all other TC2xx derivatives" */
      case VX1000_JTAGID_PN_TC22x:        sfrModel = 2U; break; /* Aurix single core */
      case VX1000_JTAGID_PN_TC23x:        sfrModel = 2U; break; /* Aurix single core */
      case VX1000_JTAGID_PN_TC24x:        sfrModel = 2U; break; /* Aurix single core */
      case VX1000_JTAGID_PN_TC26x:        sfrModel = 3U; break; /* Aurix dual core */
      case VX1000_JTAGID_PN_TC27xTC2Dx:   sfrModel = 4U; break; /* Aurix triple core */
      case VX1000_JTAGID_PN_TC29x:        sfrModel = 4U; break; /* Aurix triple core */
      /*
      TODOKNM: case VX1000_JTAGID_PN_TC32x:        sfrModel = ?U; break; How many cores have we got?
      */
      case VX1000_JTAGID_PN_TC33x:        sfrModel = 2U; break; /* Aurix single core */
      case VX1000_JTAGID_PN_TC33xED:      sfrModel = 3U; break; /* Aurix dual core */
      case VX1000_JTAGID_PN_TC35x:        sfrModel = 4U; break; /* Aurix triple core */
      case VX1000_JTAGID_PN_TC36x:        sfrModel = 3U; break; /* Aurix dual core */
      case VX1000_JTAGID_PN_TC37x:        sfrModel = 4U; break; /* Aurix triple core */
      case VX1000_JTAGID_PN_TC37xED:      sfrModel = 4U; break; /* Aurix triple core */
      case VX1000_JTAGID_PN_TC38x:        sfrModel = 5U; break; /* Aurix quad core */
      case VX1000_JTAGID_PN_TC39x:        sfrModel = 7U; break; /* Aurix hexa core */
      default:
        break;
    }

    /* Change the Redirected Address Base Register, the Overlay Mask Register and the Overlay Target Address Register to turn mappings on or off */
    switch (sfrModel)
    {
      case 0U: /* standard TriCore registers (unprotected) */
      case 1U: /* AudoMax standard TriCore registers (but ENDINIT protected) */
        if (sfrModel == 1U)
        {
          (VX1000_ADDR_TO_PTR2VU32(0xF0000478UL))[0] = 0x000000A1UL; /* unlock sequence for CBS_OCNTRL */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(0xF0000478UL))[0] = 0x0000005EUL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(0xF0000478UL))[0] = 0x000000A1UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(0xF0000478UL))[0] = 0x0000005EUL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          VX1000_MCREG_CBS_OCNTRL = 0x000000C0UL; /* unlock the ENDINIT protected registers */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        }
        VX1000_MCREG_OVC_OENABLE = 0x00000001UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        if (sfrModel == 1U)
        {
          VX1000_MCREG_CBS_OCNTRL = 0x00000040UL; /* restore the ENDINIT protection state (TODO: do this only when actually protected beforehand!) */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        }
#if 0 /* experiments on AudoMax showed that changing the range registers is not possible while the overlay is turned off --> disabled the code, while the illegal "#if 0" reminds us in every release that we have to find a workaround */
        /* turn this mapping off before changing its parameters */
        (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK(VX1000_OVERLAY_DESCR_IDX)))[0] = 0x00000000UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* 0 */
        if (target == source2)
        {
          /* disable the range */
          VX1000_MCREG_OVC_OCON = 0x00000000UL | ((0x0000FFFFUL & (VX1000_MCREG_OVC_OCON)) &~ (1UL << (VX1000_OVERLAY_DESCR_IDX))); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          VX1000_MCREG_OVC_OCON = 0x00050000UL | ((0x0000FFFFUL & (VX1000_MCREG_OVC_OCON))); /* DCINVAL|OVSTRT */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        }
        else
        {
          /* install the new mapping: */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR( VX1000_OVERLAY_DESCR_IDX)))[0] = rabrSpecBits ^ source2; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR( VX1000_OVERLAY_DESCR_IDX)))[0] = 0x0FFFFFFFUL & target; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK(VX1000_OVERLAY_DESCR_IDX)))[0] = 0x10000000UL - size; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          if (source1 == 1)
          {
            /* disable the range */
            VX1000_MCREG_OVC_OCON = 0x00000000UL | ((0x0000FFFFUL & (VX1000_MCREG_OVC_OCON)) &~ (1UL << (VX1000_OVERLAY_DESCR_IDX))); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            VX1000_MCREG_OVC_OCON = 0x00050000UL | ((0x0000FFFFUL & (VX1000_MCREG_OVC_OCON))); /* DCINVAL|OVSTRT */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          else
          {
            /* Overlay Configuration and Range Select Register (NOTE: basically we do not want to change the windows of other drivers, but when we read their enable-bits, on AudoMax all windows are turned off!? --> disabled the code) */
            VX1000_MCREG_OVC_OCON = (0x00000000UL /*| (0x0000FFFFUL & (VX1000_MCREG_OVC_OCON))*/) | (1UL << (VX1000_OVERLAY_DESCR_IDX)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            VX1000_MCREG_OVC_OCON = (0x00050000UL /*| (0x0000FFFFUL & (VX1000_MCREG_OVC_OCON))*/) | (1UL << (VX1000_OVERLAY_DESCR_IDX) /* ORed again because we cannot read from the register */); /* DCINVAL|OVSTRT */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
        }
        break;
      case 2U: /* Aurix single core */
      case 3U: /* Aurix dual core */
      case 4U: /* Aurix triple core */
      case 5U: /* AurixPlus quad core */
      /*"static code checkers see that this will never come true:" case 6U:*/ /* potential AurixPlus penta core */
      /*case 7U:*/ /* AurixPlus hexa core - see below*/
      default: /* replaces "case 7U:" as last label because some compilers/code checkers complain if dummy default case is missing and others (correctly) complain that the dummy default case is unreachable code */
        (VX1000_ADDR_TO_PTR2VU32(0xF0000478UL))[0] = 0x000000A1UL; /* unlock sequence for CBS_OCNTRL */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        (VX1000_ADDR_TO_PTR2VU32(0xF0000478UL))[0] = 0x0000005EUL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        (VX1000_ADDR_TO_PTR2VU32(0xF0000478UL))[0] = 0x000000A1UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        (VX1000_ADDR_TO_PTR2VU32(0xF0000478UL))[0] = 0x0000005EUL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        VX1000_MCREG_CBS_OCNTRL = 0x000000C0UL; /* unlock the ENDINIT protected registers */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        {
          VX1000_UINT32 sfrReadback = (VX1000_MCREG_CBS_OCNTRL) + (2 * (VX1000_MCREG_CBS_OCNTRL));
          VX1000_DUMMYREAD(sfrReadback)
        }
        VX1000_MCREG_SCU_OVCENBL_AURIX = 0x0000003FUL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        /* turn mapping off before changing its parameters */
        (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(VX1000_OVERLAY_DESCR_IDX, 0UL)))[0] = 0x00000000UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        if (sfrModel != 2U) /* not a single core derivative */
        {
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(VX1000_OVERLAY_DESCR_IDX, 1UL)))[0] = 0x00000000UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        }
        if ((sfrModel != 2U) && (sfrModel != 3U)) /* neither single nor dual core derivative */
        {
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(VX1000_OVERLAY_DESCR_IDX, 2UL)))[0] = 0x00000000UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        }
        if (((sfrModel != 2U) && (sfrModel != 3U)) && (sfrModel != 4)) /* neither single nor dual nor triple core derivative */
        {
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(VX1000_OVERLAY_DESCR_IDX, 3UL)))[0] = 0x00000000UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        }
        if ((sfrModel == 7U) /* "static code checkers see that this will never come true:" || (sfrModel == 6U)*/) /* penta or hexa core derivative */
        {
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(VX1000_OVERLAY_DESCR_IDX, 4UL)))[0] = 0x00000000UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        }
        if (sfrModel == 7U) /* hexa core derivative */
        {
          /* Note that there is one gap in the OVC core indices (CPU5 continues with index 6) */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(VX1000_OVERLAY_DESCR_IDX, 6UL)))[0] = 0x00000000UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        }
        if (target != source2)
        {
          /* install the new mapping: */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( VX1000_OVERLAY_DESCR_IDX, 0UL)))[0] = rabrSpecBits ^ source2; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( VX1000_OVERLAY_DESCR_IDX, 0UL)))[0] = 0x0FFFFFFFUL & target; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(VX1000_OVERLAY_DESCR_IDX, 0UL)))[0] = 0x10000000UL - size; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          if (sfrModel != 2U) /* not a single core derivative */
          {
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( VX1000_OVERLAY_DESCR_IDX, 1UL)))[0] = rabrSpecBits ^ source2; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( VX1000_OVERLAY_DESCR_IDX, 1UL)))[0] = 0x0FFFFFFFUL & target; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(VX1000_OVERLAY_DESCR_IDX, 1UL)))[0] = 0x10000000UL - size; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          if ((sfrModel != 2U) && (sfrModel != 3U)) /* neither single nor dual core derivative */
          {
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( VX1000_OVERLAY_DESCR_IDX, 2UL)))[0] = rabrSpecBits ^ source2; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( VX1000_OVERLAY_DESCR_IDX, 2UL)))[0] = 0x0FFFFFFFUL & target; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(VX1000_OVERLAY_DESCR_IDX, 2UL)))[0] = 0x10000000UL - size; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          if (((sfrModel != 2U) && (sfrModel != 3U)) && (sfrModel != 4)) /* neither single nor dual nor triple core derivative */
          {
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( VX1000_OVERLAY_DESCR_IDX, 3UL)))[0] = rabrSpecBits ^ source2; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( VX1000_OVERLAY_DESCR_IDX, 3UL)))[0] = 0x0FFFFFFFUL & target; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(VX1000_OVERLAY_DESCR_IDX, 3UL)))[0] = 0x10000000UL - size; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          if ((sfrModel == 7U) /* "static code checkers see that the second condition never will come true:" || (sfrModel == 6U)*/) /* penta or hexa core derivative */
          {
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( VX1000_OVERLAY_DESCR_IDX, 4UL)))[0] = rabrSpecBits ^ source2; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( VX1000_OVERLAY_DESCR_IDX, 4UL)))[0] = 0x0FFFFFFFUL & target; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(VX1000_OVERLAY_DESCR_IDX, 4UL)))[0] = 0x10000000UL - size; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          if (sfrModel == 7U) /* hexa core derivative */
          {
            /* Note that there is one gap in the OVC core indices (CPU5 continues with index 6) */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( VX1000_OVERLAY_DESCR_IDX, 6UL)))[0] = rabrSpecBits ^ source2; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( VX1000_OVERLAY_DESCR_IDX, 6UL)))[0] = 0x0FFFFFFFUL & target; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(VX1000_OVERLAY_DESCR_IDX, 6UL)))[0] = 0x10000000UL - size; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
        }
        /* Overlay Range Select Registers */
        if ((target == source2) || (source1 == 1))
        {
          VX1000_MCREG_OVC0_OSEL   &=~ (1UL << (VX1000_OVERLAY_DESCR_IDX)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          if (sfrModel != 2U) /* not a single core derivative */
          {
            VX1000_MCREG_OVC1_OSEL &=~ (1UL << (VX1000_OVERLAY_DESCR_IDX)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          if ((sfrModel != 2U) && (sfrModel != 3U)) /* neither single nor dual core derivative */
          {
            VX1000_MCREG_OVC2_OSEL &=~ (1UL << (VX1000_OVERLAY_DESCR_IDX)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          if (((sfrModel != 2U) && (sfrModel != 3U)) && (sfrModel != 4)) /* neither single nor dual nor triple core derivative */
          {
            VX1000_MCREG_OVC3_OSEL &=~ (1UL << (VX1000_OVERLAY_DESCR_IDX)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          if ((sfrModel == 7U) /* "static code checkers see that this will never come true:" || (sfrModel == 6U)*/) /* penta or hexa core derivative */
          {
            VX1000_MCREG_OVC4_OSEL &=~ (1UL << (VX1000_OVERLAY_DESCR_IDX)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          if (sfrModel == 7U) /* hexa core derivative */
          {
            VX1000_MCREG_OVC5_OSEL &=~ (1UL << (VX1000_OVERLAY_DESCR_IDX)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
        }
        else
        {
          VX1000_MCREG_OVC0_OSEL   |= 1UL << (VX1000_OVERLAY_DESCR_IDX); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          if (sfrModel != 2U) /* not a single core derivative */
          {
            VX1000_MCREG_OVC1_OSEL |= 1UL << (VX1000_OVERLAY_DESCR_IDX); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          if ((sfrModel != 2U) && (sfrModel != 3U)) /* neither single nor dual core derivative */
          {
            VX1000_MCREG_OVC2_OSEL |= 1UL << (VX1000_OVERLAY_DESCR_IDX); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          if (((sfrModel != 2U) && (sfrModel != 3U)) && (sfrModel != 4)) /* neither single nor dual nor triple core derivative */
          {
            VX1000_MCREG_OVC3_OSEL |= 1UL << (VX1000_OVERLAY_DESCR_IDX); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          if ((sfrModel == 7U) /* "static code checkers see that this will never come true:" || (sfrModel == 6U)*/) /* penta or hexa core derivative */
          {
            VX1000_MCREG_OVC4_OSEL |= 1UL << (VX1000_OVERLAY_DESCR_IDX); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          if (sfrModel == 7U) /* hexa core derivative */
          {
            VX1000_MCREG_OVC5_OSEL |= 1UL << (VX1000_OVERLAY_DESCR_IDX); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
        }
        VX1000_MCREG_SCU_OVCCON = 0x0005003FUL; /* DCINVAL|OVSTRT|COREs */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        VX1000_MCREG_CBS_OCNTRL = 0x00000040UL; /* restore the ENDINIT protection state */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        break;
      }
    retVal = 1; /* mapping updated */
  }
  return retVal;
}

#endif /* VX1000_TARGET_TRICORE */

#elif defined(VX1000_MPC56xCRAM_BASE_ADDR)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MapCalRam - may be overloaded internally for the used hardware                                       */
/* API name:      None                                                                                                        */
/* Return value:  status:                                                                                                     */
/*                0: requested overlay not possible                                                                           */
/*                1: mapping complete                                                                                         */
/* Parameter1:    Overlay window size                                                                                         */
/* Parameter2:    properly aligned virtual flash access address                                                               */
/* Parameter3:    overlay memory address (hard-wired on this hardware)                                                        */
/*                If Parameter2 == Parameter3, the overlay is turned off completely.                                          */
/* Parameter4:    physical address of Parameter3 (not used on MPC56x)                                                         */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Derivative-specific method for enabling/disabling a single overlay window using MPC500 CALRAM feature.      */
/*                Note: The driver assumes exclusive ownership of the entire CRAM controller.                                 */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_MapCalRam)(VX1000_UINT32 size, VX1000_UINT32 target, VX1000_UINT32 source1, VX1000_UINT32 source2)
{
  VX1000_UINT8 block = 0, retVal = 1; /* return "mapping complete" by default */
  volatile VX1000_UINT32 *cram = (VX1000_ADDR_TO_PTR2VU32(VX1000_MPC56xCRAM_BASE_ADDR)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */

  cram[0] = 0; /* CRAMMCR = 0x00000000 */
  if (target == source1)
  {
    cram[0x28U >> 2U] = 0x00000000UL; /* disable all overlays via CRAMOVLCR global enable bit */
    VX1000_DUMMYREAD(source2)         /* just to avoid compiler "unused" warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  }
  else
  {
    while (size >= 512UL) /* note: the dummy initialisation of size is only here for MISRA, don't remove it! */
    {
      if ((block <= 7U) && ((target & 0xFFE001FFUL) == 0))
      {
        cram[2U + block] = 0x70000000UL | target; /* CRAM_RBAx = SIZCODEx<<28 + (TADDRx &~ SIZEMASK) */
        block++;
        target += 512;
        size -= 512UL;
      }
      else
      {
        size = 1UL; /* create an impossible size to leave the loop and the function */
      }
    }
    switch (size) /* note: joining smaller than 512bytes blocks is not possible because the HW leaves gaps in the source memory */
    {
      case 256:
        if ((block <= 7U) && ((target & 0xFFE000FFUL) == 0))
        {
          cram[2U + block] = 0x60000000UL | target;
          size -= 256UL;
          block++;
        }
        break;
      case 128:
        if ((block <= 7U) && ((target & 0xFFE0007FUL) == 0))
        {
          cram[2U + block] = 0x50000000UL | target;
          size -= 128UL;
          block++;
        }
        break;
      case 64:
        if ((block <= 7U) && ((target & 0xFFE0003FUL) == 0))
        {
          cram[2U + block] = 0x40000000UL | target;
          size -= 64UL;
          block++;
        }
        break;
      case 32:
        if ((block <= 7U) && ((target & 0xFFE0001FUL) == 0))
        {
          cram[2U + block] = 0x30000000UL | target;
          size -= 32UL;
          block++;
        }
        break;
      case 16:
        if ((block <= 7U) && ((target & 0xFFE0000FUL) == 0))
        {
          cram[2U + block] = 0x20000000UL | target;
          size -= 16UL;
          block++;
        }
        break;
      case 4:  /* Note: there is no 8-byte block mapping */
        if ((block <= 7U) && ((target & 0xFFE00003UL) == 0))
        {
          cram[2U + block] = 0x10000000UL | target;
          size -= 4UL;
          block++;
        }
        break;
      default: /* zero or invalid setting */
        VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_INVALID_SIZE)
        /* error is generated below */
        break;
    }
    for (; block < 8; block++)
    {
      cram[2U + block] = 0x00000000UL; /* invalidate unused blocks */
    }
    if (size != 0)
    {
      retVal = 0;  /* requested overlay not possible */
    }
    else
    {
      cram[0x28U >> 2U] = 0x80000000UL;/* enable overlay */ /* CRAMOVLCR = OVL_ENBL << 31; Hint: MSR.DR also has an influence */
    }
  }
  return retVal;
}
#endif /* VX1000_MPC56xCRAM_BASE_ADDR */
#if defined(VX1000_SH2_FCU_BASE_ADDR)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MapCalRam - may be overloaded internally for the used hardware                                       */
/* API name:      None                                                                                                        */
/* Return value:  status:                                                                                                     */
/*                0: requested overlay not possible                                                                           */
/*                1: mapping complete                                                                                         */
/* Parameter1:    Overlay window size                                                                                         */
/* Parameter2:    properly aligned virtual flash access address                                                               */
/* Parameter3:    overlay memory address                                                                                      */
/*                If Parameter2 == Parameter3, the overlay is turned off completely.                                          */
/* Parameter4:    physical address of Parameter3 (optional)                                                                   */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Derivative-specific method for enabling/disabling a single overlay window using SH2 FCU feature.            */
/*                Note: The driver assumes exclusive ownership of the overlay unit.                                           */
/* Devel state:   Specified                                                                                                   */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_MapCalRam)( VX1000_UINT32 size, VX1000_UINT32 target, VX1000_UINT32 source1, VX1000_UINT32 source2 )
{
  VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)
  ++size; ++target; ++source1; ++source2; /* dummy usage to avoid compiler warnings */
  return 0; /* not implemented */
}
#endif /* VX1000_SH2_FCU_BASE_ADDR */

#if defined(VX1000_TARGET_XC2000)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MapCalRam - may be overloaded internally for the used hardware                                       */
/* API name:      None                                                                                                        */
/* Return value:  status:                                                                                                     */
/*                0: requested overlay not possible                                                                           */
/*                1: mapping complete                                                                                         */
/* Parameter1:    Overlay window size                                                                                         */
/* Parameter2:    properly aligned virtual flash access address                                                               */
/* Parameter3:    overlay memory address                                                                                      */
/*                If Parameter2 == Parameter3, the overlay is turned off completely.                                          */
/* Parameter4:    physical address of Parameter3 (optional)                                                                   */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Derivative-specific method for enabling/disabling a single overlay window using SH2 FCU feature.            */
/*                Note: The driver assumes exclusive ownership of the overlay unit.                                           */
/* Devel state:   Specified                                                                                                   */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_MapCalRam)( VX1000_UINT32 size, VX1000_UINT32 target, VX1000_UINT32 source1, VX1000_UINT32 source2 )
{
  VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)
  ++size; ++target; ++source1; ++source2; /* dummy usage to avoid compiler warnings */
  return 0; /* not implemented */
}
#endif /* VX1000_TARGET_XC2000 */

#endif /* VX1000_OVERLAY & !VX1000_OVERLAY_VX_CONFIGURABLE */

#if (defined(VX1000_OVERLAY)) && (defined(VX1000_OVERLAY_VX_CONFIGURABLE)) && (defined(VX1000_OVLENBL_REGWRITE_VIA_MX)) && (defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_OVERLAY_CONTROL))

#if defined(VX1000_TARGET_TRICORE)

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlayWriteEcuDescr                                                                                 */
/* API name:      None                                                                                                        */
/* Return value:  status:                                                                                                     */
/*                0: ovl regs pointed by gVX1000.Ovl.ovlConfigRegsPtr is written to ECU                                       */
/*                1: nothing is done                                                                                          */
/* Preemption:    todo vislwn                                                                                                 */
/* Termination:   todo vislwn                                                                                                 */
/* Precondition1: todo vislwn                                                                                                 */
/* Description:   This function writes gVX1000.Ovl.ovlConfigRegsPtr structure to ECU overlay regs.                            */
/*                It is called in vx1000_DetectVxAsyncEnd() when corresponding defines are activated in vx1000_cfg.h          */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlayWriteEcuDescr)(void)
{
  VX1000_UINT8 sfrModel = 0, coreIdx, descIdx, overlayDescriptors = 0, retVal = 1;

  switch ((VX1000_MCREG_CBS_JTAGID) & (VX1000_JTAGID_PN_MASK)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  {
    case VX1000_JTAGID_PN_TC172x:       overlayDescriptors = 16U; sfrModel = 0U; break; /* FutureMax : not sure about the sfrModel */
    case VX1000_JTAGID_PN_TC1387:       overlayDescriptors = 16U; sfrModel = 0U; break; /* AudoS */
    case VX1000_JTAGID_PN_TC178x:       overlayDescriptors = 16U; sfrModel = 0U; break; /* AudoMax */
    case VX1000_JTAGID_PN_TC1767:       overlayDescriptors = 16U; sfrModel = 0U; break; /* AudoFuture */
    case VX1000_JTAGID_PN_TC1797:       overlayDescriptors = 16U; sfrModel = 0U; break; /* AudoFuture */
    case VX1000_JTAGID_PN_TC1798:       overlayDescriptors = 16U; sfrModel = 1U; break; /* AudoMax */
    case VX1000_JTAGID_PN_TC21x:        overlayDescriptors =  8U; sfrModel = 2U; break; /* just a guess: "Aurix single core with 8 descriptors" */
    case VX1000_JTAGID_PN_TC22x:        overlayDescriptors =  8U; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC23x:        overlayDescriptors =  8U; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC24x:        overlayDescriptors = 32U; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC26x:        overlayDescriptors = 32U; sfrModel = 3U; break; /* Aurix dual core */
    case VX1000_JTAGID_PN_TC27xTC2Dx:   overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC29x:        overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    /*
    case VX1000_JTAGID_PN_TC32x:        overlayDescriptors = 32U; sfrModel = ?U; break; How many cores have we got?
    */
    case VX1000_JTAGID_PN_TC33x:        overlayDescriptors = 32U; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC33xED:      overlayDescriptors = 32U; sfrModel = 3U; break; /* Aurix dual core */
    case VX1000_JTAGID_PN_TC35x:        overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC36x:        overlayDescriptors = 32U; sfrModel = 3U; break; /* Aurix dual core */
    case VX1000_JTAGID_PN_TC37x:        overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC37xED:      overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC38x:        overlayDescriptors = 32U; sfrModel = 5U; break; /* Aurix quad core */
    case VX1000_JTAGID_PN_TC39x:        overlayDescriptors = 32U; sfrModel = 7U; break; /* Aurix hexa core */
    default: break;
  }
  if (gVX1000.Ovl.ovlConfigRegsPtr != 0)
  {
    switch (sfrModel)
    {
      case 4U:
        for (coreIdx = 0; coreIdx < 3; coreIdx++)
        {
          VX1000_MCREG_OVC_OSEL(coreIdx) = (VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr))->OSEL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          for (descIdx = 0; descIdx < overlayDescriptors; descIdx++)
          {
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( descIdx, coreIdx)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr))->DESCR)[descIdx].RABR; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( descIdx, coreIdx)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr))->DESCR)[descIdx].OTAR; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(descIdx, coreIdx)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr))->DESCR)[descIdx].OMASK; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
        }
        retVal = 0;
        break;
      case 7U:
        for (coreIdx = 0; coreIdx < 6; coreIdx++)
        {
          if (coreIdx == 5) { coreIdx = 6; } /* Note that there is one gap in the OVC core indices (CPU5 continues with index 6) */
          VX1000_MCREG_OVC_OSEL(coreIdx) = (VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr))->OSEL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          for (descIdx = 0; descIdx < overlayDescriptors; descIdx++)
          {
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( descIdx, coreIdx)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr))->DESCR)[descIdx].RABR; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( descIdx, coreIdx)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr))->DESCR)[descIdx].OTAR; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(descIdx, coreIdx)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr))->DESCR)[descIdx].OMASK; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
        }
        retVal = 0;
        break;
      default:
        VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)
        break;
    }
  }
  return retVal;
}

#else /* !VX1000_TARGET_TRICORE */

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlayWriteEcuDescr                                                                                 */
/* API name:      None                                                                                                        */
/* Return value:  status:                                                                                                     */
/*                0: ovl regs pointed by gVX1000.Ovl.ovlConfigRegsPtr is written to ECU                                       */
/*                1: nothing is done                                                                                          */
/* Preemption:    todo vislwn                                                                                                 */
/* Termination:   todo vislwn                                                                                                 */
/* Precondition1: todo vislwn                                                                                                 */
/* Description:   todo vislwn                                                                                                 */
/* Devel state:   Idea                                                                                                        */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlayWriteEcuDescr)(void)
{
  VX1000_UINT8 retVal = 1;
  VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)
  return retVal;
}

#endif /* !VX1000_TARGET_TRICORE */


#if defined(VX1000_TARGET_TRICORE)

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlayReadEcuDescr                                                                                  */
/* API name:      None                                                                                                        */
/* Return value:  status:                                                                                                     */
/*                0: ovl regs in ECU is read to struct pointed to by gVX1000.Ovl.ovlConfigRegsPtr                             */
/*                1: nothing is done                                                                                          */
/* Preemption:    todo vislwn                                                                                                 */
/* Termination:   todo vislwn                                                                                                 */
/* Precondition1: todo vislwn                                                                                                 */
/* Description:   This function reads ECU overlay regs to gVX1000.Ovl.ovlConfigRegsPtr structure.                             */
/*                It is called in vx1000_OverlayInit() when corresponding defines are activated in VX1000_cfg.h               */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlayReadEcuDescr)(void)
{
  VX1000_UINT8 sfrModel = 0, coreIdx, descIdx, overlayDescriptors = 0, retVal = 1;

  switch ((VX1000_MCREG_CBS_JTAGID) & (VX1000_JTAGID_PN_MASK)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  {
    case VX1000_JTAGID_PN_TC172x:       overlayDescriptors = 16U; sfrModel = 0U; break; /* FutureMax : not sure about the sfrModel */
    case VX1000_JTAGID_PN_TC1387:       overlayDescriptors = 16U; sfrModel = 0U; break; /* AudoS */
    case VX1000_JTAGID_PN_TC178x:       overlayDescriptors = 16U; sfrModel = 0U; break; /* AudoMax */
    case VX1000_JTAGID_PN_TC1767:       overlayDescriptors = 16U; sfrModel = 0U; break; /* AudoFuture */
    case VX1000_JTAGID_PN_TC1797:       overlayDescriptors = 16U; sfrModel = 0U; break; /* AudoFuture */
    case VX1000_JTAGID_PN_TC1798:       overlayDescriptors = 16U; sfrModel = 1U; break; /* AudoMax */
    case VX1000_JTAGID_PN_TC21x:        overlayDescriptors =  8U; sfrModel = 2U; break; /* just a guess: "Aurix single core with 8 descriptors" */
    case VX1000_JTAGID_PN_TC22x:        overlayDescriptors =  8U; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC23x:        overlayDescriptors =  8U; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC24x:        overlayDescriptors = 32U; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC26x:        overlayDescriptors = 32U; sfrModel = 3U; break; /* Aurix dual core */
    case VX1000_JTAGID_PN_TC27xTC2Dx:   overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC29x:        overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    /*
    TODOKNM: case VX1000_JTAGID_PN_TC32x:        overlayDescriptors = 32U; sfrModel = ?U; break; How many cores have we got?
    */
    case VX1000_JTAGID_PN_TC33x:        overlayDescriptors = 32U; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC33xED:      overlayDescriptors = 32U; sfrModel = 3U; break; /* Aurix dual core */
    case VX1000_JTAGID_PN_TC35x:        overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC36x:        overlayDescriptors = 32U; sfrModel = 3U; break; /* Aurix dual core */
    case VX1000_JTAGID_PN_TC37x:        overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC37xED:      overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC38x:        overlayDescriptors = 32U; sfrModel = 5U; break; /* Aurix quad core */
    case VX1000_JTAGID_PN_TC39x:        overlayDescriptors = 32U; sfrModel = 7U; break; /* Aurix hexa core */
    default: break;
  }
  if (gVX1000.Ovl.ovlConfigRegsPtr != 0)
  {
    switch (sfrModel)
    {
      case 7U:
      case 5U:
      case 4U:
      case 3U:
      case 2U:
        coreIdx = 0;  /* only need to read from first core, since all cores should be configured the same way */
        {
          (VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr))->OSEL = VX1000_MCREG_OVC_OSEL(coreIdx); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          for (descIdx = 0; descIdx < overlayDescriptors; descIdx++)
          {
            ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr))->DESCR)[descIdx].RABR = (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( descIdx, coreIdx)))[0]; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr))->DESCR)[descIdx].OTAR = (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( descIdx, coreIdx)))[0]; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr))->DESCR)[descIdx].OMASK= (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(descIdx, coreIdx)))[0]; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
        }
        retVal = 0;
        break;
      default:
        VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)
        break;
    }
  }
  return retVal;
}

#else /* !VX1000_TARGET_TRICORE */

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlayReadEcuDescr                                                                                  */
/* API name:      None                                                                                                        */
/* Return value:  status:                                                                                                     */
/*                0: ovl regs in ECU is read to struct pointed to by gVX1000.Ovl.ovlConfigRegsPtr                             */
/*                1: nothing is done                                                                                          */
/* Preemption:    todo vislwn                                                                                                 */
/* Termination:   todo vislwn                                                                                                 */
/* Precondition1: todo vislwn                                                                                                 */
/* Description:   This function reads ECU overlay regs to gVX1000.Ovl.ovlConfigRegsPtr structure.                             */
/*                It is called in vx1000_OverlayInit() when corresponding defines are activated in VX1000_cfg.h               */
/* Devel state:   Idea                                                                                                        */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlayReadEcuDescr)(void)
{
  VX1000_UINT8 retVal = 1;
  VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)
  return retVal;
}

#endif /* !VX1000_TARGET_TRICORE */

#endif /* VX1000_OVERLAY & VX1000_OVERLAY_VX_CONFIGURABLE & VX1000_OVLENBL_REGWRITE_VIA_MX & VX1000_MAILBOX & VX1000_MAILBOX_OVERLAY_CONTROL */

#if defined(VX1000_OVLENBL_KEEP_AWAKE)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_IsCalWakeupActive                                                                                    */
/* API name:      VX1000_IS_CAL_WAKEUP_ACTIVE                                                                                 */
/* Wrapper API:   VX1000If_IsCalWakeupActive                                                                                  */
/* Return value:  status:                                                                                                     */
/*                0: The download of the overlay image has completed (so the ECU can safely fall asleep again)                */
/*                1: The cause for the wakeup is still active                                                                 */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_InitAsyncStart must have been called.                                                                */
/* Description:   Returns whether the ECU must stay awake for calibration purposes. Since the last call of this function the  */
/*                VX1000 has requested the ECU to stay awake.                                                                 */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_IsCalWakeupActive)(void)
{
  VX1000_UINT8 retVal = 0;
  if (gVX1000.Ovl.ecuLastPresenceCounter != gVX1000.Ovl.presenceCounter)
  {
    retVal = 1;
  }
  gVX1000.Ovl.ecuLastPresenceCounter = gVX1000.Ovl.presenceCounter;
  return retVal;
}
#endif /* VX1000_OVLENBL_KEEP_AWAKE */

#if defined(VX1000_OVLENBL_PERSISTENT_EMEM)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_InvalidateEmem                                                                                       */
/* API name:      VX1000_INVALIDATE_EMEM                                                                                      */
/* Wrapper API:   VX1000If_InvalidateEmem                                                                                     */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: The ECC of the used EMEM must have been initialised (e.g. by vx1000_EmemHdrInit).                           */
/* Description:   Explicitly destroy the signature of VX-allocated persistent ECU-RAM to force reinitialisation if it.        */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
void VX1000_SUFFUN(vx1000_InvalidateEmem)(void)
{
  if (gVX1000.MagicId == (VX1000_UINT32)(VX1000_STRUCT_MAGIC))
  {
    if (gVX1000.OvlPtr != 0UL)
    {
      if (gVX1000.Ovl.persistentECUEmemHeaderPtr != 0UL)
      {
        VX1000_EMEM_HDR_T *hdr = (VX1000_ADDR_TO_PTR2EH(gVX1000.Ovl.persistentECUEmemHeaderPtr)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        hdr->ememInitEnd = 0UL;
        hdr->ememInitEndInvert = 0UL;
        hdr->ememInitStart = 0UL;
        hdr->ememInitStartInvert = 0UL;
      }
    }
  }
}
#endif /* VX1000_OVLENBL_PERSISTENT_EMEM */


#if defined(VX1000_OVERLAY) && defined(VX1000_MAILBOX_OVERLAY_CONTROL)

#if defined(VX1000_OVLENBL_SYNC_PAGESWITCH)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlaySetConfigDone                                                                                 */
/* API name:      VX1000_OVL_SET_CONFIG_DONE (normal operation) / VX1000_OVL_SET_CONFIG_DONE_STUP (start-up stage)            */
/* Wrapper API:   VX1000If_OvlSetConfigDone (normal operation) / VX1000If_OvlSetConfigDoneVoid (start-up stage)               */
/*                may be overloaded by user                                                                                   */
/* Return value:  None                                                                                                        */
/* Parameter1:    XCP error code in case of failed page switch, 0 in case of success                                          */
/* Parameter2:    The ID of the page that is in use now                                                                       */
/* Parameter3:    A boolean that tells the driver whether to forward the result to the tool (TRUE) or not (FALSE)             */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: A page switch has to be requested and executed.                                                             */
/* Description:   Inform the driver (and optionally also the tool) about the final reaction to a page switch request.         */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
void VX1000_SUFFUN(vx1000_OverlaySetConfigDone)( VX1000_UINT8 cfgResult, VX1000_UINT8 page, VX1000_UINT8 onStartup )
{
#if defined(VX1000_COMPILED_FOR_SLAVECORES)
  VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,cfgResult, page,onStartup,(0),(0))) /* dummy usage of unused operands */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
#else /* !VX1000_COMPILED_FOR_SLAVECORES */
  VX1000_UINT32 txLen;
  VX1000_CHAR txBuf[8];

  if (0 == cfgResult)
  {
    txLen = 1;
    txBuf[0] = (VX1000_CHAR)-1;
    gVX1000_ECU_CalPage = page;
    if (page != 0)
    {
      gVX1000.ToolDetectState |= VX1000_TDS_WORKING_PAGE;
    }
    else
    {
      gVX1000.ToolDetectState &= ~(VX1000_TDS_WORKING_PAGE);
    }
  }
  else
  {
    txLen = 2;
    txBuf[0] = (VX1000_CHAR)-2;
    txBuf[1] = (VX1000_CHAR)cfgResult;
  }
  if (0==onStartup)
  {
    (void)VX1000_SUFFUN(vx1000_MailboxWrite)(txLen, txBuf);
  }
#endif /* VX1000_COMPILED_FOR_SLAVECORES */
}

#if defined(VX1000_OVL_SET_CONFIG_INTERNAL)

#if defined(VX1000_FLASHPORTCTLR_BASE_ADDR)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlaySetConfig                                                                                     */
/* API name:      VX1000_OVL_SET_CONFIG (expression) / VX1000_OVL_SET_CONFIG_VOID (statement) - may be overloaded by user     */
/* Wrapper API:   VX1000If_OvlSetConfigVoid                                                                                   */
/* Return value:  status:                                                                                                     */
/*                0: Page switch done                                                                                         */
/*                1: Nothing done                                                                                             */
/*                2: Value not written correctly                                                                              */
/*                3: No single-master page-switch possible                                                                    */
/*                4: Generic error                                                                                            */
/* Parameter1:    Overlay windows to be activated/deactivated in case of VX-configurable overlay (otherwise unused)           */
/* Parameter2:    Resource Mask in case of VX-configurable overlay (otherwise unused)                                         */
/* Parameter3:    Overlay windows of optional second overlay controller for activation/deactivation in case of VX-configurable*/
/* Parameter4:    Resource Mask for optional second overlay controller in case of VX-configurable overlay (otherwise unused)  */
/* Parameter5:    Overlay Page                                                                                                */
/* Parameter6:    Masters to be activated                                                                                     */
/* Parameter7:    Masters resource Mask                                                                                       */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Derivative-specific method for globally enabling/disabling overlays on several MPC57xx family members.      */
/*                Note: the driver assumes exclusive ownership of the overlay unit.                                           */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlaySetConfig)( VX1000_UINT32 value, VX1000_UINT32 mask, VX1000_UINT32 valueB,
                                                     VX1000_UINT32 maskB, VX1000_UINT8 page, VX1000_UINT32 master,
                                                     VX1000_UINT32 calMaster )
{
  VX1000_UINT8 retVal = 0;    /* assume OK */
  volatile VX1000_UINT32 checkValue;
#if !defined(VX1000_FLASHPORTCTLR_BASE_ADDR2)
  VX1000_DUMMYREAD(valueB)    /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_DUMMYREAD(maskB)     /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
#endif /* !VX1000_FLASHPORTCTLR_BASE_ADDR2 */
#if defined(VX1000_OVERLAY_VX_CONFIGURABLE)
#if defined(VX1000_OVLENBL_CORE_SYNC_PAGESW)
  /* note: at least for sync page switch mode, calMaster comes from the FW and is always set to 0xFFFFFFFF, while master is an API parameter coming from the app according to the calling core! */
  VX1000_DUMMYREAD(calMaster) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_DUMMYREAD(master)    /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
#else  /* !VX1000_OVLENBL_CORE_SYNC_PAGESW */
  if (master != calMaster)
  {
    retVal = 3;
  }
  else 
#endif /* !VX1000_OVLENBL_CORE_SYNC_PAGESW */
  if (page == 0)
  {
    VX1000_ATOMIC_AND32((VX1000_UINT32)((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x14UL), ~mask); /* Note that decorated storage is a feature of the PRAM controller and thus SFRs DON'T support it and therefore VX1000_ATOMIC_AND32 MUST be implemented as normal store encapsulated in mutex! */
    checkValue = (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x14UL))[0];   /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    if ((checkValue & mask) != 0)
    {
      retVal = 2;
    }
#if defined(VX1000_FLASHPORTCTLR_BASE_ADDR2)
    VX1000_ATOMIC_AND32((VX1000_UINT32)((VX1000_FLASHPORTCTLR_BASE_ADDR2) + 0x14UL), ~maskB); /* Note that decorated storage is a feature of the PRAM controller and thus SFRs DON'T support it and therefore VX1000_ATOMIC_AND32 MUST be implemented as normal store encapsulated in mutex! */
    checkValue = (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR2) + 0x14UL))[0]; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    if ((checkValue & maskB) != 0)
    {
      retVal = 2;
    }
#endif /* VX1000_FLASHPORTCTLR_BASE_ADDR2 */
  }
  else
  {
    VX1000_ATOMIC_OR32((VX1000_UINT32)((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x14UL), (value & mask)); /* Note that decorated storage is a feature of the PRAM controller and thus SFRs DON'T support it and therefore VX1000_ATOMIC_OR32 MUST be implemented as normal store encapsulated in mutex! */
    checkValue = (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + 0x14UL))[0]; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    if ((checkValue & mask) != (value & mask))
    {
      retVal = 2;
    }
#if defined(VX1000_FLASHPORTCTLR_BASE_ADDR2)
    VX1000_ATOMIC_OR32((VX1000_UINT32)((VX1000_FLASHPORTCTLR_BASE_ADDR2) + 0x14UL), (valueB & maskB)); /* Note that decorated storage is a feature of the PRAM controller and thus SFRs DON'T support it and therefore VX1000_ATOMIC_OR32 MUST be implemented as normal store encapsulated in mutex! */
    checkValue = (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR2) + 0x14UL))[0]; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    if ((checkValue & maskB) != (valueB & maskB))
    {
      retVal = 2;
    }
#endif /* VX1000_FLASHPORTCTLR_BASE_ADDR2 */
  }
#else /* !VX1000_OVERLAY_VX_CONFIGURABLE */
  if (0==(master & calMaster))
  {
    VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,value, mask, valueB, maskB,0)) /* dummy usage of unused parameters to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
    retVal = 3;
  }
  else
  {
    VX1000_ENTER_SPINLOCK()
    checkValue = (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + (0x0108UL + (((VX1000_OVERLAY_DESCR_IDX) & 31) << 4))))[0]; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    if (page == 0)
    {
      checkValue &=~ (master << 16);
    }
    else
    {
      checkValue |= master << 16;
    }
#if ((VX1000_OVERLAY_DESCR_IDX) < 16) /* safe mode */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + (0x0108UL + ((((VX1000_OVERLAY_DESCR_IDX) + 16)) << 4))))[0] = /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + (0x0108UL + ((( VX1000_OVERLAY_DESCR_IDX      )) << 4))))[0] = checkValue; /* this access as a side effect clears PFCRDE.VALID */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + (0x0014UL)))[0] |= (1UL << (31 - ((VX1000_OVERLAY_DESCR_IDX) + 16))) | (1UL << (31 - (VX1000_OVERLAY_DESCR_IDX))); /* PFCRDE -> set descriptor valid again */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else /* VX1000_OVERLAY_DESCR_IDX >= 16 : big mode */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + (0x0108UL + (((VX1000_OVERLAY_DESCR_IDX) & 31) << 4))))[0] = checkValue; /* this access as a side effect clears PFCRDE.VALID */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR) + (0x0014UL)))[0] |= (1UL << (31 - ((VX1000_OVERLAY_DESCR_IDX) & 31))); /* PFCRDE -> set descriptor valid again */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_OVERLAY_DESCR_IDX >= 16 */
#if defined(VX1000_FLASHPORTCTLR_BASE_ADDR2) && defined(VX1000_OVERLAY_DESCR_IDX2)
    checkValue = (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR2) + (0x0108UL + (((VX1000_OVERLAY_DESCR_IDX2) & 31) << 4))))[0]; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    if (page == 0)
    {
      checkValue &=~ (master << 16);
    }
    else
    {
      checkValue |= master << 16;
    }
#if ((VX1000_OVERLAY_DESCR_IDX2) < 16) /* safe mode */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR2) + (0x0108UL + ((((VX1000_OVERLAY_DESCR_IDX2) + 16)) << 4))))[0] = /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR2) + (0x0108UL + ((( VX1000_OVERLAY_DESCR_IDX2      )) << 4))))[0] = checkValue; /* this access as a side effect clears PFCRDE.VALID */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR2) + (0x0014UL)))[0] |= (1UL << (31 - ((VX1000_OVERLAY_DESCR_IDX2) + 16))) | (1UL << (31 - (VX1000_OVERLAY_DESCR_IDX2))); /* PFCRDE -> set descriptor valid again */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else /* VX1000_OVERLAY_DESCR_IDX2 >= 16 : big mode */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR2) + (0x0108UL + (((VX1000_OVERLAY_DESCR_IDX2) & 31) << 4))))[0] = checkValue; /* this access as a side effect clears PFCRDE.VALID */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32((VX1000_FLASHPORTCTLR_BASE_ADDR2) + (0x0014UL)))[0] |= (1UL << (31 - ((VX1000_OVERLAY_DESCR_IDX2) & 31))); /* PFCRDE -> set descriptor valid again */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_OVERLAY_DESCR_IDX2 >= 16 */
#endif /* VX1000_FLASHPORTCTLR_BASE_ADDR2 && VX1000_OVERLAY_DESCR_IDX2 */
    VX1000_LEAVE_SPINLOCK()
  }
#endif /* !VX1000_OVERLAY_VX_CONFIGURABLE */
  return retVal;
}

#elif defined(VX1000_MPC56xCRAM_BASE_ADDR)  /* & !VX1000_FLASHPORTCTLR_BASE_ADDR */
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlaySetConfig                                                                                     */
/* API name:      VX1000_OVL_SET_CONFIG (expression) / VX1000_OVL_SET_CONFIG_VOID (statement) - may be overloaded by user     */
/* Wrapper API:   VX1000If_OvlSetConfigVoid                                                                                   */
/* Return value:  status:                                                                                                     */
/*                0: Page switch done                                                                                         */
/*                1: Nothing done                                                                                             */
/*                2: Value not written correctly                                                                              */
/*                3: No single-master page-switch possible                                                                    */
/*                4: Generic error                                                                                            */
/* Parameter1:    Overlay windows to be activated/deactivated in case of VX-configured overlay (unused otherwise)             */
/* Parameter2:    Resource Mask                                                                                               */
/* Parameter3:    not used                                                                                                    */
/* Parameter4:    not used                                                                                                    */
/* Parameter5:    Overlay Page                                                                                                */
/* Parameter6:    Masters to be activated (dummy)                                                                             */
/* Parameter7:    Masters resource Mask (dummy)                                                                               */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Derivative-specific method for globally enabling/disabling overlays on several MPC5xx family members.       */
/*                Note: the driver assumes exclusive ownership of the CRAM controller.                                        */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlaySetConfig)( VX1000_UINT32 value, VX1000_UINT32 mask, VX1000_UINT32 valueB,
                                                     VX1000_UINT32 maskB, VX1000_UINT8 page, VX1000_UINT32 master,
                                                     VX1000_UINT32 calMaster )
{
  /* Note: Implementation valid only for MPC5xx! */
  volatile VX1000_UINT32 *cram = VX1000_ADDR_TO_PTR2VU32(VX1000_MPC56xCRAM_BASE_ADDR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  VX1000_UINT8 idx, retVal = 0; /* assume OK */
  VX1000_UINT32 checkmask = 0x00000001UL;

  VX1000_DUMMYREAD(valueB) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_DUMMYREAD(maskB) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */

  for (idx = 0; idx < 8; ++idx)        /* turn all 8 windows on or all windows off */
  {
    if ((mask & checkmask) != 0)
    {
      if (page == 0)
      {
        cram[2U + idx] &=~0x70000000UL; /* disable any block remapping in CRAM_RBAx */
      }
      else
      {
        cram[2U + idx] |= 0x70000000UL; /* enable 512-byte block remapping in CRAM_RBAx (other sizes are not supported by the FW) */
      }
    }
    mask ^= checkmask;
    checkmask <<= 1;
  }

  if (mask != 0)
  {
    VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_TOO_MANY)
    retVal = 2; /* more windows requested than supported on this hardware */
    VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,master, calMaster,0,0,0))  /* this code just prevents "unused parameter" warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  }
  return retVal;
}

#else /* !VX1000_MPC56xCRAM_BASE_ADDR && !VX1000_FLASHPORTCTLR_BASE_ADDR */

#if defined(VX1000_OVERLAY_TLB)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlaySetConfig                                                                                     */
/* API name:      VX1000_OVL_SET_CONFIG (expression) / VX1000_OVL_SET_CONFIG_VOID (statement) - may be overloaded by user     */
/* Wrapper API:   VX1000If_OvlSetConfigVoid                                                                                   */
/* Return value:  status:                                                                                                     */
/*                0: Page switch done                                                                                         */
/*                1: Nothing done                                                                                             */
/*                2: Value not written correctly                                                                              */
/*                3: No single-master page-switch possible                                                                    */
/*                4: Generic error                                                                                            */
/* Parameter1:    dummy parameter because VX-configurable overlays are not supported by VX for MMU-based overlays             */
/* Parameter2:    dummy parameter because VX-configurable overlays are not supported by VX for MMU-based overlays             */
/* Parameter3:    not used                                                                                                    */
/* Parameter4:    not used                                                                                                    */
/* Parameter5:    Overlay Page                                                                                                */
/* Parameter6:    Masters to be activated                                                                                     */
/* Parameter7:    Masters resource Mask                                                                                       */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   per-core MMU-based method for enabling/disabling the static, user-defined overlay on some MPC55xx/MPC56xx   */
/*                family members.                                                                                             */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlaySetConfig)( VX1000_UINT32 value, VX1000_UINT32 mask, VX1000_UINT32 valueB,
                                                     VX1000_UINT32 maskB, VX1000_UINT8 page, VX1000_UINT32 master,
                                                     VX1000_UINT32 calMaster )
{
  /* Note: Implementation valid only for MPC55xx/MPC56xx! */
  VX1000_UINT8 retVal = 0; /* assume OK */

  VX1000_DUMMYREAD(valueB) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_DUMMYREAD(maskB) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */

#if defined(VX1000_OVERLAY_VX_CONFIGURABLE)
  if (master != calMaster)
  {
    retVal = 3; /* not called on the core who's MMU shall be reprogrammed */
  }
  else
#endif /* VX1000_OVERLAY_VX_CONFIGURABLE) */
  if (0==(calMaster & (VX1000_OVL_CAL_BUS_MASTER)))
  {
    VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,value, mask,master,calMaster,0)) /* dummy usage of unused parameters to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
    retVal = 1; /* bug in application: API called by a core who is excluded from overlaying by the user's configuration */
  }
  else
  {
    if (page == VX1000_CALPAGE_RAM)
    {
      (void)VX1000_SUFFUN(vx1000_MapCalRam)(VX1000_OVERLAY_SIZE, VX1000_OVERLAY_ADDR, VX1000_CALRAM_ADDR, VX1000_CALRAM_PHYSADDR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
    else
    {
      (void)VX1000_SUFFUN(vx1000_MapCalRam)(VX1000_OVERLAY_SIZE, VX1000_OVERLAY_ADDR, VX1000_OVERLAY_ADDR, VX1000_OVERLAY_PHYSADDR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
  }
  return retVal;
}
#else /* !VX1000_OVERLAY_TLB */
#if defined(VX1000_TARGET_POWERPC)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlaySetConfig                                                                                     */
/* API name:      VX1000_OVL_SET_CONFIG (expression) / VX1000_OVL_SET_CONFIG_VOID (statement) - may be overloaded by user     */
/* Wrapper API:   VX1000If_OvlSetConfigVoid                                                                                   */
/* Return value:  status:                                                                                                     */
/*                0: Page switch done                                                                                         */
/*                1: Nothing done                                                                                             */
/*                2: Value not written correctly                                                                              */
/*                3: No single-master page-switch possible                                                                    */
/*                4: Generic error                                                                                            */
/* Parameter1:    Overlay windows to be activated/deactivated                                                                 */
/* Parameter2:    Resource Mask (active bits' indices must not exceed the derivatives MMU-TLB count!)                         */
/* Parameter3:    not used                                                                                                    */
/* Parameter4:    not used                                                                                                    */
/* Parameter5:    Overlay Page                                                                                                */
/* Parameter6:    Masters to be activated                                                                                     */
/* Parameter7:    Masters resource Mask                                                                                       */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   per-core MMU-based method for enabling/disabling arbitrary overlays on some MPC55xx/MPC56xx family members. */
/* Devel state:   Partially specified                                                                                         */
/*                TODO:  find a way to transmit the base addresses (physical and virtual) and page attribute bits, where      */
/*                       the virtual address would be sufficient if we introduced new callbacks "TLBRE()", "mfMASx()" etc.    */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlaySetConfig)( VX1000_UINT32 value, VX1000_UINT32 mask, VX1000_UINT32 valueB,
                                                     VX1000_UINT32 maskB, VX1000_UINT8 page, VX1000_UINT32 master,
                                                     VX1000_UINT32 calMaster )
{
  /* Note: Implementation valid only for MPC55xx/MPC56xx! */
  VX1000_UINT8 idx, retVal = 0; /* assume OK */
  VX1000_UINT32 checkmask = 0x00000001UL;
  /*const VX1000_UINT32 pagetype = 1U, pageshift = 12U; /-* use 4-Kb-pages which is the minimal size on most MCUs */
  /*const VX1000_UINT8 cache_inhibit = 1;*/
  /*const VX1000_UINT8 cache_writethrough = 0;*/
  /*const VX1000_UINT8 permissions = 0x3F;*/

  VX1000_DUMMYREAD(valueB) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_DUMMYREAD(maskB) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */

  if (master != calMaster)
  {
    retVal = 3; /* not called on the core who's MMU shall be reprogrammed*/
  }
  else
  {
    for (idx = 0; idx < 32; ++idx)
    {
      if ((mask & checkmask) != 0)
      {
        /*VX1000_MPC_MMU_MTMAS0(0x10000000UL | ((VX1000_UINT32)idx << 16U)) /-* Select TLB entry */
        /*VX1000_MPC_MMU_MTMAS1(0x80000000UL | ((VX1000_UINT32)pagetype << 8U))  /-* VALID=1, IPROT=0, TID=0, TS=0, TSIZE=pagetype */
        /* todo EM00046006     VX1000_MPC_MMU_MTMAS2((target) | (cache_inhibit ? 0x8:0) | (cache_writethrough ? 0x10:0))*/ /* EPN = 0x06000 (addr=0x60000000), WIMAGE = xx000, TODO: vle */
        if (page == 0)
        {
          /* todo EM00046006      VX1000_MPC_MMU_MTMAS3((target) | (0x3FU & permissions))*/ /* restore the native page mapping for this TLB */
          VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)
        }
        else
        {
          /* todo EM00046006      VX1000_MPC_MMU_MTMAS3((source) | (0x3FU & permissions))*/ /* enable the overlay page mapping for this TLB */
          VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)
          VX1000_DUMMYREAD(value) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
        }
      }
      /*VX1000_MPC_MMU_TLBWE()  /-* Make pre-charged entry from MAS0..MAS3 to a valid MMU TLB */
      checkmask <<= 1;
    }
  }
  return retVal;
}
#endif /* VX1000_TARGET_POWERPC */
#endif /* !VX1000_OVERLAY_TLB */

#endif /* !VX1000_MPC56xCRAM_BASE_ADDR && !VX1000_FLASHPORTCTLR_BASE_ADDR */

#if defined(VX1000_SH2_FCU_BASE_ADDR)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlaySetConfig                                                                                     */
/* API name:      VX1000_OVL_SET_CONFIG (expression) / VX1000_OVL_SET_CONFIG_VOID (statement) - may be overloaded by user     */
/* Wrapper API:   VX1000If_OvlSetConfigVoid                                                                                   */
/* Return value:  status:                                                                                                     */
/*                0: Page switch done                                                                                         */
/*                1: Nothing done                                                                                             */
/*                2: Value not written correctly                                                                              */
/*                3: No single-master page-switch possible                                                                    */
/*                4: Generic error                                                                                            */
/* Parameter1:    Overlay windows to be activated/deactivated                                                                 */
/* Parameter2:    Resource Mask                                                                                               */
/* Parameter3:    not used                                                                                                    */
/* Parameter4:    not used                                                                                                    */
/* Parameter5:    Overlay Page                                                                                                */
/* Parameter6:    Masters to be activated                                                                                     */
/* Parameter7:    Masters resource Mask                                                                                       */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Derivative-specific method for enabling/disabling overlays.                                                 */
/* Devel state:   Specified                                                                                                   */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlaySetConfig)( VX1000_UINT32 value, VX1000_UINT32 mask, VX1000_UINT32 valueB,
                                                     VX1000_UINT32 maskB, VX1000_UINT8 page, VX1000_UINT32 master,
                                                     VX1000_UINT32 calMaster )
{

  VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,value, mask,page,master,calMaster)) /* dummy usage of unused operands */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_DUMMYREAD(valueB) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_DUMMYREAD(maskB) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */

  VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)

  return 1; /* "not implemented, yet" */
}
#endif /* VX1000_SH2_FCU_BASE_ADDR */

#if defined(VX1000_TARGET_X850)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlaySetConfig                                                                                     */
/* API name:      VX1000_OVL_SET_CONFIG (expression) / VX1000_OVL_SET_CONFIG_VOID (statement) - may be overloaded by user     */
/* Wrapper API:   VX1000If_OvlSetConfigVoid                                                                                   */
/* Return value:  status:                                                                                                     */
/*                0: Page switch done                                                                                         */
/*                1: Nothing done                                                                                             */
/*                2: Value not written correctly                                                                              */
/*                3: No single-master page-switch possible                                                                    */
/*                4: Generic error                                                                                            */
/* Parameter1:    Overlay windows to be activated/deactivated (bank A)                                                        */
/* Parameter2:    Resource Mask (bank A)                                                                                      */
/* Parameter3:    Overlay windows to be activated/deactivated (bank B)                                                        */
/* Parameter4:    Resource Mask (bank B)                                                                                      */
/* Parameter5:    Overlay Page                                                                                                */
/* Parameter6:    Masters to be activated                                                                                     */
/* Parameter7:    Masters resource Mask                                                                                       */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Derivative-specific method for enabling/disabling overlays.                                                 */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
#if defined(VX1000_TARGET_X850_CFU3)
VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlaySetConfig)( VX1000_UINT32 value, VX1000_UINT32 mask, VX1000_UINT32 valueB,
                                                     VX1000_UINT32 maskB, VX1000_UINT8 page, VX1000_UINT32 master,
                                                     VX1000_UINT32 calMaster)
{
  VX1000_UINT8 result;

  if (0 == (calMaster & (VX1000_OVL_CAL_BUS_MASTER)))
  {
    result = 1;
  }
#if defined(VX1000_OVERLAY_VX_CONFIGURABLE)
  else if (0 == (master & calMaster))
  {
    result = 3;
  }
#endif /* VX1000_OVERLAY_VX_CONFIGURABLE */
  else
  {
    (VX1000_ADDR_TO_PTR2VU32(0xFFFF7810UL))[0] = ((VX1000_ADDR_TO_PTR2VU32(0xFFFF7810UL))[0] & (~mask)) | (value & mask);  /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    (VX1000_ADDR_TO_PTR2VU32(0xFFFF7818UL))[0] = ((VX1000_ADDR_TO_PTR2VU32(0xFFFF7818UL))[0] & (~maskB)) | (valueB & maskB);  /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    /* clear cache */
    (VX1000_ADDR_TO_PTR2VU32(0xFFFF7808UL))[0] = 1; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    result = 0;
  }

  return result;
}
#else /* !VX1000_TARGET_X850_CFU3 */
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlaySetConfig                                                                                     */
/* API name:      VX1000_OVL_SET_CONFIG (expression) / VX1000_OVL_SET_CONFIG_VOID (statement) - may be overloaded by user     */
/* Wrapper API:   VX1000If_OvlSetConfigVoid                                                                                   */
/* Return value:  status:                                                                                                     */
/*                0: Page switch done                                                                                         */
/*                1: Nothing done                                                                                             */
/*                2: Value not written correctly                                                                              */
/*                3: No single-master page-switch possible                                                                    */
/*                4: Generic error                                                                                            */
/* Parameter1:    Overlay windows to be activated/deactivated (bank A)                                                        */
/* Parameter2:    Resource Mask (bank A)                                                                                      */
/* Parameter3:    Overlay windows to be activated/deactivated (bank B)                                                        */
/* Parameter4:    Resource Mask (bank B)                                                                                      */
/* Parameter5:    Overlay Page                                                                                                */
/* Parameter6:    Masters to be activated                                                                                     */
/* Parameter7:    Masters resource Mask                                                                                       */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Derivative-specific method for enabling/disabling overlays.                                                 */
/* Devel state:   Specified                                                                                                   */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlaySetConfig)( VX1000_UINT32 value, VX1000_UINT32 mask, VX1000_UINT32 valueB,
                                                     VX1000_UINT32 maskB, VX1000_UINT8 page, VX1000_UINT32 master,
                                                     VX1000_UINT32 calMaster)
{
  VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,value, mask,page,master,calMaster)) /* dummy usage of unused operands */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_DUMMYREAD(valueB) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_DUMMYREAD(maskB) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)

  return 1; /* "not implemented, yet" */
}
#endif /* !VX1000_TARGET_X850_CFU3 */
#endif /* VX1000_TARGET_X850 */

#if defined(VX1000_TARGET_TRICORE)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlaySetConfig                                                                                     */
/* API name:      VX1000_OVL_SET_CONFIG (expression) / VX1000_OVL_SET_CONFIG_VOID (statement) - may be overloaded by user     */
/* Wrapper API:   VX1000If_OvlSetConfigVoid                                                                                   */
/* Return value:  status:                                                                                                     */
/*                0: Page switch done                                                                                         */
/*                1: Nothing done                                                                                             */
/*                2: Value not written correctly                                                                              */
/*                3: No single-master page-switch possible                                                                    */
/*                4: Generic error                                                                                            */
/* Parameter1:    Overlay windows to be activated/deactivated in case of VX-configurable overlays (unused otherwise)          */
/* Parameter2:    Resource Mask in case of VX-configurable overlays (unused otherwise)                                        */
/* Parameter3:    not used                                                                                                    */
/* Parameter4:    not used                                                                                                    */
/* Parameter5:    Overlay Page                                                                                                */
/* Parameter6:    Masters to be activated                                                                                     */
/* Parameter7:    Masters resource Mask                                                                                       */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Derivative-specific method for enabling/disabling overlays.                                                 */
/*                Note: the driver assumes exclusive ownership of the overlay unit.                                           */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlaySetConfig)( VX1000_UINT32 value, VX1000_UINT32 mask, VX1000_UINT32 valueB,
                                                     VX1000_UINT32 maskB, VX1000_UINT8 page, VX1000_UINT32 master,
                                                     VX1000_UINT32 calMaster )
{
  VX1000_UINT8 retVal = 0; /* assume OK */
  VX1000_UINT8 sfrModel = 0U; /* default to classic TriCore registers */
  VX1000_UINT32 existDescr = 0x00000000UL; /* by default assume invalid configuration */
  VX1000_UINT32 masterMask = 0x00000000UL;

  VX1000_DUMMYREAD(valueB) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_DUMMYREAD(maskB) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  switch ((VX1000_MCREG_CBS_JTAGID) & (VX1000_JTAGID_PN_MASK)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  {
    case VX1000_JTAGID_PN_TC172x:       existDescr = 0x0000FFFFUL; masterMask = 0x00000001UL; sfrModel = 0U; break; /* FutureMax : not sure about the sfrModel */
    case VX1000_JTAGID_PN_TC1387:       existDescr = 0x0000FFFFUL; masterMask = 0x00000001UL; sfrModel = 0U; break; /* AudoS */
    case VX1000_JTAGID_PN_TC178x:       existDescr = 0x0000FFFFUL; masterMask = 0x00000001UL; sfrModel = 0U; break; /* AudoMax */
    case VX1000_JTAGID_PN_TC1767:       existDescr = 0x0000FFFFUL; masterMask = 0x00000001UL; sfrModel = 0U; break; /* AudoFuture */
    case VX1000_JTAGID_PN_TC1797:       existDescr = 0x0000FFFFUL; masterMask = 0x00000001UL; sfrModel = 0U; break; /* AudoFuture */
    case VX1000_JTAGID_PN_TC1798:       existDescr = 0x0000FFFFUL; masterMask = 0x00000001UL; sfrModel = 1U; break; /* AudoMax */
    case VX1000_JTAGID_PN_TC21x:        existDescr = 0x000000FFUL; masterMask = 0x00000001UL; sfrModel = 2U; break; /* just a guess: "Aurix single core with 8 descriptors" */
    case VX1000_JTAGID_PN_TC22x:        existDescr = 0x000000FFUL; masterMask = 0x00000001UL; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC23x:        existDescr = 0x000000FFUL; masterMask = 0x00000001UL; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC24x:        existDescr = 0xFFFFFFFFUL; masterMask = 0x00000001UL; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC26x:        existDescr = 0xFFFFFFFFUL; masterMask = 0x00000003UL; sfrModel = 3U; break; /* Aurix dual core */
    case VX1000_JTAGID_PN_TC27xTC2Dx:   existDescr = 0xFFFFFFFFUL; masterMask = 0x00000007UL; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC29x:        existDescr = 0xFFFFFFFFUL; masterMask = 0x00000007UL; sfrModel = 4U; break; /* Aurix triple core */
    /*
    TODOKNM: case VX1000_JTAGID_PN_TC32x:        existDescr = 0xFFFFFFFFUL; masterMask = ?UL; sfrModel = ?U; break; How many cores have we got?
    */
    case VX1000_JTAGID_PN_TC33x:        existDescr = 0xFFFFFFFFUL; masterMask = 0x00000001UL; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC33xED:      existDescr = 0xFFFFFFFFUL; masterMask = 0x00000003UL; sfrModel = 3U; break; /* Aurix dual core */
    case VX1000_JTAGID_PN_TC35x:        existDescr = 0xFFFFFFFFUL; masterMask = 0x00000007UL; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC36x:        existDescr = 0xFFFFFFFFUL; masterMask = 0x00000003UL; sfrModel = 3U; break; /* Aurix dual core */
    case VX1000_JTAGID_PN_TC37x:        existDescr = 0xFFFFFFFFUL; masterMask = 0x00000007UL; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC37xED:      existDescr = 0xFFFFFFFFUL; masterMask = 0x00000007UL; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC38x:        existDescr = 0xFFFFFFFFUL; masterMask = 0x0000000FUL; sfrModel = 5U; break; /* Aurix quad core */
    case VX1000_JTAGID_PN_TC39x:        existDescr = 0xFFFFFFFFUL; masterMask = 0x0000003FUL; sfrModel = 7U; break; /* Aurix hexa core */
    default: break;
  }
#if defined(VX1000_OVERLAY_VX_CONFIGURABLE)
  if (mask != existDescr) /* Ensure that the application did not allocate descriptors for its own purposes. (todo: mask is fetched from gVX1000.Ovl.syncCalData.overlayMask which is initialised to zero once but not updated by FW nor driver) */
  {
    VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_NONEXCLUSIVE)  /* we cannot mess with the global overlay settings if we're not the exclusive owner of all existing descriptors */
    retVal = 4;
  }
  else if ( (master & calMaster) != master )
  {
    retVal = 3;
  }
  else
#else  /* !VX1000_OVERLAY_VX_CONFIGURABLE) */
  VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,value, mask,existDescr,calMaster,retVal)) /* dummy usage of unused operands */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  value = 1UL << (VX1000_OVERLAY_DESCR_IDX);
#endif /* !VX1000_OVERLAY_VX_CONFIGURABLE) */
  {
    /* OK, turn the preconfigured overlays on or off */
    if (sfrModel != 0U)
    {
      VX1000_MCREG_CBS_OCNTRL = 0x000000C0UL; /* unlock the ENDINIT protected registers */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
    if ((sfrModel == 0U) || (sfrModel == 1U))
    {
        VX1000_MCREG_OVC_OENABLE = 0x00000001UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        /* Overlay Configuration and Range Select Register (clear before loading the new configuration): */
        VX1000_MCREG_OVC_OCON = 0x00000000UL | value; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        {
          volatile VX1000_UINT32 dummy;
          for (dummy = 0; dummy < 1000; ++dummy) { VX1000_DUMMYREAD(dummy) } /* writes to OVC_OCON trigger asynchronous actions by the HW and the SW shall not write again while actions are in progress. So wait a bit */
        }
        if (page == 0) { VX1000_MCREG_OVC_OCON = 0x00060000UL | value; } /* DCINVAL|OVSTP  --> display flash */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        else           { VX1000_MCREG_OVC_OCON = 0x00050000UL | value; } /* DCINVAL|OVSTRT --> display RAM in the selected windows */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
    else
    {
      master &= masterMask;
      /* Overlay Window Select Registers and Overlay Configuration Register (note: this implicitly DISABLES ALL OTHER descriptors and en/disables the user-configured descriptor as requested by SET_CAL_PAGE from the tool --> no other overlay users possible in parallel): */
      if ((master & 0x01UL) != 0) { VX1000_MCREG_OVC0_OSEL = value; } /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      if ((master & 0x02UL) != 0) { VX1000_MCREG_OVC1_OSEL = value; } /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      if ((master & 0x04UL) != 0) { VX1000_MCREG_OVC2_OSEL = value; } /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      if ((master & 0x08UL) != 0) { VX1000_MCREG_OVC3_OSEL = value; } /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      if ((master & 0x10UL) != 0) { VX1000_MCREG_OVC4_OSEL = value; } /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      if ((master & 0x20UL) != 0) { VX1000_MCREG_OVC5_OSEL = value; } /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      if (page == 0)              { VX1000_MCREG_SCU_OVCCON = (master & 0xFFFFUL) | 0x00060000UL; } /* DCINVAL|OVSTP|master  --> display flash */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      else                        { VX1000_MCREG_SCU_OVCCON = (master & 0xFFFFUL) | 0x00050000UL; } /* DCINVAL|OVSTRT|master --> display RAM on the selected masters */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
    if (sfrModel != 0U)
    {
      VX1000_MCREG_CBS_OCNTRL = 0x00000040UL; /* restore the ENDINIT protection state */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
  }
  return retVal;
}
#endif /* VX1000_TARGET_TRICORE */

#if defined(VX1000_TARGET_XC2000)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlaySetConfig                                                                                     */
/* API name:      VX1000_OVL_SET_CONFIG (expression) / VX1000_OVL_SET_CONFIG_VOID (statement) - may be overloaded by user     */
/* Wrapper API:   VX1000If_OvlSetConfigVoid                                                                                   */
/* Return value:  status:                                                                                                     */
/*                0: Page switch done                                                                                         */
/*                1: Nothing done                                                                                             */
/*                2: Value not written correctly                                                                              */
/*                3: No single-master page-switch possible                                                                    */
/*                4: Generic error                                                                                            */
/* Parameter1:    Overlay windows to be activated/deactivated                                                                 */
/* Parameter2:    Resource Mask                                                                                               */
/* Parameter3:    not used                                                                                                    */
/* Parameter4:    not used                                                                                                    */
/* Parameter5:    Overlay Page                                                                                                */
/* Parameter6:    Masters to be activated                                                                                     */
/* Parameter7:    Masters resource Mask                                                                                       */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Derivative-specific method for enabling/disabling overlays.                                                 */
/* Devel state:   Specified                                                                                                   */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlaySetConfig)( VX1000_UINT32 value, VX1000_UINT32 mask, VX1000_UINT32 valueB,
                                                     VX1000_UINT32 maskB, VX1000_UINT8 page, VX1000_UINT32 master,
                                                     VX1000_UINT32 calMaster )
{
  VX1000_DUMMYREAD(valueB) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_DUMMYREAD(maskB) /* dummy access to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */

  VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,value, mask,page,master,calMaster)) /* dummy usage of unused operands */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)

  return 1; /* "not implemented, yet" */
}
#endif /* VX1000_TARGET_XC2000 */

#endif /* VX1000_OVL_SET_CONFIG_INTERNAL */

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_overlayIsPageSwitchReq                                                                               */
/* API name:      VX1000_OVL_IS_PAGESW_REQUESTED                                                                              */
/* Wrapper API:   VX1000If_OvlIsPageSwRequested                                                                               */
/* Return value:  status:                                                                                                     */
/*                0: None of the interesting bus masters has a pending request to switch his page                             */
/*                1: At least one of the interesting masters shall switch his page                                            */
/* Parameter1:    A bit mask for up to 32 bus masters whose status the caller is interested in                                */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Returns whether there's a page switch request pending on any of the bus masters selected by the bit mask.   */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_overlayIsPageSwitchReq)(VX1000_UINT32 master)
{
  VX1000_UINT8 i, retVal = 0;
  if (gVX1000.Ovl.syncCalData.pageSwitchRequested != 0)
  {
    for (i = 0; (0==retVal) && (i < 32); i++)
    {
      if ((master & (1UL<<i)) != 0)
      {
        if (gVX1000.Ovl.syncCalData.coreDone[i] == 0)
        {
          retVal = 1;
        }
      }
    }
  }
  return retVal;
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlayChkPageSwitchCore                                                                             */
/* API name:      VX1000_OVL_CHK_PAGESW_CORE (expression) / VX1000_OVL_CHK_PAGESW_CORE_VOID (statement)                       */
/* Wrapper API:   VX1000If_OvlChkPageSwCore (expression) / VX1000If_OvlChkPageSwCoreVoid (statement)                          */
/* Return value:  status:                                                                                                     */
/*                0: All interesting bus masters finished the page switch                                                     */
/*                1: At least one of the interesting masters is still using the wrong page                                    */
/* Parameter1:    A bit mask for up to 32 bus masters whose status the caller is interested in                                */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Checks and remembers the switching status of the bus masters that are selected by the given bit mask.       */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlayChkPageSwitchCore)( VX1000_UINT32 master )
{
  VX1000_UINT32 i;
  VX1000_UINT8 someNotReady = 0;
  VX1000_UINT8 result;
  if (gVX1000.Ovl.syncCalData.pageSwitchRequested != 0)
  {
    for (i = 0; i < 32; i++)
    {
      if ((master & (1UL<<i)) != 0)
      {
        if (gVX1000.Ovl.syncCalData.coreDone[i] == 0)
        {
          someNotReady = 1;
          /*
          Note: core-synchronous page switching not supported on RH850 (only target with > 32 bits overlay configuration bitmaps)
          */
          result = VX1000_OVL_SET_CONFIG(gVX1000.Ovl.syncCalData.overlayValue, gVX1000.Ovl.syncCalData.overlayMask,
                                         gVX1000.Ovl.syncCalData.overlayValueB, gVX1000.Ovl.syncCalData.overlayMaskB,
                                         gVX1000.Ovl.syncCalData.targetPage, master, gVX1000.Ovl.ovlBusMasterMask);
          if (result == 0)
          {
            /* 0 means no error. returning 1 to signal done*/
            gVX1000.Ovl.syncCalData.coreDone[i] = 1;
          }
          else if (result == 1)
          {
            /* 1 is not a valid return value. Use Generic error instead */
            gVX1000.Ovl.syncCalData.coreDone[i] = 2;
          }
          else
          {
            gVX1000.Ovl.syncCalData.coreDone[i] = result;
          }
        }
      }
    }
  }
  return someNotReady;
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OverlayChkPageSwitchDone                                                                             */
/* API name:      VX1000_OVL_CHK_PAGESW_DONE (expression) / VX1000_OVL_CHK_PAGESW_DONE_VOID (statement)                       */
/* Wrapper API:   VX1000If_OvlChkPageSwDone (expression) / VX1000If_OvlChkPageSwDoneVoid (statement)                          */
/* Return value:  0: no page switch in progress                                                                               */
/*                1: switch in progress by some cores                                                                         */
/*                2: some cores reported a page switch error                                                                  */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Checks and remembers the switching status of all bus masters for which a page switch had been requested.    */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlayChkPageSwitchDone)( void )
{
  VX1000_UINT8 i;
  VX1000_UINT8 retVal = 0;
  VX1000_UINT8 someNotReady = 0;
  VX1000_UINT8 someHasErrors = 0;
  VX1000_UINT8 firstError = 0;
  if (gVX1000.Ovl.syncCalData.pageSwitchRequested != 0)
  {
    for (i = 0; i < 32; i++)
    {
      if ((gVX1000.Ovl.syncCalData.busMasterRequested & (1UL<<i)) != 0)
      {
        if (gVX1000.Ovl.syncCalData.coreDone[i] == 0)
        {
          someNotReady = 1;
        }
        else if (gVX1000.Ovl.syncCalData.coreDone[i] == 1)
        {
          /* Switch was executed correctly */
        }
        else
        {
          firstError = gVX1000.Ovl.syncCalData.coreDone[i];
          someHasErrors = 1;
        }
      }
    }
    if (someNotReady == 0)
    {
      retVal = 1;
      gVX1000.Ovl.syncCalData.pageSwitchRequested = 0;
      VX1000_OVL_SET_CONFIG_DONE(firstError, gVX1000.Ovl.syncCalData.targetPage)
    }
    if (someHasErrors != 0)
    {
      retVal = 2;
    }
  }
  return retVal;
}

#endif /* VX1000_OVLENBL_SYNC_PAGESWITCH */

#endif /* VX1000_OVERLAY & VX1000_MAILBOX_OVERLAY_CONTROL */

#if defined(VX1000_OVERLAY) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))

#if defined(VX1000_MAILBOX_OVERLAY_CONTROL)

#if defined(VX1000_GET_CAL_PAGE_INTERNAL)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_GetCalPage                                                                                           */
/* API name:      VX1000_GET_CAL_PAGE (internal) - may be overloaded by user                                                  */
/* Return value:  0: reference page / 1: working page                                                                         */
/* Parameter1:    overlay segment number E {0}                                                                                */
/* Parameter2:    Select between VX1000_CAL_ECU and VX1000_CAL_XCP                                                            */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Finds out whether the ECU or the XCP driver is currently using the working page or the reference page.      */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_GetCalPage)( VX1000_UINT8 segment, VX1000_UINT8 mode )
{
  VX1000_UINT8 retVal = gVX1000_ECU_CalPage;

  if ((mode & ((VX1000_CAL_ECU) | (VX1000_CAL_XCP))) == (VX1000_CAL_ECU))
  {
    retVal = gVX1000_ECU_CalPage;
  }
  else if ((mode & ((VX1000_CAL_ECU) | (VX1000_CAL_XCP))) == (VX1000_CAL_XCP))
  {
    retVal = gVX1000_XCP_CalPage;
  }
  else /* just here for MISRA */
  {
    VX1000_DUMMYREAD(segment) /* just to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  }
  return retVal;
}
#endif /* VX1000_GET_CAL_PAGE_INTERNAL */



#if defined(VX1000_SET_CAL_PAGE_EXTERNAL)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_SetCalPage                                                                                           */
/* API name:      VX1000_WRP_SET_CAL_PAGE, VX1000_WRP_SET_CAL_PAGE_VOID (internal wrapper for user's VX1000_SET_CAL_PAGE)     */
/* Return value:  0: OK; !0: ERROR                                                                                            */
/* Parameter1:    overlay segment number E {0}                                                                                */
/* Parameter2:    Select between reference page and working page                                                              */
/* Parameter3:    Select between VX1000_CAL_ECU and VX1000_CAL_XCP                                                            */
/* Parameter4:    0: called by user action; 1: called internally by vx1000_DetectVxAsyncEnd()                                 */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Call the user's implementation to physically switch the page, then update the driver's status accordingly.  */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_SetCalPage)( VX1000_UINT8 segment, VX1000_UINT8 page, VX1000_UINT8 mode, VX1000_UINT8 onStartup )
{
  VX1000_UINT8 retVal = VX1000_SET_CAL_PAGE(segment, page, mode, onStartup);

  if (retVal != 0)
  {
    /* the user callback returned an error, so assume that the page state was not changed at all. */
    VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_USER_FAILED)
  }
  else if ((mode & ((VX1000_CAL_ECU) | (VX1000_CAL_XCP))) == ((VX1000_CAL_ECU) | (VX1000_CAL_XCP)))
  {
    if (page != gVX1000_ECU_CalPage)
    {
      if (page == VX1000_CALPAGE_RAM)
      {
        gVX1000.ToolDetectState |= VX1000_TDS_WORKING_PAGE;
      }
      else
      {
        gVX1000.ToolDetectState &= (~VX1000_TDS_WORKING_PAGE);
      }
      gVX1000_XCP_CalPage = page;
      gVX1000_ECU_CalPage = page;
    }
  }
  else if ((mode & (VX1000_CAL_ECU)) != 0)
  {
    if (page != gVX1000_ECU_CalPage)
    {
      if (page == VX1000_CALPAGE_RAM)
      {
        gVX1000.ToolDetectState |= VX1000_TDS_WORKING_PAGE;
      }
      else
      {
        gVX1000.ToolDetectState &= (~VX1000_TDS_WORKING_PAGE);
      }
      gVX1000_ECU_CalPage = page;
    }
  }
  else if ((mode & (VX1000_CAL_XCP)) != 0)
  {
    gVX1000_XCP_CalPage = page;
  }
  else /* the empty else case is here only for MISRA */
  {
    VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,onStartup, segment,0,0,0)) /* just to avoid compiler warnings (VX1000_SET_CAL_PAGE() is a user macro that typically does not use the onStartup parameter) */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  }

  return retVal;
}
#endif /* VX1000_SET_CAL_PAGE_EXTERNAL */

#if defined(VX1000_SET_CAL_PAGE_INTERNAL)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_SetCalPage                                                                                           */
/* API name:      VX1000_WRP_SET_CAL_PAGE, VX1000_WRP_SET_CAL_PAGE_VOID (complete internal version, may be overloaded by user)*/
/* Return value:  0: OK; !0: ERROR                                                                                            */
/* Parameter1:    overlay segment number E {0}                                                                                */
/* Parameter2:    Select between reference page and working page                                                              */
/* Parameter3:    Select between VX1000_CAL_ECU and VX1000_CAL_XCP                                                            */
/* Parameter4:    0: called by user action; 1: called internally by vx1000_DetectVxAsyncEnd()                                 */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   perform a page switch to the desired page                                                                   */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_SetCalPage)( VX1000_UINT8 segment, VX1000_UINT8 page, VX1000_UINT8 mode, VX1000_UINT8 onStartup )
{
  VX1000_UINT8 retVal = 0;
#if defined(VX1000_OVLENBL_CORE_SYNC_PAGESW)
  VX1000_UINT8 i;
#endif /* VX1000_OVLENBL_CORE_SYNC_PAGESW */
  if (segment != 0)
  {
    VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_TOO_MANY)
    retVal = VX1000_CRC_OUT_OF_RANGE; /* Only one segment supported */
  }
  else if (page > 1)
  {
    VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_INVALID_PAGE)
    retVal = VX1000_CRC_PAGE_NOT_VALID;
  }
#if defined(VX1000_OVERLAY_VX_CONFIGURABLE)
  else if ((mode & (VX1000_CAL_ECU)) != 0)
  {
    /* Hint: switching the VX1000_CAL_XCP is not relevant in this mode (it is handled by the VX). */

#if defined(VX1000_OVLENBL_VALIDATE_PAGESW)
    if (page != 0)
    {
#if defined(VX1000_OVLENBL_USE_VX_EPK_TRANS)
      retVal = (VX1000_UINT8)(VX1000_SYNCAL_VALIDATE_WP_CB(gVX1000.Ovl.ovlWorkingPageDataEPKAddress));
#else /* !VX1000_OVLENBL_USE_VX_EPK_TRANS */
      retVal = (VX1000_UINT8)(VX1000_SYNCAL_USRVALIDATE_WP_CB(gVX1000.Ovl.ovlConfigValue, gVX1000.Ovl.ovlConfigMask, gVX1000.Ovl.ovlBusMasterMask));
#endif /* !VX1000_OVLENBL_USE_VX_EPK_TRANS */
    }
    if (retVal != 0)
    {
      ; /* do nothing any more */
    }
    else
#endif /* VX1000_OVLENBL_VALIDATE_PAGESW */

#if defined(VX1000_OVLENBL_CORE_SYNC_PAGESW)
    if (0==onStartup)
    {
      gVX1000.Ovl.syncCalData.pageSwitchRequested = 0;
      gVX1000.Ovl.syncCalData.overlayValue = gVX1000.Ovl.ovlConfigValue;
      gVX1000.Ovl.syncCalData.overlayValueB = gVX1000.Ovl.ovlConfigValueB;
      gVX1000.Ovl.syncCalData.overlayMask = gVX1000.Ovl.ovlConfigMask;
      gVX1000.Ovl.syncCalData.overlayMaskB = gVX1000.Ovl.ovlConfigMaskB;
      gVX1000.Ovl.syncCalData.targetPage = page;
      gVX1000.Ovl.syncCalData.busMasterRequested = gVX1000.Ovl.ovlBusMasterMask;
      for (i = 0; i < 32; i++)
      {
        gVX1000.Ovl.syncCalData.coreDone[i] = 0;
      }
      VX1000_DUMMYREAD(gVX1000.Ovl.syncCalData.pageSwitchRequested) /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
      gVX1000.Ovl.syncCalData.pageSwitchRequested = 1;
      /* The actual switch is done asynchronously */
      retVal = VX1000_CRC_CMD_BUSY;
    }
    else
    {
      retVal = VX1000_OVL_SET_CONFIG(gVX1000.Ovl.ovlConfigValue, gVX1000.Ovl.ovlConfigMask, gVX1000.Ovl.ovlConfigValueB, gVX1000.Ovl.ovlConfigMaskB, page, gVX1000.Ovl.ovlBusMasterMask,  gVX1000.Ovl.ovlBusMasterMask);
      VX1000_OVL_SET_CONFIG_DONE_STUP(retVal, page) /* the STUP version suppresses generation of a MX response */
    }
#else /* !VX1000_OVLENBL_CORE_SYNC_PAGESW */
    {
#if defined(VX1000_OVLENBL_SYNC_PAGESWITCH)
      retVal = VX1000_OVL_SET_CONFIG(gVX1000.Ovl.ovlConfigValue, gVX1000.Ovl.ovlConfigMask, gVX1000.Ovl.ovlConfigValueB, gVX1000.Ovl.ovlConfigMaskB, page, gVX1000.Ovl.ovlBusMasterMask, gVX1000.Ovl.ovlBusMasterMask);
      VX1000_OVL_SET_CONFIG_DONE_STUP(retVal, page) /* the STUP version suppresses generation of a MX response */
#else /* !VX1000_OVLENBL_SYNC_PAGESWITCH */
      retVal = 1;
#endif /* !VX1000_OVLENBL_SYNC_PAGESWITCH */
    }
#endif /* !VX1000_OVLENBL_CORE_SYNC_PAGESW */
  }
#else /* !VX1000_OVERLAY_VX_CONFIGURABLE */
#if (defined(VX1000_OVERLAY_TLB) || defined(VX1000_OVERLAY_DESCR_IDX)) || (defined(VX1000_MPC56xCRAM_BASE_ADDR) || defined(VX1000_SH2_FCU_BASE_ADDR))
  else if ((mode & ((VX1000_CAL_ECU) | (VX1000_CAL_XCP))) == ((VX1000_CAL_ECU) | (VX1000_CAL_XCP)))
  { /* switch both the XCP and the ECU view */
#if defined(VX1000_OVLENBL_CORE_SYNC_PAGESW)
    if (0==onStartup)
    {
      gVX1000.Ovl.syncCalData.pageSwitchRequested = 0;
      gVX1000.Ovl.syncCalData.overlayValue = gVX1000.Ovl.ovlConfigValue;
      gVX1000.Ovl.syncCalData.overlayValueB = gVX1000.Ovl.ovlConfigValueB;
      gVX1000.Ovl.syncCalData.overlayMask = gVX1000.Ovl.ovlConfigMask;
      gVX1000.Ovl.syncCalData.overlayMaskB = gVX1000.Ovl.ovlConfigMaskB;
      gVX1000.Ovl.syncCalData.targetPage = page;
      gVX1000.Ovl.syncCalData.busMasterRequested = gVX1000.Ovl.ovlBusMasterMask;
      for (i = 0; i < 32; i++)
      {
        gVX1000.Ovl.syncCalData.coreDone[i] = 0;
      }
      VX1000_DUMMYREAD(gVX1000.Ovl.syncCalData.pageSwitchRequested) /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
      gVX1000.Ovl.syncCalData.pageSwitchRequested = 1;
      /* The actual switch is done asynchronously */
      retVal = VX1000_CRC_CMD_BUSY;
    }
    else
    {
#if defined(VX1000_OVERLAY_DESCR_IDX)
      /* Pre-initialise the PFLASH controller overlay windows but do not activate them for any bus-master, yet. */
      /* The particular bus masters may just (de-)activate them later on demand. (We have to assume that no one */
      /* uses this feature on a McKinleyCut1 chip because this one lacks the ability do deactivate masters!)    */
      /* Therefore neither the page status variables nor the ToolDetectState bits are to be updated here.       */
      (void)VX1000_SUFFUN(vx1000_MapCalRam)(VX1000_OVERLAY_SIZE, VX1000_OVERLAY_ADDR, VX1000_CALRAM_ADDR, VX1000_CALRAM_PHYSADDR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#endif /* VX1000_OVERLAY_DESCR_IDX */
      retVal = VX1000_OVL_SET_CONFIG(gVX1000.Ovl.ovlConfigValue, gVX1000.Ovl.ovlConfigMask, gVX1000.Ovl.ovlConfigValueB, gVX1000.Ovl.ovlConfigMaskB, page, gVX1000.Ovl.ovlBusMasterMask,  gVX1000.Ovl.ovlBusMasterMask);
      VX1000_OVL_SET_CONFIG_DONE_STUP(retVal, page) /* the STUP version suppresses generation of a MX response */
    }
#elif defined(VX1000_OVLENBL_SYNC_PAGESWITCH)
    retVal = VX1000_OVL_SET_CONFIG(gVX1000.Ovl.ovlConfigValue, gVX1000.Ovl.ovlConfigMask, gVX1000.Ovl.ovlConfigValueB, gVX1000.Ovl.ovlConfigMaskB, page, gVX1000.Ovl.ovlBusMasterMask,  gVX1000.Ovl.ovlBusMasterMask);
    VX1000_OVL_SET_CONFIG_DONE_STUP(retVal, page)  /* the STUP version suppresses generation of a MX response */
#else  /* !VX1000_OVLENBL_CORE_SYNC_PAGESW && !VX1000_OVLENBL_SYNC_PAGESWITCH */
    if (page != gVX1000_ECU_CalPage)
    { /* configuration changed */
      if (page == VX1000_CALPAGE_RAM)
      { /* RAM */
        (void)VX1000_SUFFUN(vx1000_MapCalRam)(VX1000_OVERLAY_SIZE, VX1000_OVERLAY_ADDR, VX1000_CALRAM_ADDR, VX1000_CALRAM_PHYSADDR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        gVX1000.ToolDetectState |= VX1000_TDS_WORKING_PAGE;
      }
      else
      { /* Flash */
        (void)VX1000_SUFFUN(vx1000_MapCalRam)(VX1000_OVERLAY_SIZE, VX1000_OVERLAY_ADDR, VX1000_OVERLAY_ADDR, VX1000_OVERLAY_PHYSADDR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        gVX1000.ToolDetectState &= (~VX1000_TDS_WORKING_PAGE);
      }
      gVX1000_ECU_CalPage = page;
    }
    gVX1000_XCP_CalPage = page;
#endif /* !VX1000_OVLENBL_CORE_SYNC_PAGESW && !VX1000_OVLENBL_SYNC_PAGESWITCH */
  }
  else if ((mode & (VX1000_CAL_ECU)) != 0)
  { /* switch only ECU view */
#if defined(VX1000_OVLENBL_CORE_SYNC_PAGESW)
    if (0==onStartup)
    {
      gVX1000.Ovl.syncCalData.pageSwitchRequested = 0;
      gVX1000.Ovl.syncCalData.overlayValue = gVX1000.Ovl.ovlConfigValue;
      gVX1000.Ovl.syncCalData.overlayValueB = gVX1000.Ovl.ovlConfigValueB;
      gVX1000.Ovl.syncCalData.overlayMask = gVX1000.Ovl.ovlConfigMask;
      gVX1000.Ovl.syncCalData.overlayMaskB = gVX1000.Ovl.ovlConfigMaskB;
      gVX1000.Ovl.syncCalData.targetPage = page;
      gVX1000.Ovl.syncCalData.busMasterRequested = gVX1000.Ovl.ovlBusMasterMask;
      for (i = 0; i < 32; i++)
      {
        gVX1000.Ovl.syncCalData.coreDone[i] = 0;
      }
      VX1000_DUMMYREAD(gVX1000.Ovl.syncCalData.pageSwitchRequested) /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
      gVX1000.Ovl.syncCalData.pageSwitchRequested = 1;
      /* The actual switch is done asynchronously */
      retVal = VX1000_CRC_CMD_BUSY;
    }
    else
    {
#if defined(VX1000_OVERLAY_DESCR_IDX)
      /* Pre-initialise the PFLASH controller overlay windows but do not activate them for any bus-master, yet. */
      /* The particular bus masters may just (de-)activate them later on demand. (We have to assume that no one */
      /* uses this feature on a McKinleyCut1 chip because this one lacks the ability do deactivate masters!)    */
      /* Therefore neither the page status variables nor the ToolDetectState bits are to be updated here.       */
/*
      (void)VX1000_SUFFUN(vx1000_MapCalRam)(VX1000_OVERLAY_SIZE, VX1000_OVERLAY_ADDR, VX1000_CALRAM_ADDR, VX1000_CALRAM_PHYSADDR);
*/
#endif /* VX1000_OVERLAY_DESCR_IDX */
      retVal = VX1000_OVL_SET_CONFIG(gVX1000.Ovl.ovlConfigValue, gVX1000.Ovl.ovlConfigMask, gVX1000.Ovl.ovlConfigValueB, gVX1000.Ovl.ovlConfigMaskB, page, gVX1000.Ovl.ovlBusMasterMask,  gVX1000.Ovl.ovlBusMasterMask);
      VX1000_OVL_SET_CONFIG_DONE_STUP(retVal, page) /* the STUP version suppresses generation of a MX response */
    }
#elif defined(VX1000_OVLENBL_SYNC_PAGESWITCH)
    retVal = VX1000_OVL_SET_CONFIG(gVX1000.Ovl.ovlConfigValue, gVX1000.Ovl.ovlConfigMask, gVX1000.Ovl.ovlConfigValueB, gVX1000.Ovl.ovlConfigMaskB, page, gVX1000.Ovl.ovlBusMasterMask,  gVX1000.Ovl.ovlBusMasterMask);
    VX1000_OVL_SET_CONFIG_DONE_STUP(retVal, page)  /* the STUP version suppresses generation of a MX response */
#else  /* !VX1000_OVLENBL_CORE_SYNC_PAGESW && !VX1000_OVLENBL_SYNC_PAGESWITCH */
    if (page != gVX1000_ECU_CalPage)
    { /* configuration changed */
      if (page == VX1000_CALPAGE_RAM)
      { /* RAM */
        (void)VX1000_SUFFUN(vx1000_MapCalRam)(VX1000_OVERLAY_SIZE, VX1000_OVERLAY_ADDR, VX1000_CALRAM_ADDR, VX1000_CALRAM_PHYSADDR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        gVX1000.ToolDetectState |= VX1000_TDS_WORKING_PAGE;
      }
      else
      { /* Flash */
        (void)VX1000_SUFFUN(vx1000_MapCalRam)(VX1000_OVERLAY_SIZE, VX1000_OVERLAY_ADDR, VX1000_OVERLAY_ADDR, VX1000_OVERLAY_PHYSADDR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        gVX1000.ToolDetectState &= (~VX1000_TDS_WORKING_PAGE);
      }
      gVX1000_ECU_CalPage = page;
    }
#endif /* !VX1000_OVLENBL_CORE_SYNC_PAGESW && !VX1000_OVLENBL_SYNC_PAGESWITCH */
  }
  else if ((mode & (VX1000_CAL_XCP)) != 0)
  { /* switch only XCP view */
    gVX1000_XCP_CalPage = page;
  }
#endif /* VX1000_OVERLAY_TLB | VX1000_OVERLAY_DESCR_IDX | VX1000_MPC56xCRAM_BASE_ADDR | VX1000_SH2_FCU_BASE_ADDR */
#endif /* !VX1000_OVERLAY_VX_CONFIGURABLE */
  else /* the empty else case is here only for MISRA */
  {
    VX1000_DUMMYREAD(onStartup) /* just to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  }

  return retVal;
}
#endif /* VX1000_SET_CAL_PAGE_INTERNAL */


#if defined(VX1000_COPY_CAL_PAGE_INTERNAL)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_CopyCalPage                                                                                          */
/* API name:      VX1000_COPY_CAL_PAGE (internal) - may be overloaded by user                                                 */
/* Return value:                                                                                                              */
/* Parameter1:                                                                                                                */
/* Parameter2:                                                                                                                */
/* Parameter3:                                                                                                                */
/* Parameter4:                                                                                                                */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   Copy the content of the reference page to the working page.                                                 */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_CopyCalPage)( VX1000_UINT8 srcSeg, VX1000_UINT8 srcPage, VX1000_UINT8 dstSeg, VX1000_UINT8 dstPage )
{
#if (defined(VX1000_OVERLAY_TLB) || defined(VX1000_OVERLAY_DESCR_IDX)) || (defined(VX1000_MPC56xCRAM_BASE_ADDR) || defined(VX1000_SH2_FCU_BASE_ADDR))
  const VX1000_UINT32 overlaySize = (VX1000_UINT32)(VX1000_OVERLAY_SIZE); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  const VX1000_UINT32 overlayAddr = (VX1000_UINT32)(VX1000_OVERLAY_ADDR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  const VX1000_UINT32 calRamAddr  = (VX1000_UINT32)(VX1000_CALRAM_ADDR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  VX1000_UINT8 retVal = 0;
  /* Check parameters */
  /* Only copy from RAM to FLASH makes sense */
  if ((srcSeg | dstSeg) != 0) { retVal = VX1000_CRC_SEGMENT_NOT_VALID; /* Segments are not supported */ }
  else if (dstPage == srcPage) { retVal = VX1000_CRC_PAGE_NOT_VALID; /* Can not copy on itself */ }
  else if (0==dstPage) { retVal = VX1000_CRC_ACCESS_DENIED; /* Can not copy to FLASH page  */ }
  /* Initialise CALRAM */
  /* Initialise CALRAM, copy from FLASH to RAM */
  else
  {
    if (gVX1000_ECU_CalPage == (VX1000_CALPAGE_RAM))
    {
      /* To keep code generic, turn mapping temporarily off (even if not needed with e.g. VX1000_OVERLAY_DESCR_IDX which could use the shadow address range) */
      VX1000_WRP_SET_CAL_PAGE_VOID(0, VX1000_CALPAGE_FLASH, VX1000_CAL_ECU, /* VX1000_COPY_CAL_PAGE is called by the mailbox, not at start-up: */0)
    }
    if ((((overlaySize | overlayAddr) | calRamAddr) & 0x3UL) != 0UL)
    {
      VX1000_UINT32 cnt;
      volatile const VX1000_CHAR *pSrc = VX1000_ADDR_TO_PTR2VCC(overlayAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      volatile VX1000_CHAR *pDst = VX1000_ADDR_TO_PTR2VC(calRamAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      for (cnt = 0; cnt < overlaySize; cnt++)
      {
        pDst[cnt] = pSrc[cnt];
      }
    }
    else
    {
      VX1000_UINT32 cnt;
      volatile const VX1000_UINT32 *pSrc = VX1000_ADDR_TO_PTR2VCU32(overlayAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      volatile VX1000_UINT32 *pDst = VX1000_ADDR_TO_PTR2VU32(calRamAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      for (cnt = 0; cnt < (overlaySize >> 2U); cnt++)
      {
        pDst[cnt] = pSrc[cnt];
      }
    }
    if (gVX1000_ECU_CalPage == (VX1000_CALPAGE_RAM))
    {
      VX1000_WRP_SET_CAL_PAGE_VOID(0, VX1000_CALPAGE_RAM, VX1000_CAL_ECU, /* VX1000_COPY_CAL_PAGE is called by the mailbox, not at start-up: */0)
    }
  }
#else /* !VX1000_OVERLAY_TLB & !VX1000_OVERLAY_DESCR_IDX & !VX1000_MPC56xCRAM_BASE_ADDR & !VX1000_SH2_FCU_BASE_ADDR */
  /* CopyCalPage has to be done by the VX in this mode (note: on TMS570 from a synchronised DPRAM because active reading is not supported, yet!) */
  VX1000_UINT8 retVal = 1 /* TODO: use an appropriate error code define! */;
  VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,srcSeg, dstSeg,srcPage,dstPage,0)) /* dummy accesses to prevent compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
#if defined(VX1000_TARGET_XC2000)
  VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)
#endif /* VX1000_TARGET_XC2000 */
#endif /* !VX1000_OVERLAY_TLB & !VX1000_OVERLAY_DESCR_IDX & !VX1000_MPC56xCRAM_BASE_ADDR & !VX1000_SH2_FCU_BASE_ADDR */
  return retVal;
}
#endif /* VX1000_COPY_CAL_PAGE_INTERNAL */


#endif /* VX1000_MAILBOX_OVERLAY_CONTROL */


#endif /* VX1000_OVERLAY && !VX1000_COMPILED_FOR_SLAVECORES */


/*---------------------------------------------------------------------------- */
/* Mailbox */
#if defined(VX1000_MAILBOX) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))

#define VX1000_MAILBOX_FLG_CLR                0
#define VX1000_MAILBOX_FLG_SET                1

#if defined(VX1000_MAILBOX_PROVIDE_SPLITREAD)
static VX1000_UINT32 sFlgRdSplitPend;
#endif /* VX1000_MAILBOX_PROVIDE_SPLITREAD */
#if defined(VX1000_MAILBOX_PROVIDE_SPLITWRITE)
static VX1000_UINT32 sFlgWrSplitPend;
#endif /* VX1000_MAILBOX_PROVIDE_SPLITWRITE */


/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MailboxInit                                                                                          */
/* API name:      None                                                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:    This function must not be interrupted by any vx1000 mailbox function.                                       */
/*                This function must not interrupt any vx1000 mailbox function.                                               */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_InitAsyncStart must have been called.                                                                */
/* Description:   Initialise the Master->Slave and Slave->Master mailboxes.                                                   */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
static void VX1000_SUFFUN(vx1000_MailboxInit)(void)
{
#if defined(VX1000_MAILBOX_PROVIDE_SPLITREAD)
  sFlgRdSplitPend = VX1000_MAILBOX_FLG_CLR;
#endif /* VX1000_MAILBOX_PROVIDE_SPLITREAD */
#if defined(VX1000_MAILBOX_PROVIDE_SPLITWRITE)
  sFlgWrSplitPend = VX1000_MAILBOX_FLG_CLR;
#endif /* VX1000_MAILBOX_PROVIDE_SPLITWRITE */

  gVX1000.Mailbox.Version = 0;
  gVX1000.Mailbox.SlotSize = (VX1000_MAILBOX_SLOT_DWORDS) << 2;
  gVX1000.Mailbox.MS_ReadIdx = 0;
  gVX1000.Mailbox.MS_WriteIdx = 0;
  gVX1000.Mailbox.SM_ReadIdx = 0;
  gVX1000.Mailbox.SM_WriteIdx = 0;
  gVX1000.Mailbox.RstReq = 0;
  gVX1000.Mailbox.RstAck = 0;
  gVX1000.Mailbox.MS_Slots = VX1000_MAILBOX_SLOTS;
  gVX1000.Mailbox.SM_Slots = VX1000_MAILBOX_SLOTS;
  gVX1000.Mailbox.MS_Ptr = VX1000_PTR2VU32_TO_ADDRESS(gVX1000.Mailbox.MSData); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  gVX1000.Mailbox.SM_Ptr = VX1000_PTR2VU32_TO_ADDRESS(gVX1000.Mailbox.SMData); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  if ( (VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.Mailbox.MS_Ptr)) != (0xCUL + VX1000_PTR2VM_TO_ADDRESS(&gVX1000.Mailbox)) ) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  {
    gVX1000.ToolDetectState |= VX1000_TDS_ERROR;
    VX1000_ERRLOGGER(VX1000_ERRLOG_STRUCTS_PADDED)
  }
  gVX1000.Mailbox.MagicId = (VX1000_UINT32)VX1000_MAILBOX_MAGIC;
}


/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MailboxWrite                                                                                         */
/* API name:      VX1000_MAILBOX_WRITE (expression) / VX1000_MAILBOX_WRITE_VOID (statement)                                   */
/* Wrapper API:   VX1000If_MailboxWrite (expression) / VX1000If_MailboxWriteVoid (statement)                                  */
/* Return value:  Error code (0==OK)                                                                                          */
/* Parameter1:    len: message size in bytes.                                                                                 */
/* Parameter2:    pBuf: pointer to message data input.                                                                        */
/*                The caller is responsible that the pointer is valid and that the source memory is accessible.               */
/* Preemption:    This function must not be interrupted by any vx1000 mailbox write function.                                 */
/*                This function must not interrupt any vx1000 mailbox write function.                                         */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_MailboxInit() must have been called.                                                                 */
/* Description:   Write len bytes from pBuf to the Slave->Master mailbox and notify the master.                               */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT32 VX1000_SUFFUN(vx1000_MailboxWrite)(VX1000_UINT32 len, const VX1000_CHAR *pBuf)
{
  VX1000_UINT32 retVal = VX1000_MAILBOX_OK;

  if (0U==(VX1000_MAILBOX_FREE_WR_SLOTS))
  {
    retVal = VX1000_MAILBOX_ERR_FULL;
  }
  else if (0==pBuf)
  {
    retVal = VX1000_MAILBOX_ERR_NULL;
  }
  else if ((len + 4) > ((VX1000_MAILBOX_SLOT_DWORDS) << 2))
  {
    retVal = VX1000_MAILBOX_ERR_SIZE;
  }
  else
  {
    VX1000_UINT32 cnt;
    volatile VX1000_CHAR *pDst = VX1000_ADDR_TO_PTR2VC(VX1000_PTR2VU8_TO_ADDRESS(&gVX1000.Mailbox.SMData[gVX1000.Mailbox.SM_WriteIdx][1])); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    for (cnt = 0; cnt < len; cnt++)
    {
      pDst[cnt] = pBuf[cnt];
    }
    gVX1000.Mailbox.SMData[gVX1000.Mailbox.SM_WriteIdx][0] = len;
    gVX1000.Mailbox.SM_WriteIdx = (VX1000_UINT8)VX1000_MAILBOX_NEXT(gVX1000.Mailbox.SM_WriteIdx);
    VX1000_SPECIAL_EVENT(VX1000_EVENT_MAILBOX_UPDATE) /* note: EM00034754 did not mention to remove THIS one */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  }
  return retVal;
}


#if defined(VX1000_MAILBOX_PROVIDE_SPLITWRITE)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MailboxWriteSplit                                                                                    */
/* API name:      VX1000_MAILBOX_WRITESPLIT (expression) / VX1000_MAILBOX_WRITESPLIT_VOID (statement)                         */
/* Wrapper API:   VX1000If_MailboxWriteSplit (expression) / VX1000If_MailboxWriteSplitVoid (statement)                        */
/* Return value:  Error code (0==OK)                                                                                          */
/*                Additional data returned via pointer                                                                        */
/* Parameter1:    ppBuf (IN): pointer to a pointer variable.                                                                  */
/*                ppBuf (*OUT): pointer to the data field of the next free Slave->Master mailbox.                             */
/* Preemption:    This function must not be interrupted by any vx1000 mailbox write function.                                 */
/*                This function must not interrupt any vx1000 mailbox write function.                                         */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_MailboxInit() must have been called.                                                                 */
/* Description:   Just find out the location of the next unused message buffer and return the info to the caller.             */
/*                Note: the mailbox state is not changed nor is the master notified (vx1000_MailboxWriteDone will do this).   */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT32 VX1000_SUFFUN(vx1000_MailboxWriteSplit)(VX1000_UINT32 * * ppBuf)
{
  VX1000_UINT32 retVal = VX1000_MAILBOX_OK;

  if (0U==(VX1000_MAILBOX_FREE_WR_SLOTS))
  {
    retVal = VX1000_MAILBOX_ERR_FULL;
  }
  else if (0==ppBuf)
  {
    retVal = VX1000_MAILBOX_ERR_NULL;
  }
  else if (sFlgWrSplitPend != 0)
  {
    retVal = VX1000_MAILBOX_ERR_SPLIT_PEND;
  }
  else
  {
    sFlgWrSplitPend = VX1000_MAILBOX_FLG_SET;
    ppBuf[0] = VX1000_ADDR_TO_PTR2U32(VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.Mailbox.SMData[gVX1000.Mailbox.SM_WriteIdx][1])); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  }
  return retVal;
}


/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MailboxWriteDone                                                                                     */
/* API name:      VX1000_MAILBOX_WRITEDONE (expression) / VX1000_MAILBOX_WRITEDONE_VOID (statement)                           */
/* Wrapper API:   VX1000If_MailboxWriteDone (expression) / VX1000If_MailboxWriteDoneVoid (statement)                          */
/* Return value:  error code (0==OK)                                                                                          */
/* Parameter1:    len: the size of the entire message in bytes.                                                               */
/* Preemption:    This function must not be interrupted by any vx1000 mailbox write function.                                 */
/*                This function must not interrupt any vx1000 mailbox write function.                                         */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_MailboxWriteSplit() must have been called successfully.                                              */
/* Description:   Finish a Slave->Master mailbox transfer that has been started with vx1000_MailboxWriteSplit().              */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT32 VX1000_SUFFUN(vx1000_MailboxWriteDone)(VX1000_UINT32 len)
{
  VX1000_UINT32 retVal = VX1000_MAILBOX_OK;

  if (len > ((VX1000_MAILBOX_SLOT_DWORDS) << 2))
  {
    retVal = VX1000_MAILBOX_ERR_SIZE;
  }
  else if (0==sFlgWrSplitPend)
  {
    retVal = VX1000_MAILBOX_ERR_SPLIT_PEND;
  }
  else
  {
    sFlgWrSplitPend = VX1000_MAILBOX_FLG_CLR;
    gVX1000.Mailbox.SMData[gVX1000.Mailbox.SM_WriteIdx][0] = len;
    gVX1000.Mailbox.SM_WriteIdx = (VX1000_UINT8)VX1000_MAILBOX_NEXT(gVX1000.Mailbox.SM_WriteIdx);
    VX1000_SPECIAL_EVENT(VX1000_EVENT_MAILBOX_UPDATE) /* note: EM00034754 did not mention to remove THIS one */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* macro expansion contains VX1000_ADDR_TO_PTR2XXX: cannot avoid violating MISRA rule 11.3 because in a few configurations a peripheral register at a fix HW address must be accessed via pointer to update the HW value */
  }
  return retVal;
}
#endif /* VX1000_MAILBOX_PROVIDE_SPLITWRITE */

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MailboxRead                                                                                          */
/* API name:      VX1000_MAILBOX_READ (expression) / VX1000_MAILBOX_READ_VOID (statement)                                     */
/* Wrapper API:   VX1000If_MailboxRead (expression) / VX1000If_MailboxReadVoid (statement)                                    */
/* Return value:  Error code (0==OK)                                                                                          */
/*                Additional data returned via pointers                                                                       */
/* Parameter1:    pLen (*IN): maximum allowed message size                                                                    */
/*                pLen (*OUT): actual message size if successful                                                              */
/* Parameter2:    pBuf (IN): destination for the next message                                                                 */
/*                The caller is responsible that the destination contains at least *pLen writeable bytes.                     */
/*                The function aborts with an error if the buffer is too small for the current message (no bytes copied).     */
/*                pBuf (*OUT): a copy of the entire message if successful.                                                    */
/* Preemption:    This function must not be interrupted by any vx1000 mailbox read function.                                  */
/*                This function must not interrupt any vx1000 mailbox read function.                                          */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_MailboxInit() must have been called.                                                                 */
/* Description:   Read the data from next filled Master->Slave mailbox slot into pBuf and return the number of bytes in pLen. */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT32 VX1000_SUFFUN(vx1000_MailboxRead)(VX1000_UINT32 * pLen, VX1000_CHAR * pBuf)
{
  VX1000_UINT32 len;
  VX1000_UINT32 retVal = VX1000_MAILBOX_OK;

  if (0==(VX1000_MAILBOX_USED_RD_SLOTS))
  {
    retVal = VX1000_MAILBOX_ERR_EMPTY;
  }
  else if ((0==pLen) || (0==pBuf))
  {
    retVal = VX1000_MAILBOX_ERR_NULL;
  }
  else
  {
    len = gVX1000.Mailbox.MSData[gVX1000.Mailbox.MS_ReadIdx][0];
    if (len > *pLen)
    {
      retVal = VX1000_MAILBOX_ERR_SIZE;
    }
#if defined(VX1000_MAILBOX_PROVIDE_SPLITREAD)
    else if (sFlgRdSplitPend != 0)
    {
      retVal = VX1000_MAILBOX_ERR_SPLIT_PEND;
    }
#endif /* VX1000_MAILBOX_PROVIDE_SPLITREAD */
    else
    {
      VX1000_UINT32 cnt;
      volatile const VX1000_CHAR *pSrc = VX1000_ADDR_TO_PTR2VCC(VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.Mailbox.MSData[gVX1000.Mailbox.MS_ReadIdx][1])); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      for (cnt = 0; cnt < len; cnt++)
      {
        pBuf[cnt] = pSrc[cnt];
      }
      *pLen = len;
      gVX1000.Mailbox.MS_ReadIdx = (VX1000_UINT8)VX1000_MAILBOX_NEXT(gVX1000.Mailbox.MS_ReadIdx);
      /* VX1000_SPECIAL_EVENT(VX1000_EVENT_MAILBOX_UPDATE) -- removed due to EM00034754 */
    }
  }
  return retVal;
}


#if defined(VX1000_MAILBOX_PROVIDE_SPLITREAD)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MailboxReadSplit                                                                                     */
/* API name:      VX1000_MAILBOX_READSPLIT (expression) / VX1000_MAILBOX_READSPLIT_VOID (statement)                           */
/* Wrapper API:   VX1000If_MailboxReadSplit (expression) / VX1000If_MailboxReadSplitVoid (statement)                          */
/* Return value:  Error code (0==OK)                                                                                          */
/*                Additional data returned via pointers                                                                       */
/* Parameter1:    pLen (IN): a pointer to a 32bit variable.                                                                   */
/*                pLen (*OUT): the byte count of the next message.                                                            */
/*                The caller is responsible that the pointer is valid and that the destination is writeable.                  */
/* Parameter1:    ppBuf (IN): a pointer to a pointer variable.                                                                */
/*                ppBuf (*OUT): pointer to the data field of the next unread message.                                         */
/*                The caller is responsible that the pointer is valid and that the destination is writeable.                  */
/* Preemption:    This function must not be interrupted by any vx1000 mailbox read function.                                  */
/*                This function must not interrupt any vx1000 mailbox read function.                                          */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_MailboxInit() must have been called.                                                                 */
/* Description:   Just find out location and length of the next unread message and return the info to the caller.             */
/*                Note: the mailbox state is not changed nor is the master notified (vx1000_MailboxReadDone will do this).    */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT32 VX1000_SUFFUN(vx1000_MailboxReadSplit)(VX1000_UINT32 * pLen, VX1000_UINT32 * * ppBuf)
{
  VX1000_UINT32 retVal = VX1000_MAILBOX_OK;

  if (0==(VX1000_MAILBOX_USED_RD_SLOTS))
  {
    retVal = VX1000_MAILBOX_ERR_EMPTY;
  }
  else if ((0==pLen) || (0==ppBuf))
  {
    retVal = VX1000_MAILBOX_ERR_NULL;
  }
  else if (sFlgRdSplitPend != 0)
  {
    retVal = VX1000_MAILBOX_ERR_SPLIT_PEND;
  }
  else
  {
    sFlgRdSplitPend = VX1000_MAILBOX_FLG_SET;
    *pLen = gVX1000.Mailbox.MSData[gVX1000.Mailbox.MS_ReadIdx][0];
    ppBuf[0] = VX1000_ADDR_TO_PTR2U32(VX1000_PTR2VU32_TO_ADDRESS(&gVX1000.Mailbox.MSData[gVX1000.Mailbox.MS_ReadIdx][1])); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  }
  return retVal;
}

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MailboxReadDone                                                                                      */
/* API name:      VX1000_MAILBOX_READDONE (expression) / VX1000_MAILBOX_READDONE_VOID (statement)                             */
/* Wrapper API:   VX1000If_MailboxReadDone (expression) / VX1000If_MailboxReadDoneVoid (statement)                            */
/* Return value:  error code (0==OK)                                                                                          */
/* Parameter1:    None                                                                                                        */
/* Preemption:    This function must not be interrupted by any vx1000 mailbox read function.                                  */
/*                This function must not interrupt any vx1000 mailbox read function.                                          */
/* Termination:                                                                                                               */
/* Precondition1: mailboxReadSplit() must have been called successfully.                                                      */
/* Description:   Mark the filled Master->Slave mailbox slot as empty and notify the master afterwards.                       */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
VX1000_UINT32 VX1000_SUFFUN(vx1000_MailboxReadDone)(void)
{
  VX1000_UINT32 retVal = VX1000_MAILBOX_OK;

  if (0==sFlgRdSplitPend)
  {
    retVal = VX1000_MAILBOX_ERR_SPLIT_PEND;
  }
  else
  {
    sFlgRdSplitPend = VX1000_MAILBOX_FLG_CLR;
    gVX1000.Mailbox.MS_ReadIdx = (VX1000_UINT8)VX1000_MAILBOX_NEXT(gVX1000.Mailbox.MS_ReadIdx);
    /* VX1000_SPECIAL_EVENT(VX1000_EVENT_MAILBOX_UPDATE) -- removed due to EM00034754 */
  }
  return retVal;
}
#endif /* VX1000_MAILBOX_PROVIDE_SPLITREAD */

#define VX1000_CRM_CMD txBuf[0]
#define VX1000_CRM_ERR txBuf[1]
#define VX1000_CC_SET_MTA                      0xF6
#define VX1000_CC_BUILD_CHECKSUM               0xF3

/* XCP calibration memory handling */
#if defined(VX1000_MAILBOX_CAL_READ_WRITE)

/* XCP compliant protocol handler - though these defines are only visible inside this module, they still /
/  may conflict with stuff coming in via VX1000_cfg.h sub-includes, so better add VX1000_-prefixes      */
#define VX1000_CC_SHORT_DOWNLOAD               0xED
#define VX1000_CC_SHORT_UPLOAD                 0xF4

/* SHORT_DOWNLOAD */
#define VX1000_CRO_SHORT_DOWNLOAD_LEN          8 /* + CRO_SHORT_DOWNLOAD_SIZE */
#define VX1000_CRO_SHORT_DOWNLOAD_SIZE         rxBuf[(1)]
#define VX1000_CRO_SHORT_DOWNLOAD_EXT          rxBuf[(3)]
#define VX1000_CRO_SHORT_DOWNLOAD_ADDR         (*VX1000_ADDR_TO_PTR2U32(VX1000_PTR2C_TO_ADDRESS(&rxBuf[(4)])))
#define VX1000_CRO_SHORT_DOWNLOAD_DATA         (&rxBuf[(8)])
#define VX1000_CRM_SHORT_DOWNLOAD_LEN          1

/* SHORT_UPLOAD */
#define VX1000_CRO_SHORT_UPLOAD_LEN            8
#define VX1000_CRO_SHORT_UPLOAD_SIZE           rxBuf[(1)]
#define VX1000_CRO_SHORT_UPLOAD_EXT            rxBuf[(3)]
#define VX1000_CRO_SHORT_UPLOAD_ADDR           (*VX1000_ADDR_TO_PTR2U32(VX1000_PTR2C_TO_ADDRESS(&rxBuf[(4)])))
#define VX1000_CRM_SHORT_UPLOAD_DATA           (&txBuf[(1)])
#define VX1000_CRM_SHORT_UPLOAD_LEN            1 /* + CRO_SHORT_UPLOAD_SIZE */

#endif /* VX1000_MAILBOX_CAL_READ_WRITE */

/* XCP calibration page handling */
#if defined(VX1000_MAILBOX_OVERLAY_CONTROL)
#if (defined(VX1000_OVERLAY)) && (defined(VX1000_OVERLAY_VX_CONFIGURABLE)) && (defined(VX1000_OVLENBL_REGWRITE_VIA_MX))
#define VX1000_CC_USRCMD                       0xF1
#define VX1000_CC_USC_WOVLCFG                  0x91
#define VX1000_CC_USC_WOVLCFG_DESCR            0x0
#define VX1000_CC_USC_WOVLCFG_OSELOVCCON       0x1
#define VX1000_CC_USC_WOVLCFG_OVCCON           0x2
#define VX1000_CC_USC_WOVLCFG_MAX              0x2

#define VX1000_CRO_SUB_CMD                     rxBuf[1]
#define VX1000_CRO_USC_WOVLCFG_MODE            rxBuf[2]
#define VX1000_CRO_USC_WOVLCFG_DESCRIDX        rxBuf[3]
#define VX1000_CRO_USC_WOVLCFG_OVCCON          rxBuf[4]

#define VX1000_CRM_USRCMD_LEN                  1
#endif /* VX1000_OVERLAY & VX1000_OVERLAY_VX_CONFIGURABLE & VX1000_OVLENBL_REGWRITE_VIA_MX */

#define VX1000_CC_SET_CAL_PAGE                 0xEB
#define VX1000_CC_GET_CAL_PAGE                 0xEA
#define VX1000_CC_COPY_CAL_PAGE                0xE4

/* SET_CAL_PAGE */
#define VX1000_CRO_SET_CALPAGE_LEN             4
#define VX1000_CRO_SET_CALPAGE_MODE            rxBuf[(1)]
#define VX1000_CRO_SET_CALPAGE_SEG             rxBuf[(2)]
#define VX1000_CRO_SET_CALPAGE_PAGE            rxBuf[(3)]
#define VX1000_CRM_SET_CALPAGE_LEN             1

/* GET_CAL_PAGE */
#define VX1000_CRO_GET_CALPAGE_LEN             3
#define VX1000_CRO_GET_CALPAGE_MODE            rxBuf[(1)]
#define VX1000_CRO_GET_CALPAGE_SEG             rxBuf[(2)]
#define VX1000_CRM_GET_CALPAGE_LEN             4
#define VX1000_CRM_GET_CALPAGE_PAGE            txBuf[(3)]

/* COPY_CAL_PAGE */
#define VX1000_CRO_CPY_CALPAGE_LEN             5
#define VX1000_CRO_CPY_CALPAGE_SRCSEG          rxBuf[(1)]
#define VX1000_CRO_CPY_CALPAGE_SRCPAGE         rxBuf[(2)]
#define VX1000_CRO_CPY_CALPAGE_DSTSEG          rxBuf[(3)]
#define VX1000_CRO_CPY_CALPAGE_DSTPAGE         rxBuf[(4)]
#define VX1000_CRM_COPY_CAL_PAGE_LEN           1
#endif /* VX1000_MAILBOX_OVERLAY_CONTROL */

/* flash programming via mailbox */
#if defined(VX1000_MAILBOX_FLASH)

/* Commands: */
#define VX1000_CC_PROGRAM_START                0xD2
#define VX1000_CC_PROGRAM_CLEAR                0xD1
#define VX1000_CC_PROGRAM                      0xD0
#define VX1000_CC_PROGRAM_RESET                0xCF
#define VX1000_CC_GET_PGM_PROCESSORINFO        0xCE
#define VX1000_CC_GET_SECTOR_INFO              0xCD
#define VX1000_CC_PROGRAM_PREPARE              0xCC
#define VX1000_CC_PROGRAM_NEXT                 0xCA
#define VX1000_CC_PROGRAM_MAX                  0xC9
#define VX1000_CC_PROGRAM_VERIFY               0xC8

/* Data for: */
/* - PROGRAM_PREPARE */
#define VX1000_CRO_PGM_PREPARE_LEN             4
#define VX1000_CRO_PGM_PREPARE_SIZE            (*VX1000_ADDR_TO_PTR2U16(VX1000_PTR2C_TO_ADDRESS(&rxBuf[(2)])))
#define VX1000_CRM_PGM_PREPARE_LEN             1

/* - PROGRAM_START */
#define VX1000_CRO_PGM_START_LEN               1
#define VX1000_CRM_PROGRAM_START_LEN           7
#define VX1000_CRM_PGM_COMM_MODE_PGM           txBuf[(2)]
#define VX1000_CRM_PGM_MAX_CTO_PGM             txBuf[(3)]
#define VX1000_CRM_PGM_MAX_BS_PGM              txBuf[(4)]
#define VX1000_CRM_PGM_MIN_ST_PGM              txBuf[(5)]
#define VX1000_CRM_PGM_QUEUE_SIZE_PGM          txBuf[(6)]

/* - PROGRAM_CLEAR */
#define VX1000_CRO_PGM_CLEAR_LEN               8
#define VX1000_CRO_PGM_CLEAR_MODE              rxBuf[(1)]
#define VX1000_CRO_PGM_CLEAR_SIZE              (*VX1000_ADDR_TO_PTR2U32(VX1000_PTR2C_TO_ADDRESS(&rxBuf[(4)])))
#define VX1000_CRM_PGM_CLEAR_LEN               1

/* - PROGRAM */
#define VX1000_CRO_PGM_MAX_SIZE                ((VX1000_UINT8)((VX1000_MAILBOX_SLOT_DWORDS) << 2) - ...)
#define VX1000_CRO_PGM_LEN                     2 /* + CRO_PROGRAM_SIZE */
#define VX1000_CRO_PGM_SIZE                    rxBuf[(1)]
#define VX1000_CRO_PGM_DATA                    (&rxBuf[(2)])
#define VX1000_CRM_PGM_LEN                     1

#endif /* VX1000_MAILBOX_FLASH */

#if ((defined(VX1000_OVERLAY)) && (defined(VX1000_OVERLAY_VX_CONFIGURABLE))) && ((defined(VX1000_OVLENBL_REGWRITE_VIA_MX)) && (defined(VX1000_MAILBOX_OVERLAY_CONTROL)))

#if defined(VX1000_TARGET_TRICORE)

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OvlWritEcuDescrMxHandler                                                                             */
/* API name:      None                                                                                                        */
/* Return value:  status:                                                                                                     */
/*                0: cmd are handled correctly                                                                                */
/*                1: nothing is done                                                                                          */
/* Parameter1:    subCmdCode E [0, VX1000_CC_USC_WOVLCFG_MAX]                                                                 */
/* Parameter2:    overlay descriptor index E [0, OVERLAY_DESCRIPTORS)                                                         */
/* Parameter3:    additional data of cmdMode                                                                                  */
/* Parameter4:                                                                                                                */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   This function writes ECU overlay registers according to values written by VX.                               */
/*                This function is called by vx1000_MailboxHandler() when subCmd of usrCmd is VX1000_CC_USC_WOVLCFG.          */
/* Devel state:   Specified (only support TC27x, TC29x currently)                                                             */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_OvlWritEcuDescrMxHandler)(VX1000_UINT8 cmdMode, VX1000_UINT8 ovlIdx, VX1000_UINT32 ovccon)
{
  VX1000_UINT8 retVal = 0, sfrModel = 0U, overlayDescriptors = 0;

  switch ((VX1000_MCREG_CBS_JTAGID) & (VX1000_JTAGID_PN_MASK)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  {
    case VX1000_JTAGID_PN_TC172x:       overlayDescriptors = 16U; sfrModel = 0U; break; /* FutureMax : not sure about the sfrModel */
    case VX1000_JTAGID_PN_TC1387:       overlayDescriptors = 16U; sfrModel = 0U; break; /* AudoS */
    case VX1000_JTAGID_PN_TC178x:       overlayDescriptors = 16U; sfrModel = 0U; break; /* AudoMax */
    case VX1000_JTAGID_PN_TC1767:       overlayDescriptors = 16U; sfrModel = 0U; break; /* AudoFuture */
    case VX1000_JTAGID_PN_TC1797:       overlayDescriptors = 16U; sfrModel = 0U; break; /* AudoFuture */
    case VX1000_JTAGID_PN_TC1798:       overlayDescriptors = 16U; sfrModel = 1U; break; /* AudoMax */
    case VX1000_JTAGID_PN_TC21x:        overlayDescriptors =  8U; sfrModel = 2U; break; /* just a guess: "Aurix single core with 8 descriptors" */
    case VX1000_JTAGID_PN_TC22x:        overlayDescriptors =  8U; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC23x:        overlayDescriptors =  8U; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC24x:        overlayDescriptors = 32U; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC26x:        overlayDescriptors = 32U; sfrModel = 3U; break; /* Aurix dual core */
    case VX1000_JTAGID_PN_TC27xTC2Dx:   overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC29x:        overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    /*
    TODOKNM: case VX1000_JTAGID_PN_TC32x:        overlayDescriptors = 32U; sfrModel = ?U; break;
    */
    case VX1000_JTAGID_PN_TC33x:        overlayDescriptors = 32U; sfrModel = 2U; break; /* Aurix single core */
    case VX1000_JTAGID_PN_TC33xED:      overlayDescriptors = 32U; sfrModel = 3U; break; /* Aurix dual core */
    case VX1000_JTAGID_PN_TC35x:        overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC36x:        overlayDescriptors = 32U; sfrModel = 3U; break; /* Aurix dual core */
    case VX1000_JTAGID_PN_TC37x:        overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC37xED:      overlayDescriptors = 32U; sfrModel = 4U; break; /* Aurix triple core */
    case VX1000_JTAGID_PN_TC38x:        overlayDescriptors = 32U; sfrModel = 5U; break; /* Aurix quad core */
    case VX1000_JTAGID_PN_TC39x:        overlayDescriptors = 32U; sfrModel = 7U; break; /* Aurix hexa core */
    default: break;
  }
  if ((ovlIdx >= (overlayDescriptors)) || (cmdMode > (VX1000_CC_USC_WOVLCFG_MAX)))
  {
    retVal = 1;
  }
  else
  {
    if ((sfrModel == 4U)  /* three cores of type Aurix */
    ||  (sfrModel == 7U)) /* six cores of type AurixPlus */
    {
      VX1000_UINT8 i = ovlIdx;
      switch ((VX1000_UINT8)cmdMode)
      {
        case VX1000_CC_USC_WOVLCFG_DESCR:
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( i, 0UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].RABR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( i, 0UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].OTAR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(i, 0UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].OMASK); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( i, 1UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].RABR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( i, 1UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].OTAR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(i, 1UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].OMASK); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( i, 2UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].RABR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( i, 2UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].OTAR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(i, 2UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].OMASK); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          if (sfrModel == 7U)
          {
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( i, 3UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].RABR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( i, 3UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].OTAR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(i, 3UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].OMASK); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( i, 4UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].RABR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( i, 4UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].OTAR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(i, 4UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].OMASK); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            /* Note that there is one gap in the OVC core indices (CPU5 continues with index 6) */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_RABR_AURIX( i, 6UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].RABR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OTAR_AURIX( i, 6UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].OTAR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            (VX1000_ADDR_TO_PTR2VU32(VX1000_MCREGADDR_OMASK_AURIX(i, 6UL)))[0] = ((VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->DESCR)[i].OMASK); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          break;
        case VX1000_CC_USC_WOVLCFG_OSELOVCCON:
          VX1000_MCREG_OVC0_OSEL = (VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->OSEL); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          VX1000_MCREG_OVC1_OSEL = (VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->OSEL); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          VX1000_MCREG_OVC2_OSEL = (VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->OSEL); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          if (sfrModel == 7U)
          {
            VX1000_MCREG_OVC3_OSEL = (VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->OSEL); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            VX1000_MCREG_OVC4_OSEL = (VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->OSEL); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
            VX1000_MCREG_OVC5_OSEL = (VX1000_ADDR_TO_PTR2OCR(gVX1000.Ovl.ovlConfigRegsPtr)->OSEL); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          }
          VX1000_MCREG_SCU_OVCCON = ovccon; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          break;
        case VX1000_CC_USC_WOVLCFG_OVCCON:
          VX1000_MCREG_SCU_OVCCON = ovccon; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
          break;
        default:
          retVal = 1;
          break;
      }
    }
    else
    {
      VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)
      retVal = 1;
    }
  }
  return retVal;
}

#else  /* !VX1000_TARGET_TRICORE */

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_OvlWritEcuDescrMxHandler                                                                             */
/* API name:      None                                                                                                        */
/* Return value:  status:                                                                                                     */
/*                0: cmd are handled correctly                                                                                */
/*                1: nothing is done                                                                                          */
/* Parameter1:    subCmdCode E [0, VX1000_CC_USC_WOVLCFG_MAX]                                                                 */
/* Parameter2:    overlay descriptor index E [0, OVERLAY_DESCRIPTORS)                                                         */
/* Parameter3:    additional data of cmdMode                                                                                  */
/* Parameter4:                                                                                                                */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1:                                                                                                             */
/* Description:   This function writes ECU overlay registers according to values written by VX.                               */
/*                This function is called by vx1000_MailboxHandler() when subCmd of usrCmd is VX1000_CC_USC_WOVLCFG.          */
/* Devel state:   Idea                                                                                                        */
/*----------------------------------------------------------------------------------------------------------------------------*/
static VX1000_UINT8 VX1000_SUFFUN(vx1000_OvlWritEcuDescrMxHandler)(VX1000_UINT8 cmdMode, VX1000_UINT8 ovlIdx, VX1000_UINT32 xxx)
{
  VX1000_UINT8 retVal = 1;
  VX1000_ERRLOGGER(VX1000_ERRLOG_OVL_UNIMPL)
  VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,cmdMode, ovlIdx,xxx,0,0)) /* interims dummy access */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
  return retVal;
}

#endif /* !VX1000_TARGET_TRICORE */

#endif /* VX1000_OVERLAY && VX1000_OVERLAY_VX_CONFIGURABLE && VX1000_OVLENBL_REGWRITE_VIA_MX && VX1000_MAILBOX_OVERLAY_CONTROL */

#if (defined(VX1000_MAILBOX_OVERLAY_CONTROL) || defined(VX1000_MAILBOX_FLASH)) || defined(VX1000_MAILBOX_CAL_READ_WRITE)

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MailboxHandler                                                                                       */
/* API name:      VX1000_MAILBOX_CONTROL                                                                                      */
/* Wrapper API:   VX1000If_MailboxControl                                                                                     */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:    This function must not be interrupted by any vx1000 mailbox function.                                       */
/*                This function must not interrupt any vx1000 mailbox function.                                               */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_MailboxInit() must have been called.                                                                 */
/* Description:   check the vx1000 mailbox system for pending requests and trigger the necessary reactions.                   */
/* Devel state:   Implemented (todo: use byte-wise little-endian import)                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
void VX1000_SUFFUN(vx1000_MailboxHandler)(void)
{
#if defined(VX1000_MAILBOX_CAL_READ_WRITE)
  /* note: this unused feature is not fully implemented because it needs buffer sizes of 256 and rxLen of 252 */
#endif /* !VX1000_MAILBOX_CAL_READ_WRITE */
  VX1000_UINT32 txLen, rxLen = 8;
  VX1000_CHAR rxBuf[12], txBuf[12];
  VX1000_UINT8 postponeAnswer = 0;
  static volatile VX1000_UINT32 gVX1000_MX_mta; /* in this version made it local to this function because only accessed from here. g-prefix not removed though to keep changes minimal. */

  if ((VX1000_MAILBOX_OK) == vx1000_MailboxRead(&rxLen, &rxBuf[0]))
  {
    txLen = 2;
    VX1000_CRM_CMD = (VX1000_CHAR)-1;
    VX1000_CRM_ERR = 0x00;
    switch ((VX1000_UINT8)rxBuf[0]) /* check CRO_CMD */
    {
    case (VX1000_UINT8)(VX1000_CC_SET_MTA):
      /* VX1000_CRM_ERR is already initialised */
      gVX1000_MX_mta = ((((VX1000_UINT32)rxBuf[(4 + 0)]) | ((VX1000_UINT32)rxBuf[(4 + 1)] << 8)) | (((VX1000_UINT32)rxBuf[(4 + 2)] << 16) | ((VX1000_UINT32)rxBuf[(4 + 3)] << 24)));
      VX1000_DUMMYREAD(gVX1000_MX_mta) /* just to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
      txLen = 1;
      break;
#if defined(VX1000_MAILBOX_CAL_READ_WRITE)
    case (VX1000_UINT8)(VX1000_CC_SHORT_UPLOAD):
#if defined(VX1000_SHORT_UPLOAD)
      VX1000_CRM_ERR = (VX1000_CHAR)(VX1000_SHORT_UPLOAD((VX1000_CRM_SHORT_UPLOAD_DATA), (VX1000_CRO_SHORT_UPLOAD_ADDR), (VX1000_CRO_SHORT_UPLOAD_SIZE))); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else /* !VX1000_SHORT_UPLOAD */
      {
        VX1000_UINT32 j;
        volatile VX1000_UINT8 *dst = VX1000_ADDR_TO_PTR2VU8(VX1000_PTR2C_TO_ADDRESS(VX1000_CRM_SHORT_UPLOAD_DATA)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        volatile VX1000_UINT8 *src = VX1000_ADDR_TO_PTR2VU8(VX1000_CRO_SHORT_UPLOAD_ADDR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        for (j = 0; j < (VX1000_UINT32)(VX1000_CRO_SHORT_UPLOAD_SIZE); ++j) { dst[j] = src[j]; }
      }
#endif /* !VX1000_SHORT_UPLOAD */
      txLen = (VX1000_UINT32)(VX1000_CRM_SHORT_UPLOAD_LEN)+(VX1000_UINT32)(VX1000_CRO_SHORT_UPLOAD_SIZE);
      break;
    case (VX1000_UINT8)(VX1000_CC_SHORT_DOWNLOAD):
#if defined(VX1000_SHORT_DOWNLOAD)
      VX1000_CRM_ERR = (VX1000_CHAR)(VX1000_SHORT_DOWNLOAD((VX1000_CRO_SHORT_DOWNLOAD_ADDR), (VX1000_CRO_SHORT_DOWNLOAD_DATA), (VX1000_CRO_SHORT_DOWNLOAD_SIZE))); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
#else /* !VX1000_SHORT_DOWNLOAD */
      {
        VX1000_UINT32 j;
        volatile VX1000_UINT8 *dst = VX1000_ADDR_TO_PTR2VU8(VX1000_CRO_SHORT_DOWNLOAD_ADDR); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        volatile VX1000_UINT8 *src = VX1000_ADDR_TO_PTR2VU8(VX1000_PTR2C_TO_ADDRESS(VX1000_CRO_SHORT_DOWNLOAD_DATA)); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        for (j = 0; j < (VX1000_UINT32)(VX1000_CRO_SHORT_DOWNLOAD_SIZE); ++j) { dst[j] = src[j]; }
      }
#endif /* !VX1000_SHORT_DOWNLOAD */
      txLen = VX1000_CRM_SHORT_DOWNLOAD_LEN;
      break;
#endif /* VX1000_MAILBOX_CAL_READ_WRITE */
#if defined(VX1000_MAILBOX_OVERLAY_CONTROL)
    case (VX1000_UINT8)(VX1000_CC_GET_CAL_PAGE):
      VX1000_CRM_GET_CALPAGE_PAGE = (VX1000_CHAR)(VX1000_GET_CAL_PAGE((VX1000_CRO_GET_CALPAGE_SEG), (VX1000_CRO_GET_CALPAGE_MODE)));
      txLen = VX1000_CRM_GET_CALPAGE_LEN;
      break;
    case (VX1000_UINT8)(VX1000_CC_SET_CAL_PAGE):
      VX1000_CRM_ERR = (VX1000_CHAR)(VX1000_WRP_SET_CAL_PAGE((VX1000_CRO_SET_CALPAGE_SEG), (VX1000_CRO_SET_CALPAGE_PAGE), (VX1000_CRO_SET_CALPAGE_MODE),0));
      if (VX1000_CRM_ERR == (VX1000_CHAR)(VX1000_CRC_CMD_BUSY))
      {
        postponeAnswer = 1; /* Asynchronous call. Mailbox answer will be sent in the callback done macro.*/
      }
      txLen = VX1000_CRM_SET_CALPAGE_LEN;
      break;
#if !defined(VX1000_OVERLAY_VX_CONFIGURABLE)
    case (VX1000_UINT8)(VX1000_CC_COPY_CAL_PAGE):
      VX1000_CRM_ERR = (VX1000_CHAR)(VX1000_COPY_CAL_PAGE((VX1000_CRO_CPY_CALPAGE_SRCSEG), (VX1000_CRO_CPY_CALPAGE_SRCPAGE),
                                                     (VX1000_CRO_CPY_CALPAGE_DSTSEG), (VX1000_CRO_CPY_CALPAGE_DSTPAGE)));
      txLen = VX1000_CRM_COPY_CAL_PAGE_LEN;
      break;
#endif /* !VX1000_OVERLAY_VX_CONFIGURABLE */
#if (defined(VX1000_OVERLAY)) && (defined(VX1000_OVERLAY_VX_CONFIGURABLE)) && (defined(VX1000_OVLENBL_REGWRITE_VIA_MX))
    case (VX1000_UINT8)(VX1000_CC_USRCMD):
      if( (VX1000_UINT8)(rxBuf[1]) == VX1000_CC_USC_WOVLCFG)
      {
        VX1000_CRM_ERR = (VX1000_CHAR)(VX1000_SUFFUN(vx1000_OvlWritEcuDescrMxHandler)((VX1000_CRO_USC_WOVLCFG_MODE), (VX1000_CRO_USC_WOVLCFG_DESCRIDX), *(VX1000_ADDR_TO_PTR2U32(VX1000_PTR2C_TO_ADDRESS(&(VX1000_CRO_USC_WOVLCFG_OVCCON)))))); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        txLen = VX1000_CRM_USRCMD_LEN;
      }
      else
      {
        VX1000_CRM_ERR = VX1000_CRC_CMD_UNKNOWN;
      }
      break;
#endif /* VX1000_OVERLAY & VX1000_OVERLAY_VX_CONFIGURABLE & VX1000_OVLENBL_REGWRITE_VIA_MX & VX1000_MAILBOX */
#endif /* VX1000_MAILBOX_OVERLAY_CONTROL */
#if defined(VX1000_MAILBOX_FLASH)
    case (VX1000_UINT8)(VX1000_CC_PROGRAM_PREPARE):
      /* The user callback has to cease normal ECU operation and return OK if this succeeded  */
      /* AND the FKL memory (from param1 to param2) does not interfere with current RAM usage */
      VX1000_CRM_ERR = (VX1000_CHAR)(VX1000_PROGRAM_PREPARE( gVX1000_MX_mta, (VX1000_UINT32)(VX1000_CRO_PGM_PREPARE_SIZE) )); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      txLen = VX1000_CRM_PGM_PREPARE_LEN;
      break;
#if 0 /* disabled the code, while the illegal "#if 0" reminds us in every release that we have to fully specify and implemnt this */
//    case (VX1000_UINT8)(VX1000_CC_PROGRAM_START):
//      VX1000_CRM_PGM_COMM_MODE_PGM = 0;
//      VX1000_CRM_PGM_MAX_CTO_PGM = 8;
//      VX1000_CRM_PGM_MAX_BS_PGM = 1;
//      VX1000_CRM_PGM_MIN_ST_PGM = 0;
//      VX1000_CRM_PGM_QUEUE_SIZE_PGM = 1;
//      txLen = VX1000_CRM_PGM_START_LEN;
//      /* @@@@ Problem: how to actually program flash? */
//      break;
#endif /* 0 */
#endif /* VX1000_MAILBOX_FLASH */
    default:
      VX1000_CRM_ERR = VX1000_CRC_CMD_UNKNOWN;
      break;
    }
    if (0==postponeAnswer)
    {
      if ((VX1000_CRM_ERR) != 0)
      {
        VX1000_CRM_CMD = (VX1000_CHAR)-2;
        txLen = 2;
      }
      (void)VX1000_SUFFUN(vx1000_MailboxWrite)(txLen, txBuf);
    }
  }
}
#endif /* VX1000_MAILBOX_OVERLAY_CONTROL || VX1000_MAILBOX_FLASH || VX1000_MAILBOX_CAL_READ_WRITE */

#if defined(VX1000_MAILBOX_PRINTF)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MailboxPutchar                                                                                       */
/* API name:      None                                                                                                        */
/* Return value:  None                                                                                                        */
/* Parameter1:    a character to be transmitted                                                                               */
/*                No invalid input possible.                                                                                  */
/* Preemption:    This function must not be interrupted by any vx1000 mailbox write function.                                 */
/*                This function must not interrupt any vx1000 mailbox write function.                                         */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_MailboxInit() must have been called.                                                                 */
/* Description:   Output a single character to vx1000 mailbox system.                                                         */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
static void VX1000_SUFFUN(vx1000_MailboxPutchar)( VX1000_CHAR character )
{
  static VX1000_CHAR vx1000EvBuf[(VX1000_MAILBOX_SLOT_DWORDS) << 2]; /* note: instead of this local buffer, we _could_ use the writeSplit feature, but _then_ the user would have to disable context switches during prints! */
  static VX1000_UINT32 vx1000EvLen;

  if ((vx1000EvBuf[0] != (VX1000_CHAR)-4) || (vx1000EvBuf[1] != (VX1000_CHAR)1) || (vx1000EvLen >= ((VX1000_MAILBOX_SLOT_DWORDS) << 2)))
  {
    /* Initialise the protocol header once */
    vx1000EvBuf[0] = (VX1000_CHAR)-4; /* SERV */
    vx1000EvBuf[1] = (VX1000_CHAR)1;  /* SERV_TEXT */
    vx1000EvLen = 2;
  }
  vx1000EvBuf[vx1000EvLen] = character;
  vx1000EvLen++;
  if ( ((vx1000EvLen + 4) >= ((VX1000_MAILBOX_SLOT_DWORDS) << 2)) || (0==character) )
  {
    (void)VX1000_SUFFUN(vx1000_MailboxWrite)(vx1000EvLen, vx1000EvBuf);
    vx1000EvLen = 2;
  }
}

#if !defined(VX1000_PRINTF_MINIMAL)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MailboxPrintf                                                                                        */
/* API name:      VX1000_PRINTF                                                                                               */
/* Wrapper API:   VX1000If_PrintF                                                                                             */
/* Return value:  None                                                                                                        */
/* Parameter1:    pointer to ASCIIZ string in stdio-printf format (token support according to linked compiler library)        */
/*                Validity has to be ensured by the caller; bad input may lead to crash!                                      */
/* Parameter2+:   variadic list of additional printf input.                                                                   */
/*                Validity has to be ensured by the caller; bad input may lead to crash!                                      */
/* Preemption:    This function must not be interrupted by any vx1000 mailbox write function.                                 */
/*                This function must not interrupt any vx1000 mailbox write function.                                         */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_MailboxInit() must have been called.                                                                 */
/* Description:   Full version of classic printf() with output redirected to vx1000 mailbox system.                           */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
/* The full printf library function not only occupies two dozens of Kbytes TEXT but also allocates */
/* a rather huge amount of stack upon calls so it is not well suited for multitasking environments */
void VX1000_SUFFUN(vx1000_MailboxPrintf)( const VX1000_CHAR *format, ... ) /* PRQA S 5069 */ /* Cannot avoid violating MISRA rule 16.1 because a C++-based workaround would only violate other rules */
{
  va_list argptr;
  VX1000_CHAR buf[256];

  va_start(argptr, format);
  (void)vsprintf((VX1000_CHAR *)buf, format, argptr);
  va_end(argptr); /* PRQA S 3199 */ /* it depends on the used compiler whether this statement has useful side effects or not */

  /* Transmit the text message */
  {
    VX1000_CHAR *p = buf;
    while (p[0] != 0)
    {
      VX1000_SUFFUN(vx1000_MailboxPutchar)(p[0]);
      p = &p[1];
    }
  }

  /* Transmit the terminating 0x00. */
  VX1000_SUFFUN(vx1000_MailboxPutchar)( 0x00 );
}
#else /* VX1000_PRINTF_MINIMAL */
/* This reduced printf implementation does not support features like float support or argument */
/* repetition, but it usually uses less TEXT and RAM usage is only a few bytes at runtime.     */

#define VX1000_PFM_NONE                0U
#define VX1000_PFM_IN_TOKEN            1U
#define VX1000_PFM_IN_WIDTH            2U
#define VX1000_PFM_AFTER_WIDTH         4U
#define VX1000_PFM_IN_PREC             8U
#define VX1000_PFM_AFTER_PREC          16U
#define VX1000_PFM_IN_SIZE             32U
#define VX1000_PFM_AFTER_SIZE          64U
#define VX1000_PFM_INVALID             128U

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_MailboxPrintf                                                                                        */
/* API name:      VX1000_PRINTF                                                                                               */
/* Wrapper API:   VX1000If_PrintF                                                                                             */
/* Return value:  None                                                                                                        */
/* Parameter1:    pointer to ASCIIZ string in stdio-printf format (not all tokens supported!)                                 */
/*                Validity has to be ensured by the caller; bad input may lead to crash!                                      */
/* Parameter2+:   variadic list of additional printf input.                                                                   */
/*                Validity has to be ensured by the caller; bad input may lead to crash!                                      */
/* Preemption:    This function must not be interrupted by any vx1000 mailbox write function.                                 */
/*                This function must not interrupt any vx1000 mailbox write function.                                         */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_MailboxInit() must have been called.                                                                 */
/* Description:   Minimal version of classic printf() with output redirected to vx1000 mailbox system.                        */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
void VX1000_SUFFUN(vx1000_MailboxPrintf)( const VX1000_CHAR *format, ... ) /* PRQA S 5069 */ /* Cannot avoid violating MISRA rule 16.1 because a C++-based workaround would only violate other rules */
{
  va_list arglist;
  VX1000_LONG number;
  VX1000_LDOUBL fdummy = 0.0;          /* some compilers require a suffix here to avoid warnings but others would not understand the suffix, so no suffix here */
  VX1000_CHAR *p, *p2, numbuf[24], frmchr, digif=0, filler=0, alignleft=0, showpos=0, showbase=0, signedtype=0, floattype=0;
  VX1000_INT   i, width=0, prec=0, bsize=0, basesize=0;
  VX1000_UINT8 state, continuer = 1;

  va_start(arglist, format);
  state = (VX1000_PFM_NONE);
  for (; (format[0] != 0) && (continuer != 0); format = &format[1])
  {
    continuer = 0;
    frmchr = format[0];
    if (state == (VX1000_PFM_NONE))
    {
      if (frmchr == '%')
      {
        state = (VX1000_PFM_IN_TOKEN);
        width = prec = bsize = basesize = 0;
        floattype = signedtype = 0;
        filler = showpos = ' ';
      }
      else
      {
        VX1000_SUFFUN(vx1000_MailboxPutchar)(frmchr);
      }
      continuer = 1;
    }
    if ((0==continuer) && (frmchr == '%'))
    {
      if (state == (VX1000_PFM_IN_TOKEN)) /* no real token, just an escaped "%" */
      {
        state = (VX1000_PFM_NONE);
        VX1000_SUFFUN(vx1000_MailboxPutchar)(frmchr);
      }
      else
      {
        state |= (VX1000_PFM_INVALID);
      }
      continuer = 1;
    }
    if ((0==continuer) && ((frmchr >= '0') && (frmchr <= '9')))
    {
      frmchr -= '0';
      switch (state & ((VX1000_PFM_IN_WIDTH) | (VX1000_PFM_IN_PREC) | (VX1000_PFM_IN_SIZE))) /* continue in a number */
      {
        case (VX1000_PFM_IN_WIDTH): width = (10 * width) + (VX1000_INT)frmchr; continuer = 1; break;
        case (VX1000_PFM_IN_PREC):  prec  = (10 * prec ) + (VX1000_INT)frmchr; VX1000_DUMMYREAD(prec)/* just to avoid compiler warnings */ continuer = 1; break; /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
        case (VX1000_PFM_IN_SIZE):  bsize = (10 * bsize) + (VX1000_INT)frmchr; continuer = 1; break;
        default:  /* only here for MISRA */
          break; /* only here for MISRA */
      }
      if ((0==continuer) && (0==(state & (VX1000_PFM_AFTER_WIDTH)))) /* start reading width number */
      {
        state |= (VX1000_PFM_IN_WIDTH);
        width = (VX1000_INT)frmchr;
        if (frmchr==0) { filler = '0'; }
        continuer = 1;
      }
      if ((0==continuer) && (0==(state & (VX1000_PFM_AFTER_PREC)))) /* start reading precision number */
      {
        state |= (VX1000_PFM_IN_PREC);
        prec = (VX1000_INT)frmchr;
        VX1000_DUMMYREAD(prec) /* just to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
        continuer = 1;
      }
      if (0==continuer)
      {
        state |= (VX1000_PFM_INVALID);
      }
      continuer = 1;
    }
    if (0==continuer)
    {
      switch (state & ((VX1000_PFM_IN_WIDTH) | (VX1000_PFM_IN_PREC) | (VX1000_PFM_IN_SIZE))) /* stop any number */
      {
        case (VX1000_PFM_IN_WIDTH): state ^= (VX1000_PFM_IN_WIDTH) | (VX1000_PFM_AFTER_WIDTH); break;
        case (VX1000_PFM_IN_PREC):  state ^= (VX1000_PFM_IN_PREC)  | (VX1000_PFM_AFTER_PREC);  break;
        case (VX1000_PFM_IN_SIZE):  state ^= (VX1000_PFM_IN_SIZE)  | (VX1000_PFM_AFTER_SIZE);  break;
        default:  /* only here for MISRA */
          break; /* only here for MISRA */
      }
    }
    if ((0==continuer) && (frmchr == 'l'))
    {
      state |= (VX1000_PFM_IN_SIZE);
      bsize += (VX1000_INT)(8 * (sizeof(VX1000_LONG) - sizeof(VX1000_INT)));
      if (bsize > 64) { state |= (VX1000_PFM_INVALID); }
      continuer = 1;
    }
    if (0==continuer)
    {
      switch (frmchr) /* get the format specifier */
      {
        case '#':
          showbase = 1;
          continuer = 1;
          break;
        case '+':
          showpos = '+';
          continuer = 1;
          break;
        case '-':
          alignleft = 1;
          continuer = 1;
          break;
        case 'L':
          state |= (VX1000_PFM_AFTER_SIZE);
          bsize = 256;
          continuer = 1;
          break;
        case 'I': /* Microsoft specific size: start reading the number */
          state |= (VX1000_PFM_IN_SIZE);
          continuer = 1;
          break;
        case 'c':
          VX1000_SUFFUN(vx1000_MailboxPutchar)((VX1000_CHAR)va_arg(arglist, VX1000_INT));
          state = (VX1000_PFM_NONE);
          continuer = 1;
          break;
        case 's':
          /* todo: care for alignment */
          p = (VX1000_CHAR*)va_arg(arglist, VX1000_CHAR*);
          while (p[0] != 0) { VX1000_SUFFUN(vx1000_MailboxPutchar)(p[0]); p = &p[1]; }
          state = (VX1000_PFM_NONE);
          continuer = 1;
          break;
        case 'o':
          digif = '7';
          basesize = 8 * sizeof(VX1000_INT);
          break;
        case 'd':
          /* fall-through to 'i' */
        case 'i':
          /* fall-through to 'u' */
        case 'u':
          if (frmchr != 'u') { signedtype = 1; } /* MISRA does not like if we do this in the d/i cases above so we need this additional if statement inside the u case */
          digif = '9';
          basesize = 8 * sizeof(VX1000_INT);
          break;
        case 'X':
          digif = 'F';
          basesize = 8 * sizeof(VX1000_INT);
          break;
        case 'x':
          digif = 'f';
          basesize = 8 * sizeof(VX1000_INT);
          break;
        case 'e':
        case 'E':
          /* scientific floats are not supported */
          basesize = 128;
          floattype= 1;
          break;
        case 'f':
        case 'F':
        case 'g':
        case 'G':
          /* fixed floats are not supported */
          basesize = 64;
          floattype= 1;
          break;
        default:
          state |= (VX1000_PFM_INVALID);
      }
    }
    if ((0==continuer) && (0==(state & (VX1000_PFM_INVALID))))
    {
      state = (VX1000_PFM_NONE);
      if (bsize < basesize) { bsize = basesize; }
      if (floattype != 0)
      {
        switch (bsize)
        {
          /* floats currently are not supported - just advance the pointer and print a hash sign as place holder */
          case 64:  VX1000_SUFFUN(vx1000_MailboxPutchar)('#'); fdummy = (VX1000_LDOUBL)va_arg(arglist, /*float*/VX1000_DOUBLE); continuer = 1; break;
          case 128: VX1000_SUFFUN(vx1000_MailboxPutchar)('#'); fdummy = (VX1000_LDOUBL)va_arg(arglist, VX1000_DOUBLE);          continuer = 1; break;
          case 256: VX1000_SUFFUN(vx1000_MailboxPutchar)('#'); fdummy = (VX1000_LDOUBL)va_arg(arglist, VX1000_LDOUBL);          continuer = 1; break;
          default:    /* only here for MISRA */
            VX1000_DUMMYREAD(fdummy) /* just to avoid compiler warnings */ /* PRQA S 3112 */ /* The code in this user callback may violate MISRA rule 14.2 because the dedicated goal of this code is to actually have no (side) effect at all (except avoiding possible warnings by some compilers */
            break;    /* only here for MISRA */
        }
      }
      else
      {
        switch (bsize)
        {
          case 8 * sizeof(VX1000_INT): number = (VX1000_LONG)va_arg(arglist, VX1000_INT);   break;
          default:                     number = (VX1000_LONG)va_arg(arglist, VX1000_LONG);  break;
          }
          if (signedtype != 0)
          {
            if (number < 0)
            {
              number = -number;
              showpos = '-';
            }
        }
        if (showpos != ' ') { VX1000_SUFFUN(vx1000_MailboxPutchar)(showpos); }
        switch (digif)
        {
          case '7':
            i = 11;
            numbuf[i] = 0;
            while (i != 0)
            {
              i--; numbuf[i] = (VX1000_CHAR)(0x7UL & (VX1000_UINT32)number);
              numbuf[i] += '0';
              number = (VX1000_LONG)((VX1000_UINT32)number >> 3);
            }
            break;
          case '9':
            {
              VX1000_UINT32 data0 = 0, data1 = 0, mask0, mask1;
              for (i = 31; i >= 0; --i)
              {
                mask1 = (0x88888888UL & (data1 + 0x33333333UL)); mask0 = (0x88 & (data0 + 0x33));
                data1 += (0x00000001UL & (((VX1000_UINT32)number) >> i)) + ((data1 + mask1) - (mask1 >> 2));
                data0 += (data0 - (mask0 >> 2)) + (mask0 + (mask1 >> 31));
              }
              i = 10;
              numbuf[i] = 0;
              while (i != 0)
              {
                i--; numbuf[i] = (VX1000_CHAR)(0xFUL & data1);
                numbuf[i] += '0';
                data1 = (i != 2)? (data1 >> 4) : data0;
              }
            }
            break;
          case 'F': /* fallthrough to 'f' */
          case 'f':
            digif -= 15;
            i = 8;
            numbuf[i] = 0;
            while (i != 0)
            {
              frmchr = (VX1000_CHAR)(0xFUL & (VX1000_UINT32)number);
              number = (VX1000_LONG)((VX1000_UINT32)number >> 4);
              frmchr += (VX1000_CHAR)((frmchr > 9) ? digif : (VX1000_CHAR)'0');
              i--; numbuf[i] = frmchr;
            }
            break;
          default:  /* only here for MISRA */
            break; /* only here for MISRA */
        }
        for (p = numbuf; (p[0] == '0') && (p[1] != 0); p = &p[1]) { }
        if (alignleft == 1)
        {
          if ((showbase == 1) && (digif != '9'))
          {
            VX1000_SUFFUN(vx1000_MailboxPutchar)((VX1000_CHAR)'0');
            if (digif != '7') { VX1000_SUFFUN(vx1000_MailboxPutchar)((VX1000_CHAR)'x'); }
          }
          for (; p[0] != 0; p = &p[1]) { VX1000_SUFFUN(vx1000_MailboxPutchar)(p[0]); width--; }
          for (--width; width >= 0; width--) { VX1000_SUFFUN(vx1000_MailboxPutchar)(' ');  }
        }
        else
        {
          for (p2 = p; p2[0] != 0; p2 = &p2[1]) { width--; }
          for (--width; width >= 0; width--) { VX1000_SUFFUN(vx1000_MailboxPutchar)(filler); }
          if ((showbase == 1) && (digif != '9'))
          {
            VX1000_SUFFUN(vx1000_MailboxPutchar)((VX1000_CHAR)'0');
            if (digif != '7') { VX1000_SUFFUN(vx1000_MailboxPutchar)((VX1000_CHAR)'x'); }
          }
          for (; p[0] != 0; p = &p[1]) { VX1000_SUFFUN(vx1000_MailboxPutchar)(p[0]); }
        }
        continuer = 1;
      }
    }
    if (0==continuer)
    {
      /* Reaching here means we misunderstood the format string and assumed stupid argument type.    /
      /  To avoid invalid pointer accesses to any following arguments, we stop continuing the loop. */

      /* todo: replace the following "error marker print" by a regular VX1000_ERRLOG(<new error code>) */
      VX1000_SUFFUN(vx1000_MailboxPutchar)('!');VX1000_SUFFUN(vx1000_MailboxPutchar)(64);VX1000_SUFFUN(vx1000_MailboxPutchar)('!');
    }
  } /* for */
  va_end(arglist); /* PRQA S 3199 */ /* it depends on the used compiler whether this statement has useful side effects or not */
  /* Transmit the terminating 0x00. */
  VX1000_SUFFUN(vx1000_MailboxPutchar)( (VX1000_UINT8)0x00U );
}
#endif /* VX1000_PRINTF_MINIMAL */



#endif /* VX1000_MAILBOX_PRINTF */
#endif /* VX1000_MAILBOX  && !VX1000_COMPILED_FOR_SLAVECORES */


#if defined(VX1000_FKL_SUPPORT_ADDR) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_FlashPrepareLoop                                                                                     */
/* API name:      VX1000_DETECT_FKL_REQUESTS                                                                                  */
/* Wrapper API:   VX1000If_DetectFklRequests                                                                                  */
/* Return value:  None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Preemption:                                                                                                                */
/* Termination:   May leave entire system in an invalid state.                                                                */
/* Precondition1: vx1000_InitAsyncEnd() must have been called.                                                                */
/* Description:   Prevents the application from writing to RAM to enable successful flash kernel download;                    */
/*                Busy waits for trigger command to jump to the transferred address.                                          */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
void VX1000_SUFFUN(vx1000_FlashPrepareLoop)(void)
{
  gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_FKL_REQ_DETECTED);
  VX1000_DISABLE_ALL_INTERRUPTS()                /* prevent other tasks / ISRs from overwriting RAM / reading flash */
  VX1000_STOP_OS_TIMING_PROTECTION()             /* prevent Autosar OS in SC2 / SC4 from killing the loop/kernel */
  VX1000_STOP_OTHER_CORES()                      /* prevent other cores (or DMA/PEC) from overwriting RAM / reading flash */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  VX1000_ENABLE_STD_RAM_MAPPING()                /* repair non-default RAM mapping (the FKL is linked to standard location) */

  (VX1000_FKL_WORKSPACE)->DeprotectState     = (VX1000_UINT16)(VX1000_FKL_STATE1CODE); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  (VX1000_FKL_WORKSPACE)->DeprotectTrigger   = (VX1000_UINT16)(VX1000_FKL_TOSTATE1CODE); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  (VX1000_FKL_WORKSPACE)->TransitionTimeout  = (VX1000_UINT32)(VX1000_FKL_TRANSITION_TIMEOUT); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  (VX1000_FKL_WORKSPACE)->WdgData1           = (VX1000_UINT32)0xFFFFffffUL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  (VX1000_FKL_WORKSPACE)->WdgData2           = (VX1000_UINT32)0xFFFFffffUL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  (VX1000_FKL_WORKSPACE)->WdgData3_FklParam3 = (VX1000_UINT32)0xFFFFffffUL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
  VX1000_ADDR_TO_PTR2U32(VX1000_PTR2VFF_TO_ADDRESS(&((VX1000_FKL_WORKSPACE)->EntryPoint)))[0] = (VX1000_UINT32)0xFFFFFFFFUL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */

#if !defined(VX1000_FKL_BOUNCEBACK)
  /* Now normal operation of the ECU (Appl + XCP instrumentation) is stopped: tell the tool how to further communicate: */
  gVX1000.MagicId = (VX1000_UINT32)(VX1000_FKL_SUPPORT_ADDR);
  gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_FKL_FORCED_IDLE);
  /* Once the tool accepted the new communication area, no longer access gVX1000, because that location is likely to be */
  /* reused for storing the flash kernel code, so reads would return invalid data and writes could destroy the kernel.  */

  while (1) /* intentional infinite loop (can be left by pseudo call to FKL or by watchdog reset) */
  {
    /* The code inside this loop must not use stack (access non-register-ed local variables or call non-FKL functions) */
    /* (if that can't be ensured, we'd have to reload the SP with (VX1000_FKL_SUPPORT_ADDR-16): HW-SPECIFIC ASSEMBLY!) */
    if ( (((VX1000_FKL_WORKSPACE)->TransitionTimeout != 0) && ((VX1000_FKL_WORKSPACE)->TransitionTimeout != 0)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    &&   (((VX1000_FKL_WORKSPACE)->TransitionTimeout != 0) && ((VX1000_FKL_WORKSPACE)->TransitionTimeout != 0)) ) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    {
      (VX1000_FKL_WORKSPACE)->TransitionTimeout--; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      VX1000_SERVE_WATCHDOG(&((VX1000_FKL_WORKSPACE)->WdgData1)) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
    }
    else
    {
      switch ((((VX1000_UINT32)((VX1000_FKL_WORKSPACE)->DeprotectState) << 8U) << 8U) | (VX1000_FKL_WORKSPACE)->DeprotectTrigger) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
      {
      case ((VX1000_UINT32)(VX1000_FKL_STATE1CODE) << 16) | (VX1000_FKL_TOSTATE1CODE):
        /* still in the initialisation phase */
        break;
      case ((VX1000_UINT32)(VX1000_FKL_STATE1CODE) << 16) | (VX1000_FKL_TOSTATE2CODE):
        (VX1000_FKL_WORKSPACE)->TransitionTimeout = (VX1000_UINT32)(VX1000_FKL_TRANSITION_TIMEOUT); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        (VX1000_FKL_WORKSPACE)->DeprotectState = VX1000_FKL_STATE2CODE; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        break;
      case ((VX1000_UINT32)(VX1000_FKL_STATE2CODE) << 16) | (VX1000_FKL_TOSTATE3CODE):
        (VX1000_FKL_WORKSPACE)->TransitionTimeout = (VX1000_UINT32)(VX1000_FKL_TRANSITION_TIMEOUT); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        (VX1000_FKL_WORKSPACE)->DeprotectState = VX1000_FKL_STATE3CODE; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        break;
      case ((VX1000_UINT32)(VX1000_FKL_STATE3CODE) << 16) | (VX1000_FKL_TOSTATE4CODE):
        (VX1000_FKL_WORKSPACE)->TransitionTimeout = (VX1000_UINT32)(VX1000_FKL_TRANSITION_TIMEOUT); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        (VX1000_FKL_WORKSPACE)->DeprotectState = VX1000_FKL_STATE4CODE; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        break;
      case ((VX1000_UINT32)(VX1000_FKL_STATE4CODE) << 16) | (VX1000_FKL_TOSTATE5CODE):
        (VX1000_FKL_WORKSPACE)->TransitionTimeout = (VX1000_UINT32)(VX1000_FKL_TRANSITION_TIMEOUT); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        (VX1000_FKL_WORKSPACE)->DeprotectState = VX1000_FKL_STATE5CODE; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        break;
      case ((VX1000_UINT32)(VX1000_FKL_STATE5CODE) << 16) | (VX1000_FKL_LAUNCHCODE):
        (VX1000_FKL_WORKSPACE)->TransitionTimeout = 1UL; /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        if (VX1000_ADDR_TO_PTR2VU32(VX1000_PTR2VFF_TO_ADDRESS(&((VX1000_FKL_WORKSPACE)->EntryPoint)))[0] != 0xFFFFFFFFUL) /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        {
          ((VX1000_FKL_WORKSPACE)->EntryPoint)((VX1000_FKL_WORKSPACE)->FklParam1, (VX1000_FKL_WORKSPACE)->FklParam2, (VX1000_FKL_WORKSPACE)->WdgData3_FklParam3);  /* call the FKL: will not return here */ /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */
        }
        break;
      default:
        /* bad trigger sequence detected */
        while (1) /* intentional infinite loop without watchdog serving, will lead to watchdog reset */
        {
          /* intentionally left empty */
        }
        /*break; /-* unreachable code, intentionally here to please MISRA checkers */
      }
    }
  }
#else /* VX1000_FKL_BOUNCEBACK */
  /* The user wants that the request is reflected back to the tool. */
  gVX1000.ToolDetectState |= (VX1000_UINT32)(VX1000_TDS_FKL_REQ_IGNORED);
  /* While the API now returns to the application, the VX device will start trying to force the ECU into debug/freeze mode */
#endif /* VX1000_FKL_BOUNCEBACK */
}
#endif /* VX1000_FKL_SUPPORT_ADDR  && !VX1000_COMPILED_FOR_SLAVECORES */

#if (VX1000_ERRLOG_SIZE != 0)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: vx1000_ErrLogger                                                                                            */
/* API name:      VX1000_ERRLOGGER (internal)                                                                                 */
/* Return value:  None                                                                                                        */
/* Parameter1:    An error code.                                                                                              */
/* Preemption:                                                                                                                */
/* Termination:                                                                                                               */
/* Precondition1: vx1000_InitAsyncStart() must have been called.                                                              */
/* Description:   Log the error code to a ring buffer for later debug/support purposes                                        */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
void VX1000_SUFFUN(vx1000_ErrLogger)( VX1000_UINT16 errorcode )
{
  VX1000_UINT8 currentI, nextI;
  VX1000_UINT16 *logbuf = VX1000_ADDR_TO_PTR2U16(gVX1000.ErrLogAddr); /* PRQA S 0303 */ /* PRQA S 0306 */ /* cannot avoid violating MISRA rule 11.3 because addresses to be dereferenced are either exported as integer types to user/external tool or provided as such by user/external tool/hardware description, therefore VX1000_ADDR_TO_PTR2XXX/VX1000_PTR2XXX_TO_ADDRESS (either called directly or by macro expansion) requires a typecast */

  VX1000_ENTER_SPINLOCK()
  currentI = gVX1000.ErrLogIndex;
  nextI = currentI + 1;
  if (nextI >= gVX1000.ErrLogSize) { nextI = 0; }
  gVX1000.ErrLogIndex = nextI;
  VX1000_LEAVE_SPINLOCK()
  logbuf[currentI] = errorcode;
}
#endif /* VX1000_ERRLOG_SIZE */

#if !defined(VX1000_COMPILED_FOR_SLAVECORES) /* global data have to exist only once (in shared memory) */

/* The following is VX1000_DATA, but split to allow separate linkage of gVX1000 at a user-defined address */
#if defined(VX1000_STIM_BENCHMARK_DATA)
VX1000_STIM_BENCHMARK_DATA;
#endif /* VX1000_STIM_BENCHMARK_DATA */
#if defined(VX1000_ECUID_DATA)
VX1000_ECUID_DATA;
#endif /* VX1000_ECUID_DATA */
#if defined(VX1000_OLDA_BENCHMARK_DATA)
VX1000_OLDA_BENCHMARK_DATA;
#endif /* VX1000_OLDA_BENCHMARK_DATA */
#if defined(VX1000_COLDSTART_BENCHMARK_DATA)
VX1000_COLDSTART_BENCHMARK_DATA;
#endif /* VX1000_COLDSTART_BENCHMARK_DATA */

/* include user-defined lines with optional section pragmas to force individual linkage of VX1000 structure data. */
#define VX1000_BEGSECT_VXSTRUCT_C
#include "VX1000_cfg.h" /* PRQA S 5087 */ /* willingly violating MISRA rule 19.1 because the section pragmas are needed exactly here (and not at the start of the file) */
#define VX1000_BEGSECT_VXSTRUCT_C_UNDO

#if !defined(VX1000_USE_EXTERNAL_STRUCT_DATA)
#if defined(VX1000_STRUCT_DATA)
VX1000_STRUCT_DATA;
#endif /* VX1000_STRUCT_DATA */
#endif /* !VX1000_USE_EXTERNAL_STRUCT_DATA */

/* include user-defined lines with optional section pragmas to restore previous linkage of data: */
#define VX1000_ENDSECT_VXSTRUCT_C
#include "VX1000_cfg.h" /* PRQA S 5087 */ /* willingly violating MISRA rule 19.1 because the section pragmas are needed exactly here (and not at the start of the file) */
#define VX1000_ENDSECT_VXSTRUCT_C_UNDO

#endif /* !VX1000_COMPILED_FOR_SLAVECORES */

/* Note that we were using nested section switches here (which might be unsupported by some compilers but has the    */
/* advantage of not requiring #undefs (those would violate the MISRA coding guidelines) nor VX1000_xxxSECT_2         */
/* defines (those would spoil the yyy_cfg.h file). This means that we SHOULDN'T PLACE ANY FURTHER CODE OR DATA       */
/* below (with the exception of external user code/data that explicitly shall NOT go into VX sections).              */

/* include user-defined lines with optional section pragmas to restore standard linkage of code and/or data: */
#define VX1000_ENDSECT_VXMODULE_C
#include "VX1000_cfg.h" /* PRQA S 5087 */ /* willingly violating MISRA rule 19.1 because the section pragmas are needed exactly here (and not at the start of the file) */
#define VX1000_ENDSECT_VXMODULE_C_UNDO

#if !defined(VX1000_COMPILED_FOR_SLAVECORES) /* global data have to exist only once (in shared memory) */

/* include user-defined lines with optional section pragmas to force individual linkage of EMEM header data. */
#define VX1000_BEGSECT_EMEM_HDR_C
#include "VX1000_cfg.h" /* PRQA S 5087 */ /* willingly violating MISRA rule 19.1 because the section pragmas are needed exactly here (and not at the start of the file) */
#define VX1000_BEGSECT_EMEM_HDR_C_UNDO

#if defined(VX1000_EMEM_HDR_DATA)
VX1000_EMEM_HDR_DATA;
#endif /* VX1000_EMEM_HDR_DATA */

/* include user-defined lines with optional section pragmas to restore standard linkage of code and/or data: */
#define VX1000_ENDSECT_EMEM_HDR_C
#include "VX1000_cfg.h" /* PRQA S 5087 */ /* willingly violating MISRA rule 19.1 because the section pragmas are needed exactly here (and not at the start of the file) */
#define VX1000_ENDSECT_EMEM_HDR_C_UNDO

#endif /* !VX1000_COMPILED_FOR_SLAVECORES */

#endif /* !VX1000_DISABLE_INSTRUMENTATION */

