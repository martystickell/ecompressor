/*------------------------------------------------------------------------------- */
/* VX1000.h                                                                       */
/* Vector VX1000 Application Driver for Infineon Tricore with DAP1/2 or DigRf I/F */
/* Release: 03_06                    Build: 596          Modification: 21.10.2017 */
/* Vector Informatik GmbH                                                         */
/*                                                                                */
/* Don't modify this file, settings are defined in VX1000_cfg.h                   */
/*------------------------------------------------------------------------------- */


/* ----------------------------------------------------------------------------------------------------------------------- /
/ Status of MISRA conformance:                                                                                             /
/ ---------------------------                                                                                              /
/  * advisory rule 12.11 "wraparound in const unsigned int computation"                                                    /
/     - violated only in a preprocessor assertion (user-configuration-dependent)                                           /
/                                                                                                                          /
/  * required rule 16.1 "usage of variadic functions"                                                                      /
/     - violated only if the user explicitly enables the debug print feature during development phase.                     /
/     - see individual justifications of the particular violations in the code                                             /
/                                                                                                                          /
/  * required rule 19.4 "complete statement inside a macro"                                                                /
/     - violated to allow the application to generically call the API without having to care whether a feature             /
/       is currently disabled or enabled.                                                                                  /
/     - see individual justifications of the particular violations in the code                                             /
/                                                                                                                          /
/  * advisory rule 19.7 "functions vs. macros"                                                                             /
/     - violated because a MISRA-conform workaround would induce configuration-dependent MISRA violations in user's code   /
/     - see individual justifications of the particular violations in the code                                             /
/                                                                                                                          /
/  * required rule 19.10 "macro parameter used without parentheses"                                                        /
/     - todo ( hint: a dummy-"##" might serve as a workaround... )                                                         /
/                                                                                                                          /
/                                                                            (checked in PowerPC build 529)                /
/                                                                                                                          /
/------------------------------------------------------------------------------------------------------------------------ */


#if !defined(VX1000_H)
#define VX1000_H


/*------------------------------------------------------------------------------ */
/* Driver's version and status                                                   */

#define VX1000_APPDRIVER_BUILDNUMBER   596UL
#define VX1000_RELEASENAMEARRAY        '0','3','_','0','6',' ',' ',' ',' ',' ',' ',' '
/* semantic versioning no longer used starting with release 3.0:
/ #define VX1000_FILE_VERSION_STATUS     (VX1000_ALPHA)  /-* VX1000_RELEASED / VX1000_TESTED / VX1000_BETA / VX1000_ALPHA; a change does NOT force version increase *-/
/ #define VX1000_FILE_VERSION_MAJOR      3U   /-* major version number (1..16); a change initially forces ALPHA status *-/
/ #define VX1000_FILE_VERSION_MINOR      0U   /-* minor version number (0..99); a change initially forces BETA status  *-/
/ #define VX1000_FILE_VERSION_PATCH      3U   /-* patch level (0..7); a change does NOT change VERSION_STATUS *-/
/
/ #define VX1000_FILE_VERSION ( ((((VX1000_FILE_VERSION_MAJOR) & 0xfU) << 12U) | (((VX1000_FILE_VERSION_MINOR) & 0x7fU) << 5U)) | (((VX1000_FILE_VERSION_STATUS) << 3U) | ((VX1000_FILE_VERSION_PATCH) & 0x7U)) )
*/

/*------------------------------------------------------------------------------ */
/* data types                                                                    */

#if defined(_C166)
#define VX1000_TASKING_WRAPDEF_C166    _C166 /* map to the reserved name for MISRA checkers that do not like to see leading single underscores in user code */
#elif defined(__C166__) && defined(__VERSION__)
#define VX1000_TASKING_WRAPDEF_C166    __VERSION__ /* map to the reserved name for MISRA checkers that do not like to see surrounding underscores in user code */
#else  /* !_C166 && !__C166__ */
#define VX1000_TASKING_WRAPDEF_C166    0 /* create an impossible version as workaround for some MISRA check tools not allowing negative defined-check and optional value-comparision on same line */
#endif /* !_C166 && !__C166__ */

#if (defined(__GNUC__) || defined(__GNUG__)) && (!defined(__GNUC_PATCHLEVEL__)) /* is the GNU C or the GNU C++ toolchain compiling this source for XC2000? */
/* GCC provides these memory models:
   Small
   Compact
   Medium (special)
   Large

It allows to assign specific attributes like structure packing per object by appending e.g. "__attribute__ ((aligned (16), packed))".
*/
typedef unsigned long  VX1000_UINT32;  /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef unsigned short VX1000_UINT16;  /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef unsigned char  VX1000_UINT8;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed long    VX1000_INT32;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed short   VX1000_INT16;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed char    VX1000_INT8;    /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */


#define VX1000_INNERSTRUCT_VOLATILE    volatile
#define VX1000_STRUCTSTRUCT_VOLATILE   /* empty */
#define VX1000_OUTERSTRUCT_VOLATILE    /* empty */

#if defined(__MODEL_LARGE__)
#define VX1000_DECL_PTR_DEFAULT             huge /* ? */
#define VX1000_ADDRESS_TO_POINTER_DFLT(A, T)   ((T huge *)(T far *)(T huge *)(A)) /* PRQA S 3410 */ /* Willingly violating MISRA rule 19.10 because extra parenthesis would cause compilation to fail */
#define VX1000_POINTER_TO_ADDRESS_DFLT(P, T)   ((VX1000_UINT32)((T huge *)(P)))  /* this may complain about lost qualifiers - this cannot be avoided (an intermediate cast to non-pointer type would destroy the page bits) */
#elif defined(__MODEL_SPECMED__)
#define VX1000_DECL_PTR_DEFAULT             huge /* ? */
#define VX1000_ADDRESS_TO_POINTER_DFLT(A, T)   ((T huge *)                                                                        \
({                                                                                                                             \
  __typeof__(A) TMP = (A);                                                                                                     \
  VX1000_UINT16 PAGE = TMP >> 14;                                                                                              \
  VX1000_UINT16 OFS  = TMP & 0x3FFFU;                                                                                          \
  if      (PAGE == ({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E00\n":"=&r"(DPP)); DPP;})) /* already OK */; \
  else if (PAGE == ({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E02\n":"=&r"(DPP)); DPP;})) OFS |= 1<<14;     \
  else if (PAGE == ({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E04\n":"=&r"(DPP)); DPP;})) OFS |= 2<<14;     \
  else if (PAGE == ({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E06\n":"=&r"(DPP)); DPP;})) OFS |= 3<<14;     \
  else { OFS = 0; /* not representable --> create a null pointer */ VX1000_ERRLOGGER(VX1000_ERRLOG_PTR_NONREPSTBL) }           \
  ((T *)OFS;                                                                                                                   \
})) /* PRQA S 3410 */ /* Willingly violating MISRA rule 19.10 because extra parenthesis would cause compilation to fail */
#define VX1000_POINTER_TO_ADDRESS_DFLT(P, T)   ((VX1000_UINT32)((T huge*)(P)))  /* this may complain about lost qualifiers - this cannot be avoided (an intermediate cast to non-pointer type would destroy the page bits) */
#elif defined(__MODEL_COMPACT__)
#define VX1000_DECL_PTR_DEFAULT        /* empty (?) */
#define VX1000_ADDRESS_TO_POINTER_DFLT(A, T)   ((T *)                                                                               \
({                                                                                                                             \
  __typeof__(A) TMP = (A);                                                                                                     \
  VX1000_UINT16 PAGE = TMP >> 14;                                                                                              \
  VX1000_UINT16 OFS  = TMP & 0x3FFFU;                                                                                          \
  if      (PAGE == ({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E00\n":"=&r"(DPP)); DPP;})) /* already OK */; \
  else if (PAGE == ({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E02\n":"=&r"(DPP)); DPP;})) OFS |= 1<<14;     \
  else if (PAGE == ({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E04\n":"=&r"(DPP)); DPP;})) OFS |= 2<<14;     \
  else if (PAGE == ({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E06\n":"=&r"(DPP)); DPP;})) OFS |= 3<<14;     \
  else { OFS = 0; /* not representable --> create a null pointer */  VX1000_ERRLOGGER(VX1000_ERRLOG_PTR_NONREPSTBL) }          \
  (VX1000_UINT16 *)OFS;                                                                                                        \
})) /* Willingly violating MISRA rule 19.10 because extra parenthesis would cause compilation to fail */
#define VX1000_POINTER_TO_ADDRESS_DFLT(P, T)                                                                                                                                             \
({                                                                                                                                                                               \
  VX1000_UINT32 TMP = (VX1000_UINT32)(P);                                                                                                                                        \
  switch (((VX1000_UINT16)(TMP)) >> 14U)                                                                                                                                         \
  {                                                                                                                                                                              \
    case 0U: TMP = (((VX1000_UINT32)({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E00\n":"=&r"(DPP)); DPP;})) << 14U) | (VX1000_UINT32)((TMP) & 0x3FFFU); break; \
    case 1U: TMP = (((VX1000_UINT32)({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E02\n":"=&r"(DPP)); DPP;})) << 14U) | (VX1000_UINT32)((TMP) & 0x3FFFU); break; \
    case 2U: TMP = (((VX1000_UINT32)({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E04\n":"=&r"(DPP)); DPP;})) << 14U) | (VX1000_UINT32)((TMP) & 0x3FFFU); break; \
    case 3U: TMP = (((VX1000_UINT32)({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E06\n":"=&r"(DPP)); DPP;})) << 14U) | (VX1000_UINT32)((TMP) & 0x3FFFU); break; \
  }                                                                                                                                                                              \
  TMP;                                                                                                                                                                           \
})
#else /* small */
#define VX1000_DECL_PTR_DEFAULT        /* empty */
#define VX1000_ADDRESS_TO_POINTER_DFLT(A, T)   ((T *)                                                                               \
({                                                                                                                             \
  __typeof__(A) TMP = (A);                                                                                                     \
  VX1000_UINT16 PAGE = TMP >> 14;                                                                                              \
  VX1000_UINT16 OFS  = TMP & 0x3FFFU;                                                                                          \
  if      (PAGE == ({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E00\n":"=&r"(DPP)); DPP;})) /* already OK */; \
  else if (PAGE == ({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E02\n":"=&r"(DPP)); DPP;})) OFS |= 1<<14;     \
  else if (PAGE == ({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E04\n":"=&r"(DPP)); DPP;})) OFS |= 2<<14;     \
  else if (PAGE == ({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E06\n":"=&r"(DPP)); DPP;})) OFS |= 3<<14;     \
  else { OFS = 0; /* not representable --> create a null pointer */ VX1000_ERRLOGGER(VX1000_ERRLOG_PTR_NONREPSTBL) }           \
  (T *) OFS;                                                                                                                   \
})) /* PRQA S 3410 */ /* Willingly violating MISRA rule 19.10 because extra parenthesis would cause compilation to fail */
#define VX1000_POINTER_TO_ADDRESS_DFLT(P, T)                                                                                                                                             \
({                                                                                                                                                                               \
  VX1000_UINT32 TMP = (VX1000_UINT32)(P);                                                                                                                                        \
  switch (((VX1000_UINT16)(TMP)) >> 14U)                                                                                                                                         \
  {                                                                                                                                                                              \
    case 0U: TMP = (((VX1000_UINT32)({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E00\n":"=&r"(DPP)); DPP;})) << 14U) | (VX1000_UINT32)((TMP) & 0x3FFFU); break; \
    case 1U: TMP = (((VX1000_UINT32)({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E02\n":"=&r"(DPP)); DPP;})) << 14U) | (VX1000_UINT32)((TMP) & 0x3FFFU); break; \
    case 2U: TMP = (((VX1000_UINT32)({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E04\n":"=&r"(DPP)); DPP;})) << 14U) | (VX1000_UINT32)((TMP) & 0x3FFFU); break; \
    case 3U: TMP = (((VX1000_UINT32)({VX1000_UINT16 DPP; __asm__ volatile ("extp #3,#1\nmov %0,0x3E06\n":"=&r"(DPP)); DPP;})) << 14U) | (VX1000_UINT32)((TMP) & 0x3FFFU); break; \
  }                                                                                                                                                                              \
  TMP;                                                                                                                                                                           \
})
#endif /* small or compact */
#define VX1000_OBJAT /* __attribute__((aligned(ADDR))) -- empty: GCC does not support this tasking-proprietary language extension. Macro's semantic must be changed --> pass address as parameter! */


#elif (defined(_C166) && (((VX1000_TASKING_WRAPDEF_C166) >= 60) && ((VX1000_TASKING_WRAPDEF_C166) <= 100))) /* is it a valid tasking_classic compiler version for XC2000? */
/*
Tasking classic offers these pointer classes to c programmers:
   _xnear       --> 14-bit-arithmetic inside DPP1 page (DPP1 must point to the page that contains the stack)
   _near        --> 14-bit-arithmetic inside DPP2/DPP0 page (DPP2/DPP0 defined freely at link stage for data/const)
   _system      --> 14-bit-arithmetic inside DPP3 page with DPP3 being fixed to the CPU-SFR page
   _far         --> 14-bit-arithmetic inside a page addressed by bits 16..25. (note: far function pointers handled differently!)
   _shuge       --> 16-bit-arithmetic inside any segment (the compiler generates code to push-modify-use-pop a DPP of its choice)
   _huge        --> 24-bit-arithmetic also across page/segment boundaries (the compiler generates EXTS-prefixes for each access)
   _iram        --> same as _system, just that object size is further limited
   _bita        --> same as _iram, just that object types are further limited

Whether a class is actually available depends on the "-m XXX" command line switch that selects one of these memory models:
   tiny         --> forbids usage of _xnear, _far, _shuge, _huge; implies _near for everything; DPPx fixed for "linear-like" addresses
   small        --> implies _near for const and data and _huge for text
   medium       --> implies _near for text
   large        --> implies _far for const and data and _huge for text
   huge         --> implies _huge for everything

The compiler provides an intrinsic define called "_MODEL" so an application can choose the right pointer class e.g. for "'t'".
*/
typedef unsigned long  VX1000_UINT32;  /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef unsigned short VX1000_UINT16;  /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef unsigned char  VX1000_UINT8;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed long    VX1000_INT32;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed short   VX1000_INT16;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed char    VX1000_INT8;    /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */

#if (_MODEL == 't')                    /* 16bit linear addressing, pointers are _always_ 2 bytes */
#define VX1000_DECL_PTR_DEFAULT        _near
#define VX1000_ADDRESS_TO_POINTER_DFLT(A, T)   ((T _near *)(A)) /* PRQA S 3410 */ /* Willingly violating MISRA rule 19.10 because extra parenthesis would cause compilation to fail */
#define VX1000_POINTER_TO_ADDRESS_DFLT(P, T)   ((VX1000_UINT32)(P))
#else /* _MODEL != 't' */              /* potentially weird addressing, force pointers to use 24bit linear addressing, stored in 4 bytes */
#define VX1000_DECL_PTR_DEFAULT _huge
#define VX1000_ADDRESS_TO_POINTER_DFLT(A, T)   ((T _huge *)(A)) /* PRQA S 3410 */ /* Willingly violating MISRA rule 19.10 because extra parenthesis would cause compilation to fail */
#define VX1000_POINTER_TO_ADDRESS_DFLT(P, T)   ((VX1000_UINT32)(P))
#endif /* _MODEL != 't' */
#define VX1000_OBJAT                   _at


#elif defined (__C166__)               /* is it any other XC2000 tool chain? */

#if (defined(__VERSION__) && (((VX1000_TASKING_WRAPDEF_C166) >= 1000) && ((VX1000_TASKING_WRAPDEF_C166) <= 5000))) /* is it a valid tasking_VX compiler version? */

/*
Tasking VX offers these pointer classes to c programmers:
   __near       --> 16-bit-arithmetic using virtual 64K address space (fix DPPx then freely map the result to physical 16M address space)
   __far        --> 14-bit-arithmetic inside a page addressed by bits 16..25 (the compiler generates EXTP-prefixes for each access)
   __shuge      --> 16-bit-arithmetic inside any segment (the compiler generates EXTS-prefixes for each access)
   __huge       --> 24-bit-arithmetic also across page/segment boundaries
   __iram       --> same as __near, just that address range is further limited
   __bita       --> same as __iram, just that object types are further limited
   __bit        --> same as __iram, just that object types are further limited and using only 12-bit pointer-arithmetic

The default pointer class for const and data depends on the "--model=XXX" command line switch that selects one of these memory models:
   near         --> implies __near (attention with the __CONTIGUOUS_NEAR trick: depending on the DPPx initialisation, results of near versus non-near pointer arithmetic may differ!)
   far          --> implies __far
   shuge        --> implies __shuge
   huge         --> implies __huge

Note that the "--near-threshold=xxx" command line switch may overrule the default for specific (small) objects.
The default pointer class for functions is always __huge unless overridden to __near via the "--near-functions" command line switch.

The compiler provides an intrinsic define called "__MODEL__" so an application can choose the right pointer class e.g. for "'h'".
*/
typedef unsigned long  VX1000_UINT32;  /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef unsigned short VX1000_UINT16;  /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef unsigned char  VX1000_UINT8;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed long    VX1000_INT32;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed short   VX1000_INT16;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed char    VX1000_INT8;    /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */

#define VX1000_DECL_PTR_DEFAULT             __huge
#define VX1000_ADDRESS_TO_POINTER_DFLT(A, T) ((T __huge *)(A)) /* Willingly violating MISRA rule 19.10 because extra parenthesis would cause compilation to fail */
#define VX1000_POINTER_TO_ADDRESS_DFLT(P, T) ((VX1000_UINT32)(P))
#define VX1000_OBJAT                        __at
#define VX1000_INNERSTRUCT_VOLATILE         volatile
#define VX1000_STRUCTSTRUCT_VOLATILE        /* empty */
#define VX1000_OUTERSTRUCT_VOLATILE         /* empty */

#elif (__C166__ != 1)                  /* _seems_ to be a Keil compiler (?) -- completely untested! */

/* KEIL provides these memory models:
   Tiny
   Small
   Compact
   Hcompact
   Medium
   Large
   Hlarge

It supports the "#pragma pack" and the "#pragma bytealign" switches to change structure member padding, but probably default packing is OK for our uint16s.
*/
typedef unsigned long  VX1000_UINT32;  /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef unsigned short VX1000_UINT16;  /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef unsigned char  VX1000_UINT8;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed long    VX1000_INT32;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed short   VX1000_INT16;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed char    VX1000_INT8;    /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */

#define VX1000_DECL_PTR_DEFAULT             xhuge /* might need a 2nd define that is used everywhere IN FRONT of the type (?) */
#define VX1000_ADDRESS_TO_POINTER_DFLT(A, T) ((T xhuge *)(A))  /* might be ((xhuge VX1000_UINT8 *)(p)) instead? */ /* PRQA S 3410 */ /* Willingly violating MISRA rule 19.10 because extra parenthesis would cause compilation to fail */
#define VX1000_POINTER_TO_ADDRESS_DFLT(P, T) ((VX1000_UINT32)(P))
#define VX1000_OBJAT                        /* empty: keil does not support this tasking-proprietary language extension. Standard section method must be used! */
#define VX1000_INNERSTRUCT_VOLATILE         volatile
#define VX1000_STRUCTSTRUCT_VOLATILE        /* empty */
#define VX1000_OUTERSTRUCT_VOLATILE         /* empty */

#else  /* !TaskingVX & !Keil */
typedef unsigned long  VX1000_UINT32;  /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef unsigned short VX1000_UINT16;  /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef unsigned char  VX1000_UINT8;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed long    VX1000_INT32;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed short   VX1000_INT16;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed char    VX1000_INT8;    /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
#define VX1000_OBJAT                   /* empty: user shall use the pragmas offered by his unsupported compiler! */

#endif /* !TaskingVX & !Keil */

#else /* !<C166/C167/XC164/XC2000/XE166 compiler> */

/* The user header must be able to provide callback prototypes with VX data types, therefore the types must be located before the header include */
typedef unsigned long  VX1000_UINT32;  /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef unsigned short VX1000_UINT16;  /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef unsigned char  VX1000_UINT8;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed long    VX1000_INT32;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed short   VX1000_INT16;   /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef signed char    VX1000_INT8;    /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */

#if !defined(VX1000_DECL_PTR_DEFAULT)
#define VX1000_DECL_PTR_DEFAULT             /* empty */
#endif /* !VX1000_DECL_PTR_DEFAULT */
#if !defined(VX1000_ADDRESS_TO_POINTER_DFLT)
#define VX1000_ADDRESS_TO_POINTER_DFLT(A, T) ((/*NO parentheses here!*/T *)(A)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */ /* PRQA S 3410 */ /* Willingly violating MISRA rule 19.10 because extra parenthesis would cause compilation to fail */
#endif /* !VX1000_ADDRESS_TO_POINTER_DFLT */
#if !defined(VX1000_POINTER_TO_ADDRESS_DFLT)
#define VX1000_POINTER_TO_ADDRESS_DFLT(P, T) ((VX1000_UINT32)(P)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_POINTER_TO_ADDRESS_DFLT */
#define VX1000_INNERSTRUCT_VOLATILE         /* empty */
#define VX1000_STRUCTSTRUCT_VOLATILE        /* empty */
#define VX1000_OUTERSTRUCT_VOLATILE         volatile

#endif /* !<C166/C167/XC164/XC2000/XE166 compiler> */

typedef int            VX1000_INT;     /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef long           VX1000_LONG;    /* needed for "sizeof" expression */ /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef char           VX1000_CHAR;    /* used by strings, __date etc.; compile options may specify whether signed or not */ /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef float          VX1000_FLOAT;   /* needed for "sizeof" expression */ /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef double         VX1000_DOUBLE;  /* needed for "sizeof" expression */ /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef long double    VX1000_LDOUBL;  /* needed for "sizeof" expression */ /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef void (*VX1000_VVFUNCP_T)(void);                                     /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef VX1000_UINT32  /* codeSize in DWords */ (VX1000_VUFUNC_T)(VX1000_UINT32 Addr/*DAQ: dstAddr / STIM: srcAddr*/ , VX1000_UINT32 t0, VX1000_UINT32 evSync); /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef VX1000_VUFUNC_T * VX1000_VUFUNCP_T;


/*------------------------------------------------------------------------------ */
/* constants                                                                     */
#define VX1000_RELEASED                0x0U
#define VX1000_TESTED                  0x1U
#define VX1000_BETA                    0x2U
#define VX1000_ALPHA                   0x3U


/*------------------------------------------------------------------------------ */
/* Configuration                                                                 */

/* Include user specified defines and fetch prototypes of user callbacks (has to be done before switching sections) */
#include "VX1000_cfg.h"  /* PRQA S 0883 */ /* Willingly violating MISRA rule 19.15: this file is included multiple times because it contains multiple disjunct parts that each are filtered exactly once by preprocessor conditions */ /* PRQA S 5087*/ /* Willingly violating MISRA rule 19.1: this file is included multiple at exactly those lines where it is needed to expand to the desired lines. Inclusion/expansion of those lines at the top of this file would not make sense and lead to compile errors */

#if defined(VX1000_TARGET_X850)
#elif defined(VX1000_TARGET_POWERPC)
#elif defined(VX1000_TARGET_SH2)
#elif defined(VX1000_TARGET_TMS570)
#elif defined(VX1000_TARGET_TRICORE)
#elif defined(VX1000_TARGET_XC2000)
#else /* !VX1000_TARGET_<target> */
#define VX1000IF_CFG_NO_TARGET
#endif /* !VX1000_TARGET_<target> */
#if defined(VX1000IF_CFG_NO_TARGET)
#error You must define the appropriate VX1000_TARGET_<target> in VX1000_cfg.h!
#endif /* !VX1000IF_CFG_NO_TARGET */

#if !defined(VX1000_DISABLE_INSTRUMENTATION)
#if defined(VX1000_TARGET_XC2000) || (defined(VX1000_TARGET_X850) /* || defined(VX1000_TARGET_TRICORE) */)
/* no 64bit integer data type known on XC2000/V850 ... must live without it (and indeed we can) */
#else  /* !VX1000_TARGET_XC2000 & !VX1000_TARGET_X850 & !VX1000_TARGET_TRICORE */

/* As 64bit is no standard data type, it normally comes from the user. Therefore the default definition must be placed after the include of the user header: */
#if !defined(VX1000_UINT64)
#define VX1000_UINT64 VX1000_UINT64_T
typedef long unsigned long  VX1000_UINT64_T; /* note: 'long long' is a language extension that is not allowed by MISRA */ /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
#endif /* !VX1000_UINT64 */
#endif /* !VX1000_DISABLE_INSTRUMENTATION */

#endif /* !VX1000_TARGET_XC2000 & !VX1000_TARGET_X850 & !VX1000_TARGET_TRICORE */

#if ((!defined(VX1000_DECL_PTR_DEFAULT)) && (!defined(VX1000_DECL_PTR))) || (((!defined(VX1000_ADDRESS_TO_POINTER_DFLT)) && (!defined(VX1000_ADDRESS_TO_POINTER))) || ((!defined(VX1000_POINTER_TO_ADDRESS_DFLT)) && (!defined(VX1000_POINTER_TO_ADDRESS))))
#error Unsupported compiler; please provide the macros VX1000_DECL_PTR, VX1000_ADDRESS_TO_POINTER and VX1000_POINTER_TO_ADDRESS!
#endif /* !VX1000_DECL_PTR_DEFAULT | !VX1000_ADDRESS_TO_POINTER | !VX1000_POINTER_TO_ADDRESS */

#if (!defined(VX1000_INNERSTRUCT_VOLATILE)) || ((!defined(VX1000_STRUCTSTRUCT_VOLATILE)) || (!defined(VX1000_OUTERSTRUCT_VOLATILE)))
#error Unsupported compiler; please provide the macros VX1000_INNERSTRUCT_VOLATILE, VX1000_STRUCTSTRUCT_VOLATILE and VX1000_OUTERSTRUCT_VOLATILE!
#endif /* !VX1000_INNERSTRUCT_VOLATILE | !VX1000_STRUCTSTRUCT_VOLATILE | !VX1000_OUTERSTRUCT_VOLATILE */


#if !defined(VX1000_DECL_PTR)
#define VX1000_DECL_PTR                VX1000_DECL_PTR_DEFAULT
#endif /* !VX1000_DECL_PTR */
#if !defined(VX1000_ADDRESS_TO_POINTER)
#define VX1000_ADDRESS_TO_POINTER(A, T)    (VX1000_ADDRESS_TO_POINTER_DFLT((A), /*NO parentheses here!*/T)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */ /* PRQA S 3410 */ /* Willingly violating MISRA rule 19.10 because extra parenthesis would cause compilation to fail */
#endif /* !VX1000_ADDRESS_TO_POINTER */
#if !defined(VX1000_POINTER_TO_ADDRESS)
#define VX1000_POINTER_TO_ADDRESS(P, T)    (VX1000_POINTER_TO_ADDRESS_DFLT((P), /*NO parentheses here!*/T)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */ /* PRQA S 3410 */ /* Willingly violating MISRA rule 19.10 because extra parenthesis would cause compilation to fail */
#endif /* !VX1000_POINTER_TO_ADDRESS */



#if !defined(VX1000_ADDR_TO_PTR2U16)
#define VX1000_ADDR_TO_PTR2U16(A)      VX1000_ADDRESS_TO_POINTER((A), VX1000_UINT16)                  /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2U16 */
#if !defined(VX1000_ADDR_TO_PTR2VMC)
#define VX1000_ADDR_TO_PTR2VMC(A)      VX1000_ADDRESS_TO_POINTER((A), volatile VX1000_MEMSYNC_CPY_T)  /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2VMC */
#if !defined(VX1000_ADDR_TO_PTR2VU32)
#define VX1000_ADDR_TO_PTR2VU32(A)     VX1000_ADDRESS_TO_POINTER((A), volatile VX1000_UINT32)         /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2VU32 */
#if !defined(VX1000_ADDR_TO_PTR2VU16)
#define VX1000_ADDR_TO_PTR2VU16(A)     VX1000_ADDRESS_TO_POINTER((A), volatile VX1000_UINT16)         /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2VU16 */
#if !defined(VX1000_ADDR_TO_PTR2VF)
#define VX1000_ADDR_TO_PTR2VF(A)       VX1000_ADDRESS_TO_POINTER((A), volatile VX1000_FLOAT)          /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2VF */
#if !defined(VX1000_ADDR_TO_PTR2VD)
#define VX1000_ADDR_TO_PTR2VD(A)       VX1000_ADDRESS_TO_POINTER((A), volatile VX1000_DOUBLE)         /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2VD */
#if !defined(VX1000_ADDR_TO_PTR2VMT)
#define VX1000_ADDR_TO_PTR2VMT(A)      VX1000_ADDRESS_TO_POINTER((A), volatile VX1000_MEMSYNC_TRIG_T) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2VMT */
#if !defined(VX1000_ADDR_TO_PTR2VU8)
#define VX1000_ADDR_TO_PTR2VU8(A)      VX1000_ADDRESS_TO_POINTER((A), volatile VX1000_UINT8)          /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2VU8 */
#if !defined(VX1000_ADDR_TO_PTR2U8)
#define VX1000_ADDR_TO_PTR2U8(A)       VX1000_ADDRESS_TO_POINTER((A), VX1000_UINT8)                   /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2U8 */
#if !defined(VX1000_ADDR_TO_PTR2EH)
#define VX1000_ADDR_TO_PTR2EH(A)       VX1000_ADDRESS_TO_POINTER((A), VX1000_EMEM_HDR_T)              /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2EH */
#if !defined(VX1000_ADDR_TO_PTR2FW)
#define VX1000_ADDR_TO_PTR2FW(A)       VX1000_ADDRESS_TO_POINTER((A), VX1000_FKL_WORKSPACE_T)         /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2FW */
#if !defined(VX1000_ADDR_TO_PTR2VU64)
#define VX1000_ADDR_TO_PTR2VU64(A)     VX1000_ADDRESS_TO_POINTER((A), volatile VX1000_UINT64)         /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2VU64 */
#if !defined(VX1000_ADDR_TO_PTR2U32)
#define VX1000_ADDR_TO_PTR2U32(A)      VX1000_ADDRESS_TO_POINTER((A), VX1000_UINT32)                  /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2U32 */
#if !defined(VX1000_ADDR_TO_PTR2OE)
#define VX1000_ADDR_TO_PTR2OE(A)       VX1000_ADDRESS_TO_POINTER((A), VX1000_OLDA_EVENT_T)            /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2OE */
#if !defined(VX1000_ADDR_TO_PTR2V)
#define VX1000_ADDR_TO_PTR2V(A)        VX1000_ADDRESS_TO_POINTER((A), void)                           /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2V */
#if !defined(VX1000_ADDR_TO_PTR2VV)
#define VX1000_ADDR_TO_PTR2VV(A)       VX1000_ADDRESS_TO_POINTER((A), volatile void)                  /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2VV */
#if !defined(VX1000_ADDR_TO_PTR2VCC)
#define VX1000_ADDR_TO_PTR2VCC(A)      VX1000_ADDRESS_TO_POINTER((A), volatile const VX1000_CHAR)     /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2VCC */
#if !defined(VX1000_ADDR_TO_PTR2VC)
#define VX1000_ADDR_TO_PTR2VC(A)       VX1000_ADDRESS_TO_POINTER((A), volatile VX1000_CHAR)           /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2VC */
#if !defined(VX1000_ADDR_TO_PTR2VCU32)
#define VX1000_ADDR_TO_PTR2VCU32(A)    VX1000_ADDRESS_TO_POINTER((A), volatile const VX1000_UINT32)   /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2VCU32 */
#if !defined(VX1000_ADDR_TO_PTR2OCR)
#define VX1000_ADDR_TO_PTR2OCR(A)      VX1000_ADDRESS_TO_POINTER((A), VX1000_OVL_CONFIG_REGS_T)       /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2OCR */
#if !defined(VX1000_ADDR_TO_PTR2VTE)
#define VX1000_ADDR_TO_PTR2VTE(A)      VX1000_ADDRESS_TO_POINTER((A), VX1000_HBB_VX_TABLE_ENTRY_T)    /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2VTE */
#if !defined(VX1000_ADDR_TO_PTR2OCB)
#define VX1000_ADDR_TO_PTR2OCB(A)      VX1000_ADDRESS_TO_POINTER((A), VX1000_VUFUNC_T)                /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2OCB */
#if !defined(VX1000_PTR2VU16_TO_ADDRESS)
#define VX1000_PTR2VU16_TO_ADDRESS(P)  VX1000_POINTER_TO_ADDRESS((P), volatile VX1000_UINT16)         /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VU16_TO_ADDRESS */
#if !defined(VX1000_PTR2VU32_TO_ADDRESS)
#define VX1000_PTR2VU32_TO_ADDRESS(P)  VX1000_POINTER_TO_ADDRESS((P), volatile VX1000_UINT32)         /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VU32_TO_ADDRESS */
#if !defined(VX1000_PTR2VU64_TO_ADDRESS)
#define VX1000_PTR2VU64_TO_ADDRESS(P)  VX1000_POINTER_TO_ADDRESS((P), volatile VX1000_UINT64)         /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VU64_TO_ADDRESS */
#if !defined(VX1000_PTR2VF_TO_ADDRESS)
#define VX1000_PTR2VF_TO_ADDRESS(P)    VX1000_POINTER_TO_ADDRESS((P), volatile VX1000_FLOAT)          /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VF_TO_ADDRESS */
#if !defined(VX1000_PTR2VD_TO_ADDRESS)
#define VX1000_PTR2VD_TO_ADDRESS(P)    VX1000_POINTER_TO_ADDRESS((P), volatile VX1000_DOUBLE)         /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VD_TO_ADDRESS */
#if !defined(VX1000_PTR2VUWS_TO_ADDRESS)
#define VX1000_PTR2VUWS_TO_ADDRESS(P)  VX1000_POINTER_TO_ADDRESS((P), volatile VX1000_UINTWS)         /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VUWS_TO_ADDRESS */
#if !defined(VX1000_PTR2VVX_TO_ADDRESS)
#define VX1000_PTR2VVX_TO_ADDRESS(P)   VX1000_POINTER_TO_ADDRESS((P), volatile VX1000_STRUCT_T)       /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VVX_TO_ADDRESS */
#if !defined(VX1000_PTR2VU8_TO_ADDRESS)
#define VX1000_PTR2VU8_TO_ADDRESS(P)   VX1000_POINTER_TO_ADDRESS((P), volatile VX1000_UINT8)          /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VU8_TO_ADDRESS */
#if !defined(VX1000_PTR2SPS_TO_ADDRESS)
#define VX1000_PTR2SPS_TO_ADDRESS(P)   VX1000_POINTER_TO_ADDRESS((P), VX1000_SYNCAL_PAGE_SWITCH_T)    /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2SPS_TO_ADDRESS */
#if !defined(VX1000_PTR2OCR_TO_ADDRESS)
#define VX1000_PTR2OCR_TO_ADDRESS(P)   VX1000_POINTER_TO_ADDRESS((P), VX1000_OVL_CONFIG_REGS_T)       /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2OCR_TO_ADDRESS */
#if !defined(VX1000_PTR2VFF_TO_ADDRESS)
#define VX1000_PTR2VFF_TO_ADDRESS(P)   VX1000_POINTER_TO_ADDRESS((P), volatile VX1000_FKL_FCT)        /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VFF_TO_ADDRESS */
#if !defined(VX1000_PTR2SL_TO_ADDRESS)
#define VX1000_PTR2SL_TO_ADDRESS(P)    VX1000_POINTER_TO_ADDRESS((P), vx1000_staticLut_T)             /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2SL_TO_ADDRESS */
#if !defined(VX1000_PTR2U32_TO_ADDRESS)
#define VX1000_PTR2U32_TO_ADDRESS(P)   VX1000_POINTER_TO_ADDRESS((P), VX1000_UINT32)                  /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2U32_TO_ADDRESS */
#if !defined(VX1000_PTR2U8_TO_ADDRESS)
#define VX1000_PTR2U8_TO_ADDRESS(P)    VX1000_POINTER_TO_ADDRESS((P), VX1000_UINT8)                   /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2U8_TO_ADDRESS */
#if !defined(VX1000_PTR2VM_TO_ADDRESS)
#define VX1000_PTR2VM_TO_ADDRESS(P)    VX1000_POINTER_TO_ADDRESS((P), volatile VX1000_MAILBOX_T)      /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VM_TO_ADDRESS */
#if !defined(VX1000_PTR2CU8_TO_ADDRESS)
#define VX1000_PTR2CU8_TO_ADDRESS(P)   VX1000_POINTER_TO_ADDRESS((P), const VX1000_UINT8)             /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2CU8_TO_ADDRESS */
#if !defined(VX1000_PTR2CU32_TO_ADDRESS)
#define VX1000_PTR2CU32_TO_ADDRESS(P)  VX1000_POINTER_TO_ADDRESS((P), const VX1000_UINT32)            /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2CU32_TO_ADDRESS */
#if !defined(VX1000_PTR2VO_TO_ADDRESS)
#define VX1000_PTR2VO_TO_ADDRESS(P)    VX1000_POINTER_TO_ADDRESS((P), volatile VX1000_OLDA_T)         /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VO_TO_ADDRESS */
#if !defined(VX1000_PTR2U16_TO_ADDRESS)
#define VX1000_PTR2U16_TO_ADDRESS(P)   VX1000_POINTER_TO_ADDRESS((P), VX1000_UINT16)                  /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2U16_TO_ADDRESS */
#if !defined(VX1000_PTR2VS_TO_ADDRESS)
#define VX1000_PTR2VS_TO_ADDRESS(P)    VX1000_POINTER_TO_ADDRESS((P), volatile VX1000_STIM_T)         /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VS_TO_ADDRESS */
#if !defined(VX1000_PTR2MC_TO_ADDRESS)
#define VX1000_PTR2MC_TO_ADDRESS(P)    VX1000_POINTER_TO_ADDRESS((P), VX1000_MEMSYNC_CPY_T)           /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2MC_TO_ADDRESS */
#if !defined(VX1000_PTR2MT_TO_ADDRESS)
#define VX1000_PTR2MT_TO_ADDRESS(P)    VX1000_POINTER_TO_ADDRESS((P), VX1000_MEMSYNC_TRIG_T)          /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2MT_TO_ADDRESS */
#if !defined(VX1000_PTR2VRM_TO_ADDRESS)
#define VX1000_PTR2VRM_TO_ADDRESS(P)   VX1000_POINTER_TO_ADDRESS((P), volatile VX1000_RES_MGMT_T)     /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VRM_TO_ADDRESS */
#if !defined(VX1000_PTR2VEHM_TO_ADDRESS)
#define VX1000_PTR2VEHM_TO_ADDRESS(P)  VX1000_POINTER_TO_ADDRESS((P), volatile VX1000_EMEM_HDR_T)     /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VEHM_TO_ADDRESS */
#if !defined(VX1000_PTR2V_TO_ADDRESS)
#define VX1000_PTR2V_TO_ADDRESS(P)     VX1000_POINTER_TO_ADDRESS((P), void)                           /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2V_TO_ADDRESS */
#if !defined(VX1000_PTR2VOVL_TO_ADDRESS)
#define VX1000_PTR2VOVL_TO_ADDRESS(P)  VX1000_POINTER_TO_ADDRESS((P), VX1000_OVL_T)                   /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2VOVL_TO_ADDRESS */
#if !defined(VX1000_PTR2SE_TO_ADDRESS)
#define VX1000_PTR2SE_TO_ADDRESS(P)    VX1000_POINTER_TO_ADDRESS((P), VX1000_STIM_EVENT_T)            /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2SE_TO_ADDRESS */
#if !defined(VX1000_PTR2CC_TO_ADDRESS)
#define VX1000_PTR2CC_TO_ADDRESS(P)    VX1000_POINTER_TO_ADDRESS((P), const VX1000_CHAR)              /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2CC_TO_ADDRESS */
#if !defined(VX1000_PTR2C_TO_ADDRESS)
#define VX1000_PTR2C_TO_ADDRESS(P)     VX1000_POINTER_TO_ADDRESS((P), VX1000_CHAR)                    /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_PTR2C_TO_ADDRESS */

#if !defined(VX1000_OLDA_NOPS)
#define VX1000_OLDA_NOPS()             /* empty */
#endif /* !VX1000_OLDA_NOPS */

#if defined(VX1000_TARGET_XC2000)
#define VX1000_WORDSIZE                2
#define VX1000_WORDSIZESHIFT           1
#define VX1000_UINTWS VX1000_UINT16
#if !defined(VX1000_PTR2VWS_TO_ADDRESS)
#define VX1000_PTR2VWS_TO_ADDRESS(A)   VX1000_PTR2VU16_TO_ADDRESS(A) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* VX1000_PTR2VWS_TO_ADDRESS */
#if !defined(VX1000_ADDR_TO_PTR2UWS)
#define VX1000_ADDR_TO_PTR2UWS(A)      VX1000_ADDR_TO_PTR2U16(A)                                      /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#define VX1000_ADDR_TO_PTR2VUWS(A)     VX1000_ADDR_TO_PTR2VU16(A)                                     /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2UWS */
#else /* !VX1000_TARGET_XC2000 */
#define VX1000_WORDSIZE                4
#define VX1000_WORDSIZESHIFT           2
#define VX1000_UINTWS VX1000_UINT32
#if !defined(VX1000_PTR2VWS_TO_ADDRESS)
#define VX1000_PTR2VWS_TO_ADDRESS(A)   VX1000_PTR2VU32_TO_ADDRESS(A) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* VX1000_PTR2VWS_TO_ADDRESS */
#if !defined(VX1000_ADDR_TO_PTR2UWS)
#define VX1000_ADDR_TO_PTR2UWS(A)      VX1000_ADDR_TO_PTR2U32(A)                                      /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#define VX1000_ADDR_TO_PTR2VUWS(A)     VX1000_ADDR_TO_PTR2VU32(A)                                     /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to allow the user to provide type-casting code that compiles without warnings on his toolchain */
#endif /* !VX1000_ADDR_TO_PTR2UWS */
#endif /* !VX1000_TARGET_XC2000 */

#define VX1000_TRSFRLISTSPC_INIT(IDX)  VX1000_O8DESC_TIMESTAMP_EV    VX1000_O8DESC_TRIGSPCEV   VX1000_O8DESC_DONE /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_EVENTDESCR_SPCSIZ       VX1000_O8DLEN_TIMESTAMP_EV + (VX1000_O8DLEN_TRIGSPCEV + VX1000_O8DLEN_DONE)
#define VX1000_TRSFRLISTDAQ_INIT(IDX)  VX1000_O8DESC_TIMESTAMP_EV    VX1000_O8DESC_TRIGDAQEV(IDX)  VX1000_O8DESC_DONE /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_EVENTDESCR_DAQSIZ       VX1000_O8DLEN_TIMESTAMP_EV + (VX1000_O8DLEN_TRIGDAQEV     + VX1000_O8DLEN_DONE)
#define VX1000_EVENTLIST_INIT(IDX)     0 ,0 ,0 ,0  /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_EVENTLIST_INIT001(IDX)  , 0, 0, 0, (((VX1000_EVENTDESCR_SPCSIZ) + ((IDX) * (VX1000_EVENTDESCR_DAQSIZ))) * 0x10001UL) /* TransferIndex duplicated into unused TransferCount field to support both endianess types */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compiletime and thus cannot be turned into a function */
#define VX1000_EVENTLIST_INIT002(IDX)  VX1000_EVENTLIST_INIT001((IDX)) VX1000_EVENTLIST_INIT001((IDX) + 0x001) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_EVENTLIST_INIT004(IDX)  VX1000_EVENTLIST_INIT002((IDX)) VX1000_EVENTLIST_INIT002((IDX) + 0x002) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_EVENTLIST_INIT008(IDX)  VX1000_EVENTLIST_INIT004((IDX)) VX1000_EVENTLIST_INIT004((IDX) + 0x004) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_EVENTLIST_INIT010(IDX)  VX1000_EVENTLIST_INIT008((IDX)) VX1000_EVENTLIST_INIT008((IDX) + 0x008) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_EVENTLIST_INIT020(IDX)  VX1000_EVENTLIST_INIT010((IDX)) VX1000_EVENTLIST_INIT010((IDX) + 0x010) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_EVENTLIST_INIT040(IDX)  VX1000_EVENTLIST_INIT020((IDX)) VX1000_EVENTLIST_INIT020((IDX) + 0x020) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_EVENTLIST_INIT080(IDX)  VX1000_EVENTLIST_INIT040((IDX)) VX1000_EVENTLIST_INIT040((IDX) + 0x040) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_EVENTLIST_INIT100(IDX)  VX1000_EVENTLIST_INIT080((IDX)) VX1000_EVENTLIST_INIT080((IDX) + 0x080) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_EVENTLIST_INIT200(IDX)  VX1000_EVENTLIST_INIT100((IDX)) VX1000_EVENTLIST_INIT100((IDX) + 0x100) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_TRSFRLIST_INIT001(IDX)  , VX1000_TRSFRLISTDAQ_INIT((IDX)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compiletime and thus cannot be turned into a function */
#define VX1000_TRSFRLIST_INIT002(IDX)  VX1000_TRSFRLIST_INIT001((IDX)) VX1000_TRSFRLIST_INIT001((IDX) + 0x001) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_TRSFRLIST_INIT004(IDX)  VX1000_TRSFRLIST_INIT002((IDX)) VX1000_TRSFRLIST_INIT002((IDX) + 0x002) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_TRSFRLIST_INIT008(IDX)  VX1000_TRSFRLIST_INIT004((IDX)) VX1000_TRSFRLIST_INIT004((IDX) + 0x004) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_TRSFRLIST_INIT010(IDX)  VX1000_TRSFRLIST_INIT008((IDX)) VX1000_TRSFRLIST_INIT008((IDX) + 0x008) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_TRSFRLIST_INIT020(IDX)  VX1000_TRSFRLIST_INIT010((IDX)) VX1000_TRSFRLIST_INIT010((IDX) + 0x010) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_TRSFRLIST_INIT040(IDX)  VX1000_TRSFRLIST_INIT020((IDX)) VX1000_TRSFRLIST_INIT020((IDX) + 0x020) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_TRSFRLIST_INIT080(IDX)  VX1000_TRSFRLIST_INIT040((IDX)) VX1000_TRSFRLIST_INIT040((IDX) + 0x040) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_TRSFRLIST_INIT100(IDX)  VX1000_TRSFRLIST_INIT080((IDX)) VX1000_TRSFRLIST_INIT080((IDX) + 0x080) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */
#define VX1000_TRSFRLIST_INIT200(IDX)  VX1000_TRSFRLIST_INIT100((IDX)) VX1000_TRSFRLIST_INIT100((IDX) + 0x100) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compile-time and thus cannot be turned into a function */


#if !defined(VX1000_DUMMYREAD)
#define VX1000_DUMMYREAD(X)            /* empty */
#endif /* !VX1000_DUMMYREAD */

#if !defined(VX1000_DISCARD4DUMMYARGS)
#define VX1000_DISCARD4DUMMYARGS(T, R, A1, A2, A3, A4)  (R)
#endif /* !VX1000_DISCARD4DUMMYARGS */


#if !defined(VX1000_DISABLE_INSTRUMENTATION)
/*------------------------------------------------------------------------------ */
/* optional forced special linkage                                               */

/* include user-defined lines with optional section pragmas to force individual linkage of VX1000 code and/or data: */
#define VX1000_BEGSECT_VXMODULE_H
#include "VX1000_cfg.h"  /* PRQA S 0883 */ /* Willingly violating MISRA rule 19.15: this file is included multiple times because it contains multiple disjunct parts that each are filtered exactly once by preprocessor conditions */ /* PRQA S 5087*/ /* Willingly violating MISRA rule 19.1: this file is included multiple at exactly those lines where it is needed to expand to the desired lines. Inclusion/expansion of those lines at the top of this file would not make sense and lead to compile errors */
#define VX1000_BEGSECT_VXMODULE_H_UNDO
#endif /* !VX1000_DISABLE_INSTRUMENTATION */


#if defined(VX1000_COMPILED_FOR_SLAVECORES)
/* On multi-core systems with different instruction sets, the same function must exist multiple times, so add suffixes: */
#define VX1000_SUFFU3(F,S) F ## _ ## S /* PRQA S 3410 */ /* Willingly violating MISRA rule 19.10 because extra parentheses would change the meaning of the code (result is a label) */
#define VX1000_SUFFU2(F,S) VX1000_SUFFU3(F,S) /* PRQA S 3410 */ /* Willingly violating MISRA rule 19.10 because extra parentheses would change the meaning of the code (result is a label) */
#define VX1000_SUFFUN(F) VX1000_SUFFU2(F,VX1000_COMPILED_FOR_SLAVECORES)  /* PRQA S 3410 */ /* Willingly violating MISRA rule 19.10 because extra parentheses would change the meaning of the code (result is a label) */
#else /* !VX1000_COMPILED_FOR_SLAVECORES */
#define VX1000_SUFFUN(F) F /* direct mapping */ /* PRQA S 3410 */ /* Willingly violating MISRA rule 19.10 because extra parentheses would change the meaning of the code (result is a label) */
#endif /* !VX1000_COMPILED_FOR_SLAVECORES */

#if defined(VX1000_ADADDONS_BUILDNUMBER_H)
#if !defined(VX1000_MEMSYNC_TRIGGER_COUNT)
#define VX1000_MEMSYNC_TRIGGER_COUNT   0   /* this misplaced default setting is just a workaround for AddOns file split dependencies in some configurations. The REAL place for default settings is way down */
#endif /* !VX1000_MEMSYNC_TRIGGER_COUNT */
#include "VX1000_addons.h"                 /* basically included too early. should be included after ALL default settings and consistency checks have been done! */
#endif /* VX1000_ADADDONS_BUILDNUMBER_H */

#if defined(VX1000_TARGET_POWERPC)

/*------------------------------------------------------------------------------ */
/* PowerPC related defines                                                       */

#define VX1000_ADD_MPC_TRIGS(B)        do { (VX1000_ADDR_TO_PTR2VU32(VX1000_OLDA_DTS_BASE_ADDR))[2] = 1UL << (B);/* mirror register only available on few derivatives: *(volatile VX1000_UINT32*)((VX1000_OLDA_DTS_BASE_ADDR) + 12UL) = 1UL << (B); */ /* the HW ORs the bit to existing bits in DTS_SEMAPHORE_A,B */ } while (0)

#endif /* VX1000_TARGET_POWERPC */

#if defined(VX1000_TARGET_TRICORE)

/*------------------------------------------------------------------------------ */
/* TriCore related defines                                                       */

#define VX1000_JTAGID_PN_TC1767            0x00159000UL
#define VX1000_JTAGID_PN_TC1797            0x0015A000UL
#define VX1000_JTAGID_PN_TC178x            0x0018E000UL
#define VX1000_JTAGID_PN_TC1387            0x0018F000UL
#define VX1000_JTAGID_PN_TC1798            0x001BE000UL
#define VX1000_JTAGID_PN_TC172x            0x001D0000UL
#define VX1000_JTAGID_PN_TC27xTC2Dx        0x001DA000UL /* TC27x and TC2Dx have the same base ID, bits 28..31==1/2/3/4 define Dx/7A/7B/7C */
#define VX1000_JTAGID_PN_TC26x             0x001E8000UL
#define VX1000_JTAGID_PN_TC29x             0x001E9000UL
#define VX1000_JTAGID_PN_TC24x             0x001EA000UL
#define VX1000_JTAGID_PN_TC23x             0x00200000UL
#define VX1000_JTAGID_PN_TC22x             0x00201000UL
#define VX1000_JTAGID_PN_TC21x             0x00202000UL
#define VX1000_JTAGID_PN_TC39x             0x00205000UL /* identical PN field for both Astep and Bstep of TC39x */
#define VX1000_JTAGID_PN_TC38x             0x00206000UL /* Astep NG */
#define VX1000_JTAGID_PN_TC37x             0x00207000UL /* Astep NG */
#define VX1000_JTAGID_PN_TC37xED           0x00208000UL /* Astep NG */
#define VX1000_JTAGID_PN_TC36x             0x00209000UL /* Astep NG */
#define VX1000_JTAGID_PN_TC35x             0x0020A000UL /* Astep NG */
#define VX1000_JTAGID_PN_TC33x             0x0020B000UL /* Astep NG */
#define VX1000_JTAGID_PN_TC33xED           0x0020C000UL /* Astep NG */
#define VX1000_JTAGID_PN_TC32x             0x0020D000UL /* Astep NG */
#define VX1000_JTAGID_PN_MASK              0x0FFFF000UL

#if !defined(VX1000_ECU_IS_AURIX) /* if not defined by user to 0 or 1, provide automatic check: */
#define VX1000_ECU_IS_AURIX() (((VX1000_MCREG_CBS_JTAGID) & (VX1000_JTAGID_PN_MASK)) >= (VX1000_JTAGID_PN_TC27xTC2Dx))
#endif /* !VX1000_ECU_IS_AURIX */
#if !defined(VX1000_ECU_IS_AURIXPLUS) /* if not defined by user to 0 or 1, provide automatic check: */
#define VX1000_ECU_IS_AURIXPLUS() (((VX1000_MCREG_CBS_JTAGID) & (VX1000_JTAGID_PN_MASK)) >= (VX1000_JTAGID_PN_TC39x))
#endif /* !VX1000_ECU_IS_AURIXPLUS */

/* JTAG Device Identification Register */
#define VX1000_MCREG_CBS_JTAGID            ((VX1000_ADDR_TO_PTR2VU32(0xF0000464UL))[0])
/* OSCU Control Register (ENDINIT protection ctl?) */
#define VX1000_MCREG_CBS_OCNTRL            ((VX1000_ADDR_TO_PTR2VU32(0xF000047CUL))[0])
/* OCDS Interface Mode Control (ENDINIT protection ctl?) */
#define VX1000_MCREG_OCDS_OCNTRL           ((VX1000_ADDR_TO_PTR2VU32(0xF000047CUL))[0])
/* Aurix OCDS Interface Mode Control */
#define VX1000_MCREG_OCDS_OIFM             ((VX1000_ADDR_TO_PTR2VU32(0xF000040CUL))[0])
/* Tricore Overlay Control Registers */
#define VX1000_MCREG_OVC_OCON              ((VX1000_ADDR_TO_PTR2VU32(0xF87FFBE0UL))[0])
#define VX1000_MCREG_OVC_OENABLE           ((VX1000_ADDR_TO_PTR2VU32(0xF87FFBE8UL))[0])
/* Aurix Overlay Control Register */
#define VX1000_MCREG_SCU_OVCENBL_AURIX     ((VX1000_ADDR_TO_PTR2VU32(0xF00361E0UL))[0])
#define VX1000_MCREG_SCU_OVCCON            ((VX1000_ADDR_TO_PTR2VU32(0xF00361E4UL))[0])
/* Aurix Overlay Range Select Register for each Core */
#define VX1000_MCREG_OVC0_OSEL             ((VX1000_ADDR_TO_PTR2VU32(0xF880FB00UL))[0])
#define VX1000_MCREG_OVC1_OSEL             ((VX1000_ADDR_TO_PTR2VU32(0xF882FB00UL))[0])
#define VX1000_MCREG_OVC2_OSEL             ((VX1000_ADDR_TO_PTR2VU32(0xF884FB00UL))[0])
#define VX1000_MCREG_OVC3_OSEL             ((VX1000_ADDR_TO_PTR2VU32(0xF886FB00UL))[0])
#define VX1000_MCREG_OVC4_OSEL             ((VX1000_ADDR_TO_PTR2VU32(0xF888FB00UL))[0])
#define VX1000_MCREG_OVC5_OSEL             ((VX1000_ADDR_TO_PTR2VU32(0xF88CFB00UL))[0])
#define VX1000_MCREG_OVC_OSEL(C)           ((VX1000_ADDR_TO_PTR2VU32(0xF880FB00UL) + ((C) << 17U))[0]) /* attention: there is one gap behind C==4 */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because the expanded expression depending on the context must serve either as lvalue or as rvalue, so implementing it as a function is not possible */
/* Arrays of various Overlay Registers */
#define VX1000_MCREGADDR_RABR(I)           ((0xF87FFB20UL + (12UL * (I)))) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because the expanded expression depending on the context must serve either as lvalue or as rvalue, so implementing it as a function is not possible */
#define VX1000_MCREGADDR_RABR_AURIX(I,C)   ((0xF880FB10UL + (12UL * (I))) + ((C) << 17U)) /* attention: there is one gap behind C==4 */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because the expanded expression depending on the context must serve either as lvalue or as rvalue, so implementing it as a function is not possible */
#define VX1000_MCREGADDR_OMASK(I)          ((0xF87FFB28UL + (12UL * (I)))) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because the expanded expression depending on the context must serve either as lvalue or as rvalue, so implementing it as a function is not possible */
#define VX1000_MCREGADDR_OMASK_AURIX(I,C)  ((0xF880FB18UL + (12UL * (I))) + ((C) << 17U)) /* attention: there is one gap behind C==4 */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because the expanded expression depending on the context must serve either as lvalue or as rvalue, so implementing it as a function is not possible */
#define VX1000_MCREGADDR_OTAR(I)           ((0xF87FFB24UL + (12UL * (I)))) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because the expanded expression depending on the context must serve either as lvalue or as rvalue, so implementing it as a function is not possible */
#define VX1000_MCREGADDR_OTAR_AURIX(I,C)   ((0xF880FB14UL + (12UL * (I))) + ((C) << 17U)) /* attention: there is one gap behind C==4 */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because the expanded expression depending on the context must serve either as lvalue or as rvalue, so implementing it as a function is not possible */
/* TRIGS Register */
#define VX1000_MCREG_OCDS_TRIGS            ((VX1000_ADDR_TO_PTR2VU32(0xF00004A0UL))[0])
/* TRIGC Register */
#define VX1000_MCREG_OCDS_TRIGC            ((VX1000_ADDR_TO_PTR2VU32(0xF00004A4UL))[0])
/* Tricore TRIG v1 Register */
#define VX1000_MCREG_OCDS_TRIG             ((VX1000_ADDR_TO_PTR2VU32(0xF00004A8UL))[0])
/* Aurix TRIG20 v2 Register */
#define VX1000_MCREG_OCDS_TRIG20           ((VX1000_ADDR_TO_PTR2VU32(0xF0000550UL))[0]) /* yes! */
/* Aurix TRIG5 v3 Register */
#define VX1000_MCREG_OCDS_TRIG5            ((VX1000_ADDR_TO_PTR2VU32(0xF0000514UL))[0]) /* TRIG20 does not exist for TC23x (only 0-5) */
/* Tricore Application Reset Disable Register */
#define VX1000_MCREG_SCU_ARSTDIS           ((VX1000_ADDR_TO_PTR2VU32(0xF000055CUL))[0])

#define VX1000_AURIX_TRIG_REG_OFS          480UL /* Bit 0 in OCDS_TRIG20; */
#define VX1000_AURIX_TRIG_REG_OFS_V3       80UL  /* Bit 0 in OCDS_TRIG5;  */

#endif /* VX1000_TARGET_TRICORE */


/*------------------------------------------------------------------------------ */
/* VX1000 default parameters                                                     */

#if defined(VX1000_TARGET_TRICORE)

/*
 Enable 3 or 2 wire DAP mode and disable BYPASS (JTAG) to increase immunity against disturbances
 Default is 2 wire DAP
 VX1000_OCDS_OCNTRL =  0x0000000F;  DJMODE = 11 Three PIN DAP with disabled BYPASS
 VX1000_OCDS_OCNTRL =  0x00000007;  DJMODE = 01 Two-pin DAP with disabled BYPASS
*/
#if !defined(VX1000_OCDS_OCNTRL)
#define VX1000_OCDS_OCNTRL         0x00000007UL
#endif /* !VX1000_OCDS_OCNTRL */
#if !defined(VX1000_OCDS_OIFM)
/* 2pin DAP (legacy) */
#define VX1000_OCDS_OIFM           0x00000000UL
#endif /* !VX1000_OCDS_OIFM */

#endif /* VX1000_TARGET_TRICORE */

#if !defined(VX1000_ARM_DSB)
#define VX1000_ARM_DSB()               /* empty */
#endif /* !VX1000_ARM_DSB */

#if !defined(VX1000_ENTER_SPINLOCK)
#define VX1000_ENTER_SPINLOCK()
#endif /* !VX1000_ENTER_SPINLOCK */
#if !defined(VX1000_LEAVE_SPINLOCK)
#define VX1000_LEAVE_SPINLOCK()
#endif /* !VX1000_LEAVE_SPINLOCK */
#if !defined(VX1000_ATOMIC_XOR32)
#define VX1000_ATOMIC_XOR32(A, D) do                          \
  {                                                           \
    VX1000_ENTER_SPINLOCK()                                   \
    (VX1000_ADDR_TO_PTR2VU32(A))[0] ^= (D);                   \
    VX1000_LEAVE_SPINLOCK()                                   \
  } while (0) /* this never-looping while is here only for MISRA */
#endif /* !VX1000_ATOMIC_XOR32 */
#if !defined(VX1000_ATOMIC_XOR32X2)
#define VX1000_ATOMIC_XOR32X2(A, D) do                        \
  {                                                           \
    VX1000_ENTER_SPINLOCK()                                   \
    (VX1000_ADDR_TO_PTR2VU32(A))[0] ^= (D);                   \
    (VX1000_ADDR_TO_PTR2VU32(A))[1] ^= (D);                   \
    VX1000_LEAVE_SPINLOCK()                                   \
  } while (0) /* this never-looping while is here only for MISRA */
#endif /* !VX1000_ATOMIC_XOR32X2 */

#if !defined(VX1000_ATOMIC_AND32)
#define VX1000_ATOMIC_AND32(A, D) do                          \
  {                                                           \
    VX1000_ENTER_SPINLOCK()                                   \
    (VX1000_ADDR_TO_PTR2VU32(A))[0] &= (D);                   \
    VX1000_LEAVE_SPINLOCK()                                   \
  } while (0) /* this never-looping while is here only for MISRA */
#endif /* !VX1000_ATOMIC_AND32 */

#if !defined(VX1000_ATOMIC_OR32)
#define VX1000_ATOMIC_OR32(A, D) do                           \
  {                                                           \
    VX1000_ENTER_SPINLOCK()                                   \
    (VX1000_ADDR_TO_PTR2VU32(A))[0] |= (D);                   \
    VX1000_LEAVE_SPINLOCK()                                   \
  } while (0) /* this never-looping while is here only for MISRA */
#endif /* !VX1000_ATOMIC_OR32 */

#if !defined(VX1000_MAILBOX_SLOT_DWORDS)
#define VX1000_MAILBOX_SLOT_DWORDS     (1UL + 2UL) /* must be at least 3 (=4 bytes size info + 8 bytes payload (=XCP command)) */
#endif /* !VX1000_MAILBOX_SLOT_DWORDS */
#if !defined(VX1000_MAILBOX_SLOTS)
#define VX1000_MAILBOX_SLOTS           8U
#endif /* !VX1000_MAILBOX_SLOTS */

#if !defined(VX1000_OLDA_HIPRIOMASK)
#define VX1000_OLDA_HIPRIOMASK         0UL
#endif /* !VX1000_OLDA_HIPRIOMASK */

#if !defined(VX1000_IS_BYPASS_RESUME_ALLOWED)
#define VX1000_IS_BYPASS_RESUME_ALLOWED() 0
#endif /* VX1000_IS_BYPASS_RESUME_ALLOWED */

#if ((!defined(VX1000_OLDA_FORCE_V6)) && (!defined(VX1000_OLDA_FORCE_V7))) && (!defined(VX1000_OLDA_FORCE_V8))
#define VX1000_OLDA_FORCE_V7
#endif /* !VX1000_OLDA_FORCE_V6 && !VX1000_OLDA_FORCE_V7 && !VX1000_OLDA_FORCE_V8 */

#if !defined(VX1000_OVERLAY_PHYSADDR)
#define VX1000_OVERLAY_PHYSADDR        VX1000_OVERLAY_ADDR
#endif /* !VX1000_OVERLAY_PHYSADDR */

#if !defined(VX1000_CALRAM_PHYSADDR)
#define VX1000_CALRAM_PHYSADDR         VX1000_CALRAM_ADDR
#endif /* !VX1000_CALRAM_PHYSADDR */

#if !defined(VX1000_ERRLOG_SIZE)
#define VX1000_ERRLOG_SIZE             0
#endif /* !VX1000_ERRLOG_SIZE */

#if !defined(VX1000_MEMSYNC_TRIGGER_COUNT)
#define VX1000_MEMSYNC_TRIGGER_COUNT   0  /* this is the correct place for default settings. As a workaround for AddOns file split dependencies this had to pre-done at the beginning of the file in some configurations */
#endif /* !VX1000_MEMSYNC_TRIGGER_COUNT */

#if !defined(VX1000_MEMSYNC_COPY_COUNT)
#define VX1000_MEMSYNC_COPY_COUNT      0
#endif /* !VX1000_MEMSYNC_COPY_COUNT */

#if !defined(VX1000_CURRENT_CORE_IDX)
#define VX1000_CURRENT_CORE_IDX()      0
#endif /* !VX1000_CURRENT_CORE_IDX */

#if (!defined(VX1000_STORE64)) && defined(VX1000_UINT64)
#define VX1000_STORE64(S, A, D ) do { ((VX1000_ADDR_TO_PTR2VU64((S))))[0] = (((VX1000_UINT64)(A) << 3) << 29) | (VX1000_UINT64)(D); } while (0)  /* this never-looping while is here only for MISRA */ /* PRQA S 0303 */ /* Violation of MISRA rule 12.8 because 64-bit data types are known to be not MISRA-conformant. This is just an example to be overruled by a user-defined implementation of an atomic 64-bit-store operation. */
#endif /* !VX1000_STORE64 & VX1000_UINT64 */

#if defined(VX1000_TARGET_POWERPC)

#if !defined(VX1000_LFAST_PLLMULOFS)
#define VX1000_LFAST_PLLMULOFS         1 /* There are rumours that few derivatives use 0 instead of 1. The user can overload the default in those cases */
#endif /* !VX1000_LFAST_PLLMULOFS */

#endif /* VX1000_TARGET_POWERPC */

#if defined(VX1000_TARGET_TRICORE)

/*------------------------------------------------------------------------------ */
/* Tricore default clock: The resolution of this clock must still be specified   */
/* in VX1000_cfg.h and also in vxconfigplugin.xml!                               */
/* Otherwise a randomly chosen value of 180 MHz is assumed for SystemTimer0.     */

#if (!defined(VX1000_CLOCK)) && (!defined(VX1000_DISABLE_INSTRUMENTATION))
/* This code is only here for backwards compatible handling of badly configured  */
/* applications! It is inactive in normal configurations:                        */
#if defined(VX1000_MCREG_STM0_TIM0)
#define VX1000_CLOCK() (VX1000_MCREG_STM0_TIM0)
#else /* !VX1000_MCREG_STM0_TIM0 */
#if !defined(VX1000_CLOCK_TIMER_ADDR)
#define VX1000_CLOCK_TIMER_ADDR        (VX1000_ECU_IS_AURIX() ? ((VX1000_ECU_IS_AURIXPLUS())?0xF0001010UL:0xF0000010UL):0xF0000210UL)  /* for Tricore built-in clock we know the timer address */
#endif /* !VX1000_CLOCK_TIMER_ADDR */
#define VX1000_CLOCK()                 ((VX1000_ADDR_TO_PTR2VU32(VX1000_CLOCK_TIMER_ADDR))[0])
#endif /* !VX1000_MCREG_STM0_TIM0 */
#endif /* !VX1000_CLOCK, !VX1000_DISABLE_INSTRUMENTATION */
#if (((!defined(VX1000_CLOCK_TICKS_PER_BASE)) && (!defined(VX1000_CLOCK_TICKS_PER_S))) && ((!defined(VX1000_CLOCK_TICKS_PER_MS)) && (!defined(VX1000_CLOCK_TICKS_PER_US)))) && (!defined(VX1000_DISABLE_INSTRUMENTATION))
#define VX1000_CLOCK_TICKS_PER_US 45UL /* 180 MHz */
#endif /* !VX1000_CLOCK_TICKS_PER_MS, !VX1000_DISABLE_INSTRUMENTATION */

#endif /* VX1000_TARGET_TRICORE */

#if defined(VX1000_CLOCK_TICKS_PER_BASE)

#if (!defined(VX1000_CLOCK_TICKS_BASE_NS)) && (!defined(VX1000_DISABLE_INSTRUMENTATION))
#error Please define VX1000_CLOCK_TICKS_BASE_NS as the time bas in ns during which VX1000_CLOCK_TICKS_PER_BASE ticks occur
#endif /* !VX1000_CLOCK_TICKS_BASE_NS && !VX1000_DISABLE_INSTRUMENTATION */

#else /* !VX1000_CLOCK_TICKS_PER_BASE */

#if defined(VX1000_CLOCK_TICKS_PER_S)
#define VX1000_CLOCK_TICKS_PER_BASE    (VX1000_CLOCK_TICKS_PER_S)
#define VX1000_CLOCK_TICKS_BASE_NS     1000000000UL
#elif defined(VX1000_CLOCK_TICKS_PER_MS)
#define VX1000_CLOCK_TICKS_PER_BASE    (VX1000_CLOCK_TICKS_PER_MS)
#define VX1000_CLOCK_TICKS_BASE_NS     1000000UL
#elif defined(VX1000_CLOCK_TICKS_PER_US)
#define VX1000_CLOCK_TICKS_PER_BASE    (VX1000_CLOCK_TICKS_PER_US)
#define VX1000_CLOCK_TICKS_BASE_NS     1000UL
#elif !defined(VX1000_DISABLE_INSTRUMENTATION)
#error Please define VX1000_CLOCK_TICKS_PER_US or VX1000_CLOCK_TICKS_PER_MS or VX1000_CLOCK_TICKS_PER_S
#endif /* !VX1000_DISABLE_INSTRUMENTATION */

#endif /* !VX1000_CLOCK_TICKS_PER_BASE */

#if !defined(VX1000_CLOCK_TICKS_PER_S)
#define VX1000_CLOCK_TICKS_PER_S       ((1000000000UL * (VX1000_CLOCK_TICKS_PER_BASE)) / (VX1000_CLOCK_TICKS_BASE_NS))
#endif /* !VX1000_CLOCK_TICKS_PER_S */

#if !defined(VX1000_CLOCK_TICKS_PER_MS)
#define VX1000_CLOCK_TICKS_PER_MS      ((1000000UL * (VX1000_CLOCK_TICKS_PER_BASE)) / (VX1000_CLOCK_TICKS_BASE_NS))
#endif /* !VX1000_CLOCK_TICKS_PER_MS */

#if !defined(VX1000_CLOCK_TICKS_PER_US)
#define VX1000_CLOCK_TICKS_PER_US      ((1000UL * (VX1000_CLOCK_TICKS_PER_BASE)) / (VX1000_CLOCK_TICKS_BASE_NS))
#endif /* !VX1000_CLOCK_TICKS_PER_US */

#if !defined(VX1000_CLOCK_TIMER_SIZE)
#define VX1000_CLOCK_TIMER_SIZE        32 /* legacy mode of VX1000_CLOCK() */
#endif /* !VX1000_CLOCK_TIMER_SIZE */

#if !defined(VX1000_CLOCK_TIMER_ADDRREGION)
#define VX1000_CLOCK_TIMER_ADDRREGION  0 /* timer is a memory-mapped peripheral (no SPR) */
#endif /* !VX1000_CLOCK_TIMER_ADDRREGION */

#if !defined(VX1000_CLOCK_TIMER_REVERSE)
#define VX1000_CLOCK_TIMER_REVERSE     0 /* timer periphery uses same endianess as the CPU */
#endif /* !VX1000_CLOCK_TIMER_REVERSE */

#if !defined(VX1000_CLOCK_TIMER_ADDR)
#define VX1000_CLOCK_TIMER_ADDR        0UL /* turns the synchronisation feature off */
#endif /* !VX1000_CLOCK_TIMER_ADDR */

/* Default timeouts for VX detection in us and cold start initialization in ms */
#if !defined(VX1000_DETECTION_TIMEOUT_US)
#define VX1000_DETECTION_TIMEOUT_US    50UL
#endif /* !VX1000_DETECTION_TIMEOUT_US */
#if !defined(VX1000_COLDSTART_TIMEOUT_MS)
#if defined(VX1000_TARGET_XC2000)
#define VX1000_COLDSTART_TIMEOUT_MS    100UL
#else  /* !VX1000_TARGET_XC2000 */
#define VX1000_COLDSTART_TIMEOUT_MS    20UL
#endif /* !VX1000_TARGET_XC2000 */
#endif /* !VX1000_COLDSTART_TIMEOUT_MS */

#if !defined(VX1000_OLDA_BENCHMARK_CNT)
#if defined(VX1000_OLDA_FORCE_V8)
#define VX1000_OLDA_BENCHMARK_CNT 32U
#else /* !VX1000_OLDA_FORCE_V8 */
#define VX1000_OLDA_BENCHMARK_CNT 31U
#endif /* !VX1000_OLDA_FORCE_V8 */
#endif /* !VX1000_OLDA_BENCHMARK_CNT */


/*------------------------------------------------------------------------------ */
/* Defines                                                                       */

#if defined(VX1000_DISABLE_INSTRUMENTATION)

#define VX1000_STORE_TIMESTAMP(t)    /*VX1000_DUMMYREAD((t))*/
#define VX1000_STORE_EVENT(e)        /*VX1000_DUMMYREAD((e))*/

#else /* !VX1000_DISABLE_INSTRUMENTATION */

#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)


#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 1)

#if defined(VX1000_TARGET_TRICORE)
/* On Tricore, the timestamp must be present in both trace paths - try to accomplish this by writing twice; the dummy loop is only there for MISRA: */
#define VX1000_STORE_TIMESTAMP(t) do                                                                                                      \
{                                                                                                                                         \
  VX1000_UINT8 evprop = VX1000_CURRENT_CORE_IDX();                                                                                        \
  if (evprop >= (VX1000_MEMSYNC_TRIGGER_COUNT))                                                                                           \
  {                                                                                                                                       \
    evprop = 0;                                                                                                                           \
    VX1000_ERRLOGGER(VX1000_ERRLOG_TOO_MANY_CORES)                                                                                        \
  }                                                                                                                                       \
  VX1000_STORE64(((VX1000_ADDR_TO_PTR2VU32(gVX1000.MemSyncTrigPtr)[evprop] + 0x0C) & ~7UL), (VX1000_UINT32)&gVX1000.EventTimestamp, (t)); \
  VX1000_ARM_DSB()                                                                                                                        \
  VX1000_STORE64(((VX1000_ADDR_TO_PTR2VU32(gVX1000.MemSyncTrigPtr)[evprop] + 0x0C) & ~7UL), (VX1000_UINT32)&gVX1000.EventTimestamp, (t)); \
  VX1000_ARM_DSB()                                                                                                                        \
} while (0); /* PRQA S 3356 */ /* PRQA S 3359 */ /* cannot avoid violating MISRA rule 13.7 because this logical operation/control expression is actually an assertion that by design must always evaluate to FALSE  */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#else  /* !VX1000_TARGET_TRICORE */
#define VX1000_STORE_TIMESTAMP(t) do                                                                                                      \
{                                                                                                                                         \
  VX1000_UINT8 evprop = VX1000_CURRENT_CORE_IDX();                                                                                        \
  if (evprop >= (VX1000_MEMSYNC_TRIGGER_COUNT))                                                                                           \
  {                                                                                                                                       \
    evprop = 0;                                                                                                                           \
    VX1000_ERRLOGGER(VX1000_ERRLOG_TOO_MANY_CORES)                                                                                        \
  }                                                                                                                                       \
  VX1000_STORE64(((VX1000_ADDR_TO_PTR2VU32(gVX1000.MemSyncTrigPtr)[evprop] + 0x0C) & ~7UL), (VX1000_UINT32)&gVX1000.EventTimestamp, (t)); \
  VX1000_ARM_DSB()                                                                                                                        \
} while (0); /* PRQA S 3356 */ /* PRQA S 3359 */ /* cannot avoid violating MISRA rule 13.7 because this logical operation/control expression is actually an assertion that by design must always evaluate to FALSE  */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#endif /* !VX1000_TARGET_TRICORE */

#define VX1000_STORE_EVENT(e) do                                                                                                       \
{                                                                                                                                      \
  VX1000_UINT8 evprop = VX1000_CURRENT_CORE_IDX();                                                                                     \
  if (evprop >= (VX1000_MEMSYNC_TRIGGER_COUNT))                                                                                        \
  {                                                                                                                                    \
    evprop = 0;                                                                                                                        \
    VX1000_ERRLOGGER(VX1000_ERRLOG_TOO_MANY_CORES)                                                                                     \
  }                                                                                                                                    \
  VX1000_STORE64(((VX1000_ADDR_TO_PTR2VU32(gVX1000.MemSyncTrigPtr)[evprop] + 0x0C) & ~7UL), (VX1000_UINT32)&gVX1000.EventNumber, (e)); \
  VX1000_ARM_DSB()                                                                                                                     \
} while (0); /* PRQA S 3356 */ /* PRQA S 3359 */ /* cannot avoid violating MISRA rule 13.7 because this logical operation/control expression is actually an assertion that by design must always evaluate to FALSE  */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */

#else  /* VX1000_MEMSYNC_TRIGGER_COUNT == 1*/

#if defined(VX1000_TARGET_TRICORE)
/* On Tricore, the timestamp must be present in both trace paths - try to accomplish this by writing twice; the dummy loop is only there for MISRA: */
#define VX1000_STORE_TIMESTAMP(t) do                                                                   \
{                                                                                                      \
  VX1000_STORE64((gVX1000.MemSyncTrigPtr + 0x0C) & ~7UL, (VX1000_UINT32)&gVX1000.EventTimestamp, (t)); \
  VX1000_ARM_DSB()                                                                                     \
  VX1000_STORE64((gVX1000.MemSyncTrigPtr + 0x0C) & ~7UL, (VX1000_UINT32)&gVX1000.EventTimestamp, (t)); \
  VX1000_ARM_DSB()                                                                                     \
} while (0); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#else  /* !VX1000_TARGET_TRICORE */
#define VX1000_STORE_TIMESTAMP(t) do                                                                   \
{                                                                                                      \
  VX1000_STORE64((gVX1000.MemSyncTrigPtr + 0x0C) & ~7UL, (VX1000_UINT32)&gVX1000.EventTimestamp, (t)); \
  VX1000_ARM_DSB()                                                                                     \
} while (0); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#endif /* !VX1000_TARGET_TRICORE */

#define VX1000_STORE_EVENT(e) do                                                                    \
{                                                                                                   \
  VX1000_STORE64((gVX1000.MemSyncTrigPtr + 0x0C) & ~7UL, (VX1000_UINT32)&gVX1000.EventNumber, (e)); \
  VX1000_ARM_DSB()                                                                                  \
} while (0); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */

#endif /* VX1000_MEMSYNC_TRIGGER_COUNT == 1*/

#else /* !VX1000_MEMSYNC_TRIGGER_COUNT */

/* Store the time t into gVX1000.EventTimestamp (use all 32bit -- is only avoided in VX1000_FAST_TRACE_EVENT.  */
#if defined(VX1000_TARGET_TRICORE)
/* On Tricore, the timestamp must be present in both trace paths - try to accomplish this by writing twice; the dummy loop is only there for MISRA: */
#define VX1000_STORE_TIMESTAMP(t) do { gVX1000.EventTimestamp = (t); gVX1000.EventTimestamp = gVX1000.EventTimestamp; } while(0); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#else  /* !VX1000_TARGET_TRICORE */
#define VX1000_STORE_TIMESTAMP(t) gVX1000.EventTimestamp = (t); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#endif /* !VX1000_TARGET_TRICORE */

/* Store the event ID e into gVX1000.EventNumber (use all 32bit -- is only avoided in VX1000_FAST_TRACE_EVENT. */
#define VX1000_STORE_EVENT(e) gVX1000.EventNumber = (e); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */

#endif /* !VX1000_MEMSYNC_TRIGGER_COUNT */

#endif /* !VX1000_DISABLE_INSTRUMENTATION */

/* Special Events */
#define VX1000_EVENT_RAMZERO_START     0x00010000UL
#define VX1000_EVENT_RAMZERO_DONE      0x00010001UL
#define VX1000_EVENT_RAMSYNC_START     0x00010002UL
#define VX1000_EVENT_MAILBOX_INIT      0x00020000UL
#define VX1000_EVENT_MAILBOX_UPDATE    0x00020001UL
#define VX1000_EVENT_MAILBOX_RESETREQ  0x00020002UL
#define VX1000_EVENT_STIM_INIT         0x00030100UL
#define VX1000_EVENT_STIM_ACK          0x00030101UL
#define VX1000_EVENT_STIM_TIMEOUT(e)  (0x00030200UL | (e)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because a function call would inhibit constant propagation of the compiler */
#define VX1000_EVENT_STIM_ERR(e)      (0x00030300UL | (e)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because a function call would inhibit constant propagation of the compiler */
#define VX1000_EVENT_STRUCT_INIT       0x00040000UL
#define VX1000_EVENT_GP(e)            (0x00050000UL | (e)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because a function call would inhibit constant propagation of the compiler */
#define VX1000_ECU_EVT_SW_RESET        VX1000_EVENT_GP(0x00000001UL)
#define VX1000_ECU_EVT_OLDA_OVERLOAD   VX1000_EVENT_GP(0x00000002UL)
#define VX1000_ECU_EVT_SW_RESET_FAIL   VX1000_EVENT_GP(0x00000003UL)
#define VX1000_ECU_EVT_SW_RESET_PREP   VX1000_EVENT_GP(0x00000004UL)
#define VX1000_ECU_EVT_STIM_SKIP(e)   (VX1000_EVENT_GP(0x00008000UL) | (e)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because a function call would inhibit constant propagation of the compiler */

/*------------------------------------------------------------------------------ */
/* Check the configuration for plausibility                                      */
#if !defined(VX1000_DISABLE_INSTRUMENTATION)

#if !defined(VX1000_CLOCK)
#error Please define VX1000_CLOCK() as a free running 32 bit timer of desired resolution
#endif /* !VX1000_CLOCK */

#if ((VX1000_CLOCK_TIMER_SIZE != 0) && (VX1000_CLOCK_TIMER_SIZE != 16)) && ((VX1000_CLOCK_TIMER_SIZE != 24) && (VX1000_CLOCK_TIMER_SIZE != 32))
#error VX1000_CLOCK_TIMER_SIZE supports only these values: 0, 16, 24, 32! (you may round down to gain a valid configuration)
#endif /* VX1000_CLOCK_TIMER_SIZE */

#if defined(VX1000_OLDA) && defined(VX1000_OLDA_MEMORY_SIZE)
#if VX1000_OLDA_MEMORY_SIZE > 0xFFFCU
#error Maximum value allowed for VX1000_OLDA_MEMORY_SIZE configuration is 0xFFFC
#endif /* VX1000_OLDA_MEMORY_SIZE > 0xFFFC */
#endif /* VX1000_OLDA & VX1000_OLDA_MEMORY_SIZE */

#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE) && defined(VX1000_SUPPORT_OLDA7_ASMGNUBOOKE)
#error VX1000_SUPPORT_OLDA7_ASMGNUVLE and VX1000_SUPPORT_OLDA7_ASMGNUBOOKE optimisations cannot be enabled at the same time!
#endif /* VX1000_SUPPORT_OLDA7_ASMGNUVLE && VX1000_SUPPORT_OLDA7_ASMGNUBOOKE */

#if defined(VX1000_OLDA_FORCE_V6) && defined(VX1000_OLDA_FORCE_V7)
#error VX1000_OLDA_FORCE_V6 and VX1000_OLDA_FORCE_V7 cannot be enabled at the same time. Please make up your mind!
#endif /* VX1000_OLDA_FORCE_V6 && VX1000_OLDA_FORCE_V7 */

#if defined(VX1000_OLDA_FORCE_V6) && defined(VX1000_OLDA_FORCE_V8)
#error VX1000_OLDA_FORCE_V6 and VX1000_OLDA_FORCE_V8 cannot be enabled at the same time. Please make up your mind!
#endif /* VX1000_OLDA_FORCE_V6 && VX1000_OLDA_FORCE_V8 */

#if defined(VX1000_OLDA_FORCE_V7) && defined(VX1000_OLDA_FORCE_V8)
#error VX1000_OLDA_FORCE_V7 and VX1000_OLDA_FORCE_V8 cannot be enabled at the same time. Please make up your mind!
#endif /* VX1000_OLDA_FORCE_V7 && VX1000_OLDA_FORCE_V8 */

#if defined(VX1000_STIM_BY_OLDA) && (!defined(VX1000_OLDA))
#error VX1000_STIM_BY_OLDA cannot be enabled while VX1000_OLDA is disabled!
#endif /* VX1000_STIM_BY_OLDA && !VX1000_OLDA */

#if defined(VX1000_STIM_BY_OLDA) && (!defined(VX1000_STIM))
#error VX1000_STIM_BY_OLDA cannot be enabled while VX1000_STIM is disabled!
#endif /* VX1000_STIM_BY_OLDA && !VX1000_STIM */

#if defined(VX1000_TARGET_TRICORE)

#if defined(VX1000_STIM) && (!defined(VX1000_STIM_BY_OLDA))
#if (0==(VX1000_MEMSYNC_TRIGGER_COUNT))
#error On Tricore, VX1000_STIM cannot be enabled while VX1000_STIM_BY_OLDA is disabled!
#endif /* !VX1000_MEMSYNC_TRIGGER_COUNT */
#endif /* VX1000_STIM && !VX1000_STIM_BY_OLDA */

#if (!defined(VX1000_OLDA)) && (!defined(VX1000_DISABLE_INSTRUMENTATION))
#error On Tricore, VX1000_OLDA must be always enabled!
#endif /* !VX1000_OLDA && !VX1000_DISABLE_INSTRUMENTATION */

#endif /* VX1000_TARGET_TRICORE */

#if defined(VX1000_TARGET_SH2)

#if defined(VX1000_STIM) && (!defined(VX1000_STIM_BY_OLDA))
#if (0==(VX1000_MEMSYNC_TRIGGER_COUNT))
#error On SH2, VX1000_STIM cannot be enabled while VX1000_STIM_BY_OLDA is disabled!
#endif /* !VX1000_MEMSYNC_TRIGGER_COUNT */
#endif /* VX1000_STIM && !VX1000_STIM_BY_OLDA */

#if (!defined(VX1000_OLDA)) && (!defined(VX1000_DISABLE_INSTRUMENTATION))
#error On SH2, VX1000_OLDA must be always enabled!
#endif /* !VX1000_OLDA && !VX1000_DISABLE_INSTRUMENTATION */

#endif /* VX1000_TARGET_SH2 */

#if defined(VX1000_TARGET_TMS570)

#if defined(VX1000_STIM_BY_OLDA)
#error On TMS570, VX1000_STIM_BY_OLDA must be always disabled!
#endif /* VX1000_STIM_BY_OLDA */

#if defined(VX1000_OLDA)
#error On TMS570, VX1000_OLDA must be always disabled!
#endif /* VX1000_OLDA */

#endif /* VX1000_TARGET_TMS570 */

#if defined(VX1000_TARGET_XC2000)

#if defined(VX1000_STIM) && (!defined(VX1000_STIM_BY_OLDA))
#if (0==(VX1000_MEMSYNC_TRIGGER_COUNT))
#error On XC2000, VX1000_STIM cannot be enabled while VX1000_STIM_BY_OLDA is disabled!
#endif /* !VX1000_MEMSYNC_TRIGGER_COUNT */
#endif /* VX1000_STIM && !VX1000_STIM_BY_OLDA */

#if defined(VX1000_SUPPORT_OLDA7_BYTEDAQ) && defined(VX1000_OLDA_FORCE_V7)
/*#error On XC2000, VX1000_SUPPORT_OLDA7_BYTEDAQ must be disabled!*/
#endif /* VX1000_SUPPORT_OLDA7_BYTEDAQ & VX1000_OLDA_FORCE_V7*/

#if defined(VX1000_OLDA_FORCE_V8)
/*#error On XC2000, VX1000_OLDA_FORCE_V8 must be disabled!*/
#endif /* VX1000_OLDA_FORCE_V8 */

#endif /* VX1000_TARGET_XC2000 */

/* To save memory, the number range for stim events may be reduced, maximum is 31  */
#if !defined(VX1000_STIM_EVENT_COUNT)
#define VX1000_STIM_EVENT_COUNT 31   /* Count of STIM events used starting from VX1000_STIM_EVENT_OFFSET */
#elif VX1000_STIM_EVENT_COUNT > 31
#error "VX1000_STIM_EVENT_COUNT must be <= 31"
#endif /* VX1000_STIM_EVENT_COUNT && VX1000_STIM_EVENT_COUNT > 31*/

#if !defined(VX1000_STIM_EVENT_OFFSET)
#define VX1000_STIM_EVENT_OFFSET 0U   /* Number of the first STIM event */
#elif (VX1000_STIM_EVENT_OFFSET > 30)
#error "Event numbers must be <= 30!"
#endif /* VX1000_STIM_EVENT_OFFSET && VX1000_STIM_EVENT_OFFSET > 30 */

#if defined(VX1000_OLDA_FORCE_V8) || (!defined(VX1000_SUPPRESS_TRACE_SUPPORT))
#if !defined(VX1000_OLDA_EVENT_COUNT)
#define VX1000_OLDA_EVENT_COUNT 31U
#elif VX1000_OLDA_EVENT_COUNT > 512U
#error "VX1000_OLDA_EVENT_COUNT must be <= 512U!"
#endif /* VX1000_OLDA_EVENT_COUNT && VX1000_OLDA_EVENT_COUNT > 512 */
#else /* VX1000_SUPPRESS_TRACE_SUPPORT || !VX1000_OLDA_FORCE_V8 */
#if !defined(VX1000_OLDA_AUDMBR_REG_ADDR)
#if !defined(VX1000_OLDA_EVENT_COUNT)
#define VX1000_OLDA_EVENT_COUNT 31U
#elif VX1000_OLDA_EVENT_COUNT > 31U
#error "VX1000_OLDA_EVENT_COUNT must be <= 31U!"
#endif /* VX1000_OLDA_EVENT_COUNT && VX1000_OLDA_EVENT_COUNT > 31 */
#else /* VX1000_OLDA_AUDMBR_REG_ADDR) */
#if !defined(VX1000_OLDA_EVENT_COUNT)
#define VX1000_OLDA_EVENT_COUNT 15U
#elif VX1000_OLDA_EVENT_COUNT > 15U
#error "VX1000_OLDA_EVENT_COUNT must be <= 15U because the AUDMBR register is selected for event signalling!"
#endif /* VX1000_OLDA_EVENT_COUNT && VX1000_OLDA_EVENT_COUNT > 15 */
#endif /* VX1000_OLDA_AUDMBR_REG_ADDR) */
#endif /* VX1000_SUPPRESS_TRACE_SUPPORT || !VX1000_OLDA_FORCE_V8 */

#if (VX1000_MEMSYNC_TRIGGER_COUNT > 1) && defined(VX1000_MEMSYNC_TRIGGER_PTR)
#error "VX1000_MEMSYNC_TRIGGER_PTR must NOT be defined by the user when VX1000_MEMSYNC_TRIGGER_COUNT > 1"
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT vs. VX1000_MEMSYNC_TRIGGER_PTR */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 1) && (!defined(VX1000_MEMSYNC_TRIGGER_PTR0))
#error "VX1000_MEMSYNC_TRIGGER_PTR0 must be provided and point to 64 bytes unused RAM!"
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT vs. VX1000_MEMSYNC_TRIGGER_PTRx */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 1) && (!defined(VX1000_MEMSYNC_TRIGGER_PTR1))
#error "VX1000_MEMSYNC_TRIGGER_PTR1 must be provided and point to 64 bytes unused RAM!"
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT vs. VX1000_MEMSYNC_TRIGGER_PTRx */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 2) && (!defined(VX1000_MEMSYNC_TRIGGER_PTR2))
#error "VX1000_MEMSYNC_TRIGGER_PTR2 must be provided and point to 64 bytes unused RAM!"
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT vs. VX1000_MEMSYNC_TRIGGER_PTRx */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 3) && (!defined(VX1000_MEMSYNC_TRIGGER_PTR3))
#error "VX1000_MEMSYNC_TRIGGER_PTR3 must be provided and point to 64 bytes unused RAM!"
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT vs. VX1000_MEMSYNC_TRIGGER_PTRx */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 4) && (!defined(VX1000_MEMSYNC_TRIGGER_PTR4))
#error "VX1000_MEMSYNC_TRIGGER_PTR4 must be provided and point to 64 bytes unused RAM!"
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT vs. VX1000_MEMSYNC_TRIGGER_PTRx */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 5) && (!defined(VX1000_MEMSYNC_TRIGGER_PTR5))
#error "VX1000_MEMSYNC_TRIGGER_PTR5 must be provided and point to 64 bytes unused RAM!"
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT vs. VX1000_MEMSYNC_TRIGGER_PTRx */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 6) && (!defined(VX1000_MEMSYNC_TRIGGER_PTR6))
#error "VX1000_MEMSYNC_TRIGGER_PTR6 must be provided and point to 64 bytes unused RAM!"
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT vs. VX1000_MEMSYNC_TRIGGER_PTRx */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 7) && (!defined(VX1000_MEMSYNC_TRIGGER_PTR7))
#error "VX1000_MEMSYNC_TRIGGER_PTR7 must be provided and point to 64 bytes unused RAM!"
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT vs. VX1000_MEMSYNC_TRIGGER_PTRx */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 8)
#error "VX1000_MEMSYNC_TRIGGER_COUNT configured too big (max. 8 is allowed)!"
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 7 */

#if (defined(VX1000_SUPPORT_OLDA8CMD_CP64N) && defined(VX1000_OLDA_FORCE_V8)) && (!defined(VX1000_UINT64))
#error VX1000_SUPPORT_OLDA8CMD_CP64N cannot be enabled while VX1000_UINT64 is not provided!
#endif /* VX1000_SUPPORT_OLDA8CMD_CP64N && VX1000_OLDA_FORCE_V8 && !VX1000_UINT64 */

#if defined(VX1000_HOOK_BASED_BYPASSING) && (!defined(VX1000_STIM_BY_OLDA))
#error VX1000_HOOK_BASED_BYPASSING cannot be enabled while VX1000_STIM_BY_OLDA is disabled!
#endif /* VX1000_HOOK_BASED_BYPASSING && !VX1000_STIM_BY_OLDA */

#if defined(VX1000_HOOK_BASED_BYPASSING) && (!defined(VX1000_BYPASS_HBB_LUT_ENTRIES))
#error VX1000_BYPASS_HBB_LUT_ENTRIES must be defined to a value >0 if VX1000_HOOK_BASED_BYPASSING shall be used!
#endif /* VX1000_HOOK_BASED_BYPASSING && !VX1000_BYPASS_HBB_LUT_ENTRIES */

#if defined(VX1000_HOOK_BASED_BYPASSING) && (!defined(VX1000_BYPASS_HBB_LUT_ADDR))
#error VX1000_BYPASS_HBB_LUT_ADDR must be defined if VX1000_HOOK_BASED_BYPASSING shall be used!
#endif /* VX1000_HOOK_BASED_BYPASSING && !VX1000_BYPASS_HBB_LUT_ADDR */

#if defined(VX1000_HOOK_BASED_BYPASSING) && (!defined(VX1000_STIM_FORCE_V1))
#error VX1000_HOOK_BASED_BYPASSING cannot be enabled while VX1000_STIM_FORCE_V1 is disabled!
#endif /* VX1000_HOOK_BASED_BYPASSING && !VX1000_STIM_FORCE_V1 */

#if defined(VX1000_BYPASS_ALL_CHANS_STIMD) && (!defined(VX1000_STIM_FORCE_V1))
#error VX1000_BYPASS_ALL_CHANS_STIMD cannot be enabled while VX1000_STIM_FORCE_V1 is disabled!
#endif /* VX1000_BYPASS_ALL_CHANS_STIMD && !VX1000_STIM_FORCE_V1 */

#if defined(VX1000_STIM_FORCE_V1) && (!defined(VX1000_SET_STIM_INFO))
#error VX1000_SET_STIM_INFO() must be defined properly when VX1000_STIM_FORCE_V1 is enabled!
#endif /* VX1000_STIM_FORCE_V1 && !VX1000_SET_STIM_INFO */

#if defined(VX1000_OVERLAY)

#if (defined(VX1000_INIT_CAL_PAGE) && defined(VX1000_GET_CAL_PAGE)) && (defined(VX1000_SET_CAL_PAGE) && defined(VX1000_COPY_CAL_PAGE))
#define VX1000_OVERLAY_USERMANAGED
#endif /* VX1000_INIT_CAL_PAGE && VX1000_GET_CAL_PAGE && VX1000_SET_CAL_PAGE && VX1000_COPY_CAL_PAGE */

#if ((defined(VX1000_OVLENBL_SYNC_PAGESWITCH) || defined(VX1000_OVLENBL_CORE_SYNC_PAGESW)) && defined(VX1000_OVERLAY_USERMANAGED))
#error User-defined overlay handling callbacks cannot be used in conjunction with (core-) synchronous page switching
#endif /* (VX1000_OVLENBL_SYNC_PAGESWITCH || VX1000_OVLENBL_CORE_SYNC_PAGESW) && VX1000_OVERLAY_USERMANAGED */

#if defined(VX1000_OVERLAY_VX_CONFIGURABLE) && (defined(VX1000_OVERLAY_USERMANAGED))
#error VX1000_xxx_CAL_PAGE() user callbacks will NOT be executed because VX1000_OVERLAY_VX_CONFIGURABLE is enabled
#endif /* VX1000_OVERLAY_VX_CONFIGURABLE & VX1000_OVERLAY_USERMANAGED */

#if defined(VX1000_TARGET_POWERPC)

#if defined(VX1000_OVERLAY_VX_CONFIGURABLE) && (defined(VX1000_OVERLAY_TLB) || defined(VX1000_OVERLAY_DESCR_IDX))
#error VX1000_OVERLAY_VX_CONFIGURABLE cannot be enabled while VX1000_OVERLAY_TLB or VX1000_OVERLAY_DESCR_IDX is enabled
#endif /* VX1000_OVERLAY_VX_CONFIGURABLE && (VX1000_OVERLAY_TLB || VX1000_OVERLAY_DESCR_IDX) */

#if defined(VX1000_OVERLAY_USERMANAGED) && defined(VX1000_OVERLAY_TLB)
#error VX1000_xxx_CAL_PAGE() user callbacks will NOT be executed because VX1000_OVERLAY_TLB is enabled
#endif /* VX1000_OVERLAY_USERMANAGED & VX1000_OVERLAY_TLB */

#if defined(VX1000_OVERLAY_USERMANAGED) && (defined(VX1000_OVERLAY_DESCR_IDX))
#error VX1000_xxx_CAL_PAGE() user callbacks will NOT be executed because VX1000_OVERLAY_DESCR_IDX is enabled
#endif /* VX1000_OVERLAY_USERMANAGED & VX1000_OVERLAY_DESCR_IDX */

#if ((!defined(VX1000_OVERLAY_TLB)) && (!defined(VX1000_OVERLAY_DESCR_IDX)) && (!defined(VX1000_MPC56xCRAM_BASE_ADDR)) && (!defined(VX1000_OVERLAY_VX_CONFIGURABLE))) && (!defined(VX1000_OVERLAY_USERMANAGED))
#error To use VX1000_OVERLAY, configure VX1000_MPC56xCRAM_BASE_ADDR, VX1000_OVERLAY_TLB, VX1000_OVERLAY_DESCR_IDX or VX1000_OVERLAY_VX_CONFIGURABLE or all VX1000_xxx_CAL_PAGE() macros in VX1000_cfg.h !
#endif /* !VX1000_OVERLAY_TLB & !VX1000_OVERLAY_DESCR_IDX & !VX1000_MPC56xCRAM_BASE_ADDR & !VX1000_OVERLAY_VX_CONFIGURABLE && !VX1000_OVERLAY_USERMANAGED */

#endif /* VX1000_TARGET_POWERPC */

#if defined(VX1000_TARGET_TRICORE)

#if defined(VX1000_OVERLAY_VX_CONFIGURABLE) && (defined(VX1000_OVERLAY_DESCR_IDX))
#error VX1000_OVERLAY_VX_CONFIGURABLE cannot be enabled while VX1000_OVERLAY_DESCR_IDX is enabled
#endif /* VX1000_OVERLAY_VX_CONFIGURABLE & VX1000_OVERLAY_DESCR_IDX */

#if ( (!defined(VX1000_OVERLAY_DESCR_IDX)) && (!defined(VX1000_OVERLAY_VX_CONFIGURABLE)) ) && (!defined(VX1000_OVERLAY_USERMANAGED))
#error To use VX1000_OVERLAY, configure VX1000_OVERLAY_DESCR_IDX or VX1000_OVERLAY_VX_CONFIGURABLE or all VX1000_xxx_CAL_PAGE() macros in VX1000_cfg.h !
#endif /* !VX1000_OVERLAY_DESCR_IDX && !VX1000_OVERLAY_VX_CONFIGURABLE && !VX1000_OVERLAY_USERMANAGED */

#if defined(VX1000_OVERLAY_USERMANAGED) && (defined(VX1000_OVERLAY_DESCR_IDX))
#error VX1000_xxx_CAL_PAGE() user callbacks will NOT be executed because VX1000_OVERLAY_DESCR_IDX is enabled
#endif /* VX1000_OVERLAY_USERMANAGED & VX1000_OVERLAY_DESCR_IDX */

#endif /* VX1000_TARGET_TRICORE */

#if defined(VX1000_TARGET_SH2)

#if defined(VX1000_OVERLAY_USERMANAGED) && (defined(VX1000_SH2_FCU_BASE_ADDR))
#error VX1000_xxx_CAL_PAGE() user callbacks will NOT be executed because VX1000_SH2_FCU_BASE_ADDR is enabled
#endif /* VX1000_OVERLAY_USERMANAGED & VX1000_SH2_FCU_BASE_ADDR */

#if ( (!defined(VX1000_SH2_FCU_BASE_ADDR)) && (!defined(VX1000_OVERLAY_VX_CONFIGURABLE)) ) && (!defined(VX1000_OVERLAY_USERMANAGED))
#error To use VX1000_OVERLAY, configure VX1000_SH2_FCU_BASE_ADDR or VX1000_OVERLAY_VX_CONFIGURABLE or all VX1000_xxx_CAL_PAGE() macros in VX1000_cfg.h !
#endif /* !VX1000_SH2_FCU_BASE_ADDR && !VX1000_OVERLAY_VX_CONFIGURABLE && !VX1000_OVERLAY_USERMANAGED */

#endif /* VX1000_TARGET_SH2 */

#if defined(VX1000_TARGET_X850)

#if (!defined(VX1000_OVERLAY_VX_CONFIGURABLE)) && (!defined(VX1000_OVERLAY_USERMANAGED))
#error To use VX1000_OVERLAY, configure VX1000_OVERLAY_VX_CONFIGURABLE or all VX1000_xxx_CAL_PAGE() macros in VX1000_cfg.h !
#endif /* !VX1000_OVERLAY_VX_CONFIGURABLE && !VX1000_OVERLAY_USERMANAGED */

#endif /* VX1000_TARGET_X850 */

#if defined(VX1000_TARGET_TMS570)

#if (!defined(VX1000_OVERLAY_VX_CONFIGURABLE)) && (!defined(VX1000_OVERLAY_USERMANAGED))
#error To use VX1000_OVERLAY, configure VX1000_OVERLAY_VX_CONFIGURABLE or all VX1000_xxx_CAL_PAGE() macros in VX1000_cfg.h !
#endif /* !VX1000_OVERLAY_VX_CONFIGURABLE && !VX1000_OVERLAY_USERMANAGED */

#endif /* VX1000_TARGET_TMS570 */

#if defined(VX1000_TARGET_XC2000)

#if (!defined(VX1000_OVERLAY_VX_CONFIGURABLE)) && (!defined(VX1000_OVERLAY_USERMANAGED))
#error To use VX1000_OVERLAY, configure VX1000_OVERLAY_VX_CONFIGURABLE or all VX1000_xxx_CAL_PAGE() macros in VX1000_cfg.h !
#endif /* !VX1000_OVERLAY_VX_CONFIGURABLE && !VX1000_OVERLAY_USERMANAGED */

#endif /* VX1000_TARGET_XC2000 */

#if (!defined(VX1000_OVERLAY_VX_CONFIGURABLE)) && (!defined(VX1000_OVERLAY_USERMANAGED))

/* check completeness of the common settings */
#if ((!defined(VX1000_OVERLAY_ADDR)) || (!defined(VX1000_OVERLAY_SIZE)) || (!defined(VX1000_CALRAM_ADDR)) )
#error Please define the overlay parameters VX1000_OVERLAY_SIZE, VX1000_OVERLAY_ADDR and VX1000_CALRAM_ADDR in VX1000_cfg.h !
#endif /* !VX1000_OVERLAY_ADDR | !VX1000_OVERLAY_SIZE | !VX1000_CALRAM_ADDR */

#if defined(VX1000_TARGET_POWERPC)

/* check completeness of the settings for the MMU method */
#if defined(VX1000_OVERLAY_TLB)
#if !(defined(VX1000_MPC_MMU_MTMAS0)&&defined(VX1000_MPC_MMU_MTMAS1)&&defined(VX1000_MPC_MMU_MTMAS2)&&defined(VX1000_MPC_MMU_MTMAS3))
#error VX1000_MPC_MMU_MTMASxx() must be defined for your compiler in VX1000_cfg.h
#endif /* VX1000_MPC_MMU_MTMAS** */
#if !defined(VX1000_MPC_MMU_TLBWE)
#error VX1000_MPC_MMU_TLBWE() must be defined for your compiler in VX1000_cfg.h
#endif /* !VX1000_MPC_MMU_TLBWE */
#endif /* VX1000_OVERLAY_TLB */

/* check completeness of the settings for the flash port method */
#if defined(VX1000_OVERLAY_DESCR_IDX) && (!defined(VX1000_FLASHPORTCTLR_BASE_ADDR))
#error Please define the overlay parameters VX1000_FLASHPORTCTLR_BASE_ADDR in VX1000_cfg.h !
#endif /* VX1000_OVERLAY_DESCR_IDX && !VX1000_FLASHPORTCTLR_BASE_ADDR */

#endif /* VX1000_TARGET_POWERPC */

#if defined(VX1000_TARGET_XC2000)

/* check completeness of the common settings */
#if ((!defined(VX1000_OVERLAY_ADDR)) || (!defined(VX1000_OVERLAY_SIZE)) || (!defined(VX1000_CALRAM_ADDR)) )
#error Please define the overlay parameters VX1000_OVERLAY_SIZE, VX1000_OVERLAY_ADDR and VX1000_CALRAM_ADDR in VX1000_cfg.h !
#endif /* !VX1000_OVERLAY_ADDR | !VX1000_OVERLAY_SIZE | !VX1000_CALRAM_ADDR */

#endif /* VX1000_TARGET_XC2000 */

#endif /* !VX1000_OVERLAY_VX_CONFIGURABLE && !VX1000_OVERLAY_USERMANAGED */

#endif /* VX1000_OVERLAY */

#if defined(VX1000_OVLENBL_USE_VX_EPK_TRANS) && (!defined(VX1000_OVLENBL_VALIDATE_PAGESW))
#error VX1000_OVLENBL_USE_VX_EPK_TRANS cannot be enabled while VX1000_OVLENBL_VALIDATE_PAGESW is disabled!
#endif /* VX1000_OVLENBL_USE_VX_EPK_TRANS & !VX1000_OVLENBL_VALIDATE_PAGESW */

#if defined(VX1000_OVLENBL_USE_VX_EPK_TRANS) && (!defined(VX1000_SYNCAL_VALIDATE_WP_CB))
#error VX1000_SYNCAL_VALIDATE_WP_CB must be configured to be able to use the EPK translation!
#endif /* VX1000_OVLENBL_USE_VX_EPK_TRANS & !VX1000_OVLENBL_USE_VX_EPK_TRANS */

#if defined(VX1000_OVLENBL_USE_VX_EPK_TRANS) && (!defined(VX1000_OVL_EPK_REFPAGE_ADDR))
#error VX1000_OVL_EPK_REFPAGE_ADDR must be configured to be able to use the EPK translation!
#endif /* VX1000_OVLENBL_USE_VX_EPK_TRANS & !VX1000_OVL_EPK_REFPAGE_ADDR */

#if defined(VX1000_OVLENBL_USE_VX_EPK_TRANS) && (!defined(VX1000_OVL_EPK_LENGTH))
#error VX1000_OVL_EPK_LENGTH must be configured to be able to use the EPK translation!
#endif /* VX1000_OVLENBL_USE_VX_EPK_TRANS & !VX1000_OVL_EPK_LENGTH */

#if defined(VX1000_OVLENBL_VALIDATE_PAGESW) && (!(defined(VX1000_SYNCAL_USRVALIDATE_WP_CB) || defined(VX1000_OVLENBL_USE_VX_EPK_TRANS)))
#error VX1000_OVLENBL_VALIDATE_PAGESW cannot be enabled while neither VX1000_SYNCAL_USRVALIDATE_WP_CB nor EPK translation are configured!
#endif /* VX1000_OVLENBL_VALIDATE_PAGESW & !VX1000_SYNCAL_USRVALIDATE_WP_CB & !VX1000_OVLENBL_USE_VX_EPK_TRANS */

#if defined(VX1000_OVLENBL_CORE_SYNC_PAGESW) && (!defined(VX1000_OVL_CAL_BUS_MASTER))
#error VX1000_OVLENBL_CORE_SYNC_PAGESW cannot be enabled while VX1000_OVL_CAL_BUS_MASTER is not configured!
#endif /* VX1000_OVLENBL_CORE_SYNC_PAGESW & !VX1000_OVL_CAL_BUS_MASTER */

#if defined(VX1000_OVLENBL_SYNC_PAGESWITCH) && (!defined(VX1000_OVL_CAL_BUS_MASTER))
#error VX1000_OVLENBL_SYNC_PAGESWITCH cannot be enabled while VX1000_OVL_CAL_BUS_MASTER is not configured!
#endif /* VX1000_OVLENBL_SYNC_PAGESWITCH & !VX1000_OVL_CAL_BUS_MASTER */

#if defined(VX1000_OVLENBL_CORE_SYNC_PAGESW) && (!defined(VX1000_OVLENBL_SYNC_PAGESWITCH))
#error VX1000_OVLENBL_CORE_SYNC_PAGESW cannot be used without VX1000_OVLENBL_SYNC_PAGESWITCH!
#endif /* VX1000_OVLENBL_CORE_SYNC_PAGESW & !VX1000_OVLENBL_SYNC_PAGESWITCH */

#if defined(VX1000_OVLENBL_RST_ON_CALWAKEUP) && (!defined(VX1000_OVL_RST_ON_CAL_WAKEUP_CB))
#error VX1000_OVL_RST_ON_CAL_WAKEUP_CB() must be configured because VX1000_OVLENBL_RST_ON_CALWAKEUP is enabled!
#endif /* VX1000_OVLENBL_RST_ON_CALWAKEUP & !VX1000_OVL_RST_ON_CAL_WAKEUP_CB */

#if defined(VX1000_MAILBOX) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
#if (VX1000_MAILBOX_SLOT_DWORDS) < 3U
#error VX1000_MAILBOX_SLOT_DWORDS must be at least 3 ! /* 4 bytes + 8 bytes xcp command = 12bytes = 3 dwords */
#endif /* VX1000_MAILBOX_SLOT_DWORDS */
#if (VX1000_MAILBOX_SLOTS < 1U) || (((VX1000_MAILBOX_SLOTS) & ((VX1000_MAILBOX_SLOTS) - 1U)) != 0U)
#error VX1000_MAILBOX_SLOTS must be a power of 2 !
#endif /* VX1000_MAILBOX_SLOTS */
#endif /* VX1000_MAILBOX & ! VX1000_COMPILED_FOR_SLAVECORES */

#if (defined(VX1000_DETECTION) && defined(VX1000_OLDA)) && (defined(VX1000_OLDA_FORCE_V6))
/* The coldstart settings in VxConfig lack the "SwapValue" field, therefore VX can only handle address encodings that are not based on swapValue -> only v7 encoding possible */
#error Coldstart/VX1000Detection is incompatible to olda v6 --> either disable VX1000_DETECTION or VX1000_OLDA_FORCE_V6!
#endif /* VX1000_DETECTION && VX1000_OLDA_FORCE_V6 */

#if (!defined(VX1000_MEMSYNC_TRIGGER_COUNT)) || ((VX1000_MEMSYNC_TRIGGER_COUNT) == 0)
#if defined(VX1000_MEMSYNC_COPY_COUNT)
#if ((VX1000_MEMSYNC_COPY_COUNT) > 0)
#error VX1000_MEMSYNC_COPY_COUNT cannot be defined > 0 unless VX1000_MEMSYNC_TRIGGER_COUNT is defined > 0, too!
#endif /* VX1000_MEMSYNC_COPY_COUNT > 0*/
#endif /* VX1000_MEMSYNC_COPY_COUNT */
#endif /* !VX1000_MEMSYNC_TRIGGER_COUNT */

#if defined(VX1000_OLDA_BENCHMARK)
#if ((VX1000_OLDA_BENCHMARK_CNT) == 0U)
#error VX1000_OLDA_BENCHMARK_CNT must be defined greater than zero when VX1000_OLDA_BENCHMARK is enabled!
#endif /* !VX1000_OLDA_BENCHMARK_CNT */
#endif /* VX1000_OLDA_BENCHMARK */

#if defined(VX1000_LFAST_BASEADDR) && (!defined(VX1000_SIPI_CLK_TXN_TXP_RXN_RXP))
#error Please define VX1000_SIPI_CLK_TXN_TXP_RXN_RXP() to C code that enables the 5 LFAST pins as clock out plus 2 LVDS pairs.
#endif /* VX1000_LFAST_BASEADDR & !VX1000_SIPI_CLK_TXN_TXP_RXN_RXP */

#endif /* !VX1000_DISABLE_INSTRUMENTATION */

/* States */

/* ToolDetectState */
#define VX1000_TDS_INIT                  0x00000001UL    /* VX1000_INIT() has been called */
#define VX1000_TDS_DETECT                0x00000002UL    /* VX1000_DETECT_VX() has been called */
#define VX1000_TDS_DETECTED              0x00000004UL    /* VX1000 was detected by VX1000_DETECT_VX() */
#define VX1000_TDS_COLDSTART_DELAY_REQ   0x00000008UL    /* Coldstart delay request has been accepted by VX */
#define VX1000_TDS_COLDSTART_DONE        0x00000010UL    /* Coldstart handshake successfully executed */
#define VX1000_TDS_COLDSTART_TIMEOUT     0x00000020UL    /* Coldstart handshake resulted in timeout */
#define VX1000_TDS_COLDSTART_DELAY       0x00000040UL    /* Coldstart Delay has been entered */
#define VX1000_TDS_APPRST                0x00000080UL    /* Resume after application reset done */
#define VX1000_TDS_FKL_REQ_DETECTED      0x00000100UL    /* FlashPrepare request detected: trying to handle it */
#define VX1000_TDS_FKL_REQ_IGNORED       0x00000200UL    /* FlashPrepare request could not be served right now; app still runs */
#define VX1000_TDS_FKL_FORCED_IDLE       0x00000400UL    /* request served: ECU stays idle; FKL can be downloaded by the tool; */
                                                         /* gVX1000.MagicId contains a pointer to the new communication space. */
#define VX1000_TDS_DETECT_DONE           0x00000800UL    /* VX1000 Tooldetection sequence finished */
#define VX1000_TDS_ERROR                 0x00001000UL    /* VX1000_INIT() detected a misconfiguration */
#define VX1000_TDS_VX_ACCESS_DISABLED    0x00002000UL    /* The VX has NO access to the ECU, another tool is connected */
#define VX1000_TDS_WORKING_PAGE          0x00004000UL    /* 0: ECU is on reference page / 1: ECU is on working page */

#define VX1000_TDS_COLDSTART_CHS_BUSY    0x00010000UL    /* Coldstart checksum calculation ongoing */
#define VX1000_TDS_COLDSTART_CHS_DONE    0x00020000UL    /* Coldstart checksum calculation done */

#define VX1000_TDS_NO_BYP_RESUME         0x00040000UL    /* Inform VX1000 not to resume VX bypassing */

/* ToolCtrlState */
#define VX1000_TCS_PRESENT               0x00000001UL    /* VX1000 is present */
#define VX1000_TCS_COLDSTART_DELAY       0x00000002UL    /* VX1000 requests coldstart delay */
#define VX1000_TCS_COLDSTART_DONE        0x00000004UL    /* VX1000 coldstart configuration done */
#define VX1000_TCS_FKL_REQUEST           0x00000008UL    /* VX1000 requests the appl to go idle and prepare to jump to a pointer */
#define VX1000_TCS_SOFTRESET_PREP        0x00000010UL    /* VX1000 acknowledges a soft-reset announcement */
#define VX1000_TCS_SWITCH_TO_WP          0x00000020UL    /* Tell the ECU to switch to WP */
#define VX1000_TCS_CAL_WAKEUP            0x00000040UL    /* The VX1000 has woken up the ECU */
#define VX1000_TCS_SKIP_WP_INIT          0x00000080UL    /* Inform the ECU that the WP's RAM content is still intact */


/*------------------------------------------------------------------------------ */
/* VX1000 Detection                                                              */

#if defined(VX1000_DETECTION) || (defined(VX1000_TARGET_TRICORE) && (!defined(VX1000_DISABLE_INSTRUMENTATION)))
/* Check whether a VX1000 was already detected by VX1000_DETECT_VX() */
/*#define VX1000_DETECTED()   ((gVX1000.ToolCtrlState  & (VX1000_TDS_DETECTED)) != 0) //really "Ctrl & TDS"? and not #define VX1000_DETECTED() ((gVX1000.ToolDetectState & (VX1000_TDS_DETECTED)) != 0)*/
#define VX1000_DETECTED() (((gVX1000.ToolCtrlState & (VX1000_TCS_PRESENT)) != 0UL) || ((gVX1000.ToolDetectState & (VX1000_TDS_DETECTED)) != 0UL)) /* Wrapper API:   VX1000If_DeviceDetected */
#else /* !VX1000_DETECTION & !VX1000_TARGET_TRICORE | VX1000_DISABLE_INSTRUMENTATION */
#define VX1000_DETECTED()                (0)             /* means "no VX detected" */
#endif /* !VX1000_DETECTION & !VX1000_TARGET_TRICORE | VX1000_DISABLE_INSTRUMENTATION */


/*------------------------------------------------------------------------------ */
/* Coldstart                                                                     */

#define VX1000_COLDSTART_CHS_MAGIC 0xFEC70A09UL
#if (defined(VX1000_DETECTION) || (defined(VX1000_TARGET_TRICORE) && (!defined(VX1000_DISABLE_INSTRUMENTATION)))) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
extern volatile VX1000_UINT32 gVX1000_DETECT_StartTime;
#if defined(VX1000_COLDSTART_BENCHMARK) && defined(VX1000_OLDA)
extern volatile VX1000_UINT32 gVX1000_DETECT_StartTimeAsyncEnd, gVX1000_DETECT_ToolDetectTime, gVX1000_DETECT_ChecksumDoneTime, gVX1000_DETECT_EndTimeAsyncStart, gVX1000_DETECT_EndTime;
#define VX1000_COLDSTART_BENCHMARK_DATA volatile VX1000_UINT32 gVX1000_DETECT_StartTime, gVX1000_DETECT_StartTimeAsyncEnd, gVX1000_DETECT_ToolDetectTime, gVX1000_DETECT_ChecksumDoneTime, gVX1000_DETECT_EndTimeAsyncStart, gVX1000_DETECT_EndTime
#else /* !VX1000_COLDSTART_BENCHMARK || !VX1000_OLDA*/
#define VX1000_COLDSTART_BENCHMARK_DATA volatile VX1000_UINT32 gVX1000_DETECT_StartTime
#endif /* !VX1000_COLDSTART_BENCHMARK || !VX1000_OLDA*/
#else  /* (!VX1000_DETECTION & !VX1000_TARGET_TRICORE) | VX1000_DISABLE_INSTRUMENTATION | VX1000_COMPILED_FOR_SLAVECORES */
#endif /* (!VX1000_DETECTION & !VX1000_TARGET_TRICORE) | VX1000_DISABLE_INSTRUMENTATION | VX1000_COMPILED_FOR_SLAVECORES */


/*------------------------------------------------------------------------------ */
/* Flash kernel download support                                                 */
#if defined(VX1000_FKL_SUPPORT_ADDR)
extern void VX1000_SUFFUN(vx1000_FlashPrepareLoop)(void);
#define VX1000_DETECT_FKL_REQUESTS() do                                                                                         \
  {                                                                                                                             \
    if ((gVX1000.MagicId == (VX1000_UINT32)(VX1000_STRUCT_MAGIC)) && ((gVX1000.ToolCtrlState & (VX1000_TCS_FKL_REQUEST)) != 0)) \
    {                                                                                                                           \
      VX1000_SUFFUN(vx1000_FlashPrepareLoop)();                                                                                 \
    }                                                                                                                           \
  } while (0); /* This dummy loop is only here for MISRA */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */

typedef void VX1000_FKL_FCT(VX1000_UINT32, VX1000_UINT32, VX1000_UINT32); /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
typedef struct
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 DeprotectTrigger;        /*  0+ */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 DeprotectState;          /*  2+ */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 TransitionTimeout;       /*  4+ */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 WdgData1;                /*  8+ */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 WdgData2;                /* 12+ */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 WdgData3_FklParam3;      /* 16+ */ /* it depends on the platform whether the watchdog handler or the kernel needs more parameters */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 FklParam1;               /* 20+ */
  VX1000_FKL_FCT * VX1000_INNERSTRUCT_VOLATILE EntryPoint;           /* 24+ */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 FklParam2;               /* 28+ */
} VX1000_FKL_WORKSPACE_T;              /* PRQA S 3205 */ /* Accepting to define a type that might be unused in some users' configurations/applications */
#define VX1000_FKL_WORKSPACE           (VX1000_ADDR_TO_PTR2FW(0xFFFFffe0UL & (VX1000_UINT32)(VX1000_FKL_SUPPORT_ADDR)))
#define VX1000_FKL_TRANSITION_TIMEOUT  0xffffffffUL
#define VX1000_FKL_STATE1CODE          0x1243U
#define VX1000_FKL_STATE2CODE          0x2486U
#define VX1000_FKL_STATE3CODE          0x36c9U
#define VX1000_FKL_STATE4CODE          0x480cU
#define VX1000_FKL_STATE5CODE          0x5a4fU
#define VX1000_FKL_TOSTATE1CODE        0x6bf0U
#define VX1000_FKL_TOSTATE2CODE        0x7a0fU
#define VX1000_FKL_TOSTATE3CODE        0x891eU
#define VX1000_FKL_TOSTATE4CODE        0x98e1U
#define VX1000_FKL_TOSTATE5CODE        0xa72dU
#define VX1000_FKL_LAUNCHCODE          0xb6d2U
#else  /* !VX1000_FKL_SUPPORT_ADDR */
#if defined(VX1000_DISABLE_INSTRUMENTATION)
#define VX1000_DETECT_FKL_REQUESTS()   /* empty */
#else /* !VX1000_DISABLE_INSTRUMENTATION)*/
#define VX1000_DETECT_FKL_REQUESTS() do                                                        \
{                                                                                              \
  if ((gVX1000.ToolCtrlState & (VX1000_TCS_FKL_REQUEST)) != 0)                                 \
  {                                                                                            \
    gVX1000.ToolDetectState |= (VX1000_TDS_FKL_REQ_DETECTED) | (VX1000_TDS_FKL_REQ_IGNORED);   \
  }                                                                                            \
} while (0);  /* This dummy loop is only here for MISRA */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#endif /* !VX1000_DISABLE_INSTRUMENTATION */
#endif /* !VX1000_FKL_SUPPORT_ADDR */


/*------------------------------------------------------------------------------ */
/* VX1000 Mailbox                                                                */

#define VX1000_MAILBOX_OK              0UL
#define VX1000_MAILBOX_ERR_FULL        1UL
#define VX1000_MAILBOX_ERR_EMPTY       2UL
#define VX1000_MAILBOX_ERR_INDICES     3UL
#define VX1000_MAILBOX_ERR_SEQUENCE    4UL
#define VX1000_MAILBOX_ERR_SIZE        5UL
#define VX1000_MAILBOX_SLOT_RSVD       6UL
#define VX1000_MAILBOX_ERR_NULL        7UL
#define VX1000_MAILBOX_ERR_SPLIT_PEND  8UL

#define VX1000_CRC_CMD_UNKNOWN         0x20U
#define VX1000_CRC_CMD_BUSY            0x10U
#define VX1000_CRC_CMD_SYNTAX          0x21U
#define VX1000_CRC_OUT_OF_RANGE        0x22U
#define VX1000_CRC_WRITE_PROTECTED     0x23U
#define VX1000_CRC_ACCESS_DENIED       0x24U
#define VX1000_CRC_ACCESS_LOCKED       0x25U
#define VX1000_CRC_PAGE_NOT_VALID      0x26U
#define VX1000_CRC_PAGE_MODE_NOT_VALID 0x27U
#define VX1000_CRC_SEGMENT_NOT_VALID   0x28U

#define VX1000_CAL_ECU                 0x01U
#define VX1000_CAL_XCP                 0x02U
#define VX1000_CAL_ALL                 0x80U

#if defined(VX1000_MAILBOX) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))

#define VX1000_MAILBOX_MAGIC           0xFEC70A06UL
#define VX1000_MAILBOX_FREE_WR_SLOTS   (((VX1000_UINT8)(gVX1000.Mailbox.SM_ReadIdx-gVX1000.Mailbox.SM_WriteIdx-1)) & ((VX1000_MAILBOX_SLOTS)-1U))
#define VX1000_MAILBOX_USED_RD_SLOTS   (((VX1000_UINT32)(gVX1000.Mailbox.MS_WriteIdx - gVX1000.Mailbox.MS_ReadIdx)) & ((VX1000_MAILBOX_SLOTS) - 1U))
#define VX1000_MAILBOX_NEXT(idx)       (((idx) + 1U) & ((VX1000_MAILBOX_SLOTS) - 1U))  /* PRQA S 3453 */ /* Accepting violation of MISRA rule 19.7 because a function call would consume unnecessarily high resources */

typedef struct
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32  MagicId;       /* 0x00 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32  Version;       /* 0x04 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16  SlotSize;      /* 0x08 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8   MS_Slots;      /* 0x0A */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8   SM_Slots;      /* 0x0B */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32  MS_Ptr;        /* 0x0C */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32  SM_Ptr;        /* 0x10 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8   MS_ReadIdx;    /* 0x14 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8   MS_WriteIdx;   /* 0x15 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8   SM_ReadIdx;    /* 0x16 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8   SM_WriteIdx;   /* 0x17 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16  RstReq;        /* 0x18 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16  RstAck;        /* 0x1A */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32  MSData[VX1000_MAILBOX_SLOTS][VX1000_MAILBOX_SLOT_DWORDS]; /* 0x1C */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32  SMData[VX1000_MAILBOX_SLOTS][VX1000_MAILBOX_SLOT_DWORDS]; /* no fix offset */
} VX1000_MAILBOX_T;

#if (defined(VX1000_MAILBOX_OVERLAY_CONTROL) || defined(VX1000_MAILBOX_FLASH)) || defined(VX1000_MAILBOX_CAL_READ_WRITE)
extern void VX1000_SUFFUN(vx1000_MailboxHandler)(void);
#define VX1000_MAILBOX_CONTROL() VX1000_SUFFUN(vx1000_MailboxHandler)(); /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#else /* !VX1000_MAILBOX_OVERLAY_CONTROL & !VX1000_MAILBOX_FLASH & !VX1000_MAILBOX_CAL_READ_WRITE */
#define VX1000_MAILBOX_CONTROL() /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#endif /* !VX1000_MAILBOX_OVERLAY_CONTROL & !VX1000_MAILBOX_FLASH & !VX1000_MAILBOX_CAL_READ_WRITE */
extern VX1000_UINT32 VX1000_SUFFUN(vx1000_MailboxWrite)(VX1000_UINT32 len, const VX1000_CHAR * pBuf);
#define VX1000_MAILBOX_WRITE(     L, B)       VX1000_SUFFUN(vx1000_MailboxWrite)((L), (const VX1000_CHAR*)(B))  /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_MAILBOX_WRITE_VOID(L, B) (void)VX1000_SUFFUN(vx1000_MailboxWrite)((L), (const VX1000_CHAR*)(B)); /* usable as a whole statement */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
extern VX1000_UINT32 VX1000_SUFFUN(vx1000_MailboxRead)(VX1000_UINT32 * pLen, VX1000_CHAR * pBuf);
#define VX1000_MAILBOX_READ(     L, B)       VX1000_SUFFUN(vx1000_MailboxRead)((L), (B))  /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_MAILBOX_READ_VOID(L, B) (void)VX1000_SUFFUN(vx1000_MailboxRead)((L), (B)); /* usable as a whole statement */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#define VX1000_MAILBOX_DATA VX1000_MAILBOX_T Mailbox
#define VX1000_MAILBOX_PTR             (VX1000_PTR2VM_TO_ADDRESS(&gVX1000.Mailbox))

#else /* !VX1000_MAILBOX | VX1000_COMPILED_FOR_SLAVECORES */

#define VX1000_MAILBOX_CONTROL()       /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_MAILBOX_WRITE(     L, B) VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(VX1000_MAILBOX_ERR_FULL), (L),((const VX1000_CHAR*)(B)-(const VX1000_CHAR*)(0)),(0),(0)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 0488 */ /* cannot avoid violating MISRA rule 17.4 because pointer arithmetic is the only simple way to cast qualified pointers to integers with only-level-4 violations */
#define VX1000_MAILBOX_WRITE_VOID(L, B) VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(VX1000_MAILBOX_ERR_FULL), (L),((const VX1000_CHAR*)(B)-(const VX1000_CHAR*)(0)),(0),(0)))/* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 0488 */ /* cannot avoid violating MISRA rule 17.4 because pointer arithmetic is the only simple way to cast qualified pointers to integers with only-level-4 violations */
#define VX1000_MAILBOX_READ(     L, B) VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(VX1000_MAILBOX_ERR_EMPTY), ((VX1000_UINT32*)(L)-(VX1000_UINT32*)(0)),((VX1000_CHAR*)(B)-(VX1000_CHAR*)(0)),(0),(0)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 0488 */ /* cannot avoid violating MISRA rule 17.4 because pointer arithmetic is the only simple way to cast qualified pointers to integers with only-level-4 violations */
#define VX1000_MAILBOX_READ_VOID(L, B) VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(VX1000_MAILBOX_ERR_EMPTY), ((VX1000_UINT32*)(L)-(VX1000_UINT32*)(0)),((VX1000_CHAR*)(B)-(VX1000_CHAR*)(0)),(0),(0)))/* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 0488 */ /* cannot avoid violating MISRA rule 17.4 because pointer arithmetic is the only simple way to cast qualified pointers to integers with only-level-4 violations */
#define VX1000_MAILBOX_PTR             0UL

#endif /* !VX1000_MAILBOX | VX1000_COMPILED_FOR_SLAVECORES */

#if (defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_PROVIDE_SPLITWRITE)) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
extern VX1000_UINT32 VX1000_SUFFUN(vx1000_MailboxWriteDone)(VX1000_UINT32 len);
#define VX1000_MAILBOX_WRITEDONE(      L)       VX1000_SUFFUN(vx1000_MailboxWriteDone)((L))           /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_MAILBOX_WRITEDONE_VOID( L) (void)VX1000_SUFFUN(vx1000_MailboxWriteDone)((L));          /* usable as a whole statement */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
extern VX1000_UINT32 VX1000_SUFFUN(vx1000_MailboxWriteSplit)(VX1000_UINT32 * * ppBuf);
#define VX1000_MAILBOX_WRITESPLIT(     B)       VX1000_SUFFUN(vx1000_MailboxWriteSplit)((B))          /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_MAILBOX_WRITESPLIT_VOID(B) (void)VX1000_SUFFUN(vx1000_MailboxWriteSplit)((B));         /* usable as a whole statement */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#else /* !VX1000_MAILBOX || !VX1000_MAILBOX_PROVIDE_SPLITWRITE || VX1000_COMPILED_FOR_SLAVECORES */
#define VX1000_MAILBOX_WRITEDONE(      L) VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(VX1000_MAILBOX_ERR_SIZE), (L),(0),(0),(0)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_MAILBOX_WRITEDONE_VOID( L) VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(VX1000_MAILBOX_ERR_SIZE), (L),(0),(0),(0))) /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_MAILBOX_WRITESPLIT(     B) VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(VX1000_MAILBOX_ERR_FULL), ((VX1000_UINT32**)(B)-(VX1000_UINT32**)(0)),(0),(0),(0)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 0488 */ /* cannot avoid violating MISRA rule 17.4 because pointer arithmetic is the only simple way to cast qualified pointers to integers with only-level-4 violations */
#define VX1000_MAILBOX_WRITESPLIT_VOID(B) VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(VX1000_MAILBOX_ERR_FULL), ((VX1000_UINT32**)(B)-(VX1000_UINT32**)(0)),(0),(0),(0))) /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 0488 */ /* cannot avoid violating MISRA rule 17.4 because pointer arithmetic is the only simple way to cast qualified pointers to integers with only-level-4 violations */
#endif /* !VX1000_MAILBOX || !VX1000_MAILBOX_PROVIDE_SPLITWRITE || VX1000_COMPILED_FOR_SLAVECORES */
#if (defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_PROVIDE_SPLITREAD)) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
extern VX1000_UINT32 VX1000_SUFFUN(vx1000_MailboxReadDone)(void);
#define VX1000_MAILBOX_READDONE(     )       VX1000_SUFFUN(vx1000_MailboxReadDone)()                  /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_MAILBOX_READDONE_VOID() (void)VX1000_SUFFUN(vx1000_MailboxReadDone)();                 /* usable as a whole statement */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
extern VX1000_UINT32 VX1000_SUFFUN(vx1000_MailboxReadSplit)(VX1000_UINT32 * pLen, VX1000_UINT32 * * ppBuf);
#define VX1000_MAILBOX_READSPLIT(     L, B)       VX1000_SUFFUN(vx1000_MailboxReadSplit)((L), (B))    /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_MAILBOX_READSPLIT_VOID(L, B) (void)VX1000_SUFFUN(vx1000_MailboxReadSplit)((L), (B));   /* usable as a whole statement */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#else /* !VX1000_MAILBOX || !VX1000_MAILBOX_PROVIDE_SPLITREAD || VX1000_COMPILED_FOR_SLAVECORES */
#define VX1000_MAILBOX_READDONE(     ) (VX1000_MAILBOX_ERR_SPLIT_PEND) /* todo: find a better one */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_MAILBOX_READDONE_VOID() /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_MAILBOX_READSPLIT(     L, B) VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(VX1000_MAILBOX_ERR_EMPTY), ((VX1000_UINT32*)(L)-(VX1000_UINT32*)(0)),((VX1000_UINT32**)(B)-(VX1000_UINT32**)(0)),(0),(0)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 0488 */ /* cannot avoid violating MISRA rule 17.4 because pointer arithmetic is the only simple way to cast qualified pointers to integers with only-level-4 violations */
#define VX1000_MAILBOX_READSPLIT_VOID(L, B) VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(VX1000_MAILBOX_ERR_EMPTY), ((VX1000_UINT32*)(L)-(VX1000_UINT32*)(0)),((VX1000_UINT32**)(B)-(VX1000_UINT32**)(0)),(0),(0)))/* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 0488 */ /* cannot avoid violating MISRA rule 17.4 because pointer arithmetic is the only simple way to cast qualified pointers to integers with only-level-4 violations */
#endif /* !VX1000_MAILBOX || !VX1000_MAILBOX_PROVIDE_SPLITREAD || VX1000_COMPILED_FOR_SLAVECORES */
#if (defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_PRINTF)) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
void VX1000_SUFFUN(vx1000_MailboxPrintf)( const VX1000_CHAR *format, ... ); /* PRQA S 5069 */ /* Cannot avoid violating MISRA rule 16.1 because a C++-based workaround would only violate other rules */
#define VX1000_PRINTF VX1000_SUFFUN(vx1000_MailboxPrintf)
#else  /* !VX1000_MAILBOX || !VX1000_MAILBOX_PRINTF || VX1000_COMPILED_FOR_SLAVECORES */
#define VX1000_PRINTF  (void)
#endif /* !VX1000_MAILBOX || !VX1000_MAILBOX_PRINTF || VX1000_COMPILED_FOR_SLAVECORES */
#if (defined(VX1000_OVERLAY) && (!defined(VX1000_COMPILED_FOR_SLAVECORES)))
#if (defined(VX1000_INIT_CAL_PAGE)) && (!defined(VX1000_OVERLAY_VX_CONFIGURABLE))
#endif /* VX1000_INIT_CAL_PAGE && !VX1000_OVERLAY_VX_CONFIGURABLE */
#if (!defined(VX1000_INIT_CAL_PAGE)) && (!defined(VX1000_OVERLAY_VX_CONFIGURABLE))
#define VX1000_INIT_CAL_PAGE_INTERNAL
#endif /* !VX1000_INIT_CAL_PAGE & !VX1000_OVERLAY_VX_CONFIGURABLE */
#endif /* VX1000_OVERLAY & !VX1000_COMPILED_FOR_SLAVECORES */
#if ((!defined(VX1000_OVERLAY)) || ((defined(VX1000_COMPILED_FOR_SLAVECORES)) || defined(VX1000_OVERLAY_VX_CONFIGURABLE)))
#endif /* !VX1000_OVERLAY | VX1000_COMPILED_FOR_SLAVECORES | VX1000_OVERLAY_VX_CONFIGURABLE */
#if (defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_OVERLAY_CONTROL)) && (!defined(VX1000_COMPILED_FOR_SLAVECORES))
#if !defined(VX1000_GET_CAL_PAGE)
#define VX1000_GET_CAL_PAGE(seg, mod)  VX1000_SUFFUN(vx1000_GetCalPage)((VX1000_UINT8)(seg), (VX1000_UINT8)(mod)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to use reconfigurable API wrappers */
#define VX1000_GET_CAL_PAGE_INTERNAL
#endif /* !VX1000_GET_CAL_PAGE */
#define VX1000_WRP_SET_CAL_PAGE(seg, pag, mod, stup) VX1000_SUFFUN(vx1000_SetCalPage)((VX1000_UINT8)(seg), (VX1000_UINT8)(pag), (VX1000_UINT8)(mod), (VX1000_UINT8)(stup)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to use reconfigurable API wrappers */
#define VX1000_WRP_SET_CAL_PAGE_VOID(seg, pag, mod, stup) (void)VX1000_WRP_SET_CAL_PAGE((seg), (pag), (mod), (stup));  /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#if defined(VX1000_SET_CAL_PAGE)
#define VX1000_SET_CAL_PAGE_EXTERNAL
#else /* !VX1000_SET_CAL_PAGE */
#define VX1000_SET_CAL_PAGE_INTERNAL
#endif /* !VX1000_SET_CAL_PAGE */
#else /* !VX1000_MAILBOX | !VX1000_MAILBOX_OVERLAY_CONTROL | VX1000_COMPILED_FOR_SLAVECORES */
#define VX1000_WRP_SET_CAL_PAGE( seg, pag, mod, stup) VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,(VX1000_CRC_OUT_OF_RANGE), (seg), (pag), (mod), (stup))/* return code for "inactive" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_WRP_SET_CAL_PAGE_VOID(seg, pag, mod, stup) VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,(VX1000_CRC_OUT_OF_RANGE), (seg), (pag), (mod), (stup)))/* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#endif  /* !VX1000_MAILBOX | !VX1000_MAILBOX_OVERLAY_CONTROL | VX1000_COMPILED_FOR_SLAVECORES */
#if (defined(VX1000_MAILBOX) && defined(VX1000_MAILBOX_OVERLAY_CONTROL)) && ((!defined(VX1000_COMPILED_FOR_SLAVECORES)) && (!defined(VX1000_OVERLAY_VX_CONFIGURABLE)))
#if !defined(VX1000_COPY_CAL_PAGE)
#define VX1000_COPY_CAL_PAGE(sseg, spag, dseg, dpag) VX1000_SUFFUN(vx1000_CopyCalPage)((VX1000_UINT8)(sseg), (VX1000_UINT8)(spag), (VX1000_UINT8)(dseg), (VX1000_UINT8)(dpag)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to use reconfigurable API wrappers */
#define VX1000_COPY_CAL_PAGE_INTERNAL
#endif /* !VX1000_COPY_CAL_PAGE */
#endif  /* VX1000_MAILBOX && VX1000_MAILBOX_OVERLAY_CONTROL && !VX1000_COMPILED_FOR_SLAVECORES && !VX1000_OVERLAY_VX_CONFIGURABLE */


/*------------------------------------------------------------------------------ */
/* VX1000 ECU ID                                                                 */

#if !defined(VX1000_ECUID_PTR)
#if defined(VX1000_ECUID) && defined(VX1000_ECUID_LEN)
extern const VX1000_UINT8 gVX1000_ECUID[(VX1000_ECUID_LEN) + 1U];
#define VX1000_ECUID_DATA              const VX1000_UINT8 gVX1000_ECUID[(VX1000_ECUID_LEN) + 1U] = VX1000_ECUID
#define VX1000_ECUID_PTR               (VX1000_PTR2CU8_TO_ADDRESS(&gVX1000_ECUID[0]))
#else /* !VX1000_ECUID || !VX1000_ECUID_LEN */
#define VX1000_ECUID_PTR               0UL
#endif /* !VX1000_ECUID || !VX1000_ECUID_LEN */
#endif /* !VX1000_ECUID_PTR */

#if !defined(VX1000_ECUID_LEN)
#define VX1000_ECUID_LEN               0U
#endif /* !VX1000_ECUID_LEN */


/*------------------------------------------------------------------------------ */
/* OLDA                                                                          */

#if defined(VX1000_OLDA)

#if defined(VX1000_OLDA_FORCE_V7)
#define VX1000_OLDA_VERSION            0x07U /* Olda Version 7 + 0x10 * Plattform code 0x0 (same as MPC56x) */
#define VX1000_OLDA_SIZE_SWAP_VALUE    0x00000000UL     /* dummy value, only used in coldstart verification */
#define VX1000_OLDA_SIZE_LENGTH        0x0              /* dummy value, only used in coldstart verification */
#define VX1000_OLDA_SIZE_OFFSET        0x0              /* dummy value, only used in coldstart verification */
#elif defined(VX1000_OLDA_FORCE_V8)
#define VX1000_OLDA_VERSION            0x08U /* Olda Version 7 + 0x10 * Plattform code 0x0 (same as MPC56x) */
#define VX1000_OLDA_SIZE_SWAP_VALUE    0x00000000UL     /* dummy value, only used in coldstart verification */
#define VX1000_OLDA_SIZE_LENGTH        0x0              /* dummy value, only used in coldstart verification */
#define VX1000_OLDA_SIZE_OFFSET        0x0              /* dummy value, only used in coldstart verification */
#else /* !VX1000_OLDA_FORCE_xx */
#if defined(VX1000_TARGET_XC2000)
#define VX1000_OLDA_VERSION            0x0016U /* Olda Version 6 + 0x10 * Plattform code 0x1 (XC2000) */
#else  /* !VX1000_TARGET_XC2000 */
#define VX1000_OLDA_VERSION            0x06U /* Olda Version 6 + 0x10 * Plattform code 0x0 (same as PowerPC)*/
#endif /* !VX1000_TARGET_XC2000 */
/* As olda_V6 allows overloading of the SWAP, LENGTH and OFFSET defaults via the CFG file, first check if already defined: */

#if defined(VX1000_TARGET_POWERPC)
#if !defined(VX1000_OLDA_SIZE_SWAP_VALUE)
#define VX1000_OLDA_SIZE_SWAP_VALUE    0x00000000UL     /* use olda-v5-like default for backward compatibility */
#endif /* !VX1000_OLDA_SIZE_SWAP_VALUE */
#if !defined(VX1000_OLDA_SIZE_LENGTH)
#define VX1000_OLDA_SIZE_LENGTH        6UL              /* use olda-v5-like default for backward compatibility */
#endif /* !VX1000_OLDA_SIZE_LENGTH */
#if !defined(VX1000_OLDA_SIZE_OFFSET)
#define VX1000_OLDA_SIZE_OFFSET        22UL             /* use olda-v5-like default for backward compatibility */
#endif /* !VX1000_OLDA_SIZE_OFFSET */
#endif /* VX1000_TARGET_POWERPC */
#if defined(VX1000_TARGET_SH2)
#if !defined(VX1000_OLDA_SIZE_SWAP_VALUE)
#define VX1000_OLDA_SIZE_SWAP_VALUE    0x0000000FUL
#endif /* !VX1000_OLDA_SIZE_SWAP_VALUE */
#if !defined(VX1000_OLDA_SIZE_LENGTH)
#define VX1000_OLDA_SIZE_LENGTH        4UL
#endif /* !VX1000_OLDA_SIZE_LENGTH */
#if !defined(VX1000_OLDA_SIZE_OFFSET)
 #define VX1000_OLDA_SIZE_OFFSET       20UL
#endif /* !VX1000_OLDA_SIZE_OFFSET */
#endif /* VX1000_TARGET_SH2 */
#if defined(VX1000_TARGET_TMS570)
#if !defined(VX1000_OLDA_SIZE_SWAP_VALUE)
#define VX1000_OLDA_SIZE_SWAP_VALUE    0x0000000FUL
#endif /* !VX1000_OLDA_SIZE_SWAP_VALUE */
#if !defined(VX1000_OLDA_SIZE_LENGTH)
#define VX1000_OLDA_SIZE_LENGTH        4UL
#endif /* !VX1000_OLDA_SIZE_LENGTH */
#if !defined(VX1000_OLDA_SIZE_OFFSET)
 #define VX1000_OLDA_SIZE_OFFSET       20UL
#endif /* !VX1000_OLDA_SIZE_OFFSET */
#endif /* VX1000_TARGET_TMS570 */
#if defined(VX1000_TARGET_TRICORE)
#if !defined(VX1000_OLDA_SIZE_SWAP_VALUE)
#define VX1000_OLDA_SIZE_SWAP_VALUE    0x00000000UL
#endif /* !VX1000_OLDA_SIZE_SWAP_VALUE */
#if !defined(VX1000_OLDA_SIZE_LENGTH)
#define VX1000_OLDA_SIZE_LENGTH        6UL
#endif /* !VX1000_OLDA_SIZE_LENGTH */
#if !defined(VX1000_OLDA_SIZE_OFFSET)
#define VX1000_OLDA_SIZE_OFFSET        21UL
#endif /* !VX1000_OLDA_SIZE_OFFSET */
#endif /* VX1000_TARGET_TRICORE */
#if defined(VX1000_TARGET_X850)
#if !defined(VX1000_OLDA_SIZE_SWAP_VALUE)
#define VX1000_OLDA_SIZE_SWAP_VALUE    0x0000003bUL     /* all V850 addresses have these bits set: 0x0EC00000 */
#endif /* !VX1000_OLDA_SIZE_SWAP_VALUE */
#if !defined(VX1000_OLDA_SIZE_LENGTH)
#define VX1000_OLDA_SIZE_LENGTH        6UL              /* size/4 is encoded within 6 bit */
#endif /* !VX1000_OLDA_SIZE_LENGTH */
#if !defined(VX1000_OLDA_SIZE_OFFSET)
#define VX1000_OLDA_SIZE_OFFSET        22UL             /* Default address format is with mask 0xF03FFFFF */
#endif /* !VX1000_OLDA_SIZE_OFFSET */
#endif /* VX1000_TARGET_X850 */
#if defined(VX1000_TARGET_XC2000)
#if !defined(VX1000_OLDA_SIZE_SWAP_VALUE)
#define VX1000_OLDA_SIZE_SWAP_VALUE    0x00000000UL
#endif /* !VX1000_OLDA_SIZE_SWAP_VALUE */
#if !defined(VX1000_OLDA_SIZE_LENGTH)
#define VX1000_OLDA_SIZE_LENGTH        6UL
#endif /* !VX1000_OLDA_SIZE_LENGTH */
#if !defined(VX1000_OLDA_SIZE_OFFSET)
#define VX1000_OLDA_SIZE_OFFSET        24UL
#endif /* !VX1000_OLDA_SIZE_OFFSET */
#endif /* VX1000_TARGET_XC2000 */

#if ( ((VX1000_OLDA_SIZE_LENGTH) < 4U) || ((VX1000_OLDA_SIZE_LENGTH) > 7U) )
#error "VX1000_OLDA_SIZE_LENGTH not in allowed range of 4..7!"
#endif /* ! 4 <= VX1000_OLDA_SIZE_LENGTH <= 7 */
#if (((VX1000_OLDA_SIZE_OFFSET) + (VX1000_OLDA_SIZE_LENGTH)) > 32U)
#error "VX1000_OLDA_SIZE_OFFSET + VX1000_OLDA_SIZE_LENGTH exceeds DWORD border!"
#endif /* VX1000_OLDA_SIZE_OFFSET + VX1000_OLDA_SIZE_LENGTH > 32 */
#endif /* !VX1000_OLDA_FORCE_V7 */

#define VX1000_OLDA_MAGIC              0x2603U


/* Generate Olda Size mask and replacement */
#define VX1000_OLDA_SIZE_MASK ( ((1UL << (VX1000_OLDA_SIZE_LENGTH)) - 1UL) << (VX1000_OLDA_SIZE_OFFSET))
#define VX1000_OLDA_SIZE_REPLACEMENT ((VX1000_UINT32) (((VX1000_OLDA_SIZE_SWAP_VALUE) << (VX1000_OLDA_SIZE_OFFSET)) & (VX1000_OLDA_SIZE_MASK)))

#if (((VX1000_OLDA_SIZE_SWAP_VALUE) & (~((1UL << (VX1000_OLDA_SIZE_LENGTH)) - 1UL))) != 0UL) /* PRQA S 3302 */ /* Accepted violation of MISRA rule 12.11 because this formula deals with bitfields and not with numbers */
#error Overlapping data fields inside the olda descriptors (SIZE_SWAP_VALUE vs. SIZE_LENGTH)!
#endif /* VX1000_OLDA_SIZE_SWAP_VALUE , VX1000_OLDA_SIZE_LENGTH */


#if defined(VX1000_TARGET_TRICORE)
/* Default memory size and address */
#if !defined(VX1000_OLDA_MEMORY_SIZE)
#define VX1000_OLDA_MEMORY_SIZE         0UL
#if !defined(VX1000_OLDA_MEMORY_ADDR)
#define VX1000_OLDA_MEMORY_ADDR         0UL
#endif /* !VX1000_OLDA_MEMORY_ADDR */
#else /* !VX1000_OLDA_MEMORY_SIZE */
#if !defined(VX1000_MEMORY_ADDR)
#define VX1000_MEMORY_ADDR              (&gVX1000_OldaMem)
#endif /* !VX1000_MEMORY_ADDR */
#endif /* !VX1000_OLDA_MEMORY_SIZE */
#else  /* !VX1000_TARGET_TRICORE */
/* Default memory size */
#if (!defined(VX1000_OLDA_MEMORY_SIZE)) && (!defined(VX1000_DISABLE_INSTRUMENTATION))
#error VX1000_OLDA_MEMORY_SIZE must be defined in VX1000_cfg.h!
#endif /* !VX1000_OLDA_MEMORY_SIZE */
#endif /* !VX1000_TARGET_TRICORE */

/* Feature flags */
/* using 1st featureFlags field: */
#define VX1000_FEAT_OLDA_TRIGGER_TYPE0   (((0x0U << 15U) | (0x0U << 4U)) | (0x0U << 3U)) /* all targets: 32bits in gVX1000 */
#define VX1000_FEAT_OLDA_TRIGGER_TYPE1   (((0x0U << 15U) | (0x0U << 4U)) | (0x1U << 3U)) /* SHx: 16bits in AUDMBR; PowerPC: 32bits + 32duplicatebits in DTS; TC1xxx: 32bits in TRIGS; reserved on other targets */
#define VX1000_FEAT_OLDA_TRIGGER_TYPE2   (((0x0U << 15U) | (0x1U << 4U)) | (0x0U << 3U)) /* TC24x..TC29x: 32bits in TRIGS; reserved on other targets */
#define VX1000_FEAT_OLDA_TRIGGER_TYPE3   (((0x0U << 15U) | (0x1U << 4U)) | (0x1U << 3U)) /* TC21x..TC23x: 16bits in TRIGS; reserved on other targets */
#define VX1000_FEAT_OLDA_TRIGGER_TYPE4   (((0x1U << 15U) | (0x0U << 4U)) | (0x0U << 3U)) /* all targets: 32bits + 32inversebits in gVX1000 */
#define VX1000_FEAT_OLDA_TRIGGER_TYPE5   (((0x1U << 15U) | (0x0U << 4U)) | (0x1U << 3U)) /* all targets (except Tricore): event signalling via trace */
#define VX1000_FEAT_OLDA_TRIGGER_TYPE6   (((0x1U << 15U) | (0x1U << 4U)) | (0x0U << 3U)) /* reserved */
#define VX1000_FEAT_OLDA_TRIGGER_TYPE7   (((0x1U << 15U) | (0x1U << 4U)) | (0x1U << 3U)) /* reserved */
#if defined(VX1000_OLDA_AUDMBR_REG_ADDR) || defined(VX1000_OLDA_DTS_BASE_ADDR)
#define VX1000_FEAT_OLDA_TRIGGER        (VX1000_FEAT_OLDA_TRIGGER_TYPE1)
#else  /* classic olda event trigger with inverse */
#if defined(VX1000_SUPPRESS_TRACE_SUPPORT)
#define VX1000_FEAT_OLDA_TRIGGER        (VX1000_FEAT_OLDA_TRIGGER_TYPE4)
#else /* !VX1000_SUPPRESS_TRACE_SUPPORT */
#define VX1000_FEAT_OLDA_TRIGGER        (VX1000_FEAT_OLDA_TRIGGER_TYPE5)
#endif /* !VX1000_SUPPRESS_TRACE_SUPPORT */
#endif /* classic olda event trigger with inverse */
/* note: bit 15 already used for TRIGGER_TYPE */
#if defined(VX1000_OLDA_BENCHMARK)
#define VX1000_FEAT_OLDA_BENCHMARK      (1U << 14U)
#else  /* !VX1000_OLDA_BENCHMARK */
#define VX1000_FEAT_OLDA_BENCHMARK      (0U << 14U)
#endif /* !VX1000_OLDA_BENCHMARK */
#define VX1000_FEAT_OLDA_UNUSED_FLAG13  (0U << 13U) /* still free for future use */
#define VX1000_FEAT_OLDA_UNUSED_FLAG12  (0U << 12U) /* still free for future use */
#define VX1000_FEAT_OLDA_UNUSED_FLAG11  (0U << 11U) /* still free for future use */
#define VX1000_FEAT_OLDA_512EVENTS      (1U << 10U)
#if defined(VX1000_SUPPORT_OLDA_MULTIBUFFER)
#define VX1000_FEAT_OLDA_MULTIBUFFER    (1U << 9U)
#else  /* !VX1000_SUPPORT_OLDA_MULTIBUFFER */
#define VX1000_FEAT_OLDA_MULTIBUFFER    (0U << 9U)
#endif /* !VX1000_SUPPORT_OLDA_MULTIBUFFER */
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUBOOKE) && (defined(VX1000_OLDA_FORCE_V7) || (defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8)))
#define VX1000_FEAT_OLDA_V7_ASMDAQBOOKE (1U << 8U)
#else  /* !VX1000_SUPPORT_OLDA7_ASMGNUBOOKE || !VX1000_OLDA_FORCE_V7 */
#define VX1000_FEAT_OLDA_V7_ASMDAQBOOKE (0U << 8U)
#endif /* !VX1000_SUPPORT_OLDA7_ASMGNUBOOKE || !VX1000_OLDA_FORCE_V7 */
#if defined(VX1000_SUPPORT_OLDA7_ASMGNUVLE) && (defined(VX1000_OLDA_FORCE_V7) || (defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8)))
#define VX1000_FEAT_OLDA_V7_ASMDAQVLE   (1U << 7U)
#else  /* !VX1000_SUPPORT_OLDA7_ASMGNUVLE || !VX1000_OLDA_FORCE_V7 */
#define VX1000_FEAT_OLDA_V7_ASMDAQVLE   (0U << 7U)
#endif /* !VX1000_SUPPORT_OLDA7_ASMGNUVLE || !VX1000_OLDA_FORCE_V7 */
#if (defined(VX1000_SUPPORT_OLDA7_BYTEDAQ) || ((VX1000_MEMSYNC_TRIGGER_COUNT > 0))) && (defined(VX1000_OLDA_FORCE_V7) || (defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8)))
#define VX1000_FEAT_OLDA_V7_BYTEDAQ     (1U << 6U)
#define VX1000_FEAT_OLDA_V7_MEMSY_SEL   (1U << 6U)
#else  /* !(VX1000_SUPPORT_OLDA7_BYTEDAQ || VX1000_MEMSYNC_TRIGGER_COUNT) || !VX1000_OLDA_FORCE_V7 */
#define VX1000_FEAT_OLDA_V7_BYTEDAQ     (0U << 6U)
#define VX1000_FEAT_OLDA_V7_MEMSY_SEL   (0U << 6U)
#endif /* !(VX1000_SUPPORT_OLDA7_BYTEDAQ || VX1000_MEMSYNC_TRIGGER_COUNT) || !VX1000_OLDA_FORCE_V7 */
#if defined(VX1000_HOOK_BASED_BYPASSING)
#define VX1000_FEAT_OLDA_HBB            (1U << 5U)
#else  /* !VX1000_HOOK_BASED_BYPASSING */
#define VX1000_FEAT_OLDA_HBB            (0U << 5U)
#endif /* !VX1000_HOOK_BASED_BYPASSING */
/* note: bits 3,4 already used for TRIGGER_TYPE */
#if defined(VX1000_STIM_BY_OLDA)
#define VX1000_FEAT_OLDA_STIM           (1U << 2U)
#else  /* !VX1000_STIM_BY_OLDA */
#define VX1000_FEAT_OLDA_STIM           (0U << 2U)
#endif /* !VX1000_STIM_BY_OLDA */
#define VX1000_FEAT_OLDA_V5_DYNSIZE     (0U << 1U) /* V5 no longer used */
#if defined(VX1000_SUPPORT_OLDA7_COMPRESS)  && (defined(VX1000_OLDA_FORCE_V7) || (defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8)))
#define VX1000_FEAT_OLDA_V7_COMPRESS    (1U << 1U)
#else  /* !VX1000_SUPPORT_OLDA7_COMPRESS  || !VX1000_OLDA_FORCE_V7 */
#define VX1000_FEAT_OLDA_V7_COMPRESS    (0U << 1U)
#endif /* !VX1000_SUPPORT_OLDA7_COMPRESS || !VX1000_OLDA_FORCE_V7 */
#if defined(VX1000_OLDA_OVERLOAD_DETECTION)
#define VX1000_FEAT_OLDA_OVERLOADDETECT (1U << 0U)
#else  /* !VX1000_OLDA_OVERLOAD_DETECTION */
#define VX1000_FEAT_OLDA_OVERLOADDETECT (0U << 0U)
#endif /* !VX1000_OLDA_OVERLOAD_DETECTION */
/* using featureFlags2 field: */
#if defined(VX1000_MEMCPY)&& defined(VX1000_OLDA_FORCE_V8)
#define VX1000_FEAT_OLDA_V8CMD_USRCPY   (1U << 14U)
#else  /* !VX1000_MEMCPY  || !VX1000_OLDA_FORCE_V8 */
#define VX1000_FEAT_OLDA_V8CMD_USRCPY   (0U << 14U)
#endif /* !VX1000_MEMCPY  || !VX1000_OLDA_FORCE_V8 */
#if defined(VX1000_SUPPORT_OLDA8CMD_SP8N)  && defined(VX1000_OLDA_FORCE_V8)
#define VX1000_FEAT_OLDA_V8CMD_SP8N     (1U << 13U)
#else  /* !VX1000_SUPPORT_OLDA8CMD_SP8N    || !VX1000_OLDA_FORCE_V8 */
#define VX1000_FEAT_OLDA_V8CMD_SP8N     (0U << 13U)
#endif /* !VX1000_SUPPORT_OLDA8CMD_SP8N   || !VX1000_OLDA_FORCE_V8 */
#if defined(VX1000_SUPPORT_OLDA8CMD_CP8N)  && defined(VX1000_OLDA_FORCE_V8)
#define VX1000_FEAT_OLDA_V8CMD_CP8N     (1U << 12U)
#else  /* !VX1000_SUPPORT_OLDA8CMD_CP8N    || !VX1000_OLDA_FORCE_V8 */
#define VX1000_FEAT_OLDA_V8CMD_CP8N     (0U << 12U)
#endif /* !VX1000_SUPPORT_OLDA8CMD_CP8N   || !VX1000_OLDA_FORCE_V8 */
#if defined(VX1000_SUPPORT_OLDA8CMD_CP16N) && defined(VX1000_OLDA_FORCE_V8)
#define VX1000_FEAT_OLDA_V8CMD_CP16N    (1U << 11U)
#else  /* !VX1000_SUPPORT_OLDA8CMD_CP16N   || !VX1000_OLDA_FORCE_V8 */
#define VX1000_FEAT_OLDA_V8CMD_CP16N    (0U << 11U)
#endif /* !VX1000_SUPPORT_OLDA8CMD_CP16N  || !VX1000_OLDA_FORCE_V8 */
#if defined(VX1000_SUPPORT_OLDA8CMD_CP32N) && defined(VX1000_OLDA_FORCE_V8)
#define VX1000_FEAT_OLDA_V8CMD_CP32N    (1U << 10U)
#else  /* !VX1000_SUPPORT_OLDA8CMD_CP32N   || !VX1000_OLDA_FORCE_V8 */
#define VX1000_FEAT_OLDA_V8CMD_CP32N    (0U << 10U)
#endif /* !VX1000_SUPPORT_OLDA8CMD_CP32N  || !VX1000_OLDA_FORCE_V8 */
#if defined(VX1000_SUPPORT_OLDA8CMD_CP64N) && defined(VX1000_OLDA_FORCE_V8)
#define VX1000_FEAT_OLDA_V8CMD_CP64N    (1U << 9U)
#else  /* !VX1000_SUPPORT_OLDA8CMD_CP64N   || !VX1000_OLDA_FORCE_V8 */
#define VX1000_FEAT_OLDA_V8CMD_CP64N    (0U << 9U)
#endif /* !VX1000_SUPPORT_OLDA8CMD_CP64N  || !VX1000_OLDA_FORCE_V8 */
#if defined(VX1000_SUPPORT_OLDA8CMD_LEGACYVR) && defined(VX1000_OLDA_FORCE_V8)
#define VX1000_FEAT_OLDA_V8CMD_LEGACYVR (1U << 8U)
#else  /* !VX1000_SUPPORT_OLDA8CMD_LEGACYVR || !VX1000_OLDA_FORCE_V8 */
#define VX1000_FEAT_OLDA_V8CMD_LEGACYVR (0U << 8U)
#endif /* !VX1000_SUPPORT_OLDA8CMD_LEGACYVR || !VX1000_OLDA_FORCE_V8 */
#if defined(VX1000_SUPPORT_OLDA8CMD_WAIT)  && defined(VX1000_OLDA_FORCE_V8)
#define VX1000_FEAT_OLDA_V8CMD_WAIT     (1U << 7U)
#else  /* !VX1000_SUPPORT_OLDA8CMD_WAIT    || !VX1000_OLDA_FORCE_V8 */
#define VX1000_FEAT_OLDA_V8CMD_WAIT     (0U << 7U)
#endif /* !VX1000_SUPPORT_OLDA8CMD_WAIT   || !VX1000_OLDA_FORCE_V8 */
#if defined(VX1000_SUPPORT_OLDA8CMD_SUBEVT)&& defined(VX1000_OLDA_FORCE_V8)
#define VX1000_FEAT_OLDA_V8CMD_SUBEVT   (1U << 6U)
#else  /* !VX1000_SUPPORT_OLDA8CMD_SUBEVT  || !VX1000_OLDA_FORCE_V8 */
#define VX1000_FEAT_OLDA_V8CMD_SUBEVT   (0U << 6U)
#endif /* !VX1000_SUPPORT_OLDA8CMD_SUBEVT || !VX1000_OLDA_FORCE_V8 */
#if defined(VX1000_SUPPORT_OLDA8CMD_CALLJITA)  && defined(VX1000_OLDA_FORCE_V8)
#define VX1000_FEAT_OLDA_V8CMD_CALLJITA (1U << 1U)
#else  /* !VX1000_SUPPORT_OLDA8CMD_CALLJITA  || !VX1000_OLDA_FORCE_V8 */
#define VX1000_FEAT_OLDA_V8CMD_CALLJITA (0U << 1U)
#endif /* !VX1000_SUPPORT_OLDA8CMD_CALLJITA || !VX1000_OLDA_FORCE_V8 */

#if defined(VX1000_TARGET_X850)

/* advice the compiler to pack the data of the following structures byte-wise ==> todo: shall be moved to userconfig (VX1000_BEGSECT_VXMODULE_H) */
#pragma pack(1) /* syntax valid for both GCC and for GHS as well as for Renesas-CX; may need adaptation for other compilers */

#endif /* VX1000_TARGET_X850 */
typedef struct
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   EventCounter;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   EventTimestamp;  /* NB: for OLDA stimulation, this field is used to store the address of the stimulation buffer */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   TransferDest;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16   TransferIndex;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16   TransferCount;   /* NB: for OLDAv8, this field is used to define the aligned size in bytes of the event's olda buffer */
} VX1000_OLDA_EVENT_T;

typedef struct
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16        MagicId;              /* 0x00 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8         Version;              /* 0x02 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8         Running;              /* 0x03 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32        MemoryAddr;           /* 0x04 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16        MemorySize;           /* 0x08 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16        EventCount;           /* 0x0A */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32        EventList;            /* 0x0C */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32        TransferList;         /* 0x10 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8         SizeLengthNOffset;    /* 0x14 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8         SizeSwapValue;        /* 0x15 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16        OldaFeatures;         /* 0x16 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16        OldaFeatures2;        /* 0x18 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16        OldaBenchmarkCnt;     /* 0x1A */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32        Res2;                 /* 0x1C */
#if !defined(VX1000_OLDA_MEMORY_ADDR)
#if defined(VX1000_TARGET_XC2000)
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16        Data[(VX1000_OLDA_MEMORY_SIZE) / 2UL]; /* TODO: align on 32bit-boundaries */
#else  /* !VX1000_TARGET_XC2000 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32        Data[(VX1000_OLDA_MEMORY_SIZE) / 4UL];
#endif /* !VX1000_TARGET_XC2000 */
#endif /* !VX1000_OLDA_MEMORY_ADDR */
} VX1000_OLDA_T;

#if defined(VX1000_TARGET_X850)

/* restore previous padding strategy for the application ==> todo: shall be moved to user-config (VX1000_ENDSECT_VXMODULE_H) */
#if defined(__CX__)
/* Renesas CX compiler syntax */
#pragma pack(4)
#else /* !__CX__ */
/* GHS compiler syntax */
#pragma pack()
#endif /* !__CX__ */

#endif /* VX1000_TARGET_X850 */

#if defined(VX1000_OLDA_FORCE_V8)
extern void VX1000_SUFFUN(vx1000_OldaEvent)( VX1000_UINT16 eventNumber, VX1000_UINT32 extendedParameter );
#define VX1000_OLDA_EVENT(x)           VX1000_SUFFUN(vx1000_OldaEvent)((1+(x)), 0); /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#define VX1000_OLDA_EVENT_COUNT2       ((VX1000_OLDA_EVENT_COUNT) + 1)
#define VX1000_OLDA_BENCHMARK_CNT2     ((VX1000_OLDA_BENCHMARK_CNT) + 1)
#else /* !VX1000_OLDA_FORCE_V8 */
extern void VX1000_SUFFUN(vx1000_OldaEvent)( VX1000_UINT16 eventNumber );
#define VX1000_OLDA_EVENT(x)           VX1000_SUFFUN(vx1000_OldaEvent)((x)); /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#define VX1000_OLDA_EVENT_COUNT2       (VX1000_OLDA_EVENT_COUNT)
#define VX1000_OLDA_BENCHMARK_CNT2     (VX1000_OLDA_BENCHMARK_CNT)
#endif /* !VX1000_OLDA_FORCE_V8 */
#define VX1000_OLDA_DATA               VX1000_OLDA_T Olda
#define VX1000_OLDA_PTR                (VX1000_PTR2VO_TO_ADDRESS(&gVX1000.Olda))
#if ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0) && (!defined(VX1000_OLDA_FORCE_V8))
extern void VX1000_SUFFUN(vx1000_OldaSpecialEvent)( VX1000_UINT32 eventNumber );
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT & !VX1000_OLDA_FORCE_V8 */

#if defined(VX1000_OLDA_BENCHMARK)
extern VX1000_UINT32 gVX1000_OLDA_Duration[VX1000_OLDA_BENCHMARK_CNT2];
extern VX1000_UINT32 gVX1000_OLDA_TransferSize[VX1000_OLDA_BENCHMARK_CNT2];
extern VX1000_UINT32 gVX1000_OLDA_TransferCount[VX1000_OLDA_BENCHMARK_CNT2];
#define VX1000_OLDA_BENCHMARK_DATA     VX1000_UINT32 gVX1000_OLDA_Duration[VX1000_OLDA_BENCHMARK_CNT2], gVX1000_OLDA_TransferSize[VX1000_OLDA_BENCHMARK_CNT2], gVX1000_OLDA_TransferCount[VX1000_OLDA_BENCHMARK_CNT2]
#define VX1000_OLDA_DURARRAY_PTR       (VX1000_PTR2U32_TO_ADDRESS(gVX1000_OLDA_Duration))
#else /* !VX1000_OLDA_BENCHMARK */
#define VX1000_OLDA_DURARRAY_PTR       0UL
#endif /* VX1000_OLDA_BENCHMARK */

#define VX1000_STRUCT_OFSADDR(OFS)     ((OFS) + VX1000_PTR2VVX_TO_ADDRESS(&gVX1000)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compiletime and thus cannot be turned into a function */
#define VX1000_O8DESC_DONE             0x00000005UL /* without colon beause it is always the last entry in the array */
#define VX1000_O8DLEN_DONE             1
#if (!defined(VX1000_CLOCK)) || (!defined(VX1000_CLOCK_TICKS_PER_BASE))
#define VX1000_O8DESC_TIMESTAMP_EV     /* empty */
#define VX1000_O8DLEN_TIMESTAMP_EV     0
#define VX1000_O8DESC_TIMESTAMP_NOW    /* empty */
#define VX1000_O8DLEN_TIMESTAMP_NOW    0
#endif /* !VX1000_CLOCK || !VX1000_CLOCK_TICKS_PER_BASE */
#if (VX1000_MEMSYNC_TRIGGER_COUNT > 0)
#define VX1000_O8DESC_TRIGSPCEV        0x00970108UL, VX1000_STRUCT_OFSADDR(0x0UL),
#define VX1000_O8DLEN_TRIGSPCEV        2
#define VX1000_O8DESC_TRIGDAQEV(IDX)   0x00960108UL, VX1000_STRUCT_OFSADDR(0x0UL), (IDX), /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compiletime and thus cannot be turned into a function */
#define VX1000_O8DLEN_TRIGDAQEV        3
#if defined(VX1000_CLOCK) && defined(VX1000_CLOCK_TICKS_PER_BASE)
#define VX1000_O8DESC_TIMESTAMP_EV     0x00980108UL, VX1000_STRUCT_OFSADDR(0x4UL),
#define VX1000_O8DLEN_TIMESTAMP_EV     2
#define VX1000_O8DESC_TIMESTAMP_NOW    0x00990108UL, VX1000_STRUCT_OFSADDR(0x4UL),
#define VX1000_O8DLEN_TIMESTAMP_NOW    2
#endif /* VX1000_CLOCK && VX1000_CLOCK_TICKS_PER_BASE */
#else  /* !VX1000_MEMSYNC_TRIGGER_COUNT */
#define VX1000_O8DESC_TRIGSPCEV        0x00900108UL, VX1000_STRUCT_OFSADDR(0x0UL),
#define VX1000_O8DLEN_TRIGSPCEV        2
#define VX1000_O8DESC_TRIGDAQEV(IDX)   0x00800108UL, VX1000_STRUCT_OFSADDR(0x0UL), (IDX), /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 because this expression is evaluated at compiletime and thus cannot be turned into a function */
#define VX1000_O8DLEN_TRIGDAQEV        3
#if defined(VX1000_CLOCK) && defined(VX1000_CLOCK_TICKS_PER_BASE)
#define VX1000_O8DESC_TIMESTAMP_EV     0x00910108UL, VX1000_STRUCT_OFSADDR(0x4UL),
#define VX1000_O8DLEN_TIMESTAMP_EV     2
#define VX1000_O8DESC_TIMESTAMP_NOW    0x009A0108UL, VX1000_STRUCT_OFSADDR(0x4UL),
#define VX1000_O8DLEN_TIMESTAMP_NOW    2
#endif /* VX1000_CLOCK && VX1000_CLOCK_TICKS_PER_BASE */
#endif /* !VX1000_MEMSYNC_TRIGGER_COUNT */

#else /* !VX1000_OLDA */

#define VX1000_OLDA_EVENT(x)           /*VX1000_DUMMYREAD((x))*/
#define VX1000_OLDA_PTR                0UL
#define VX1000_OLDA_DURARRAY_PTR       0UL

#endif /* !VX1000_OLDA */


/*------------------------------------------------------------------------------ */
/* Driver Error Logger                                                           */

#define VX1000_ERRLOG_NO_ERROR         0x0000U /* no error at all */

#define VX1000_ERRLOG_STRUCTS_PADDED   0x0101U /* due to bad compiler settings, the VX1000 structures were padded (but they must be packed) */
#define VX1000_ERRLOG_JTAGID_UNKNOWN   0x0102U /* the driver found a derivative that it does not know how to handle (e.g. support was de-configured to reduce code size) */
#define VX1000_ERRLOG_TYPESIZE_INVAL   0x0103U /* due to bad compiler settings or wrong user configuration the VX1000_xxxINTxx typedefs result in wrong byte size */
#define VX1000_ERRLOG_PTR_NONREPSTBL   0x0104U /* the driver received a linear pointer from the tool that has no valid representation in the used compiler memory model */
#define VX1000_ERRLOG_TOO_MANY_CORES   0x0105U /* VX1000_CURRENT_CORE_IDX() returned a core index that exceeds the configured maximum */

#define VX1000_ERRLOG_TM_RESO_TOO_LOW  0x0201U /* the resolution of the timer is too low for some features */
#define VX1000_ERRLOG_TM_RESO_TOO_HIGH 0x0202U /* the resolution of the timer is too high for some features */
#define VX1000_ERRLOG_TM_DT_IS_ZERO    0x0203U /* due to a low resolution, the conversion from a non-zero time resulted in a tick count of zero */
#define VX1000_ERRLOG_TM_DTDT_TOO_LONG 0x0204U /* the user requested a VX1000_DETECT() timeout that cannot be represented within the available bit slice */
#define VX1000_ERRLOG_TM_DTCS_TOO_LONG 0x0205U /* the user requested a VX1000_INIT() timeout that cannot be represented within the available bit slice */
#define VX1000_ERRLOG_TM_DTST_TOO_LONG 0x0206U /* the user called the STIM API with a timeout that cannot be represented within the available bit slice */
#define VX1000_ERRLOG_TM_DTSR_TOO_LONG 0x0207U /* the user requested a VX1000_PREPARE_SOFTRESET() timeout that cannot be represented within the available bit slice */

#define VX1000_ERRLOG_OLDA_UNIMPLCMD   0x0301U /* the olda descriptors contain an unimplemented command */
#define VX1000_ERRLOG_OLDA_UNIMPLSUB   0x0302U /* the olda descriptors contain an unimplemented subcommand */
#define VX1000_ERRLOG_OLDA_BUFINVAL    0x0303U /* the olda buffer is linked to an invalid address / not linked at all */
#define VX1000_ERRLOG_OLDA_BUFSMALL    0x0304U /* the olda buffer was configured too small */

#define VX1000_ERRLOG_OVL_INVALID_PAGE 0x0401U /* the XCP master requested a page number > 1 which is not supported by the driver */
#define VX1000_ERRLOG_OVL_INVALID_SIZE 0x0402U /* the user specified an overlay size that is not supported by the hardware/driver */
#define VX1000_ERRLOG_OVL_MISALIGNED   0x0403U /* the user specified an overlay alignment that is not supported by the hardware/driver */
#define VX1000_ERRLOG_OVL_TOO_MANY     0x0404U /* the XCP master requested a segment number > 1 which is not supported by the driver */
#define VX1000_ERRLOG_OVL_USER_FAILED  0x0405U /* a user's overlay callback returned a value != 0 */
#define VX1000_ERRLOG_OVL_UNIMPL       0x0406U /* code was triggered that is not implemented for the derivative it is executed on */
#define VX1000_ERRLOG_OVL_NONEXCLUSIVE 0x0407U /* the AppDriver was triggered to handle overlays but not all descriptors were assigned to his control */

/* reserved range for DAS:             0x05xxU */

#if (VX1000_ERRLOG_SIZE != 0)
extern void VX1000_SUFFUN(vx1000_ErrLogger)( VX1000_UINT16 errorcode );
#define VX1000_ERRLOGGER(E)            VX1000_SUFFUN(vx1000_ErrLogger)(E); /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#define VX1000_ERRLOG_DATA             VX1000_UINT16 ErrLog[VX1000_ERRLOG_SIZE]
#define VX1000_ERRLOG_ADDR             (VX1000_PTR2VU16_TO_ADDRESS(gVX1000.ErrLog))
#else /* !VX1000_ERRLOG_SIZE */
#define VX1000_ERRLOGGER(E)            /* empty */
#define VX1000_ERRLOG_ADDR             0UL
#endif /* !VX1000_ERRLOG_SIZE */


/*------------------------------------------------------------------------------ */
/* VX1000 Stimulation                                                            */

/* STIM event API return codes */
#define VX1000_STIM_RET_INACTIVE       0
#define VX1000_STIM_RET_SUCCESS        1
#define VX1000_STIM_RET_TIMEOUT        2
#define VX1000_STIM_RET_ERROR          3

#if defined(VX1000_STIM)

#define VX1000_STIM_MAGIC              0xFEC70A07UL
#define VX1000_STIM_EVENT_MAGIC        0xFEC70A19UL

#if !defined(VX1000_STIM_FORCE_V1)
typedef struct
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 MagicId;           /* 0x00 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 Version;           /* 0x04 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 Control;           /* 0x06 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8  EvtOffset;         /* 0x08 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8  EvtNumber;         /* 0x09 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 Padding1;          /* 0x0A */
  struct VX1000_stim_event                                     /* 0x0C */
  {
    VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8 Ctr;              /* 0x0C + 4*e */
    VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8 RqCtr;            /* 0x0D + 4*e */
    VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8 Enable;           /* 0x0E + 4*e */
    VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8 Copying;          /* 0x0F + 4*e */
  } Event[VX1000_STIM_EVENT_COUNT];
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 Enable;            /* 0x0C + 4*VX1000_STIM_EVENT_COUNT */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 TimeoutCtr;        /* 0x0E + 4*VX1000_STIM_EVENT_COUNT */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 TimeoutCtr2;       /* 0x10 + 4*VX1000_STIM_EVENT_COUNT */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 Padding2;          /* 0x12 + 4*VX1000_STIM_EVENT_COUNT */
} VX1000_STIM_T;
#define VX1000_STIMEVENT_ARRAYNAME gVX1000.Stim.Event
#else /* VX1000_STIM_FORCE_V1 */
typedef struct
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 MagicId;           /* 0x20 + 0x00 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 Version;           /* 0x20 + 0x04 */
  struct VX1000_stim_event                                     /* 0x20 + 0x08 */
  {
    VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8 EventType;        /* 0x20 + 0x08 + 8*e */
    VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8 DaqEvent;         /* 0x20 + 0x09 + 8*e */
    VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8 Enable;           /* 0x20 + 0x0A + 8*e */
    VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8 Copying;          /* 0x20 + 0x0B + 8*e */
    VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8 Ctr;              /* 0x20 + 0x0C + 8*e */
    VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8 RqCtr;            /* 0x20 + 0x0D + 8*e */
    VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 Reserved;        /* 0x20 + 0x0E + 8*e */
  } Event[VX1000_STIM_EVENT_COUNT];
} VX1000_STIM_EVENT_T;

typedef struct VX1000_hbb_table
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8 StimEvent;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8 Reserved;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 OldaAddress;
} VX1000_HBB_VX_TABLE_ENTRY_T;

typedef struct
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 MagicId;           /* 0x00 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 Version;           /* 0x04 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 Control;           /* 0x06 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8 EvtOffset;          /* 0x08 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8 EvtNumber;          /* 0x09 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 Enable;            /* 0x0a */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 EventPointer;      /* 0x0c */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 TimeoutCtr;        /* 0x10 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 TimeoutCtr2;       /* 0x12 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 hbbLUTNumber;      /* 0x14 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 hbbLUTPointer;     /* 0x18 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 hbbLUTVXPointer;   /* 0x1c */
  VX1000_STIM_EVENT_T Events;                                  /* 0x20 */ /* attention: no reserved field for future extensions! */
} VX1000_STIM_T;
#define VX1000_STIMEVENT_ARRAYNAME gVX1000.Stim.Events.Event
#endif /* VX1000_STIM_FORCE_V1 */

/* STIM event enable flag     */
#define VX1000_STIM_INACTIVE         0
#define VX1000_STIM_VX_ENABLE        1
#define VX1000_STIM_DAQ_SENT         2
#define VX1000_STIM_BUFFER_VALID     3
#define VX1000_STIM_BUFFER_INVALID   4

/* STIM event global enable   */
#define VX1000_STIM_GLOBAL_INACTIVE  0
#define VX1000_STIM_GLOBAL_VX_ENABLE 1
#define VX1000_STIM_GLOBAL_ALL_CHAN  2

/* STIM event types           */
#define VX1000_BYPASS_TYPE_DIRECT    1
#define VX1000_BYPASS_TYPE_OLDA      2
#define VX1000_BYPASS_TYPE_HOOK      3

/* Copy Active Flag meanings - used for direct stimulation  */
#define VX1000_BP_CPACT_WAITING_DAQ  0
#define VX1000_BP_CPACT_WAITING_STIM 1
#define VX1000_BP_CPACT_DATA_READY   2
#define VX1000_BP_CPACT_COPYING      3

#define VX1000_BP_TIMESTAMP_SIZE     4


/* stim API */
#define VX1000_STIM_DATA               VX1000_STIM_T Stim
#define VX1000_STIM_PTR                (VX1000_PTR2VS_TO_ADDRESS(&gVX1000.Stim))
extern VX1000_UINT8 VX1000_SUFFUN(vx1000_StimWait)( VX1000_UINT8 stim_event, VX1000_UINT8 copy_enable, VX1000_UINT32 timeout_us );
#define VX1000_STIM_WAIT(     E, T)          VX1000_SUFFUN(vx1000_StimWait)((E), 1, (T) )  /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_STIM_WAIT_VOID(E, T)    (void)VX1000_SUFFUN(vx1000_StimWait)((E), 1, (T) ); /* usable as a whole statement */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#if defined(VX1000_STIM_BY_OLDA)
extern VX1000_UINT8 VX1000_SUFFUN(vx1000_Stimulate)( VX1000_UINT8 stim_trigger_event, VX1000_UINT8 stim_event,
                                                     VX1000_UINT8 cycle_delay, VX1000_UINT32 timeout_us);
#define VX1000_STIMULATE(     D, S, P, T)       VX1000_SUFFUN(vx1000_Stimulate)((D), (S), (P), (T))  /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_STIMULATE_VOID(D, S, P, T) (void)VX1000_SUFFUN(vx1000_Stimulate)((D), (S), (P), (T)); /* usable as a whole statement */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#else  /* !VX1000_STIM_BY_OLDA */
#define VX1000_STIMULATE(     D, S, P, T) VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (D),(S),(P),(T))   /* return code for "inactive" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_STIMULATE_VOID(D, S, P, T) VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (D),(S),(P),(T)))      /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#endif /* !VX1000_STIM_BY_OLDA */
extern void VX1000_SUFFUN(vx1000_StimControl)( void );
#define VX1000_STIM_CONTROL()          VX1000_SUFFUN(vx1000_StimControl)(); /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_CONTROL()        VX1000_SUFFUN(vx1000_StimControl)(); /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
extern VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassTrigger)( VX1000_UINT8 daq_event, VX1000_UINT8 stim_event );
#define VX1000_BYPASS_TRIGGER(     D, S)       VX1000_SUFFUN(vx1000_BypassTrigger)((D), (S))  /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_TRIGGER_VOID(D, S) (void)VX1000_SUFFUN(vx1000_BypassTrigger)((D), (S)); /* usable as a whole statement */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
extern VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassDaq) ( VX1000_UINT8 daq_event, VX1000_UINT8 stim_event );
#define VX1000_BYPASS_DAQ(     D, S)         VX1000_SUFFUN(vx1000_BypassDaq) ((D), (S))       /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_DAQ_VOID(D, S)   (void)VX1000_SUFFUN(vx1000_BypassDaq) ((D), (S));      /* usable as a whole statement */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_STIM(     S )          VX1000_SUFFUN(vx1000_StimWait)((S), 1, 0 )       /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_STIM_VOID(S)     (void)VX1000_SUFFUN(vx1000_StimWait)((S), 1, 0 );      /* usable as a whole statement */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
extern VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassWait)( VX1000_UINT8 stim_event, VX1000_UINT32 timeout_us);
#define VX1000_BYPASS_WAIT(     S, T)        VX1000_SUFFUN(vx1000_BypassWait)((S), (T))       /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_WAIT_VOID(S, T)  (void)VX1000_SUFFUN(vx1000_BypassWait)((S), (T));      /* usable as a whole statement */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
extern VX1000_UINT8 VX1000_SUFFUN(vx1000_Bypass)( VX1000_UINT8 daq_event, VX1000_UINT8 stim_event, VX1000_UINT32 timeout_us );
#define VX1000_BYPASS(      D, S, T)         VX1000_SUFFUN(vx1000_Bypass)( (D), (S), (T))     /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_VOID( D, S, T)   (void)VX1000_SUFFUN(vx1000_Bypass)( (D), (S), (T));    /* usable as a whole statement */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#if (0==(VX1000_STIM_EVENT_OFFSET))
#define VX1000_REGISTER_STIM_EVENT(daq_event, stim_event, stim_type) do                                \
  {                                                                                                    \
    if ((stim_event) < ((VX1000_STIM_EVENT_OFFSET) + (VX1000_STIM_EVENT_COUNT)))                       \
    {                                                                                                  \
      (VX1000_STIMEVENT_ARRAYNAME)[(stim_event) - (VX1000_STIM_EVENT_OFFSET)].DaqEvent = (daq_event);  \
      (VX1000_STIMEVENT_ARRAYNAME)[(stim_event) - (VX1000_STIM_EVENT_OFFSET)].EventType = (stim_type); \
    }                                                                                                  \
  } while(0); /* this never-looping while is here only for MISRA */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#else /* VX1000_STIM_EVENT_OFFSET */
#define VX1000_REGISTER_STIM_EVENT(daq_event, stim_event, stim_type) do                                                          \
  {                                                                                                                              \
    if (((stim_event) < ((VX1000_STIM_EVENT_OFFSET) + (VX1000_STIM_EVENT_COUNT))) && ((stim_event) >= VX1000_STIM_EVENT_OFFSET)) \
    {                                                                                                                            \
      (VX1000_STIMEVENT_ARRAYNAME)[(stim_event) - (VX1000_STIM_EVENT_OFFSET)].DaqEvent = (daq_event);                            \
      (VX1000_STIMEVENT_ARRAYNAME)[(stim_event) - (VX1000_STIM_EVENT_OFFSET)].EventType = (stim_type);                           \
    }                                                                                                                            \
  } while(0); /* this never-looping while is here only for MISRA */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#endif /* VX1000_STIM_EVENT_OFFSET */
#if defined(VX1000_STIM_BENCHMARK)
extern volatile VX1000_UINT32 gVX1000_STIM_Begin[VX1000_STIM_EVENT_COUNT];
extern volatile VX1000_UINT32 gVX1000_STIM_Duration[VX1000_STIM_EVENT_COUNT];
#if defined(VX1000_STIM_HISTOGRAM)
extern volatile VX1000_UINT32 gVX1000_STIM_Histogram[256];
extern volatile VX1000_UINT32 gVX1000_STIM_Histogram2[16];
#define VX1000_STIM_BENCHMARK_DATA     volatile VX1000_UINT32 gVX1000_STIM_Begin[VX1000_STIM_EVENT_COUNT], gVX1000_STIM_Duration[VX1000_STIM_EVENT_COUNT], gVX1000_STIM_Histogram[256], gVX1000_STIM_Histogram2[16]
#else /* !VX1000_STIM_HISTOGRAM */
#define VX1000_STIM_BENCHMARK_DATA     volatile VX1000_UINT32 gVX1000_STIM_Begin[VX1000_STIM_EVENT_COUNT], gVX1000_STIM_Duration[VX1000_STIM_EVENT_COUNT]
#endif /* !VX1000_STIM_HISTOGRAM */
#define VX1000_STIM_DURARRAY_PTR       (VX1000_PTR2VU32_TO_ADDRESS(gVX1000_STIM_Duration))
#else /* !VX1000_STIM_BENCHMARK */
#define VX1000_STIM_DURARRAY_PTR       0UL
#endif /* !VX1000_STIM_BENCHMARK */

#define VX1000_STIM_TIMEOUTS gVX1000.Stim.TimeoutCtr
#define VX1000_STIM_TIMEOUTS_IN_SEQ gVX1000.Stim.TimeoutCtr2

extern VX1000_UINT8 VX1000_SUFFUN(vx1000_StimActive)( VX1000_UINT8 stim_event );
#define VX1000_STIM_ACTIVE(S) VX1000_SUFFUN(vx1000_StimActive)((S))   /* usable in/as an expression */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
extern void VX1000_SUFFUN(vx1000_StimRequest)( VX1000_UINT8 stim_event );
#define VX1000_STIM_REQUEST(S) VX1000_SUFFUN(vx1000_StimRequest)((S)); /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
extern void VX1000_SUFFUN(vx1000_StimSkip)( VX1000_UINT8 stim_event );
#define VX1000_STIM_SKIP(S) VX1000_SUFFUN(vx1000_StimSkip)((S)); /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */

#if defined(VX1000_STIM_BY_OLDA)
#if defined(VX1000_OLDA_FORCE_V8)
extern void VX1000_SUFFUN(vx1000_OldaStimRequestEvent)( VX1000_UINT16 eventNumber );
#else  /* !VX1000_OLDA_FORCE_V8 */
extern void VX1000_SUFFUN(vx1000_OldaStimRequestEvent)( VX1000_UINT8 eventNumber );
#endif /* !VX1000_OLDA_FORCE_V8 */
#define VX1000_STIM_REQUEST_EVENT(x) VX1000_SUFFUN(vx1000_OldaStimRequestEvent)((x)); /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#else /* !VX1000_STIM_BY_OLDA */
#define VX1000_STIM_REQUEST_EVENT(x) VX1000_EVENT((x)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#endif /* !VX1000_STIM_BY_OLDA */

#else /* !VX1000_STIM */

#define VX1000_STIM_PTR                      0UL /* "invalid" */
#define VX1000_STIM_WAIT(     E, T)          VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,1, (E),(T),(0),(0))  /* return code for "timeout" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_STIM_WAIT_VOID(E, T)          VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,1, (E),(T),(0),(0)))    /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_STIMULATE(     D, S, P, T)    VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (D),(S),(P),(T)) /* return code for "inactive" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_STIMULATE_VOID(D, S, P, T)    VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (D),(S),(P),(T)))    /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_STIM_ACTIVE(S)                VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (S),(0),(0),(0)) /* return code for "inactive" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_STIM_CONTROL()                /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_STIM_REQUEST(S)               VX1000_DUMMYREAD((S)) /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_STIM_SKIP(S)                  VX1000_DUMMYREAD((S)) /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_CONTROL()              /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_DAQ(     D, S)         VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (D),(S),(0),(0)) /* return code for "inactive" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_DAQ_VOID(D, S)         VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (D),(S),(0),(0)))    /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_STIM(      S)          VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (S),(0),(0),(0)) /* return code for "inactive" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_STIM_VOID( S)          VX1000_DUMMYREAD(S) /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_TRIGGER(     D, S)     VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (D),(S),(0),(0)) /* return code for "inactive" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_TRIGGER_VOID(D, S)     VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (D),(S),(0),(0)))    /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_WAIT(     S, T)        VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (S),(T),(0),(0)) /* return code for "inactive" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_WAIT_VOID(S, T)        VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (S),(T),(0),(0)))    /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS(     D, S, T)          VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (D),(S),(T),(0)) /* return code for "inactive" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_VOID(D, S, T)          VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (D),(S),(T),(0)))    /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_REGISTER_STIM_EVENT(D, S, T)  VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,0, (D),(S),(T),(0)))   /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_STIM_DURARRAY_PTR             0UL

#endif /* !VX1000_STIM */

/* Hook based bypassing macros */
#if defined(VX1000_HOOK_BASED_BYPASSING) && defined(VX1000_STIM)

extern VX1000_UINT8 VX1000_SUFFUN(vx1000_BypassHbbGetVal_8) ( VX1000_UINT32 HookID, VX1000_UINT8 DefaultValue );
#define VX1000_BYPASS_HBB_GETVAL_8( H, D )      VX1000_SUFFUN(vx1000_BypassHbbGetVal_8) ((H), (D))      /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
extern VX1000_UINT16 VX1000_SUFFUN(vx1000_BypassHbbGetVal_16) ( VX1000_UINT32 HookID, VX1000_UINT16 DefaultValue );
#define VX1000_BYPASS_HBB_GETVAL_16( H, D )     VX1000_SUFFUN(vx1000_BypassHbbGetVal_16) ((H), (D))     /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
extern VX1000_UINT32 VX1000_SUFFUN(vx1000_BypassHbbGetVal_32) ( VX1000_UINT32 HookID, VX1000_UINT32 DefaultValue );
#define VX1000_BYPASS_HBB_GETVAL_32( H, D )     VX1000_SUFFUN(vx1000_BypassHbbGetVal_32) ((H), (D))     /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
extern VX1000_UINT64 VX1000_SUFFUN(vx1000_BypassHbbGetVal_64) ( VX1000_UINT32 HookID, VX1000_UINT64 DefaultValue );
#define VX1000_BYPASS_HBB_GETVAL_64( H, D )     VX1000_SUFFUN(vx1000_BypassHbbGetVal_64) ((H), (D))     /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
extern VX1000_FLOAT VX1000_SUFFUN(vx1000_BypassHbbGetVal_Float) ( VX1000_UINT32 HookID, VX1000_FLOAT DefaultValue );
#define VX1000_BYPASS_HBB_GETVAL_FLOAT( H, D )  VX1000_SUFFUN(vx1000_BypassHbbGetVal_Float) ((H), (D))  /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
extern VX1000_DOUBLE VX1000_SUFFUN(vx1000_BypassHbbGetVal_Double) ( VX1000_UINT32 HookID, VX1000_DOUBLE DefaultValue );
#define VX1000_BYPASS_HBB_GETVAL_DOUBLE( H, D ) VX1000_SUFFUN(vx1000_BypassHbbGetVal_Double) ((H), (D)) /* usable in/as an expression  */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */

#else /* !VX1000_HOOK_BASED_BYPASSING || !VX1000_STIM */

#define VX1000_BYPASS_HBB_GETVAL_8( H, D )      ((VX1000_UINT8)(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,(D), (H),(0),(0),(0))))   /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_HBB_GETVAL_16( H, D )     ((VX1000_UINT16)(VX1000_DISCARD4DUMMYARGS(VX1000_UINT16,(D), (H),(0),(0),(0)))) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_HBB_GETVAL_32( H, D )     VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(D), (H),(0),(0),(0))   /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_HBB_GETVAL_64( H, D )     VX1000_DISCARD4DUMMYARGS((VX1000_UINT64),(D), (H),(0),(0),(0)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_HBB_GETVAL_FLOAT( H, D )  VX1000_DISCARD4DUMMYARGS(VX1000_FLOAT,(D), (H),(0),(0),(0))    /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_BYPASS_HBB_GETVAL_DOUBLE( H, D ) VX1000_DISCARD4DUMMYARGS(VX1000_DOUBLE,(D), (H),(0),(0),(0))   /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */

#endif /* !VX1000_HOOK_BASED_BYPASSING || !VX1000_STIM */


/*------------------------------------------------------------------------------ */
/* Calibration                                                                   */

#if defined(VX1000_CAL)

#include "cc_autosar.h"
#define CC_PTR                         (VX1000_PTR2V_TO_ADDRESS(&gCC))

#else /* !VX1000_CAL */

#define CC_PTR                         0UL

#endif /* !VX1000_CAL */


/*------------------------------------------------------------------------------ */
/* Overlay                                                                       */

#if defined(VX1000_OVERLAY)

#define VX1000_OVL_MAGIC                 0xfec70a17UL
#define VX1000_EMEM_HDR_MAGIC            0xfec81b28UL

#define VX1000_OVLFEAT_KEEP_AWAKE        (1UL << 0) /* VX may suppress ECU falling asleep and/or shut down */
#define VX1000_OVLFEAT_SYNC_PAGESWITCH   (1UL << 1) /* ... */
#define VX1000_OVLFEAT_PERSISTENT_EMEM   (1UL << 2) /* ... */
#define VX1000_OVLFEAT_RST_ON_CALWAKEUP  (1UL << 3) /* ... */
#define VX1000_OVLFEAT_USE_VX_EPK_TRANS  (1UL << 4) /* ... */
#define VX1000_OVLFEAT_VALIDATE_PAGESW   (1UL << 5) /* ... */
#define VX1000_OVLFEAT_CORE_SYNC_PAGESW  (1UL << 6) /* After a page switch request, cores will switch their respective pages individually */
#define VX1000_OVLFEAT_NONE              (0x0UL << 7)
#define VX1000_OVLFEAT_USER              (0x1UL << 7)
#define VX1000_OVLFEAT_DRIVER            (0x2UL << 7)
#define VX1000_OVLFEAT_VX                (0x3UL << 7)
#define VX1000_OVLFEAT_ECU_REGS_VIA_MX   (1UL << 9)  /* overlay registers are configured over mailbox */
#define VX1000_OVLFEAT_RST_ON_SNCPAGESW  (1UL << 10) /* ECU will perform an ECU Reset on Sync_Cal_Page_Switch */

#if defined(VX1000_TARGET_POWERPC)
typedef struct s_ecu_ovl_descriptor
{    /* 4x4 = 16B = 0x10 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 MAS0;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 MAS1;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 MAS2;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 MAS3;
} VX1000_OVL_DESCRIPTOR_T;
typedef struct s_ecu_ovl_config_regs
{   /* 16*1*64 + 4*2 + 2 + 2 = 1036 = 0x40C Bytes */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 magicId;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 version;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 reserved1;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 reserved2;
  VX1000_INNERSTRUCT_VOLATILE VX1000_OVL_DESCRIPTOR_T DESCR[64];   /* for all cores, the descriptors need the same value; there are maximum 64 descriptors in the hardware */
} VX1000_OVL_CONFIG_REGS_T;
#endif /* VX1000_TARGET_POWERPC */

#if defined(VX1000_TARGET_SH2)
typedef struct s_ecu_ovl_descriptor
{    /* 4x4 = 16B = 0x10 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 TODO_SH2;
} VX1000_OVL_DESCRIPTOR_T;
typedef struct s_ecu_ovl_config_regs
{   /* 16*1*64 + 4*2 + 2 + 2 = 1036 = 0x40C Bytes */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 TODO_SH2;
} VX1000_OVL_CONFIG_REGS_T;
#endif /* VX1000_TARGET_SH2 */

#if defined(VX1000_TARGET_TMS570)
typedef struct s_ecu_ovl_descriptor
{    /* 4x4 = 16B = 0x10 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 TODO_TMS570;
} VX1000_OVL_DESCRIPTOR_T;
typedef struct s_ecu_ovl_config_regs
{   /* 16*1*64 + 4*2 + 2 + 2 = 1036 = 0x40C Bytes */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 TODO_TMS570;
} VX1000_OVL_CONFIG_REGS_T;
#endif /* VX1000_TARGET_TMS570 */

#if defined(VX1000_TARGET_TRICORE)
typedef struct s_ecu_ovl_descriptor
{    /* 4x3 = 12B = 0xC */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 RABR;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 OTAR;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 OMASK;
} VX1000_OVL_DESCRIPTOR_T;
typedef struct s_ecu_ovl_config_regs
{   /* 12*1*32 + 4*2 + 2 + 2 = 396 = 0x18C Bytes */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 magicId;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 version;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 reserved;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 OSEL;
  VX1000_INNERSTRUCT_VOLATILE VX1000_OVL_DESCRIPTOR_T DESCR[32];   /* for all cores, the descriptors need the same value; there are maximum 32 descriptors in the hardware */
} VX1000_OVL_CONFIG_REGS_T;
#endif /* VX1000_TARGET_TRICORE */

#if defined(VX1000_TARGET_X850)
typedef struct s_ecu_ovl_descriptor
{    /* 4x4 = 16B = 0x10 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 TODO_x850;
} VX1000_OVL_DESCRIPTOR_T;
typedef struct s_ecu_ovl_config_regs
{   /* 16*1*64 + 4*2 + 2 + 2 = 1036 = 0x40C Bytes */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 TODO_x850;
} VX1000_OVL_CONFIG_REGS_T;
#endif /* VX1000_TARGET_X850 */

#if defined(VX1000_TARGET_XC2000)
typedef struct s_ecu_ovl_descriptor
{    /* 4x4 = 16B = 0x10 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 TODO_XC2000;
} VX1000_OVL_DESCRIPTOR_T;
typedef struct s_ecu_ovl_config_regs
{   /* 16*1*64 + 4*2 + 2 + 2 = 1036 = 0x40C Bytes */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 TODO_XC2000;
} VX1000_OVL_CONFIG_REGS_T;
#endif /* VX1000_TARGET_XC2000 */

typedef struct s_ecu_page_switch
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 magicId;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 version;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8  pageSwitchRequested;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8  targetPage;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 overlayValue;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 overlayMask;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 busMasterRequested;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8  coreDone[32];
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 overlayValueB;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 overlayMaskB;
} VX1000_SYNCAL_PAGE_SWITCH_T;

/* Structure for EMEM invalidation when cal-wakeup and EMEM-supply is used */
typedef struct s_ecu_emem_hdr
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 magicId;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 version;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 reserved;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 ememInitEnd;           /* VX-RW, ECU-RO */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 ememInitEndInvert;     /* VX-RW, ECU-RO */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 ememInitStart;         /* VX-RW, ECU-RW */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 ememInitStartInvert;   /* VX-RW, ECU-RW */
#define VX1000_EMEM_HDR_VERSION 1
} VX1000_EMEM_HDR_T;

typedef struct
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 magicId;                        /* 0x00 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 version;                        /* 0x04 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 presenceCounter;                /* 0x06 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 ovlConfigValue;                 /* 0x08 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 ovlConfigMask;                  /* 0x0C */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 calFeaturesEnable;              /* 0x10 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 persistentECUEmemHeaderPtr;     /* 0x14 */    /* actually a segmented-huge-ptr to VX1000_EMEM_HDR_T */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 ovlBusMasterMask;               /* 0x18 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 ecuLastPresenceCounter;         /* 0x1C */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 ovlEPKLength;                   /* 0x1E */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 ovlReferencePageDataEPKAddress; /* 0x20 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 ovlWorkingPageDataEPKAddress;   /* 0x24 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 syncCalSwitchDataPtr;           /* 0x28 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 ovlConfigRegsPtr;               /* 0x2C */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 ovlConfigValueB;                /* 0x30 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 ovlConfigMaskB;                 /* 0x34 */
  VX1000_SYNCAL_PAGE_SWITCH_T syncCalData;                                  /* 0x38 */
  VX1000_OVL_CONFIG_REGS_T ovlConfigRegs;
#define VX1000_OVL_VERSION             5
} VX1000_OVL_T;

#define VX1000_OVL_DATA                VX1000_OVL_T Ovl
#define VX1000_OVL_PTR                 (VX1000_PTR2VO_TO_ADDRESS(&gVX1000.Ovl))
#define VX1000_CAL_WAKEUP_REQUESTED()  ((gVX1000.ToolCtrlState & ((VX1000_UINT32)(VX1000_TCS_CAL_WAKEUP))) != 0) /* Wrapper API:   VX1000If_CalWakeupRequested */

#if defined(VX1000_OVLENBL_KEEP_AWAKE)
extern VX1000_UINT8 VX1000_SUFFUN(vx1000_IsCalWakeupActive)( void );
#define VX1000_IS_CAL_WAKEUP_ACTIVE()  VX1000_SUFFUN(vx1000_IsCalWakeupActive)()
#else /* !VX1000_OVLENBL_KEEP_AWAKE */
#define VX1000_IS_CAL_WAKEUP_ACTIVE()  0 /* provide return value for "not active" */
#endif /* !VX1000_OVLENBL_KEEP_AWAKE */

#if defined(VX1000_OVLENBL_SYNC_PAGESWITCH)
#if !defined(VX1000_OVL_SET_CONFIG)
#define VX1000_OVL_SET_CONFIG_INTERNAL
extern VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlaySetConfig)(VX1000_UINT32 value, VX1000_UINT32 mask, VX1000_UINT32 valueB, VX1000_UINT32 maskB, VX1000_UINT8 page, VX1000_UINT32 master, VX1000_UINT32 calMaster);
#define VX1000_OVL_SET_CONFIG(value,mask,valueB,maskB,page,master,calMasters) VX1000_SUFFUN(vx1000_OverlaySetConfig)((value),(mask),(valueB),(maskB),(page),(master),(calMasters)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_OVL_SET_CONFIG_VOID(value,mask,valueB,maskB,page,master,calMasters) (void)VX1000_OVL_SET_CONFIG((value),(mask),(valueB),(maskB),(page),(master),(calMasters)); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#endif /* !VX1000_OVL_SET_CONFIG */

extern void VX1000_SUFFUN(vx1000_OverlaySetConfigDone)( VX1000_UINT8 cfgResult, VX1000_UINT8 page, VX1000_UINT8 onStartup );
#define VX1000_OVL_SET_CONFIG_DONE(result, page) VX1000_SUFFUN(vx1000_OverlaySetConfigDone)((result),(page), 0); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#define VX1000_OVL_SET_CONFIG_DONE_STUP(result,page) VX1000_SUFFUN(vx1000_OverlaySetConfigDone)((result),(page), 1); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */

extern VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlayChkPageSwitchDone)( void );
#define VX1000_OVL_CHK_PAGESW_DONE() VX1000_SUFFUN(vx1000_OverlayChkPageSwitchDone)()
#define VX1000_OVL_CHK_PAGESW_DONE_VOID()  (void)VX1000_OVL_CHK_PAGESW_DONE(); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */

extern VX1000_UINT8 VX1000_SUFFUN(vx1000_OverlayChkPageSwitchCore)( VX1000_UINT32 master );
#define VX1000_OVL_CHK_PAGESW_CORE(cores) VX1000_SUFFUN(vx1000_OverlayChkPageSwitchCore)((cores)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_OVL_CHK_PAGESW_CORE_VOID(cores) (void)VX1000_OVL_CHK_PAGESW_CORE((cores)); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */

extern VX1000_UINT8 VX1000_SUFFUN(vx1000_overlayIsPageSwitchReq)( VX1000_UINT32 master );
#define VX1000_OVL_IS_PAGESW_REQUESTED(cores) VX1000_SUFFUN(vx1000_overlayIsPageSwitchReq)((cores)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */

#else /* !VX1000_OVLENBL_SYNC_PAGESWITCH */

#if !defined(VX1000_OVL_SET_CONFIG)
#define VX1000_OVL_SET_CONFIG(value,mask,valueB,maskB,page,master,calMasters)   VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,(1), (value),(mask),(page),VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(master), (calMasters),(valueB),(maskB),(0))) /* return value for "nothing done" */  /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_OVL_SET_CONFIG_VOID(value,mask,valueB,maskB,page,master,calMasters) VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,(1), (value),(mask),(page),VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(master), (calMasters),(valueB),(maskB),(0)))) /* return value for "nothing done" */  /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#endif /* !VX1000_OVL_SET_CONFIG */
#define VX1000_OVL_SET_CONFIG_DONE(result, page)     VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,0UL, (result),(page),(0),(0)))/* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_OVL_SET_CONFIG_DONE_STUP(result,page) VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,0UL, (result),(page),(0),(0)))/* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_OVL_CHK_PAGESW_DONE()                 ((VX1000_UINT8)(1)) /* provide return value "not ready" */
#define VX1000_OVL_CHK_PAGESW_DONE_VOID()            /* empty */
#define VX1000_OVL_CHK_PAGESW_CORE(cores)            VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,((VX1000_UINT8)(1)), (cores),(0),(0),(0))/* provide return value "not ready" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_OVL_CHK_PAGESW_CORE_VOID(cores)       VX1000_DUMMYREAD((cores))/* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_OVL_IS_PAGESW_REQUESTED(cores)        VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (cores),(0),(0),(0))/* provide return value "no" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */

#endif /* !VX1000_OVLENBL_SYNC_PAGESWITCH */

#if defined(VX1000_OVLENBL_PERSISTENT_EMEM)
#if defined(VX1000_EMEM_HDR_MEMORY_SECTION)
#define VX1000_EMEM_HDR_DATA           VX1000_EMEM_HDR_MEMORY_SECTION volatile VX1000_EMEM_HDR_T gVXEmemHdr
#else /* !VX1000_EMEM_HDR_MEMORY_SECTION */
#define VX1000_EMEM_HDR_DATA           volatile VX1000_EMEM_HDR_T gVXEmemHdr
#endif /* !VX1000_EMEM_HDR_MEMORY_SECTION */
#define VX1000_EMEM_HDR_PTR            (VX1000_PTR2VEHM_TO_ADDRESS(&gVXEmemHdr))
extern void VX1000_SUFFUN(vx1000_InvalidateEmem)( void );
#define VX1000_INVALIDATE_EMEM()       VX1000_SUFFUN(vx1000_InvalidateEmem)(); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */

#else /* !VX1000_OVLENBL_PERSISTENT_EMEM */

#define VX1000_EMEM_HDR_PTR            0UL
#define VX1000_INVALIDATE_EMEM()       /* empty */

#endif /* !VX1000_OVLENBL_PERSISTENT_EMEM */

/* provide empty user callbacks if not already specified by the user: */
#if !defined(VX1000_OVL_RST_ON_CAL_WAKEUP_CB)
#define VX1000_OVL_RST_ON_CAL_WAKEUP_CB() /* empty */
#endif /* !VX1000_OVL_RST_ON_CAL_WAKEUP_CB */


#else /* !VX1000_OVERLAY */

#define VX1000_OVL_PTR                 0UL
#define VX1000_EMEM_HDR_PTR            0UL
#define VX1000_INVALIDATE_EMEM()       /* empty */
#define VX1000_CAL_WAKEUP_REQUESTED()  0 /* provide return value for "no request" */
#define VX1000_IS_CAL_WAKEUP_ACTIVE()  0 /* provide return value for "not active" */
#if !defined(VX1000_OVL_SET_CONFIG)
#define VX1000_OVL_SET_CONFIG(value,mask,valueB,maskB,page,master,calMasters)      VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,(1), (value),(mask),(page),VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(master), (calMasters),(valueB),(maskB),(0)))  /* return value for "nothing done" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_OVL_SET_CONFIG_VOID(value,mask,valueB,maskB,page,master,calMasters) VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,(1), (value),(mask),(page),VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,(master), (calMasters),(valueB),(maskB),(0))))  /* return value for "nothing done" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#endif /* !VX1000_OVL_SET_CONFIG */
#define VX1000_OVL_SET_CONFIG_DONE(result, page)                      VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,0UL, (result),(page),(0),(0)))/* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_OVL_SET_CONFIG_DONE_STUP(result,page)                  VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,0UL, (result),(page),(0),(0)))/* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_OVL_CHK_PAGESW_DONE()                                  ((VX1000_UINT8)(1))         /* provide return value for "not ready" ? */
#define VX1000_OVL_CHK_PAGESW_DONE_VOID()                             /* empty */
#define VX1000_OVL_CHK_PAGESW_CORE(cores)                             VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,((VX1000_UINT8)(1)), (cores),(0),(0),(0))/* provide return value for "not ready" ? */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_OVL_CHK_PAGESW_CORE_VOID(cores)                        VX1000_DUMMYREAD((cores))/* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_OVL_IS_PAGESW_REQUESTED(cores)                         VX1000_DISCARD4DUMMYARGS(VX1000_UINT8,0, (cores),(0),(0),(0))/* provide return value "no" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#endif /* !VX1000_OVERLAY */

/*------------------------------------------------------------------------------ */
/* VX1000_RES_MGMT                                                               */
/* Note: this is a pure project specific feature.                                */
/* The specification comes from the customer, so do not improve.                 */

#if defined(VX1000_RES_MGMT)

#define VX1000_RES_MGMT_MAGIC          0xFEC70A18UL

typedef struct
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 magicId;                 /* 0x00 */ /* Version 2 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 version;                 /* 0x04 */
  /* Resource 0 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8  ovlConfigItemStart;      /* 0x06 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8  ovlConfigItemLength;     /* 0x07 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 resMgmtEnable;           /* 0x08 */
  /* Resource 1 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 ovlRamStart;             /* 0x0C */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 ovlRamSize;              /* 0x10 */
} VX1000_RES_MGMT_T;

#define VX1000_RES_MGMT_RES_CONFIG_ITEM (1UL << 0)
#define VX1000_RES_MGMT_RES_OVL_RAM    (1UL << 1)

#define VX1000_RES_MGMT_DATA           VX1000_RES_MGMT_T ResMgmt
#define VX1000_RES_MGMT_PTR            (VX1000_PTR2VRM_TO_ADDRESS(&gVX1000.ResMgmt))
#define VX1000_ENABLE_ACCESS()         gVX1000.ToolDetectState &= ~(VX1000_TDS_VX_ACCESS_DISABLED); /* Wrapper API:   VX1000If_EnableAccess */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
extern VX1000_UINT8 VX1000_SUFFUN(vx1000_DisableAccess)( void );
#define VX1000_DISABLE_ACCESS()        VX1000_SUFFUN(vx1000_DisableAccess)()
#define VX1000_DISABLE_ACCESS_VOID()   (void)VX1000_SUFFUN(vx1000_DisableAccess)(); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#define VX1000_IS_ACCESS_DISABLED()    ((gVX1000.ToolDetectState & (VX1000_TDS_VX_ACCESS_DISABLED)) != 0UL) /* Wrapper API:   VX1000If_IsAccessDisabled */
#define VX1000_RES_MGMT_VERSION        1

/* Resource Overlay Configuration Item */
#if !defined(VX1000_RES_MGMT_CFG_ITEM_START)
#define VX1000_RES_MGMT_CFG_ITEM_START 0
#endif /* !VX1000_RES_MGMT_CFG_ITEM_START */

#if !defined(VX1000_RES_MGMT_CFG_ITEM_LEN)
#define VX1000_RES_MGMT_CFG_ITEM_LEN   32
#endif /* !VX1000_RES_MGMT_CFG_ITEM_LEN */

#if !defined(VX1000_RES_MGMT_ENABLE_CFG_ITEM)
#define VX1000_RES_MGMT_ENBLVAL_CFG_ITEM 0
#else /* VX1000_RES_MGMT_ENABLE_CFG_ITEM */
#define VX1000_RES_MGMT_ENBLVAL_CFG_ITEM VX1000_RES_MGMT_RES_CONFIG_ITEM
#endif /* VX1000_RES_MGMT_ENABLE_CFG_ITEM */

/* Resource Ram */
#if !defined(VX1000_RES_MGMT_RAM_START)
#define VX1000_RES_MGMT_RAM_START  0
#endif /* !VX1000_RES_MGMT_RAM_START */

#if !defined(VX1000_RES_MGMT_RAM_SIZE)
#define VX1000_RES_MGMT_RAM_SIZE   0
#endif /* !VX1000_RES_MGMT_RAM_SIZE */

#if !defined(VX1000_RES_MGMT_ENABLE_OVL_RAM)
#define VX1000_RES_MGMT_ENBLVAL_OVL_RAM 0
#else /* VX1000_RES_MGMT_ENABLE_OVL_RAM */
#define VX1000_RES_MGMT_ENBLVAL_OVL_RAM VX1000_RES_MGMT_RES_OVL_RAM
#endif /* VX1000_RES_MGMT_OVL_ENABLE */


#else /* !VX1000_RES_MGMT */

#define VX1000_DISABLE_ACCESS_VOID()   /* empty */
#define VX1000_DISABLE_ACCESS()        (0) /* provide return value for "there is no VX anyway" */
#define VX1000_ENABLE_ACCESS()         /* empty */
#define VX1000_IS_ACCESS_DISABLED()    (1) /* provide return value for "tool is disabled" */
#define VX1000_RES_MGMT_PTR            0UL

#endif /* !VX1000_RES_MGMT */

typedef struct
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 MagicId;               /* 0x00 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8  StructVersion;         /* 0x04 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8  Reserved0;             /* 0x05 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16 Reserved1;             /* 0x06 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 MemSync_0;             /* 0x08 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 MemSync_1;             /* 0x0C */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 MemSync_2;             /* 0x10 */
} VX1000_MEMSYNC_TRIG_T;
#define VX1000_STRUCT_MAGIC_MEMSYNCTRIG 0xFEC72016UL

typedef struct
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 MagicId;               /* 0x00 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8  StructVersion;         /* 0x04 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8  SyncWinStatus;         /* 0x05 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8  SyncMasterId;          /* 0x06 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8  SyncTriggerId;         /* 0x07 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 SyncWinStart;          /* 0x08 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 SyncWinSize;           /* 0x0C */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32 SyncChunkSize;         /* 0x10 */
} VX1000_MEMSYNC_CPY_T;
#define VX1000_STRUCT_MAGIC_MEMSYNCCPY 0xFEC70815UL

#if ((VX1000_MEMSYNC_TRIGGER_COUNT) == 0)
#define VX1000_MEMSYNC_TRIGGER_PTR     0UL
#elif ((VX1000_MEMSYNC_TRIGGER_COUNT) == 1)
#if !defined(VX1000_MEMSYNC_TRIGGER_PTR)
#define VX1000_MEMSYNCTRIG_DATA        VX1000_MEMSYNC_TRIG_T MemSyncTrigger
#define VX1000_MEMSYNC_TRIGGER_PTR     (VX1000_PTR2MT_TO_ADDRESS(&gVX1000.MemSyncTrigger))
#endif /* !VX1000_MEMSYNC_TRIGGER_PTR */
#else  /* VX1000_MEMSYNC_TRIGGER_COUNT > 1 */
#define VX1000_MEMSYNCTRIG_DATA        VX1000_UINT32 MemSyncTrigger[VX1000_MEMSYNC_TRIGGER_COUNT]
#define VX1000_MEMSYNC_TRIGGER_PTR     (VX1000_PTR2U32_TO_ADDRESS(gVX1000.MemSyncTrigger))
#endif /* VX1000_MEMSYNC_TRIGGER_COUNT > 1 */
#if ((VX1000_MEMSYNC_COPY_COUNT) == 0)
#define VX1000_MEMSYNC_COPY_PTR        0UL
#else  /* VX1000_MEMSYNC_COPY_COUNT */
#define VX1000_MEMSYNCCPY_DATA         VX1000_MEMSYNC_CPY_T MemSyncCopy[VX1000_MEMSYNC_COPY_COUNT]
#define VX1000_MEMSYNC_COPY_PTR        (VX1000_PTR2MC_TO_ADDRESS(gVX1000.MemSyncCopy))
#endif /* VX1000_MEMSYNC_COPY_COUNT */

#if !defined(VX1000_ADDONS_INIT)
#define VX1000_ADDONS_INIT()           /* empty */
#endif /* !VX1000_ADDONS_INIT */
#if !defined(VX1000_ADDONS_DASDAQ_OLDACMD)
#define VX1000_ADDONS_DASDAQ_OLDACMD(A, B, C, D, E) VX1000_ERRLOGGER(VX1000_ERRLOG_OLDA_UNIMPLCMD)  /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to overload a simple default implementation by a premium version implemented in an optional source file */
#endif /* VX1000_ADDONS_DASDAQ_OLDACMD */
#if !defined(VX1000_ADDONS_DASSTIM_OLDACMD)
#define VX1000_ADDONS_DASSTIM_OLDACMD(A, B, C, D, E) VX1000_ERRLOGGER(VX1000_ERRLOG_OLDA_UNIMPLCMD) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to overload a simple default implementation by a premium version implemented in an optional source file */
#endif /* VX1000_ADDONS_DASSTIM_OLDACMD */


#if defined(VX1000_DISABLE_INSTRUMENTATION)

#define VX1000_DATA                     /* empty */
#define VX1000_INIT()                   /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_INIT_ASYNC_START()       /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_INIT_ASYNC_END()         /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_PREPARE_SOFTRESET_VOID() /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_PREPARE_SOFTRESET()      (0) /* provide return value for "accepted" */
#define VX1000_EVENT(e)                 VX1000_DUMMYREAD((e)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_TIMESTAMP_EVENT(e, t)    VX1000_DUMMYREAD(VX1000_DISCARD4DUMMYARGS(VX1000_UINT32,0UL, (e),(t),(0),(0))) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_TIMESTAMP_UPDATE(t)      VX1000_DUMMYREAD((t)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_TIMESTAMP()              /* empty */

#else /* !VX1000_DISABLE_INSTRUMENTATION */

/*------------------------------------------------------------------------------ */
/* User functions and macros for DAQ                                             */

#if defined(VX1000_OLDA)

#if defined(VX1000_OLDA_FORCE_V8)
/* Trigger a universal event (TRACE or OLDA; event numbers 0 to 0xFFFE) */
#else /* !VX1000_OLDA_FORCE_V8 */
/* Trigger a universal event (TRACE or OLDA; event numbers 0 to 30) */
#endif /* !VX1000_OLDA_FORCE_V8 */
#define VX1000_EVENT(x)                VX1000_OLDA_EVENT((x)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */

/* Trigger a special event (EventNumber=0..0xFFFFFFFF, OldaEventNumbers=31/30) */
/* Note that we can't do the "gVX1000.Olda.EventList[31].EventCounter += 2;" because the array's */
/* size is dynamically assigned by the VX at runtime depending on the current DAQ configuration! */
#if defined(VX1000_OLDA_FORCE_V8)
#define VX1000_SPECIAL_EVENT(e)        VX1000_SUFFUN(vx1000_OldaEvent)(0U, (e)); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#define VX1000_SPECIAL_EVENT2(e)       VX1000_SPECIAL_EVENT((e)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#elif ((VX1000_MEMSYNC_TRIGGER_COUNT) > 0)
#define VX1000_SPECIAL_EVENT(e)        VX1000_SUFFUN(vx1000_OldaSpecialEvent)((e)); /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#define VX1000_SPECIAL_EVENT2(e)       VX1000_SPECIAL_EVENT((e)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#else /* !VX1000_OLDA_FORCE_V8 */
#define VX1000_STORE_SPECIAL_EVENT(e) do                                                                              \
  {                                                                                                                   \
    VX1000_STORE_TIMESTAMP((VX1000_CLOCK()))                                                                          \
    VX1000_STORE_EVENT((VX1000_UINT32)(e))                                                                            \
  } while(0) /* this never-looping while is here only for MISRA */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#if defined(VX1000_SUPPRESS_TRACE_SUPPORT) || defined(VX1000_TARGET_TRICORE)
#if defined(VX1000_OLDA_AUDMBR_REG_ADDR)
#define VX1000_TRIGGER_SPECIAL_EVENT() do                                                                             \
  {                                                                                                                   \
    (VX1000_ADDR_TO_PTR2VU16(VX1000_OLDA_AUDMBR_REG_ADDR))[0] = (1U << 15U);                                          \
  } while(0); /* this never-looping while is here only for MISRA */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#elif defined(VX1000_OLDA_DTS_BASE_ADDR)
#define VX1000_TRIGGER_SPECIAL_EVENT() do                                                                             \
  {                                                                                                                   \
    VX1000_ADD_MPC_TRIGS(31);                                                                                         \
  } while(0); /* this never-looping while is here only for MISRA */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#elif defined(VX1000_TARGET_TRICORE)
#define VX1000_TRIGGER_SPECIAL_EVENT() do                                                                             \
  {                                                                                                                   \
      if (VX1000_ECU_IS_AURIX()) { VX1000_MCREG_OCDS_TRIGS = 31UL;         }                                          \
      else                       { VX1000_MCREG_OCDS_TRIGS = 0x80000000UL; }                                          \
  } while(0); /* this never-looping while is here only for MISRA */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#elif defined(VX1000_TARGET_XC2000)
#define VX1000_TRIGGER_SPECIAL_EVENT() do                                                                             \
  {                                                                                                                   \
      gVX1000.OldaEventNumber ^= (VX1000_UINT32)0x80000000UL;                                                         \
      gVX1000.CalPtr          ^= (VX1000_UINT32)0x80000000UL;                                                         \
  } while(0); /* this never-looping while is here only for MISRA */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#else /* classic olda event trigger */
#define VX1000_TRIGGER_SPECIAL_EVENT() do                                                                             \
  {                                                                                                                   \
    VX1000_ATOMIC_XOR32X2((/*(VX1000_UINT32)*/&gVX1000.OldaEventNumber), 0x80000000UL);                               \
  } while(0); /* this never-looping while is here only for MISRA */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#define VX1000_SPECIAL_EVENT_BARRIER() do                                                                             \
  {                                                                                                                   \
    VX1000_ARM_DSB()                                                                                                  \
  } while(0); /* this never-looping while is here only for MISRA */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#endif /* classic olda event trigger */
#else  /* !VX1000_SUPPRESS_TRACE_SUPPORT && !VX1000_TARGET_TRICORE */
/* No need for process serialisation - just trigger a trace event but care for instruction serialisation */
#define VX1000_TRIGGER_SPECIAL_EVENT() /* empty */
#define VX1000_SPECIAL_EVENT_BARRIER() do                                                                             \
  {                                                                                                                   \
    VX1000_ARM_DSB()                                                                                                  \
  } while(0); /* this never-looping while is here only for MISRA */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#endif /* !VX1000_SUPPRESS_TRACE_SUPPORT && !VX1000_TARGET_TRICORE */
#endif /* !VX1000_OLDA_FORCE_V8 */

#if defined(VX1000_TARGET_TRICORE)
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: None                                                                                                        */
/* API name:      VX1000_TRACE_EVENT                                                                                          */
/* Wrapper API:   None                                                                                                        */
/* Parameter1:    eventNumber E [0,512)                                                                                       */
/* Return value:  None                                                                                                        */
/* Preemption:    No problem                                                                                                  */
/* Termination:   No effect                                                                                                   */
/* Precondition1:                                                                                                             */
/* Description:   Trigger a TriCore specific TRACE event for the XCP master.                                                  */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
#define VX1000_TRACE_EVENT(e) do               \
  {                                            \
    VX1000_STORE_TIMESTAMP((VX1000_CLOCK()))   \
    VX1000_STORE_EVENT((VX1000_UINT32)(e))     \
  } while(0); /* this never-looping while is here only for MISRA */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#endif /* VX1000_TARGET_TRICORE */

#if !defined(VX1000_SPECIAL_EVENT)
#if !defined(VX1000_SPECIAL_EVENT_BARRIER)
#define VX1000_SPECIAL_EVENT_BARRIER() /* empty */
#endif /* !VX1000_SPECIAL_EVENT_BARRIER */

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: None or vx1000_OldaEvent (depending on configuration)                                                       */
/* API name:      VX1000_SPECIAL_EVENT                                                                                        */
/* Wrapper API:   None                                                                                                        */
/* Parameter1:    special eventNumber                                                                                         */
/* Return value:  None                                                                                                        */
/* Preemption:    No problem                                                                                                  */
/* Termination:   No effect                                                                                                   */
/* Precondition1:                                                                                                             */
/* Description:   Trigger a special event for VX internal status synchronisation                                              */
/* Devel state:   Implemented                                                                                                 */
/*----------------------------------------------------------------------------------------------------------------------------*/
#define VX1000_SPECIAL_EVENT(e) do             \
  {                                            \
    VX1000_STORE_SPECIAL_EVENT(e);             \
    VX1000_TRIGGER_SPECIAL_EVENT()             \
    VX1000_SPECIAL_EVENT_BARRIER()             \
  } while(0); /* this never-looping while is here only for MISRA */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#define VX1000_SPECIAL_EVENT2(e) do            \
  {                                            \
    VX1000_STORE_SPECIAL_EVENT(e);             \
    VX1000_SPECIAL_EVENT_BARRIER()             \
  } while(0); /* this never-looping while is here only for MISRA */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#endif /* !VX1000_SPECIAL_EVENT */

#else /* !VX1000_OLDA */

/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: None or vx1000_OldaEvent (depending on configuration)                                                       */
/* API name:      VX1000_EVENT                                                                                                */
/* Wrapper API:   VX1000If_Event                                                                                              */
/* Parameter1:    eventNumber E [0,512)                                                                                       */
/* Return value:  None                                                                                                        */
/* Preemption:    No problem                                                                                                  */
/* Termination:   No effect                                                                                                   */
/* Precondition1:                                                                                                             */
/* Description:   Trigger a normal TRACE event for the XCP master.                                                            */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
#define VX1000_EVENT(e) do                     \
  {                                            \
    VX1000_STORE_TIMESTAMP((VX1000_CLOCK()))   \
    VX1000_STORE_EVENT((e))                    \
    VX1000_ARM_DSB()                           \
  } while(0); /* this never-looping while is here only for MISRA */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */

#define VX1000_SPECIAL_EVENT(e)        VX1000_EVENT((e)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_SPECIAL_EVENT2(e)       VX1000_SPECIAL_EVENT((e)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */

/* Update the ECU timestamp with a given value */
#define VX1000_TIMESTAMP_UPDATE(t)             \
  VX1000_STORE_TIMESTAMP((VX1000_UINT32)(t)) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */

#endif /* !VX1000_OLDA */


/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: None                                                                                                        */
/* API name:      VX1000_TIMESTAMP_EVENT                                                                                      */
/* Wrapper API:   None                                                                                                        */
/* Parameter1:    eventNumber E [0,512)                                                                                       */
/* Parameter2:    timestamp (32bit upwards counting)                                                                          */
/* Return value:  None                                                                                                        */
/* Preemption:    No problem                                                                                                  */
/* Termination:   No effect                                                                                                   */
/* Precondition1:                                                                                                             */
/* Description:   Trigger an event with a given value for ECU timestamp.                                                      */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
#define VX1000_TIMESTAMP_EVENT(e, t) do          \
  {                                              \
    VX1000_STORE_TIMESTAMP((VX1000_UINT32)(t))   \
    VX1000_STORE_EVENT((e))                      \
    VX1000_ARM_DSB()                             \
  } while(0); /* this never-loop-while is here only for MISRA */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */


/*----------------------------------------------------------------------------------------------------------------------------*/
/* Internal name: None                                                                                                        */
/* API name:      VX1000_TIMESTAMP                                                                                            */
/* Wrapper API:   None                                                                                                        */
/* Parameter1:    None                                                                                                        */
/* Return value:  None                                                                                                        */
/* Preemption:    No problem                                                                                                  */
/* Termination:   No effect                                                                                                   */
/* Precondition1:                                                                                                             */
/* Description:   Update the timestamp (optional, only needed if timestamp update rate by events is too slow).                */
/* Devel state:   Tested                                                                                                      */
/*----------------------------------------------------------------------------------------------------------------------------*/
#define VX1000_TIMESTAMP() VX1000_STORE_TIMESTAMP((VX1000_CLOCK())) /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */


/*------------------------------------------------------------------------------ */
/* VX1000 data structure                                                         */
/* Version history (as "new in version"):                                        */
/*  4: OldaPtr                                                                   */
/*  5: Olda integrated at fixed position 0x40                                    */
/*  6: CalPtr                                                                    */
/*  7: ToolCtrlState (written by tool)                                           */
/*  8: ResMgmtPtr, OvlPtr                                                        */
/*  9: FileVersion, FeatureFlags0, TimerXXX, xxxDur-pointers, ErrLog             */
/* 10: MemSyncXXXPtr, MemSyncXXXCnt, ReleaseNamexxx, BuildNumber                 */

#define VX1000_STRUCT_VERSION          11UL
#define VX1000_STRUCT_MAGIC            0xFEC70A08UL

#define VX1000_ECUFEAT_FKLINSTRUMENTED     (1UL << 0) /* The application provides support to download and start an external flash kernel*/
#define VX1000_ECUFEAT_MX_FLASHSTART       (1UL << 1) /* reserved for mailbox flashing */
#define VX1000_ECUFEAT_MX_FLASHMODIFY      (1UL << 2) /* reserved for mailbox flashing */
#define VX1000_ECUFEAT_MX_SOFTRESET        (1UL << 3) /* reserved for mailbox flashing */
#define VX1000_ECUFEAT_MX_CALPAGE_RW       (1UL << 4) /* The application wants to handle calpage up/downloads manually via mailbox callbacks */
#define VX1000_ECUFEAT_MX_CALPAGE_GSC      (1UL << 5) /* The application provides mailbox handling for get/set/copy calpage commands */
#define VX1000_ECUFEAT_STIMWT_DURATION     (1UL << 6) /* The application provides a snapshot of all STIM events' busy waiting times */
#define VX1000_ECUFEAT_STIMRT_DURATION     (1UL << 7) /* The application provides a snapshot of all STIM events' round trip times and a global timeout counter */
#define VX1000_ECUFEAT_STIMRT_HISTOGR      (1UL << 8) /* The application provides a histogram of one STIM event's round trip times since ECU boot */
/* reserved range for DAS:                 (1UL << 9) */

#if !defined(VX1000_ADDONS_FEATUREFLAGS0)
#define VX1000_ADDONS_FEATUREFLAGS0        0
#endif /* !VX1000_ADDONS_FEATUREFLAGS0 */

#define VX1000_RELEASECODEH(LIST) VX1000_RELEASECODEH_(LIST) /* PRQA S 3453 */ /* Cannot avoid violating MISRA rule 19.7 because this macro may be used as initialiser of a constant and thus cannot be changed to a runtime function */
#define VX1000_RELEASECODEB(LIST) VX1000_RELEASECODEB_(LIST) /* PRQA S 3453 */ /* Cannot avoid violating MISRA rule 19.7 because this macro may be used as initialiser of a constant and thus cannot be changed to a runtime function */
#define VX1000_RELEASECODET(LIST) VX1000_RELEASECODET_(LIST) /* PRQA S 3453 */ /* Cannot avoid violating MISRA rule 19.7 because this macro may be used as initialiser of a constant and thus cannot be changed to a runtime function */
#define VX1000_RELEASECODEH_(C0, C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11) (((VX1000_UINT8)((C0)-' ')<<2) | ((VX1000_UINT8)((C1)-' ')>>4)) /* PRQA S 3453 */ /* Cannot avoid violating MISRA rule 19.7 because this macro may be used as initialiser of a constant and thus cannot be changed to a runtime function */
#define VX1000_RELEASECODEB_(C0, C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11) ((((VX1000_UINT32)(((C1)-' ')&0xF)<<28) | ((VX1000_UINT32)((C2)-' ')<<22)) | (((VX1000_UINT32)((C3)-' ')<<16) | ((VX1000_UINT32)((C4)-' ')<<10))) | (((VX1000_UINT32)((C5)-' ')<<4) | ((VX1000_UINT32)((C6)-' ')>>2))  /* PRQA S 3453 */ /* Cannot avoid violating MISRA rule 19.7 because this macro may be used as initialiser of a constant and thus cannot be changed to a runtime function */
#define VX1000_RELEASECODET_(C0, C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11) ((((VX1000_UINT32)(((C6)-' ')&0x3)<<30) | ((VX1000_UINT32)((C7)-' ')<<24)) | (((VX1000_UINT32)((C8)-' ')<<18) | ((VX1000_UINT32)((C9)-' ')<<12))) | (((VX1000_UINT32)((C10)-' ')<<6) | ((VX1000_UINT32)((C11)-' ')>>0)) /* PRQA S 3453 */ /* Cannot avoid violating MISRA rule 19.7 because this macro may be used as initialiser of a constant and thus cannot be changed to a runtime function */

typedef struct
{
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   EventNumber;         /* 0x00 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   EventTimestamp;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   MagicId;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   Version;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   GetIdPtr;            /* 0x10 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   GetIdLen;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   XcpMailboxPtr;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   StimCtrlPtr;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   RamSynchField;       /* 0x20 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   ToolDetectState;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   OldaPtr;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   OldaEventNumber;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   CalPtr;              /* 0x30 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   ToolCtrlState;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   ErrLogAddr;          /* previous: VX1000_UINT32 TimestampInfoMant is no longer used, redefined in v9 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8    ErrLogSize;          /* previous: VX1000_INT16  TimestampInfoExpDiv, no longer used, redefined in v9 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8    ErrLogIndex;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16   FileVersion;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   ResMgmtPtr;          /* 0x40 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   OvlPtr;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   FeatureFlags0;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   TimerFreqkHz;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   TimerAddr;           /* 0x50 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8    TimerAddrRegion;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8    TimerSize;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8    TimerOffset;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8    TimerDirection;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   StimDurArrayPtr;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   OldaDurArrayPtr;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   MemSyncTrigPtr;      /* 0x60 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   Padding32;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   MemSyncCpyPtr;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8    MemSyncTrigCnt;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8    Padding8;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8    MemSyncCpyCnt;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT8    ReleaseNameHead;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   ReleaseNameBody;     /* 0x70 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   ReleaseNameTail;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   BuildNumber;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   MagicIdSfrAddr;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   StaticLutAddr;       /* 0x80 */
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   DynamicLutAddr;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT32   MemsyncLutAddr;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16   DynamicAddressSignalsCnt;
  VX1000_INNERSTRUCT_VOLATILE VX1000_UINT16   Padding16;

  /* note: no pointer to VX1000_COLDSTART_BENCHMARK_DATA because that info is not settled, yet */
  /* note: no pointer to VX1000_HOOK_CONTROL_DATA because that part of the driver is typically outsourced now */
  /* note: no pointers to gVX1000_OLDA_TransferCount[] and gVX1000_OLDA_TransferSize[] because the VX can collect such info internally */
#if defined(VX1000_MEMSYNCTRIG_DATA)
  VX1000_MEMSYNCTRIG_DATA;             /* no fix offset */
#endif /* VX1000_MEMSYNCTRIG_DATA */
#if defined(VX1000_DYNADDRSIG_DYNAMICLUT_DT)
  VX1000_DYNADDRSIG_DYNAMICLUT_DT;     /* no fix offset */
#endif /* VX1000_DYNADDRSIG_DYNAMICLUT_DT */
#if defined(VX1000_DYNADDRSIG_MEMSYNCLUT_DT)
  VX1000_DYNADDRSIG_MEMSYNCLUT_DT;     /* no fix offset */
#endif /* VX1000_DYNADDRSIG_MEMSYNCLUT_DT */
#if defined(VX1000_OLDA_DATA)
  VX1000_OLDA_DATA;                    /* no fix offset (note: FW < 1.8 expected 0x40 during cold-start!) */
#endif /* VX1000_OLDA_DATA */
#if defined(VX1000_STIM_DATA)
  VX1000_STIM_DATA;                    /* no fix offset */
#endif /* VX1000_STIM_DATA */
#if defined(VX1000_MAILBOX_DATA)
  VX1000_MAILBOX_DATA;                 /* no fix offset */
#endif /* VX1000_MAILBOX_DATA */
#if defined(VX1000_OVL_DATA)
  VX1000_OVL_DATA;                     /* no fix offset */
#endif /* VX1000_OVL_DATA */
#if defined(VX1000_RES_MGMT_DATA)
  VX1000_RES_MGMT_DATA;                /* no fix offset */
#endif /* VX1000_RES_MGMT_DATA */
#if defined(VX1000_ERRLOG_DATA)
  VX1000_ERRLOG_DATA;                  /* no fix offset */
#endif /* VX1000_ERRLOG_DATA */
#if defined(VX1000_MEMSYNCCPY_DATA)
  VX1000_MEMSYNCCPY_DATA;              /* no fix offset */
#endif /* VX1000_MEMSYNCCPY_DATA */
  /* note: other VX data not included by now to not loose backward compatibility of old A2L files; this will be changed changed when */
  /* there'd be a user's requirement for specific linkage of all VX objects or there is a requirement to split ROM and RAM anyway... */
} VX1000_STRUCT_T;

/*------------------------------------------------------------------------------ */
/* VX1000_INIT_ASYNC_START(), VX1000_INIT_ASYNC_END(); legacy VX1000_DATA dummy  */
/* API for declaring and initialising the global VX1000 data needed for VX1000   */
/* Must be called before any other VX1000_xxxx() routine is called but _after_   */
/* timestamp provider has been initialised                                       */

extern void VX1000_SUFFUN(vx1000_Init)(void);
#define VX1000_INIT() VX1000_SUFFUN(vx1000_Init)(); /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
extern void VX1000_SUFFUN(vx1000_InitAsyncStart)(void);
#define VX1000_INIT_ASYNC_START() VX1000_SUFFUN(vx1000_InitAsyncStart)(); /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
extern void VX1000_SUFFUN(vx1000_InitAsyncEnd)(void);
#define VX1000_INIT_ASYNC_END() VX1000_SUFFUN(vx1000_InitAsyncEnd)(); /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */

#if defined(VX1000_SOFTRESET_TIMEOUT_MS)
extern VX1000_UINT8 VX1000_SUFFUN(vx1000_PrepareSoftreset)(void);
#define VX1000_PREPARE_SOFTRESET() VX1000_SUFFUN(vx1000_PrepareSoftreset)() /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_PREPARE_SOFTRESET_VOID() (void)VX1000_SUFFUN(vx1000_PrepareSoftreset)(); /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */ /* PRQA S 3412 */ /* Willingly violating MISRA rule 19.4 to be able to provide configurable API wrappers */
#else /* !VX1000_SOFTRESET_TIMEOUT_MS */
#define VX1000_PREPARE_SOFTRESET()      (0) /* provide return value for "accepted" */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#define VX1000_PREPARE_SOFTRESET_VOID() /* empty */ /* PRQA S 3453 */ /* Willingly violating MISRA rule 19.7 to be able to provide configurable API wrappers */
#endif /* !VX1000_SOFTRESET_TIMEOUT_MS */

/* Define the global data needed for VX1000 (the user must ensure that the VX1000_STRUCT_DATA part is linked first!) */
#define VX1000_DATA /* now an empty dummy because the actual data is already declared in VX1000.c module directly! */

/* include user-defined lines with optional section pragmas to force individual linkage of VX1000 structure data. */
/* Note that we're using nested section switches here (which might be unsupported by some compilers but has the  */
/* advantage of not requiring #undefs as those would violate the MISRA coding guidelines).                       */
#define VX1000_BEGSECT_VXSTRUCT_H
#include "VX1000_cfg.h"  /* PRQA S 0883 */ /* Willingly violating MISRA rule 19.15: this file is included multiple times because it contains multiple disjunct parts that each are filtered exactly once by preprocessor conditions */ /* PRQA S 5087*/ /* Willingly violating MISRA rule 19.1: this file is included multiple at exactly those lines where it is needed to expand to the desired lines. Inclusion/expansion of those lines at the top of this file would not make sense and lead to compile errors */
#define VX1000_BEGSECT_VXSTRUCT_H_UNDO

#if defined(VX1000_TARGET_TRICORE)
#if defined(VX1000_MEMORY_SECTION)
#define VX1000_STRUCT_DATA VX1000_MEMORY_SECTION volatile VX1000_STRUCT_T gVX1000
#elif defined(VX1000_STRUCT_ADDR)
#define VX1000_STRUCT_DATA volatile VX1000_STRUCT_T gVX1000 __at(VX1000_STRUCT_ADDR) /* TaskingVX syntax */
#else /* !VX1000_MEMORY_SECTION & !VX1000_STRUCT_ADDR */
#define VX1000_STRUCT_DATA volatile VX1000_STRUCT_T gVX1000 /* Shall be linked into a memory segment with zero initialisation! */
#endif /* !VX1000_MEMORY_SECTION & !VX1000_STRUCT_ADDR */
#elif defined(VX1000_TARGET_XC2000)
#if defined(VX1000_STRUCT_ADDR)
#define VX1000_STRUCT_DATA volatile VX1000_STRUCT_T gVX1000 VX1000_OBJAT (VX1000_STRUCT_ADDR) /* TODO: how is the ADDR interpreted? huge? typecast needed? */
#elif defined(VX1000_MEMORY_SECTION)
#define VX1000_STRUCT_DATA volatile VX1000_STRUCT_T gVX1000 VX1000_MEMORY_SECTION
#else /* !VX1000_MEMORY_SECTION & !VX1000_STRUCT_ADDR */
#define VX1000_STRUCT_DATA volatile VX1000_STRUCT_T gVX1000 /* Shall be linked into a memory segment with zero initialisation! */
#endif /* !VX1000_MEMORY_SECTION & !VX1000_STRUCT_ADDR */
#else  /* !VX1000_TARGET_TRICORE & !VX1000_TARGET_XC2000 */
#if defined(VX1000_STRUCT_ADDR)
#error "VX1000_STRUCT_ADDR must not be defined (in VX1000_cfg.h), because this feature is not supported for this microcontroller!"
#else /* !VX1000_STRUCT_ADDR */
#if defined(VX1000_MEMORY_SECTION)
#define VX1000_STRUCT_DATA VX1000_MEMORY_SECTION volatile VX1000_STRUCT_T gVX1000
#else /* !VX1000_MEMORY_SECTION */
#define VX1000_STRUCT_DATA volatile VX1000_STRUCT_T gVX1000
#endif /* !VX1000_MEMORY_SECTION */
#endif /* !VX1000_STRUCT_ADDR */
#endif /* !VX1000_TARGET_TRICORE & !VX1000_TARGET_XC2000 */

#if defined(VX1000_STRUCT_DATA)
extern VX1000_STRUCT_DATA;
#endif /* VX1000_STRUCT_DATA */

/* include user-defined lines with optional section pragmas to restore previous linkage of data: */
#define VX1000_ENDSECT_VXSTRUCT_H
#include "VX1000_cfg.h"  /* PRQA S 0883 */ /* Willingly violating MISRA rule 19.15: this file is included multiple times because it contains multiple disjunct parts that each are filtered exactly once by preprocessor conditions */ /* PRQA S 5087*/ /* Willingly violating MISRA rule 19.1: this file is included multiple at exactly those lines where it is needed to expand to the desired lines. Inclusion/expansion of those lines at the top of this file would not make sense and lead to compile errors */
#define VX1000_ENDSECT_VXSTRUCT_H_UNDO

/* include user-defined lines with optional section pragmas to restore previous linkage of data: */
#define VX1000_ENDSECT_VXMODULE_H
#include "VX1000_cfg.h"  /* PRQA S 0883 */ /* Willingly violating MISRA rule 19.15: this file is included multiple times because it contains multiple disjunct parts that each are filtered exactly once by preprocessor conditions */ /* PRQA S 5087*/ /* Willingly violating MISRA rule 19.1: this file is included multiple at exactly those lines where it is needed to expand to the desired lines. Inclusion/expansion of those lines at the top of this file would not make sense and lead to compile errors */
#define VX1000_ENDSECT_VXMODULE_H_UNDO

#if defined(VX1000_OVLENBL_PERSISTENT_EMEM)
/* include user-defined lines with optional section pragmas to force individual linkage of EMEM header data. */
#define VX1000_BEGSECT_EMEM_HDR_H
#include "VX1000_cfg.h"  /* PRQA S 0883 */ /* Willingly violating MISRA rule 19.15: this file is included multiple times because it contains multiple disjunct parts that each are filtered exactly once by preprocessor conditions */ /* PRQA S 5087*/ /* Willingly violating MISRA rule 19.1: this file is included multiple at exactly those lines where it is needed to expand to the desired lines. Inclusion/expansion of those lines at the top of this file would not make sense and lead to compile errors */
#define VX1000_BEGSECT_EMEM_HDR_H_UNDO

#if defined(VX1000_EMEM_HDR_DATA)
extern VX1000_EMEM_HDR_DATA;
#endif /* VX1000_EMEM_HDR_DATA */

/* include user-defined lines with optional section pragmas to restore previous linkage of data: */
#define VX1000_ENDSECT_EMEM_HDR_H
#include "VX1000_cfg.h"  /* PRQA S 0883 */ /* Willingly violating MISRA rule 19.15: this file is included multiple times because it contains multiple disjunct parts that each are filtered exactly once by preprocessor conditions */ /* PRQA S 5087*/ /* Willingly violating MISRA rule 19.1: this file is included multiple at exactly those lines where it is needed to expand to the desired lines. Inclusion/expansion of those lines at the top of this file would not make sense and lead to compile errors */
#define VX1000_ENDSECT_EMEM_HDR_H_UNDO
#endif /* VX1000_OVLENBL_PERSISTENT_EMEM */

#endif /* !VX1000_DISABLE_INSTRUMENTATION */

#endif /* !VX1000_H */

