// TASKING VX-toolset for TriCore
// Eclipse project linker script file
//
#if defined(__PROC_TC23X__)
#define __BMHD0_CONFIG __BMHD_GENERATE
#include "tc23x.lsl"
derivative my_tc23x extends tc23x
{
}
#else
#include <cpu.lsl>
#endif



/* allocate cinit: section = .text._c_init.libcs_fpu to a specific location */ 

section_layout mpe:vtc:linear
{
	group CSTART_NOT_CACHED(fill,ordered, contiguous, run_addr = mem:mpe:pflash0  / not_cached [0x400]) // / not_cached)
	{
		
		select ".text.cstart.*";
		select ".text._c_init.libcs_fpu";
	}
}

section_layout mpe:vtc:linear
{
	group TASKING_LIB(ordered, contiguous, fill, run_addr=0x80001000)
	{
		group TASKING_LIB_CODE(ordered, contiguous, fill)
		{
			select ".text.*.libcs_fpu";
			select ".text.*.libfp";
			select ".text.*.libc";
			select ".text.*.librt";
	
		}
        group TASKING_LIB_ROMDATA (fill,ordered, contiguous)
        {
            select ".rodata.*.libcs_fpu";
			select ".rodata.*.libfp";
            select ".rodata.librt";
            select ".rodata.libc";
        }

	}
}


section_layout mpe:vtc:linear
{
        group TASKING_LIBRARY_DATA (fill,ordered, contiguous, run_addr = mem:mpe:dspr0)
        {
                select ".data.*.libcs_fpu";
                select ".bss.*.libcs_fpu";
				select ".data.*.libfp";
				select ".bss.*.libfp";
                select ".data.librt";
                select ".bss.librt";
                select ".data.libc";
                select ".bss.libc";
                select ".data.librt";
        }
}

section_layout mpe:vtc:linear
{
	group APPLICATION_ROM_DATA(ordered, contiguous, fill, run_addr=0x80010000)
	{
		select ".rodata.*";
	}
}

 
/* Interrupt Service Routines in RAM - everyone wants speed !
* But before this works these routines need to be copied from their storage location in Flash to their designated RAM location
* The cstart initialization takes care of that
*/
section_layout mpe:vtc:linear
{
	group my_RAMCODE_group ( run_addr = mem:mpe:pspr0, ordered, copy ) // was spe: spram on TC17xx
	{
		select ".text.ISR_RAMCODE";
	}
}

